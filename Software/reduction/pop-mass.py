#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 26 09:37:38 2022

@author: vaubaill
"""

import astropy.units as u
import numpy as np
import matplotlib.pyplot as plt

from showersim.phot import mass2radius,mag2mass_Hughes1995

def cumulmag2r(a):
    return 1.0+np.log10(a)

def population_index(a):
    return 10**a

def mass_index(r):
    return 1 + 2.3*np.log10(r)

def size_index(sm):
    return 3*sm - 2


# Mag contains magnitude of each meteors
Mag_TAH2022 = np.array([-0.19,-1.5,-2.06,0.87,0.92,1.19,-1.07,0.86,-1.4,0.27,-0.88,0.92,-0.68,1.44,-1.32,-0.31,-0.01,0.85,0.62,
     1.7,-0.21,-0.56,0.73,1.33,-0.65,0.41,-1.72,0.7,-1.09,0.26,-0.31,0.27,-0.82,0.56])
Mag = np.sort(Mag_TAH2022)
#Mag_SPE2016 = np.array([-11.3,-1.6,0.4,-2.1,-0.9,1.2,1.2,-0.1,1.2])
#Mag = np.sort(Mag_SPE2016[1:])#[:-3]


V = 12.2*u.km/u.s
omega = 50.0*u.deg
rho = 2500.0*u.kg/u.m**3.0
average_dist = 232.0*u.km
absmag_dist = 100*u.km
H = Mag*u.mag + 2.5*np.log10(average_dist / absmag_dist)*u.mag
Mass = mag2mass_Hughes1995(H,V)
radius = mass2radius(Mass,density=rho)
print('H: ',H.value)
print('Mass: ',np.sort(Mass.value))
print('radius: ',np.sort(radius.value))
print('diameter: ',np.sort(radius.value*2))
print('average radius: ',np.mean(radius.value))
print('radius (raw): ',radius.to('mm'))
print('total mass: ',np.sum(Mass))
print('total diameter: ',2.0*mass2radius(np.sum(Mass),density=rho))


# Magnitude distribution
histoCuMag,binMag,patches = plt.hist(Mag,bins=8,histtype='bar',cumulative=True,log=False)
histoCuMag,binMag,patches = plt.hist(H.value,bins=8,histtype='bar',cumulative=True,log=False)
fit_hcmag = np.polyfit(binMag[:-1],histoCuMag,1)
print('Fit Cumul Mag = ',fit_hcmag)
#plt.plot(binMag,(fit_hcmag[0]*binMag + fit_hcmag[1])),
#         label = str(round(fit_hcmag[0],2))+'*Mag+'+str(round(fit_hcmag[1],2)),
#         linestyle = '--')
plt.xlabel('Magnitude')
plt.ylabel('Cumulative number')
plt.legend()
plt.show()


# compute population index
r = cumulmag2r(fit_hcmag[0])
sm = mass_index(r)
smc = sm - 1
s = size_index(sm)
print('r = ',r)
print('sm = ',sm)
print('smc = ',smc)
print('s = ',s)

# Mass distribution
logMas = np.log10(Mass.to('kg').value)
histoCuMas,binMas,patches = plt.hist(logMas,bins=8,histtype='bar',cumulative=True,log=False)
fit_hcmas = np.polyfit(binMas[:-1],histoCuMas,1)
print('Fit Cumul Mas = ',fit_hcmas)
plt.plot(binMas,(fit_hcmas[0]*binMas + fit_hcmas[1]),
         label = str(round(fit_hcmas[0],2))+'*Mass+'+str(round(fit_hcmas[1],2)),
         linestyle = '--')
plt.xlabel('log(Mass)')
plt.ylabel('Cumulative number')
plt.legend()
plt.show()

# Size distribution
logRad = np.log10(radius.to('m').value)
histoCuRad,binRad,patches = plt.hist(logRad,bins=8,histtype='bar',cumulative=True,log=False)
fit_hcrad = np.polyfit(binRad[:-1],histoCuRad,1)
print('Fit CumulRads = ',fit_hcrad)
plt.plot(binMas,(fit_hcrad[0]*binRad + fit_hcrad[1]),
         label = str(round(fit_hcrad[0],2))+'*Radius+'+str(round(fit_hcrad[1],2)),
         linestyle = '--')
plt.xlabel('Radius')
plt.ylabel('Cumulative number')
plt.legend()
plt.xlim(np.min(binRad),np.max(binRad))
plt.show()
s2 = np.log10(np.abs(fit_hcrad[0]))
print('s2 = ',s2)


y = np.linspace(1,len(Mag),len(Mag))
y = np.log10(y)

print("Magnitude of meteors : Mag = ",Mag)
print("log(cumulative number of meteors) : y = ",y)

# lowM = 7
# higM = 6
# endM = 32
lowM = None
higM = None
endM = None

if not lowM:
    lowM = 0
if not higM:
    lowM = len(Mag)
if not endM:
    endM = len(Mag)

# choose points for the fit (high magnitudes)
x_fit_high = Mag[higM:endM:]
y_fit_high = y[higM:endM:]

# choose points for the fit (low magnitudes)
x_fit_low = Mag[:lowM:]
y_fit_low = y[:lowM:]

# fit with a first-degree polynomial
fit_high = np.polyfit(x_fit_high,y_fit_high,1)
print('Fit for high magnitudes = ',fit_high)

fit_low = np.polyfit(x_fit_low,y_fit_low,1)
print('Fit for low magnitudes = ',fit_low)

# plot data
plt.plot(Mag,y,'o')
plt.ylim((0,np.max(y)+0.5))
# plot the fit for high magnitudes
#plt.plot(x_fit_high, fit_high[0]*x_fit_high + fit_high[1], label = 'high Mag',linewidth=4)
#plt.plot(x,(fit_high[0]*x + fit_high[1]),label = str(round(fit_high[0],2)) + '*x + ' + str(round(fit_high[1],2)),linestyle = '--')
# plot the fit for low magnitudes
#plt.plot(x_fit_low, fit_low[0]*x_fit_low + fit_low[1], label = 'low Mag',linewidth=4)
#plt.plot(x,(fit_low[0]*x + fit_low[1]),label = str(round(fit_low[0],2)) + '*x + ' + str(round(fit_low[1],2)),linestyle = '-.')
#plt.plot(x,np.repeat(y[-1],len(y)),label = 'x=log(34.)')
plt.xlabel('Magnitude')
plt.ylabel('Cumulative number (log)')
plt.legend()

# r: population index
# sm: mass index
# s: size index
r_high = population_index(fit_high[0])
sm_high = mass_index(r_high)
s_high = size_index(sm_high)
print('For high magnitudes: r = ', r_high, ', sm = ', sm_high, ' and s = ', s_high)

r_low = population_index(fit_low[0])
sm_low = mass_index(r_low)
s_low = size_index(sm_low)
print('For low magnitudes: r = ', r_low, ', sm = ', sm_low, ' and s = ', s_low)