from detect_stars_all2 import detect_stars

# set parameters
path='/Users/aac/projects/meteors/malbec/test_data/field2/'
CEILING=10
fwhm0=6.0
nsigma_thres=10.0
photometry=False

detect_stars(fpath=path,CEILING=CEILING,fwhm0=fwhm0,
             nsigma_thres=nsigma_thres,photometry=photometry)
