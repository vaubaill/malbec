#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 11:39:37 2024

@author: vaubaill
"""

import astropy.units as u
import numpy as np
from astropy. constants import g0


rho_atm = 1.204 * u.kg / u.m**3
Cd=0.47
rhoc = 2700*u.kg/u.m**3
r=np.array([0.01,0.012])*u.m
m=4./3. * np.pi * rhoc * r**3
A = np.pi * r**2
v=(np.sqrt(2*m*g0 / (Cd*rho_atm*A))).to('km/h')
E  = (0.5*m*v**2).to('J')
print(E)