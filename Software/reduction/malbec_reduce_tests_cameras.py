"""Reduce images taken during camera tests.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project.
"""

import glob
import os
import logging
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
#from photutils import datasets, DAOStarFinder, CircularAperture,IRAFStarFinder
#from photutils import Background2D, SigmaClip, MedianBackground
from PIL import Image,ImageStat
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable,Table
from astropy import units as u
import astropy.io.ascii as ascii
from astropy.stats import sigma_clipped_stats
import subprocess


# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# goal: reduce images taken with malbec camera
# compute background noise, maximum pixel value, knowing that a priori only stars are bright in the images
# determine the equivalent gain when it is set to a value greater than 23.

# initializations
home_dir = os.getenv('HOME')+'/'

def reduce_test_camera(path,outfile='data.dat',offset_file='None.fits',mask_file='None.fits',Loffset=False):
    """Launches the reduction of camera tests.
    
    Parameters
    ----------
    path : string
        Directory where data are located.
    outfile : string, optional
        Name of data output file. Default is 'data.dat'.
    offset_file : string, optional
        Name of offset file for data correction. Default is 'None.fits'.
    mask_file : string, optional
        Name of mask image file. Default is 'None.fits'
    camera_lambda : astropy.units.Quantity Object.
        camera wavelength. Default is 6*u.um.
    Loffset : boolean, optional
        If True the master offset is created. Default is False.
    
    
    Returns
    -------
    None. Data are saved in outfile.
    
    """
    
    # read offset file if exists
    if os.path.exists(offset_file):
        hdu_offset = fits.open(offset_file)[0]
        offset = hdu_offset.data
    else:
        offset = 0.0
    
    # read mask file if exists
    if os.path.exists(mask_file):
        hdu_mask = fits.open(mask_file)[0]
        data_mask = hdu_mask.data
    else:
        data_mask = 1.0
    
    # get list of image files
    listimg = glob.glob(path+'*/FITS/*.fits')
    listimg.sort()
    
    # create output table
    all_data = QTable(names=['file','exp','gain','bck','std','min','max'],dtype=['object','float','float','float','float','float','float'])
    
    # loop over the images
    for imgfile in listimg:                         # get list of image files
        #log.info ('======= Now treating image: '+os.path.basename(imgfile))  
        hdu = fits.open(imgfile)[0]          # open file
        npixX = hdu.header['NAXIS1']
        npixY = hdu.header['NAXIS2']
        expo = float(hdu.header['EXPTIME']) * u.s
        gain = float(hdu.header['GAIN']) * u.electron/u.adu
        # performs basic stats
        data = hdu.data
        # multiply by the mask, remove the offset and the dark
        #data_new   = (data - offset) * data_mask
        data_new   = data
        #log.info ('computing basic stats...')
        median = np.median(data_new)
        std = np.std(data_new)
        mean = np.mean(data_new)
        minpix = np.min(data_new)
        maxpix = np.max(data_new)
        all_data.add_row([os.path.basename(imgfile),expo,gain,median,std,minpix,maxpix])
        log.info(os.path.basename(imgfile)+' exp: '+str(expo)+' gain: '+str(gain)+' med: '+str(median)+' std: '+str('{:5.2f}'.format(std))+' min: '+str(minpix)+' max: '+str(maxpix))
        # END of loop over fit images
    
    # save data in output file
    all_data['std'].info.format = '5.2f'
    all_data.sort(['exp','gain'])
    all_data.write(path+outfile,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('Data saved in '+path+outfile)

###########################################################
# make list of all directories to process
tests_dir = os.getenv('HOME')+'/data/TESTS_CAMERAS/'
# define which disrectories you want to process
root_dir = 'DMK' # 'Basler_acA2040-55um-Pentax-6mm-1.2-None_20-0.04'
# get list of all directores to process
list_tests_dirs = glob.glob(tests_dir+'*'+root_dir+'*')

# loop over directories to process.
for tests_dir in list_tests_dirs:
    if os.path.isdir(tests_dir):
        reduce_test_camera(tests_dir+'/')
