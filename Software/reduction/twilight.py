# goal: compute twilight as a function of altitude

 
import numpy as np
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.coordinates import get_sun
import matplotlib.pyplot as plt
import itertools

target_name	= 'Polaris'
target		= SkyCoord.from_name(target_name)  
midnight	= Time('2017-8-12 00:00:00')
delta_midnight	= np.linspace(-1, 9, 10)*u.hour
times		= midnight + delta_midnight

for alt in np.linspace(0,40,5): # unit is km
 R_earth	= 6400.0*u.km
 beta		= np.arccos(R_earth/(R_earth+alt*u.km))
 print 'altitude=',alt,' beta=',beta.to('deg')
 site		= EarthLocation(lat=43.707311*u.deg, lon=-0.251529*u.deg, height=alt*u.km)
 altazframe	= AltAz(obstime=times, location=site)
 sunaltazs	= get_sun(times).transform_to(altazframe)
 targetaltazs	= target.transform_to(altazframe) 
 for (t,el) in itertools.izip(times,sunaltazs.alt):
  print t, el.to('deg')+beta.to('deg')


"""
plt.plot(delta_midnight, targetaltazs.secz)  
plt.xlim(-2, 7)  
plt.ylim(1, 4)  
plt.xlabel('Hours from EDT Midnight')  
plt.ylabel('Airmass [Sec(z)]')
plt.plot(delta_midnight, sunaltazs.alt, color='y', label='Sun')  
plt.scatter(delta_midnight, targetaltazs.alt, c=targetaltazs.az, label=target_name, lw=0, s=8)  
plt.fill_between(delta_midnight.to('hr').value, 0, 90, sunaltazs.alt < -0*u.deg, color='0.5', zorder=0)  
plt.fill_between(delta_midnight.to('hr').value, 0, 90, sunaltazs.alt < -18*u.deg, color='k', zorder=0)  
plt.colorbar().set_label('Azimuth [deg]')  
plt.legend(loc='upper left')  
plt.xlim(-12, 12)  
plt.xticks(np.arange(13)*2 -12)  
plt.ylim(0, 90)  
plt.xlabel('Hours from EDT Midnight')  
plt.ylabel('Altitude [deg]')  
"""
