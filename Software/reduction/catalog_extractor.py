"""
07/02/2014
Author : FRIPON
Copyright : Observatoire de Paris, FRIPON
Free license
"""
import ephem
import numpy as np
from astropy.table import QTable, vstack,Column
from astropy.coordinates import SkyCoord,AltAz
import astropy.units as u
from fripipe import path_and_file as pthf

class CatalogExtractor(object):

    star_cat = QTable(names=['ALT','AZ','RA','DEC','Mag','bv','ri','X','Y','R','Time'])
    
    def __init__(self):
        self.__configdir = pthf.fri_pipeline_path+'astrometry/config/'
        self.__bsc5file = self.__configdir + 'bsc5.dat'
        return
    
    def extract_stars(self,cat_file,station,times,LimMag):
        """Extract star data from catalog.
        
        Parameters
        ----------
        cat_file : string
            Reference Star catalog file name.
        station : fripipe.definitions.station.Station Object.
            Station from which the reference stars are to be extracted.
        times : array of astropy.time.Time Object.
            Time for which the reference stars are to be extracted.
        LimMag :  float
            Desired limiting Magnitude. MUST be >5.
        
        Returns
        -------
        star_cat : astropy.table.QTable Object
            Refrence Star Catalog containing the stars
            visible from Station station at time times.
        
        """
        star_cat = QTable(names=['ALT','AZ','RA','DEC','Mag','bv','ri','X','Y','R','Time'])
        pthf.fri_log.info('Now reading catalog file : '+cat_file)
        cat_data = QTable.read(cat_file,format='ascii.fixed_width_two_line',delimiter=' ',
                               names=['RA','DEC','Mag','bv','ri','pmra','pmdec'])
        locmag = np.where(cat_data['Mag']<LimMag)
        num_mag = np.asarray(locmag).size
        pthf.fri_log.info('There are '+str(num_mag)+' reference stars of Mag<'+str(LimMag))
        if (num_mag==0):
            msg = '*** FATAL ERROR: There is no star with mag<'+str(LimMag)+' in '+cat_file
            pthf.fri_log.error(msg)
            raise ValueError(msg)
        cat_data = cat_data[locmag]
        Lmerge=False
        star_cat_interm = QTable()
        # solve the case where there is only one time
        try:
            len(times)
        except:
            times = [times]
        for time in sorted(set(times)):
            pthf.fri_log.info('Determining risen stars from '+station.name+' at '+time.iso)
            star_cat_interm=0 # reset variable
            try:
                # if not first time in the loop
                star_cat_interm.remove_columns(['ALT','AZ','RA','DEC','Mag','bv','ri','X','Y','R','Time'])
            except:
                # if first time in the loop
                star_cat_interm = QTable()
            skycoo = SkyCoord(cat_data['RA'],cat_data['DEC'],unit=(u.deg,u.deg))
            altaz = skycoo.transform_to(AltAz(obstime=time,location=station.eloc))
            locrisen = np.where(altaz.alt>0.0)
            num_risen = np.asarray(locrisen).size
            pthf.fri_log.info('There are '+str(num_risen)+' stars of Mag<'+str(LimMag)+' above the horizon from '+station.name+' at '+time.iso)
            if (num_risen==0):
                pthf.fri_log.warning('*** WARNING: There is no star with mag<'+str(LimMag)+' above the horizon from '+station.name+' at '+time.iso)
                continue
            cat_data = cat_data[locrisen]
            skycoo = skycoo[locrisen]
            altaz = altaz[locrisen]
            star_cat_interm.add_columns((Column(data=altaz.alt.to('deg').value,name='ALT',unit='deg'),
                                         Column(data=altaz.az.to('deg').value,name='AZ',unit='deg'),
                                         Column(data=skycoo.ra.to('deg').value,name='RA',unit='deg'),
                                         Column(data=skycoo.dec.to('deg').value,name='DEC',unit='deg'),
                                         Column(data=cat_data['Mag'],name='Mag',unit='mag'),
                                         Column(data=cat_data['bv'],name='bv',unit='mag'),
                                         Column(data=cat_data['ri'],name='ri',unit='mag'),
                                         Column(data=np.repeat(0.0,num_risen),name='X',unit='pixel'),
                                         Column(data=np.repeat(0.0,num_risen),name='Y',unit='pixel'),
                                         Column(data=np.repeat(0.0,num_risen),name='R',unit='pixel'),
                                         Column(data=np.repeat(time.mjd,num_risen),name='Time',unit='d')
                                         ))
            if Lmerge:
                pthf.fri_log.info('merging output catalogs for time: '+time.isot)
                star_cat = vstack([star_cat,star_cat_interm])
            else:
                pthf.fri_log.info('creating output catalogs for time: '+time.isot)
                star_cat = star_cat_interm
            Lmerge=True
        pthf.fri_log.info('done')
        return star_cat
        
    def LimMag2catfilename(self,LimMag):
        """Get the star catalog file name given a Limiting Magnitude.
        
        The file name is constructed as 'bsc5-qtable-sub'+str(LimMag)+'.dat'
        
        Parameters
        ---------
        LimMag : float
            Limiting magnitude. Must be lower or equal to 5.
        
        Returns
        -------
        cat_file : string
            Star catalog file name.
        
        """
        return pthf.fri_pipeline_path+'astrometry/config/bsc5-qtable-sub'+str(LimMag)+'.dat'
    
    
    def make_subsc5(self,LimMag):
        """Make a subversion of the bsc5 catalog already existing in Fripipe.
        
        Selection is performed based on the Limiting Magnitude.
        
        Parameters
        ----------
        LimMag : float
            Reference star catalog Limiting Magnitude.
            It MUST be lower than 5 since stars are extract from bsc5.
        
        Returns
        -------
        cat_file_out : string
            Output star catalog file name.
        
        """
        cat_file_out = self.LimMag2catfilename(self,LimMag)
        # check if catalog already exist, in which case it is useless to do anything.
        if not os.path.exists(starcat_file):
            pthf.fri_log.info('Now making reference star catalog file '+cat_file_out)
            cat_file_out = self.bsc2qtable(LimMag)
        # now double checking that the file was indeed created.
        if not os.path.exists(starcat_file):
            msg = '*** FATAL ERROR: star catalog file '+cat_file_out+' was not created.'
            pthf.fri_log.error(msg)
            raise IOError(msg)
        return cat_file_out
    
    def bsc2qtable(self,LimMag):
        """Converts a sub-section of the Bright Star Catalog into a QTable.
        
        Limiting Magnitude of th BSC is 5.
        
        Parameters
        ---------
        LimMag : float
            Limiting magnitude. Must be lower or equal to 5.
        
        Returns
        -------
        cat_file_out : string
            Output star catalog file name.
        
        """
        star_cat = QTable(names=['RA','DEC','Mag','bv','ri','pmra','pmdec'])
        
        pthf.fri_log.info('Now reading catalog file : '+self.__bsc5file)
        data = QTable.read(self.__bsc5file,format='ascii.fixed_width_no_header',col_starts=[75,77,79,83,84,86,88,90,103,107,109,114,121,126,148,154,160],
                            names=['RAh','RAm','RAs','DEsgn','DEd','DEm','DEs','t1','Mag','t2','bv','ri','t3','t4','pmra','pmdec','t7'])
        cat_data = QTable(data,masked=True)
        locmag = np.where(cat_data['Mag']<LimMag)
        if (np.asarray(locmag).size==0):
            msg = '*** FATAL ERROR: There is no star with mag<'+str(LimMag)+' in '+self.__bsc5file
            pthf.fri_log.error(msg)
            raise ValueError(msg)
        cat_data = cat_data[locmag]
        
        for (rah,ram,ras,desgn,ded,dem,des,mask,mag,bv,ri,pmra,pmdec) in zip(cat_data['RAh'],cat_data['RAm'],cat_data['RAs'],cat_data['DEsgn'],cat_data['DEd'],cat_data['DEm'],cat_data['DEs'],cat_data['RAh'].mask,cat_data['Mag'],cat_data['bv'],cat_data['ri'],cat_data['pmra'],cat_data['pmdec']):
           if not mask:
               skycoostr = str(rah).zfill(2) +':'+ str(ram).zfill(2)+':' +str(ras)+' ' +str(desgn)+str(ded).zfill(2)+':'+str(dem).zfill(2)+':'+str(des).zfill(2)
               skycoo = SkyCoord(skycoostr,unit=(u.hourangle,u.deg))
               star_cat.add_row([skycoo.ra,skycoo.dec,mag,bv,ri,pmra,pmdec])
        # set units
        star_cat['RA'].unit='deg'
        star_cat['DEC'].unit='deg'
        star_cat['Mag'].unit='mag'
        star_cat['bv'].unit='mag'
        star_cat['ri'].unit='mag'
        star_cat['pmra'].unit='arcsec/yr'
        star_cat['pmdec'].unit='arcsec/yr'
        # save the file
        cat_file_out = self.__bsc5file.replace('.dat','-qtable-sub'+str(LimMag)+'.dat')
        star_cat.write(cat_file_out,format='ascii.fixed_width_two_line',delimiter=' ',overwrite=True)
        pthf.fri_log.info('Reference star sub-catalog saved in '+cat_file_out)
        return cat_file_out
