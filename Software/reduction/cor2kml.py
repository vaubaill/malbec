"""
installation:
 sudo pip install astropy
 sudo pip install simplekml

syntax:
 python cor2kml file_name



"""
import os
import sys
import astropy.io.ascii as ascii
from astropy.time import Time as Time
from astropy.table import QTable, Column
from astropy import units as u
import simplekml


# get input file name
filein=sys.argv[1]
"""
try:
    fmt=sys.argv[2]
except:
    fmt='2019'
"""

# check if file exists
if not os.path.isfile(filein):
    msg = '*** FATAL ERROR: file '+filein+' does not exist'
    raise IOError(msg)

# create output file name.
if os.path.dirname(filein):	# Solve the slash bar syndrome
    slashbar = '/'
else:
    slashbar = ''
filout=os.path.dirname(filein)+slashbar+os.path.basename(filein).split('.')[0]+".kml"
filout_dat=os.path.dirname(filein)+slashbar+os.path.basename(filein).split('.')[0]+".dat"

# determine day of observation thanks to file name
datestr     = filein.split('/')[-1].split('-')[0].split('AS')[1]
time_start  = Time(datestr[0:4]+'-'+datestr[4:6]+'-'+datestr[6:8]+'T00:00:00')
print('Time start: '+time_start.isot)

# reads data
print('Now reading input file '+filein)
data = QTable.read(filein,format='ascii')
try:
    data.rename_column('Temps','Time')
    print('[Temps] column replaced by [Time]')
except:
    print('[Time] column is present in data')
data['Time'].unit = u.s
time = time_start + data['Time']
data['Altitude'].unit = u.m
data['Latitude'].unit = u.rad
data['Longitude'].unit = u.rad
dim = len(data['Latitude'])

# convert to deg
data['Latitude'] = data['Latitude'].to('deg')
data['Longitude']= data['Longitude'].to('deg')


# save data into dat file
data.replace_column('Time',time.isot)
ascii.write(data,filout_dat,format='fixed_width',delimiter=' ',overwrite=True)
print(str(dim)+" data saved in "+filout_dat)

# convert into kml data and save into output file
kml = simplekml.Kml()
coords = []
i=0
for dat in data:
    coords.append((dat['Longitude'].to('deg').value,
                   dat['Latitude'].to('deg').value,
                   dat['Altitude'].to('m').value))
    pnt = kml.newpoint(name="",coords=[coords[i]],extrude=1)
    pnt.altitudemode = simplekml.AltitudeMode.absolute
    pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_square.png'
    pnt.timestamp.when=time[i].isot
    i = i+1
lin = kml.newlinestring(name="Path", description="Balloon REAL path",coords=coords)
lin.altitudemode = simplekml.AltitudeMode.absolute
kml.save(filout)
print(str(dim)+" data saved in "+filout)

