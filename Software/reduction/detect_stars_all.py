# -*- coding: utf-8 -*-
"""
Spyder Editor

program written by A. Christou, modified by J. Vaubaillon
last update: 09/12/2019
"""
# this programs reads in a list of fits images
# detects stars in each one and 
# estimates the Full Width at Half Maximum (FWHM)
# it uses the astropy and photutils modules 
# developed on Python 3.6
import os
import glob
import logger
import warnings
import numpy as np
import random
import matplotlib.pyplot as plt
from matplotlib.colors import PowerNorm
#
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,SigmaClip
from astropy.modeling import models, fitting
#
from photutils import DAOStarFinder, CircularAperture
from photutils import Background2D, MedianBackground
#

# set logger
log = logging.getLogger(__name__)
# set log level
log.setLevel(logging.INFO)
log.propagate = True
# log format
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
# create StreamHandler
sdlr = logging.StreamHandler()
#sdlr.setLevel(logging.INFO)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
# add StreamHandler into logger
log.addHandler(sdlr)


def detect_star_all(path='./',CEILING=None,fwhm0=6.0,nsigma_thres=10.0,photometry=False):
    """Detect stars in a list of fits images.
    
    Parameters
    ----------
    path : string, optional
        Path where fits images are located.
        Default is './'.
    CEILING : integer
        Max number of images to process.
        If None, all images are processed.
        Default is None. To use for debug purpose.
    fwhm0 : float, optional
        FHWM to use for the star-finding algorithm.
        Too large value will not find stars close together.
        Too small value will catch bright stars multiple times.
        Typical values are 4-8. Default is 6.0.
    nsigma_thres : float, optional
        How many sigma above backgrd to search for sources.
        Low values give many false detections and is slow.
        High values is fast but will miss some stars.
        Typical values are 6-10. Default is 10.0.
    photometry : logical, optional
        If True the star detection is performed on the raw image.
        If False, the detection is performed on the background subtracted image.
        Default is False.
    
    
    Returns
    -------
    TBF
    
    
    """
    # get list of files in fpath
    pattern = fpath+'*.fit*'
    image_list = glob.glob(pattern)
    # double check that the list of images is not empty, otherwise raise IOError.
    if not len(listfiles):
        msg = '*** There is no file of pattern '+pattern
        log.error(msg)
        raise IOError(msg)
    # initialize plot
    plt.figure(1)
    plt.clf()
    # select image files: take into account the CEILING parameter
    if CEILING:
        image_list_limit = image_list[:CEILING]
    else:
        image_list_limit = image_list
    # loop over file names
    for filename in image_list_limit:
        # go to next file is filename is empty
        if not len(filename):
            continue
        # process the file. first open it and get data.
        hdu=fits.open(filename)
        data=hdu[0].data
        data=data.astype(float)
        mean, median, std = sigma_clipped_stats(data,sigma=10.0)
        # apply median filter to estimate background
        sigma_clip = SigmaClip(sigma=3.0)
        bkg_estimator = MedianBackground()
        bkg = Background2D(data, (50, 50), filter_size=(6, 6),
            sigma_clip=sigma_clip, bkg_estimator=bkg_estimator)
        # subtract background from image
        # do not do this if doing photometry: replace next line with data2=data
        if photometry:
            data2=data
        else:
            data2=data-bkg.background
        # detect stars in (background subtracted) image: create DAOStarFinder object.
        daofind = DAOStarFinder(fwhm=fwhm0, threshold=nsigma_thres*std,
                # sharplo=-1.0, sharphi=1.0, roundlo=0.0, roundhi=12.5,
                  exclude_border=True)
        # runs the star detection on (background subtracted) image.
        sources = daofind(data2) 
        half_max=0.5*(np.max(data2)-np.min(data2))
        # pick a random source
        pick=random.sample(np.arange(len(sources)),1)
        # pick a source that is not too bright or too faint
        while np.abs(sources['peak'][pick[0]]-half_max) > 0.5*half_max: 
                   pick=random.sample(range(len(sources)),1)
        p_init = models.Gaussian2D(amplitude=sources['peak'][pick[0]],
                x_mean=0,y_mean=0,
                # for a gaussian profile, fwhm=2.355*sigma
                x_stddev=fwhm0/2.355,y_stddev=fwhm0/2.355)
        fit_p = fitting.LevMarLSQFitter()
        # fit gaussian on a 10x10 grid to estimate fwhm
        width=10    
        y, x = np.mgrid[:width, :width]
        xcentre=int((sources['xcentroid'][pick[0]]))
        ycentre=int((sources['ycentroid'][pick[0]]))
        with warnings.catch_warnings():
        # Ignore model linearity warning from the fitter
             warnings.simplefilter('ignore')
             p = fit_p(p_init, x,y,
                        data[ycentre-int(width/2):ycentre+int(width/2),
                        xcentre-int(width/2):xcentre+int(width/2)])
        positions = np.transpose((sources['xcentroid'],
                                  sources['ycentroid']))
        # amber circles show detected sources
        apertures = CircularAperture(positions, r=12.)
        # red circle shows source used to extract fwhm    
        highlight = CircularAperture([sources['xcentroid'][pick[0]], 
                                     sources['ycentroid'][pick[0]]],
                                     r=25.)
        norm = PowerNorm(0.3)
        plt.imshow(data2, cmap='gray', norm=norm)
        apertures.plot(color='yellow', lw=1.5, alpha=0.5)
        highlight.plot(color='red', lw=2.5, alpha=0.5)
        # short delay before displaying next image
        plt.pause(1)
        # log some info
        log.info('Image ',index,': ',hdu[0].header['DATE'],
                   data.shape,(mean, median, std))
        log.info('Detected ', len(sources), ' sources')
        log.info('FWHM: {:.3f} x {:.3f} pixels\n'.format(2.355*p.x_stddev.value,
                        2.355*p.y_stddev.value)) 
        #
        hdu.close()
        # close plot
        plt.clf()
