#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 10:32:13 2022

@author: vaubaill
"""

import numpy as np
import astropy.units as u
from astropy.constants import L_sun, c

# observer altitude 
h_a = 0.10*u.km
# meteor altitude
h_m = 100.0*u.km
# relative altitude
h = h_m - h_a


# compute average physical distance
d_phy = 714.0*u.km

# meteor atmosphereic velocity
v = 65.0*u.km/u.s
# cluster duration
t = 7.0*u.s

# Earth orbital velocity
v_e = 30.3 *u.km/u.s
# maximum distance given velocity and duration
d_dur = v_e * t

# maximum total distance
d_tot = np.sqrt(d_phy**2+d_dur**2)

print('d_phy = ',d_phy)
print('d_dur = ',d_dur)
print('d_tot = ',d_tot)


# estimate the time of breakup at zero velocity,
# considering the acceleration of the Solar radiation pressure
# assume particle diameter
d = 1.0*u.mm # 5.8 - 18.4
# density
rho = 2500.0*u.kg/u.m**3
# heliocentric distance
r_h = 150.0E+06*u.km
# estimate Q
Q = 1.0#/u.W 
# compute acceleration
acc = 3.0*L_sun*Q / (8.0*np.pi*c * r_h**2 *rho*d.to('m'))

t_d = np.sqrt(2.0*d_tot.to('m')/acc.to('m/s2'))



print('acc = ',acc.decompose())
print('t_d = ',t_d.to('d'))
