"""Rename directory name so tha the camera brand, name, lens brand and lens name are easily retrieved.

"""

import os
import glob
import copy

datapth='/media/ccolomer/MyPassport/DATA/TESTS_CAMERAS/'

#pattern = ['Basler_acA1920-155um-','-None']
#replcmt = ['Basler_acA1920-155um_','_None']
#pattern = ['DMK33UX273']
#replcmt = ['DMK_33UX273']
pattern = ['-Com']
replcmt = ['_Com']

listdir = glob.glob(datapth+'*'+pattern[0]+'*')

for src in listdir:
    srcori = copy.copy(src)
    for pat,rep in zip(pattern,replcmt):
        dst = src.replace(pat,rep)
        src = dst
        if not os.path.isdir(srcori):
            raise ValueError(srcori+' does not exist')
        else:
            print('mv '+os.path.basename(srcori)+'  '+os.path.basename(dst))
            #print('src: '+srcori+'  dst: '+dst)
            
