class person:
    def __init__(self):
        self.__name=''
        self.__fullname = ('','')
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self, name):
        self.__name=name
    
    @property
    def fullname(self):
        return self.__fullname
    @fullname.setter
    def fullname(self,firs_name,last_name):
        self.__fullname = (firs_name,last_name)


def displaydecorator(fn):
    def display_wrapper(str):
        print('Output:', end=" ")
        fn(str)
    return display_wrapper



@displaydecorator
def display(str):
    print(str)



p1=person()
p1.name="Steve"
print(p1.name)
p1.fullname=["Steve","McQueen"]

print(p1.fullname)
