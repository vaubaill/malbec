import glob
from onera_skybrightness import get_onera_skybgns


listf=glob.glob('/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/ONERA/0.4-14um/*.csv')

for f in listf:
    get_onera_skybgns(f,mkplot=True)
