"""Rename directory name so tha the camera brand, name, lens brand and lens name are easily retrieved.

"""

import os
import glob
import copy
from lens import Lens
from camera import Camera
import astropy.units as u


datapth='/media/ccolomer/MyPassport/DATA/TESTS_CAMERAS/'

listdir = glob.glob(datapth+'*')

for camdir in listdir:
    print('camdir'+camdir)
    [cam_brand,cam_name,lens_bn,filter_name,gainexp] = os.path.basename(camdir).split('_')
    print('cam_brand: '+cam_brand)
    print('cam_name: '+cam_name)
    print('lens_bn: '+lens_bn)
    print('filter_name: '+filter_name)
    print('gainexp: '+gainexp)
    [lns_band,lns_name] = lens_bn.split('-',1)
    print('lns_band: '+lns_band)
    print('lns_name: '+lns_name)
    [gain,expt] = gainexp.split('-')
    print('gain: '+gain)
    print('expt: '+expt)
    
    
    lns = Lens(brand=lns_band,name=lns_name)
    cam = Camera(brand=cam_brand,name=cam_name,lens=lns)
    cam.set_gain(float(gain)*u.dB)
    cam.info()
