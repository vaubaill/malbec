from astroquery.nist import Nist
import astropy.units as u

"""
Meteor spectra line (from Borovicka et al. Meteoroid book, chapter 2: Physical and Chemical properties of meteoroids)

Fe I : 527-545 nm (multiplet 15)
Na I 589 nm (doublet)
Mg I 518 nm (triplet)

Ca I 423 nm
Ca II 393nm and 397 nm
Cr I 425-430 nm
Mn I 403 nm
Mg I 383 nm

above 650 nm: mainly O, N N2: atmospheric (from N/O measurement)

Na I 819 nm (doublet)
K I 770 nm
K I 776 nm: overlaps with O2 absorption


"""

lambda_beg = 526.0 * u.nm
lambda_end = 546.0 * u.nm

table = Nist.query(lambda_beg,lambda_end, linename="Fe I",energy_level_unit='eV', output_order='wavelength',wavelength_type='vacuum')
# remove low intensity line
for rel in table['Rel.']:
    if 'bl' in rel:
        rel.mask=True
        
table.write('FeI.dat',format='ascii.fixed_width_two_line',overwrite=True)



from astroquery.atomic import AtomicLineList
wavelength_range = (lambda_beg,lambda_end)
table = AtomicLineList.query_object(wavelength_range, wavelength_type='Air', wavelength_accuracy=20, element_spectrum='Fe I',features="html5lib")



from astroquery.hitran import Hitran
"""
1 	H2O 	Water
2 	CO2 	Carbon Dioxide
3 	O3 	Ozone
4 	N2O 	Nitrogen oxide
5 	CO 	Carbon Monoxide
6 	CH4 	Methane
7 	O2 	Oxygen
8 	NO 	Nitric Oxide
9 	SO2 	Sulfur Dioxide
10 	NO2 	Nitrogen Dioxide
11 	NH3 	Ammonia
12 	HNO3 	Nitric Acid
13 	OH 	Hydroxyl
"""
tbl = Hitran.query_lines(molecule_number=13,isotopologue_number=1,min_frequency=lambda_beg,max_frequency=lambda_end)
tbl.write('Hitran.dat',format='ascii.fixed_width_two_line',overwrite=True)
