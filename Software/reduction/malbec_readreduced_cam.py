import glob
import os
import logging
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
#from photutils import datasets, DAOStarFinder, CircularAperture
#from photutils import Background2D, SigmaClip, MedianBackground
from PIL import Image,ImageStat
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable
from astropy import units as u
import astropy.io.ascii as ascii
from astropy import constants as const

from reduction.camera import Camera
from reduction.lens import Lens

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# USER DEFINED initializations
data_file           = '../../DATA/20170812/Results/ALLDATA.dat'
# create Lens Object : value for the 201708 flight
lens = Lens(brand='Pentax',name='12mm-1.4')
# create Camera Object : value for the 201708 flight
cam = Camera(brand='Basler',name='acA640-100gm',lens=lens)

# pixel area in [m^2]
A_pix   = cam.sensor_pixsz[0] * cam.sensor_pixsz[1]
# pixel x-size projected unto the sky [rad]
x_skysz = cam.sensor_pixsz[0] * 360.0 / (2.0* np.pi * lens.F) * u.rad
# pixel y-size projected unto the sky [rad]
y_skysz = cam.sensor_pixsz[1] * 360.0 / (2.0* np.pi * lens.F) * u.rad
# pixel size projected unto the sky [deg^2]
A_sky   = x_skysz.to('deg') * y_skysz.to('deg')

# Basler system gain, from documentation
camera_lamba    = 0.6 *  1.0E-06 * u.m          # wavelength [m]
QE              = 0.5 * u.electron / u.photon   # Quantum efficiency at camera_lamba
Dark_noise      = 2.0 * u.adu                   # Dark noise

log.info('reading data file '+data_file)
# read an offset image here
if not os.path.exists(data_file):
    msg = '*** FATAL ERROR: file '+data_file+' does not exist'
    log.error(msg)
    raise IOError(msg)
data = QTable.read(data_file,  format='ascii',names=('Alt','Sun_el','BCK','expo','gain'))
data['Alt'].unit='km'
data['Sun_el'].unit='deg'
data['BCK'].unit='adu'
data['expo'].unit='s'
data['gain'].unit='electron/adu'
# now select ascending flight only
loc_explode =np.argmax(data['Alt'].value)   # location of altitude of balloon explosion
loc_takeoff =np.where(data['Alt'][:loc_explode].to('km').value > 1.0)[0][0]

# get effective gain, knowing that the camera is limited by a given gain
# this must be converted into multiplication factor
locmaxg = np.where(data['gain'] > cam.max_gain_dB)
if len(locmaxg):
    data['gain'][locmaxg] = np.repeat(0.0,len(locmaxg))*u.electron/u.adu
eff_gain =  10.0**(data['gain'][loc_takeoff:loc_explode].value/20.0) *u.electron/u.adu

n_electron  = eff_gain * (data['BCK'][loc_takeoff:loc_explode]) #  - Dark_noise : useless since images are already dark corrected
n_photons   = n_electron / QE
E           = n_photons/u.photon * const.h * const.c / (A_pix * camera_lamba * data['expo'][loc_takeoff:loc_explode] ) # irradiance on the sensor surface [W/m2]
E_perdegsq  = E / A_sky         # irradiance per deg^2
E_persterad = E_perdegsq.to('W/m2/steradian')

# plots Irradiance vs altitude
locpos=np.where(E_perdegsq.value>0.0)
E_alt_plot_file     = 'EAlt.png'
fig=plt.figure()
plt.plot(data['Alt'][loc_takeoff:loc_explode][locpos],np.log10(E_perdegsq[locpos].value),'bo',linewidth=1.5)
plt.title('Altitude vs Irradiance')
plt.xlabel('Altitude [km]')
plt.ylabel('log(Irradiance) [W/m2/deg2]')
plt.savefig(E_alt_plot_file)
plt.close(fig)
log.info('plot saved in '+E_alt_plot_file)
#
E_alt_plot_file     = 'EAlt-ste.png'
fig=plt.figure()
plt.plot(data['Alt'][loc_takeoff:loc_explode][locpos],np.log10(E_persterad[locpos].value),'bo',linewidth=1.5)
plt.title('Altitude vs Irradiance')
plt.xlabel('Altitude [km]')
plt.ylabel('log(Irradiance) [W/m2/steradian]')
plt.savefig(E_alt_plot_file)
plt.close(fig)
log.info('plot saved in '+E_alt_plot_file)

# plots Irradiance vs Sun elevation
E_Sunel_plot_file     = 'ESunel.png'
fig=plt.figure()
plt.plot(data['Sun_el'][loc_takeoff:loc_explode][locpos],np.log10(E_perdegsq[locpos].value),'bo',linewidth=1.5)
plt.title('Sun elevation vs Irradiance')
plt.xlabel('Sun elevation [deg]')
plt.ylabel('log(Irradiance) [W/m2/deg2]')
plt.savefig(E_Sunel_plot_file)
plt.close(fig)
log.info('plot saved in '+E_Sunel_plot_file)
#
E_Sunel_plot_file     = 'ESunel-ste.png'
fig=plt.figure()
plt.plot(data['Sun_el'][loc_takeoff:loc_explode][locpos],np.log10(E_persterad[locpos].value),'bo',linewidth=1.5)
plt.title('Sun elevation vs Irradiance')
plt.xlabel('Sun elevation [deg]')
plt.ylabel('log(Irradiance) [W/m2/steradian]')
plt.savefig(E_Sunel_plot_file)
plt.close(fig)
log.info('plot saved in '+E_Sunel_plot_file)

# plot exposure vs altitude
exp_alt_plot_file   = 'ExpAlt.png'
fig=plt.figure()
plt.plot(data['expo'][loc_takeoff:loc_explode],data['Alt'][loc_takeoff:loc_explode],'bo',linewidth=1.5) # SET A LOG SCALE FOR Y-AXIS !!!
plt.title('Altitude vs Exposure time')
plt.ylabel('Altitude [km]')
plt.xlabel('Exposure time [s]')
plt.xscale('log')
plt.savefig(exp_alt_plot_file)
plt.close(fig)
log.info('plot saved in '+exp_alt_plot_file)

# now plot results
# plot Background vs Altitude
Bck_alt_plot_file   = 'BckgAlt.png'
fig=plt.figure()
plt.plot(data['Alt'][loc_takeoff:loc_explode],data['BCK'][loc_takeoff:loc_explode],'bo',linewidth=1.5)
plt.title('Background vs Altitude')
plt.ylabel('Bckg [ADU]')
plt.xlabel('Altitude [km]')
plt.savefig(Bck_alt_plot_file)
plt.close(fig)
log.info('Background vs Altitude saved in '+Bck_alt_plot_file)
# plot Background vs Sun elevation
Bck_sunel_plot_file = 'BckgSunel.png'
fig=plt.figure()
plt.plot(data['Sun_el'][loc_takeoff:loc_explode],data['BCK'][loc_takeoff:loc_explode],'bo',linewidth=1.5)
plt.title('Background vs Sun elevation')
plt.ylabel('Bckg (ADU)')
plt.xlabel('Sun elevation [deg]')
plt.savefig(Bck_sunel_plot_file)
plt.close(fig)
log.info('Background vs Sun elevation saved in '+Bck_sunel_plot_file)
# plot Altitude vs Sun elevation
Alt_sunel_plot_file = 'AltSunel.png'
fig=plt.figure()
plt.plot(data['Sun_el'][loc_takeoff:loc_explode],data['Alt'][loc_takeoff:loc_explode],'bo',linewidth=1.5)
plt.title('Altitude vs Sun elevation')
plt.ylabel('Altitude [km]')
plt.xlabel('Sun elevation [deg]')
plt.savefig(Alt_sunel_plot_file)
plt.close(fig)
log.info('Altitude vs Sun elevation saved in '+Alt_sunel_plot_file)
