"""
07/02/2014
Author : Min-Kyung Kwon, FRIPON
Copyright : Observatoire de Paris, FRIPON
Free license
"""
import ephem
from catalog_extractor import CatalogExtractor
import numpy as np
from matplotlib import pylab
from camera import Camera
#import f2n
from ZPN_fit import ZPNFit
from k1k2_fit import K1K2Fit


class Local_Sky(object):
    __camera = Camera()
    __site = ephem.Observer()
    __raw_sources=[]
    __recentered_sources_xyr=[]
    __matched_sources =[]
    __ref_stars_cat=[]
    __image=0
    __image_in_path=''
    __image_out_path=''
    
    def __init__(self,site,camera,ref_stars_cat,image_in_path,image_out_path):
        self.__camera=camera
        self.__site=site
        self.__ref_stars_cat=ref_stars_cat
        self.__altaz_to_distortedxy()
        d= np.loadtxt('config/CAT_OUT.dat')
        x=np.array(d[:,8]).tolist() # XWIN_IMAGE
        y=np.array(d[:,9]).tolist() # YWIN_IMAGE
        self.__raw_sources.append(x)
        self.__raw_sources.append(y)
        self.__init_sources()
        self.__init_matched_sources()
        self.__image_in_path= image_in_path
        self.__image_out_path= image_out_path
        #self.__draw_image()
        
        
    """
    def __draw_image(self):
        self.__image = f2n.fromfits(self.__image_in_path)
        self.__image.setzscale(1, "ex")
        self.__image.makepilimage("log", negative = True)
        for i in xrange (0,len(self.__ref_stars_cat[9])):
            self.__image.drawcircle(self.__camera.get_opt_center()[0]+self.__ref_stars_cat[9][i],self.__camera.get_opt_center()[1]+self.__ref_stars_cat[10][i],int((8-float(self.__ref_stars_cat[4][i]))), (0,255,0))
        self.__image.drawcircle(self.__camera.get_opt_center()[0], self.__camera.get_opt_center()[1],5, (0,0,255))
        self.__image.tonet(self.__image_out_path)
        """
    
    def __init_sources (self):       
        self.__recentered_sources_xyr[:]=[]
        r=[]
        for i in xrange (0,len (self.__raw_sources[0])):
            r.append(np.sqrt(((self.__raw_sources[0][i]-self.__camera.get_opt_center()[0])**2)+((self.__raw_sources[1][i]-self.__camera.get_opt_center()[1])**2)))
        self.__recentered_sources_xyr.append(self.__raw_sources[0]-np.asarray(self.__camera.get_opt_center()[0]))
        self.__recentered_sources_xyr.append(self.__raw_sources[1]-np.asarray(self.__camera.get_opt_center()[1]))
        self.__recentered_sources_xyr.append(r)

    
    #initialize the list that will be filled with matched sources
    def __init_matched_sources (self):
        alt=[]      # 0
        az=[]       # 1
        ra=[]       # 2
        dec=[]      # 3
        m=[]        # 4
        bv=[]       # 5
        ri=[]       # 6
        pmra=[]     # 7
        pmdec=[]    # 8
        x=[]        # 9
        y=[]        # 10
        r=[]        # 11
        self.__matched_sources.append (alt)
        self.__matched_sources.append (az)
        self.__matched_sources.append (ra)
        self.__matched_sources.append (dec)
        self.__matched_sources.append (m)
        self.__matched_sources.append (bv)
        self.__matched_sources.append (ri)
        self.__matched_sources.append (pmra)
        self.__matched_sources.append (pmdec)
        self.__matched_sources.append (x)
        self.__matched_sources.append (y)
        self.__matched_sources.append (r)


    def associate_elevation (self, max_radius):
        found = 0
        for i in xrange (0, len(self.__recentered_sources_xyr[0])):
            min_dist_index=0
            min_dist = max_radius
            neighbour_found = False
            for j in xrange (0, len (self.__ref_stars_cat[0])):
                dist = np.sqrt(((self.__recentered_sources_xyr[0][i] - self.__ref_stars_cat[9][j])**2)+((self.__recentered_sources_xyr[1][i]-self.__ref_stars_cat[10][j])**2))
                if (dist< min_dist):
                    min_dist = dist
                    min_dist_index =j
                    neighbour_found = True
                else : 
                    continue
            if (neighbour_found == False):
                continue
            else :
                found = found+1
                for k in xrange (0, len(self.__matched_sources)-3):
                    self.__matched_sources[k].append(self.__ref_stars_cat[k][min_dist_index])
                self.__image.drawcircle(self.__raw_sources[0][i],self.__raw_sources[1][i],1, (200,0,0))
                self.__matched_sources[9].append(self.__recentered_sources_xyr[0][i])
                self.__matched_sources[10].append(self.__recentered_sources_xyr[1][i])
                self.__matched_sources[11].append(self.__recentered_sources_xyr[2][i])
                self.__image.drawline(self.__raw_sources[0][i],self.__camera.get_ccd_size_y()-self.__raw_sources[1][i],self.__ref_stars_cat[9][min_dist_index]+self.__camera.get_opt_center()[0],self.__camera.get_ccd_size_y()-(self.__ref_stars_cat[10][min_dist_index]+self.__camera.get_opt_center()[1]),1,(0,0,255))
        self.__image.tonet(self.__image_out_path)
        print('ASSOCIATED : ',found,'/',i)

        
    def __altaz_to_distortedxy(self):
        stars_distortedx=[]
        stars_distortedy=[]
        stars_distortedr=[]
        for i in xrange (0,len(self.__ref_stars_cat[0])):
            stars_distortedx.append(self.__camera.get_k1()*self.__camera.get_f()*np.sin(((np.pi/2)-self.__ref_stars_cat[0][i])/self.__camera.get_k2())*np.cos(self.__ref_stars_cat[1][i])/self.__camera.get_px_size())
            stars_distortedy.append(self.__camera.get_k1()*self.__camera.get_f()*np.sin(((np.pi/2)-self.__ref_stars_cat[0][i])/self.__camera.get_k2())*np.sin(self.__ref_stars_cat[1][i])/self.__camera.get_px_size())
            stars_distortedr.append(np.sqrt((stars_distortedx[i])**2+(stars_distortedy[i]**2)))
        self.__ref_stars_cat[9] = stars_distortedx
        self.__ref_stars_cat[10] = stars_distortedy
        self.__ref_stars_cat[11] = stars_distortedr
     
    def rotate (self,angle):
        new_x=[]
        new_y=[]
        new_r=[]
        for i in xrange (0,len(self.__ref_stars_cat[9])):
            new_x.append(self.__ref_stars_cat[9][i]*np.cos(angle*np.pi/180)- self.__ref_stars_cat[10][i]*np.sin(angle*np.pi/180))
            new_y.append(self.__ref_stars_cat[9][i]*np.sin(angle*np.pi/180)+ self.__ref_stars_cat[10][i]*np.cos(angle*np.pi/180))
            new_r.append(np.sqrt((new_x[i]**2)+(new_y[i]**2)) )
        self.__ref_stars_cat[9]=new_x
        self.__ref_stars_cat[10]=new_y
        self.__ref_stars_cat[11]=new_r
        self.__camera.set_rotation(angle)
        self.__init_sources()
        #self.__draw_image()
        
    def shift (self,x,y):
        self.__camera.set_opt_center((self.__camera.get_opt_center()[0]+x, self.__camera.get_opt_center()[1]+y))
        self.__altaz_to_distortedxy()
        self.__init_sources()
        #self.__draw_image()
        
    def tilt (self, angle):
        return
        
    def scale (self,k1):
        self.__camera.set_k1(k1)
        self.__altaz_to_distortedxy()
        self.__init_sources()
        #self.__draw_image()
        
    def distort(self,k2):
        self.__camera.set_k2(k2)
        self.__altaz_to_distortedxy()
        self.__init_sources()
        #self.__draw_image()
        
    def optimize_zpn (self,n):
        zpn = ZPNFit(self.__camera,n)
        final,std=zpn.optimize_p(np.asarray(self.__matched_sources[0]), self.__camera.get_px_size()*np.asarray(self.__matched_sources[9]), self.__camera.get_px_size()*np.asarray(self.__matched_sources[10]))
        print("std after optimization : ", std)
        p1,=pylab.plot(np.asarray(self.__matched_sources[0])*180/np.pi,self.__matched_sources[11],'r+')
        p2,=pylab.plot(np.asarray(self.__matched_sources[0])*180/np.pi,final,'b+')
        pylab.title('ZPN')
        pylab.legend([p1,p2],["sources","fit"])
        pylab.show()
        
    def optimize_k1k2(self):      
        fe_fit = K1K2Fit(self.__camera)
        (x0,y0),starAx,starAy,starBx,starBy,starCx,starCy=fe_fit.optimize_center(self.__camera.get_px_size()*self.__camera.get_opt_center()[0],self.__camera.get_px_size()*self.__camera.get_opt_center()[1],self.__camera.get_px_size()*np.asarray(self.__raw_sources))
        final,std =fe_fit.optimize_k1k2(np.asarray(self.__matched_sources[0]), self.__camera.get_px_size()*np.asarray(self.__matched_sources[9]), self.__camera.get_px_size()*np.asarray(self.__matched_sources[10]))
        print("std after optimization : ", std)
        print("manual optical center : ",self.__camera.get_opt_center())
        print("computed optical center : ",(x0,y0))
        p1,=pylab.plot(np.asarray(self.__matched_sources[0])*180/np.pi,self.__matched_sources[11],'r+')
        p2,=pylab.plot(np.asarray(self.__matched_sources[0])*180/np.pi,final,'b+')
        pylab.title('K1 K2')
        pylab.legend([p1,p2],["sources","fit"])
        pylab.show()
        


orsay = ephem.Observer()
orsay.long='02:11:15'
orsay.lat ='+48:41:53'
orsay.elevation = 70
orsay.date = '2014-02-27 01:04:23'
cat = CatalogExtractor()
ref_cat_list=cat.build_ref_cat('config/bsc5.dat',orsay,5)
camera=Camera()
image_in_path="config/check.fits"
image_out_path="config/orsay_test.png"
orsay_sky = Local_Sky(orsay,camera,ref_cat_list,image_in_path,image_out_path)
orsay_sky.scale(1.733413)
orsay_sky.distort(1.655828)
orsay_sky.shift(20, 25)
orsay_sky.rotate(0.8)
orsay_sky.associate_elevation(3)
orsay_sky.optimize_zpn(5)
orsay_sky.optimize_k1k2()
