import glob, os
import logging
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
#from photutils import datasets, DAOStarFinder, CircularAperture
#from photutils import Background2D, SigmaClip, MedianBackground
from PIL import Image,ImageStat
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable
from astropy import units as u
import astropy.io.ascii as ascii
from astropy.stats import sigma_clipped_stats
import utils.malbec_tools

# goal: reduce images taken with malbec camera

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# initializations
R_earth         = (1.0*u.R_earth).to('km')

# user part:
#data_dir        = 'DATA/20170813'           # to change if needed
data_dir        = '/media/ccolomer/Elements/MALBEC_scicam/201708/20170813/'           # to change if needed
out_dir         = data_dir + '../results/'
loc_file        = '/home/ccolomer/malbec/DATA/20170812/CNES/AS201708130240-cor.cor'  # to change if needed
calib_dir       = 'DATA/20170813'           # to change if needed
camera_lamba    = 6.0E-07 *u.m              # center wavelength observation

# check that the data directory exists
for ddir in [data_dir,out_dir]:
    if not os.path.isdir(ddir):
        raise IOError('directory: '+ddir+' does not exist.')

# make and set master offset fits file
Lmake_offset=False
#offset_dir          ='/Volumes/BackUpMac/MALBEC/Basler/OFFSET/'
offset_dir          ='/media/ccolomer/Elements/MALBEC_scicam/201708/OFFSET/'
master_offset_file  = 'master_offset.fit'
if Lmake_offset:
    malbec_tools.malbec_mkimgaverage(imgdir=offset_dir,
                                     img_root_name='offset_',
                                     img_ext_name='.fit',
                                     master_name=master_offset_file,
                                     master_dir=offset_dir)
# open file
hdu_offset = fits.open(offset_dir+master_offset_file)[0]
offset = hdu_offset.data


# determine day of observation thanks to data_dir name
datestr = data_dir.split('/')[-2]
#log.info('datestr: '+str(datestr))
#time_loc_start = Time(data_dir[0:11]+'-'+data_dir[11:13]+'-'+data_dir[13:15]+'T00:00:00')
timestr = datestr[0:4]+'-'+datestr[4:6]+'-'+datestr[6:8]+'T00:00:00'
#log.info('timestr: '+timestr)
time_loc_start  = Time(timestr)
log.info('time_loc_start: '+time_loc_start.isot)

# first read localisation data file
log.info('now reading file '+loc_file)
data_loc                    = QTable.read(loc_file,  format='ascii')
data_loc['Temps'].unit      ='s'
data_loc['Altitude'].unit   ='m'
data_loc['Latitude'].unit   ='rad'
data_loc['Longitude'].unit  ='rad'
data_loc['Press'].unit      ='Pa'
data_loc['HumTemp'].unit    ='deg_C'
time_loc                    = time_loc_start+data_loc['Temps']
log.info('time_loc[0]: '+time_loc[0].isot)
log.info('time_loc[-1]: '+time_loc[-1].isot)

Lplot=True
if Lplot:
    # plot altitude vs time
    Alt_plot_file   = out_dir + 'AltT.png'
    fig=plt.figure()
    plt.plot(data_loc['Temps'].to('h').value,data_loc['Altitude'].to('km').value,'bo',linewidth=1.5)
    plt.title('Altitude vs Time')
    plt.xlabel('Time [hr after 0:00 UT]')
    plt.ylabel('Altitude [km]')
    plt.savefig(Alt_plot_file)
    plt.close(fig)
    log.info('Altitude curve saved in '+Alt_plot_file)
    # plot Time vs Temp
    Temp_plot_file	= out_dir + 'TempT.png'
    fig=plt.figure()
    plt.plot(data_loc['Temps'].to('h').value,data_loc['HumTemp'].to('deg_C').value,'bo',linewidth=1.5)
    plt.title('Temp vs Time')
    plt.xlabel('Time [hr after 0:00 UT]')
    plt.ylabel('Temp [C]')
    plt.savefig(Temp_plot_file)
    plt.close(fig)
    log.info('Temperature curve saved in '+Temp_plot_file)
    # plot Altitude vs Temp
    AltTemp_plot_file	= out_dir + 'AltTemp.png'
    fig=plt.figure()
    plt.plot(data_loc['Altitude'].to('km').value,data_loc['HumTemp'].to('deg_C').value,'bo',linewidth=1.5)
    plt.title('Altitude vs Temperature')
    plt.xlabel('Altitude [km]')
    plt.ylabel('Temp [C]')
    plt.savefig(AltTemp_plot_file)
    plt.close(fig)
    log.info('Altitude vs Temperature curve saved in '+AltTemp_plot_file)


# set up output file names
all_bck     = np.array([])
all_alt     = np.array([])
all_exp     = np.array([])
all_gain    = np.array([])
all_sunel   = np.array([])

# now loops over the images
listimg=glob.glob(data_dir+'*.fit')
nimg = len(listimg)
if not nimg:
    raise IOError('There is no fit image in '+data_dir)
listimg.sort()
for imgfile in listimg:                         # get list of image files
    log.info('======= Now treating image: '+imgfile)
    hdu        = fits.open(imgfile)[0]          # open file
    time_img   = Time(hdu.header['DATE-OBS'])   # retrieve img time
    sec_img    = time_img - time_loc_start      # compute seconds since 00:00:00 UT
    expo       = float(hdu.header['EXPTIME']) * u.s
    gain       = float(hdu.header['GAIN']) * u.electron/u.adu
    # interpolate location data to know where the image was taken
    Alt        =           np.interp(sec_img.to('s').value,data_loc['Temps'].to('s').value,data_loc['Altitude'].to('km').value) * u.km # [deg]
    lat        =  Latitude(np.interp(sec_img.to('s').value,data_loc['Temps'].to('s').value,data_loc['Latitude'].to('deg').value),unit='deg')   # [deg]
    lon        = Longitude(np.interp(sec_img.to('s').value,data_loc['Temps'].to('s').value,data_loc['Longitude'].to('deg').value),unit='deg')  # [deg]
    Press      =           np.interp(sec_img.to('s').value,data_loc['Temps'].to('s').value,data_loc['Press'].to('Pa').value) * u.Pa    # [deg]
    Temp       =           np.interp(sec_img.to('s').value,data_loc['Temps'].to('s').value,data_loc['HumTemp'].to('deg_C').value) * u.deg_C    # [deg]
    # compute location of Sun at the time of the exposure
    beta       = np.arccos(R_earth/(R_earth+Alt))
    site       = EarthLocation(lat=lat, lon=lon, height=Alt)
    altazframe = AltAz(obstime=time_img, location=site,pressure=Press,temperature=Temp,obswl=camera_lamba)
    sunaltazs  = get_sun(time_img).transform_to(altazframe)
    sunelevation   = sunaltazs.alt#+beta            # correct for altitude
    
    # performs basic stats
    data       = hdu.data
    mean, median, std = sigma_clipped_stats(data)
    log.info('mean,median,std='+str([mean,median,std]))
    if (mean==median):
       log.info('saturated image '+imgfile)
       continue
    # set dark fits file from temperature
    # remove the offset and the dark
    data_new   = data - offset
    mean, median, std  = sigma_clipped_stats(data_new, sigma=3.0)      # change paramters?
    
    # gather usefull data
    all_bck    = np.append(all_bck,median)
    all_alt    = np.append(all_alt,Alt.to('km').value)
    all_sunel  = np.append(all_sunel,sunelevation.to('deg').value)
    all_exp    = np.append(all_exp,expo.value)
    all_gain   = np.append(all_gain,gain.value)
    
    log.info('Time='+time_img.iso+' Alt='+str(Alt.to('km'))+' T='+str(Temp.to('deg_C'))+' Sun_el='+str(sunelevation.to('deg').value)+' bck='+str(median)+' exp='+str(expo.to('s')))
    
    
    """
    daofind        = DAOStarFinder(fwhm=2.0, threshold=2.*std) # change paramters?
    # search for stars
    sources        = daofind(data - median)
    nbstar         = len(sources)
    print nbstar,' stars found in image ',imgfile
    if (nbstar>0):
     positions     = (sources['xcentroid'], sources['ycentroid'])
     apertures     = CircularAperture(positions, r=4.)
     norm          = ImageNormalize(stretch=SqrtStretch())
     plt.imshow(data, cmap='Greys', origin='lower', norm=norm)
     apertures.plot(color='blue', lw=1.5, alpha=0.5)
     #plt.show(block=False) 
     plt.show() 
    
    # estimate the background with photutils
    # see: https://photutils.readthedocs.io/en/stable/photutils/background.html
    sigma_clip = SigmaClip(sigma=3., iters=10)
    bkg_estimator  = MedianBackground()
    bkg        = Background2D(data2, (50, 50), filter_size=(3, 3),sigma_clip=sigma_clip, bkg_estimator=bkg_estimator)
    print 'photutil.bck median=',bkg.background_median,' photutil.bck rms_median=',bkg.background_rms_median
    # plot the background image
    plt.imshow(bkg.background, norm=norm, origin='lower',cmap='Greys_r')
    plt.show()
    # now plot the background corrected image
    plt.imshow(data - bkg.background, norm=norm, origin='lower',cmap='Greys_r')
    plt.show() 
    """
    
    # update the img file header
    hdu.header['SITELAT']  = (lat.to_string(unit=u.degree, sep=':'),'Latitude of Observatory, in deg')
    hdu.header['SITELON']  = (lon.to_string(unit=u.degree, sep=':'),'Longitude of Observatory, in deg')
    hdu.header['SITEALT']  = (Alt.to('m').value,'Altitude of Observatory, in m')
    hdu.header['TEMP'] = (Temp.to('deg_C').value,'Outside Temperature in C')
    hdu.header['PRESS']    = (Press.to('Pa').value,'Outside Pressure in Pa')
    hdu.header['COMMENT']  = ('Sun elevation: '+str(sunelevation.to('deg').value)+' deg')
    hdu.header['COMMENT']  = ('Beta: '+str(beta.to('deg').value)+' deg')
    hdu.header['COMMENT']  = ('Header updated by the malbec_reduce.py script')
    # save the reduced image in new file
    imgfile_new        = os.path.splitext(imgfile)[0] + '-c.fits'
    hdu.writeto(imgfile_new,overwrite=True)
    log.info('Reduced image saved in '+imgfile_new)
    # END of loop over fit images

# save table output
outfile = out_dir + 'ALLDATA.dat'
outable = QTable([all_alt,
                  all_sunel,
                  all_bck,
                  all_exp,
                  all_gain,
                  ],
                  names=('# Alt (km)','Sun_el (deg)','BCK (ADU)','exp (sec)','gain (e/adu)'))
outable.write(outfile,format='ascii.fixed_width',delimiter=' ',overwrite=True)
log.info('data saved in '+outfile)
log.info('done')
