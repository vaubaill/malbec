#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 13:06:42 2019

@author: dzilkova

Script to read data file and output kml file
"""
from configparser import ConfigParser, ExtendedInterpolation
import simplekml
from astropy.table import QTable
from astropy.time import Time
import astropy.units as u
from astropy.io import ascii

class Getkml(object):
    """ Convert data files from maballtrap into kml files
    
    Parameters
    ----------
        data_file.dat
        
    Returns
    -------
        file.kml
    
    """
    def __init__(self,config_file):
        """Initialize MaBallTraP Object.
        
        Parameters
        ----------
        config : string, optional
            full name of configuration parameters file.
        Returns
        -------
        datafiles
        
        """
        
        # get data from config files
        # set config_file
        self.config_file = config_file
        # creates configparser.ConfigParser() object for the configuration file
        self.config = ConfigParser(interpolation=ExtendedInterpolation())  
        self.config.read(config_file)
        
        # get data directly from simulation directory
        self.datafile_path = '/obs/dzilkova/malbec/DATA/20190523/CNES/Simulation/FLIGHT/'
        self.name = 'landing'
        self.datafile_ball = self.datafile_path + self.name +'.dat'
        
        # set the output directory and names
        self.fileout_dat = self.config['OUTPUT']['maball_root']+'/'+'ascend_cnes.dat'
        self.fileout_kml = self.config['OUTPUT']['maball_root']+'/'+'ascend_cnes.kml'
        self.fileout_dat_ball = self.datafile_path +'DAT/'+ self.name +'2.dat'
        self.fileout_kml_ball = self.datafile_path +'KML/'+self.name+'.kml'
        
        self.LoadData()
        #self.DataTableCnes()
        #self.savekmlCnes()
        self.DataTableMaBall()
        self.savekmlMaBall()
        
    def LoadData(self):
        """Load the data from data_files
        
        Parameters
        ----------
            data_file from InFlight measurements
        
        Returns
        -------
            data from data file : Altitude
                                  Pressure
                                  Time
                                  Altitude from Pressure
                                  Geometric Altitude
        """
        
        #datafile_cnes = self.config['WEATHER']['gps_file']
        
        
        # Read the data form data file from cnes          
        '''
        #names = ['Temps', 'Altitude', 'Latitude', 'Longitude', 'VE', 'VN', 'VVert', 'VHor', 'VDir', 'DP', 'T', 'U', 'Press', 'Flag']  # names for 2017 
        names=['Time','Altitude','Latitude','Longitude','VE','VN','Ascent','WindF','WindD','DP','T','U','Press','Flag']
        cols=(0, 26, 37, 53, 71, 80, 89, 99, 107, 115, 124, 133, 140, 149)  #  col for 20180627
        #cols=(0, 26, 37, 53, 74, 83, 92, 102, 110, 118, 127, 136, 144, 152)  # col for 20190219
        #cols=(0, 7, 17 ,27, 39, 45, 52, 59, 66,  73, 80, 87, 94, 101)   # col for 20171212
        self.dat = QTable().read(datafile_cnes,format='ascii.fixed_width', col_starts=cols, names=names, delimiter=' ') 
        
        self.Time_cnes = Time(self.dat['Time'], format='isot', scale='utc')
        self.Alt_cnes = self.dat['Altitude']*u.m
        self.Lat_cnes = (self.dat['Latitude'])*u.deg
        self.Lon_cnes = (self.dat['Longitude'])*u.deg
        self.T_cnes = self.dat['T']*u.K
        self.Press_cnes = self.dat['Press']*u.hPa
        self.Press_cnes_pa = self.Press_cnes.to('Pa')
         
        '''        
        # Read our computed data 
        self.dat = QTable().read(self.datafile_ball,format='ascii.fixed_width_two_line')
        
        self.Time_ball = Time(self.dat['Time'], format='isot', scale='utc')
        self.Alt_ball = self.dat['Altitude']*u.m
        self.Lat_ball = self.dat['Latitude']*u.deg
        self.Lon_ball = self.dat['Longitude']*u.deg
        self.T_ball = self.dat['Temperature']*u.K
        self.Press_ball = self.dat['Pressure']*u.Pa
        self.Vz_ball = self.dat['Vz']*u.m/u.s
        self.Vx_ball = self.dat['Vx']*u.m/u.s
        self.Vy_ball = self.dat['Vy']*u.m/u.s
        
    def DataTableCnes(self):
        
        self.dim=len(self.Alt_cnes)
        times=self.Time_cnes
        self.data=QTable([times,self.Lat_cnes,self.Lon_cnes, self.Alt_cnes],names=['Time','Latitude','Longitude','Altitude'])
        ascii.write(self.data,self.fileout_dat,format='fixed_width',delimiter=' ',overwrite=True)              
        return
    
    def DataTableMaBall(self):
        
        self.dim_ball=len(self.Alt_ball)
        times=self.Time_ball
        self.data_ball=QTable([times,self.Lat_ball,self.Lon_ball, self.Alt_ball],names=['Time','Latitude','Longitude','Altitude'])
        ascii.write(self.data_ball,self.fileout_dat_ball,format='fixed_width',delimiter=' ',overwrite=True)
        print (self.dim_ball," data saved in ",self.fileout_dat_ball)
        return    
        
        
    def savekmlCnes(self):
        # convert into kml data and save into output file
        kml = simplekml.Kml()
        coords = []
        for (t,lon,lat,alt) in zip(self.data['Time'],self.data['Longitude'].value,self.data['Latitude'].value,self.data['Altitude'].value):
          coords.append((lon,lat,alt))
          pnt = kml.newpoint(name="",coords=[(lon,lat,alt)],extrude=1)
          pnt.altitudemode = simplekml.AltitudeMode.relativetoground 
          pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
          #pnt.timestamp.when=t.isot
        lin = kml.newlinestring(name="Path", description="Balloon path",coords=coords)
        lin.altitudemode = simplekml.AltitudeMode.absolute
        kml.save(self.fileout_kml)
        print (self.dim," data saved in ",self.fileout_kml)
        
        return
    
    def savekmlMaBall(self):
        # convert into kml data and save into output file
        kml = simplekml.Kml()
        coords = []
        for (t,lon,lat,alt) in zip(self.data_ball['Time'],self.data_ball['Longitude'].value,self.data_ball['Latitude'].value,self.data_ball['Altitude'].value):
          coords.append((lon,lat,alt))
          pnt = kml.newpoint(name="",coords=[(lon,lat,alt)],extrude=1)
          pnt.altitudemode = simplekml.AltitudeMode.relativetoground 
          pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
          #pnt.timestamp.when=t.isot
        lin = kml.newlinestring(name="Path", description="Balloon path",coords=coords)
        lin.altitudemode = simplekml.AltitudeMode.absolute
        kml.save(self.fileout_kml_ball)
        print (self.dim_ball," data saved in ",self.fileout_kml_ball)
        
        return
    
config_file = '/obs/dzilkova/malbec/Software/MaBallTraP/conf/config.in'        
test = Getkml(config_file=config_file)     
