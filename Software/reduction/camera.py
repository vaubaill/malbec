"""Definition of a Camera object.

Author : J. Vaubaillon, IMCCE
The MALBEC project

"""
import logging
import numpy as np
import astropy.units as u
from astropy.table import QTable, Column, unique
from scipy import interpolate

from reduction.bands import bands
from missionplanner.utils import log


def gain_law1_analog2dB(analogGain):
    """Compute gain in dB from raw gain setting.
    
    This applies to the following Basler cameras: 
    acA640-90gm/gc
    acA640-100gm/gc
    acA640-120gm/gc
    acA645-100gm/gc
    acA750-30gm/gc
    acA780-75gm/gc
    acA1300-22gm/gc
    acA1300-30gm/gc
    acA1600-20gm/gc
    
    Parameters
    ----------
    analogGain : float
        Analog Gain.
    
    Returns
    -------
    GaindB : float
        Gain in dB.
    """
    GaindB = 0.0359 * analogGain.to('electron/adu').value
    return GaindB * u.dB
    
def gain_law1_dB2analog(GaindB):
    """Compute raw gain from gain in dB.
    
    This applies to the following Basler cameras: 
    acA640-90gm/gc
    acA640-100gm/gc
    acA640-120gm/gc
    acA645-100gm/gc
    acA750-30gm/gc
    acA780-75gm/gc
    acA1300-22gm/gc
    acA1300-30gm/gc
    acA1600-20gm/gc
    
    Parameters
    ----------
    GaindB : float
        Gain in dB.
    
    Returns
    -------
    analogGain : float
        Analog Gain.
    
    
    """
    analogGain = GaindB.to('dB').value / 0.0359
    return analogGain * u.electron / u.adu

def gain_law2_analog2dB(analogGain):
    """Compute gain in dB from raw gain setting.
    
    This applies to the following Basler cameras: 
    acA1280-60gm/gc
    acA1300-60gm/gc
    acA1600-60gm/gc
    
    Parameters
    ----------
    analogGain : float
        Analog Gain.
    
    Returns
    -------
    GaindB : float
        Gain in dB.
    
    """
    if analogGain<32*u.electron/u.adu:
        GaindB = 20.0*np.log10(1 + ((analogGain.to('electron/adu').value * 2) /64))
    elif analogGain<96*u.electron/u.adu:
        GaindB = 20.0*np.log10(2.0 * (1.0 + ((analogGain.to('electron/adu').value - 32.0) /64.0)))
    else:
        msg = '*** Analog gain MUST be less than 96. It is now set to '+str(analogGain)
        log.error(msg)
        raise ValueError(msg)
    return GaindB * u.dB
    
def gain_law2_dB2analog(GaindB):
    """Compute raw gain from gain in dB.
    
    This applies to the following Basler cameras: 
    acA1280-60gm/gc
    acA1300-60gm/gc
    acA1600-60gm/gc
    
    Parameters
    ----------
    GaindB : float
        Gain in dB.
    
    Returns
    -------
    analogGain : float
        Analog Gain.
    
    """
    if GaindB<6.02*u.dB:
        analogGain = ((10.0**(GaindB.to('dB').value / 20.0)) - 1.0) * 32.0
    elif GaindB<11.974*u.dB:
        analogGain = (10.0**(GaindB.to('dB').value / 20.0) / 2 - 1.0) * 64.0 + 32.0
    else:
        msg = '*** GaindB MUST be less than 11.97. It is now set to '+str(GaindB)
        log.error(msg)
        raise ValueError(msg)
    return analogGain * u.electron / u.adu

def gain_law3_analog2dB(analogGain):
    """Compute gain in dB from raw gain setting.
    
    This applies to the following Basler cameras: 
    acA640-300gm/gc,
    acA800-200gm/gc,
    acA1300-75gm/gc,
    acA1920-48gm/gc,
    acA2500-20gm/gc
    
    Parameters
    ----------
    analogGain : float
        Analog Gain.
    
    Returns
    -------
    GaindB : float
        Gain in dB.
    """
    GaindB = 20.0 * np.log10(analogGain.to('electron/adu').value / 138.0)
    return GaindB * u.dB
    
def gain_law3_dB2analog(GaindB):
    """Compute raw gain from gain in dB.
    
    This applies to the following Basler cameras: 
    acA640-300gm/gc,
    acA800-200gm/gc,
    acA1300-75gm/gc,
    acA1920-48gm/gc,
    acA2500-20gm/gc
    
    Parameters
    ----------
    GaindB : float
        Gain in dB.
    
    Returns
    -------
    analogGain : float
        Analog Gain.
    
    
    """
    analogGain = (10.0**(GaindB.to('dB').value / 20.0)) * 138.0
    return analogGain * u.electron / u.adu

def gain_law4_analog2dB(analogGain):
    """Compute gain in dB from raw gain setting.
    
    This applies to the following Basler cameras: 
    acA1920-40gm/gc
    acA1920-50gm/gc
    acA2040-35gm/gc
    acA2440-20gm/gc
    
    Parameters
    ----------
    analogGain : float
        Analog Gain.
    
    Returns
    -------
    GaindB : float
        Gain in dB.
    """
    GaindB = 0.1 * analogGain.to('electron/adu').value
    return GaindB * u.dB
    
def gain_law4_dB2analog(GaindB):
    """Compute raw gain from gain in dB.
    
    This applies to the following Basler cameras: 
    acA1920-40gm/gc
    acA1920-50gm/gc
    acA2040-35gm/gc
    acA2440-20gm/gc
    
    Parameters
    ----------
    GaindB : float
        Gain in dB.
    
    Returns
    -------
    analogGain : float
        Analog Gain.
    
    
    """
    analogGain = GaindB.to('dB').value / 0.1
    return analogGain * u.electron / u.adu


def gain_law5_analog2dB(analogGain):
    """Compute gain in dB from raw gain setting.
    
    This applies to the following Basler cameras: 
    acA2000-50gm/gc/gmNIR
    acA2040-25gm//gc/gmNIR
    acA3800-10gm/gc
    acA4600-7gc
    
    Parameters
    ----------
    analogGain : float
        Analog Gain.
    
    Returns
    -------
    GaindB : float
        Gain in dB.
    """
    GaindB = 20.0 * np.log10(analogGain.to('electron/adu').value / 32.0)
    return GaindB * u.dB
    
def gain_law5_dB2analog(GaindB):
    """Compute raw gain from gain in dB.
    
    This applies to the following Basler cameras: 
    acA2000-50gm/gc/gmNIR
    acA2040-25gm//gc/gmNIR
    acA3800-10gm/gc
    acA4600-7gc
    
    Parameters
    ----------
    GaindB : float
        Gain in dB.
    
    Returns
    -------
    analogGain : float
        Analog Gain.
    
    
    """
    analogGain = (10.0**(GaindB.to('dB').value / 20.0)) * 32.0
    return analogGain * u.electron / u.adu


def gain_law0_analog2dB(analogGain):
    """Compute gain in dB from raw gain setting.
    
    Parameters
    ----------
    analogGain : float
        Analog Gain.
    
    Returns
    -------
    GaindB : float
        Gain in dB.
    """
    GaindB = 20.0 * np.log10(analogGain.to('electron/adu').value)
    return GaindB * u.dB
    
def gain_law0_dB2analog(GaindB):
    """Compute raw gain from gain in dB.
    
    Parameters
    ----------
    GaindB : float
        Gain in dB.
    
    Returns
    -------
    analogGain : float
        Analog Gain.
    
    
    """
    analogGain = (10.0**(GaindB.to('dB').value / 20.0))
    return analogGain * u.electron / u.adu




# define dictionnary of Basler camera name and gain laws
dB2analog_laws = {'acA640-90gm' : gain_law1_dB2analog,
                  'acA640-90gc' : gain_law1_dB2analog,
                  'acA640-100gm' : gain_law1_dB2analog,
                  'acA640-100gc' : gain_law1_dB2analog,
                  'acA640-120gm' : gain_law1_dB2analog,
                  'acA640-120gc' : gain_law1_dB2analog,
                  'acA645-100gm' : gain_law1_dB2analog,
                  'acA645-100gc' : gain_law1_dB2analog,
                  'acA750-30gm' : gain_law1_dB2analog,
                  'acA750-30gc' : gain_law1_dB2analog,
                  'acA780-75gm' : gain_law1_dB2analog,
                  'acA780-75gc' : gain_law1_dB2analog,
                  'acA1300-22gm' : gain_law1_dB2analog,
                  'acA1300-22gc' : gain_law1_dB2analog,
                  'acA1300-30gm' : gain_law1_dB2analog,
                  'acA1300-30gc' : gain_law1_dB2analog,
                  'acA1600-20gm' : gain_law1_dB2analog,
                  'acA1600-20gc' : gain_law1_dB2analog,
                  'acA1280-60gm' : gain_law2_dB2analog,
                  'acA1280-60gc' : gain_law2_dB2analog,
                  'acA1300-60gm' : gain_law2_dB2analog,
                  'acA1300-60gmNIR' : gain_law2_dB2analog,
                  'acA1300-60gc' : gain_law2_dB2analog,
                  'acA1600-60gm' : gain_law2_dB2analog,
                  'acA1600-60gc' : gain_law2_dB2analog,
                  'acA640-300gm' : gain_law3_dB2analog,
                  'acA640-300gc' : gain_law3_dB2analog,
                  'acA800-200gm' : gain_law3_dB2analog,
                  'acA800-200gc' : gain_law3_dB2analog,
                  'acA1300-75gm' : gain_law3_dB2analog,
                  'acA1300-75gc' : gain_law3_dB2analog,
                  'acA1920-48gm' : gain_law3_dB2analog,
                  'acA1920-48gc' : gain_law3_dB2analog,
                  'acA2500-20gm' : gain_law3_dB2analog,
                  'acA2500-20gc' : gain_law3_dB2analog,
                  'acA1920-40gm' : gain_law4_dB2analog,
                  'acA1920-40gc' : gain_law4_dB2analog,
                  'acA1920-50gm' : gain_law4_dB2analog,
                  'acA1920-50gc' : gain_law4_dB2analog,
                  'acA1920-155um' : gain_law4_dB2analog,
                  'acA2040-35gm' : gain_law4_dB2analog,
                  'acA2040-35gc' : gain_law4_dB2analog,
                  'acA2440-20gm' : gain_law4_dB2analog,
                  'acA2440-20gc' : gain_law4_dB2analog,
                  'acA2000-50gm': gain_law5_dB2analog,
                  'acA2000-50gc': gain_law5_dB2analog,
                  'acA2000-50gmNIR': gain_law5_dB2analog,
                  'acA2000-165umNIR': gain_law5_dB2analog,
                  'acA2040-25gm' : gain_law5_dB2analog,
                  'acA2040-25gc' : gain_law5_dB2analog,
                  'acA2040-25gmNIR' : gain_law5_dB2analog,
                  'acA2040-55um' : gain_law5_dB2analog,
                  'acA3800-10gm' : gain_law5_dB2analog,
                  'acA3800-10gc' : gain_law5_dB2analog,
                  'acA4600-7gc' : gain_law5_dB2analog,
                  '33UX174' : gain_law0_dB2analog,
                  '33UX249' : gain_law0_dB2analog,
                  '33UX273' : gain_law0_dB2analog,
                  'alpha7s': gain_law0_dB2analog, # TODO: TO CHANGE
                  '3DCM734': gain_law0_dB2analog, # TODO: TO CHANGE
                  'eye': gain_law0_dB2analog, # TODO: TO CHANGE
                  'test': gain_law0_dB2analog, # TODO: TO CHANGE
                  'ASI6200': gain_law0_dB2analog # TODO: TO CHANGE
                  }
analog2dB_laws = {'acA640-90gm' : gain_law1_analog2dB,
                  'acA640-90gc' : gain_law1_analog2dB,
                  'acA640-100gm' : gain_law1_analog2dB,
                  'acA640-100gc' : gain_law1_analog2dB,
                  'acA640-120gm' : gain_law1_analog2dB,
                  'acA640-120gc' : gain_law1_analog2dB,
                  'acA645-100gm' : gain_law1_analog2dB,
                  'acA645-100gc' : gain_law1_analog2dB,
                  'acA750-30gm' : gain_law1_analog2dB,
                  'acA750-30gc' : gain_law1_analog2dB,
                  'acA780-75gm' : gain_law1_analog2dB,
                  'acA780-75gc' : gain_law1_analog2dB,
                  'acA1300-22gm' : gain_law1_analog2dB,
                  'acA1300-22gc' : gain_law1_analog2dB,
                  'acA1300-30gm' : gain_law1_analog2dB,
                  'acA1300-30gc' : gain_law1_analog2dB,
                  'acA1600-20gm' : gain_law1_analog2dB,
                  'acA1600-20gc' : gain_law1_analog2dB,
                  'acA1280-60gm' : gain_law2_analog2dB,
                  'acA1280-60gc' : gain_law2_analog2dB,
                  'acA1300-60gm' : gain_law2_analog2dB,
                  'acA1300-60gmNIR' : gain_law2_analog2dB,
                  'acA1300-60gc' : gain_law2_analog2dB,
                  'acA1600-60gm' : gain_law2_analog2dB,
                  'acA1600-60gc' : gain_law2_analog2dB,
                  'acA640-300gm' : gain_law3_analog2dB,
                  'acA640-300gc' : gain_law3_analog2dB,
                  'acA800-200gm' : gain_law3_analog2dB,
                  'acA800-200gc' : gain_law3_analog2dB,
                  'acA1300-75gm' : gain_law3_analog2dB,
                  'acA1300-75gc' : gain_law3_analog2dB,
                  'acA1920-48gm' : gain_law3_analog2dB,
                  'acA1920-48gc' : gain_law3_analog2dB,
                  'acA2500-20gm' : gain_law3_analog2dB,
                  'acA2500-20gc' : gain_law3_analog2dB,
                  'acA1920-40gm' : gain_law4_analog2dB,
                  'acA1920-40gc' : gain_law4_analog2dB,
                  'acA1920-50gm' : gain_law4_analog2dB,
                  'acA1920-50gc' : gain_law4_analog2dB,
                  'acA1920-155um' : gain_law4_analog2dB,
                  'acA2040-35gm' : gain_law4_analog2dB,
                  'acA2040-35gc' : gain_law4_analog2dB,
                  'acA2440-20gm' : gain_law4_analog2dB,
                  'acA2440-20gc' : gain_law4_analog2dB,
                  'acA2000-50gm': gain_law5_analog2dB,
                  'acA2000-50gc': gain_law5_analog2dB,
                  'acA2000-50gmNIR': gain_law5_analog2dB,
                  'acA2000-165umNIR': gain_law5_analog2dB,
                  'acA2040-25gm' : gain_law5_analog2dB,
                  'acA2040-25gc' : gain_law5_analog2dB,
                  'acA2040-25gmNIR' : gain_law5_analog2dB,
                  'acA2040-55um' : gain_law5_analog2dB,
                  'acA3800-10gm' : gain_law5_analog2dB,
                  'acA3800-10gc' : gain_law5_analog2dB,
                  'acA4600-7gc' : gain_law5_analog2dB,
                  '33UX174' : gain_law0_analog2dB,
                  '33UX249' : gain_law0_analog2dB,
                  '33UX273' : gain_law0_analog2dB,
                  'alpha7s': gain_law0_analog2dB, # TODO: TO CHANGE
                  '3DCM734': gain_law0_analog2dB, # TODO: TO CHANGE
                  'eye': gain_law0_analog2dB, # TODO: TO CHANGE
                  'test': gain_law0_analog2dB, # TODO: TO CHANGE
                  'ASI6200': gain_law0_analog2dB # TODO: TO CHANGE
                  }

class Camera(object):
    """Camera Object.
    
    Parameters
    ----------
    TBF
    
    
    """
    #Camera private attributes
    @property
    def brand(self):
        return self.__brand
    @brand.setter
    def brand(self,brand):
        self.__brand = brand
    
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,name):
        self.__name = name
    
    @property
    def known(self):
        return self.__known
    @known.setter
    def known(self,known):
        self.__known = known
    
    @property
    def img_sz(self):
        return self.__img_sz
    @img_sz.setter
    def img_sz(self,img_sz):
        self.__img_sz = img_sz
    
    @property
    def gain_law_analog2dB(self):
        return self.__gain_law_analog2dB
    @gain_law_analog2dB.setter
    def gain_law_analog2dB(self,gain_law_analog2dB):
        self.__gain_law_analog2dB = gain_law_analog2dB
    
    @property
    def gain_law_dB2analog(self):
        return self.__gain_law_dB2analog
    @gain_law_dB2analog.setter
    def gain_law_dB2analog(self,gain_law_dB2analog):
        self.__gain_law_dB2analog = gain_law_dB2analog
    
    @property
    def gain_dB(self):
        return self.__gain_dB
    @gain_dB.setter
    def gain_dB(self,gain_dB):
        self.__gain_dB = gain_dB
        # now set analogGain
        self.__analogGain = self.__gain_law_dB2analog(self.__gain_dB)
        #log.debug('analogGain in gain_dB: '+str(self.__analogGain))
    
    @property
    def analogGain(self):
        return self.__analogGain
    @analogGain.setter
    def analogGain(self,analogGain):
        self.__analogGain = analogGain
    
    @property
    def max_gain_dB(self):
        return self.__max_gain_dB
    @max_gain_dB.setter
    def max_gain_dB(self,max_gain_dB):
        self.__max_gain_dB = max_gain_dB
    
    @property
    def max_fps(self):
        return self.__max_fps
    @max_fps.setter
    def max_fps(self,max_fps):
        self.__max_fps = max_fps
    
    @property
    def fps(self):
        return self.__fps
    @fps.setter
    def fps(self,fps):
        self.__fps = fps
    
    @property
    def exp_time(self):
        return self.__exp_time
    @exp_time.setter
    def exp_time(self,exp_time):
        self.__exp_time = exp_time
    
    @property
    def max_nbbit(self):
        return self.__max_nbbit
    @max_nbbit.setter
    def max_nbbit(self,max_nbbit):
        self.__max_nbbit = max_nbbit
    
    @property
    def saturation_capacity(self):
        return self.__saturation_capacity
    @saturation_capacity.setter
    def saturation_capacity(self,saturation_capacity):
        self.__saturation_capacity = saturation_capacity
    
    # saturation level. Note: this one is set when self.__nbbit is set.
    @property
    def satur(self):
        return self.__satur
    @satur.setter
    def satur(self,satur):
        self.__satur = satur
    
    # system gain. Note: this one is set when self.__nbbit is set.
    @property
    def sys_gain(self):
        return self.__sys_gain
    @sys_gain.setter
    def sys_gain(self,sys_gain):
        self.__sys_gain = sys_gain
    
    # number of bits
    @property
    def nbbit(self):
        return self.__nbbit
    @nbbit.setter
    def nbbit(self,nbbit):
        self.__nbbit = nbbit
        # check if greater than self.__maxnbbit
        if (self.__nbbit > self.__max_nbbit):
            msg = '*** FATAL ERROR: nbbit > max_nbbit : '+str(self.__nbbit)+' > '+str(self.max__nbbit)
            log.error(msg)
            raise ValueError(msg)
        # set saturation level
        self.satur = (2.0**self.__nbbit - 1) * u.adu
        # set system gain
        self.sys_gain = self.saturation_capacity / (2.0**self.__nbbit) # [e-/ADU]
    
    @property
    def dark_noise(self):
        return self.__dark_noise
    @dark_noise.setter
    def dark_noise(self,dark_noise):
        self.__dark_noise = dark_noise
    
    @property
    def lens(self):
        return self.__lens
    @lens.setter
    def lens(self,lens):
        self.__lens = lens
        self.compute_fov()
    
    @property
    def sensor_name(self):
        return self.__sensor_name
    @sensor_name.setter
    def sensor_name(self,sensor_name):
        self.__sensor_name = sensor_name
    
    @property
    def xphysz(self):
        return self.__xphysz
    @xphysz.setter
    def xphysz(self,xphysz):
        self.__xphysz = xphysz.to('mm')
    
    @property
    def yphysz(self):
        return self.__yphysz
    @yphysz.setter
    def yphysz(self,yphysz):
        self.__yphysz = yphysz.to('mm')
    
    @property
    def sensor_physz(self):
        return self.__sensor_physz
    @sensor_physz.setter
    def sensor_physz(self,sensor_physz):
        self.__sensor_physz = sensor_physz
        (self.xphysz,self.yphysz) = self.__sensor_physz
    
    @property
    def xpixnum(self):
        return self.__xpixnum
    @xpixnum.setter
    def xpixnum(self,xpixnum):
        self.__xpixnum = xpixnum.to('mm')
    
    @property
    def ypixnum(self):
        return self.__ypixnum
    @ypixnum.setter
    def ypixnum(self,ypixnum):
        self.__ypixnum = ypixnum.to('mm')
    
    @property
    def sensor_pixnum(self):
        return self.__sensor_pixnum
    @sensor_pixnum.setter
    def sensor_pixnum(self,sensor_pixnum):
        self.__sensor_pixnum = sensor_pixnum
        (self.__xpixnum,self.__ypixnum) = self.__sensor_pixnum
    
    @property
    def xpixsz(self):
        return self.__xpixsz
    @xpixsz.setter
    def xpixsz(self,xpixsz):
        self.__xpixsz = xpixsz.to('um')
    
    @property
    def ypixsz(self):
        return self.__ypixsz
    @ypixsz.setter
    def ypixsz(self,ypixsz):
        self.__ypixsz = ypixsz.to('um')
    
    @property
    def sensor_pixsz(self):
        return self.__sensor_pixsz
    @sensor_pixsz.setter
    def sensor_pixsz(self,sensor_pixsz):
        self.__sensor_pixsz = sensor_pixsz
        (self.xpixsz,self.ypixsz) = self.__sensor_pixsz
    
    @property
    def QE(self):
        return self.__QE
    @QE.setter
    def QE(self,QE):
        self.__QE = QE
    
    
    __brand = 'Unknown'
    __name = 'Unknown'
    __xphysz = 0*u.mm
    __yphysz = 0*u.mm
    __xpixnum = 0*u.pixel
    __ypixnum = 0*u.pixel
    __xpx_sz = 0*u.mm
    __ypx_sz = 0*u.mm
    __img_sz = 0
    __max_gain_dB = 0*u.dB
    __gain_dB = 0*u.dB
    __analogGain = 0*u.electron/u.adu
    __lens = None
    __QE = None
    __known = False
    __nbbit = None
    __sys_gain = None
    
    #Camera object init
    def __init__(self,brand='Unknown',name='Unknown',pixnum=(0*u.pixel,0*u.pixel),px_size=(0.0*u.um,0.0*u.um),physz=(0*u.mm,0*u.mm),QE=None,gain=None,max_gain_dB=0*u.dB,max_fps=0/u.s,max_nbbit=0,nbbit=None,dark_noise=9999*u.electron,gain_law_analog2dB=lambda ga : ga,gain_law_db2analog=lambda gb: gb,lens=None):
        # set Brand and name
        self.brand = brand
        self.name = name
        # create QE table
        self.QE = QTable()
        self.QE.add_column(bands['name'],name='band_name')
        self.QE.add_column(bands['lambda'],name='lambda')
        # check if known
        self.check_known()
        
        # if not known, set other elements
        if not self.__known:
            self.sensor_physz = physz
            self.sensor_pixnum = pixnum
            self.sensor_pixsz = px_size
            self.max_gain_dB = max_gain_dB
            self.max_fps = max_fps
            self.max_nbbit(max_nbbit)
            self.dark_noise(dark_noise)
            self.gain_law_analog2dB(gain_law_analog2dB)
            self.gain_law_dB2analog(gain_law_db2analog)
            self.QE.add_column(Column(QE*u.electron/u.ph, name='QE'))
        
        # sort QE in order to prepare for interpolation
        self.QE.sort('lambda')
        # create temporary table with unique values of lambda
        tmptab = unique(self.QE,keys='lambda')
        # create QE interpolator function
        self.QEfunc = interpolate.interp1d(tmptab['lambda'].value,tmptab['QE'].value,kind='cubic',fill_value=0.0) # ,bounds_error=False,kind='cubic'
        
        if nbbit is not None:
            self.set_nbbit(nbbit)
        
        if gain is not None:
                self.set_gain(gain)
        
        if lens is not None:
            self.lens = lens
            # compute FOV
            self.compute_fov()
        
        return
    
    
    def set_log_level(self,level=logging.DEBUG):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG
        
        Returns
        -------
        None.
        
        """
        # set log level
        log.setLevel(level)
        return      
    
    
    def compute_fov(self):
        """Compute field of View, given Lens.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        # compute FOV
        self.fov = (2.0 * np.arctan2(np.array([self.__xphysz.to('mm').value,self.__yphysz.to('mm').value])/2.0,np.repeat(self.lens.F.to('mm').value,2)) *u.rad).to('deg')
        # compute diagonal FOV
        self.fovdiag = np.sqrt(self.fov[0]**2.0+self.fov[1]**2.0)
        # compute FOV for each pixel, assuming no distortion
        self.fov_pix = self.fov / self.sensor_pixnum.value # [deg]
        self.pix_sky_area = self.fov_pix[0] * self.fov_pix[1] # [deg2]
        # compute number of pixels in psf
        self.pixinpsf = self.lens.psf.to('um') / self.__sensor_pixsz
        # compute angle subtangled by all pixels in lens psf:
        xpsfangle = self.fov_pix[0]*self.pixinpsf[0]
        ypsfangle = self.fov_pix[1]*self.pixinpsf[1]
        self.psf_angle = np.sqrt(xpsfangle**2+ypsfangle**2)
        return
    
    def set_gain(self,gain):
        """Set gain in dB and electron/adu.
        
        Parameters
        ----------
        gain : astropy.units.Quantity object.
            Camera gain, either in [dB] or in [electron/adu]
        
        Returns
        -------
        None.
        
        """
        if gain.unit==u.dB:
            log.debug('Setting gain from dB, gain: '+str(gain))
            if gain>self.__max_gain_dB:
                gain = self.__max_gain_dB
            self.__gain_dB = gain
            self.__analogGain = self.__gain_law_dB2analog(self.gain_dB)
        if gain.unit==u.electron/u.adu:
            log.debug('Setting gain from u.electron/u.adu, gain: '+str(gain))
            self.__analogGain = gain
            self.__gain_dB = self.__gain_law_analog2dB(self.analogGain)
        return
    
    def set_nbbit(self,nbbit):
        """Set number of bit.
        
        Parameters
        ----------
        nbbit : float
            Number of bit.
        
        Returns
        -------
        None.
        
        """
        self.__nbbit = nbbit
        return
    
    def check_known(self):
        """Check if the Camera is known.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        self.__known : boolean
            True if the Camera is known, False otherwise.
        
        """
        self.known = False
        # Basler cameras
        if self.brand=='Basler':
            # acA640-100gm (Aug 2017) ; note: typo in FITS file header: ac640-100gm
            if self.name=='acA640-100gm':
                self.sensor_name = 'ICX618'
                self.sensor_physz = [3.70,2.80]*u.mm
                self.sensor_pixnum = [659,494] *u.pix
                self.sensor_pixsz = [5.60,5.60]*u.um
                # from https://www.ads-img.co.jp/secure/wordpress/wp-content/uploads/Manta_TechMan_V7_4_0.pdf p34s
                self.QE.add_column(Column([0.48,0.72,0.78,0.68,0.35,0.0 ,0.0 ,0.0 ,0.79,0.60,0.35,0.13]* u.electron/u.ph, name='QE'))
                self.max_fps = 100/u.s
                self.max_gain_dB = 36.7257*u.dB
                self.max_nbbit = 12
                self.dark_noise = 11.0*u.electron
                self.saturation_capacity = 16.6E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            # acA1300-60gmNIR (Dec 2017, 2018) EV76C661  5,3 x 5.3 um QE=59.0%, 23.2 e- noise 12 bits possible
            elif self.name=='acA1300-60gmNIR':
                self.sensor_name = 'EV76C661'
                self.sensor_physz = [6.80,5.40] *u.mm
                self.sensor_pixnum = [1280,1024]*u.pix
                self.sensor_pixsz = [5.30,5.30] *u.um
                # from https://imaging.teledyne-e2v.com/content/uploads/2019/01/QE-curve-3.jpg
                self.QE.add_column(Column([0.48,0.63,0.65,0.64,0.58,0.0 ,0.0 ,0.0 ,0.65,0.64,0.58,0.28]* u.electron/u.ph, name='QE'))
                self.max_fps = 60/u.s
                self.max_gain_dB = 95*u.dB
                self.max_nbbit = 12
                self.dark_noise = 23.2*u.electron
                self.saturation_capacity = 7.4E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            # acA2000-165umNIR     CMV2000 NIR-enhanced 2048 px x 1088 px  5.5 µm x 5.5 µm 63.9% 14.2 e¯ 12 bits
            elif self.name=='acA2000-165umNIR':
                self.sensor_name = 'CMV2000'
                self.sensor_physz = [11.30,6.00]*u.mm
                self.sensor_pixnum = [2048,1088]*u.pix
                self.sensor_pixsz = [5.50,5.50] *u.um
                # from https://forums.xilinx.com/xlnx/attachments/xlnx/XLNXBRD/8298/1/datasheet_CMV2000%20v3%202.pdf
                self.QE.add_column(Column([0.15,0.50,0.60,0.52,0.30,0.0 ,0.0 ,0.0 ,0.60,0.50,0.30,0.12]* u.electron/u.ph, name='QE'))
                self.max_fps = 165/u.s
                self.max_gain_dB = 24*u.dB
                self.max_nbbit = 12
                self.dark_noise = 14.2*u.electron
                self.saturation_capacity = 11.8E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            # IMX174 1920 px x 1200 px 5.86 µm x 5.86 µm 70.0% 6.6 e¯ 12 bits
            elif self.name=='acA1920-155um':
                self.sensor_name = 'IMX174'
                self.sensor_physz = [11.30,7.10] *u.mm
                self.sensor_pixnum = [1920,1200] *u.pix
                self.sensor_pixsz = [5.860,5.860]*u.um
                # from: https://www.ximea.com/downloads/usb3/manuals/xic_technical_manual.pdf
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 164/u.s
                self.max_gain_dB = 24*u.dB
                self.max_nbbit = 12
                self.dark_noise = 6.6*u.electron
                self.saturation_capacity = 32.7E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            elif self.name=='acA2040-55um':
                self.sensor_name = 'IMX265'
                self.sensor_physz = [7.10,5.3]   *u.mm
                self.sensor_pixnum = [2048,1536] *u.pix
                self.sensor_pixsz = [3.450,3.450]*u.um
                # from https://www.alliedvision.com/en/products/cameras/detail/Manta/G-319/action/pdf.html
                self.QE.add_column(Column([0.35,0.61,0.64,0.56,0.28,0.0 ,0.0 ,0.0 ,0.63,0.50,0.28,0.10]* u.electron/u.ph, name='QE'))
                self.max_fps = 55/u.s
                self.max_gain_dB = 24*u.dB
                self.max_nbbit = 12
                self.dark_noise = 2.2*u.electron
                self.saturation_capacity = 10.4E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            elif self.name=='acA4112-30um':
                self.sensor_name = 'IMX253'
                self.sensor_physz = [14.1,10.3]   *u.mm
                self.sensor_pixnum = [4096,3000] *u.pix
                self.sensor_pixsz = [3.450,3.450]*u.um
                # from https://www.baslerweb.com/fp-1520251673/media/downloads/documents/emva_data/BD00116201_Basler_acA4112-30um_EMVA_Standard_1288.pdf
                self.QE.add_column(Column([0.35,0.62,0.69,0.60,0.31,0.0 ,0.0 ,0.0 ,0.68,0.55,0.30,0.11]* u.electron/u.ph, name='QE'))
                self.max_fps = 30/u.s
                self.max_gain_dB = 24*u.dB
                self.max_nbbit = 12
                self.dark_noise = 2.4*u.electron
                self.saturation_capacity = 10.5E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
        # DMK cameras
        elif self.brand=='DMK':
            # DMK 33UX174
            if self.name=='33UX174':
                self.sensor_name = 'IMX174LQJ'
                self.sensor_physz = [11.3,7.1]  *u.mm
                self.sensor_pixnum = [1920,1200]*u.pix
                self.sensor_pixsz = [5.86,5.86] *u.um
                # from: https://www.ximea.com/downloads/usb3/manuals/xic_technical_manual.pdf
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 164/u.s
                self.max_gain_dB = 24*u.dB
                self.max_nbbit = 12
                self.dark_noise = 6.6*u.electron
                self.saturation_capacity = 32.7E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            if self.name=='33UX273':
                self.sensor_name = 'IMX273'
                self.sensor_physz = [4.968,3.726]*u.mm
                self.sensor_pixnum = [1440,1080] *u.pix
                self.sensor_pixsz = [3.45,3.45]  *u.um
                # QE from: https://www.alliedvision.com/en/products/cameras/detail/Manta/G-158/action/pdf.html
                self.QE.add_column(Column([0.40,0.57,0.64,0.56,0.28,0.0 ,0.0 ,0.0 ,0.63,0.49,0.28,0.10]* u.electron/u.ph, name='QE'))
                self.max_fps = 77/u.s
                self.max_gain_dB = 53*u.dB
                self.max_nbbit = 12
                self.dark_noise = 2.14*u.electron
                self.saturation_capacity = 10.5E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
            if self.name=='33UX249':
                self.sensor_name = 'IMX249LLJ'
                self.sensor_physz = [11.3,7.1]  *u.mm
                self.sensor_pixnum = [1920,1200]*u.pix
                self.sensor_pixsz = [5.86,5.86] *u.um
                # from: https://www.ximea.com/downloads/usb3/manuals/xic_technical_manual.pdf
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 30./u.s
                self.max_gain_dB = 24*u.dB
                self.max_nbbit = 12
                self.dark_noise = 6.6*u.electron
                self.saturation_capacity = 32.7E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
        # Sony cameras
        elif self.brand=='Sony':
            # Sony alpha7s
            if self.name=='alpha7s':
                self.sensor_name = 'Exmor'
                #self.sensor_physz = [35.6,23.8]  *u.mm
                self.sensor_physz = [41.850,20.40]  *u.mm
                self.sensor_pixnum = [1920,1080]*u.pix # video format used by Spanish team for GEM2016 see Ocana et al. 2018
                self.sensor_pixsz = [8.40,8.40] *u.um
                # TODO: TO CHANGE all values below
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 164/u.s # TO CHANGE!!!!
                self.max_gain_dB = 24*u.dB # TO CHANGE!!!!
                self.max_nbbit = 12 # TO CHANGE!!!!
                self.dark_noise = 6.6*u.electron # TO CHANGE!!!!
                self.saturation_capacity = 32.7E+03*u.electron # TO CHANGE!!!!
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
        # Meteorix cameras
        elif self.brand=='3D+':
            # Sony alpha7s
            if self.name=='3DCM734':
                self.sensor_name = 'CMV2000-2E5M1pp'
                self.sensor_physz = [5.5,5.5]  *u.mm
                self.sensor_pixnum = [2048,20480]*u.pix 
                self.sensor_pixsz = [11.264,11.264] *u.um
                # TODO: TO CHANGE all values below
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 70.0/u.s
                self.max_gain_dB = 60*u.dB # TO CHANGE!!!!
                self.max_nbbit = 12
                self.dark_noise = 13.0*u.electron 
                self.saturation_capacity = 13.5E+03*u.electron
                self.gain_law_analog2dB = analog2dB_laws[self.__name] # TODO:  TO CHANGE!!!
                self.gain_law_dB2analog = dB2analog_laws[self.__name] # TODO:  TO CHANGE!!!
                self.set_gain(0*u.dB)
                self.known = True
        # TEST cameras
        elif self.brand=='TEST':
            # Sony alpha7s
            if self.name=='test':
                self.sensor_name = 'test'
                #self.sensor_physz = [1.050,7.0]  *u.mm
                self.sensor_physz = [0.315,12.0]  *u.mm
                self.sensor_pixnum = [1920,1080]*u.pix # video format used by Spanish team for GEM2016 see Ocana et al. 2018
                self.sensor_pixsz = [8.40,8.40] *u.um
                # TODO: TO CHANGE all values below
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 164/u.s # TO CHANGE!!!!
                self.max_gain_dB = 24*u.dB # TO CHANGE!!!!
                self.max_nbbit = 12 # TO CHANGE!!!!
                self.dark_noise = 6.6*u.electron # TO CHANGE!!!!
                self.saturation_capacity = 32.7E+03*u.electron # TO CHANGE!!!!
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
        # ZWO cameras
        elif self.brand=='ZWO':
            # Sony alpha7s
            if self.name=='ASI6200':
                self.sensor_name = 'IMX455'
                self.sensor_physz = [40.961,31.108]  *u.mm
                self.sensor_pixnum = [9602,6498]*u.pix
                self.sensor_pixsz = [3.76,3.76] *u.um
                self.QE.add_column(Column([0.50,0.90,0.82,0.61,0.30,0.0 ,0.0 ,0.0 ,0.88,0.55,0.30,0.12]* u.electron/u.ph, name='QE'))
                self.max_fps = 25./u.s 
                self.max_gain_dB = 83*u.dB 
                self.max_nbbit = 16 
                self.dark_noise = 0.1*u.electron 
                self.saturation_capacity = 47.7E+03*u.electron 
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
        elif self.brand=='Human':
            # Test Human eye
            if self.name=='eye':
                self.sensor_name = 'retina' # fov. 200x135 deg https://en.wikipedia.org/wiki/Human_eye
                #self.sensor_physz = [1.050,7.0]  *u.mm#  https://en.wikipedia.org/wiki/File:Human_photoreceptor_distribution.svg
                self.sensor_physz = [200.0,110.0]  *u.mm
                self.sensor_pixnum = [10000,10000]*u.pix # video format used by Spanish team for GEM2016 see Ocana et al. 2018
                self.sensor_pixsz = [6.60,6.60] *u.um
                self.QE.add_column(Column([0.40,0.71,0.70,0.55,0.23,0.0 ,0.0 ,0.0 ,0.76,0.45,0.23,0.08]* u.electron/u.ph, name='QE'))
                self.max_fps = 25./u.s # TO CHANGE!!!!
                self.max_gain_dB = 64*u.dB # TO CHANGE!!!!
                self.max_nbbit = 16 # TO CHANGE!!!!
                self.dark_noise = 0.1*u.electron # TO CHANGE!!!!
                self.saturation_capacity = 32.7E+05*u.electron # TO CHANGE!!!!
                self.gain_law_analog2dB = analog2dB_laws[self.__name]
                self.gain_law_dB2analog = dB2analog_laws[self.__name]
                self.set_gain(0*u.dB)
                self.known = True
        return self.known
    
    
    def info(self):
        log.info('---- Camera info --------')
        log.info('Brand         '+self.__brand)
        log.info('Name          '+self.__name)
        log.info('sensor_physz  '+str(self.__sensor_physz))
        log.info('xphysz        '+str(self.__xphysz.to('mm')))
        log.info('yphysz        '+str(self.__yphysz.to('mm')))
        log.info('sensor_pixnum '+str(self.__sensor_pixnum))
        #log.info('Bands         '+str(self.__QE['bands']))
        #log.info('QE            '+str(self.__QE['QE']))
        log.info('QE            '+str(self.__QE))
        log.info('xpixnum       '+str(self.__xpixnum))
        log.info('ypixnum       '+str(self.__ypixnum))
        log.info('xpixsz        '+str(self.__xpixsz.to('um')))
        log.info('ypixsz        '+str(self.__ypixsz.to('um')))
        log.info('max_gain_dB   '+str(self.__max_gain_dB))
        log.info('gain_dB       '+str(self.__gain_dB))
        log.info('analogGain    '+str(self.__analogGain))
        log.info('dark_noise    '+str(self.__dark_noise))
        log.info('saturation    '+str(self.__saturation_capacity))
        log.info('known         '+str(self.__known))
        if self.lens:
            self.lens.info()
            log.info('fov    '+str(self.fov))
        if self.sys_gain is not None:
            log.info('nbbit         '+str(self.__nbbit))
            log.info('system gain   '+str(self.__sys_gain))
        
        return
    
    def compute_mean_QE(self,list_bands):
        """Compute mean QE over all bands
        
        Paramters
        ---------
        list_bands : list of strings
            List of bands to run the average.
        
        Returns
        -------
        mean_QE :  astropy.units.Quantity Object
            mean QE over the considered bands.
        
        """
        # REMOVE THE FOLLOWING LINE
        mean_QE = 0.0
        #for band in list_bands:
            # retrive band data
            
            # compute QE for the band
        
        
        return mean_QE
    
    
