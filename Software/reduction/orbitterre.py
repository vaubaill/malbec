#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 21:01:41 2021

@author: vaubaill
"""

# illustrate the orbit of the Earth vs perfect circle

import numpy as np
import spiceypy as sp
import matplotlib.pyplot as plt
import astropy.units as u

# import spice routine
sp.furnsh('/astrodata/kernels/standard.ker')

# number of days per year
ndpy = 370.0  * sp.spd()

step_day = 1.0
step_time = step_day * sp.spd()
allet= np.arange(0.0,ndpy,step_time)

xe=np.array([])
ye=np.array([])
xo=np.array([])
yo=np.array([])
# loop over time
for et in allet:
    # get location of Earth
    (xyz,lt) = sp.spkezr('EARTH', et, 'ECLIPJ2000', 'NONE', 'SUN')
    x = (xyz[0]*u.km).to('AU')
    y = (xyz[1]*u.km).to('AU')
    z = (xyz[2]*u.km).to('AU')
    xe = np.append(xe,x.value)
    ye = np.append(ye,y.value)
    (r,lon,lat) = sp.reclat([x.value,y.value,z.value])
    #print(r)
    #print(x,y,z)
    #print(lon,lat,r)
    xyzo = sp.latrec(1.0,lon,0.0)
    #print(xyzo)
    xo = np.append(xo,xyzo[0])
    yo = np.append(yo,xyzo[1])
    #print(xo,yo)

fig=plt.figure()
plt.plot(xe,ye,'k-',label='Earth')
plt.plot(xo,yo,'k--',label='Circle')
plt.xlim([-1.10,1.10])
plt.ylim([-1.10,1.10])
plt.axis('equal')
plt.legend(loc='upper right')
outfile = 'EarthCircle.png'
plt.savefig(outfile,dpi=600)
plt.close(fig)

print('done')