#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 09:13:23 2024

@author: vaubaill
"""

# Create MoMet instrument

import os
import glob
import numpy as np
import astropy.units as u
import logging
import spiceypy as sp
import argparse

from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import EarthLocation
from configparser import ConfigParser, ExtendedInterpolation

from obsplanner.obsplan import ObservationPlanner,log
from showersim.meteorshower import MeteorShower
from showersim.shower_radiant import Shower_Radiant
from reduction.filter import Filter
from reduction.lens import Lens
from reduction.camera import Camera

# define all cameras and lenses for each MoMet suitcase
momet_names = ['MOMET1','MOMET2','MOMET3']

def make_cameras_momet1():
    """Make Camera objects for MoMet1.
    
    Parameters
    ----------
    None.

    Returns
    -------
    cameras : tuple of reduction.camera.Camera object
        Tuple of Cameras in the MoMet1 suitcase.

    """
    cameras = {'RNAC' : Camera(brand='DMK',name='33UX249',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'LNAC' : Camera(brand='DMK',name='33UX249',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'SPEC' : Camera(brand='DMK',name='33UX249',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'RWAC' : Camera(brand='DMK',name='33UX249',
                               lens=Lens(brand='Basler',name='6mm-1.4')),
               'LWAC' : Camera(brand='DMK',name='33UX249',
                               lens=Lens(brand='Basler',name='6mm-1.4'))}
    return cameras
def make_cameras_momet2():
    """Make Camera objects for MoMet2.
    
    Parameters
    ----------
    None.

    Returns
    -------
    cameras : Ttuple of reduction.camera.Camera object
        Tuple of Cameras in the MoMet1 suitcase.

    """
    cameras = {'RNAC' : Camera(brand='Basler',name='acA1920-155um',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'LNAC' : Camera(brand='Basler',name='acA1920-155um',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'SPEC' : Camera(brand='Basler',name='acA1920-155um',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'RWAC' : Camera(brand='Basler',name='acA1920-155um',
                               lens=Lens(brand='Basler',name='6mm-1.8')),
               'LWAC' : Camera(brand='Basler',name='acA1920-155um',
                               lens=Lens(brand='Basler',name='6mm-1.8'))}
    return cameras
def make_cameras_momet3():
    """Make Camera objects for MoMet3.
    
    Parameters
    ----------
    None.

    Returns
    -------
    cameras : tuple of reduction.camera.Camera object
        Tuple of Cameras in the MoMet1 suitcase.

    """
    cameras = {'RNAC' : Camera(brand='Basler',name='acA2040-55um',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'LNAC' : Camera(brand='Basler',name='acA2040-55um',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'SPEC' : Camera(brand='Basler',name='acA2040-55um',
                               lens=Lens(brand='Computar',name='12.5mm-1.4')),
               'RWAC' : Camera(brand='Basler',name='acA2040-55um',
                               lens=Lens(brand='Basler',name='6mm-1.4')),
               'LWAC' : Camera(brand='Basler',name='acA2040-55um',
                               lens=Lens(brand='Basler',name='6mm-1.4'))}
    return cameras

def make_allcameras():
    """Make all MoMet camer objects.

    Returns
    -------
    allcam : TYPE
        DESCRIPTION.

    """
    cam1 = make_cameras_momet1()
    cam2 = make_cameras_momet2()
    cam3 = make_cameras_momet3()
    allcam = {'MOMET1' : cam1,
              'MOMET2' : cam2,
              'MOMET3' : cam3}
    return allcam

class MoMet(object):
    """MoMet instrument.
    
    """
    def __init__(self,name,datapth,obsplan,config_file):
       """Initializes a Fakeor object.
       
       Parameters
       ----------
       name : str
           name of the Fakeor objects. Default is ''.
       datapth : str
            Full path to observation data including data from the 5 cameras.
       config_file : str
           ObsPlan configuration file.
       obsplan : obsplanner.ObsrevationPlanner Object
           ObsPlan object that includes the configuration file.
       
       Returns
       -------
       None.
       
       """
       self.name=name.upper()
       self.datapth = datapth
       self.obsplan = obsplan
       self.config_file = config_file
       # check name
       self.check_name()
       # make cameras
       self.make_cameras()       
       # load configuration file
       self.obsplan.load(self.config_file)
       return
    
    def check_name(self):
        """Check if MoMet name exists or not.
        
        Returns
        -------
        None.
        
        """
        if not self.name in momet_names:
            msg = '*** FATAL ERROR: MoMet name: '+self.name+' is unknown' +\
                'possible names are: '+str(momet_names)
            log.error(msg)
            raise ValueError(msg)
        return
     
    def make_cameras(self):
        """
        
        Returns
        -------
        None.

        """
        allcams = make_allcameras()
        self.cameras = allcams[self.name]
        for cam in self.cameras:
            self.cameras[cam].compute_fov()
            self.cameras[cam].info()
        return
        
         

if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='momet arguments.')
    parser.add_argument('-n', help='MoMet Object name')
    parser.add_argument('-p', help='MoMet data path')
    parser.add_argument('-c',default='./conf/config.in',help='configuration file. Default is: ./conf/config.in')
    args = parser.parse_args()
    
    # retrieve argumanets
    # set MoMet name
    name = args.n
    # set MoMet data path
    datapth = args.p
    # set configuration file
    config_file = args.conf
    
    # create ObservationPlanner Object
    op = ObservationPlanner()
    
    # create MoMet object
    mmt = MoMet(name, datapth, op, config_file)
else:
    log.debug('successfully imported')