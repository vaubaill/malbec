#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 09:43:55 2024

@author: vaubaill
"""

# reorganize data for MoMet trajectory calculation

import os
import shutil
import glob
from configparser import ConfigParser, ExtendedInterpolation
import logging

# needed files for traejectory computation
arxivdir = 'ArchivedFiles'
confile = '.config'
ftpfile_root = 'FTPdetectinfo_'
platefile = 'platepar_cmn2010.cal'
platerecailbfile = 'platepars_all_recalibrated.json'

pthi = '/media/sf_Volumes/Transcend/2023GEM/'
ptho = '/media/sf_Volumes/Expansion/GEM2023/'


# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

listmomet = glob.glob(pthi+'/MoMet*')
listmomet.sort()
for momet in listmomet:
    log.info('======= MoMet : '+momet)
    listcam = glob.glob(momet+'/CAM-*/')
    listcam.sort()
    for cam in listcam:
        log.info('=== cam : '+ cam)
        # copy the ArchivedFiles directory
        src_dir = cam + '/'+ arxivdir
        dst_dir = cam.replace(pthi,ptho)
        if not os.path.exists(src_dir):
            log.warning(src_dir + 'Does NOT exist: check original data!!!')
            continue
        log.info('now copying '+ src_dir + ' into ' + dst_dir)
        shutil.copytree(src_dir, dst_dir, dirs_exist_ok=True)
        # read the config file to get the station code
#        camconf = 
#        config = ConfigParser(interpolation=ExtendedInterpolation())
#        config.read(camconf)
#        stationID = config['System']['stationID']
    log.info('Cam '+cam+' done')
log.info('done')      