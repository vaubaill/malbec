"""Goal: read results of camera tests


"""

import os
import glob
import logging
import astropy.units as u
from astropy.table import QTable,Table,unique
import astropy.io.ascii as ascii
import numpy as np
import matplotlib.pyplot as plt


# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# data path
data_path = os.getenv('HOME')+'/malbec/DATA/TESTS_CAMERAS/'

# make list of all data files:
data_list = glob.glob(data_path+'*/data.dat')

for data_file in data_list:
    log.info('reading: '+data_file)
    data = QTable(masked=True).read(data_file,format='ascii.fixed_width_two_line')
    
    # get list of all gain
    all_gain = unique(data,keys=['gain'])['gain']
    all_exp = unique(data,keys=['exp'])['exp']
    all_bck = unique(data,keys=['bck'])['bck']
    
    # get camera and lens name from path
    camera_lens = os.path.dirname(data_file).split('/')[-1]
    
    # make plot curve for each gain
    figfile = data_file.replace('.dat','.png')
    fig = plt.figure()
    plt.title(camera_lens)
    plt.ylabel('Background [ADU]')
    plt.xlabel('exp time [s]')
    #plt.xlim((0,np.max(data['exp'])))
    #plt.ylim((0,np.max(data['bck'])))
    plt.xlim((0,0.1))
    plt.ylim((0,255))
    for gain in all_gain:
        log.info('ploting for gain: '+str(gain))
        # select only interesing data
        mask_gain = np.asarray(data['gain']==gain)
        loc_gain = mask_gain.nonzero()[0]
        median_bck = np.array([])
        median_max = np.array([])
        median_min = np.array([])
        for exp in all_exp:
            mask_exp = np.asarray(data['exp'][loc_gain]==exp)
            loc_exp = mask_exp.nonzero()[0]
            median_bck = np.append(median_bck,np.median(data['bck'][loc_gain[loc_exp]]))
            median_max = np.append(median_max,np.median(data['max'][loc_gain[loc_exp]]))
            median_min = np.append(median_min,np.median(data['min'][loc_gain[loc_exp]]))
        #log.debug('len(loc): '+str(len(loc)))
        if gain==all_gain[0]:
            plt.plot(all_exp,median_bck,'ro-', label='BCK') # 'r+'
            plt.plot(all_exp,median_max,'bo-', label='MAX') # 'r+'
        else:
            plt.plot(all_exp,median_bck,'ro-')
            plt.plot(all_exp,median_max,'bo-')
        plt.text(all_exp[-1], median_bck[-1], 'g='+str(gain), fontdict={'color': 'red'})
        plt.text(all_exp[-1], median_max[-1], 'g='+str(gain), fontdict={'color': 'blue'})
    plt.legend()
    plt.savefig(figfile)
    plt.close(fig)
    log.info('Figure saved in '+figfile)
    
log.info('Done')
    
