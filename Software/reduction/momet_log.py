#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 15:58:40 2025

@author: vaubaill
"""
import logging

# --------------------------------------------------------------------------------
# create default logger object
# --------------------------------------------------------------------------------
level = logging.INFO
log = logging.getLogger(__name__)
log.setLevel(level)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(level)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
fdlr = logging.FileHandler('momet.log')
fdlr.setLevel(level)
fdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)
log.addHandler(fdlr)