import glob, os
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
from photutils import datasets, DAOStarFinder, CircularAperture,IRAFStarFinder
from photutils import Background2D, SigmaClip, MedianBackground
from PIL import Image,ImageStat
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable,Table
from astropy import units as u
import astropy.io.ascii as ascii
from astropy.stats import sigma_clipped_stats
import subprocess

import malbec_tools


# goal: reduce images taken with malbec camera

# initializations
R_earth         = (1.0*u.R_earth).to('km')

# user part:
malbec_dir      = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/'
malbec_confdir  = malbec_dir + 'conf/'
malbec_data_dir = malbec_dir + 'DATA/'
data_dir        = malbec_data_dir + '201806/'        # to change if needed
campaign_dir    = malbec_data_dir + '201806/20180626/' # campaign data path
mask_file       = campaign_dir + 'mask.fit'
loc_file        = data_dir + 'GPS/AS2018062612_1.cor'  # GPS location file
camera_lamba    = 6.0E-07 *u.m              # center wavelength observation
Lmake_offset=False                  # make master OFFSET
Lloc=False                          # consider the GPS location file to derive the long,lat,alt

# make and set master offset fits file
offset_dir          = malbec_data_dir + 'OFFSET_NIR_FREEZER/'
master_offset_file  = 'master_offset.fit'
if Lmake_offset:
    malbec_tools.malbec_mkimgaverage(imgdir=offset_dir,
                                     img_root_name='offset_',
                                     img_ext_name='.fit',
                                     master_name=master_offset_file,
                                     master_dir=offset_dir)
print('Read offset file: '+offset_dir+master_offset_file)
hdu_offset      = fits.open(offset_dir+master_offset_file)[0]	# open file
offset          = hdu_offset.data

# read mask file if exists
if os.path.exists(mask_file):
    hdu_mask = fits.open(mask_file)[0]
    data_mask = hdu_mask.data
else:
    data_mask = 1.0

# determine day of observation thanks to data_dir name
datestr         = campaign_dir.split('/')[-2]
print 'campaign_dir=',campaign_dir
print 'datestr=',datestr
time_loc_start  = Time(datestr[0:4]+'-'+datestr[4:6]+'-'+datestr[6:8]+'T00:00:00')


# first read localisation data file
print('now reading file '+loc_file)
data_loc                    = QTable.read(loc_file,format='ascii')
data_loc['Time'].unit       ='s'
data_loc['Altitude'].unit   ='m'
data_loc['Latitude'].unit   ='deg'
data_loc['Longitude'].unit  ='deg'
data_loc['Press'].unit      ='Pa'
data_loc['T'].unit          ='deg_C'
time_loc                    = time_loc_start+data_loc['Time']

Lplot=True
if Lplot:
    # plot altitude vs time
    Alt_plot_file   = campaign_dir+'AltT.png'
    fig=plt.figure()
    plt.plot(data_loc['Time'].to('h').value,data_loc['Altitude'].to('km').value,'bo',linewidth=1.5)
    plt.title('Altitude vs Time')
    plt.xlabel('Time [hr after 0:00 UT]')
    plt.ylabel('Altitude [km]')
    plt.savefig(Alt_plot_file)
    plt.close(fig)
    print('Altitude curve saved in '+Alt_plot_file)
    # plot Time vs Temp
    Temp_plot_file	= campaign_dir+'TempT.png'
    fig=plt.figure()
    plt.plot(data_loc['Time'].to('h').value,data_loc['T'].to('deg_C').value,'bo',linewidth=1.5)
    plt.title('Temp vs Time')
    plt.xlabel('Time [hr after 0:00 UT]')
    plt.ylabel('Temp [C]')
    plt.savefig(Temp_plot_file)
    plt.close(fig)
    print('Temperature curve saved in '+Temp_plot_file)
    # plot Altitude vs Temp
    AltTemp_plot_file = campaign_dir+'AltTemp.png'
    fig=plt.figure()
    plt.plot(data_loc['Altitude'].to('km').value,data_loc['T'].to('deg_C').value,'bo',linewidth=1.5)
    plt.title('Altitude vs Temperature')
    plt.xlabel('Altitude [km]')
    plt.ylabel('Temp [C]')
    plt.savefig(AltTemp_plot_file)
    plt.close(fig)
    print('Altitude vs Temperature curve saved in '+AltTemp_plot_file)


# set up output file names
all_bck     = np.array([])
all_alt     = np.array([])
all_exp     = np.array([])
all_gain    = np.array([])
all_sunel   = np.array([])

# now loops over the images
#list11=glob.glob(campaign_dir+'*/20180626T11*.fit')
#list12=glob.glob(campaign_dir+'*/20180626T12*.fit')
#listimg=list11+list12
listimg=glob.glob(campaign_dir+'*/20180626T10*.fit')
#listimg=glob.glob(campaign_dir+'*/20180626T10220*.fit')
listimg.sort()
stardata = Table([['20000101T101010_010_UT.fit'],[0]],names=['file','NumStar'])

for imgfile in listimg:                         # get list of image files
    print ('======= Now treating image: '_os.path.basename(imgfile))  
    hdu = fits.open(imgfile)[0]          # open file
    time_img = Time(hdu.header['DATE-OBS'])   # retrieve img time
    npixX = hdu.header['NAXIS1']
    npixY = hdu.header['NAXIS2']
    if Lloc:
        sec_img = time_img - time_loc_start      # compute seconds since 00:00:00 UT
        expo = float(hdu.header['EXPTIME']) * u.s
        gain = float(hdu.header['GAIN']) * u.electron/u.adu
        # interpolate location data to know where the image was taken
        Alt        =           np.interp(sec_img.to('s').value,data_loc['Time'].to('s').value,data_loc['Altitude'].to('km').value) * u.km # [deg]
        lat        =  Latitude(np.interp(sec_img.to('s').value,data_loc['Time'].to('s').value,data_loc['Latitude'].to('deg').value),unit='deg')   # [deg]
        lon        = Longitude(np.interp(sec_img.to('s').value,data_loc['Time'].to('s').value,data_loc['Longitude'].to('deg').value),unit='deg')  # [deg]
        Press      =           np.interp(sec_img.to('s').value,data_loc['Time'].to('s').value,data_loc['Press'].to('Pa').value) * u.Pa    # [deg]
        Temp       =           np.interp(sec_img.to('s').value,data_loc['Time'].to('s').value,data_loc['T'].to('deg_C').value) * u.deg_C    # [deg]
        # compute location of Sun at the time of the exposure
        beta       = np.arccos(R_earth/(R_earth+Alt))
        site       = EarthLocation(lat=lat, lon=lon, height=Alt)
        altazframe = AltAz(obstime=time_img, location=site,pressure=Press,temperature=Temp,obswl=camera_lamba)
        sunaltazs  = get_sun(time_img).transform_to(altazframe)
        sunelevation   = sunaltazs.alt+beta            # correct for altitude
        # gather usefull data
        all_bck    = np.append(all_bck,median)
        all_alt    = np.append(all_alt,Alt.to('km').value)
        all_sunel  = np.append(all_sunel,sunelevation.to('deg').value)
        all_exp    = np.append(all_exp,expo)
        all_gain   = np.append(all_gain,gain)
        print ('Time='+time_img.iso+' Alt='+str(Alt.to('km'))+' T='+str(Temp.to('deg_C'))+' Sun_el='+str(sunelevation.to('deg').value)+' bck='+str(median)+' exp='+str(expo.to('s').value))
    
    
    # performs basic stats
    data = hdu.data
    # multiply by the mask, remove the offset and the dark
    data_new   = (data - offset) * data_mask
    print ('computing basic stats...')
    median = np.median(data_new)
    std = np.std(data_new)
    mean = np.mean(data_new)
    #mean, median, std  = sigma_clipped_stats(data_new, sigma=3.0)      # change paramters?
    if (mean==median):
       print ('saturated image '+imgfile)
       continue
    # set star find parameters
    fwhm = 2.0
    threshold = 2.*std
    daofind        = DAOStarFinder(fwhm=fwhm, threshold=2.*std)#,exclude_border=True)
    irafind        = IRAFStarFinder(fwhm=fwhm, threshold=2.*std)# ,exclude_border=True)
    #sources        = daofind(data_new - median)
    #print 'now finding stars with DAO'
    daosources  = daofind.find_stars(data_new)
    #print 'now finding stars with IRAF'
    irasources  = irafind.find_stars(data_new)
    daonbstar         = len(daosources)
    iranbstar         = len(irasources)
    print (str(daonbstar)+' stars found with DAO '+str(iranbstar)+' found with IRAF')
    stardata.add_row([os.path.basename(imgfile),daonbstar])
    # save stars position in file
    if not (daonbstar==0):
        daofile = imgfile.replace('.fit','-dao.dat')
        daopng  = daofile.replace('.dat','.png')
        ascii.write(daosources,daofile,format='fixed_width',delimiter=' ',overwrite=True)
        print ('star positions saved in '+daofile)
        plot = plt.figure()
        plt.xlim(0,npixX)
        plt.ylim(0,npixY)
        plt.plot(daosources['xcentroid'],daosources['ycentroid'],'b+')
        plt.savefig(daopng)
        plt.close(plot)
        print 'plot saved in ',daopng
    if not (iranbstar==0):
        irafile = imgfile.replace('.fit','-irf.dat')
        irapng  = irafile.replace('.dat','.png')
        ascii.write(irasources,irafile,format='fixed_width',delimiter=' ',overwrite=True)
        print 'star positions saved in '+irafile
        plot = plt.figure()
        plt.xlim(0,npixX)
        plt.ylim(0,npixY)
        plt.plot(irasources['xcentroid'],irasources['ycentroid'],'b+')
        plt.savefig(irapng)
        plt.close(plot)
        print 'plot saved in ',irapng
    # now try with SExtractor
    sexdir = campaign_dir+'SEXRES/'
    if not os.path.isdir(sexdir):
        print 'now making directory '+sexdir
        os.mkdir(sexdir)
    bck_file = sexdir + os.path.basename(imgfile).replace('.fit','-bkg.fits')
    obj_file = sexdir + os.path.basename(imgfile).replace('.fit','-obj.fits')
    cat_file = sexdir + os.path.basename(imgfile).replace('.fit','.cat')
    cat = '-CATALOG_NAME '+cat_file
    conf = ' -c ' + malbec_confdir + 'malbec.conf '
    param = ' -PARAMETERS_NAME '+malbec_confdir + 'malbec.param '
    filtr = ' -FILTER_NAME '+malbec_confdir + 'default.conv '
    check = ' -CHECKIMAGE_TYPE  -BACKGROUND,OBJECTS -CHECKIMAGE_NAME '+bck_file+','+obj_file
    stnnw = ' -STARNNW_NAME '+malbec_confdir + 'default.nnw '
    cmd = '/usr/bin/sextractor '+imgfile+conf+param+filtr+check+stnnw
    print 'now submitting command: '+cmd
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    print 'out=',out
    print 'err=',err
    
    
    """
    if (nbstar>0):
     positions     = (sources['xcentroid'], sources['ycentroid'])
     apertures     = CircularAperture(positions, r=4.)
     norm          = ImageNormalize(stretch=SqrtStretch())
     plt.imshow(data, cmap='Greys', origin='lower', norm=norm)
     apertures.plot(color='blue', lw=1.5, alpha=0.5)
     #plt.show(block=False) 
     plt.show() 
    """
    
    """
    # estimate the background with photutils
    # see: https://photutils.readthedocs.io/en/stable/photutils/background.html
    sigma_clip = SigmaClip(sigma=3., iters=10)
    bkg_estimator  = MedianBackground()
    bkg        = Background2D(data2, (50, 50), filter_size=(3, 3),sigma_clip=sigma_clip, bkg_estimator=bkg_estimator)
    print 'photutil.bck median=',bkg.background_median,' photutil.bck rms_median=',bkg.background_rms_median
    # plot the background image
    plt.imshow(bkg.background, norm=norm, origin='lower',cmap='Greys_r')
    plt.show()
    # now plot the background corrected image
    plt.imshow(data - bkg.background, norm=norm, origin='lower',cmap='Greys_r')
    plt.show() 
    """
    
    if Lloc:
        # update the img file header
        hdu.header['SITELAT']  = (lat.to_string(unit=u.degree, sep=':'),'Latitude of Observatory, in deg')
        hdu.header['SITELON']  = (lon.to_string(unit=u.degree, sep=':'),'Longitude of Observatory, in deg')
        hdu.header['SITEALT']  = (Alt.to('m').value,'Altitude of Observatory, in m')
        hdu.header['TEMP'] = (Temp.to('deg_C').value,'Outside Temperature in C')
        hdu.header['PRESS']    = (Press.to('Pa').value,'Outside Pressure in Pa')
        hdu.header['COMMENT']  = ('Sun elevation: '+str(sunelevation.to('deg').value)+' deg')
        hdu.header['COMMENT']  = ('Beta: '+str(beta.to('deg').value)+' deg')
        hdu.header['COMMENT']  = ('Header updated by the malbec_reduce.py script')
    
    """
    # save the reduced image in new file
    hdu.data = data_new
    imgfile_new = os.path.splitext(imgfile)[0] + '-c.fits'
    hdu.writeto(imgfile_new,overwrite=True)
    print prog,'Reduced image saved in ',imgfile_new
    """
    # END of loop over fit images
    
# save the number of stars
filestar = campaign_dir+'Star.dat'
stardata.remove_row(0)
ascii.write(stardata,filestar,format='fixed_width',delimiter=' ')
print ('Star data saved in '+filestar)

if Lloc:
    # save table output
    outfile = campaign_dir+'ALLDATA.dat'
    outable = QTable([all_alt,
                    all_sunel,
                    all_bck,
                    all_exp,
                    all_gain,
                    all_numstar,
                    ],
                    names=('# Alt (km)','Sun_el (deg)','BCK (ADU)','exp (sec)','gain (e/adu)','numstar'))
    out	= open(outfile, "w")
    ascii.write(outable,out,format='fixed_width',delimiter=' ')
    out.close
    print ('data saved in '+outfile)
print prog,'done'
