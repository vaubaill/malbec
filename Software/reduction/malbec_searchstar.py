import glob, os
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
from photutils import datasets, DAOStarFinder, CircularAperture,IRAFStarFinder
from photutils import Background2D, SigmaClip, MedianBackground
from PIL import Image,ImageStat
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable,Table
from astropy import units as u
import astropy.io.ascii as ascii
from astropy.stats import sigma_clipped_stats
import subprocess

import malbec_tools

import fripipe.trajectory.definitions as df
from fripipe.astrometry.localsky import LocalSky
from fripipe.astrometry.camera import Camera
from fripipe import path_and_file as pthf

camera=Camera(ccd_size_x=1282*u.pixel,ccd_size_y=1026*u.pixel,opt_center=((1282/2)*u.pixel,(1026/2)*u.pixel),px_size=0.0056*u.mm,img_sz=0,gain=0,f=12.0*u.mm,k1=1.5,k2=2.5,rotation=0*u.rad,tilt=0*u.rad,ahead='malbec.ahead')
station = df.Station()
station.putname('AireSurlAdour')
station.put_fromgeodetic(-0.251585767842,43.7063092324,154.55)
time=Time('2018-06-26T12:07:51.0')
LimMag=3.50
maxnumstars=100
LocSky=LocalSky(station,camera=camera)
LocSky.set_catalog_file(LocSky.LimMag2catfilename(LimMag))
LocSky.set_ref_stars_cat(time,LimMag)
LocSky.limitstars(maxnumstars)
LocSky.ref_stars_cat.sort('Mag')

septhld=3.0*u.deg
istart=1
for (star1,mag1,altaz1) in zip(LocSky.skycoo,LocSky.ref_stars_cat['Mag'],LocSky.altaz):
    for (star2,mag2,altaz2) in zip(LocSky.skycoo[istart:],LocSky.ref_stars_cat['Mag'][istart:],LocSky.altaz[istart:]):
        sep=star1.separation(star2)
        if sep<septhld:
            print star1.ra.to_string(unit=u.hour),star1.dec.to_string(unit=u.deg),mag1,star1.get_constellation(),altaz1.az.to('deg').value,altaz1.alt.to('deg').value
            print star2.ra.to_string(unit=u.hour),star2.dec.to_string(unit=u.deg),mag2,star2.get_constellation(),altaz2.az.to('deg').value,altaz2.alt.to('deg').value
            print sep.to('deg').value
            print '####'
            #print('separation<'+septhld.to_string(unit=u.degree)+' found: '+star1.to_string()+' '+star2.to_string())
    istart=istart+1
