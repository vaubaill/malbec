#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 14:56:56 2022

@author: vaubaill
"""
import os
import shutil
import spiceypy as sp

import astropy.units as u

def make_atmosphere_kernel(start,stop,meteor_alt=100.0*u.km,freq=60.0*u.s,out_dir='./',tmp_dir='/tmp/',sim_id='alt2atm',spice_exe_path='/home/vaubaill/MISC-UTIL/NAIF/toolkit/exe/',kernel_path='/astrodata/kernels/'):
    """Make Atmopshere kernel.

    Parameters
    ----------
    start : float
        Start time of the Atmosphere object validity, in SPICE format.
    stop : float
        Stop time of the Atmosphere object validity, in SPICE format..
    meteor_alt : astropy.quantity.Quantity object, optional
        Altitude of the meteors. The default is 100.0*u.km.
    freq : astropy.quantity.Quantity object, optional
        time frequency at which the atmosphere is to be created. The default is 60.0*u.s.
    out_dir : string, optional
        Ouput directory. The default is './'.
    tmp_dir : string, optional
        Temporary directory where SPICE object will be created. The default is '/tmp/'.
    sim_id : string, optional
        Simulation identification string. The default is 'alt2atm'.
    spice_exe_path : string, optional
        PICE executable files directory. The default is '/home/vaubaill/MISC-UTIL/NAIF/toolkit/exe/brief/'.
    kernel_path : string, optional
        SPICE kernels directory. The default is '/astrodata/kernels/'.

    Raises
    ------
    IOError
        DESCRIPTION.

    Returns
    -------
    atmosphere_name : string
        Atmopshere SPICE name.
    atmosphere_id : int
        Atmopsher SPICE id.
    atmosphere_text_kernel : string
        Atmopshere SPICE text kernel file name.
    atmosphere_spk_file : string
        Atmosphere SPICE SPK file name.

    """
    # create output file name
    atmosphere_text_kernel = out_dir +'/'+ 'atmosphere.ker'
    atmosphere_spk_file = atmosphere_text_kernel.replace('.ker','.spk')
    # make SPICE object name and id
    atmosphere_name = 'ATMOSPHERE_ELLIPSOID'
    atmosphere_id = 1399001
    surface_name = 'ATMOSPHERE_SURFACE'
    surface_id = 1399002
    atmosphere_frame_name = 'ATM_FRAME'
    atmosphere_frame_id = 1399003
    # get Earth radii
    earth_radii = sp.bodvrd( "EARTH", "RADII", 3)[1]*u.km
    # set atmosphere radii
    atm_radii = earth_radii + meteor_alt
    
    # make temporary directory
    sim_tmp_dir = tmp_dir + '/' + sim_id + '/'
    if not os.path.isdir(sim_tmp_dir):
        os.makedirs(sim_tmp_dir)
        print('Temporary directory created: '+sim_tmp_dir)
    # set start time
    et = start - 10.0
    # creates the ephemeris data file
    names=['EPOCH','X','Y','Z','VX','VY','VZ']
    # make intermediate spk_data file name
    data_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file.replace('.spk','_spk.dat'))
    # intermediate setup file name
    setup_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file.replace('.spk','.setup'))
    # intermediate output file
    tmpout_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file)
    print('spk_file: '+atmosphere_spk_file)
    print('data_file: '+data_file)
    print('setup_file: '+setup_file)
    print('tmpout_file: '+tmpout_file)
    with open(data_file,'w') as out:
        out.write("# "+' '.join(names)+" \n")
        while et < stop:
            out.write(sp.et2utc(et,'ISOC',2,50).replace('T',' ')+"\n")
            out.write('0.000000000 0.000000000 0.000000000 -0.000000000 -0.000000000 -0.000000000\n')
            et = et + freq.to('s').value
        et = stop + 10.0
        out.write(sp.et2utc(et,'ISOC',2,50).replace('T',' ')+"\n")
        out.write('0000.000000000 0.000000000 0.000000000 -0.000000000 -0.000000000 -0.000000000\n')
    print('Atmosphere data saved in '+data_file)
    # make the intermediate setup file
    with open(setup_file,'w') as out:
        out.write("\\begindata \n")
        out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
        out.write("   OUTPUT_SPK_TYPE   = 13\n")
        out.write("   OBJECT_ID         = "+str(atmosphere_id)+"\n")
        out.write("   OBJECT_NAME       = "+repr(atmosphere_name)+"\n")
        out.write("   CENTER_ID         = 399\n")
        out.write("   REF_FRAME_NAME    = "+repr('ITRF93')+"\n")
        out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
        out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
        out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
        out.write("   LEAPSECONDS_FILE  = "+repr(kernel_path+'naif0012.tls')+"\n")
        out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
        out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
        out.write("   PCK_FILE          = "+repr(kernel_path+'pck00010.tpc')+"\n")
        out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
        out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
        out.write("   IGNORE_FIRST_LINE = 1 \n")
        out.write("   LINES_PER_RECORD  = 2 \n")
        out.write("   POLYNOM_DEGREE    = 3 \n")
        out.write("   SEGMENT_ID        = "+repr(str(atmosphere_id))+"\n")
        out.write(" \n")
    print("setup saved in: "+setup_file)
    
    # remove spk file if it already exists
    if os.path.exists(tmpout_file):
        os.remove(tmpout_file)
    # build the command
    cmd = spice_exe_path+"mkspk -setup "+setup_file
    print("cmd="+cmd)
    # launches the cmd
    try:
        os.system(cmd)
    except Exception:
        msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
        print(msg)
        raise IOError(msg)
    # double check that the SPK file was successfully created
    if not os.path.exists(tmpout_file):
        msg='*** FATAL ERROR: temporary SPK file: '+atmosphere_spk_file+' was not created.'
        print(msg)
        raise IOError(msg)
    # now copy temporary output file into spk_file
    outdir = os.path.dirname(atmosphere_spk_file)+'/'
    for file2move in [setup_file,data_file,tmpout_file]:
        dst = outdir+os.path.basename(file2move)
        if os.path.exists(dst):
            os.remove(dst)
        print('Moving file '+file2move+' into '+dst)
        try:
            # remark: os.remove does not always work when trying to move file from one FS to another...
            shutil.copy(file2move,dst)
        except:
            msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
            print(msg)
            raise IOError(msg)
    print("SPICE SPK kernel saved in: "+atmosphere_spk_file)
    # remove temporary directory
    try:
        shutil.rmtree(sim_tmp_dir)
        print('Temporary directory deleted: '+sim_tmp_dir)
    except:
        msg = '*** FATAL ERROR: Impossible to remove temporary directory: '+sim_tmp_dir
        print(msg)
        raise IOError(msg)
    
    # make text kernel
    with open(atmosphere_text_kernel,'w') as out:
        out.write("\\begintext \n\n")
        out.write('  Definition of the top of atmosphere at meteor altitude \n')
        out.write('  Author: J. Vaubaillon - IMCCE - 2020\n')
        out.write('  MALBEC project\n\n')
        out.write("\\begindata \n\n")
        out.write('   NAIF_BODY_NAME += ('+repr(atmosphere_name)+')\n')
        out.write('   NAIF_BODY_CODE += ( '+str(atmosphere_id)+ ')\n')
        out.write('   NAIF_BODY_CENTER = 399 \n')
        out.write('   NAIF_SURFACE_NAME += ( '+repr(surface_name)+' )\n')
        out.write('   NAIF_SURFACE_CODE += ( '+str(surface_id)+' )\n')
        out.write('   NAIF_SURFACE_BODY += ( '+str(atmosphere_id)+' )\n\n')
        out.write('   OBJECT_'+str(surface_id)+'_FRAME = '+repr('ITRF93')+'\n\n')
        out.write('   BODY'+str(atmosphere_id)+'_CENTER = 399\n\n')
        out.write('   BODY'+str(atmosphere_id)+'_RADII = ( '+str(atm_radii[0].to('km').value)+' , '+str(atm_radii[1].to('km').value)+' , '+str(atm_radii[2].to('km').value)+' )\n')
        out.write('   BODY'+str(atmosphere_id)+'_POLE_RA        = (    0.      -0.641         0. )\n')
        out.write('   BODY'+str(atmosphere_id)+'_POLE_DEC       = (   90.      -0.557         0. )\n')
        out.write('   BODY'+str(atmosphere_id)+'_PM             = (  190.147  360.9856235     0. )\n')
        out.write('   BODY'+str(atmosphere_id)+'_LONG_AXIS      = (    0.                        )\n\n')
        out.write('   FRAME_'+atmosphere_frame_name+' = '+str(atmosphere_frame_id)+'\n')
        out.write('   FRAME_'+str(atmosphere_frame_id)+'_NAME    =  '+repr(atmosphere_frame_name)+'\n')
        out.write('   FRAME_'+str(atmosphere_frame_id)+'_CLASS   =  4 \n')
        out.write('   FRAME_'+str(atmosphere_frame_id)+'_CLASS_ID = '+str(atmosphere_frame_id)+'\n')
        out.write('   FRAME_'+str(atmosphere_frame_id)+'_CENTER  = '+str(atmosphere_id)+'\n\n')
        out.write('   OBJECT_'+str(atmosphere_id)+'_FRAME = '+repr(atmosphere_frame_name)+'\n\n')
        out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_RELATIVE            =  '+repr('ITRF93')+'\n')
        out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_SPEC                =  '+repr('ANGLES')+'\n')
        out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_UNITS               =  '+repr('DEGREES')+'\n')
        out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_AXES                =  ( 3, 2, 3 )\n')
        out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_ANGLES              =  ( 0, 0, 0 )\n')
    # load kernels
    sp.furnsh(atmosphere_text_kernel)
    sp.furnsh(atmosphere_spk_file)
    print('atmosphere_text_kernel file loaded: '+atmosphere_text_kernel)
    print('atmosphere_spk_file file loaded: '+atmosphere_spk_file)
    return (atmosphere_name,atmosphere_id,atmosphere_text_kernel,atmosphere_spk_file)
