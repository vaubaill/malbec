#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 09:43:55 2024

@author: vaubaill
"""

# reorganize data for MoMet trajectory calculation

import os
import shutil
import glob
from configparser import ConfigParser, ExtendedInterpolation
import logging

# needed files for traejectory computation
arxivdir = 'ArchivedFiles'
confile = '.config'
ftpfile_root = 'FTPdetectinfo_'
platefile = 'platepar_cmn2010.cal'
platerecalibfile = 'platepars_all_recalibrated.json'

pthi = '/media/sf_Volumes/Expansion/GEM2023/'
ptho = '/media/sf_Volumes/Expansion/GEM2023/Traj/'


# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# create output directory
if not os.path.exists(ptho):
    log.info('Makeing output directory '+ptho)
    os.mkdir(ptho)

listcam = glob.glob(pthi+'/MoMet*/CAM-*')
listcam.sort()
for cam in listcam:
    log.info('=== cam : '+ cam)
    listnight = glob.glob(cam +'/'+arxivdir+ '/FR*/')
    if not len(listnight):
        msg = 'listnight empty for cam '+cam
        log.error(msg)
        raise IOError(msg)  
    for nightdir in listnight:
        log.info('processing : '+nightdir)
        # read the config file to get the station code
        nightID = nightdir.split('/')[-2]
        stnID = nightID.split('_')[0]
        conf = nightdir + confile
        ftp = nightdir + ftpfile_root + nightID+'.txt'
        plate = nightdir + platefile
        platere = nightdir + platerecalibfile
        for f in [conf,ftp,plate,platere]:
            if not os.path.exists(f):
                msg= f+' does not exist'
                log.error(msg)
                raise IOError(msg)
        log.info('All files present: ok')
        config = ConfigParser(interpolation=ExtendedInterpolation())
        config.read(conf)
        stationID = config['System']['stationID']
        log.info('StationID: '+stationID)
        # verification of the stationID
        if not (stationID==stnID):
            msg = 'StationID suspicious: '+stationID+' different from night ID: '+stnID
            log.error(msg)
            raise ValueError(msg)
        # making output directoies
        stndir = ptho + '/' + stationID + '/'
        nightdir_det = stndir + nightID + '_detected/'
        for d in     [stndir,nightdir_det]:
            if not os.path.exists(d):
                log.info('Making output directory '+d)
                os.mkdir(d)
        # copy useful files into output directory
        for f in [conf,ftp,plate,platere]:
            src = f
            dst = f.replace(nightdir,nightdir_det)
            log.info('copying ' + src +  ' into ' + dst)
            shutil.copyfile(src, dst)
        
    log.info('Cam '+cam+' done')
log.info('done')      