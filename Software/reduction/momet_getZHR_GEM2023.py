
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 30 14:20:48 2024

@author: vaubaill
"""

# récupération des temps des météores
import os
import re
import glob
import numpy as np
import logging
import matplotlib.pyplot as plt 
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import EarthLocation
from configparser import ConfigParser, ExtendedInterpolation

from showersim.meteorshower import MeteorShower
from showersim.shower_radiant import Shower_Radiant
from showersim.shower_utils import HR2ZHR,get_eye_area

from momet_log import log
from momet_readftp import read_rms_ftp_file 

# set home directory
home = os.getenv('HOME') + '/'

# input USER variables
# input data directory
pth = '/Volumes/Transcend/'
#pth = home + 'PROJECTS/PODET/PODET-MET/MALBEC/malbec/Software/reduction/tests/'

year = '2024'
time_max = Time(year+'-12-13T18:00:00',scale='utc')
# get midnight date
midnight_str = year+'-12-13T00:00:00.000'
midnight = Time(midnight_str,format='isot')

# Define the Geminids meteor shower
radiant = Shower_Radiant(ra=112.0*u.deg,
                         dec=+33.0*u.deg,
                         time_max=time_max,
                         dra=1.0*u.deg/u.d,
                         ddec=-0.17*u.deg/u.d)
shower = MeteorShower('Geminids',
                      35.0*u.km/u.s,
                      1.7,
                      radiant=radiant,
                      time_max=time_max,
                      ZHR=100.0/u.hr,
                      fwhm=24.0*u.hr,
                      leap_sec_ker='../conf/spice_kernels/naif0012.tls')

# set total number of meteor detected over all the MoMet suitcases
nmet_tot = 0
# get MoMet list
os.chdir(pth)
listMomet = glob.glob('MoMet*')
listMomet.sort()
log.debug(str(listMomet))
# loop over MoMet suitcases
for momet in listMomet:
    log.info('==============================')
    log.info('*** Now processing Momet: '+momet)
    os.chdir(pth+'/'+momet)
    listCAM = glob.glob('CAM*')
    listCAM.sort()
    log.debug(str(listCAM))
    for CAM in listCAM :
        log.debug('* now processing camera '+CAM)
        if not os.path.isdir(CAM):
            log.debug(CAM+' is not a directory: skipping...')
            continue
        log.info('= = = = = = = = = = = = =')
        log.info('*** Now processing camera: '+CAM)
        # camera directory
        camdir = pth+'/'+momet+'/'+CAM
        os.chdir(camdir)
        # output data and graph file names
        timefile = camdir+'alltimes.dat'
        HRplot = camdir+'HR-Time.png'
        ZHRplot = camdir+'ZHR-Time.png'
        ZHRfile = camdir+'ZHR.dat'
        # initialize alltimes data
        alltimes = []
        # get all FTP-type files
        listFTP = glob.glob('FR*/FTP*.txt')
        listFTP = [f for f in listFTP if not (f.endswith('_unfiltered.txt') or '_backup_' in f)]
        listFTP.sort()
        log.info('listFTP: '+str(listFTP))
        # loop over all ftp-type files
        for ftpfile in listFTP:
            log.debug('Opening ftp file: '+ftpfile)
            meteor_tables,meteor_timax = read_rms_ftp_file(ftpfile)
            # mise du Time dans la matrice alltimes
            for t in meteor_timax:
                alltimes.append(t)
        nmet = len(alltimes)
        log.info('number of meteors deteted: '+str(nmet))
        nmet_tot = nmet_tot + nmet
        if not nmet:
            log.warning('*** Warning: no meteor detected in cam: '+CAM)
            # go back to the MoMet directory to loop over next camera
            os.chdir(pth+'/'+momet)
            continue
        # save all detected meteor times in a text file
        with open(timefile,'w') as f:
            for times in alltimes:
                f.write(times.isot+'\n')
        log.info('Output time data saved in '+timefile)
        
        # read RMS configuration file to get camera FOV and location
        config = ConfigParser(interpolation=ExtendedInterpolation())
        confile = os.path.dirname(ftpfile)+'/.config'
        if not os.path.exists(confile):
            msg = '*** config file '+confile+' does not exist'
            log.error(msg)
            raise IOError(msg)
        # read configuration file
        config.read(confile)
        # set location of the observer
        lon = float(config['System']['longitude'].split(';')[0])*u.deg
        lat = float(config['System']['latitude'].split(';')[0])*u.deg
        alt = float(config['System']['elevation'].split(';')[0])*u.m
        eloc = EarthLocation(lon=lon,lat=lat,height=alt)
        # set Capture parameters
        fps = float(config['Capture']['fps'].split(';')[0])/u.s
        fov = {'w':float(config['Capture']['fov_w'])*u.deg,
               'h':float(config['Capture']['fov_h'])*u.deg}
        # compute FOV area as a rectangle
        meteor_height = 100.0*u.km
        fov_area = (2.0 * meteor_height)**2.0 * \
                    np.arctan(fov['w'].to('rad').value/2.0) * \
                    np.arctan(fov['h'].to('rad').value/2.0)             
        # get naked eye fov area
        eye_area = get_eye_area()
        # compute fov factor
        f_fov = eye_area / fov_area
        log.info('f_fov : '+str(f_fov))

        # reads time file
        log.debug('Read file: '+timefile)
        data = QTable.read(timefile,format='ascii',names=['time'],data_start=0)
        log.debug('There are '+str(len(data))+' detected meteors')
        times=Time(data['time'],format='isot')

        # make histogram
        # Convert to Julian Dates différence
        relative_time = (times - midnight).to('hr').value
        
        # create bin edges for histogram
        min_edge = int(np.min(relative_time))-1
        max_edge = int(np.max(relative_time))+1
        duration = 1.0 *u.hr # set to 1 hour to get ZHR values in [hr-1]
        bin_edges = np.arange(min_edge, max_edge + duration.to('hr').value, duration.to('hr').value)
        bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])

        # Create and plot histogram
        fig=plt.figure()
        HR, bin_edges, patches = plt.hist(relative_time, bins=bin_edges, 
                                          color='blue', alpha=0.6, 
                                          edgecolor='black', label='HR')
        HRerr = np.sqrt(HR)
        plt.errorbar(bin_centers,HR,yerr=HRerr[:len(bin_centers)], fmt='ro', label='HRerr')
        plt.xlabel('Relative time since '+midnight_str+ ' [hr]')
        plt.ylabel('HR [hr^-1]')
        plt.title('Hourly rate '+momet+' '+CAM)
        plt.legend()
        plt.savefig(HRplot,dpi=300)
        plt.close(fig)
        log.info('HR plot saved in '+HRplot)

        # set camera limiting magnitude
        LM = 6.0*u.mag
        # get radiant AltAz from times and eloc
        radiant.to_altaz(midnight+bin_edges[1:]*u.hr,eloc)
        # compute ZHR
        ZHR = HR2ZHR(HR,duration,shower.population_index,radiant.altaz.alt,LM,LM_naked_eye=6.0*u.mag)*f_fov
        # compute theoretical ZHR
        ZHR_theo = shower.get_ZHR(midnight+bin_edges[1:]*u.hr)
        
        # save all data into file
        data = QTable([midnight+bin_edges[1:]*u.hr,HR,ZHR,ZHR_theo,radiant.altaz.alt.to('deg')],
                      names=('Time','HR [hr-1]','ZHR [hr-1]','ZHR_theo [hr-1]','radiant_el [deg]'))
        data.write(ZHRfile,format='ascii.fixed_width_two_line',overwrite=True,
                        formats={'Time' : '%23s',
                                'HR [hr-1]' : '5.0f',
                                'ZHR [hr-1]' : '5.0f',
                                'ZHR_theo [hr-1]' : '5.0f',
                                'radiant_el [deg]' : '3.1f'})
        log.info('ZHR data saved in '+ZHRfile)
        
        # plot ZHR as a function of time
        fig=plt.figure()
        #plt.bar(bin_centers,HR, color='blue', alpha=0.6, edgecolor='black', label='HR')
        plt.bar(bin_centers,ZHR.value, color='blue', alpha=0.6, edgecolor='black', label='ZHR')
        plt.errorbar(bin_centers,ZHR.value,yerr=HRerr[:len(bin_centers)], fmt='ro', label='ZHRerr')
        plt.plot(bin_centers,ZHR_theo.value, 'b+', label='theoretical ZHR')
        plt.xlim(-10,30)
        plt.ylim(0, 500)
        plt.xlabel('Relative time since '+midnight_str+ ' [hr]')
        plt.ylabel('ZHR [hr^-1]')
        plt.title('ZHR ('+momet+' '+CAM+')')
        plt.legend()
        plt.savefig(ZHRplot,dpi=300)
        plt.close(fig)
        log.info('ZHR plot saved in '+ZHRplot)
        
        
        # go back to the MoMet directory to loop over next camera
        os.chdir(pth+'/'+momet)

log.info('Total number of meteor detected by all MoMet suitcases: '+str(nmet_tot))
log.info('all done')
