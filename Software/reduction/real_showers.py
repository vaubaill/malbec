#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Estimate the expected of meteor showers from active asteroids.

Created on Fri Sep 30 13:33:20 2022

@author: vaubaill
"""

# current number of known asteroids
n_ast = 600000.
# current number of active asteroids, from  # https://ui.adsabs.harvard.edu/abs/2022arXiv220812364C/abstract
n_act = 30
# current number of NEAs
n_nea = 26000.
# IAU number of established meteor showers
n_shw = 1000.

# number of shower parent comet (from IAU list of showers)
n_pcom = 32
# number of shower parent asteroid (from IAU list of showers)
n_past = 16

# expected LSST performances
n_lsst_neo = 1.0E+05
n_lsst_ast = 5.5E+06
# LSST NEO discovery percentage, according to Chesley & Veres 2017: https://ui.adsabs.harvard.edu/abs/2017arXiv170506209C/abstract
lsst_neo_eff = 0.6

# compute the probability of an asteroid to be active
p_act = n_act / n_ast
# compute expected number of active NEAs = expected number of showers from active asteroid
e_actneo_numast = n_nea * p_act

# if number of showers is real, compute proba of active nea
p_nea_iaushw = n_shw / n_nea
# expected number of active nea, considering IAU number of showers
e_actneo_iaushw = p_nea_iaushw * n_nea
# expected number of meteor showers from number of active asteroids
e_swh_act = n_nea * p_nea_iaushw
# expected number of active asteroids from LSST survey from current active asteroid
e_lsst_actneo = p_act * n_lsst_neo / lsst_neo_eff
# expecteneod number of active asteroid from LSST survey from current active asteroid
e_lsst_actast = p_act * n_lsst_ast

# compute difference between expected and real number of active asteroid from the 2 methods.
diff_actneo = e_swh_act / e_actneo_numast
# difference between expecte LSST number of active neo (=nb of showers) to iau nnb of showers.
diff_lsstiau = n_shw / e_lsst_actneo



# log
print('p_act : '+str(p_act))
print('p_nea_iaushw : '+str(p_nea_iaushw))
print('e_actneo_numast : '+str(e_actneo_numast))
print('e_actnea_iaushw : '+str(e_actneo_iaushw))
print('e_swh_act : '+str(e_swh_act))
print('diff_actneo : '+str(diff_actneo))
print('e_lsst_actneo : '+str(e_lsst_actneo))
print('e_lsst_actast : '+str(e_lsst_actast))
print('diff_lsstiau : '+str(diff_lsstiau))
