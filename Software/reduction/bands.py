import numpy as np
from astropy import units as u
from astropy import constants
from astropy.table import Table,QTable,Column

bands=QTable()
bands.add_column(Column(['U','B','V','R','I','J','H','K','g','r','i','z'], name='name'))
bands.add_column(Column([0.36,0.44,0.55,0.64,0.79,1.26,1.60,2.22,0.52,0.67,0.79,0.91]*u.um, name='lambda'))
bands.add_column(Column([0.15,0.22,0.16,0.23,0.19,0.16,0.23,0.23,0.14,0.14,0.16,0.13], name='dlambda/lambda'))
bands.add_column(Column([1810,4260,3640,3080,2550,1600,1080,670 ,3730,4490,4760,4810]*u.Jy, name='Flux0'))
bands.add_column(Column(['Bessel (1979)','Bessel (1979)','Bessel (1979)','Bessel (1979)','Bessel (1979)','Campins, Reike, & Lebovsky (1985)','Campins, Reike, & Lebovsky (1985)','Campins, Reike, & Lebovsky (1985)','Schneider, Gunn, & Hoessel (1983)','Schneider, Gunn, & Hoessel (1983)','Schneider, Gunn, & Hoessel (1983)','Schneider, Gunn, & Hoessel (1983)'], name='Reference'))
bands['dlambda'] = bands['lambda'] * bands['dlambda/lambda']
bands['lambda_min'] = bands['lambda'] - bands['dlambda'] / 2.0
bands['lambda_max'] = bands['lambda'] + bands['dlambda'] / 2.0
# compute frequency in [Hz]
bands['freq']  = (constants.c / bands['lambda']).to('Hz')
# compute Energy in [J]
bands['E'] = constants.h * bands['freq'].to('s-1')
# compute flux in SI [W m-2 nm-1]
bands['Flux0SI'] = (bands['Flux0'].to('W m-2 Hz-1')*bands['freq']/bands['lambda']).to('W m-2 nm-1')
# compute number of photons in SI [photons s-1 m-2 nm-1]
bands['ph/s/m2/nm'] = (bands['Flux0SI'] / bands['E']).to('s-1 m-2 nm-1')  * u.ph
# compute number of photons for the whole band in [photons s-1 m-2]
bands['ph/s/m2'] = (bands['ph/s/m2/nm']*bands['dlambda']).to('ph s-1 m-2')


"""
http://www.vikdhillon.staff.shef.ac.uk/teaching/phy217/instruments/phy217_inst_photsys.html#table1

 table 1: 	characteristics of the Johnson-Morgan-Cousins UBVRI photometric system. mVega refers to the magnitude of the star Vega in the filter. Strictly speaking, the magnitude of Vega is V=0.03 and all colours, e.g. U-B, B-V, etc, are zero. Magnitudes defined in this way are referred to as being in the Vega magnitude system. Alternative magnitude systems do exist, most prominently the AB magnitude system, in which the V-band magnitude of Vega is still 0.03 but only a star with a flat spectrum, i.e. F = constant, has the same magnitude in all filters (and hence zero colour). Note that any photometric (filter) system can be used with any magnitude system, and it is important not to confuse the two.

filter    	lambda (nm)    	Delta_lambda (nm)    	mVega    	F (W m-2 Hz-1)    	F_lambda (W m-2 nm-1)    	N_lambda (photons s-1 cm-2 Angstrom-1)
					
U	360	50	0	1.81 x 10-23	4.19 x 10-11	759
B	430	72	0	4.26 x 10-23	6.91 x 10-11	1496
V	550	86	0	3.64 x 10-23	3.61 x 10-11	1000
R	650	133	0	3.08 x 10-23	2.19 x 10-11	717
I	820	140	0	2.55 x 10-23	1.14 x 10-11	471

one can verify that:

bands['ph/s/m2/nm'].to('photons s-1 cm-2 Angstrom-1') = 
<Quantity [ 758.78728476, 1461.17049211,  998.80950071,  726.29777395,
            487.14366558 ...] ph / (Angstrom cm2 s)>
which is consistent with the above table.


"""
