# goal: clean *.cor file from empty lines

prog="(cleancorfile.sh) "
file_in=$1
file_out=$2
# verifications
syntax="**** syntaxe is: cleancorfile.sh file_in file_out"
if (test -z $file_in) # test to prevent empty chain
then
        echo "**** FATAL ERROR : input file to specify. $syntax"
        exit
fi
if (test -z $file_out) # test to prevent empty chain
then
        echo "**** FATAL ERROR : output file to specify. $syntax"
        exit
fi

awk 'NF>1 {print $0}' $file_in > $file_out

echo "$prog done"

