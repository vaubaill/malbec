# -*- coding: utf-8 -*-
"""
Spyder Editor

program written by aac
last update: 09/12/2019
"""
# this programs reads in a list of fits images
# detects stars in each one and 
# estimates the Full Width at Half Maximum (FWHM)
# it uses the astropy and photutils modules 
# developed on Python 3.6
import warnings
import numpy as np
import random
import matplotlib.pyplot as plt
from matplotlib.colors import PowerNorm
#
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,SigmaClip
from astropy.modeling import models, fitting
#
from photutils import DAOStarFinder, CircularAperture
from photutils import Background2D, MedianBackground
#
# where to go to find the fits files
fpath='/Users/aac/projects/meteors/malbec/test_data/field1/'
# read name of fie that contains list of fits images
# if not available, generate on shell eg ls -1 *.fits > files1.txt
filename=fpath+'files1.txt'
f=open(filename,"r")
data0=f.read()
# assumes one name per record
image_list=data0.split('\n')
f.close()
#
plt.figure(1); plt.clf()
# CEILING is used to artificially limit the number of images read from file
# for test purposes only 
CEILING=10
# to ignore CEILING, replace next line with
#   for index in np.arange(len(image_list)):
for index in np.arange(min(len(image_list),CEILING)):
    if image_list[index] != '':
        hdu=fits.open(fpath+image_list[index][:-4]+'.fit')        
        data=hdu[0].data
        data=data.astype(float)
        mean, median, std = sigma_clipped_stats(data, sigma=10.0)
    #  apply median filter to estimate background
        sigma_clip = SigmaClip(sigma=3.)    
        bkg_estimator = MedianBackground()
        bkg = Background2D(data, (50, 50), filter_size=(6, 6),
              sigma_clip=sigma_clip, bkg_estimator=bkg_estimator)
    # subtract background from image
    # do not do this if doing photometry: replace next line with data2=data   
        data2=data-bkg.background
    # fwhm0: choice of fwhm to use for the star-finding algorithm
    #   too large value will not find stars close together
    #   too small value will catch bright stars multiple times
    #   typical values are 4-8 
        fwhm0=6.0
    # nsigma_thres: how many sigma above backgrd to search for sources 
    # low values give many false detections and is slow
    # high values is fast but will miss some stars
    # typical values are 6-10 
        nsigma_thres=10.0
        daofind = DAOStarFinder(fwhm=fwhm0, threshold=nsigma_thres*std,
    ###                  sharplo=-1.0, sharphi=1.0, roundlo=0.0, roundhi=12.5,
                  exclude_border=True)  
        sources = daofind(data2) 
        half_max=0.5*(np.max(data2)-np.min(data2))
        pick=random.sample(range(len(sources)),1)
    # pick a source that is not too bright or too faint    
        while np.abs(sources['peak'][pick[0]]-half_max) > 0.5*half_max: 
                   pick=random.sample(range(len(sources)),1)
        p_init = models.Gaussian2D(amplitude=sources['peak'][pick[0]],
                x_mean=0,y_mean=0,
    #    for a gaussian profile, fwhm=2.355*sigma            
                x_stddev=fwhm0/2.355,y_stddev=fwhm0/2.355)
        fit_p = fitting.LevMarLSQFitter()
    # fit gaussian on a 10x10 grid to estimate fwhm
        width=10    
        y, x = np.mgrid[:width, :width]
        xcentre=int((sources['xcentroid'][pick[0]]))
        ycentre=int((sources['ycentroid'][pick[0]]))       
        with warnings.catch_warnings():
    # Ignore model linearity warning from the fitter
             warnings.simplefilter('ignore')
             p = fit_p(p_init, x,y,
                 data[ycentre-int(width/2):ycentre+int(width/2),
                      xcentre-int(width/2):xcentre+int(width/2)])
        positions = np.transpose((sources['xcentroid'],
                                  sources['ycentroid']))
    #
    # amber circles show detected sources
        apertures = CircularAperture(positions, r=12.)
    # red circle shows source used to extract fwhm    
        highlight = CircularAperture([sources['xcentroid'][pick[0]], 
                                     sources['ycentroid'][pick[0]]],
                                     r=25.)
        norm = PowerNorm(0.3)        
        plt.imshow(data2, cmap='gray', norm=norm)
        apertures.plot(color='yellow', lw=1.5, alpha=0.5)
        highlight.plot(color='red', lw=2.5, alpha=0.5)
    # short delay before displaying next image
        plt.pause(1)
    #
        print('Image ',index,': ',hdu[0].header['DATE'],
                   data.shape,(mean, median, std))
        print('Detected ', len(sources), ' sources')
        print('FWHM: {:.3f} x {:.3f} pixels\n'
              .format(2.355*p.x_stddev.value,
                      2.355*p.y_stddev.value)) 
    #
        hdu.close()
        if index < min(len(image_list),CEILING)-1: plt.clf()
