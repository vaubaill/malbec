#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 26 10:35:49 2022

@author: vaubaill
"""
import numpy as np
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import SkyCoord,EarthLocation,AltAz,ICRS
import spiceypy as sp
from make_atmosphere_from_alt import make_atmosphere_kernel
import matplotlib.pyplot as plt

from showersim.phot import mass2radius,mag2mass_Hughes1995

def cumulmag2r(a):
    return 1.0+np.log10(a)

def population_index(a):
    return 10**a

def mass_index(r):
    return 1 + 2.3*np.log10(r)

def size_index(sm):
    return 3*sm - 2

# Start of program
file_csv = './2022TAH-Cluster.csv'

names=['A','Comm','#C','#L','#o','F','F_b','F_e','Tb','Te','D','Ab','Am','Ae','Eb','Em','Ee','Mag']
data = QTable().read(file_csv,format='ascii.csv',delimiter=',',names=names,data_start=3,data_end=37)
data.sort('Ab')

# Cluster manual data:
time = Time('2022-05-31T06:48:00.000',scale='utc')
fps=20.0/u.s
# assume meteor altitude
alt_met = 90.0*u.km
# meteor entry velocity
V = 12.2*u.km/u.s
# meteoroid density
rho = 2500.0*u.kg/u.m**3
rho = 1000.0*u.kg/u.m**3

# load standard SPICE kernel
sp.furnsh('/astrodata/kernels/standard.ker')
# load aircraft SPICE kernel
dir_plane = '../../DATA/20220531/PHENOM300/'
plane_spk =dir_plane+'PHENOM300.spk' 
sp.furnsh(plane_spk)
sp.furnsh(dir_plane+'PHENOM300.tf')
sp.furnsh(dir_plane+'PHENOM300.ik')
sp.furnsh(dir_plane+'PHENOM300_CAMORI_FRAME.tk')
sp.furnsh(dir_plane+'PHENOM300.ck')
# get airplane SPICE id
plane_id = sp.spkobj(plane_spk)[0]
plane_name = sp.bodc2n(plane_id)
# get airplaen position at time of the eteor cluster
et = sp.utc2et(time.isot)
(eph, lt) = sp.spkezr(plane_name,et,'ITRF93','NONE','EARTH')
plane_eloc = EarthLocation.from_geocentric(eph[0], eph[1], eph[2],unit=u.km)

# Make Sky coordinates from measurements
altaz_b = AltAz(az=data['Ab']*u.deg,alt=data['Eb']*u.deg,obstime=time,location=plane_eloc)
altaz_e = AltAz(az=data['Ae']*u.deg,alt=data['Ee']*u.deg,obstime=time,location=plane_eloc)
radec_b = altaz_b.transform_to(ICRS)
radec_e = altaz_e.transform_to(ICRS)
# compute min/max separation between all meteors
sky_dist= radec_b.separation(np.roll(radec_e,1))
print('max separation: ',np.max(sky_dist))
print('Min separation: ',np.min(sky_dist))


radec_radiant = SkyCoord(ra=209.5*u.deg,dec=28.0*u.deg, frame="icrs")
rad_dist = radec_b.separation(radec_radiant)
print('min/max sep w/ radiant: ',np.min(rad_dist.value),np.max(rad_dist.value),np.median(rad_dist.value))

# computes time of meteors
time_beg = time + data['F_b']/fps
time_end = time + data['F_e']/fps
et_beg = 1.0E+22 # something big
et_end = -et_beg # something small
for tb,te in zip(time_beg,time_end):
    et_beg = np.min([et_beg,sp.utc2et(tb.isot)])
    et_end = np.max([et_end,sp.utc2et(te.isot)])
print('et_beg: ',et_beg)
print('et_end: ',et_end)

# make Atmosphere object
(atmosphere_name,atmosphere_id,atmosphere_text_kernel,atmosphere_spk_file) = make_atmosphere_kernel(et_beg,et_end,meteor_alt=alt_met)
# computes physical distance btween airplane and meteor and absolute magnitude
H = np.array([])
for (az,el,mag) in zip(altaz_b.az,altaz_b.alt,data['Mag']):
    # computes intersection between meteor direction and atmopshere
    bound = sp.sphrec(1.0, (90.0*u.deg-el).to('rad').value, az.to('rad').value)
    bound_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',plane_name,'PHENOM300_TOPOLIKE',bound)
    phy_dist = sp.vnorm(vecout)*u.km
    print('phy_dist: ',phy_dist)
    h = mag - 2.5*np.log10(phy_dist/(100.0*u.km))
    print('Mag: ',mag,' H: ',h)
    H = np.append(H,h)
 # set units to H
H = H*u.mag

# make histogram of H   
Mass = mag2mass_Hughes1995(H,V)
radius = mass2radius(Mass,density=rho)
print('H: ',H.value)
#print('Mass: ',np.sort(Mass.value))
print('Mass: ',np.sort(Mass.to('kg').value))
print('radius: ',np.sort(radius.value),np.min(radius),np.max(radius))
print('diameter: ',np.sort(radius.value*2))
print('average radius: ',np.mean(radius.value))
print('radius (raw): ',radius.to('mm'))
print('total mass: ',np.sum(Mass))
print('total diameter: ',2.0*mass2radius(np.sum(Mass),density=rho))


# Magnitude distribution
#histoCuMag,binMag,patches = plt.hist(Mag,bins=8,histtype='bar',cumulative=True,log=False)
histoCuMag,binMag,patches = plt.hist(H.value,bins=8,histtype='bar',cumulative=True,log=False)
fit_hcmag = np.polyfit(binMag[:-1],histoCuMag,1)
print('Fit Cumul Mag = ',fit_hcmag)
#plt.plot(binMag,(fit_hcmag[0]*binMag + fit_hcmag[1])),
#         label = str(round(fit_hcmag[0],2))+'*Mag+'+str(round(fit_hcmag[1],2)),
#         linestyle = '--')
plt.xlabel('Absolute Magnitude')
plt.ylabel('Cumulative number')
plt.legend()
plt.show()


# compute population index
r = cumulmag2r(fit_hcmag[0])
sm = mass_index(r)
smc = sm - 1
s = size_index(sm)
print('r = ',r)
print('sm = ',sm)
print('smc = ',smc)
print('s = ',s)
