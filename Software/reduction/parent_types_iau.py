#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 30 16:11:40 2022

get number of comet and asteroid meteor shower parents from IAU data.

@author: vaubaill
"""

from astropy.table import Table

# set IAU data file
iau_file = '../conf/streamestablisheddata_jv.csv'

# read IAU data file
data = Table.read(iau_file, format='ascii.csv', delimiter=';')

# clean up data to eliminate doublons
data_clean = Table(names=data.colnames,dtype=data.dtype)
iaucode = {}
for d in data:
    # test if IAU code already exists
    try:
        tmp = iaucode[d['IAUNo']]
    except:
        iaucode[d['IAUNo']] = d['IAUNo']
        data_clean.add_row(d)

# get total number of showers
nb_tot = len(data_clean['Parent_body'])

# get comet bodies
mask_c1 = data_clean['Parent_body']=='*C/*'
mask_c2 = data_clean['Parent_body']=='*C_*'
mask_c3 = data_clean['Parent_body']=='*P/*'
mask_c4 = data_clean['Parent_body']=='*D/*'
nb_c = len(data_clean['Parent_body'][mask_c1])+len(data_clean['Parent_body'][mask_c2])+len(data_clean['Parent_body'][mask_c3])+len(data_clean['Parent_body'][mask_c4])

# get unknown parent
mask_v = data_clean['Parent_body']==''
nb_v = len(data_clean['Parent_body'][mask_v])

# get number of asteroid bodies
nb_a = nb_tot - nb_c - nb_v

# print
print('nb_tot: '+str(nb_tot))
print('nb_v: '+str(nb_v))
print('nb_c :'+str(nb_c))
print('nb_a :'+str(nb_a))