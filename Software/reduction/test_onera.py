import os
import sys
from astropy.time import Time
import astropy.units as u
from astropy.table import QTable

from reduction.onera_skybrightness import select_onera_file,get_all_onera_file_info,select_onera_file,get_onera_data,get_sky_brightness

from bands import bands





"""
date = Time('2017-06-21T13:00:00',scale='utc')
alt = 15500.0*u.m
band_name = 'z'
"""

"""
data_file = get_all_onera_file_info()
print(data_file)
"""

"""
onera_file = select_onera_file(date,alt)
print('selected onera file: '+onera_file)
"""

"""
onera_file = select_onera_file(date,alt,band)
print('onera_file *should be*: lum_Rayjn-20170621T120000_UT-ALT15AZ180EL30.csv')
print('onera_file is: '+onera_file)
"""

"""
data = get_onera_data(date,alt,band_name)
print('ONERA data:')
print(data)
"""

"""
(rad1,rad2) = get_sky_brightness(date,alt,band_name=band_name)
print('rad1,rad2: ',rad1,rad2)
"""

"""
band_name = 'V'
(rad1,rad2) = get_sky_brightness(date,alt,band_name=band_name)
print('rad1,rad2: ',rad1,rad2)
"""

date = Time('2020-06-21T13:00:00',scale='utc')
alt = 29500.0*u.m
band_name = 'z'

(rad1,rad2) = get_sky_brightness(date,alt,band_name=band_name)
print('rad1,rad2: ',rad1,rad2)
