import glob
import os
import logging
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable
from astropy import units as u
import astropy.io.ascii as ascii


# goal: reduce images taken with malbec camera

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# user part:
data_dir = '/media/ccolomer/Elements/DATA/SUNRISE-acA1920-155um-Basler-12mm-1.6-None/'
camera_lamba = 6.0E-07 *u.m              # center wavelength observation
# max ADU value given the camera settings during the acquisition
camera_nbbit = 8
# max allowed gain
camera_max_gain = 32.0* u.dB
# camera pixel size in x-dimension
camera_xpixsz = 5.86 * u.um
camera_ypixsz = 5.86 * u.um
# offset of the camera given the settings during the acquisition
offset = 0.0

# output file and QTable
outfile = data_dir + 'ALLDATA.dat'
outable = QTable(names=['Sun_el (deg)','BCK (ADU)','exp (sec)','gain (e/adu)'],
                 dtype=['float'       ,'float'    ,'float'    ,'float'])

# get list of all image files
pattern = data_dir + 'FITS/*.fits'
log.info('Getting image file list in '+pattern)
listimg=glob.glob(pattern)
listimg.sort()
log.info('There are '+str(len(listimg))+' images to process')

# loop over all image files
for imgfile in listimg: 
    log.info('======= Now treating image: '+os.path.basename(imgfile))
    # open file
    hdu = fits.open(imgfile)[0]
    # get number of bits
    nbbit = float(hdu.header['BITPIX'])
    # set the maximum possible ADU
    max_adu = (2**camera_nbbit - 1) * u.adu
    # retrieve img time
    time_img = Time(hdu.header['DATE-OBS'],scale='utc')
    # retrieve exposure duration
    expo = float(hdu.header['EXPTIME']) * u.s
    # retrieve gain setting
    gain = float(hdu.header['GAIN']) * u.dB
    # retrieve location
    site = EarthLocation.from_geodetic(float(hdu.header['GEOLON'])*u.deg,
                                       float(hdu.header['GEOLAT'])*u.deg,
                                       float(hdu.header['GEOALT'])*u.m)
    # correct for aberrant value of gain
    if gain > camera_max_gain:
        gain = 0.0 * u.dB
    
    # create local frame
    altazframe = AltAz(obstime=time_img, location=site,obswl=camera_lamba)
    # get location of the Sun at the time of the image
    sunaltazs = get_sun(time_img).transform_to(altazframe)
    sunelevation = sunaltazs.alt
    
    # performs basic stats
    data = hdu.data
    mean, median, std = sigma_clipped_stats(data)
    log.info('mean,median,std='+str([mean,median,std]))
    if (mean==median==max_adu.value):
       log.info('saturated image '+imgfile)
       continue
    # store usefull data
    outable.add_row([sunelevation,median,expo,gain])
    # verbose
    log.info('Time='+time_img.iso+' Sun_el='+str(sunelevation.to('deg').value)+' bck='+str(median)+' exp='+str(expo.to('s'))+' gain='+str(gain))
    
    # update the img file header
    hdu.header['XPIXELSZ'] = (5.86,'X pixel size, in um') # for Basler acA1920-155um
    hdu.header['YPIXELSZ'] = (5.86,'Y pixel size, in um') # for Basler acA1920-155um
    hdu.header['FOCAL'] = (12,'Focal length, in mm') # for Basler-12mm-1.6
    hdu.header['SITELAT'] = (site.lat.to_string(unit=u.degree, sep=':'),'Latitude of Observatory, in deg')
    hdu.header['SITELON'] = (site.lon.to_string(unit=u.degree, sep=':'),'Longitude of Observatory, in deg')
    hdu.header['SITEALT'] = (site.height.to('m').value,'Altitude of Observatory, in m')
    hdu.header['COMMENT'] = ('Sun elevation: '+str(sunelevation.to('deg').value)+' deg')
    hdu.header['COMMENT'] = ('Header updated by the malbec_reduce_sunrise.py script')
    # save the reduced image in new file
    #imgfile_new = os.path.splitext(imgfile)[0] + '-c.fits'
    imgfile_new = imgfile
    hdu.writeto(imgfile_new,overwrite=True)
    log.info('Reduced image saved in '+imgfile_new)
    # END of loop over fit images

# save table output
outable.write(outfile,format='ascii.fixed_width_two_line',delimiter=' ')
log.info('data saved in '+outfile)
log.info('done')
