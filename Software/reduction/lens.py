"""Define a Lens Object

Author: J. Vaubaillon, IMCCE, Apr 2020

The MALBEC project
"""

import logging
import numpy as np
import astropy.units as u
import logging

from filter import Filter


# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


class Lens(object):
    # Brand name
    @property
    def brand(self):
        return self.__brand
    @brand.setter
    def brand(self,brand):
        self.__brand = brand
    
    # Lens name
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,name):
        self.__name = name
    
    # focal length
    @property
    def F(self):
        return self.__F
    @F.setter
    def F(self,F):
        self.__F = F.to('mm')
    
    # F-number
    @property
    def N(self):
        return self.__N
    @N.setter
    def N(self,N):
        self.__N = N
    
    # aperture: effective aperture ; D in: N=F/D
    @property
    def D(self):
        return self.__D
    @D.setter
    def D(self,D):
        self.__D = D.to('mm')
    
    # collecting Area
    @property
    def area(self):
        return self.__area
    @area.setter
    def area(self,area):
        self.__area = area.to('mm2')
    
    # PSF diameter, in [um]
    @property
    def psf(self):
        return self.__psf
    @psf.setter
    def psf(self,psf):
        self.__psf = psf.to('um')
    
    # filter name, optional. Default is None.
    @property
    def filtre(self):
        return self.__filtre
    @filtre.setter
    def filtre(self,filtre):
        self.__filtre = filtre
    
    # known
    @property
    def known(self):
        return self.__known
    @known.setter
    def known(self,known):
        self.__known = known
    
    # initialization
    __brand = ''
    __name = ''
    __F = 0 *u.mm
    __N = 0
    __D = 0 *u.mm
    __psf = 0 *u.um
    __filtre = 'None'
    
    def __init__(self,brand='Unknown',name='Unknown',f=0*u.mm,N=0.0,psf=0.0*u.um,filtre='None'):
        # set brand and name
        self.brand = brand
        self.name = name
        self.filtre = filtre
        
        # check if the lens is already known
        self.known = self.check_known()
        
        # if needed, set f, N and psf
        if self.F==0*u.mm:
            self.F = f
        if self.N==0.0:
            self.N = N
            self.D = self.N / self.F
            self.area = np.pi * (self.D / 2)**2
        if self.psf==0.0*u.um:
            self.psf = psf
        
        # check that initialization went well
        if (self.N==0.0):
            msg = '*** FATAL ERROR: Nture is zero'
            log.error(msg)
            raise ValueError(msg)
        if (self.F==0*u.m):
            msg = '*** FATAL ERROR: f is zero'
            log.error(msg)
            raise ValueError(msg)
        if (self.psf==0*u.um):
            msg = '*** FATAL ERROR: psf is zero'
            log.error(msg)
            raise ValueError(msg)
        if (self.brand=='Unknown'):
            msg = '*** FATAL ERROR: brand is Unknown'
            log.error(msg)
        if (self.name=='Unknown'):
            msg = '*** FATAL ERROR: name is Unknown'
            log.error(msg)
            raise ValueError(msg)
        if ((self.N==0.0) or (self.F==0*u.mm) or (self.brand=='Unknown') or (self.name=='Unknown') or (self.psf==0*u.um)):
            msg = '*** FATAL ERROR: unable to initialize the lens.'
            log.error(msg)
            raise ValueError(msg)
        
        return
    
    
    
    def info(self):
        """log information about the lens.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('----- Lens info --- ')
        log.info('Brand:    '+self.brand)
        log.info('Name:     '+self.name)
        log.info('F:        '+str(self.F.to('mm')))
        log.info('N:        '+str(self.N))
        log.info('D:        '+str(self.D.to('mm')))
        log.info('area:     '+str(self.area.to('mm2')))
        log.info('psf:      '+str(self.psf.to('um')))
        log.info('known:    '+str(self.known))
        return
    
    
    def compute_D(self):
        """Computer diameter of the entrance pupil (effective aperture).
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        self.D = self.F / self.N
        return
    
    def compute_area(self):
        """Computer light collecting area.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        self.area = np.pi * (self.D / 2)**2
        
        return
    
    def check_known(self):
        """Check if the lens is known.
        
        If it is, focal lenght f and aperture-number N are set and True is returned.
        Otherwise return False.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        known : boolean
            True if the lens is known, False otherwise.
        
        """
        self.known = False
        # check Basler lens
        if self.brand=='Basler':
            if self.name=='12mm-1.6':
                self.F = 12.0*u.mm
                self.N = 1.6
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
        # check Computar lens
        elif self.brand=='Computar':
            if self.name=='8mm-1.4':
                self.F = 8.0*u.mm
                self.N = 1.4
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
            if self.name=='12.5mm-1.4':
                self.F = 12.5*u.mm
                self.N = 1.4
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
            if self.name='M7528MP':
                self.F = 75.0*u.mm
                self.N = 2.8
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
        # check Kowa lens
        elif self.brand=='Kowa':
            if self.name=='6mm-1.8':
                self.F = 6.0*u.mm
                self.N = 1.8
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
        # check SV lens
        elif self.brand=='SV':
            if self.name=='6mm-1.4':
                self.F = 6.0*u.mm
                self.N = 1.4
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
        # check Pentax lens
        elif self.brand=='Pentax':
            if self.name=='12mm-1.2':
                self.F = 12.0*u.mm
                self.N = 1.4
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
            if self.name=='6mm-1.2':
                self.F = 6.0*u.mm
                self.N = 1.2
                self.compute_D()
                self.compute_area()
                self.psf = 5*u.um
                self.known = True
        # log
        log.debug('known: '+str(self.known))
        return self.known
