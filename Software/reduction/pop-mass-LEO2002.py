#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 07:24:59 2022

@author: vaubaill
"""

import numpy as np
import matplotlib.pyplot as plt

def cumulmag2r(a):
    return 1.0+np.log10(a)

def population_index(a):
    return 10**a

def mass_index(r):
    return 1 + 2.3*np.log10(r)

def size_index(sm):
    return 3*sm - 2


# LEO2002 Magnitude distribution
histoMag = np.array([0.0,1.0,2.0,4.0,7.0,10.0,14.0])
binMag = np.arange(-2,5,dtype=float)
histoCuMag = np.cumsum(histoMag)
plt.plot(binMag,histoCuMag)
fit_hcmag = np.polyfit(binMag,histoCuMag,1)
print('Fit Cumul Mag = ',fit_hcmag)
plt.plot(binMag,(fit_hcmag[0]*binMag + fit_hcmag[1]),
         label = str(round(fit_hcmag[0],2))+'*Mag+'+str(round(fit_hcmag[1],2)),
         linestyle = '--')
plt.xlabel('Magnitude')
plt.ylabel('Cumulative number')
plt.legend()
plt.show()


# compute population index
r = cumulmag2r(fit_hcmag[0])
sm = mass_index(r)
smc = sm - 1
s = size_index(sm)
print('r = ',r)
print('sm = ',sm)
print('smc = ',smc)
print('s = ',s)