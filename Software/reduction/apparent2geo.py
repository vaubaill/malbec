import pandas as pd
import numpy as np
import argparse
import wmpl
from wmpl.Utils.TrajConversions import J2000_JD, date2JD, raDec2ECI, eci2RaDec, geo2Cartesian, jd2LST, altAz2RADec, equatorialCoordPrecession, cartesianToSpherical, sphericalToCartesian, raDec2AltAz, cartesian2Geo
from wmpl.Utils.Math import vectMag, vectNorm
from wmpl.Utils.GeoidHeightEGM96 import mslToWGS84Height
from wmpl.Trajectory.Orbit import Orbit
from astropy.time import Time

G = 6.67430e-11  # Gravitational constant (m^3 kg^-1 s^-2)
EARTH_MASS = 5.9722e24  # Mass of Earth (kg)

def convertToGeocentric(ra_a, dec_a, v_init, lat, lon, alt, jd_ref):
    """ Converts the given apparent radiant to the geocentric radiant. """
    # Convert apparent radiant (RA, Dec) to ECI coordinates
    radiant_eci = np.array(raDec2ECI(np.radians(ra_a), np.radians(dec_a)))

    # Calculate the reference ECI position (lat, lon, alt) to Cartesian ECI coordinates
    eci_ref = geo2Cartesian(lat, lon, alt, jd_ref)

    # Calculate the geocentric latitude (latitude which considers the Earth as an ellipsoid) of the reference 
    # trajectory point
    lat_geocentric = np.arctan2(eci_ref[2], np.sqrt(eci_ref[0]**2 + eci_ref[1]**2))

    # Calculate the geographical coordinates of the reference trajectory ECI position
    lat_ref, lon_ref, ht_ref = cartesian2Geo(jd_ref, *eci_ref)  # Updated to use correct number of arguments

    # Initialize a new orbit structure
    orb = Orbit()

    # Calculate the velocity of the Earth rotation at the position of the reference trajectory point (m/s)
    v_e = 2 * np.pi * vectMag(eci_ref) * np.cos(lat_geocentric) / 86164.09053

    # Calculate the equatorial coordinates of east from the reference position on the trajectory
    azimuth_east = np.pi / 2
    altitude_east = 0
    ra_east, dec_east = altAz2RADec(azimuth_east, altitude_east, jd_ref, lat_ref, lon_ref)

    # Apply the Earth rotation correction if the station coordinates are fixed (a MUST for the 
    # intersecting planes method!)
    ### Set fixed stations radiant info ###

    # If the stations are fixed, then the input state vector is already fixed to the ground
    orb.ra_norot, orb.dec_norot = eci2RaDec(radiant_eci)

    # Apparent azimuth and altitude (no rotation)
    orb.azimuth_apparent_norot, orb.elevation_apparent_norot = raDec2AltAz(orb.ra_norot, orb.dec_norot, \
        jd_ref, lat_ref, lon_ref)

    # # Estimated average velocity (no rotation)
    # orb.v_avg_norot = v_avg

    # Estimated initial velocity (no rotation)
    orb.v_init_norot = v_init

    ### ###

    # If the initial velocity was the reference velocity, use it for the correction
    v_ref_vect = v_init*radiant_eci


    v_ref_corr = np.zeros(3)

    # Calculate the corrected reference velocity vector/radiant
    v_ref_corr[0] = v_ref_vect[0] - v_e*np.cos(ra_east)
    v_ref_corr[1] = v_ref_vect[1] - v_e*np.sin(ra_east)
    v_ref_corr[2] = v_ref_vect[2]

    v_init_corr = vectMag(v_ref_corr)

    # Calculate apparent RA and Dec from radiant state vector
    orb.ra, orb.dec = eci2RaDec(radiant_eci)
    orb.v_init = v_init
    # orb.v_init_stddev = v_init_stddev_direct
    # orb.v_avg = v_avg

    # Calculate the apparent azimuth and altitude (geodetic latitude, because ra/dec are calculated from ECI,
    #   which is calculated from WGS84 coordinates)
    orb.azimuth_apparent, orb.elevation_apparent = raDec2AltAz(orb.ra, orb.dec, jd_ref, lat_ref, lon_ref)

    orb.jd_ref = jd_ref
    orb.lon_ref = lon_ref
    orb.lat_ref = lat_ref
    orb.ht_ref = ht_ref
    orb.ht_ref_wgs84 = wmpl.Utils.GeoidHeightEGM96.mslToWGS84Height(orb.lat_ref, orb.lon_ref, orb.ht_ref)
    orb.lat_geocentric = lat_geocentric

    # Assume that the velocity in infinity is the same as the initial velocity (after rotation correction, if
    # it was needed)
    orb.v_inf = v_init_corr


    # Calculate the geocentric velocity (sqrt of squared inital velocity minus the square of the Earth escape 
    # velocity at the height of the trajectory), units are m/s.
    # Square of the escape velocity is: 2GM/r, where G is the 2014 CODATA-recommended value of 
    # 6.67408e-11 m^3/(kg s^2), and the mass of the Earth is M = 5.9722e24 kg
    v_g = np.sqrt(v_init_corr**2 - (2*6.67408*5.9722)*1e13/vectMag(eci_ref))


    # Calculate the radiant corrected for Earth's rotation (ONLY if the stations were fixed, otherwise it
    #   is the same as the apparent radiant)
    ra_corr, dec_corr = eci2RaDec(vectNorm(v_ref_corr))

    # Calculate the Local Sidreal Time of the reference trajectory position
    lst_ref = np.radians(jd2LST(jd_ref, np.degrees(lon_ref))[0])

    # Calculate the apparent zenith angle
    zc = np.arccos(np.sin(dec_corr)*np.sin(lat_geocentric) \
        + np.cos(dec_corr)*np.cos(lat_geocentric)*np.cos(lst_ref - ra_corr))

    # Calculate the zenith attraction correction
    delta_zc = 2*np.arctan2((v_init_corr - v_g)*np.tan(zc/2), v_init_corr + v_g)

    # Zenith distance of the geocentric radiant
    zg = zc + np.abs(delta_zc)

    ##########################################################################################################



    ### Calculate the geocentric radiant ###
    ##########################################################################################################

    # Get the azimuth from the corrected RA and Dec
    azimuth_corr, _ = raDec2AltAz(ra_corr, dec_corr, jd_ref, lat_geocentric, lon_ref)

    # Calculate the geocentric radiant
    ra_g, dec_g = altAz2RADec(azimuth_corr, np.pi/2 - zg, jd_ref, lat_geocentric, lon_ref)
    

    ### Precess ECI coordinates to J2000 ###

    # Convert rectangular to spherical coordiantes
    re, delta_e, alpha_e = cartesianToSpherical(*eci_ref)

    # Precess coordinates to J2000
    alpha_ej, delta_ej = equatorialCoordPrecession(jd_ref, J2000_JD.days, alpha_e, delta_e)

    # Convert coordinates back to rectangular
    eci_ref = sphericalToCartesian(re, delta_ej, alpha_ej)
    eci_ref = np.array(eci_ref)

    ######

    # Precess the geocentric radiant to J2000
    ra_g, dec_g = equatorialCoordPrecession(jd_ref, J2000_JD.days, ra_g, dec_g)
    
    return ra_g, dec_g, v_g  # Returning initial velocity as geocentric velocity for now

def process_csv(filename):
    df = pd.read_csv(filename)
    results = []

    for _, row in df.iterrows():
        try:
            ra_a = row['Radian RA']
            dec_a = row['Radian Dec']
            v_init = row['VE expected'] 
            lat = np.radians(row['Initial Latitude'])
            lon = np.radians(row['Initial Longitude'])
            alt = row['Initial Altitude']  # m
            jd = row['JD']

            ra_g, dec_g, v_g = convertToGeocentric(ra_a, dec_a, v_init, lat, lon, alt, jd)
            results.append({
                'Event': row['event_codename'],
                'Geocentric_RA': np.degrees(ra_g),
                'Geocentric_Dec': np.degrees(dec_g),
                'Geocentric_Velocity_km_s': v_g / 1000  # convert m/s to km/s
            })
        except Exception as e:
            print(f"Error processing row: {row['event_codename']}, Error: {e}")

    result_df = pd.DataFrame(results)
    result_df.to_csv('geocentric_results.csv', index=False)
    print('Geocentric results saved to geocentric_results.csv')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process fireball events and convert apparent radiant to geocentric radiant.")
    parser.add_argument("-f", "--filename", required=True, help="Path to the input CSV file.")
    args = parser.parse_args()

    process_csv(args.filename)