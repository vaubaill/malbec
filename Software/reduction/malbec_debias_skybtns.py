"""Debias sky brightness measurement as a function of altitude.

Use ground based measurements to remove the degenerancy in Sun elevation.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project

"""
import os
import logging
import numpy as np
import matplotlib.pyplot as plt
from astropy.table import QTable
from astropy import units as u
import astropy.io.ascii as ascii

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


# user's part:
file_alt = '../../DATA/20170812/Results/ALLDATA.dat'
file_sb_altel = '../../DATA/20170812/Results/skybrightness.dat'
file_sb_el = '../..//DATA/TESTS_CAMERAS/SUNRISE-acA1920-155um-Basler-12mm-1.6-None/skybrightness.dat'
out_dir = os.path.dirname(file_sb_altel)+'/'

# set column names
names = ['Sun_elevation','E [W.m-2.deg-2]','E [W.m-2.sterad-1]']
# read sky brightness as a function of altitude
log.info('Read: '+file_sb_altel)
data_sb_altel = QTable.read(file_sb_altel,names=names,format='ascii.fixed_width_two_line')
data_sb_altel['Sun_elevation'].unit = u.deg
data_sb_altel['E [W.m-2.deg-2]'].unit = 'W.m-2.deg-2'
data_sb_altel['E [W.m-2.sterad-1]'].unit = 'W.m-2.sr-1'
log.info('There are '+str(len(data_sb_altel['Sun_elevation']))+' data in '+file_sb_altel)

# read sky brightness as a function of Sun elevation
log.info('Read: '+file_sb_el)
data_sb_el = QTable.read(file_sb_el,names=names,format='ascii.fixed_width_two_line')
data_sb_el['Sun_elevation'].unit = u.deg
data_sb_el['E [W.m-2.deg-2]'].unit = 'W.m-2.deg-2'
data_sb_el['E [W.m-2.sterad-1]'].unit = 'W.m-2.sr-1'
log.info('There are '+str(len(data_sb_el['Sun_elevation']))+' data in '+file_sb_el)

# read altitude file
log.info('Read: '+file_alt)
data = QTable.read(file_alt, format='ascii',names=('Alt','Sun_el','BCK','expo','gain'))
data['Alt'].unit='km'
data['Sun_el'].unit='deg'
data['BCK'].unit='adu'
data['expo'].unit='s'
data['gain'].unit=u.electron/u.adu
log.info('There are '+str(len(data['Sun_el']))+' data in '+file_alt)

# get location of take_off and landing
loc_explode =np.argmax(data['Alt'].value)
loc_takeoff =np.where(data['Alt'][:loc_explode].to('km').value > 0.01)[0][0]

# creates interpolator of Altitude given Sun elevation
data_sb_altel['Alt [km]'] = np.interp(data_sb_altel['Sun_elevation'].to('deg').value,
                                      data['Sun_el'][loc_takeoff:loc_explode].to('deg').value,
                                      data['Alt'][loc_takeoff:loc_explode].to('km').value)
data_sb_altel['Alt [km]'].unit = u.km

# creates an interpolator of sky brightness at ground for given Sun elevation
data_sb_altel['E_el@ground [W.m-2.sterad-1]'] = np.interp(data_sb_altel['Sun_elevation'].to('deg').value,
                                                          data_sb_el['Sun_elevation'].to('deg').value,
                                                          data_sb_el['E [W.m-2.sterad-1]'].to('W.m-2.sr-1').value)
data_sb_altel['E_el@ground [W.m-2.sterad-1]'].unit = 'W.m-2.sr-1'




# compute gain as a function of Sun elevation
data_sb_altel['gain_el'] = data_sb_altel['E_el@ground [W.m-2.sterad-1]'] / data_sb_altel['E [W.m-2.sterad-1]']
# save data
file_gain_el = out_dir + 'gain_el.dat'
out_data = QTable([data_sb_altel['Alt [km]'].to('km'),
                   data_sb_altel['Sun_elevation'].to('deg'),
                   data_sb_altel['E [W.m-2.sterad-1]'],
                   data_sb_altel['E_el@ground [W.m-2.sterad-1]'],
                   data_sb_altel['gain_el']],
                    names=['Alt [km]','Sun_elevation [deg]','E [W.m-2.sterad-1]','E_el@ground [W.m-2.sterad-1]','gain'])
out_data.write(file_gain_el,format='ascii.fixed_width_two_line',overwrite=True)
log.info('Data saved in '+file_gain_el)

# plot gain as a function of Sun elevation
E_gain_plot_file = out_dir + 'Gain_Sunel.png'
fig=plt.figure()
plt.plot(data_sb_altel['Sun_elevation'].to('deg').value,data_sb_altel['gain_el'].value,'bo',linewidth=1.5)
plt.title('Gain vs Sun elevation')
plt.xlabel('Sun elevation [deg]')
plt.ylabel('gain')
plt.savefig(E_gain_plot_file)
plt.close(fig)
log.info('plot saved in '+E_gain_plot_file)

# plot gain as a function of altitude
E_gain_plot_file = out_dir + 'gain_el.png'
fig=plt.figure()
plt.plot(data_sb_altel['Alt [km]'].to('km').value,data_sb_altel['gain_el'].value,'bo',linewidth=1.5)
plt.title('Gain vs Altitude')
plt.xlabel('Altitude [km]')
plt.ylabel('gain')
plt.savefig(E_gain_plot_file)
plt.close(fig)
log.info('plot saved in '+E_gain_plot_file)
