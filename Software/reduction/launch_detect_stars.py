from detect_stars_all import detect_stars

# set parameters
path=''
CEILING=10
fwhm0=0.6
nsigma_thres=10.0
photometry=False

detect_star_all(path=path,CEILING=CEILING,fwhm0=fwhm0,nsigma_thres=nsigma_thres,photometry=photometry)
