#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 14:51:04 2025

@author: vaubaill
"""
import os
import logging
import matplotlib.pyplot as plt 
import astropy.units as u
from astropy.time import Time,TimeDelta
import astropy.table as table
import re
from collections import defaultdict

from momet_log import log

def read_rms_ftp_file(file_path):
    """Read FTP file created by RMS and return meteor data.

    Parameters
    ----------
    file_path : TYPE
        DESCRIPTION.

    Returns
    -------
    meteor_tables: list of astropy.table.Table.
        Data of meteor.

    """
    # Read the file
    with open(file_path, 'r') as f:
        lines = f.readlines()
    
    # The header and metadata part of the file is before "Per segment:"
    start_data_index = None
    for i, line in enumerate(lines):
        if line.startswith("Per segment:"):
            start_data_index = i + 1  # The data starts right after this line
            break
    
    # Extract the actual tabular data lines (after the header section)
    data_lines = lines[start_data_index:]
    
    # Prepare the list of columns you're interested in
    columns = ['Frame#', 'Col', 'Row', 'RA', 'Dec', 'Azim', 'Elev', 'Inten', 'Mag']
    
    # Regex pattern to match the data rows, skipping the first line of each meteor
    #data_pattern = re.compile(r'(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)')
    data_pattern = re.compile(r'(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+([+-]?\d+\.\d+)\s+([+-]?\d+\.\d+)\s+([+-]?\d+\.\d+)\s+([+-]?\d+\.\d+)\s+(\d+)\s+(\d+\.\d+)')
    
    # Initialize a list to store all the tables for each meteor
    meteor_tables = []
    
    # store meteor maximum intensity times
    meteor_timax = []

    # Temporary list to store data for the current meteor
    current_meteor_data = {col: [] for col in columns}
    fps_value = None
    
    # Process each data line
    log.debug('=========== Entering line loop =======')
    for line in data_lines:
        log.debug('line: '+line.strip())
        # get time
        m = re.match('FF_FR[0-9]{4}_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_.*fits',line)
        if m:
            log.debug('Match FF_FR : '+line)
            log.debug('Match Time')
            # mise en forme de time_str pour être en format ISOT
            time_str = m.group(1)+'-'+m.group(2)+'-'+m.group(3)+'T'+m.group(4)+':'+m.group(5)+':'+m.group(6)
            # conversion de la chaine de caractère en Time()
            time = Time(time_str,format='isot', scale = 'utc')
            log.debug('time: '+time.isot)
            continue
        
        # Look for the lines that contain fps data (e.g., FR1001 0001 0011 0025.00 000.0 ...)
        # match = re.match(r'FR\d+\s+(\d+)\s+(\d+)\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)', line.strip())
        match = re.match(r'FR\d+\s+(\d+)\s+(\d+)\s+(\d+\.\d+)\s+([\d\.\-]+)\s+([\d\.\-]+)\s+([\d\.\-]+)\s+([\d\.\-]+)\s+([\d\.\-]+)\s+([\d\.\-]+)', line.strip())

        if match:
            log.debug('Match FR => fps')
            # Extract FPS value (4th value from the line)
            fps_value = float(match.group(3))  # FPS value is the 4th entry
            log.info('fps_value: '+str(fps_value))
            # compute delta time per frame
            dt = TimeDelta((1.0/fps_value)*u.s)
            log.info('dt: '+str(dt.to('s')))
            continue
        
        match = data_pattern.match(line.strip())
        if match:
            log.debug('data detected! Appending current_meteor_data')
            # Append the matched data to respective columns
            current_meteor_data['Frame#'].append(float(match.group(1)))
            current_meteor_data['Col'].append(float(match.group(2)))
            current_meteor_data['Row'].append(float(match.group(3)))
            current_meteor_data['RA'].append(float(match.group(4)))
            current_meteor_data['Dec'].append(float(match.group(5)))
            current_meteor_data['Azim'].append(float(match.group(6)))
            current_meteor_data['Elev'].append(float(match.group(7)))
            current_meteor_data['Inten'].append(float(match.group(8)))
            current_meteor_data['Mag'].append(float(match.group(9)))
       
        # Check if the line looks like a new meteor's data (e.g., if it contains "FF" as in your example)
        #if 'FF_' in line:
        if '----------------------------------' in line:
            # If we are in the middle of collecting data for a meteor, store it
            log.debug('match separation')
            log.debug('len(current_meteor_data[Frame#]): '+str(len(current_meteor_data['Frame#'])))
            if len(current_meteor_data['Frame#']) > 0:  # Ensure there's data to save
                log.info('Storing Meteor data')
                meteor_table = table.Table(current_meteor_data)
                # set table units and format
                meteor_table['Col'].info.unit = 'pixel'
                meteor_table['Row'].info.unit = 'pixel'
                meteor_table['RA'].info.unit = 'deg'
                meteor_table['Dec'].info.unit = 'deg'
                meteor_table['Azim'].info.unit = 'deg'
                meteor_table['Elev'].info.unit = 'deg'
                meteor_table['Inten'].info.unit = 'ADU'
                meteor_table['Mag'].info.unit = 'mag'

                # add time in Table
                meteor_table['Time'] = time + dt * meteor_table['Frame#']
                meteor_table['rel_Time'] = (meteor_table['Time'] - meteor_table['Time'][0]).to('s')
                meteor_table['rel_Time'].info.unit = 's'
                meteor_table['rel_Time'].info.format = '5.3f'
                log.info(meteor_table)
                # get time of maximum intensity
                loc = meteor_table['Inten'].argmax()
                timax = meteor_table['Time'][loc]
                log.info('time max: '+timax.isot)
                meteor_tables.append(meteor_table)
                meteor_timax.append(timax)
                
                # plot light curve
                fig=plt.figure()
                plt.plot(meteor_table['rel_Time'],meteor_table['Mag'], 'rx')
                plt.gca().invert_yaxis()
                plt.xlabel('Relative time [s]')
                plt.ylabel('Apparent Magnitude')
                plt_file = os.path.dirname(file_path) + '/' + timax.isot.replace('-','').replace(':','').replace('.','')  + '-LC.png'
                plt.savefig(plt_file,dpi=300)
                plt.close(fig)
                log.info('LC saved in '+plt_file)
                # Reset for the next meteor
                log.info('current_meteor_data reset')     
                current_meteor_data = {col: [] for col in columns}      
    log.debug('=========== End of line loop =======')
    
    # Store the last meteor if any data was collected
    if len(current_meteor_data['Frame#']) > 0:
        log.warning('*** Last Meteor data!!!')
        meteor_table = table.Table(current_meteor_data)
        # set table units and format
        meteor_table['Col'].info.unit = 'pixel'
        meteor_table['Row'].info.unit = 'pixel'
        meteor_table['RA'].info.unit = 'deg'
        meteor_table['Dec'].info.unit = 'deg'
        meteor_table['Azim'].info.unit = 'deg'
        meteor_table['Elev'].info.unit = 'deg'
        meteor_table['Inten'].info.unit = 'ADU'
        meteor_table['Mag'].info.unit = 'mag'
        meteor_table['Mag'].info.format = '4.2f'
        # add time in Table
        meteor_table['Time'] = time + dt * meteor_table['Frame#']
        meteor_table['rel_Time'] = (meteor_table['Time'] - meteor_table['Time'][0]).to('s')
        meteor_table['rel_Time'].info.unit = 's'
        meteor_table['rel_Time'].info.format = '5.3f'
        log.info(meteor_table)
        # get time of maximum intensity
        loc = meteor_table['Inten'].argmax()
        timax = meteor_table['Time'][loc]
        log.info('time max: '+timax.isot)
        meteor_tables.append(meteor_table)
        meteor_timax.append(timax)
        # plot light curve
        fig=plt.figure()
        plt.plot(meteor_table['rel_Time'],meteor_table['Mag'], 'rx')
        #plt.gca().invert_yaxis()
        plt.xlabel('Relative time [s]')
        plt.ylabel('Apparent Magnitude')
        plt_file = os.path.dirname(file_path) + '/' + timax.isot.replace('-','').replace(':','').replace('.','')  + '-LC.png'
        plt.savefig(plt_file,dpi=300)
        plt.close(fig)
        log.info('LC saved in '+plt_file)
        # Reset for the next meteor
        current_meteor_data = {col: [] for col in columns}               
   
    # log.info out each meteor's table
    # for ((i, meteor_table), timax) in zip(enumerate(meteor_tables),meteor_timax):
    #     log.info(f"Table for Meteor {i + 1}:")
    #     log.info(meteor_table)
    #     log.info('Time of maximum: '+timax.isot)
    #     log.info()
    log.info('======= end of FTP file reading ======')
    return meteor_tables,meteor_timax

# file_path = '/media/sf_Volumes/Transcend/MoMet1/CAM1/FR1001_20241212_202729_675317/FTPdetectinfo_FR1001_20241212_202729_675317.txt'
# meteor_tables,meteor_timax=read_rms_ftp_file(file_path)
# log.info(meteor_tables)