"""Definition of a filter Object

"""

import astropy.units as u

from missionplanner.utils import log

class Filter(object):
    # Brand name
    @property
    def brand(self):
        return self.__brand
    @brand.setter
    def brand(self,brand):
        self.__brand = brand
    
    # Filter name
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,name):
        self.__name = name
    
    # minimum cutoff wavelength
    @property
    def min_wavelength(self):
        return self.__min_wavelength
    @min_wavelength.setter
    def min_wavelength(self,min_wavelength):
        self.__min_wavelength = min_wavelength
    
    # maximum cutoff wavelength
    @property
    def max_wavelength(self):
        return self.__max_wavelength
    @max_wavelength.setter
    def max_wavelength(self,max_wavelength):
        self.__max_wavelength = max_wavelength
    
    # minimum cutoff percentage
    @property
    def min_passpercent(self):
        return self.__min_passpercent
    @min_passpercent.setter
    def min_passpercent(self,min_passpercent):
        self.__min_passpercent = min_passpercent
    
    # maximum cutoff percentage
    @property
    def max_passpercent(self):
        return self.__max_passpercent
    @max_passpercent.setter
    def max_passpercent(self,max_passpercent):
        self.__max_passpercent = max_passpercent
    
    # know
    @property
    def known(self):
        return self.__known
    @known.setter
    def known(self,known):
        self.__known = known
    
    def __init__(self,brand='Unknown',name='Unknown',min_wavelength=0.0*u.um,max_wavelength=99.9*u.um,min_passpercent=0.0,max_passpercent=1.0):
        # set brand and name
        self.brand = brand
        self.name = name
        
        # check if known Filter
        self.check_known()
        
        if not self.known:
            self.min_wavelength = min_wavelength
            self.max_wavelength = max_wavelength
            self.min_passpercent = min_passpercent
            self.max_passpercent = max_passpercent
            
        return
    
    def info(self):
        """log information about the Filter.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('----- Lens info --- ')
        log.info('Brand:           '+self.brand)
        log.info('Name:            '+self.name)
        log.info('min_wavelength:  '+str(self.min_wavelength.to('um')))
        log.info('max_wavelength:  '+str(self.max_wavelength.to('um')))
        log.info('min_passpercent: '+str(self.min_passpercent))
        log.info('max_passpercent: '+str(self.max_passpercent))
        return
    
    
    def check_known(self):
        """Check if the filter is known.
        
        If it is, focal lenght f and Nture N are set and True is returned.
        Otherwise return False.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        known : boolean
            True if the lens is known, False otherwise.
        
        """
        self.known = False
        
        if self.brand=='EdmundOptics':
            if self.name=='66670':
                self.min_wavelength =  450.0 * u.nm
                self.max_wavelength = 2000.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
            if self.name=='66653':
                self.min_wavelength =  510.0 * u.nm
                self.max_wavelength = 1050.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
            if self.name=='66675':
                self.min_wavelength =  725.0 * u.nm
                self.max_wavelength = 2000.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
            if self.name=='36640':
                self.min_wavelength = 530.0 * u.nm
                self.max_wavelength = 534.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
            if self.name=='36641':
                self.min_wavelength = 778.0 * u.nm
                self.max_wavelength = 782.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
            if self.name=='36642':
                self.min_wavelength = 1062.0 * u.nm
                self.max_wavelength = 1066.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
            if self.name=='36643':
                self.min_wavelength = 1548.0 * u.nm
                self.max_wavelength = 1552.0 * u.nm
                self.min_passpercent = 0.0
                self.max_passpercent = 1.0
                self.known = True
        elif self.brand=='None':
            self.min_wavelength =  350.0 * u.nm
            self.max_wavelength = 2000.0 * u.nm
            self.min_passpercent = 0.0
            self.max_passpercent = 1.0
            self.known = True
            
        # log
        log.debug('known: '+str(self.known))
        return self.known
