"""Compute the expected sky brightness observed by a given camera in given conditions.

"""
import os
import numpy as np
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable, Column
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation
import logging
import matplotlib.pyplot as plt

from bands import bands
from filter import Filter
from reduction.lens import Lens
from reduction.camera import Camera

from onera_skybrightness import get_onera_data,get_onera_data_per_lambda

import logging
fig=plt.figure()

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


# set time
time = Time('2020-06-21T12:00:00',scale='utc')
# location
eloc = EarthLocation(lat=43.7064*u.deg, lon=-0.2512*u.deg, height=28000.0*u.m)
az = 175.0*u.deg
el = 45.0*u.deg
# set min/max wavelength to consider
lambda_min = 0.4 *u.um
lambda_max = 1.0 *u.um

# create Filter object
#filtre = Filter(brand='None')
#filtre = Filter(brand='EdmundOptics',name='66675')
filtre = Filter(brand='EdmundOptics',name='36640')
# create Lens Object : value for the 201708 flight
lens = Lens(brand='Pentax',name='12mm-1.4')
# create Camera Object : value for the 201708 flight
cam = Camera(brand='Basler',name='acA1300-60gmNIR',lens=lens)
# camera settings
cam.exp_time = 1/30.0 *u.s
cam.nbbit = 8
cam.info()

# get ONERA file to read
onera_data = get_onera_data_per_lambda(time,eloc.height,lambda_min,lambda_max)
log.debug(str(onera_data))
plt.plot(onera_data['wavelength'].to('um').value,onera_data['radiance2'].value,'b-') # 'r+'
plt.title('Sky Radiance vs lambda')
plt.xlabel('lambda [um]')
plt.ylabel('Radiance [ph/[m2/s/sr]]')
plt.show()

# create mask to take into account filter
mask_filter_min = np.array((onera_data['wavelength'] >= filtre.min_wavelength))
mask_filter_max = np.array((onera_data['wavelength'] <= filtre.max_wavelength))
mask_filter = np.logical_and(mask_filter_min,mask_filter_max)

# interpolate camera QE for the whole ONERA data
QE = cam.QEfunc(onera_data['wavelength'].to('um').value) * u.electron / u.ph
# select only positive QE values
mask_QE = np.array(QE > 0.0)
#tmp = np.ma.array(np.ones(len(onera_data['wavelength'])),mask=mask_QE)
QE_eff = np.ma.array(QE,mask=np.logical_not(mask_QE))
QE_effilt = np.ma.array(QE,mask=np.logical_not(mask_filter))
plt.plot(onera_data['wavelength'].to('um').value,QE,'g-') # 'r+'
plt.plot(onera_data['wavelength'].to('um').value,QE_eff,'b-') # 'r+'
plt.plot(onera_data['wavelength'].to('um').value,QE_effilt,'r-') # 'r+'
plt.title('Quantum Efficiency vs lambda')
plt.xlabel('lambda [um]')
plt.ylabel('QE [e-/ph]')
plt.show()


# put all masks together
mask_all = np.logical_not(np.logical_and(mask_filter,mask_QE))
radiance2_eff = np.ma.array(onera_data['radiance2'].value,mask=mask_all)
plt.plot(onera_data['wavelength'].to('um').value,onera_data['radiance2'].value,'b-') # 'r+'
plt.plot(onera_data['wavelength'].to('um').value,radiance2_eff,'r-') # 'r+'
plt.title('(filtered) Radiance vs lambda')
plt.xlabel('lambda [um]')
plt.ylabel('Radiance [ph/ [m2/s/sr]]')
plt.show()

# compute number of photons for each wavelength, given lens and filter features
num_ph     = onera_data['radiance2'] * cam.lens.area.to('m2') * cam.pix_sky_area.to('sr') * cam.exp_time
num_ph_eff = np.ma.array(num_ph,mask=mask_all)
log.debug('num_ph     : '+str(num_ph))
log.debug('num_ph_eff : '+str(num_ph_eff))
plt.plot(onera_data['wavelength'].to('um').value,num_ph,'b-') # 'r+'
plt.plot(onera_data['wavelength'].to('um').value,num_ph_eff,'r-') # 'r+'
plt.title('Photons vs lambda')
plt.xlabel('lambda [um]')
plt.ylabel('Photons at sensor per exposure [ph]')
plt.show()

# compute number of electrons for each wavelength, given QE curve
#num_el = np.ma.array(num_ph * QE,mask=mask_all)
num_el = num_ph     * QE
num_el_eff = np.ma.array(num_el,mask=mask_all)
log.debug('num_el     : '+str(num_el))
log.debug('num_el_eff : '+str(num_el_eff))
plt.plot(onera_data['wavelength'].to('um').value,num_el,'b-') # 'r+'
plt.plot(onera_data['wavelength'].to('um').value,num_el_eff,'r-') # 'r+'
plt.title('Electrons vs lambda')
plt.xlabel('lambda [um]')
plt.ylabel('Electrons per pixel per exposure [ph]')
plt.show()

# compute signal, given camera settings
num_adu     = num_el / cam.sys_gain
num_adu_eff = np.ma.array(num_adu,mask=mask_all)
log.debug('cam.sys_gain  : '+str(cam.sys_gain))
log.debug('num_adu     : '+str(num_adu))
log.debug('num_adu_eff : '+str(num_adu_eff))
plt.plot(onera_data['wavelength'].to('um').value,num_adu,'b-') # 'r+'
plt.plot(onera_data['wavelength'].to('um').value,num_adu_eff,'r-') # 'r+'
plt.title('Signal vs lambda')
plt.xlabel('lambda [um]')
plt.ylabel('Signal [ADU]')
plt.show()


num_el_tot      = np.sum(num_el)
num_el_tot_eff  = np.sum(num_el_eff)
num_adu_tot     = num_el_tot     / cam.sys_gain
num_adu_tot_eff = num_el_tot_eff / cam.sys_gain

log.info('num_el_tot      = '+str(num_el_tot))
log.info('num_el_tot_eff  = '+str(num_el_tot_eff))
log.info('num_adu_tot     = '+str(num_adu_tot))
log.info('num_adu_tot_eff = '+str(num_adu_tot_eff))

