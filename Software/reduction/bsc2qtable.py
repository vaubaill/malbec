"""
07/02/2014
Author : FRIPON
Copyright : Observatoire de Paris, FRIPON
Free license
"""
import ephem
import numpy as np
from astropy.table import QTable, Column
from astropy.coordinates import SkyCoord,AltAz
import astropy.units as u

prog='(bsc2qtable_) '

LimMag=2.0

cat_file='config/bsc5.dat'
cat_file_out=cat_file.replace('.dat','-qtable-sub'+str(LimMag)+'.dat')
star_cat = QTable(names=['RA','DEC','Mag','bv','ri','pmra','pmdec'])


print prog,'Now reading catalog file : ',cat_file
data = QTable.read(cat_file,format='ascii.fixed_width_no_header',col_starts=[75,77,79,83,84,86,88,90,103,107,109,114,121,126,148,154,160],
                       names=['RAh','RAm','RAs','DEsgn','DEd','DEm','DEs','t1','Mag','t2','bv','ri','t3','t4','pmra','pmdec','t7'])
cat_data = QTable(data,masked=True)
locmag=np.where(cat_data['Mag']<LimMag)
if (np.asarray(locmag).size==0):
    sys.exit(''.join([prog,'*** FATAL ERROR: There is no star with mag<',str(max_mg),' in ',cat_file]))
cat_data = cat_data[locmag]

for (rah,ram,ras,desgn,ded,dem,des,mask,mag,bv,ri,pmra,pmdec) in zip(cat_data['RAh'],cat_data['RAm'],cat_data['RAs'],cat_data['DEsgn'],cat_data['DEd'],cat_data['DEm'],cat_data['DEs'],cat_data['RAh'].mask,cat_data['Mag'],cat_data['bv'],cat_data['ri'],cat_data['pmra'],cat_data['pmdec']):
   if not mask:
       skycoostr = str(rah).zfill(2) +':'+ str(ram).zfill(2)+':' +str(ras)+' ' +str(desgn)+str(ded).zfill(2)+':'+str(dem).zfill(2)+':'+str(des).zfill(2)
       skycoo = SkyCoord(skycoostr,unit=(u.hourangle,u.deg))
       star_cat.add_row([skycoo.ra,skycoo.dec,mag,bv,ri,pmra,pmdec]) 



# set units
star_cat['RA'].unit='deg'
star_cat['DEC'].unit='deg'
star_cat['Mag'].unit='mag'
star_cat['bv'].unit='mag'
star_cat['ri'].unit='mag'
star_cat['pmra'].unit='arcsec/yr'
star_cat['pmdec'].unit='arcsec/yr'

star_cat.write(cat_file_out,format='ascii.fixed_width_two_line',delimiter=' ',overwrite=True)
print prog,'Reference star sub-catalog saved in ',cat_file_out
