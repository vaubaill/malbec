# -*- coding: utf8 -*-
################################################################################
# Malbec Block Migration Utility
#-------------------------------------------------------------------------------
# This program is automatically launched by the Malbec main process
#-------------------------------------------------------------------------------
# Version: 3g
# Last modification: 2019-05-27
################################################################################
"""
Copyright (c) ScotopicLabs / IMCCE 2019
This file is part of the Malbec project.
"""
import os
import sys
from datetime import datetime, date, time, timedelta
import numpy as np
import pandas as pd
from astropy.io import fits
from PIL import Image

#-------------------------------------------------------------------------------
# Parameters to set before execution

# Exact date and time of the first image in the first block
# stamp_start = '2019-02-19 09:55:05.199000'

# Dictionary to store exposure times
dic_exposures  = {}

# List of GPS date recording
list_gps_dates = []

# List of GPS location recording
list_gps_locs = []

# List of BME date recording
list_bme_dates = []

# List of GPS location recording
list_bme_tpus = []

# List of block index to process ([] means all blocks)
list_block_index = []

# Folder where blocks can be found
#block_folder = 'D:/tmp/Malbec/Vol'
#block_folder = 'D:/ScotopicLabs/Projets/Malbec/Malbec 3/Source/Test processing'
block_folder = 'E:/Malbec/2019-05-23 Mission/Images blocks'

# Folder where output images must be put
#image_folder = 'D:/tmp/Malbec/Vol'
#image_folder = 'D:/ScotopicLabs/Projets/Malbec/Malbec 3/Source/Test processing'
image_folder = 'E:/Malbec/2019-05-23 Mission/Images blocks/fits'

# Folder where logs can be found
log_folder = 'E:/Malbec/2019-05-23 Mission/Logs'

# Format to be used for saving images ('png' or 'fits')
output_format = 'fits'

# Flag to specify if we use GPS data
use_gps = False

# Image format
# Basler acA2000 - 165umNIR 
h = 1088
w = 2048
# ImagingSource DMK 33UX174
#h = 1200
#w = 1920

#-------------------------------------------------------------------------------
# Global variables

# Header of the stamp files
stamp_columns = ['block_num', 'num', 'camera_t', 'clock_t']

# Image counter
img_count = 0

# Millisecond clock for the first image of all blocks
clk_start = -1

#-------------------------------------------------------------------------------
def process_block(block_file, num_block):
    """Extract all FITS files from an image block"""
    global img_count
    global clk_start

    raw_path = block_folder + '/' + block_file
    stamp_file = os.path.splitext(os.path.basename(block_file))[0] + '.stamps'
    stamp_path = block_folder + '/' + stamp_file
    
    # Read the full block in memory
    block_img = np.fromfile(raw_path, dtype=np.uint8)
    
    # Get the number of images in the block
    nb_images_block = int(block_img.shape[0] / payload)
    
    # Reshape the block taking into account width and height
    try:
        block_img = np.reshape(block_img, (nb_images_block, h, w))
    except:
        print('Error: block %s does not have the right shape'%block_file)
        return
    
    # Read the timestamp file
    block_stamps = pd.read_csv(stamp_path, sep='\t', header=None, names=stamp_columns)

    # Allocate the FITS HDU object
    if output_format == 'fits':
        hdu = fits.PrimaryHDU()

    # Iterate through all the images in the block
    print('Processing image block %s (%d images)'%(block_file, nb_images_block))
    for i in range(nb_images_block):
        # Increment image counter
        img_count += 1
        # Get the image raw data
        data = block_img[i]
        # Get the time in ms from the first image
        if clk_start < 0:
            clk_start = block_stamps['clock_t'].iloc[i]
            clk_current = 0
        else:
            clk_current = block_stamps['clock_t'].iloc[i] - clk_start
        # Build image root name under the form img_<nnnnnn>_<ms> where
        # <nnnnnn> is a sequential number and <ms> is the time in ms since the
        # first image on 8 digits
        image_root = 'img_%06d_%08d'%(img_count, clk_current)
        # Get the date of the image
        dt_current = dt_start + timedelta(milliseconds=np.float64(clk_current))
        # Produce the output file
        if output_format == 'png':
            img = Image.frombytes('L', (w, h), data)
            img.save(image_folder + '/' + image_root + '.png')
        elif output_format == 'fits':
            hdu.data = data
            hdu.header['DATE-OBS'] = dt_current.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            hdu.header['EXPTIME'] = dic_exposures[num_block]
            hdu.header['GAIN'] = gain
            if use_gps:
                # Find the GPS location to be used
                date_in_list = min(list_gps_dates, key=lambda d: abs(d - dt_current))
                location = list_gps_locs[list_gps_dates.index(date_in_list)]
                hdu.header['GEOLAT'] = location[0]
                hdu.header['GEOLON'] = location[1]
                hdu.header['GEOALT'] = location[2]
            # Find the BME data to be used
            date_in_list = min(list_bme_dates, key=lambda d: abs(d - dt_current))
            tpu = list_bme_tpus[list_bme_dates.index(date_in_list)]
            hdu.header['AMBTEMP'] = tpu[0]
            hdu.header['ATMPRESS'] = tpu[1]
            hdu.header['RELHUM'] = tpu[2]
            hdu.writeto(image_folder + '/' + image_root + '.fits', overwrite=True)

# Get the datetime object of the starting image as well as the gain
#-------------------------------------------------------------------------------
if not os.path.exists(log_folder + '/malbec.log'):
    print('Error: log file malbec.log cannot be found')
    sys.exit(1)
f_log = open(log_folder + '/malbec.log', 'r')
lines = f_log.readlines()
f_log.close()
num_line = 0
stamp_start = ''
gain = -1
for line in lines:
    num_line += 1
    line = line.strip()
    if 'Gain' in line:
        gain = int(line.split('=', 1)[1].strip().split(' ', 1)[0])
    if '******** Block start: #0000 at ' in line:
        stamp_start = line.split('[', 1)[1].split(']', 1)[0]
        if gain != -1:
            break
if stamp_start == '':
    print('Error: start stamp not found in malbec.log')
    sys.exit(1)
if gain == -1:
    print('Error: gain not found in malbec.log')
    sys.exit(1)
print('Start stamp: %s'%stamp_start)
dt_start = datetime.strptime(stamp_start, '%Y-%m-%d %H:%M:%S.%f')

# Image payload
payload = w * h

# We read the log file to memorize the exposure times for all the blocks
#-------------------------------------------------------------------------------
if not os.path.exists(log_folder + '/malbec.log'):
    print('Error: log file malbec.log cannot be found')
    sys.exit(1)
f_log = open(log_folder + '/malbec.log', 'r')
lines = f_log.readlines()
f_log.close()
num_line = 0
cur_block = 0
for line in lines:
    num_line += 1
    line = line.strip()
    if 'Using starting exposure: ' in line:
        cur_exp = float(line.split('Using starting exposure: ', 1)[1].split(' s', 1)[0])
    elif '******** Block start: #' in line:
        cur_block = int(line.split('******** Block start: #', 1)[1].split(' at ')[0])
        dic_exposures[cur_block] = cur_exp
    elif 'Exposure time to be used: ' in line:
        cur_exp = float(line.split('Exposure time to be used: ', 1)[1].split('s', 1)[0])

# We read the GPS log file to memorize the location and elevation at all times
#-------------------------------------------------------------------------------
if use_gps:
    if not os.path.exists(log_folder + '/gps_acq.log'):
        print('Error: log file gps_acq.log cannot be found')
        sys.exit(1)
    f_log = open(log_folder + '/gps_acq.log', 'r')
    lines = f_log.readlines()
    f_log.close()
    num_line = 0
    for line in lines:
        num_line += 1
        line = line.strip()
        if 'GPS location: ' in line:
            data = line.split(' ')
            str_date = data[0] + ' ' + data[1]
            list_gps_dates.append(datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S'))
            str_latitude = data[4]
            str_longitude = data[5]
            str_altitude = data[6]
            list_gps_locs.append([float(str_latitude), float(str_longitude), int(str_altitude)])

# We read the BME log file to memorize the location and elevation at all times
#-------------------------------------------------------------------------------
if not os.path.exists(log_folder + '/bme_acq.log'):
    print('Error: log file bme_acq.log cannot be found')
    sys.exit(1)
f_log = open(log_folder + '/bme_acq.log', 'r')
lines = f_log.readlines()
f_log.close()
num_line = 0
for line in lines:
    num_line += 1
    line = line.strip()
    if 'BME 280 reading' in line:
        data = line.split(' ')
        str_date = data[0] + ' ' + data[1]
        list_bme_dates.append(datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S'))
        str_temperature = data[5]
        str_pressure = data[6]
        str_humidity = data[7]
        list_bme_tpus.append([float(str_temperature), float(str_pressure), float(str_humidity)])

# We iterate through all the blocks. We do it sequentially to guarantee the
# correct order
#-------------------------------------------------------------------------------
for i_file in range(1000):
    block_file = block_folder + '/block%04d.raw'%i_file
    if os.path.exists(block_file):
        if list_block_index == [] or i_file in list_block_index:
            process_block('block%04d.raw'%i_file, i_file)
print('Conversion process done')
