"""
installation:
 sudo pip install astropy
 sudo pip install simplekml

syntax:
 python cor2kml file_name



"""
import os
import sys
import csv
import astropy.io.ascii as ascii
from astropy.time import Time as Time
from astropy.table import QTable
from astropy import units as u
import simplekml

# initializations
prog	= '(csv2kml.py) '

# get input file name
filein=sys.argv[1]

# check if file exists
if not os.path.isfile(filein):
    sys.exit(prog+'*** FATAL ERROR: file '+filein+' does not exist')

# create output file name.
if os.path.dirname(filein):	# Solve the slash bar syndrome
    slashbar = '/'
else:
    slashbar = ''
filout_kml=os.path.dirname(filein)+slashbar+os.path.basename(filein).split('.')[0]+".kml"
filout_dat=os.path.dirname(filein)+slashbar+os.path.basename(filein).split('.')[0]+".dat"

alltime=[]
alllon=[]
alllat=[]

with open(filein, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        if (len(row)==1):
            continue
        datestr = row[0]
        [hourstr,latstr,lonstr,tmp,tmpp]=row[1].split(';')
        [mm,dd,yyyy]=datestr.split('/')
        time=yyyy+'-'+mm+'-'+dd+'T'+hourstr
        lat=float(latstr.replace(',','.'))
        lon=float(lonstr.replace(',','.'))
        alltime.append(time)
        alllon.append(lon)
        alllat.append(lat)
dim=len(alllat)
times=Time(alltime)
data=QTable([times,alllon,alllat],names=['Time','Latitude','Longitude'])
ascii.write(data,filout_dat,format='fixed_width',delimiter=' ')
print prog,dim," data saved in ",filout_dat

# convert into kml data and save into output file
kml = simplekml.Kml()
coords = []
for (t,lon,lat) in zip(data['Time'],data['Longitude'],data['Latitude']):
  coords.append((lon,lat,0.0))
  pnt = kml.newpoint(name="",coords=[(lat,lon,0.0)],extrude=1)
  pnt.altitudemode = simplekml.AltitudeMode.relativetoground 
  pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
  pnt.timestamp.when=t.isot
lin = kml.newlinestring(name="Path", description="Balloon path",coords=coords)
lin.altitudemode = simplekml.AltitudeMode.absolute
kml.save(filout_kml)
print prog,dim," data saved in ",filout_kml

