# goal: compute twilight as a function of altitude

 
import numpy as np
import astropy
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.coordinates import get_sun
from astropy.table import QTable
import astropy.constants as cst
import matplotlib.pyplot as plt
import itertools
from scipy import interpolate

# user part
vert_speed	= 4.5 *u.m/u.s							# vertical speed of the balloon [m/s]
flight_duration	= 2.0 *u.h							# flight ascending phase duration [hours]
time_launch	= Time('2017-8-12 03:08:00')					# Balloon launch time [UT]
time_start	= Time('2017-8-12 03:00:00')					# start time [UT]
time_step_init	= 30.0*u.min							# time step [min]
time_minstep	= 0.5*u.min							# time tolerance [min]
altsun_ref	= [-18.0,-12.0,0.0]*u.deg 					# reference Sun altitude to compute time for
altsun_ref	= [0.0]*u.deg 					# reference Sun altitude to compute time for
site0		= EarthLocation(lat=43.707311*u.deg, lon=-0.251529*u.deg)	# reference site at 0m of altitude

# initializations
prog		= '(twilight_search) '
R_earth		= (1.0*u.R_earth).to('km')
R_air		= 287.0*u.J/u.K/u.kg

# read Earth atmosphere model
atm_file	= '/home/vaubaill/PROJECTS/PODET/PODET-MET/ADMIRE/AFM/McAuliffe/atmospheres/Earth_atmos.txt'
print prog,'now reading atmosphere file=',atm_file
data		= astropy.io.ascii.read(atm_file,data_start=2)
Qdata		= astropy.table.QTable(data=data,names=['alt','rho','T'])
Qdata.replace_column('alt',Qdata['alt'].astype(float)*u.m)
Qdata.replace_column('rho',Qdata['rho']*u.kg/u.m/u.m/u.m)
Qdata.replace_column('T',Qdata['T']*u.K)

# start calculation
print prog,'=== Now computing time of Sun-elevation ocurrence for site=',site0.lon.to('deg'),site0.lat.to('deg')
# loop over the recquired altitude
for altsun in altsun_ref:
 print prog,'=== now looking for time when Sun is at ',altsun.to('deg'),' from horizon, within ',time_minstep
 all_alt	= np.array([])
 all_time	= np.array([])
 for alt in np.linspace(0,40,5)*u.km: # unit is km
  all_alt	= np.append(all_alt,alt)
  beta		= np.arccos(R_earth/(R_earth+alt))
  # initializations
  time		= time_start
  time_step	= time_step_init
  site		= EarthLocation(lat=43.707311*u.deg, lon=-0.251529*u.deg, height=alt)
  density	= np.interp(alt.to('m').value, Qdata['alt'].value,Qdata['rho'].value, left=Qdata['rho'][0].value, right=Qdata['rho'][-1].value) # kg/m3
  Temp		= np.interp(alt.to('m').value, Qdata['alt'].value,Qdata['T'].value  , left=Qdata['T'][0].value  , right=Qdata['T'][-1].value)   # K
  pressure	= R_air * (density*u.kg/u.m/u.m/u.m) * (Temp*u.K)  	# [Pa]
  altazframe	= AltAz(obstime=time, location=site,pressure=pressure.to('Pa'),temperature=(Temp+273.0)*u.deg_C,obswl=0.5E-06*u.m) # change when astropy is updated
  sunaltazs	= get_sun(time).transform_to(altazframe)
  sunelevation	= sunaltazs.alt+beta			# correct for altitude
  time_current	= time_start	# initialize
  # loop over time to find when Sun is at altsun_ref
  while(np.abs(time_step.to('min').value)>np.abs(time_minstep.to('min').value)):
   old_sunalt	= sunelevation				# save old value
   time		= time + time_step			# change time
   altazframe	= AltAz(obstime=time, location=site)	# compute frame
   sunaltazs	= get_sun(time).transform_to(altazframe)# compute Sun altAz
   sunelevation	= sunaltazs.alt+beta			# correct for altitude
   if (((old_sunalt-altsun)*(sunelevation-altsun)).value < 0.0):
    time_step	= -time_step/2.0
  print 'Height=',round(site.height.to('m').value,0),' T=',round(Temp-273.0,0),'C P=',round(pressure.to('Pa').value,0),' beta=',round(beta.to('deg').value,1), 't=',time,' S_el=',round(sunelevation.to('deg').value,1),' S_az=',round(sunaltazs.az.to('deg').value,1)	# print results
  #all_time	= np.append(all_time,time)
 # now looks for time when balloon enters the considered zone
 #Alt_from_vert_speed	= vert_speed.to('m/s') * (all_time-time_launch).to('s')
 #Alt		=  np.interp((all_time-time_launch).to('h').value,all_alt.to('km').value ,Alt_from_vert_speed.to('s').value) * u.deg	# [deg]
