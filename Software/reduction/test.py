#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 14:15:47 2025

@author: vaubaill
"""
from momet_readftp import read_rms_ftp_file
from momet_log import log



file_path = '/media/sf_Volumes/Transcend/MoMet1/CAM1/FR1001_20241212_202729_675317/FTPdetectinfo_FR1001_20241212_202729_675317.txt'
meteor_tables,meteor_timax=read_rms_ftp_file(file_path)
#log.info(meteor_tables)
log.info('done')