#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 11:58:06 2024

@author: vaubaill
"""
import os
from astropy.table import QTable
import astropy.units as u


# read IAU shower data file
shw_file = self.config['USER']['conf_dir'] + '/streamestablisheddata2022.csv'
if not os.path.exist(shw_file):
    msg = '*** FATAL ERROR: file '+shw_file+' does not exist'
    log.error(msg)
    raise IOError(msg)
iau_data = QTable.read(shw_file,format='ascii.csv')
shower_name = self.config['SHOWER']['shower_name']
mask = iau_data['shower_name']==shower_name
shower_data = iau_data[mask]
if not len(shower_data):
    msg = '*** FATAL ERROR: no data for shower '+shower_name
    log.error(msg)
    raise ValueError(msg)
# get annual component
mask = shower_data['activity']=='annual'
shower_annual = shower_data[mask][-1]
LoS = shower_annual['LoS'][-1]*u.deg