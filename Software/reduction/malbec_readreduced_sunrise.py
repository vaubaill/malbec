import glob
import os
import logging
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats,biweight_midvariance, mad_std
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
#from photutils import datasets, DAOStarFinder, CircularAperture
#from photutils import Background2D, SigmaClip, MedianBackground
from PIL import Image,ImageStat
from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
from astropy.table import QTable
from astropy import units as u
import astropy.io.ascii as ascii
from astropy import constants as const

from reduction.camera import Camera
from reduction.lens import Lens

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# initializations
cam = Camera(brand='Basler',name='acA1920-155um',lens=Lens(brand='Basler',name='12mm-1.6'))
#data_dir = '/media/ccolomer/Elements/DATA/SUNRISE-acA1920-155um-Basler-12mm-1.6-None/'
data_dir = os.getenv('HOME')+'/malbec/DATA/TESTS_CAMERAS/SUNRISE-acA1920-155um-Basler-12mm-1.6-None/'
img_reference_file = data_dir + 'FITS/img_000001_00000000.fits'
data_file = data_dir + 'ALLDATA.dat'
output_file = data_dir + 'skybrightness.dat'

# read an offset image here
if not os.path.exists(img_reference_file):
    msg = '*** FATAL ERROR: file '+img_reference_file+' does not exist'
    log.error(msg)
    raise IOError(msg)
log.info('reading image reference file '+img_reference_file)
hdu     = fits.open(img_reference_file)[0]
x_pixsz = hdu.header['XPIXELSZ'] * u.um
y_pixsz = hdu.header['YPIXELSZ'] * u.um
F       = hdu.header['FOCAL'] * u.mm

# double check that the FITS and the camera info are the same
if not np.abs(x_pixsz.to('um') - cam.xpixsz.to('um'))<0.0001*u.um:
    cam.info()
    msg = '*** FATAL ERROR: Camera x-pixel size '+str(cam.xpixsz.to('um'))+' is different from FITS header XPIXELSZ: '+str(x_pixsz.to('um'))
    log.error(msg)
    raise ValueError(msg)
if not F.to('mm')==cam.lens.F.to('mm'):
    cam.info()
    msg = '*** FATAL ERROR: Camera lens focal lens '+str(cam.lens.F.to('mm'))+' is different from FITS header FOCAL: '+str(F.to('mm'))
    log.error(msg)
    raise ValueError(msg)
    
A_pix   = x_pixsz * y_pixsz         # pixel area in [m^2]
x_skysz = x_pixsz * 360.0 / (2.0* np.pi * F) * u.rad    # pixel x-size projected unto the sky [rad]
y_skysz = y_pixsz * 360.0 / (2.0* np.pi * F) * u.rad    # pixel y-size projected unto the sky [rad]
A_sky   = x_skysz.to('deg') * y_skysz.to('deg')         # pixel size projected unto the sky [deg^2]


# Basler system gain, from documentation
camera_lamba    = 0.6 *  1.0E-06 * u.m          # wavelength [m]
QE              = 0.5 * u.electron / u.photon   # Quantum efficiency at camera_lamba for Basler acA1920-155um
Dark_noise      = cam.dark_noise                # Dark noise

log.info('reading data file '+data_file)
# read an offset image here
if not os.path.exists(data_file):
    msg = '*** FATAL ERROR: file '+data_file+' does not exist'
    log.error(msg)
    raise IOError(msg)
data = QTable.read(data_file,  format='ascii.fixed_width_two_line',names=('Sun_el','BCK','expo','gain'))
data['Sun_el'].unit='deg'
data['BCK'].unit='adu'
data['expo'].unit='s'
data['gain'].unit='electron/adu' # ??? double check this!!!
"""
#data['gain'].unit='dB'

# get effective gain, knowing that the camera is limited by a gain of 23 dB
# this must be converted into multiplication factor
locmaxg = np.where(data['gain'] > cam.max_gain_dB)
if len(locmaxg):
    data['gain'][locmaxg] = np.repeat(0.0,len(locmaxg))*u.dB
#eff_gain = cam.gain_law_dB2analog(data['gain'])
"""
eff_gain = data['gain']

n_electron  = eff_gain * data['BCK'] #  - Dark_noise : useless since images are already dark corrected
n_photons   = n_electron / QE
E           = n_photons/u.photon * const.h * const.c / (A_pix * camera_lamba * data['expo'] ) # irradiance on the sensor surface [W/m2]
E_perdegsq  = E / A_sky # irradiance per deg^2
E_persterad = E_perdegsq.to('W.m-2.sr-1')

# select data that make sense
locpos = np.where(E_perdegsq.value>0.0)

# save data in output file
data_out = QTable([data['Sun_el'][locpos].to('deg'),E_perdegsq[locpos].to('W.m-2.deg-2'),E_persterad[locpos].to('W.m-2.sr-1')],
                  names=['Sun_elevation','E [W.m-2.deg-2]','E [W.m-2.sterad-1]'])
data_out.write(output_file,format='ascii.fixed_width_two_line',overwrite=True)
log.info('Data saved in '+output_file)

# plots Irradiance vs Sun elevation
E_Sunel_plot_file = data_dir + 'ESunel.png'
fig=plt.figure()
plt.plot(data['Sun_el'][locpos],np.log10(E_perdegsq[locpos].to('W.m-2.deg-2').value),'bo',linewidth=1.5)
plt.title('Sun elevation vs Irradiance')
plt.xlabel('Sun elevation [deg]')
plt.ylabel('log(Irradiance) [W/m2/deg2]')
plt.savefig(E_Sunel_plot_file)
plt.close(fig)
log.info('plot saved in '+E_Sunel_plot_file)
#
E_Sunel_plot_file = data_dir + 'ESunel-ste.png'
fig=plt.figure()
plt.plot(data['Sun_el'][locpos],np.log10(E_persterad[locpos].value),'bo',linewidth=1.5)
plt.title('Sun elevation vs Irradiance')
plt.xlabel('Sun elevation [deg]')
plt.ylabel('log(Irradiance) [W/m2/setrad]')
plt.savefig(E_Sunel_plot_file)
plt.close(fig)
log.info('plot saved in '+E_Sunel_plot_file)


# now plot results
# plot Background vs Sun elevation
Bck_sunel_plot_file = data_dir + 'BckgSunel.png'
fig=plt.figure()
plt.plot(data['Sun_el'],data['BCK'],'bo',linewidth=1.5)
plt.title('Background vs Sun elevation')
plt.ylabel('Bckg (ADU)')
plt.xlabel('Sun elevation [deg]')
plt.savefig(Bck_sunel_plot_file)
plt.close(fig)
log.info('Background vs Sun elevation saved in '+Bck_sunel_plot_file)
