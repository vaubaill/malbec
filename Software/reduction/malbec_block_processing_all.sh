data_path="/media/ccolomer/MyPassport/DATA/TESTS_CAMERAS/"


#acA1300-60gmNIR
#img_w="1280"
#img_h="1024"

#camera="Basler_acA1920-155um"
#img_h="1200"
#img_w="1920"

#camera="Basler_acA2000-165umNIR"
#img_h="1088"
#img_w="2048"

#camera="Basler_acA2040-55um"
#img_h="1536"
#img_w="2048"

camera="DMK33UX273"
img_h="1080"
img_w="1440"

#pushd ./
#cd $data_path

for dirname in `ls $data_path | grep $camera`
do
 cmd="python3 ~/malbec/Software/Odroid/Acquisition/malbec_block_processing.py -root_dir=$data_path/$dirname -no_gps -no_bme -img_h=$img_h -img_w=$img_w"
 echo $cmd
 python3 ~/malbec/Software/Odroid/Acquisition/malbec_block_processing.py -root_dir=$data_path/$dirname -no_gps -no_bme -img_h=$img_h -img_w=$img_w
done

echo "All done."

#popd
