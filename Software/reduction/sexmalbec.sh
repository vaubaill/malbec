# Script to perform the astrometry of the MALBEC images
#
# WARNINGS: 
# -THIS PROGRAM MUST BE LAUNCHD AFTER THE DARK+FLAT CORRECTION!!!
# -THIS PROGRAM MAKES USE OF $station.head
#
# This program successively launches:
# -sextractor to extract sources from corrected images (Dark and Flat corrected)
# -psfex to compute the PSF in the image
# -sextractor again, taking into account the PSF
# -scamp to perform the astrometry
# -cab_getbestcontrast.py to get the name of the file having the best contrast
# -copy the latest in cabernet.ahead
#
# syntax: sexmalbec.sh $motif $Lsex $Lpsfex $Lsexpsf $Lscamp $Lnewahead
#
# TODO:
#
#
# user choice to launch which script
prog="(sexmalbec.sh) "
syntax="sexmalbec.sh motif Lsex Lsfex Lsexpsf Lscamp Lnewahead"
motif=$1
Lsex=$2
Lpsfex=$3
Lsexpsf=$4
Lscamp=$5
Lnewahead=$6
(test -z $motif) && motif="no_motif"	
(test -z $Lsex) && Lsex=0
(test -z $Lpsfex) && Lpsfex=0
(test -z $Lsexpsf) && Lsexpsf=0 
(test -z $Lscamp) && Lscamp=0
(test -z $Lnewahead) && Lnewahead=0
echo "motif=$motif"
echo "Lsex=$Lsex"
echo "Lpsfex=$Lpsfex"
echo "Lsexpsf=$Lsexpsf"
echo "Lscamp=$Lscamp"
echo "Lnewahead=$Lnewahead"
#=================================================
#==== START OF THE PROGRAM: DO NOT CHANGE!!! =====
#=================================================
motifsex=".fit"
motifpsfex=".ldac"
motifscamp=".ldac"
# configuration files
workdir="$HOME/PROJECTS/PODET/PODET-MET/MALBEC/Demo2017/code/"
common_path="$workdir/common/"
script_path="$workdir/script/"
procdir="PROCESSED/"
sexdir="SEXRES/"
sexconfile="$common_path/cabernet.sex"
sexparamfile="$common_path/cabernet.param"
sexfilterfile="$common_path/default.conv"
sexmask="$common_path/mask.fits"
PSFdir="PSFRES/"
psfconfile="$common_path/cabernet.psfex"
sexparampsfile="$common_path/cabernet.parampsfex"
scampdir="SCAMPRES/"
scampconfile="$common_path/cabernet.scamp"
scampcatalog="$common_path/HIP-8.ldac"
scampaheadfile="./cabernet_bestsofar.ahead"
#refldac="./cabernet_bestsofar.ldac"
refldac=""

########################################################################
# end of user's choice
#########################################################################
# launches sextractor
listsex=`ls $procdir$motif$motifsex | grep -v obj | grep -v bck | grep -v snap | grep -v chi | grep -v proto | grep -v resi |  grep -v samp `
if (test $Lsex = 1) then  
 echo "$prog================================================="
 echo "$prog============ Now lauching sextractor ============"
 echo "$prog================================================="
 echo "$prog listsex=$listsex"
 for file in $listsex
 do
 (test $Lsex)&& echo "$prog --- Now treating file: $file"
 filename="${file##*/}"
 filebasename="${filename%.*}"
 catalogname="$filebasename.ldac"
 bckgname="$filebasename-bckg.fits"
 objname="$filebasename-obj.fits"
  echo "$prog sex $file -c $sexconfile -PARAMETERS_NAME $sexparamfile -FILTER_NAME $sexfilterfile -WEIGHT_IMAGE $sexmask -CATALOG_NAME $catalogname -CHECKIMAGE_NAME $bckgname,$objname"
              sex $file -c $sexconfile -PARAMETERS_NAME $sexparamfile -FILTER_NAME $sexfilterfile -WEIGHT_IMAGE $sexmask -CATALOG_NAME $catalogname -CHECKIMAGE_NAME $bckgname,$objname
 done
 # now moves the sextractor results into $SEXDIR subdirectory
 (test ! -e $sexdir) && mkdir $sexdir
 mv *-bckg.fits $sexdir
 mv *-obj.fits $sexdir
 mv *.ldac $sexdir
fi


# launches psfex
if (test $Lpsfex = 1) then  
 echo "$prog ================================================="
 echo "$prog ================ Now lauching psfex ============="
 echo "$prog ================================================="
 listpsfex=`ls $sexdir$motif$motifpsfex`
 echo "$prog - - - - - - - - - - - - - - -"
 echo "$prog psfex -c $psfconfile $listpsfex"
             psfex -c $psfconfile $listpsfex
 # now moves psfex results into PSF dir
 (test ! -e $PSFdir) && mkdir $PSFdir
 mv chi*      $PSFdir
 mv proto*    $PSFdir
 mv samp*     $PSFdir
 mv snap*     $PSFdir
 mv resi*     $PSFdir
 mv psfex.xml $PSFdir
fi 

# re-launches sextractor taking into account the psf
if (test $Lsexpsf = 1) then  
 echo "$prog ============================================================="
 echo "$prog ============ Now RE-lauching sextractor with psf ============"
 echo "$prog ============================================================="
 echo "$prog listsex=$listsex"
 for file in $listsex
 do
  filename="${file##*/}"
  filebasename="${filename%.*}"
  psfile="$PSFdir/$filebasename.psf"
  catalogname="$filebasename.ldac"
  bckgname="$filebasename-bckg.fits"
  objname="$filebasename-obj.fits"
   echo "$prog sex $file -c $sexconfile -PSF_NAME $psfile -PARAMETERS_NAME $sexparampsfile -FILTER_NAME $sexfilterfile -WEIGHT_IMAGE $sexmask -CATALOG_NAME $catalogname -CHECKIMAGE_NAME $bckgname,$objname"
               sex $file -c $sexconfile -PSF_NAME $psfile -PARAMETERS_NAME $sexparampsfile -FILTER_NAME $sexfilterfile -WEIGHT_IMAGE $sexmask -CATALOG_NAME $catalogname -CHECKIMAGE_NAME $bckgname,$objname
 done
 mv *-bckg.fits $sexdir/
 mv *-obj.fits  $sexdir/
 mv *.ldac $sexdir/
fi



# launches scamp
if (test $Lscamp = 1) then  
 echo "sexdir=$sexdir motif=$motif motifscamp=$motifscamp sexdirmotifmotifscamp=$sexdir$motif$motifscamp"
 if (test -z $refldac) # $refldac has no value
 then
  echo "cmd=ls $sexdir$motif$motifscamp"
  listscamp=`ls $sexdir$motif$motifscamp` 
 else
  echo "cmd=ls $sexdir$motif$motifscamp | grep -v $refldac"
  listscamp=`ls $sexdir$motif$motifscamp | grep -v $refldac`
 fi
 echo "listscamp=$listscamp  "
 echo "$prog ============================================"
 echo "$prog ============ Now lauching scamp ============"
 echo "$prog ============================================"
 echo "$prog scamp $refldac $listscamp -c $scampconfile -AHEADER_GLOBAL $scampaheadfile -ASTREFCAT_NAME $scampcatalog"
             scamp $refldac $listscamp -c $scampconfile -AHEADER_GLOBAL $scampaheadfile -ASTREFCAT_NAME $scampcatalog
 # now move results into scamp dir
 (test ! -e $scampdir) && mkdir $scampdir
 mv *.png     $scampdir
 mv scamp.xml $scampdir
 mv *.head    $scampdir
fi 

# now determine best contrast file and copy it to default
if (test $Lnewahead = 1) then
 echo "$prog =================================================================="
 echo "$prog ============ Now determining best contrast file =================="
 echo "$prog =================================================================="
 bestcontfile=`python $script_path/cab_getbestcontrast.py`
 echo "$prog bestcontfile=$bestcontfile"
 # case when the best head file is $refldac
 if [[ $bestcontfile == *"bestsofar"* ]]
 then
  echo "$prog *** WARNING: no improvement of best file: $refldac"
 else
  newaheadfile="$sexdir$bestcontfile"
  echo "$prog newaheadfile=$newaheadfile"
  echo "$prog head2ahead.sh $newaheadfile $scampaheadfile"
              head2ahead.sh $newaheadfile $scampaheadfile
 fi
fi

echo "$prog ============================================"
echo "$prog =========== sexmalbec.sh done ============"
echo "$prog ============================================"
