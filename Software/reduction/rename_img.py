#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  2 08:46:22 2022

@author: vaubaill
"""
import os
import glob


imgdir = '/home/vaubaill/OBSERVATIONS/OHPT120/2022-08-01/DARK/'
pattern = 'Flat*'

os.chdir(imgdir)
listimg = glob.glob(pattern)
for imgfile in listimg:
    sfx = imgfile.split('-')[-1]
    newfile = 'Dark'+'-'+sfx
    os.rename(imgfile,newfile)
    print(imgfile+' is being renamed '+newfile)