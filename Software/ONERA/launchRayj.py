"""Make configuration file for Pierre Simoneau Ray_j_n.exe program
and execute the program

"""

import os
import glob
from shutil import copyfile
import subprocess


path = 'C:\\Users\\Soho\\Desktop\\Jerem\\ONERA\\Pierre\\Rayjn2\\'
conf_in = path + 'Rayjn.data_template_OsirisREX'
conf_out = path + 'Rayjn.data'
exe_file = 'Ray_j_n.exe'

#allday = ['21/06/2020   12:00','21/12/2020   12:00','21/03/2020   12:00','21/09/2020   12:00']
allday = ['24/09/2023   15:00']
allalt = ['0','10.0','15.0']
#allaz = ['0.0','90.0','180.0','270.0']
allaz = ['270.0','180.0']
allzen = ['10','20','45.0']

################################################
# first change directory
for day in allday:
    for az in allaz:
        for zen in allzen:
            for alt in allalt:
                os.chdir(path)
                strconf = day.replace('/','').replace(' ','').replace(':','')+'-'+alt+'-'+az+'-'+zen
                tmpdir = path + strconf+'/'
                tmpdir = tmpdir.replace('\\','/')
                print('tmpdir = '+tmpdir)
                # make tp dir
                if not os.path.isdir(tmpdir):
                    os.mkdir(tmpdir)
                # symlink exe and data
                if not os.path.islink(tmpdir+exe_file):
                    os.symlink(path+exe_file,tmpdir+exe_file)
                if not os.path.islink(tmpdir+'DATA'):
                    os.symlink(path+'DATA',tmpdir+'DATA')
                # conf_out
                conf_out = tmpdir + os.path.basename(conf_out)
                with open(conf_in, mode='r') as fin:
                    with open(conf_out, mode='w') as fout:
                        nline = 0
                        for line in fin.readlines():
                            nline = nline+1
                            if nline==1:
                                lineout =  day+'  \t date et heure (TU) \n'
                            elif nline==3:
                                lineout = alt+'        	  \t   altitude de l\'observateur (km) \n'
                            elif nline==4:
                                lineout = zen+'     '+az+'        \t angle zénithal et azimut d\'observation (azimut par rapport au nord) \n'
                            else:
                                lineout = line
                            #print(line)
                            #print(lineout)
                            fout.write(lineout)
                # end of reading/writing config
                print('config saved in '+conf_out)
                # now launch the program
                os.chdir(tmpdir)
                subprocess.Popen(exe_file)
                #os.system(exe_file) #+' > '+tmpdir+'out.txt')
            # prompt user so that the program waits
            print('press a key...')
            loopit = input()
print('All computations done.')
                
        
                                
