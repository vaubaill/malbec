# MALBEC M-IR meteor signal to noise computation

import os
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
from astropy.constants import c, h, k_B
from astropy.table import QTable

from rayjn_log import log

def spectral_luminance(T, wavelength):
    """
    Compute spectral luminance.
    
    Parameters
    ----------
    T: astropy.units.Quantity object
        Temperature, in [K].
    wavelength: astropy.units.Quantity object
        Wavelength, in [m].
    
    Returns
    -------
    spc_lum: astropy.units.Quantity object
        Spectral luminance, in [ph / s.sr.m3].
    
    """    
    spc_lum = (2 * c / (wavelength.to('m')**4 * (np.exp((h * c) / (wavelength.to('m') * k_B * T.to('K')))  - 1))) *  u.ph / u.sr # unit-=[ph / s.sr.m3]
    return spc_lum


def func_snr_SR(wavelength,lens,sensor,meteoroid,orbital_geometry,comp_file='./comp_Rayjn.txt',outdir='./'):
    """
    Computes signal to noise ratio, with formulae provided by Sylvain R. (ONERA).
    
    Parameters
    ----------
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    lens : dict
        Lens parameters:
            Dpup: astropy.units.Quantity object
                Pupil diamater, in [m] or [mm].
            FocL: astropy.units.Quantity object
                Focal lens. unit=[m].
            Nopt: float
                Lens aperture (no unit).
            Topt: float
                Lens transmission (no unit).
    sensor : dict
        Sensor parameters:
            pix_sz: astropy.units.Quantity object
                Detector size, in [m] or [um].
            pix_pitch: astropy.units.Quantity object
                Sampling pitch, in [m] or [um].
            pix_num: integer
                Number of samples, i.e. number of pixels, assuming a square sensor. no unit.
            QE: float
                Quantum efficiency over the whole wavelength band range. no unit.
            exp_time: astropy.units.Quantity object
                Integration time, in [s] or [us].
            pix_T: astropy.units.Quantity object
                Sensor temperature, in [K].
            Idark: astropy.units.Quantity object
                Sensor dark current, in [A] or [pA].
            Rsh: astropy.units.Quantity object
                Dynamic resistance of the photodiode, in [Ohm].
            VSF: astropy.units.Quantity object
                Output amplifier noise, in [V] or [uV].
            CsG: astropy.units.Quantity object
                Storage capacity at the output follower in [F] or [pF].
            Delat_Vs: astropy.units.Quantity object
                Excursion voltage of the output follower, in [V]
            nbbit: integer
                Number of digitization bits (dimensionless).
    meteoroid: dict
        Meteoroid parameters:
            x: astropy.units.Quantity object
                Meteoroid size in x direction, in [m].
            y: astropy.units.Quantity object
                Meteoroid size in y direction, in [m].
            T: astropy.units.Quantity object
                Meteoroid surface temperature, in [K].
            eps: float
                Meteoroid emissivity (dimensionless).
    orbital_geometry: dict
        Orbital geometry parameters.
            met_Vatm: astropy.units.Quantity object
                Meteoroid entry velocity w.r.t Earth., in [km/s] or [m/s].
            radiant_dist: astropy.units.Quantity object
                Angular distance between pointing direction and meteoroid radiant, in [rad] or [deg].
            Lobs: astropy.units.Quantity object
                Physical distance between the camera and the meteoroid, in [m] or [km].
    comp_file string, optional
        Rayjn comp-type file. Default is './comp_Rayjn.xls'.
    outdir: string, optional
        Output directory for plot images. Default is './'.        
    
    Returns
    -------
    snr: float
        Signal to noise ratio (dimensionless).
    
    """
    # TODO: virer !!!
#    os.chdir('/Users/vaubaill/Desktop')
    
    # Store parameters
    # lens parameters
    Dpup = lens['Dpup'].to('m')
    FocL = lens['FocL'].to('m')
    Topt = lens['Topt']
    # wavelength parameters
    lambda_min = wavelength['lambda_min'].to('um')
    lambda_max = wavelength['lambda_max'].to('um')
    # sensor parameters
    pix_sz = sensor['pix_sz'].to('um')
    pix_pitch = sensor['pix_pitch'].to('um')
    pix_num = sensor['pix_num']
    QE = sensor['QE']
    exp_time = sensor['exp_time'].to('s')
    pix_T = sensor['pix_T'].to('K')
    Idark = sensor['Idark'].to('A')
    Rsh = sensor['Rsh'].to('Ohm')
    VSF = sensor['VSF'].to('V')  # Output amplifier noise
    CsG = sensor['CsG'].to('F')  # Storage capacity at the output follower
    Delat_Vs = sensor['Delat_Vs'].to('V')  # Excursion voltage of the output follower
    nbbit = sensor['nbbit']
    # meteoroid parameters
    met_x = meteoroid['x'].to('m')
    met_y = meteoroid['y'].to('m')
    met_T = meteoroid['T'].to('K')
    met_eps = meteoroid['eps']
    # orbital geometry
    met_Vatm = orbital_geometry['met_Vatm'].to('m/s')
    radiant_dist = orbital_geometry['radiant_dist'].to('deg')
    Lobs = orbital_geometry['Lobs'].to('m')

    # input control
    log.debug(str(wavelength))
    log.debug(str(lens))
    log.debug(str(sensor))
    log.debug(str(meteoroid))
    log.debug(str(orbital_geometry))
    
    # Compute necessary quantities
    q = (1*u.eV).to('J').value * u.C # electron electric charge. unit=[C]
    lambda_mean = (lambda_max + lambda_min) / 2  # Wavelength for diffraction calculation
    Nopt = FocL / Dpup  # numerical aperture
    Spup = np.pi * (Dpup / 2)**2  # Area of the pupil
    Spix = pix_pitch**2  # Area of a pixel
    Tirage = FocL  # Assumption for distance from the pupil to the detector
    Gpix = (Spup.to('m2') * Spix.to('m2') / Tirage.to('m')**2) # geometric extent, in [m2]
    omega_pup = np.arctan(Dpup.to('m').value/(2.0*Tirage.to('m').value))
    omega_ciblex = np.arctan(met_x.to('m').value/(2*Lobs.to('m').value))
    omega_cibley = np.arctan(met_y.to('m').value/(2*Lobs.to('m').value))
    Gcible = (4 * np.sin(omega_ciblex) * np.sin(omega_cibley) * Spup).to('m2') # target surface, in [m2]
    met_Vatm_app = met_Vatm * np.sin(radiant_dist.to('rad')) # apparent target velocity, in [km/s]
    log.debug('met_x = '+str(met_x.to('m')))
    log.debug('met_y = '+str(met_y.to('m')))
    log.debug('Lobs = '+str(Lobs))
    log.debug('Nopt = '+str(Nopt))
    log.debug('Spup = '+str(Spup))
    log.debug('Spix = '+str(Spix))
    log.debug('Tirage = '+str(Tirage))
    log.debug('Gpix = '+str(Gpix))
    log.debug('omega_pup = '+str(omega_pup))
    log.debug('omega_ciblex = '+str(omega_ciblex))
    log.debug('omega_cibley = '+str(omega_cibley))
    log.debug('Gcible = '+str(Gcible))
    
    ##### Compute target radiance
    # Define wavelengths range
    wavelengths = np.linspace(lambda_min.value, lambda_max.value, 1000) * u.micron
    # Calculate the spectral radiance at each wavelength
    cible_spectral_luminance = spectral_luminance(met_T, wavelengths)
    # Integrate over the wavelength range to get total radiance
    cible_band_luminance = np.trapz(cible_spectral_luminance, wavelengths.to('m'))
    log.debug('cible_band_luminance = '+str(cible_band_luminance))
   
    
    # Read Rayjn data from comp_Rayjn.txt type file
    log.info('Now read compt-type file: '+comp_file)
    if not os.path.exists(comp_file):
        raise IOError(comp_file+' does not exist')
    colstart = [0,20,44,61,86,116,139,160,188,202,224,260,284,308]
    # colunits = (u.dimensionless_unscaled/u.cm,u.dimensionless_unscaled,u.W/(u.m**2 * u.sr),
    #             u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr),
    #             u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr),
    #             u.W / (u.m**2 * u.sr),u.W / (u.m**2 * u.sr),
    #             u.dimensionless_unscaled,u.dimensionless_unscaled,u.dimensionless_unscaled)
    colnames = [
        'wavenumber', 'transmission', 'thermique', 'diffusion_multiple', 'diffusion_simple', 
        'nightglow', 'stellaire', 'fond_de_sol', 'totale', 'source_exoatmos', 
        'source_transmise','?0','?1','?2']
    data = QTable.read(comp_file,format='ascii.fixed_width',data_start=2,
                       col_starts=colstart,
                       names=colnames) #,
                       # units=colunits)
    # sort data as a function of wavelength (inverse of wave number) for later compatibility
    data['wavelength'] = (1/(data['wavenumber']/u.cm)).to('um')
    data.sort('wavelength')
    # Extract relevant columns and convert to astropy units
    wavenumber = data['wavenumber'] / u.cm
    transmission = data['transmission'] # dimensionless
    # thermique = data['thermique'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # diffusion_multiple = data['diffusion_multiple'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # diffusion_simple = data['diffusion_simple'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # nightglow = data['nightglow'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # stellaire = data['stellaire'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # fond_de_sol = data['fond_de_sol'] * (u.W / (u.m**2 * u.sr)) * u.cm
    lum_totale = data['totale'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # source_exoatmos = data['source_exoatmos'] * (u.W / (u.m**2 * u.sr)) * u.cm
    # source_transmise = data['source_transmise'] * (u.W / (u.m**2 * u.sr)) * u.cm
    
    
    # Extract required atmospheric and photonic luminance values
    wavelength = data['wavelength'] # unit=[um]
    lambda_factor = wavenumber.to('cm-1') / wavelength.to('um') # unit=[1/cm.um]
    luminance_spectrale = (lum_totale * lambda_factor).to('W/sr.um.m2') # unit=[W/sr.um.m2]
    energy_photon = (h*c/wavelength).to('W.s.m/um') # unit=[W.m/s.um]=[J]
    luminance_photonique_spectrale = luminance_spectrale / energy_photon.to('W.s') * u.ph # unit=[ph / (s sr m2 um)]
    
    # Retrieve wavelenghts in band
    wavelength.sort()
    mask = (wavelength >= lambda_min) & (wavelength <= lambda_max)
    wavelength_band = wavelength[mask]
    
    # compute luminance in band
    luminance_photonique_spectrale_band = luminance_photonique_spectrale[mask] # unit=[ph / (s sr m2 um)]
    luminance_photonique_band = np.sum(luminance_photonique_spectrale_band * np.diff(wavelength_band,append=lambda_max)) # unit=[ph / (s sr m2)]
    
    # get transmission for the considered band
    trans_atm_spectrale_band = transmission[mask]
    trans_atm_band = np.sum(trans_atm_spectrale_band) / len(trans_atm_spectrale_band)
    
    # plot luminance spectrale
    figfile = 'Luminance_spectrale_band.png'
    fig=plt.figure()
    plt.plot(wavelength_band.to('um').value,luminance_photonique_spectrale_band.value,'r-') # 'r+'
    plt.title('Spectral Luminance')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('Luminance [ph / (s sr m2 um)]')
    plt.savefig(figfile)
    plt.close(fig)
    log.info('Figure saved in '+figfile)
    # plot Transmission as a function of wavelength
    figfile = 'Transmission_spectrale_band.png'
    fig=plt.figure()
    plt.plot(wavelength_band.to('um').value,trans_atm_spectrale_band,'r-') # 'r+'
    plt.title('Transmision')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('Transmission []')
    plt.savefig(figfile)
    plt.close(fig)
    log.info('Figure saved in '+figfile)
    
    log.debug('luminance_photonique_band: '+str(luminance_photonique_band))
    log.debug('trans_atm_band: '+str(trans_atm_band))
    
    
    ####### Estimation des énergies photoniques
    # Photon flux calculations
    # for debug purpose TODO: remove !!!!
    #luminance_photonique_band = 4.439E+17 * u.ph / (u.s*u.sr*u.m**2)
    #log.debug('luminance_photonique_band= '+str(luminance_photonique_band))
    
    log.debug('Gpix = '+str(Gpix))
    log.debug('Gcible = '+str(Gcible))
    log.debug('Topt = '+str(Topt))
    Flux_bckg_hors_cible = (Gpix - Gcible).to('m2') * luminance_photonique_band * Topt
    Flux_bckg_avant_cible = Gcible.to('m2') * luminance_photonique_band * Topt
    Flux_cible = met_eps * cible_band_luminance * trans_atm_band * Gcible.to('m2') * Topt
    Flux_pix_bckg = Gpix * luminance_photonique_band * Topt
    Flux_pix_cible = Flux_bckg_hors_cible + Flux_bckg_avant_cible + Flux_cible
    Flux_utile = Flux_pix_cible - Flux_pix_bckg
    
    log.debug('Flux_bckg_hors_cible = '+str(Flux_bckg_hors_cible))
    log.debug('Flux_bckg_avant_cible = '+str(Flux_bckg_avant_cible))
    log.debug('Flux_cible = '+str(Flux_cible))
    log.debug('Flux_pix_bckg = '+str(Flux_pix_bckg))
    log.debug('Flux_pix_cible = '+str(Flux_pix_cible))
    log.debug('Flux_utile = '+str(Flux_utile))
    
    
    # Compute the number of photons. Unit=[ph]
    N_transfert_max = (CsG * Delat_Vs / q  ).decompose() # unit=[]
    N_phot_dark = (Idark / q * exp_time).decompose() * u.ph # unit=[ph]
    N_phot_bckg_hors_cible = (QE * Flux_bckg_hors_cible * exp_time.to('s')).to('ph/sr') * u.sr # unit=[ph]
    N_phot_bckg_avant_cible = (QE * Flux_bckg_avant_cible * exp_time.to('s')).to('ph/sr') * u.sr # unit=[ph]
    N_phot_cible = QE * Flux_cible * exp_time.to('s') * u.sr # unit=[ph]
    N_phot_pix_bckg = QE * Flux_pix_bckg * exp_time.to('s') * u.sr + N_phot_dark # unit=[ph]
    N_phot_estime_bckg_et_cible = QE * Flux_pix_cible * exp_time.to('s') * u.sr + N_phot_dark # unit=[ph]
    N_phot_utile = QE * Flux_utile * exp_time.to('s') * u.sr # unit=[ph]
    
    log.debug('QE = '+str(QE))
    log.debug('exp_time = '+str(exp_time.to('s')))
    log.debug('N_transfert_max = '+str(N_transfert_max))
    log.debug('N_phot_dark = '+str(N_phot_dark))
    log.debug('N_phot_bckg_hors_cible = '+str(N_phot_bckg_hors_cible))
    log.debug('N_phot_bckg_avant_cible = '+str(N_phot_bckg_avant_cible))
    log.debug('N_phot_cible = '+str(N_phot_cible))
    log.debug('N_phot_pix_bckg = '+str(N_phot_pix_bckg))
    log.debug('N_phot_estime_bckg_et_cible = '+str(N_phot_estime_bckg_et_cible))
    log.debug('N_phot_utile = '+str(N_phot_utile))
    
    # compute voltage
    factor_V = q / CsG / u.ph
    V_utile = (N_phot_utile * factor_V).to('V') # unit=[V]
    V_dark = (N_phot_dark * factor_V).to('V')
    V_pix_bckg = (N_phot_pix_bckg * factor_V).to('V')
    V_estime_bckg_et_cible = (N_phot_estime_bckg_et_cible * factor_V).to('V')
    log.debug('V_utile = '+str(V_utile))    
    log.debug('V_dark = '+str(V_dark))   
    log.debug('V_pix_bckg = '+str(V_pix_bckg))   
    log.debug('V_estime_bckg_et_cible = '+str(V_estime_bckg_et_cible)) 


    # Compute noise. unit=[photo-electron rms]
    sigma_dark = np.sqrt(N_phot_dark / u.ph)
    sigma_phot_bckg = np.sqrt(N_phot_pix_bckg / u.ph)
    sigma_estime_bckg_et_cible = np.sqrt(N_phot_estime_bckg_et_cible / u.ph)
    # Johnson noise
    sigma_johnson = (np.sqrt( 2*k_B*pix_T * exp_time.to('s') /  Rsh ) / q).decompose()
    # total noise at photo-diode output
    sigma_PV_cible = np.sqrt(sigma_estime_bckg_et_cible**2 + sigma_johnson**2)
    sigma_PV_bckg = np.sqrt( sigma_phot_bckg**2 + sigma_johnson**2)
    # Read out noise
    sigma_lec = (CsG * VSF / q).decompose()
    nb_niv = 2**nbbit
    sigma_num = N_transfert_max / (nb_niv * np.sqrt(12))
    # output noise
    sigma_total_cible = np.sqrt( sigma_PV_cible**2 + sigma_lec**2 + sigma_num**2 ) 
    sigma_total_bckg = np.sqrt( sigma_PV_bckg**2 + sigma_lec**2 + sigma_num**2 )
    # equivalent voltage noise
    factor_V_sigma = q / CsG
    sigma_V_dark = (sigma_dark * factor_V_sigma).to('V')
    sigma_V_lec = (sigma_lec * factor_V_sigma).to('V')
    sigma_V_num = (sigma_num * factor_V_sigma).to('V')
    sigma_V_total_cible = (sigma_total_cible * factor_V_sigma).to('V')
    sigma_V_total_bckg = (sigma_total_bckg * factor_V_sigma).to('V')
    log.debug('sigma_dark = '+str(sigma_dark))
    log.debug('sigma_phot_bckg = '+str(sigma_phot_bckg))    
    log.debug('sigma_estime_bckg_et_cible = '+str(sigma_estime_bckg_et_cible))   
    log.debug('sigma_johnson = '+str(sigma_johnson))   
    log.debug('sigma_PV_cible = '+str(sigma_PV_cible))   
    log.debug('sigma_PV_bckg = '+str(sigma_PV_bckg))   
    log.debug('sigma_lec = '+str(sigma_lec))   
    log.debug('sigma_num = '+str(sigma_num))  
    log.debug('sigma_total_cible = '+str(sigma_total_cible))   
    log.debug('sigma_total_bckg = '+str(sigma_total_bckg))   
    log.debug('sigma_V_dark = '+str(sigma_V_dark))   
    log.debug('sigma_V_lec = '+str(sigma_V_lec))   
    log.debug('sigma_V_num = '+str(sigma_V_num))   
    log.debug('sigma_V_total_cible = '+str(sigma_V_total_cible))   
    log.debug('sigma_V_total_bckg = '+str(sigma_V_total_bckg))   
    
    # Several other quantities
    # apparent lenght of target displacement
    taille_file = met_Vatm_app.to('m/s') * exp_time.to('s') # unit = [m]
    # diffraction diameter
    diam_diffraction = 2.44 * lambda_mean * Nopt # unit=[um]
    # pixel size at target
    pix_sz_cible = (pix_sz * Lobs / FocL).decompose() # unit=[m])
    # camera total FOV
    FOV_tot = (np.arctan(pix_pitch.to('m') * pix_num.value / FocL.to('m'))).to('deg') # unit=[deg]
    # Current from background only
    I_pix_bckg = ((Flux_pix_bckg * u.sr / u.ph) * q).to('A') + Idark
    
    # compute signal to noise ratios
    R_signal_util_sur_bruit = V_utile / np.sqrt(sigma_V_total_cible**2+sigma_V_total_bckg**2) # unit=[]
    R_signal_util_sur_signal_bckg_atmo = V_utile / (V_pix_bckg - V_dark) # unit=[]
    
    # output
    log.debug('pix_pitch='+str(pix_pitch))
    log.debug('pix_num='+str(pix_num))
    log.debug('FocL='+str(FocL))
    log.debug('FOV_tot='+str(FOV_tot))
    log.debug('pix_sz_cible='+str(pix_sz_cible))
    log.debug('I_pix_bckg = '+str(I_pix_bckg))
    log.debug('V_pix_bckg - V_dark = '+str(V_pix_bckg - V_dark))      
    log.debug('R_signal_util_sur_bruit = '+str(R_signal_util_sur_bruit))
    log.debug('R_signal_util_sur_signal_bckg_atmo = '+str(R_signal_util_sur_signal_bckg_atmo))

    snr = R_signal_util_sur_signal_bckg_atmo
    return snr



def func_snr_SR_spectral(wavelength,lens,sensor,meteoroid,orbital_geometry,
                comp_file='./comp_Rayjn.xls',outdir='./'):
    """
    Computes signal to noise ratio, with formulae provided by Sylvain R. (ONERA).
    
    Parameters
    ----------
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    lens : dict
        Lens parameters:
            Dpup: astropy.units.Quantity object
                Pupil diamater, in [m] or [mm].
            FocL: astropy.units.Quantity object
                Focal lens. unit=[m].
            Nopt: float
                Lens aperture (no unit).
            Topt: float
                Lens transmission (no unit).
    sensor : dict
        Sensor parameters:
            pix_sz: astropy.units.Quantity object
                Detector size, in [m] or [um].
            pix_pitch: astropy.units.Quantity object
                Sampling pitch, in [m] or [um].
            pix_num: integer
                Number of samples, i.e. number of pixels, assuming a square sensor. no unit.
            QE: float
                Quantum efficiency over the whole wavelength band range. no unit.
            exp_time: astropy.units.Quantity object
                Integration time, in [s] or [us].
            pix_T: astropy.units.Quantity object
                Sensor temperature, in [K].
            Idark: astropy.units.Quantity object
                Sensor dark current, in [A] or [pA].
            Rsh: astropy.units.Quantity object
                Dynamic resistance of the photodiode, in [Ohm].
            VSF: astropy.units.Quantity object
                Output amplifier noise, in [V] or [uV].
            CsG: astropy.units.Quantity object
                Storage capacity at the output follower in [F] or [pF].
            Delat_Vs: astropy.units.Quantity object
                Excursion voltage of the output follower, in [V]
            nbbit: integer
                Number of digitization bits (dimensionless).
    meteoroid: dict
        Meteoroid parameters:
            x: astropy.units.Quantity object
                Meteoroid size in x direction, in [m].
            y: astropy.units.Quantity object
                Meteoroid size in y direction, in [m].
            T: astropy.units.Quantity object
                Meteoroid surface temperature, in [K].
            eps: float
                Meteoroid emissivity (dimensionless).
    orbital_geometry: dict
        Orbital geometry parameters.
            met_Vatm: astropy.units.Quantity object
                Meteoroid entry velocity w.r.t Earth., in [km/s] or [m/s].
            radiant_dist: astropy.units.Quantity object
                Angular distance between pointing direction and meteoroid radiant, in [rad] or [deg].
            Lobs: astropy.units.Quantity object
                Physical distance between the camera and the meteoroid, in [m] or [km].
    comp_file string, optional
        Rayjn comp-type file. Default is './comp_Rayjn.xls'.
    outdir: string, optional
        Output directory for plot images. Default is './'.        
    
    Returns
    -------
    snr_spectral: float
        Signal to noise ratio  as a function of wavelength (dimensionless).
    
    """
    if not outdir.endswith('/'):
        outdir = outdir + '/'
    # Store parameters
    # lens parameters
    Dpup = lens['Dpup'].to('m')
    FocL = lens['FocL'].to('m')
    Topt = lens['Topt']
    # wavelength parameters
    lambda_min = wavelength['lambda_min'].to('um')
    lambda_max = wavelength['lambda_max'].to('um')
    # sensor parameters
    pix_sz = sensor['pix_sz'].to('um')
    pix_pitch = sensor['pix_pitch'].to('um')
    pix_num = sensor['pix_num']
    QE = sensor['QE']
    exp_time = sensor['exp_time'].to('s')
    pix_T = sensor['pix_T'].to('K')
    Idark = sensor['Idark'].to('A')
    Rsh = sensor['Rsh'].to('Ohm')
    VSF = sensor['VSF'].to('V')  # Output amplifier noise
    CsG = sensor['CsG'].to('F')  # Storage capacity at the output follower
    Delat_Vs = sensor['Delat_Vs'].to('V')  # Excursion voltage of the output follower
    nbbit = sensor['nbbit']
    # meteoroid parameters
    met_x = meteoroid['x'].to('m')
    met_y = meteoroid['y'].to('m')
    met_T = meteoroid['T'].to('K')
    met_eps = meteoroid['eps']
    # orbital geometry
    met_Vatm = orbital_geometry['met_Vatm'].to('m/s')
    radiant_dist = orbital_geometry['radiant_dist'].to('deg')
    Lobs = orbital_geometry['Lobs'].to('m')

    # input control
    log.debug(str(wavelength))
    log.debug(str(lens))
    log.debug(str(sensor))
    log.debug(str(meteoroid))
    log.debug(str(orbital_geometry))
    
    # Compute necessary quantities
    q = (1*u.eV).to('J').value * u.C # electron electric charge. unit=[C]
    lambda_mean = (lambda_max + lambda_min) / 2  # Wavelength for diffraction calculation
    Nopt = FocL / Dpup  # numerical aperture
    Spup = np.pi * (Dpup / 2)**2  # Area of the pupil
    Spix = pix_pitch**2  # Area of a pixel
    Tirage = FocL  # Assumption for distance from the pupil to the detector
    Gpix = (Spup.to('m2') * Spix.to('m2') / Tirage.to('m')**2) # geometric extent, in [m2]
    omega_pup = np.arctan(Dpup.value/(2.0*Tirage.value))
    omega_ciblex = np.arctan(met_x.to('m').value/(2*Lobs.to('m').value))
    omega_cibley = np.arctan(met_y.to('m').value/(2*Lobs.to('m').value))
    Gcible = (4 * np.sin(omega_ciblex) * np.sin(omega_cibley) * Spup).to('m2') # target surface, in [m2]
    met_Vatm_app = met_Vatm * np.sin(radiant_dist.to('rad')) # apparent target velocity, in [km/s]
    log.debug('Nopt = '+str(Nopt))
    log.debug('Spup = '+str(Spup))
    log.debug('Spix = '+str(Spix))
    log.debug('Tirage = '+str(Tirage))
    log.debug('Gpix = '+str(Gpix))
    log.debug('omega_pup = '+str(omega_pup))
    log.debug('omega_ciblex = '+str(omega_ciblex))
    log.debug('omega_cibley = '+str(omega_cibley))
    log.debug('Gcible = '+str(Gcible))
    
    ##### Compute target radiance
    # Define wavelengths range
    wavelengths = np.linspace(lambda_min.value, lambda_max.value, 1000) * u.micron
    # Calculate the spectral radiance at each wavelength
    cible_spectral_luminance = spectral_luminance(met_T, wavelengths)
    cible_spectral_luminance = cible_spectral_luminance.to('ph/m2.s.sr.um') * np.diff(wavelengths.to('m'),append=lambda_max)/u.um
    # Integrate over the wavelength range to get total radiance
    #cible_band_luminance = np.trapz(cible_spectral_luminance, wavelengths.to('m'))
    #log.debug('cible_band_luminance = '+str(cible_band_luminance))   
    log.debug('cible_spectral_luminance = '+str(cible_spectral_luminance[:10]))   
    log.debug('len(cible_spectral_luminance) = '+str(len(cible_spectral_luminance)))   
    log.debug('len(wavelengths) = '+str(len(wavelengths)))   
    
    # Read Rayjn data from comp_Rayjn.txt type file
    if not os.path.exists(comp_file):
        raise IOError(comp_file+' does not exist')
    colstart = [0,20,44,61,86,116,139,160,188,202,224,260,284,308]
    colnames = [
        'wavenumber', 'transmission', 'thermique', 'diffusion_multiple', 'diffusion_simple', 
        'nightglow', 'stellaire', 'fond_de_sol', 'totale', 'source_exoatmos', 
        'source_transmise','?0','?1','?2']
    data = QTable.read(comp_file,format='ascii.fixed_width',data_start=2,
                       col_starts=colstart,
                       names=colnames) #,
                       # units=colunits)
    # sort data as a function of wavelength (inverse of wave number) for later compatibility
    data['wavelength'] = (1/(data['wavenumber']/u.cm)).to('um')
    data.sort('wavelength')
    # Extract relevant columns and convert to astropy units
    wavenumber = data['wavenumber'] / u.cm
    transmission = data['transmission'] # dimensionless
    lum_totale = data['totale'] * (u.W / (u.m**2 * u.sr)) * u.cm    
    
    # Extract required atmospheric and photonic luminance values
    wavelength = data['wavelength'] # unit=[um]
    lambda_factor = wavenumber.to('cm-1') / wavelength.to('um') # unit=[1/cm.um]
    luminance_spectrale = (lum_totale * lambda_factor).to('W/sr.um.m2') # unit=[W/sr.um.m2]
    energy_photon = (h*c/wavelength).to('W.s.m/um') # unit=[W.m/s.um]=[J]
    luminance_photonique_spectrale = luminance_spectrale / energy_photon.to('W.s') * u.ph # unit=[ph / (s sr m2 um)]
    
    # Retrieve wavelenghts in band
    wavelength.sort()
    mask = (wavelength >= lambda_min) & (wavelength <= lambda_max)
    wavelength_band = wavelength[mask]
    
    # compute sky luminance in band
    luminance_photonique_spectrale_band = luminance_photonique_spectrale[mask] # unit=[ph / (s sr m2 um)]
    #luminance_photonique_band = np.sum(luminance_photonique_spectrale_band * np.diff(wavelength_band,append=lambda_max)) # unit=[ph / (s sr m2)]
    
    # get transmission for the considered band
    trans_atm_spectrale_band = transmission[mask]
    #trans_atm_band = np.sum(trans_atm_spectrale_band) / len(trans_atm_spectrale_band)
    
    # compute target luminance in band = target luminance because target luminance was computed in the selected band already  
    cible_spectral_luminance_band = np.interp(wavelength_band.to('um'),wavelengths.to('um'),cible_spectral_luminance)
    
    # plot luminance spectrale
    figfile = outdir+'Luminance_spectrale_band.png'
    fig=plt.figure()
    plt.plot(wavelength_band.to('um').value,luminance_photonique_spectrale_band.value,'r-') # 'r+'
    plt.title('Sky Spectral Luminance')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('Luminance [ph / (s sr m2 um)]')
    plt.savefig(figfile)
    plt.close(fig)
    log.debug('Figure saved in '+figfile)
    # plot Transmission as a function of wavelength
    figfile = outdir+'Transmission_spectrale_band.png'
    fig=plt.figure()
    plt.plot(wavelength_band.to('um').value,trans_atm_spectrale_band,'r-') # 'r+'
    plt.title('Transmision')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('Transmission []')
    plt.savefig(figfile)
    plt.close(fig)
    log.debug('Figure saved in '+figfile)
    # plot target luminance spectrale
    figfile = outdir+'Target_luminance_spectrale_band.png'
    fig=plt.figure()
    plt.plot(wavelength_band.to('um').value,cible_spectral_luminance_band.value,'r-') # 'r+'
    plt.title('Target Spectral Luminance')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('Luminance [ph / (s sr m2 um)]')
    plt.savefig(figfile)
    plt.close(fig)
    log.debug('Figure saved in '+figfile)
    
    log.debug('luminance_photonique_spectrale_band: '+str(luminance_photonique_spectrale_band))
    log.debug('trans_atm_spectrale_band: '+str(trans_atm_spectrale_band))
    log.debug('cible_spectral_luminance_band: '+str(cible_spectral_luminance_band))
    
    
    ####### Estimation des énergies photoniques
    # Photon flux calculations
    
    # for debug purpose TODO: remove !!!!
    #luminance_photonique_band = 2.481E+17 * u.ph / (u.s*u.sr*u.m**2)
    
    Flux_bckg_hors_cible = (Gpix - Gcible).to('m2') * luminance_photonique_spectrale_band * Topt # unit=[ph/s.sr.um]
    Flux_bckg_avant_cible = Gcible.to('m2') * luminance_photonique_spectrale_band * Topt # unit=[ph/s.sr.um]
    Flux_cible = met_eps * cible_spectral_luminance_band * trans_atm_spectrale_band * Gcible.to('m2') * Topt # unit=[ph/s.sr.um]
    Flux_pix_bckg = Gpix * luminance_photonique_spectrale_band * Topt # unit=[ph/s.sr.um]
    Flux_pix_cible = Flux_bckg_hors_cible + Flux_bckg_avant_cible + Flux_cible # unit=[ph/s.sr.um]
    Flux_utile = Flux_pix_cible - Flux_pix_bckg # unit=[ph/s.sr.um]
    
    log.debug('Flux_bckg_hors_cible = '+str(Flux_bckg_hors_cible))
    log.debug('Flux_bckg_avant_cible = '+str(Flux_bckg_avant_cible))
    log.debug('Flux_cible = '+str(Flux_cible))
    log.debug('Flux_pix_bckg = '+str(Flux_pix_bckg))
    log.debug('Flux_pix_cible = '+str(Flux_pix_cible))
    log.debug('Flux_utile = '+str(Flux_utile))
    
    
    # Compute the number of photons. Unit=[ph]
    N_transfert_max = (CsG * Delat_Vs / q  ).decompose() # unit=[]
    N_phot_dark = (Idark / q * exp_time).decompose() * u.ph / u.um # unit=[ph/um]
    N_phot_bckg_hors_cible = (QE * Flux_bckg_hors_cible * exp_time.to('s')).to('ph/sr.um') * u.sr# unit=[ph/um]
    N_phot_bckg_avant_cible = (QE * Flux_bckg_avant_cible * exp_time.to('s')).to('ph/sr.um') * u.sr# unit=[ph/um]
    N_phot_cible = QE * Flux_cible * exp_time.to('s') * u.sr # unit=[ph/um]
    N_phot_pix_bckg = QE * Flux_pix_bckg * exp_time.to('s') * u.sr + N_phot_dark # unit=[ph/um]
    N_phot_estime_bckg_et_cible = QE * Flux_pix_cible * exp_time.to('s') * u.sr + N_phot_dark # unit=[ph/um]
    N_phot_utile = QE * Flux_utile * exp_time.to('s') * u.sr # unit=[ph/um]
    
    log.debug('N_transfert_max = '+str(N_transfert_max))
    log.debug('N_phot_dark = '+str(N_phot_dark))
    log.debug('N_phot_bckg_hors_cible = '+str(N_phot_bckg_hors_cible))
    log.debug('N_phot_bckg_avant_cible = '+str(N_phot_bckg_avant_cible))
    log.debug('N_phot_cible = '+str(N_phot_cible))
    log.debug('N_phot_pix_bckg = '+str(N_phot_pix_bckg))
    log.debug('N_phot_estime_bckg_et_cible = '+str(N_phot_estime_bckg_et_cible))
    log.debug('N_phot_utile = '+str(N_phot_utile))
    
    # compute voltage
    factor_V = q / CsG / u.ph
    V_utile = (N_phot_utile * factor_V).to('V/um') # unit=[V]
    V_dark = (N_phot_dark * factor_V).to('V/um')
    V_pix_bckg = (N_phot_pix_bckg * factor_V).to('V/um')
    V_estime_bckg_et_cible = (N_phot_estime_bckg_et_cible * factor_V).to('V/um')
    V_pix_bckg = (N_phot_pix_bckg * factor_V).to('V/um')
    
    # Compute noise. unit=[photo-electron rms]
    sigma_dark = np.sqrt(N_phot_dark.value) / u.um
    sigma_phot_bckg = np.sqrt(N_phot_pix_bckg.value) / u.um
    sigma_estime_bckg_et_cible = np.sqrt(N_phot_estime_bckg_et_cible.value) / u.um
    # Johnson noise
    sigma_johnson = (np.sqrt( 2*k_B*pix_T * exp_time.to('s') /  Rsh ) / q).decompose() / u.um
    log.debug('sigma_estime_bckg_et_cible = '+str(sigma_estime_bckg_et_cible))
    log.debug('sigma_johnson = '+str(sigma_johnson))
    # total noise at photo-diode output
    sigma_PV_cible = np.sqrt(sigma_estime_bckg_et_cible**2 + sigma_johnson**2)
    log.debug('sigma_phot_bckg = '+str(sigma_phot_bckg))
    log.debug('sigma_johnson = '+str(sigma_johnson))
    sigma_PV_bckg = np.sqrt( sigma_phot_bckg**2 + sigma_johnson**2)
    # Read out noise
    sigma_lec = (CsG * VSF / q).decompose() / u.um
    nb_niv = 2**nbbit
    sigma_num = N_transfert_max / (nb_niv * np.sqrt(12)) / u.um
    # output noise
    log.debug('sigma_PV_cible = '+str(sigma_PV_cible))
    log.debug('sigma_lec = '+str(sigma_lec))
    log.debug('sigma_num = '+str(sigma_num))
    sigma_total_cible = np.sqrt( sigma_PV_cible**2 + sigma_lec**2 + sigma_num**2 ) 
    sigma_total_bckg = np.sqrt( sigma_PV_bckg**2 + sigma_lec**2 + sigma_num**2 )
    # equivalent voltage noise
    factor_V_sigma = q / CsG
    log.debug('sigma_dark = '+str(sigma_dark))
    log.debug('factor_V_sigma = '+str(factor_V_sigma))
    sigma_V_dark = (sigma_dark * factor_V_sigma).to('V/um')
    sigma_V_lec = (sigma_lec * factor_V_sigma).to('V/um')
    sigma_V_num = (sigma_num * factor_V_sigma).to('V/um')
    sigma_V_total_cible = (sigma_total_cible * factor_V_sigma).to('V/um')
    sigma_V_total_bckg = (sigma_total_bckg * factor_V_sigma).to('V/um')
    
    
    # Several other quantities
    # apparent lenght of target displacement
    taille_file = met_Vatm_app.to('m/s') * exp_time.to('s') # unit = [m]
    # diffraction diameter
    diam_diffraction = 2.44 * lambda_mean * Nopt # unit=[um]
    # pixel size at target
    pix_sz_cible = (pix_sz * Lobs / FocL).decompose() # unit=[m])
    # camera total FOV
    log.debug('pix_pitch='+str(pix_pitch))
    log.debug('pix_num='+str(pix_num))
    log.debug('FocL='+str(FocL))
    FOV_tot = (np.arctan(pix_pitch.to('m') * pix_num.value / FocL.to('m'))).to('deg') # unit=[deg]
    log.debug('FOV_tot='+str(FOV_tot))
    # Current from background only
    log.debug('Flux_pix_bckg='+str(Flux_pix_bckg))
    log.debug('Idark='+str(Idark))
    I_pix_bckg = ((Flux_pix_bckg * u.sr / u.ph) * q).to('A/um') + Idark/u.um
    
    # compute signal to noise ratios
    R_signal_util_sur_bruit = V_utile / np.sqrt(sigma_V_total_cible**2+sigma_V_total_bckg**2) # unit=[]
    R_signal_util_sur_signal_bckg_atmo = V_utile / (V_pix_bckg - V_dark)
    
    # output
    log.debug('R_signal_util_sur_bruit = '+str(R_signal_util_sur_bruit))
    log.debug('R_signal_util_sur_signal_bckg_atmo = '+str(R_signal_util_sur_signal_bckg_atmo))

    snr_spectral_band = R_signal_util_sur_signal_bckg_atmo
    log.debug('snr_spectral_band = '+str(snr_spectral_band))
    
    # plot Transmission as a function of wavelength
    figfile = outdir + 's2n_spectral_band.png'
    fig=plt.figure()
    plt.plot(wavelength_band.to('um').value,snr_spectral_band,'r-') # 'r+'
    plt.title('S/N=f(wavelength)')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('S/N []')
    plt.savefig(figfile)
    plt.close(fig)
    log.info('Figure saved in '+figfile)
    
    return snr_spectral_band,wavelength_band.to('um')



# # # Default System Characteristics
# # Lens parameters
# Dpup = 50 * u.mm  # Diameter of the pupil
# FocL = 100 * u.mm  # Focal length
# Topt = 0.8  # Optical transmission (dimensionless)
# lens = {'Dpup':Dpup,'FocL':FocL,'Topt':Topt}

# # Wavelength parameters
# lambda_min = 3 * u.um  # Lower cut-off wavelength
# lambda_max = 5 * u.um  # Upper cut-off wavelength
# wavelength = {'lambda_min': lambda_min, 'lambda_max':lambda_max}

# # Sensor parameters
# pix_sz = 15 * u.um  # Detector size
# pix_pitch = 15 * u.um  # Sampling pitch
# pix_num = 640 *u.pix  # Number of samples (dimensionless)
# QE = 80 / 100  # Quantum efficiency (dimensionless)
# exp_time = 5 * u.ms  # Integration time
# pix_T = 80 * u.K  # Cooling temperature of the focal plane
# Idark = (2 * u.pA).to('A')  # Dark current
# Rsh = 1e9 * u.Ohm  # Dynamic resistance of the photodiode
# VSF = (100 * u.uV).to('V')  # Output amplifier noise
# CsG = (5.2 * u.pF).to('F')  # Storage capacity at the output follower
# Delat_Vs = 2.2 * u.V   # Excursion voltage of the output follower
# nbbit = 14  # Number of digitization bits (dimensionless)
# sensor = {'pix_sz':pix_sz, 'pix_pitch':pix_pitch, 'pix_num':pix_num, 'QE':QE, 'exp_time':exp_time, 'pix_T':pix_T, 'Idark':Idark, 'Rsh':Rsh, 'VSF':VSF, 'CsG':CsG, 'Delat_Vs': Delat_Vs, 'nbbit': nbbit}

# # meteoroid parameters
# met_x = 1 * u.cm  # Target size in x direction
# met_y = 1 * u.cm  # Target size in y direction
# met_T = 2000 * u.K  # Target temperature
# met_eps = 1  # Target emissivity (dimensionless)
# meteoroid = {'x':met_x, 'y':met_y, 'T':met_T, 'eps': met_eps}

# # geometry definition
# #Lobs = 150 * u.km  # Observation distance TODO: re-compute this quantity
# Lobs = 42 * u.km  # Observation distance TODO: re-compute this quantity
# met_Vatm = 10 * u.km / u.s  # Relative speed of the target perpendicular to the line of sight
# radiant_dist = 45.0 * u.deg # angular distance between the radiant and the target
# orbital_geometry = {'met_Vatm':met_Vatm,'radiant_dist':radiant_dist, 'Lobs':Lobs}


# # # compute snr
# snr = func_snr_SR(wavelength,lens,sensor,meteoroid,orbital_geometry)
# #snr_spectral = func_snr_SR_spectral(wavelength,lens,sensor,meteoroid,orbital_geometry)
# log.debug('snr = '+str(snr))

