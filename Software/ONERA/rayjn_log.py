#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 09:58:48 2024

@author: vaubaill
"""

import logging
from astropy.time import Time


# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

strt = 'log-' + Time.now().isot.replace('-','').replace(':','').split('.')[0]+'.log'
hdlr = logging.FileHandler(strt)
hdlr.setLevel(logging.DEBUG)
hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(hdlr)
