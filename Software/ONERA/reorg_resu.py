#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 09:49:55 2024

@author: vaubaill

Reorganise the results of Rayjn software.

"""

import os
import glob
import shutil

from rayjn_log import log
from read_rayjn import plot_lum,plot_comp


home = os.getenv('HOME')
pth = home + '/malbec/Software/ONERA/Pierre/Rayjn*/'
outpth = home + '/malbec/Software/ONERA/Pierre/DATA'


def Rayjn_conf2name(confile):
    """Rayn configuration file to name.
    
    Parameters
    ----------
    confile : string
        Rayjn configuration file.
    
    Returns
    -------
    name : string
        Name in format $date_UT-ALT$altAZ$azEL$el
    
    """
    
    with open(confile,'r',encoding="ISO-8859-1") as f:
        lines = f.readlines()
    [day,month,yearhour] = lines[0].split('\t')[0].split('/')
    [year,hour] = yearhour.split('   ')
    hour = hour.replace(':','').replace(' ','0')
    date = year+month+day+'T'+hour+'_UT'
    [zenith,az] = lines[1].split('    ')[:2]
    zenith = zenith.split('.')[0]
    el = str(90-int(zenith))
    az = az.split('.')[0]
    alt = lines[2].split()[0]
    alt = alt.split('.')[0]
    
    name = date+'-ALT'+alt+'AZ'+az+'EL'+el
    
    return name

# main program
if not os.path.isdir(outpth):
    os.mkdir(outpth)
    log.info('Output directory was created: '+outpth)
list_dir = glob.glob(pth+'RESU/*')
for resudir in list_dir:
    if os.path.isdir(resudir):
        confile = resudir + '/Rayjn.data'
        if not os.path.exists(confile):
            msg = '*** FATAL ERROR: Rayjn config file '+confile+' does not exist'
            log.error(msg)
            raise IOError(msg)
        log.info('Reading config file '+confile)
        name = Rayjn_conf2name(confile)
        log.info('Name: '+name)
        # copy output files into new output directory
        datadir = outpth + '/' + name
        if not os.path.isdir(datadir):
            os.mkdir(datadir)
            log.info('Creating data dir: '+datadir)
        list2copy = ['Rayjn.data','lum_Rayjn.xls','comp_Rayjn.xls']
        for f2copy in list2copy:
            src = resudir+'/'+f2copy
            dst = datadir+'/'+f2copy
            if not os.path.isfile(src):
                msg = '*** FATAL ERROR: file '+src+' does not exist'
                log.error(msg)
                raise IOError(msg)
            try:
                shutil.copy(src, dst)
            except:
                msg = '*** FATAL ERROR: impossible to copy '+src+' to '+dst
                log.error(msg)
                raise IOError(msg)
        # make graphs
        plot_lum(datadir+'/'+'lum_Rayjn.xls')
        plot_comp(datadir+'/'+'comp_Rayjn.xls')

