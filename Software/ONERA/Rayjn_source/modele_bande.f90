         subroutine mod_bande(ntemp,ip,ibinx,iprm,iblk,iblock,irecnm,&
       imolx,ialfx,ipx,ibndwd,iv1,ifwhm,mxfreq,nseg,tband,sdz,odz,&
       pmoy,tmoy,dstmol,dstcfc,wt,dopfac,sdzx,odzx,&
       iparam,ibin,imol,ialf,jj,ff,itb,itbx,iv,dv,trans,tranc,init)

!     Les fichiers parametres modele de bande sont :
!           - le fichier DIRAC pour les traces -> ITB = 9
!           - le fichier UFTAPX.asc pour les CFC -> ITBX = 10
       
      include 'parameter.rayjn'
       
      real sd(ntmx,mmolt2),od(ntmx,mmolt)
      real sdz(ntmx,nipmx),odz(ntmx,nipmx),alf0(mmolt),dopfac(mmolt)
      real colsum(mmolt),dopsum(mmolt),odsum(mmolt),sdsum(mmolt2)
      real pmoy(laythr),dop0(mmolt),odzx(5),sdzx(5),ff(laythr)
      real tband(ntmx),tmoy(laythr),wt(laythr),tcfc(nspecx)
      real tauold(laythr,nspc+2),taunew(laythr,nspc+2),taucont(nspc+2)
      real dstmol(laythr,nspc),dstcfc(laythr,mmolx)
      real *8 tranc(laythr),trans(nspc)
                
      integer ibin(nipmx),imol(nipmx),ialf(nipmx)
      integer iparam(nblkmx),jj(laythr),indice(laythr)
      integer iv
       
      logical mltrnx,hr

      pi = acos(-1.)
      RAD=PI/180.0

!        initialisations
         mltrnx = .true.
         hr = .true.
!        trans() = transmission Haute Resolution des 12 premieres molecules de HITRAN
         do k=1,nspc
            trans(k) = 1.
         enddo
!        tcfc() = transmission Haute Resolution des CFC                                
         do k=1,nspecx 
            tcfc(k) = 1.
         enddo
         do  k=1,nspect
            dop0(k)=iv*dopfac(k)
            colsum(k)=0.
            dopsum(k)=0.
            odsum(k)=0.
            sdsum(k)=0.
            sdsum(k+nspect)=0.
         enddo     
         colo3=0.
         do iml=1,nspect2
            sd(1,iml)=0.
         enddo
         tautot = 0.
!        fin initialisations
       
!        MXFREQ = nb d'onde du dernier parametre stocke dans le fichier DIRAC
!        Si nb d'onde > MXFREQ  -> transmission egale a 1
         if(iv.gt.mxfreq) hr = .false.
       
!         -> IBIN(IP) = nb d'onde correspondant a la ligne IP
!        test si il existe des parametres modele de bande au nb d'onde IV
         if(ibin(ip).ne.iv) hr = .false.
       
!         -> IBINX = nb d'onde correspondant a la ligne IPX
!        test si il existe des parametres modele de bande au nb d'onde IV pour les CFC
         if(ibinx .ne. iv) mltrnx=.false.
!      
         if ( hr ) then
!           chargement des tableaux sd, od et alf0 pour le nb d'onde IBIN(IP) et pour 
!           l'espece IBIN(IP)
30          call load(ip,ntemp,imol,sd,od,sdz,odz,alf0,ialf)
       
!           Test sur IP                                     
!                 -> IP n'est pas la derniere ligne du block IBLK 
!                    on incremente IP=IP+1 : 1/ IBIN(IP+1) = IV  => la ligne IP+1 donne
!                                               les parametres modele de bande pour l'espece suivante
!                                               au nb d'onde IV. On retourne donc au label 30 pour
!                                               recharger sd, od et alf0 pour l'espece suivante.
!                                            2/ IBIN(IP+1) > IV, toutes les molecules sont chargees
!                                               dans sd, od et alf0 ; on continue le programme.
!                 -> IP est la derniere ligne du block IBLK 
!                    - on incremente IBLK, puis lecture de tous les parametres du block IBLK+1
!                      sachant que les tableaux sd, od et alf0 ont ete charges precedemment dans 
!                      le sous programme load pour toutes les especes au nb d'onde IV=IBIN(IP)
!                      
!                    - initialisation de IP : IP=1   
                 
            if(ip.lt.iprm)then
                ip=ip+1
                if(ibin(ip).eq.iv) goto 30
            else
                if(iblk.lt.iblock)then
                    iblk=iblk+1
                    iprm=iparam(iblk)
                    irecnm = iblk +1
                    if(irecnm.lt.300) read(itb,rec=irecnm)(ibin(ip),&
                    imol(ip),(sdz(it,ip),it=1,ntemp),ialf(ip),&
                    (odz(it,ip),it=1,ntemp),ip=1,iprm)
                    ip=1
                endif
            endif
         endif
       
         if ( mltrnx) then
!           chargement des tableaux sd, od et alf0 pour les CFC                          
 27         call load_cfc(imolx,ntemp,ialfx,sd,od,alf0,sdzx,odzx)
            ipx = ipx+1
            ibinx =0
            imolx=0
            ialfx=0
            do i=1,5
               sdzx(i)=0.
               odzx(i)=0.
            enddo
            if(iv.gt.1789) go to 29
            read(itbx,*,err=29,end=29) ibinx,imolx,(sdzx(izx),izx=1,5),&
                                       ialfx,(odzx(izx),izx=1,5)
            if (ibinx .eq. iv) go to 27
         endif
 29      continue

!        BOUCLE SUR LES SEGMENTS : calcul des transmissions cumulatives pour chaque molecule
!      

         do l=1,nseg     
         
            if (init.eq.0) then
               do  k=1,nspect
                  dop0(k)=iv*dopfac(k)
                  colsum(k)=0.
                  dopsum(k)=0.
                  odsum(k)=0.
                  sdsum(k)=0.
                  sdsum(k+nspect)=0.
               enddo     
               colo3=0.
            endif
       
!           DEBUT DE BOUCLE SUR LES ESPECES                
!           si calcul Haute Resolution (implicitement, on suppose que si il n'y a pas de 
!           calcul haute resolution, il n'y a pas de calcul pour les CFC.

!           initialisation de l'epaisseur optique totale
            tautot = 0.
            if (hr) then
               nsped=nspc+nspc
               ! Boucle sur les esp�ces
               do knew=1,nspect
                   k = knew
                   if (knew .le. nspc) then
                      ksd = k
                      ktail=k+nspc
                   else
                      kp = knew-nspc
                      ksd = kp+nsped
                      ktail=ksd+nspecx
                   endif
          
!                  Test si le parametre S/d est non nul                 
                   if(sd(1,ksd).gt.0.)then
          
!                       interpolation des parametres S/d et 1/d sur la temperature
!                        -> JJ(L) = indice correspondant au n0 de la temperature 
!                           du tableau TBAND superieure a la temperature de la couche
!                        -> FF(L) = coefficient d'interpolation pour le calcul de 
!                           la temperature de la couche 
!                        -> ABSM    = S/d (T)  
!                        -> DINV    = 1/d (T)  
!                        -> TAIL    = C(T)  
!                        -> PL      = P/P0
!                        -> WT      = SQRT(T/T0)
!                        -> WPATH   = densite integree (cm-3*km)*1.E5/lochmidt
!                        -> SDSUM   = [Su/d] pour le centre de raie (equation (33) annexe 3)
!                                     (rapport RTS 3/4635 PY Janvier 96)
!                                   = [Cu] pour les ailes de raie (equation (17) annexe 2)
!                        -> ODSUM   = [Su/d]*<1/d>                                          
!                        -> DOPSUM  = [Su/d]*<gammad/d>/gammad0                                  
!                        -> COLSUM  = [Su/d]*<gammal/d>/gammal0                                  
!                        -> ODBAR   = <1/d> (equation (34) annexe 3)
!                        -> ACBAR   = <gammal/d> (equation (35) annexe 3)
!                        -> ADBAR   = <gammad/d> (equation (36) annexe 3)
                        j=jj(l)
                        f=ff(l)
                        jm1=j-1
                        store=sd(j,ksd)
                        absm=store+f*(sd(jm1,ksd)-store)
                        store=od(j,k)
                        dinv=store+f*(od(jm1,k)-store)
                        store=sd(j,ktail)
                        tail=store+f*(sd(jm1,ktail)-store)
                        pl=pmoy(l)
                        if (knew .gt. nspc) pl = 1.
                        wl=wt(l)
                        if (knew .le. nspc) store=dstmol(l,k)
                        if (knew .gt. nspc) store=dstcfc(l,kp)
                        sdsum(ktail)=sdsum(ktail)+store*tail*pl
                        store=absm*store
                        sdsum(ksd)=sdsum(ksd)+store
                        store=dinv*store
                        odsum(k)=odsum(k)+store
                        dopsum(k)=dopsum(k)+store*wl
                        store=store*pl/wl
                        if(k.eq.2)then
                            colsum(k)=colsum(k)+store/sqrt(wl)
                        else
                            colsum(k)=colsum(k)+store
                        endif
                        if(k.eq.3)colo3=colo3+store*dinv*pl/wl
                        tdepth=sdsum(ktail)
                        depth=sdsum(ksd)
                        odbar=odsum(k)
                        adbar=dopsum(k)
                        acbar=colsum(k)
                        acbar2=0.
                        if(k.eq.3)acbar2=colo3

!                          Test si on est en regime de raie faible (depth = [Su/d])
                           if (depth.lt.0.001)then                      
                              transm=1. - depth
                              tautot = tautot + depth
                           else
                              odbar=odbar/depth
                              adbar=dop0(k)*adbar/depth
                              acbar=alf0(k)*acbar/depth
                              if(acbar2.ne.0.)acbar2=&
                                              acbar2/depth*alf0(k)**2

!                                                                                <n>
!                             calcul de la transmission : transm = (1. -<Wsl>/dv)
!                             transm est la transmission moyenne cumulee pour la molecule k entre 
!                             l'extremite de la couche 1 et la couche l
                              call bmtran(depth,acbar,adbar,odbar,dv,&
                                                        transm,acbar2)
                               if(transm.gt.0.)then
                                  tautot = tautot - log(transm)
                               else
                                  tautot = tautot + depth
                               endif
                            endif
     
!                          calcul de l'expression (16) annexe 2 : ailes de raies
                           if (knew .le. nspc) then
                              trans(k)=transm*exp(-tdepth)
                              tautot = tautot + tdepth
                           else
                              tcfc(kp)=transm*exp(-tdepth)
                              tautot = tautot + tdepth
                           endif
       
!                  Si S/d = 0. on calcule la transmission due aux ailes de raies uniquement 

                   elseif(sd(1,ktail).gt.0.)then

!                       interpolation des parametres S/d et 1/d sur la temperature
                       j=jj(l)
                       store=sd(j,ktail)
                       tail=store+ff(l)*(sd(j-1,ktail)-store)
                       if (knew.le.nspc) store=dstmol(l,k)*tail*pmoy(l)
                       if (knew .gt. nspc)store=dstcfc(l,kp)*tail
                       sdsum(ktail)=sdsum(ktail)+store
                       tdepth=sdsum(ktail)
                        
                          if (knew .le. nspc) then
                             trans(k) = exp(-tdepth)
                             if(trans(k) .gt. 1.)trans(k) = 1.
                             tautot = tautot + tdepth
                          else
                             tcfc(kp)=exp(-tdepth)
                             if(tcfc(kp) .gt. 1.) tcfc(kp) = 1.
                             tautot = tautot + tdepth
                          endif
                   else
                       if (knew .le. nspc) then
                          trans(k)=1.
                       else
                          tcfc(kp)=1.
                       endif
                   endif
               enddo     
            endif     
!    
!        Chargement des transmissions cummulees dans le tableau tranc(l) : 
!    *** tranc(1) = transmission mol�culaire du segment 1
!        tranc(2) = transmission mol�culaire cumul�e des segments 1 et 2
!        tranc(3) = transmission mol�culaire cumul�e des segments 1, 2 et 3 
!        tranc(nseg) = transmission mol�culaire cumul�e des segments 1, 2, ..., nseg-2, nseg-1 et nseg

         if (tautot.lt.90) then
            tranc(l) = exp(-tautot)
         else
            tranc(l) = 1.e-40
         endif
         if (init.eq.0) tranc(l) = tautot
         
!        Fin de boucle sur les couches
         enddo
         end