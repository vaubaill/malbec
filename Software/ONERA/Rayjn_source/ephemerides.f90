      subroutine ephemerides(heure_d,imois,iannee,ijour,Jday,lati,long,teta0,hobs,tetal,ang_phas,Rs_ini,Rtl,azsol,azlun)
      
      real   lati,long,Para,Refrac,diam,eta1,eta2,hobs,heure_d,nl
      real * 8 correc,ET,heure,DT,pi,conv,lat,Ttransit,Tlever,Tcoucher
      real * 8 va,arg,arg1,arg2,arg3,Dec,Ah,hs,hl,hs_tra,hl_tra,Decs,alphas,h1l,h1s,h1a
      real * 8 TD,jour,JD,mois,L0,cent,amoy,Lonsol,minute,seconde,Rs,TsaGs,TDs
      real * 8 t0,TsG,eps0,U,alpha,alphah,dpsi,deps,omega,lonlun,epsilon,TsaG,TsL,TSAG0,TcG
      real * 8 alpha1,alpha2,alpha3,dec1,dec2,dec3,m0,m1,m2,nm,h0
      real * 8 aa,ba,ca,ad,bd,cd,alpha_int,deltam,h_int,dec_int
      real * 8 lambda,delta,beta,psy,anglei,lprime,bprime,lprime0
      real * 8 dpsi_save,F,F_save,Lonsol_ini
      real * 8 latec,longec
      
      character val*4, date*8, time*10,zone*5,pos1*2,pos2*2
      integer values(8),nmois(12)
      
      data nmois /31,28,31,30,31,30,31,31,30,31,30,31/

    
       pi = dacos(-1.d0)
       conv = pi/180.d0
       
      iheure = int(heure_d)
      minute = (heure_d - iheure)*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      pos1=' N'
      pos2=' W'
      if (lati.lt.0) pos1=' S'
      if (long.le.0) pos2=' E'
      write(*,*) 
      write(*,'(10x,a45)') '*************************************************************'
      write(*,'(10x,a22,i2,a1,i2,a1,i4,12x,a1)') '* Ephemerides pour le ',ijour,'/',imois,'/',iannee,'*'
      write(*,'(10x,a31,i3,10x,a1)') '* Numero du jour dans l''annee :',Jday,'*'
      write(*,'(10x,a22,f7.3,a2,a3,f7.3,a2,a2)') '* Position Observateur ',abs(lati),pos1,' / ',abs(long),pos2,' *'
      write(*,'(10x,a25,f7.3,a6,6x,a1)')'* Altitude Observateur : ',hobs,'km','*'
      write(*,'(10x,a9,i2,a1,i2,a2,i2,a5,20x,a2)') '* Heure :',iheure,'h',imn,'mn',isec,'s  UT',' *'
      write(*,'(10x,a45)') '**************************************************'
      write(*,*)
      write(16,*) 
      write(16,'(10x,a45)') '*************************************************************'
      write(16,'(10x,a22,i2,a1,i2,a1,i4,12x,a1)') '* Ephemerides pour le ',ijour,'/',imois,'/',iannee,'*'
      write(16,'(10x,a31,i3,10x,a1)') '* Numero du jour dans l''annee :',Jday,'*'
      write(16,'(10x,a22,f7.3,a2,a3,f7.3,a2,a2)') '* Position Observateur ',abs(lati),pos1,' / ',abs(long),pos2,' *'
      write(16,'(10x,a25,f7.3,a6,6x,a1)')'* Altitude Observateur : ',hobs,'km','*'
      write(16,'(10x,a9,i2,a1,i2,a2,i2,a5,20x,a2)') '* Heure :',iheure,'h',imn,'mn',isec,'s  UT',' *'
      write(16,'(10x,a45)') '**************************************************'
      write(16,*)
      
      call DATE_AND_TIME(date, time, zone, values)
      itz = int(values(4)/60)
      write(*,'(a22,i2,a1,i2,a1,i4,a3,i2,a1,i2,a2,i2,a6,i2)') ' Calculs effectues le ',values(3),'/',values(2),'/',values(1),' a ',values(5),'h',values(6),'mn',values(7),'s UTC+',itz
      write(*,*)
      
      
      ! Calcul du temps terrestre
      heure = heure_d
      DT = 67.d0/3600.d0
      heure = heure + DT
 
       ! Calcul du jour Julien : JD
       call Julien(imois,iannee,ijour,heure,JD,TD)
       TDs = TD
       !write(*,'(a14,f12.3)') 'Jour Julien  :',JD

       ! Calcul du temps Sid�ral moyen � Greenwich (angle horaire � Greenwich par rapport au point vernal) : TsG       
       call tsgm(JD,TD,TSG)
      
       t0 = TsG/15.
       minute = (t0 - int(t0))*60
       seconde = (minute-int(minute))*60
       imn = int(minute)
       isec = int(seconde)
       !write(*,'(a34,i2,a1,i2,a2,i2,a1)') 'Temps Sideral moyen a Greenwich :',int(t0),'h',imn,'mn',isec,'s'    
       
       ! calcul de la longitude solaire
       call long_sol(TD,Lonsol,Rs)
       Rs_ini = Rs
       Lonsol_ini = Lonsol
       ! Param�tres de l'�cliptique
       call ecliptic(TD,Lonsol,dpsi,epsilon)
       
       ! On sauve les donn�es pour les calculs des param�tres de la lune
       dpsi_save = dpsi
             
       ! temps sid�ral apparent � Greenwich (en degr�s): TsaG
       TsaG = TsG + dpsi*dcos(epsilon*conv)/3600.d0
       TsaGs = TsaG
       

       write(*,*)
       write(*,'(a37)') ' Parametres solaire pour la position '
       write(*,'(a37)') '*************************************'
       write(16,*)
       write(16,'(a37)') ' Parametres solaire pour la position '
       write(16,'(a37)') '*************************************'
       ! ascension droite du soleil : alpha = angle entre la direction solaire et le point vernal projet� sur l'�quateur c�leste 
       arg1 = dcos(epsilon*conv)*sin(Lonsol*conv)
       arg2 = dcos(Lonsol*conv)
       alpha = datan2(arg1,arg2)/conv
       if (alpha.lt.0) alpha = 360.d0 + alpha
       alphas = alpha
       alphah = alpha/15.  ! conversion en heure
       
       minute = (alphah - int(alphah))*60
       seconde = (minute-int(minute))*60
       imn = int(minute)
       isec = int(seconde)
       write(*,'(1x,a20,i2,a1,i2,a2,i2,a1)') 'Ascension droite  : ',int(alphah),'h',imn,'mn',isec,'s'
       write(16,'(1x,a20,i2,a1,i2,a2,i2,a1)') 'Ascension droite  : ',int(alphah),'h',imn,'mn',isec,'s'
       Dec = asin(sin(epsilon*conv)*sin(Lonsol*conv))/conv
       decs = Dec
       minute = (dec - int(dec))*60
       imn = abs(minute)
       write(*,'(1x,a20,i3,a7,i2,a2)') 'Declinaison       :  ',int(Dec),'degres',imn,'mn'
       write(16,'(1x,a20,i3,a7,i2,a2)') 'Declinaison       :  ',int(Dec),'degres',imn,'mn'
      
      
      Para = 0  !  sauf pour lune para=57'
      Refrac = 0.56 ! 34'
      diam = 0.53   ! 32'
      eta1 = 0.0193*sqrt(hobs*1000.)
      ! eta2 = atan(D/l)  l=distance � la montagne de hauteur D
      eta2 = 0.
      
      lat = lati*conv

      ! hauteur du soleil : hs
      Ah = TsaGs - long - alpha ! angle horaire local
      arg = dsin(lat)*dsin(Dec*conv) + dcos(lat)*dcos(Dec*conv)*dcos(Ah*conv)
      hs = dasin(arg)/conv
      teta0 = 90 - hs ! angle z�nithal solaire
      
      ! Azimuth solaire : az
      arg1 = dsin(Ah*conv)
      arg2 = dcos(Ah*conv)*dsin(lat)- dtan(Dec*conv)*dcos(lat)
      az = datan2(arg1,arg2)/conv + 180.
      azsol = az
 
      write(*,'(1x,a20,f10.3,a7)') 'Angle horaire  : ',Ah,' degres'
      write(*,'(1x,a20,f10.3,a7)') 'Hauteur        : ', hs,'degres'
      write(*,'(1x,a20,f10.3,a7)') 'Azimuth        : ',az,'degres'
      write(*,'(1x,a20,f10.3,a7)') 'Angle zenithal : ',teta0,'degres'  
      write(16,'(1x,a20,f10.3,a7)') 'Angle horaire  : ',Ah,' degres'
      write(16,'(1x,a20,f10.3,a7)') 'Hauteur        : ', hs,'degres'
      write(16,'(1x,a20,f10.3,a7)') 'Azimuth        : ',az,'degres'
      write(16,'(1x,a20,f10.3,a7)') 'Angle zenithal : ',teta0,'degres'  
                
      ! Equation du temps : ET
      arg = 360*(Jday-81)/365*conv
      ET = L0 - 0.0057183 - alpha + (dpsi/3600*dcos(epsilon*conv))
      !write(*,*) 'equation du temps (mn)',ET*4
        
     
     ! Lever, Coucher et transit du soleil 
     ! ***********************************
          
      ics0 =0
      ils0 =0
      hs_tra = 0.
      imax = 24*360
      h0 = Para - Refrac - 0.5*diam - eta1 + eta2
      do i=1,imax+1
         heure = float(i-1)/360.
         call TsapG(imois,iannee,ijour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
         ! Calculs pour le soleil
         call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
         if (i.gt.1) then
            diff = hs - h1s
            if (diff.gt.0) then
               if (hs.le.h0) then
                  if (ils0.eq.0) then
                     ils0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tlevers = heure
                     azls = az
                     itest = i
                  endif
               endif
            else
               if (hs.ge.h0) then
                  if (ics0.eq.0) then
                     ics0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tcouchers = heure
                     azcs = az
                     itest = i
                  endif
               endif
            endif          
         endif
         if (hs.gt.hs_tra) then
            hs_tra = hs
            Ttransits = heure
         endif
         h1s = hs
         h1a = hs
      enddo
      write(*,*)
      write(*,'(a47)') ' Heure (UT) lever, transit et coucher du soleil'
      write(*,'(a47)') ' ----------------------------------------------'
      minute = (Tlevers - int(Tlevers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a16,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Lever : ',&
      int(Tlevers),'h',imn,'mn',isec,'s',' Azimut :',azls
      minute = (Ttransits - int(Ttransits))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a16,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Transit : ',&
      int(Ttransits),'h',imn,'mn',isec,'s',' hauteur :',hs_tra   
      minute = (Tcouchers - int(Tcouchers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a16,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Coucher : ',&
      int(Tcouchers),'h',imn,'mn',isec,'s',' Azimut :',azcs   
          
     
     ! aube et cr�puscule civil
     ! ************************
      ics0 =0
      ils0 =0
      h_tra = 0.
      imax = 24*360
      h0 = Para - 6. - eta1 + eta2
      do i=1,imax+1
         heure = float(i-1)/360.
         call TsapG(imois,iannee,ijour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
         ! Calculs pour le soleil
         call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
         if (i.gt.1) then
            diff = hs - h1s
            if (diff.gt.0) then
               if (hs.le.h0) then
                  if (ils0.eq.0) then
                     ils0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tlevers = heure
                     azls = az
                     itest = i
                  endif
               endif
            else
               if (hs.ge.h0) then
                  if (ics0.eq.0) then
                     ics0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tcouchers = heure
                     azcs = az
                     itest = i
                  endif
               endif
            endif          
         endif
         if (hs.gt.hs_tra) then
            hs_tra = hs
            Ttransits = heure
         endif
         h1s = hs
         h1a = hs
      enddo
      write(*,*)
      write(*,'(a60)') ' Heure (UT) aube et crepuscule civil (-6 deg sous l''horizon)'
      write(*,'(a60)') ' ------------------------------------------------------------'
      minute = (Tlevers - int(Tlevers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a26,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Aube       : ',&
      int(Tlevers),'h',imn,'mn',isec,'s',' Azimut :',azls
      minute = (Tcouchers - int(Tcouchers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a26,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Crepuscule : ',&
      int(Tcouchers),'h',imn,'mn',isec,'s',' Azimut :',azcs   
      
     ! aube et cr�puscule nautique
     ! ***************************
      ics0 =0
      ils0 =0
      h_tra = 0.
      imax = 24*360
      h0 = Para - 12. - eta1 + eta2
      do i=1,imax+1
         heure = float(i-1)/360.
         call TsapG(imois,iannee,ijour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
         ! Calculs pour le soleil
         call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
         if (i.gt.1) then
            diff = hs - h1s
            if (diff.gt.0) then
               if (hs.le.h0) then
                  if (ils0.eq.0) then
                     ils0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tlevers = heure
                     azls = az
                     itest = i
                  endif
               endif
            else
               if (hs.ge.h0) then
                  if (ics0.eq.0) then
                     ics0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tcouchers = heure
                     azcs = az
                     itest = i
                  endif
               endif
            endif          
         endif
         if (hs.gt.hs_tra) then
            hs_tra = hs
            Ttransits = heure
         endif
         h1s = hs
         h1a = hs
      enddo
      write(*,*)
      write(*,'(a64)') ' Heure (UT) aube et crepuscule nautique (-12 deg sous l''horizon)'
      write(*,'(a64)') ' ----------------------------------------------------------------'
      minute = (Tlevers - int(Tlevers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a26,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Aube       : ',&
      int(Tlevers),'h',imn,'mn',isec,'s',' Azimut :',azls
      minute = (Tcouchers - int(Tcouchers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a26,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Crepuscule : ',&
      int(Tcouchers),'h',imn,'mn',isec,'s',' Azimut :',azcs  
     
     ! aube et cr�puscule astronomique
     ! ***************************
      ics0 =0
      ils0 =0
      h_tra = 0.
      imax = 24*360
      h0 = Para - 18. - eta1 + eta2
      do i=1,imax+1
         heure = float(i-1)/360.
         call TsapG(imois,iannee,ijour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
         ! Calculs pour le soleil
         call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
         if (i.gt.1) then
            diff = hs - h1s
            if (diff.gt.0) then
               if (hs.le.h0) then
                  if (ils0.eq.0) then
                     ils0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tlevers = heure
                     azls = az
                     itest = i
                  endif
               endif
            else
               if (hs.ge.h0) then
                  if (ics0.eq.0) then
                     ics0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tcouchers = heure
                     azcs = az
                     itest = i
                  endif
               endif
            endif          
         endif
         if (hs.gt.hs_tra) then
            hs_tra = hs
            Ttransits = heure
         endif
         h1s = hs
         h1a = hs
      enddo
      write(*,*)
      write(*,'(a68)') ' Heure (UT) aube et crepuscule astronomique (-18 deg sous l''horizon)'
      write(*,'(a68)') ' --------------------------------------------------------------------'
      minute = (Tlevers - int(Tlevers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a26,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Aube       : ',&
      int(Tlevers),'h',imn,'mn',isec,'s',' Azimut :',azls
      minute = (Tcouchers - int(Tcouchers))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a26,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Crepuscule : ',&
      int(Tcouchers),'h',imn,'mn',isec,'s',' Azimut :',azcs   
     
     
     ! Calcul des param�tres de la lune
     !***********************************
      call para_lune(TDs,lambda,delta,beta,F_save)
      
      Rtl = delta
      
      ! longitude apparente de la lune
      Lonlun = lambda + dpsi/3600
      ! Ascension droite de la lune
      arg1 = dsin(Lonlun*conv)*dcos(epsilon*conv) - dtan(beta*conv)*dsin(epsilon*conv)
      arg2 = dcos(Lonlun*conv)
      alpha = datan2(arg1,arg2)/conv
      if (alpha.lt.0) alpha = 360. + alpha
      ! D�clinaison
      arg1 = dsin(beta*conv)*dcos(epsilon*conv) + dcos(beta*conv)*dsin(epsilon*conv)*dsin(Lonlun*conv)
      Dec = dasin(arg1)/conv
      
      ! hauteur de la lune : hl
      
      Ah = TsaGs - long - alpha ! angle horaire local
      arg = dsin(lat)*dsin(Dec*conv) + dcos(lat)*dcos(Dec*conv)*dcos(Ah*conv)
      hl = dasin(arg)/conv
      tetal = 90 - hl ! angle z�nithal lunaire
      
      ! Azimuth de la lune : az
      arg1 = dsin(Ah*conv)
      arg2 = dcos(Ah*conv)*dsin(lat)- dtan(Dec*conv)*dcos(lat)
      az = datan2(arg1,arg2)/conv + 180.
      azlun = az
      
      ! fraction de lune illumin�e
      arg1 = dsin(decs*conv)*dsin(Dec*conv) + dcos(decs*conv)*dcos(Dec*conv)*dcos((alphas - alpha)*conv)
      psy = dacos(arg1)/conv
      arg1 = Rs*dsin(psy*conv)
      arg2 = delta - Rs*dcos(psy*conv)
      anglei = datan2(arg1,arg2)     
      ang_phas = anglei/conv
      frac = 0.5*(1+dcos(anglei))
      
      ! Calcul de la libration
      call libration(TDs,dpsi_save,F_save,lambda,beta,lprime,bprime,lprime0,lati,hobs,delta,Dec,alpha,epsilon,Ah,Lonsol_ini,Rs_ini)
    
      
      write(*,*)
      write(*,*)
      write(*,*)
      write(*,'(a24)') ' Parametres pour la lune'
      write(*,'(a24)') ' ***********************'
      write(16,*) 
      write(16,*)
      write(16,*)
      write(16,'(a24)') ' Parametres pour la lune'
      write(16,'(a24)') ' ***********************'
      alphah = alpha/15.  
      minute = (alphah - int(alphah))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(1x,a20,i2,a1,i2,a2,i2,a1)') 'Ascension droite  : ',int(alphah),'h',imn,'mn',isec,'s'
      write(16,'(1x,a20,i2,a1,i2,a2,i2,a1)') 'Ascension droite  : ',int(alphah),'h',imn,'mn',isec,'s'
      minute = (dec - int(dec))*60
      imn = abs(minute)
      write(*,'(1x,a20,i3,a7,i2,a2)') 'Declinaison       :  ',int(Dec),'degres',imn,'mn'
      write(*,*)
      write(*,'(1x,a21,f10.3,a7)') 'Angle horaire  : ',Ah,' degres'
      write(*,'(1x,a21,f10.3,a7)') 'Hauteur        : ', hl,'degres'
      write(*,'(1x,a21,f10.3,a7)') 'Azimuth        : ',az,'degres'
      write(*,'(1x,a21,f10.3,a7)') 'Angle zenithal : ',tetal,'degres'
      write(*,'(1x,a21,f10.3,a7)') 'Angle de phase    : ',ang_phas,'degres'
      write(*,'(1x,a21,f10.3)') 'Fraction illuminee : ',frac   
      write(*,'(1x,a20,i3,a7,i2,a2)') 'Declinaison       :  ',int(Dec),'degres',imn,'mn'
      write(16,*)
      write(16,'(1x,a21,f10.3,a7)') 'Angle horaire  : ',Ah,' degres'
      write(16,'(1x,a21,f10.3,a7)') 'Hauteur        : ', hl,'degres'
      write(16,'(1x,a21,f10.3,a7)') 'Azimuth        : ',az,'degres'
      write(16,'(1x,a21,f10.3,a7)') 'Angle zenithal : ',tetal,'degres'
      write(16,'(1x,a21,f10.3,a7)') 'Angle de phase    : ',ang_phas,'degres'
      write(16,'(1x,a21,f10.3)') 'Fraction illuminee : ',frac
      
      
     ! Lever, Coucher et transit de la lune
     ! *************************************
      icl0 =0
      ill0 =0
      hl_tra = 0.
      imax = 24*360
      do i=1,imax+1
         heure = float(i-1)/360.
         call TsapG(imois,iannee,ijour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
         ! Calculs pour le soleil
         call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
 
          ! Calculs pour la lune
         h0 = 0.125 
         call para_lune(TD,lambda,delta,beta,F)
         Lonlun = lambda + dpsi/3600
         call geom_astr(0,beta,epsilon,Lonlun,TsaG,long,lat,hl,az)
         if (i.gt.1) then
            diff = hl - h1l
            if (diff.gt.0) then
               if (hl.le.h0) then
                  if (ill0.eq.0) then
                     ill0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tleverl = heure
                     azll = az
                     itest = i
                  endif
               endif
            else
               if (hl.ge.h0) then
                  if (icl0.eq.0) then
                     icl0 = 1
                     itest = i-1
                  endif
                  idiff = i-itest
                  if (idiff.eq.1) then
                     Tcoucherl = heure
                     azcl = az
                     itest = i
                  endif
               endif
            endif
         endif
         if (hl.gt.hl_tra) then
            hl_tra = hl
            Ttransitl = heure
         endif
         h1l = hl
      enddo
      
      write(*,*)
      write(*,'(a57,i2,a1,i2)') ' Heure (UT) lever, transit et coucher de la lune pour le ',ijour,'/',imois
      write(*,'(a64)') ' ----------------------------------------------------------------'
      minute = (Tleverl - int(Tleverl))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a16,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Lever : ',&
      int(Tleverl),'h',imn,'mn',isec,'s',' Azimut :',azll
      minute = (Ttransitl - int(Ttransitl))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a16,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Transit : ',&
      int(Ttransitl),'h',imn,'mn',isec,'s',' hauteur :',hl_tra  
      minute = (Tcoucherl - int(Tcoucherl))*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      write(*,'(a16,i2,a1,i2,a2,i2,a1,a9,f10.3)') 'Coucher : ',&
      int(Tcoucherl),'h',imn,'mn',isec,'s',' Azimut :',azcl     
      
      
      ! Calcul phases de la lune
      
      write(*,*)
      write(*,'(a50,i2)') ' Phases (a un jour pres) de la lune pour le mois :',imois
      write(*,'(a52)') ' ---------------------------------------------------'
      ! initialisation
      heure = 0.
      if (imois.eq.1) then
         init_annee = iannee -1
         init_mois = 12
         init_jour = 31
      else
         init_annee = iannee
         init_mois = imois-1
         init_jour = nmois(init_mois)
      endif
      call TsapG(init_mois,init_annee,init_jour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
      call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
      arg1 = dcos(epsilon*conv)*sin(Lonsol*conv)
      arg2 = dcos(Lonsol*conv)
      alphas = datan2(arg1,arg2)/conv
      if (alpha.lt.0) alphas = 360.d0 + alphas
      Decs = asin(sin(epsilon*conv)*sin(Lonsol*conv))/conv
      call para_lune(TD,lambda,delta,beta,F)
      Lonlun = lambda + dpsi/3600
      call geom_astr(0,beta,epsilon,Lonlun,TsaG,long,lat,hl,az)
      arg1 = dsin(Lonlun*conv)*dcos(epsilon*conv) - dtan(beta*conv)*dsin(epsilon*conv)
      arg2 = dcos(Lonlun*conv)
      alpha = datan2(arg1,arg2)/conv
      if (alpha.lt.0) alpha = 360. + alpha
      arg1 = dsin(beta*conv)*dcos(epsilon*conv) + dcos(beta*conv)*dsin(epsilon*conv)*dsin(Lonlun*conv)
      Dec = dasin(arg1)/conv
      arg1 = dsin(decs*conv)*dsin(Dec*conv) + dcos(decs*conv)*dcos(Dec*conv)*dcos((alphas - alpha)*conv)
      psy = dacos(arg1)/conv
      arg1 = Rs*dsin(psy*conv)
      arg2 = delta - Rs*dcos(psy*conv)
      anglei = datan2(arg1,arg2)
      frac1 = 0.5*(1+dcos(anglei))
      
      
      nl = 2
      pl = -1
      pc = 2
      dc = -1
      itnl1 = 0
      itpl1 = 0
      ipc1 = 0
      idc1 = 0
      ipl = 0
      inl = 0
      ipc = 0
      idc = 0
      imax = nmois(imois)
      jjour = 0
      itest = 0
      do while (itest.ne.4)
         jjour = jjour+1
         heure = 0.
         call TsapG(imois,iannee,jjour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)       
         ! Calculs pour le soleil
         call geom_astr(1,beta,epsilon,Lonsol,TsaG,long,lat,hs,az)
         arg1 = dcos(epsilon*conv)*dsin(Lonsol*conv)
         arg2 = dcos(Lonsol*conv)
         alphas = datan2(arg1,arg2)/conv
         if (alpha.lt.0) alphas = 360.d0 + alphas
         Decs = asin(sin(epsilon*conv)*sin(Lonsol*conv))/conv
         call para_lune(TD,lambda,delta,beta,F)
         Lonlun = lambda + dpsi/3600
         call geom_astr(0,beta,epsilon,Lonlun,TsaG,long,lat,hl,az)
         arg1 = dsin(Lonlun*conv)*dcos(epsilon*conv) - dtan(beta*conv)*dsin(epsilon*conv)
         arg2 = dcos(Lonlun*conv)
         alpha = datan2(arg1,arg2)/conv
         if (alpha.lt.0) alpha = 360. + alpha
         arg1 = dsin(beta*conv)*dcos(epsilon*conv) + dcos(beta*conv)*dsin(epsilon*conv)*dsin(Lonlun*conv)
         Dec = dasin(arg1)/conv
         arg1 = dsin(decs*conv)*dsin(Dec*conv) + dcos(decs*conv)*dcos(Dec*conv)*dcos((alphas - alpha)*conv)
         psy = dacos(arg1)/conv
         arg1 = Rs*dsin(psy*conv)
         arg2 = delta - Rs*dcos(psy*conv)
         anglei = datan2(arg1,arg2)
         frac = 0.5*(1+dcos(anglei))
         diff = frac - frac1
         !if (frac.ge.pl) then
         if (diff.ge.0) then
            pl = frac
            iTpl = jjour
            ipl = 1
            if (frac.le.0.5) then
               pc = frac
               itpc = jjour
               ipc = 1
            endif
         endif
         if (diff.lt.0) then
            if (pl.ge.0) then
               itpl1 = itpl
               pl = -1
               write(*,'(a14,i2,a1,i2)') ' Pleine lune :',itpl1,'/',imois
            endif
            if (pc.lt.2) then
               itpc1 = itpc
               pc = 2
               write(*,'(a19,i2,a1,i2)') ' Premier quartier :',itpc1,'/',imois
            endif
         endif
       !  if (frac.le.nl) then
         if (diff.le.0) then
            nl = frac
            iTnl = jjour
            inl = 1
            if (frac.ge.0.5) then
               dc = frac
               itdc = jjour
               idc = 1
            endif
         endif
         if (diff.gt.0) then
            if (nl.le.1) then
               itnl1 = itnl
               nl = 2
               write(*,'(a16,i2,a1,i2)') ' Nouvelle lune :',itnl1,'/',imois
            endif
            if (dc.ge.0) then
               itdc1 = itdc
               dc = -1
               write(*,'(a19,i2,a1,i2)') ' Dernier quartier :',itdc1,'/',imois
            endif
         endif
         frac1 = frac
         itest = inl+ipl+ipc+idc
      enddo
      
      end
!c----------------------------------------------------------------------------      
      subroutine Julien(imois,iannee,ijour,heure,JD,TD)
      
      real * 8 heure
      real * 8 jour,JD,TD,mois
      
      if (imois.le.2) then
         Y = iannee - 1
         Mois = float(imois + 12)
      else
         Y = iannee
         Mois = float(imois)
      endif
      jour = float(ijour) + heure/24.
      A = int(Y/100)
      B = 2 - A + int(A/4)
      if ((iannee.ge.1900).and.(iannee.lt.2100)) B = -13
      JD = int(365.25d0*(Y+4716)) + int(30.6001*(Mois+1)) + jour +B - 1524.5   
      TD = (JD - 2451545.d0)/36525.d0  ! temps en si�cle Julien
      end  
!c------------------------------------------------------------------------
      subroutine tsgm(JD,TD,TSG)
      
      real * 8 TD,JD,TsG,tamp
      
      TSG = 280.46061837d0 + 360.98564736629d0*(JD - 2451545.0d0) + 0.000387933d0*(TD**2) - (TD**3)/38710000d0
      if (TsG.lt.0.) then
         iTsG = int(abs(TsG)/360.d0) + 1
         TsG = TsG + 360.d0*float(iTsG)
      endif
      tamp = Tsg
      if (tamp.gt.360.) then
         TsG = (tamp/360.d0 - int(tamp/360.d0))*360.d0
      endif
      end
!---------------------------------------------------------------------------------
       subroutine ecliptic(TD,Lonsol,dpsi,epsilon)
       
       real * 8 TD,eps0,U,dpsi,deps,omega,lonlun,epsilon,Lonsol
       real * 8 pi,conv
       
       pi = dacos(-1.d0)
       conv = pi/180.d0
       
       ! obliquit� de l'�cliptique : eps0
       U = TD/100.d0
       eps0 = 84381.448d0 - 4680.93d0*U - 1.55d0*u*u + 1999.25d0*(u**3)- 51.38d0*(u**4) - 249.67d0*(u**5) - 39.05d0*(u**6) + 7.12d0*(u**7) + 27.87d0*(u**8) + 5.79d0*(u**9) + 2.45d0*(u**10)
       eps0 = eps0/3600.d0
       ! Longitude du noeud ascendant de l'orbite moyenne de la lune : omega
       omega = 125.04452d0 - 1934.136261d0*TD
       ! longitude moyenne de la lune : lonlun
       lonlun = 218.3165d0 + 481267.8813d0*TD
       ! nutation en longitude : dpsi (en secondes)
       dpsi = -17.20d0*dsin(omega*conv) - 1.32d0*dsin(2*Lonsol*conv) - 0.23d0*dsin(2*lonlun*conv) + 0.21d0*dsin(2*omega*conv)
       ! nutation en obliquit� : deps (en secondes)
       deps = 9.20d0*dcos(omega*conv) + 0.57d0*dcos(2*Lonsol*conv) + 0.10d0*dcos(2*lonlun*conv) + 0.09d0*dcos(2*omega*conv)
       epsilon = eps0 + deps/3600.d0  

       end
       
!---------------------------------------------------------------------------------------------------
      subroutine long_sol(TD,Lonsol,Rs)
      
      real * 8 TD,L0,cent,amoy,Lonsol,av,ex,Rs,pi,conv
      
       pi = dacos(-1.d0)
       conv = pi/180.d0
      
      ! Coordonn�es Solaires
       !*********************
       ! longitude g�om�trique moyenne par rapport � l'�quinoxe moyenne : L0
       L0 = 280.4664567d0 + 36000.76982779d0*TD + 0.0003032028d0*TD*TD  + (TD**3)/49931.d0 - (TD**4)/15299.d0 - (TD**5)/1988000.d0
       if (L0.gt.360.) then
          il0 = int(L0/360.d0)
          L0 = L0 - 360.d0*float(il0)
       endif     
       if (L0.lt.0.) then
          il0 = int(abs(L0)/360.d0) + 1
          L0 = L0 + 360.d0*float(il0)
       endif
       if (L0.gt.360.) then
       write(*,*) 'Probleme calcul longitude soleil'
       endif  
       ! anomalie moyenne du soleil : amoy
       amoy = 357.52910d0 + 35999.05030d0*TD - 0.0001559d0*TD*TD - 0.00000048d0*TD*TD*TD  
       if (amoy.gt.360.) then
          im0 = int(amoy/360.d0)
          amoy = amoy - 360.d0*float(im0)
       endif     
       if (amoy.lt.0.) then
          im0 = int(abs(amoy)/360.d0) + 1
          amoy = amoy + 360.d0*float(im0)
       endif
       if (amoy.gt.360.) then
       write(*,*) 'Probleme calcul M'
       endif   
       ! Equation du soleil du centre : cent
       cent = (1.914600d0 - 0.004817d0*TD - 0.000014d0*TD*TD)*dsin(amoy*conv) + (0.019993d0 - 0.00010d0*TD)*dsin(2*amoy*conv)+ 0.000290d0*dsin(3*amoy*conv)
       ! Longitude solaire : Lonsol
       Lonsol = L0 + cent
       ! excenticit� de l'orbite de la Terre
       ex = 0.016708617d0 - 0.000042037d0*T - 0.0000001236d0*t*t
       ! anomalie vraie
       av = amoy + cent
       ! distance Terre - soleil (1 UA = 149 597 871 km)
       Rs = 1.000001018d0*(1.-(ex*ex))/(1d0+(ex*dcos(av*conv)))*149597871.d0
       
       end
! ---------------------------------------------------------------------------------------
       subroutine para_lune(T,lambda,delta,beta,F)
       
       real *8 Lp,D,M,Mp,F,A1,A2,A3,E,T,lambda,delta,beta,pi,conv,arg,coefm
       integer arD(60),arM(60),arMp(60),arF(60),coefl(60),coefr(60)
       integer arDb(60),arMb(60),arMpb(60),arFb(60),coefb(60)
       
       data arD /0,2,2,0,0,0,2,2,2,2,0,1,0,2,0,0,4,0,4,2,2,1,1,2,2,4,2,0,2,2,1,2,0,0,2,2,2,4,0,3,2,4,0,2,2,2,4,0,4,1,2,0,1,3,4,2,0,1,2,2/
       data arM /0,0,0,0,1,0,0,-1,0,-1,1,0,1,0,0,0,0,0,0,1,1,0,1,-1,0,0,0,1,0,-1,0,-2,1,2,-2,0,0,-1,0,0,1,-1,2,2,1,-1,0,0,-1,0,1,0,1,0,0,-1,2,1,0,0/
       data arMp/1,-1,0,2,0,0,-2,-1,1,0,-1,0,1,0,1,1,-1,3,-2,-1,0,-1,0,1,2,0,-3,-2,-1,-2,1,0,2,0,-1,1,0,-1,2,-1,1,-2,-1,-1,-2,0,1,4,0,-2,0,2,1,-2,-3,2,1,-1,3,-1/
       data arF /0,0,0,0,0,2,0,0,0,0,0,0,0,-2,2,-2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,-2,2,0,2,0,0,0,0,0,0,-2,0,0,0,0,-2,-2,0,0,0,0,0,0,0,-2/
       data coefl/6288774,1274027,658314,213618,-185116,-114332,58793,57066,53322,&
       45758,-40923,-34720,-30383,15327,-12528,10980,10675,10034,8548,-7888,-6766,&
       -5163,4987,4036,3994,3861,3665,-2689,-2602,2390,-2348,2236,-2120,-2069,2048,&
       -1773,-1595,1215,-1110,-892,-810,759,-713,-700,691,596,549,537,520,-487,-399,&
       -381,351,-340,330,327,-323,299,294,0/
       data coefr/-20905355,-3699111,-2955968,-569925,48888,-3149,246158,-152138,&
       -170733,-204586,-129620,108743,104755,10321,0,79661,-34782,-23210,-21636,&
       24208,30824,-8379,-16675,-12831,-10445,-11650,14403,-7003,0,10056,6322,-9884,&
       5751,0,-4950,4130,0,-3958,0,3258,2616,-1897,-2117,2354,0,0,-1423,-1117,-1571,-1739,&
       0,-4421,0,0,0,0,1165,0,0,8752/
       
       data arDb /0,0,0,2,2,2,2,0,2,0,2,2,2,2,2,2,2,0,4,0,0,0,1,0,0,0,1,0,4,4,0,4,2,&
       2,2,2,0,2,2,2,2,4,2,2,0,2,1,1,0,2,1,2,0,4,4,1,4,1,4,2/
       data arMb /0,0,0,0,0,0,0,0,0,0,-1,0,0,1,-1,-1,-1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,&
       0,0,-1,0,0,0,0,1,1,0,-1,-2,0,1,1,1,1,1,0,-1,1,0,-1,0,0,0,-1,-2/
       data arMpb/0,1,1,0,-1,-1,0,2,1,2,0,-2,1,0,-1,0,-1,-1,-1,0,0,-1,0,1,1,0,0,3,0,-1,&
       1,-2,0,2,1,-2,3,2,-3,-1,0,0,1,0,1,1,0,0,-2,-1,1,-2,2,-2,-1,1,1,-1,0,0/
       data arFb /1,1,-1,-1,1,-1,1,1,-1,-1,-1,-1,1,-1,1,1,-1,-1,-1,1,3,1,1,1,-1,-1,-1,1,&
       -1,1,-3,1,-3,-1,-1,1,-1,1,-1,1,1,1,1,-1,3,-1,-1,1,-1,-1,1,-1,1,-1,-1,-1,-1,-1,-1,1/
       data coefb/5128122,280602,277693,173237,55413,46271,32573,17198,9266,8822,8216,&
       4324,4200,-3359,2463,2211,2065,-1870,1828,-1794,-1749,-1565,-1491,-1475,-1410,&
       -1344,-1335,1107,1021,833,777,671,607,596,491,-451,439,422,421,-366,-351,331,315,&
       302,-283,-229,223,223,-220,-220,-185,181,-177,176,166,-164,132,-119,115,107/

       pi = dacos(-1.d0)
       conv = pi/180.d0
              
       ! Moon's mean longitude, referred to the mean equinox of the date       
       Lp = 218.3164591d0 + 481267.88134236d0*T - 0.0013268d0*(T**2) + (T**3)/538841d0 - (T**4)/65194000d0
       if (Lp.gt.360.d0) then
          im0 = int(Lp/360.d0)
          Lp = Lp - 360.d0*float(im0)
       endif     
       if (Lp.lt.0.) then
          im0 = int(abs(Lp)/360.d0) + 1
          Lp = Lp + 360.d0*float(im0)
       endif
       ! Mean elongation of the Moon
       D = 297.8502042d0 + 445267.1115168d0*T - 0.0016300d0*(T**2) + (T**3)/545868d0 - (T**4)/113065000d0
       if (D.gt.360.d0) then
          im0 = int(D/360.d0)
          D = D - 360.d0*float(im0)
       endif     
       if (D.lt.0.) then
          im0 = int(abs(D)/360.d0) + 1
          D = D + 360.d0*float(im0)
       endif
       ! Sun�s mean anomaly
       M = 357.5291092d0 + 35999.0502909d0*T - 0.0001536d0*(T**2)+ (T**3)/24490000d0
       if (M.gt.360.d0) then
          im0 = int(M/360.d0)
          M = M - 360.d0*float(im0)
       endif     
       if (M.lt.0.) then
          im0 = int(abs(M)/360.d0) + 1
          M = M + 360.d0*float(im0)
       endif
       ! Moon�s mean anomaly
       Mp = 134.9634114d0 + 477198.8676313d0*T + 0.0089970d0*(T**2) + (T**3)/69699d0 - (T**4)/14712000d0
       if (Mp.gt.360.d0) then
          im0 = int(Mp/360.d0)
          Mp = Mp - 360.d0*float(im0)
       endif     
       if (Mp.lt.0.) then
          im0 = int(abs(Mp)/360.d0) + 1
          Mp = Mp + 360.d0*float(im0)
       endif
       ! Moon�s argument of latitude (mean distance of the Moon from its ascending node)
       F = 93.2720993d0 + 483202.0175273d0*T - 0.0034029d0*(T**2) - (T**3)/3526000d0 + (T**4)/863310000d0
       if (F.gt.360.d0) then
          im0 = int(F/360.d0)
          F = F - 360.d0*float(im0)
       endif     
       if (F.lt.0.) then
          im0 = int(abs(F)/360.d0) + 1
          F = F + 360.d0*float(im0)
       endif
       ! Three further arguments (again, in degrees) are needed
       A1 = 119.75d0 + 131.849d0*T
       if (A1.gt.360.d0) then
          im0 = int(A1/360.d0)
          A1 = A1 - 360.d0*float(im0)
       endif     
       if (A1.lt.0.d0) then
          im0 = int(abs(A1)/360.d0) + 1
          A1 = A1 + 360.d0*float(im0)
       endif
       A2 = 53.09d0 + 479264.290d0*T
       if (A2.gt.360.d0) then
          im0 = int(A2/360.d0)
          A2 = A2 - 360.d0*float(im0)
       endif     
       if (A2.lt.0.) then
          im0 = int(abs(A2)/360.) + 1
          A2 = A2 + 360.*float(im0)
       endif
       A3 = 313.45d0 + 481266.484d0*T
       if (A3.gt.360.d0) then
          im0 = int(A3/360.d0)
          A3 = A3 - 360.d0*float(im0)
       endif     
       if (A3.lt.0.) then
          im0 = int(abs(A3)/360.d0) + 1
          A3 = A3 + 360.d0*float(im0)
       endif
       ! excentricite orbite de la terre autour du soleil
       E = 1d0 - 0.002516d0*T - 0.0000074d0*(T**2)
       
       suml = 0.
       sumr = 0.
       do i =1,60
          arg = arD(i)*D + arM(i)*M + arMP(i)*Mp + arF(i)*F
          coefm = abs(arM(i))
          if (coefm.ne.0) then
             coef = E
             if (2*coefm.eq.4) coef = E*E
          else
             coef = 1d0
          endif
          suml = suml + coef*coefl(i)*dsin(arg*conv)
          sumr = sumr + coef*coefr(i)*dcos(arg*conv)
       enddo   
       sumb = 0.
       do i =1,60
          arg = arDb(i)*D + arMb(i)*M + arMPb(i)*Mp + arFb(i)*F
          coefm = abs(arMb(i))
          if (coefm.ne.0) then
             coef = E
             if (2*coefm.eq.4) coef = E*E
          else
             coef = 1
          endif
          sumb = sumb + coef*coefb(i)*dsin(arg*conv)
       enddo
       sumb = sumb -2235*dsin(LP*conv) + 382*dsin(A3*conv) + 175*dsin((A1-F)*conv) + 175*dsin((A1+F)*conv) + 127*dsin((LP-MP)*conv) - 115*dsin((LP+MP)*conv)
       suml = suml + 3958*dsin(A1*conv) + 1962*dsin((Lp-F)*conv) + 318*dsin(A2*conv)
       
       lambda = Lp + suml/1e6           ! longitude g�ocentrique du centre de la lune
       beta = sumb/1e6                  ! latitude g�ocentrique du centre de la lune
       delta = 385000.56d0 + sumr/1e3   ! distance entre les centres de la Terre et de la lune
             
       end
       
!------------------------------------------------------------------------     
      subroutine libration(T,dpsi,F,lambda,beta,lprime,bprime,lprime0,lati,hobs,delta,Dec,alpha,epsilon,Ah,Lonsol,Rs)      
      
       !!!!!!!!! Calcul des coordonn�es s�l�nographique de l'Observateur et du soleil 
       !         pour le calcul de la r�flectance lunaire  !!!!!!!!!!!!!!!!!
       ! Calcul de la longitude et de la latitude s�l�nographique de la lune en coordonn�es topocentriques
       ! Calcul de la longitude S�l�nographique du Soleil 
       
      real *8 T,omega,dpsi,F,epsilon,Incl,W,A,arg1,arg2,lambda,beta
      real *8 lprime,pi,conv,sinbp,lati_geo,u,ro_cos,ro_sin,ro,sin_para,para
      real *8 delta,Dec,alpha,d_alpha,alpha_prim,Dec_prim,Ah,bprime,lprime0
      real *8 lambda_prim,beta_prim,Lonsol,lambda0,lambdaH,betaH
      real    lati,hobs,Rs
      real *8 M,Mp,D,MM,Mpp,DD,FF,ro_lib,sig_lib,V,om,sinP,P,Q,cosz,Z,pi_prim,delta_l,delta_b
       
      pi = dacos(-1.d0)
      conv = pi/180.d0
       
      ! Incl : inclinaison de l'�quateur moyen de la lune par rapport � l'ecliptique
      Incl = 1.54242d0   ! en degr�s
      ! Longitude du noeud ascendant de l'orbite moyenne de la lune : omega
      omega = 125.044555d0 - 1934.1361849d0*T + 0.0020762d0*T*T + T*T*T/467410 - T*T*T*T/60616000
      W = lambda - dpsi/3600d0 - omega
      if (W.gt.360.d0) then
         im0 = int(W/360.d0)
         W = W - 360.d0*float(im0)
      endif     
      if (W.lt.0.d0) then
         im0 = int(abs(W)/360.d0) + 1
         W = W + 360.d0*float(im0)
      endif
      arg1 = dsin(W*conv)*dcos(beta*conv)*dcos(Incl*conv) - dsin(beta*conv)*dsin(Incl*conv)
      arg2 = dcos(W*conv)*dcos(beta*conv)
      A = datan2(arg1,arg2)/conv
      if (A.gt.360.d0) then
         im0 = int(A/360.d0)
         A = A - 360.d0*float(im0)
      endif     
      if (A.lt.0.d0) then
         im0 = int(abs(A)/360.d0) + 1
         A = A + 360.d0*float(im0)
      endif
      lprime = A - F  ! libration en longitude
      if (lprime.gt.180d0) lprime = 360d0 - lprime   ! convention de l'UAI
      
      sinbp = -dsin(W*conv)*dcos(beta*conv)*dsin(Incl*conv) - dsin(beta*conv)*dcos(Incl*conv)
      bprime = dasin(sinbp)/conv  ! libration en latitude
      if (bprime.gt.360.d0) then
         im0 = int(bprime/360.d0)
         bprime = bprime - 360.d0*float(im0)
      endif     
      
      write(*,*) 'long',lprime,'lat',bprime
   
      ! Pour le calcul des longitudes et latitudes s�l�nographiques de l'observateur, 
      ! on passe en coordonn�es topocentriques
      ! Rappel : G�ocentrique = r�f�rence au centre de la Terre
      !          Topocentrique = r�f�rence � la position de l'observateur
      
      ! On calcule par correction diff�rentielle
      ! Mean elongation of the Moon
      D = 297.8502042d0 + 445267.1115168d0*T - 0.0016300d0*(T**2) + (T**3)/545868d0 - (T**4)/113065000d0
      if (D.gt.360.d0) then
         im0 = int(D/360.d0)
         D = D - 360.d0*float(im0)
      endif     
      if (D.lt.0.) then
         im0 = int(abs(D)/360.d0) + 1
         D = D + 360.d0*float(im0)
      endif
      ! Sun�s mean anomaly
      M = 357.5291092d0 + 35999.0502909d0*T - 0.0001536d0*(T**2)+ (T**3)/24490000d0
      if (M.gt.360.d0) then
         im0 = int(M/360.d0)
         M = M - 360.d0*float(im0)
      endif     
      if (M.lt.0.) then
         im0 = int(abs(M)/360.d0) + 1
         M = M + 360.d0*float(im0)
      endif
      ! Moon�s mean anomaly
      Mp = 134.9634114d0 + 477198.8676313d0*T + 0.0089970d0*(T**2) + (T**3)/69699d0 - (T**4)/14712000d0
      if (Mp.gt.360.d0) then
         im0 = int(Mp/360.d0)
         Mp = Mp - 360.d0*float(im0)
      endif     
      if (Mp.lt.0.) then
         im0 = int(abs(Mp)/360.d0) + 1
         Mp = Mp + 360.d0*float(im0)
      endif
      MM = M*conv
      Mpp = Mp*conv
      DD = D*conv 
      FF = F*conv
      ro_lib = -0.02752d0*dcos(Mpp)- 0.02245d0*dsin(FF) + 0.00684d0*dcos(Mpp-2d0*FF) - 0.00293d0*dcos(2d0*FF)-0.00085d0*dcos(2d0*FF - 2d0*DD) - 0.00054d0*dcos(Mpp-2d0*DD) - 0.0002d0*(dsin(Mpp+FF) + dcos(Mpp+2d0*FF) + dcos(Mpp-FF)) + 0.00014*dcos(Mpp+2d0*(FF-DD))
      sig_lib = -0.02816d0*dsin(Mpp)+ 0.02244d0*dcos(FF) - 0.00682d0*dsin(Mpp-2d0*FF) - 0.00279d0*dsin(2d0*FF)-0.00083d0*dsin(2d0*FF - 2d0*DD) + 0.00069d0*dsin(Mpp-2d0*DD) + 0.0004d0*dcos(Mpp+FF) - 0.00025d0*dsin(2d0*Mpp) -0.00023*dsin(Mpp+2d0*FF) + 0.0002d0*dcos(Mpp-FF) + 0.00019d0*dsin(Mpp-FF) + 0.00013d0*dsin(Mpp+2d0*(FF-DD)) - 0.0001d0*dcos(Mpp-3d0*FF)
      V = omega + dpsi/3600 + sig_lib/dsin(Incl*conv)  !! dpsi en secondes
      arg1 = dsin((Incl+ro_lib)*conv)*dsin(V*conv)
      arg2 = dsin((Incl+ro_lib)*conv)*dcos(V*conv)*dcos(epsilon*conv) - dcos((Incl+ro_lib)*conv)*dsin(epsilon*conv)
      om = datan2(arg1,arg2)/conv
      if (om.gt.360.d0) then
         im0 = int(M/360.d0)
         om = om - 360.d0*float(im0)
      endif     
      if (om.lt.0.) then
         im0 = int(abs(om)/360.d0) + 1
         om = om + 360.d0*float(im0)
      endif
      sinP = sqrt(arg1*arg1 + arg2*arg2)*dcos((alpha-om)*conv)/dcos(bprime*conv)
      P = dasin(sinP)/conv
      arg1 = dcos(lati*conv)*dsin(Ah*conv)
      arg2 = dcos(Dec*conv)*dsin(lati*conv) - dsin(Dec*conv)*dcos(lati*conv)*dcos(Ah*conv)
      Q = datan2(arg1,arg2)/conv
      cosz = dsin(Dec*conv)*dsin(lati*conv) + dcos(Dec*conv)*dcos(lati*conv)*dcos(Ah*conv)
      Z = dacos(cosz)
      ga = 6378.14  ! grand axe
      sin_para = ga/delta          ! parallaxe �quatoriale pour la lune (delta : distance au centre de la lune
      para = dasin(sin_para)/conv
      pi_prim = para*(dsin(z) + 0.0084d0*dsin(2d0*z))
      delta_l = -pi_prim*dsin((Q-P)*conv)/dcos(bprime*conv)
      delta_b = pi_prim*dcos((Q-P)*conv)
      lprime = lprime+delta_l
      bprime = bprime+delta_b
      
      ! Calcul de la longitude s�l�nographique du soleil
      lambda0 = Lonsol - 0.00569d0 - 0.00478d0*dsin(omega*conv)  ! longitude g�ocentrique apparente du solei
      lambdaH = lambda0 + 180.d0 + delta/Rs*57.296d0*dcos(beta*conv)*dsin((lambda0-lambda)*conv)
      betaH = delta/Rs*beta
      W = lambdaH - dpsi/3600d0 - omega
      if (W.gt.360.d0) then
         im0 = int(W/360.d0)
         W = W - 360.d0*float(im0)
      endif     
      if (W.lt.0.d0) then
         im0 = int(abs(W)/360.d0) + 1
         W = W + 360.d0*float(im0)
      endif
      arg1 = dsin(W*conv)*dcos(betaH*conv)*dcos(Incl*conv) - dsin(betaH*conv)*dsin(Incl*conv)
      arg2 = dcos(W*conv)*dcos(betaH*conv)
      A = datan2(arg1,arg2)/conv
      if (A.gt.360.d0) then
         im0 = int(A/360.d0)
         A = A - 360.d0*float(im0)
      endif     
      if (A.lt.0.d0) then
         im0 = int(abs(A)/360.d0) + 1
         A = A + 360.d0*float(im0)
      endif
      lprime0 = A - F  ! longitude s�l�nographique du soleil
      if (lprime0.gt.180d0) lprime0 = 360d0 - lprime   ! convention de l'UAI
      
      end
!-----------------------------------------------------------------------------------           
       
      subroutine calc_T(Lon_astr,epsilon,beta,TsaG0,h0,lat,long,etat,Time,az)
      
      real long
      real * 8 pi,conv,lat
      real * 8 arg1,arg2,Dec,Ah,alpha,beta
      real * 8 Lon_astr,time
      real * 8 TsG,epsilon,TsaG,TsL,TsaG0,h0
      character * 1 etat
      
      pi = dacos(-1.d0)
      conv = pi/180.d0
      
      ! Ascension droite de l'astre au temps TD
      arg1 = dsin(Lon_astr*conv)*dcos(epsilon*conv) - dtan(beta*conv)*dsin(epsilon*conv)
      arg2 = dcos(Lon_astr*conv)
      alpha = datan2(arg1,arg2)/conv
      if (alpha.lt.0) alpha = 360. + alpha
      ! D�clinaison de l'astre au temps TD
      arg1 = dsin(beta*conv)*dcos(epsilon*conv) + dcos(beta*conv)*dsin(epsilon*conv)*dsin(Lon_astr*conv)
      Dec  = dasin(arg1)/conv
      
      arg1 = dsin(lat)*dsin(Dec*conv)
      arg2 = dcos(lat)*dcos(Dec*conv)
      Ah = dacos((dsin(h0*conv)-arg1)/arg2)/conv ! angle horaire entre la position du lieu et l'astre � son coucher
      
      arg1 = dsin(Ah*conv)
      arg2 = dcos(Ah*conv)*dsin(lat)- dtan(Dec*conv)*dcos(lat)
      az = datan2(arg1,arg2)/conv
      if (etat.eq.'c') then
         Tsl = alpha + Ah ! temps sid�ral local au moment du coucher
      else
         Tsl = alpha - Ah ! temps sid�ral local au moment du lever
      endif
      Tsg = Tsl + long ! temps sid�ral � Greenwich au moment du coucher
      Ta = (Tsl - TsaG0)/15. ! Heure UT approch�e au moment du coucher
      Time = Ta
      
      end
!---------------------------------------------------------------------------
      subroutine TsapG(imois,iannee,ijour,heure,Lonsol,dpsi,epsilon,TD,TsaG,Rs)
      
      real * 8 heure,pi,conv
      real * 8 TD,JD,Lonsol,Rs,TsaG,TSG,dpsi,epsilon
      
      pi = dacos(-1.d0)
      conv = pi/180.d0

      call Julien(imois,iannee,ijour,heure,JD,TD)      
      call tsgm(JD,TD,TSG)
      call long_sol(TD,Lonsol,Rs)
      call ecliptic(TD,Lonsol,dpsi,epsilon)
      TsaG = TsG + dpsi*dcos(epsilon*conv)/3600.
      end
!------------------------------------------------------------------------------
      subroutine geom_astr(isol,beta,epsilon,Lon_astr,TsaG,long,lat,ha,az)
      
      real   long
      real * 8 pi,conv,lat
      real * 8 arg,arg1,arg2,Dec,Ah,ha
      real * 8 alpha,epsilon,beta,TsaG,Lon_astr
      
      pi = dacos(-1.d0)
      conv = pi/180.d0
    
      if (isol.eq.1) then   
         arg1 = dcos(epsilon*conv)*dsin(Lon_astr*conv)
         arg2 = dcos(Lon_astr*conv)
         Dec = asin(dsin(epsilon*conv)*dsin(Lon_astr*conv))/conv
      else
         arg1 = dsin(beta*conv)*dcos(epsilon*conv) + dcos(beta*conv)*dsin(epsilon*conv)*dsin(Lon_astr*conv)
         Dec = dasin(arg1)/conv
         arg1 = dsin(Lon_astr*conv)*dcos(epsilon*conv) - dtan(beta*conv)*dsin(epsilon*conv)
         arg2 = dcos(Lon_astr*conv)
      endif
      alpha = datan2(arg1,arg2)/conv
      if (alpha.lt.0) alpha = 360.d0 + alpha
      Ah = TsaG - long - alpha ! angle horaire local
      arg = dsin(lat)*dsin(Dec*conv) + dcos(lat)*dcos(Dec*conv)*dcos(Ah*conv)
      ha = dasin(arg)/conv
      arg1 = dsin(Ah*conv)
      arg2 = dcos(Ah*conv)*dsin(lat)- dtan(Dec*conv)*dcos(lat)
      az = datan2(arg1,arg2)/conv + 180.
      end
