subroutine geometrie(pmoy,tmoy,dstmol,dstcfc,dstcont,alt,hum_rel,&
atmp,ncouche,inua,itype,altb,alts,ialtb,ialts,hbg,epg,ep_OH,ep_OI,&
ep_Na,nom_prof,path,nom_nua,i120)

!     SOUS PROGRAMME DE CALCUL DES DONNEES THERMODYNAMIQUES 
!     POUR CHAQUE COUCHE ATMOSPHERIQUE

!     Modifications f�vrier 2017 : prise en compte OI, Na


      include 'parameter.rayjn'

      real alt(laythr+1),patm(laythr+1),tatm(laythr+1)
      real dstair(laythr+1),rapmel(nmolmx,laythr+1)
      real dstcont(laythr,ncont),hum_rel(laythr+1)
      real pmoy(laythr),tmoy(laythr)
      real dstmol(laythr,nspc),dstcfc(laythr,mmolx)
      real tetaobs,hobs,hfin,atmp(2,knln),hbg,epg
      REAL(KIND=8), DIMENSION(laythr) :: ep_OH,ep_OI,ep_Na

      real boltz,losch,h1,h2,hmin,hmax,z1,z2,pa,pb,ta,tb,roa,rob
      real hp,hro,ds,dz,dsta,dstb,hd,dstmoy,rapa,rapb,hden,mah2o
      
      character * 1 type
      character * 50 nom_prof,nom_nua
      character * 30 path

!     losch = nb de Loschmidt en molecules/cm3 = ro(P0,T0) = P0/(k*T0)
!
!     calcul des valeurs moyennes en utilisant P = ro*boltz*T*cst
!               avec : P = pression en mb = 1hPa = 1E-2 Pa = 1E-2 N/m2
!                      ro = densite moleculaire en molecules/cm3
!                      boltz = k = cste de Boltzmann 
!                            = 1.3807E-23 J/deg. (N.m/deg. car 1 J=1 N.m)
!                      T = temperature en K                 
!                      cst = 1E6/1E2 = 1E4 = conversion -> 1 mb = 1E2 N/m2 (1 hPa) 
!                                                       -> 1 m3 = 1E6 cm-3
      data losch /2.6868E19/
      data boltz,cst,p0,t0,avog,mah2o /1.3807E-23,1.E4,1013.25,273.15,&
      6.022136e+23,18.01534/
      ! mah2o = masse molaire (g) de H2O
      boltz = boltz*cst  ! conversion pour travailler en mb et en cm-3
        
!    lecture des parametres atmospheriques
!     alt = altitude en km
!     patm = pression en mb
!     tatm = temperature en K
!     dstair = densite de l'air en cm-3
!     rapmel = rapport de melange en ppmv

      call lecdo(laythr,nmolmx,nalt,alt,patm,tatm,dstair,rapmel,nmol,&
      ncfc,inua,itype,altb,alts,ialtb,ialts,hbg,epg,ep_OH,ep_OI,ep_Na,&
      nom_prof,1,path,nom_nua)

    !  write(*,*) ' CALCUL DES VALEURS MOYENNES DANS CHAQUE COUCHE'
 
!     Definition du trajet dans la couche
!     epaisseur de la couche  dz

     ! calcul et stockage de l'humidit� relative dans hum_rel
      t0= 273.15
      
 !densite vapeur saturante (formule modtran)[gr-1]  
      do i=1,nalt
         ta = t0/tatm(i)
         !densite de l'air en cm-3 par d�finition de Boltz ici
         rhoair= patm(i)/(boltz*tatm(i))
         !densite de vapeur d'eau en cm-3
         rhoh2o = rapmel(1,i)*1.e6*rhoair
         !densite vapeur saturante en g/m3
         dsat = exp(18.9766-14.9595*ta-2.43882*ta*ta)*ta
         ! conversion densite de vapeur d'eau en cm-3 en g/m3
         rhoh2o = rhoh2o*mah2o/avog/1e6
         hum_rel(i) = rhoh2o/dsat*100.
      enddo
      
   !  Ecriture des donn�es du profil thermodynamique local
      write(16,*) 
      write(16,*) ' Profil thermodynamique local '
      write(16,*) ' **************************** '
      write(16,'(8(a7,2x))') 'n�','alt','P','T','densit�','H2O','CO2','HR'
      write(16,'(8x,8(a7,2x))') 'km','hPa','K','cm-3','ppmv','ppmv','%'
      do i=1,nalt
         write(16,'(i3,3(f8.3,2x),1e8.3,3(2x,f8.3))') &
         i,alt(i),patm(i),tatm(i),dstair(i),rapmel(1,i),rapmel(2,i),&
         hum_rel(i)
         if (alt(i).eq.120) i120 = i   ! indice de la couche � 120 km pour calcul diffusion multiple
      enddo
   
      ncouche = nalt - 1


!     pmoy(i) = pression moyenne dans la couche i
!     tmoy(i) = temperature moyenne dans la couche i
!     dstmoy(i) = densite moleculaire moyenne dans la couche i



!     imin = indice de l'altitude du bord inferieur de la premiere couche
!     imax = indice de l'altitude du bord superieur de la derniere couche
!     j    = indice de la couche 
      j = 0
      do i=1,nalt-1
         j = j + 1
         z1 = alt(i)
         z2 = alt(i+1)
         pa = patm(i)
         pb = patm(i+1)
         ta = tatm(i)
         tb = tatm(i+1)
         roa = pa/(boltz*ta)
         rob = pb/(boltz*tb)
         dsta = dstair(i)
         dstb = dstair(i+1)
         dsta = roa
         dstb = rob
         dz = z2 - z1
         hp = -dz/alog(pb/pa)
         hro = -dz/alog(rob/roa)
         hd = -dz/alog(dstb/dsta)
         pmoy(j) = 1./dz*hp*(pa-pb)
         romoy = 1./dz*hro*(roa-rob)
         tmoy(j) = pmoy(j)/(boltz*romoy)
         dstmoy = 1./dz*hd*(dsta-dstb)
         pmoy(j) = pmoy(j)/p0
         
       ! chargement du tableau atmp
         atmp(1,i) = ta
         atmp(2,i) = tb
         
!        calcul des densites  
         roa = pa/(boltz*ta)
         rob = pb/(boltz*tb)
         do k=1,nspc
!           rapport de melange => 1E-6
            rapa = rapmel(k,i)*1E-6
            rapb = rapmel(k,i+1)*1E-6
            if (abs(rapa-rapb).lt.1e-5) then
!              conversion ro(cm-3) -> ro(amagat) : ro(amagat) = ro(cm-3)/losch
               dstmol(j,k) = 0.5*(rapa+rapb)*dstmoy/losch
            else
               hden = -dz/alog(rapb/rapa)
               dstmol(j,k) = 1./dz*hden*(rapa-rapb)*dstmoy/losch
            endif
         enddo

!        calcul des densites des CFC
         do k=1,ncfc    
            kp = nmol + k
!           rapport de melange => 1E-6
            rapa = rapmel(kp,i)*1E-6
            rapb = rapmel(kp,i+1)*1E-6
            if (abs(rapa-rapb).lt.1e-5) then
!              conversion ro(cm-3) -> ro(amagat) : ro(amagat) = ro(cm-3)/losch
               dstcfc(j,k) = 0.5*(rapa+rapb)*dstmoy/losch
            else
               hden = -dz/alog(rapb/rapa)
               dstcfc(j,k) = 1./dz*hden*(rapa-rapb)*dstmoy/losch
            endif
         enddo

!        Calcul des densites des continua    
!        Continuum de N2
!        densite integree en km*amagat**2
         dstcont(j,1) = 0.781*((pmoy(j)*296.15/tmoy(j))**2)
!        Continuum de H2O
         rhoair = pmoy(j)*t0/tmoy(j)
         rhos = dstmol(j,1)
         rhof   = rhoair - rhos
         dstcont(j,2) = losch*rhos*rhos*296./273.15
         dstcont(j,3) = losch*rhos*rhof*296./273.15
         dstcont(j,4) = dstcont(j,2)*(296.0-tmoy(j))/(296.0-260.0)
!        Continuum de O3 
         dstcont(j,5) = dstmol(j,3)
         dstcont(j,6) = dstmol(j,3)*0.269*(tmoy(j)-T0)   
         dstcont(j,7) = dstmol(j,3)*0.269*(tmoy(j)-T0)**2   
!        Continuum de O2 
         dt = tmoy(j) - 220.
         dstcont(j,8) = dstmol(j,7)*pmoy(j)*tmoy(j)
         dstcont(j,9) = dstmol(j,7)*pmoy(j)*dt*dt
         dstcont(j,10) = dstmol(j,7)*pmoy(j)
         dstcont(j,11) = dstmol(j,7)*(1. + 0.83*pmoy(j)*t0/tmoy(j))
!        Diffusion moleculaire
         dstcont(j,12) = pmoy(j)*t0/tmoy(j)
!        Continuum de SO2
         dstcont(j,13) = dstmol(j,9)
!        Continuum de NO2
         dstcont(j,14) = dstmol(j,10)

!        Approximation plan parall�le : ds = dz          
!        ***********
!        dz : km -> cm   => 1E5 
         ds = dz*1e5
         do k=1,nspc
            dstmol(j,k) = dstmol(j,k)*ds
         enddo
         do k=1,nspecx
            dstcfc(j,k) = dstcfc(j,k)*ds
         enddo
         do k=1,ncont
!           Continuum de N2 : pas de conversion km -> cm
!           Diffusion moleculaire : pas de conversion km -> cm
            if ((k.eq.1).or.(k.eq.12)) then
               ds = dz
            else
               ds = dz*1.e5
            endif
            dstcont(j,k) = dstcont(j,k)*ds            
         enddo
      enddo
      end
!------------------------------------------------------------------------
      subroutine lecdo(laythr,nmolmx,nalt,alt,patm,tatm,densite,rapmel,&
      nmol,ncfc,inua,itype,altb,alts,ialtb,ialts,hbg,epg,ep_OH,ep_OI,ep_Na,&
      nom_prof,idep,path,nom_nua)

!     Modifications f�vrier 2017 : prise en compte OI, Na

      real alt(laythr+1),patm(laythr+1),tatm(laythr+1)
      real densite(laythr+1),rapmel(nmolmx,laythr+1)
      real, DIMENSION(:),ALLOCATABLE :: dump_alt,dump_p,dump_t,dump_d
      real, DIMENSION(:,:),ALLOCATABLE :: dump_r
      real, DIMENSION(:),ALLOCATABLE :: prof_OH,prof_OI,prof_Na,dump_ep,altn
      real epa(2),hbg,epg
      REAL(KIND=8), DIMENSION(laythr) :: ep_OH,ep_OI,ep_Na

      integer, DIMENSION(:),ALLOCATABLE :: indice
      character * 50 nom_prof,nomfic,nom_nua,entete
      character * 30 path
      integer ok

      ALLOCATE(dump_alt(laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation alt dans Lecdo'
        call exit(1)
      ENDIF       
      ALLOCATE(dump_p(laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation patm dans Lecdo'
        call exit(1)
      ENDIF  
      ALLOCATE(dump_t(laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation atm dans Lecdo'
        call exit(1)
      ENDIF  
      ALLOCATE(dump_d(laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation densite dans Lecdo'
        call exit(1)
      ENDIF  
      ALLOCATE(dump_ep(laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation dump_ep dans Lecdo'
        call exit(1)
      ENDIF  
      ALLOCATE(dump_r(nmolmx,laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation rapmel dans Lecdo'
        call exit(1)
      ENDIF
      ALLOCATE(indice(laythr+1),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation indice dans Lecdo'
        call exit(1)
      ENDIF   
      ALLOCATE(altn(50),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation altn dans Lecdo'
        call exit(1)
      ENDIF  

!    lecture des parametres atmospheriques
!     alt = altitude en km
!     patm = pression en mb
!     tatm = temperature en K
!     densite = densite de l'air en cm-3
!     rapmel = rapport de melange pour les molecules indicees K tel que
!          K = 1 -> H2O / 2 -> CO2 /3 -> O3 /4 -> N2O /5 -> CO /6 -> CH4 /
!              7 -> O2 /8 -> NO /9 -> SO2 /10 -> NO2 /11 -> NH3 /12 -> HNO3 /
!             13-> OH /14 -> HF /15 -> HCL /16 -> HBR /17 -> HI /18 -> CLO /
!             19-> OCS /20 -> H2CO /21 -> HOCL /22 -> N2 /23 -> HCN /24 -> CH3CL /
!             25-> H2O2 /26 -> C2H2 /27 -> C2H6 /28 -> PH3 /          

      data epa /1.0,0.2/                


    !  write(*,*) ' LECTURE DES DONNEES ATMOSPHERIQUES'
      nomfic= TRIM(ADJUSTL(nom_prof))
      open(5,file='DATA/'//nomfic)
      read(5,*)
      read(5,*) nalt,nmol,ncfc
      if (nalt.gt.laythr+1) then
         write(*,*) ' Nombre de couches incompatible'
         stop
      endif
      read(5,*)
      read(5,*) (dump_alt(i),i=1,nalt)
      read(5,*)
      read(5,*) (dump_p(i),i=1,nalt)
      read(5,*)
      read(5,*) (dump_t(i),i=1,nalt)
      read(5,*)
      read(5,*) (dump_d(i),i=1,nalt)
      do k=1,nmol+ncfc
         read(5,*)
         read(5,*) (dump_r(k,i),i=1,nalt)
      enddo
      close(5)
      
      
      !  On lit le profil cr�� par OH
      open(2,file='DATA/profil_nightglow_OH.txt')
      read(2,*) nc_OH
      ALLOCATE(prof_OH(nc_OH),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation prof_OH dans Lecdo'
        call exit(1)
      ENDIF    
      do i=1,nc_OH
         read(2,*) prof_OH(i)
      enddo
      close (2)
      ! On r�adapte le profil thermodynamique aux couches impos�es par OH
      ! Calcul de l'indice des couches OH
      igb = 1
      igd = 1
      hbg = prof_OH(1)
      hsg = prof_OH(nc_OH)
      do while (dump_alt(igd).lt.hbg)
         alt(igb) = dump_alt(igd)
         igb = igb+1
         igd = igd+1
      enddo
      if (dump_alt(igb).eq.hbg) then
         igd = igd +1
      endif
      alt(igb) = hbg
      igbase = igb  ! igbase = indice de la base de la couche OH
      igb = igb +1
      do k=2,nc_OH    
        do while (dump_alt(igd).lt.prof_OH(k)) 
              alt(igb)= dump_alt(igd)       
              igb = igb +1
              igd = igd +1
        enddo
        alt(igb)= prof_OH(k)       
        igb = igb +1
        if (dump_alt(igd).eq.prof_OH(k)) igd = igd + 1
      enddo   
      igsom = igb - 1   ! igsom = indice du sommet de la couche OH           

      do while (dump_alt(igd).lt.dump_alt(nalt))
         alt(igb) = dump_alt(igd)
   !      indice(igb) = igd
         igb = igb+1
         igd = igd+1
      enddo 
      alt(igb) = dump_alt(igd)
    !  indice(igb) = igd
      nalt = igb
   ! calcul de l'�paisseur de chaque couche �mettrice
      i=1
      do while(i.lt.igbase)
         ep_OH(i) = 0.
         i=i+1
      enddo
      do while(i.lt.igsom)
         ep_OH(i) = alt(i+1) - alt(i)
         i=i+1
      enddo
      do while(i.lt.nalt)
         ep_OH(i) = 0.
         i=i+1
      enddo
   ! calcul des donn�es thermodynamiques aux inter-couches  
      do i=1,igbase-1
         patm(i) = dump_p(i)
         tatm(i) = dump_t(i)
         densite(i) = dump_d(i)
         do k=1,nmol+ncfc
            rapmel(k,i) = dump_r(k,i)
         enddo
      enddo
      ! � ce niveau alt(i-1) = dump-alt(i-1)
      ! et alt(i) = altitude de la base entre dump(i-1) et dump(i)
      ! i = indice de la base
      j = i
      do while(i.le.igsom) 
         if (alt(i).le.dump_alt(j)) then
            dz = dump_alt(j) - dump_alt(j-1)
            pa = dump_p(j-1)
            pb = dump_p(j)
            hp = -dz/alog(pb/pa)
            patm(i) = pa*exp(-(alt(i)-dump_alt(j-1))/hp)
            ta = dump_t(j-1)
            tb = dump_t(j)
            dt = tb - ta
            tatm(i) = ta + dt*(alt(i)-dump_alt(j-1))/dz
            da = dump_d(j-1)
            db = dump_d(j)
            hd = -dz/alog(db/da)
            densite(i) = da*exp(-(alt(i)-dump_alt(j-1))/hd)
            do k=1,nmol+ncfc
               ra = dump_r(k,j-1)
               rb = dump_r(k,j)
               dr = rb - ra
               rapmel(k,i) = ra + dr*(alt(i)-dump_alt(j-1))/dz
            enddo   
            i = i+1
         else
            j = j+1
         endif
      enddo
      if (alt(igsom).eq.dump_alt(j)) j=j+1
      do while(i.le.nalt)
         patm(i) = dump_p(j)
         tatm(i) = dump_t(j)
         densite(i) = dump_d(j)
         do k=1,nmol+ncfc
            rapmel(k,i) = dump_r(k,j)
         enddo            
         i=i+1
         j=j+1
      enddo              
 
     
     !*********************************************
     !  On lit le profil cr�� par OI
      do i = 1,nalt
        dump_alt(i) = alt(i)
        dump_p(i) = patm(i)
        dump_t(i) = tatm(i)
        dump_d(i) = densite(i)
        do k=1,nmol+ncfc
           dump_r(k,i) = rapmel(k,i)
        enddo
      enddo 
      open(2,file='DATA/profil_nightglow_OI.txt')
      read(2,*) nc_OI
      ALLOCATE(prof_OI(nc_OI),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation prof_OI dans Lecdo'
        call exit(1)
      ENDIF    
      do i=1,nc_OI
         read(2,*) prof_OI(i)
      enddo
      close (2)
      ! On r�adapte le profil thermodynamique aux couches impos�es par OI
      ! Calcul de l'indice des couches OI
      igb = 1
      igd = 1
      hbg = prof_OI(1)
      hsg = prof_OI(nc_OI)
      do while (dump_alt(igd).lt.hbg)
         alt(igb) = dump_alt(igd)
         igb = igb+1
         igd = igd+1
      enddo
      if (dump_alt(igb).eq.hbg) then
         igd = igd +1
      endif
      alt(igb) = hbg
      igbase = igb  ! igbase = indice de la base de la couche OI
      igb = igb +1
      do k=2,nc_OI    
        do while (dump_alt(igd).lt.prof_OI(k)) 
              alt(igb)= dump_alt(igd)       
              igb = igb +1
              igd = igd +1
        enddo
        alt(igb)= prof_OI(k)       
        igb = igb +1
        if (dump_alt(igd).eq.prof_OI(k)) igd = igd + 1
      enddo   
      igsom = igb - 1   ! igsom = indice du sommet de la couche OI           

      do while (dump_alt(igd).lt.dump_alt(nalt))
        alt(igb) = dump_alt(igd)
   !      indice(igb) = igd
         igb = igb+1
         igd = igd+1
      enddo 
      alt(igb) = dump_alt(igd)
    !  indice(igb) = igd
      nalt = igb
   ! calcul de l'�paisseur de chaque couche �mettrice
      i=1
      do while(i.lt.igbase)
         ep_OI(i) = 0.
         i=i+1
      enddo
      do while(i.lt.igsom)
         ep_OI(i) = alt(i+1) - alt(i)
         i=i+1
      enddo
      do while(i.lt.nalt)
         ep_OI(i) = 0.
         i=i+1
      enddo
   ! calcul des donn�es thermodynamiques aux inter-couches  
      do i=1,igbase-1
         patm(i) = dump_p(i)
         tatm(i) = dump_t(i)
         densite(i) = dump_d(i)
         do k=1,nmol+ncfc
            rapmel(k,i) = dump_r(k,i)
         enddo
      enddo
      ! � ce niveau alt(i-1) = dump-alt(i-1)
      ! et alt(i) = altitude de la base entre dump(i-1) et dump(i)
      ! i = indice de la base
      j = i
      do while(i.le.igsom) 
         if (alt(i).le.dump_alt(j)) then
            dz = dump_alt(j) - dump_alt(j-1)
            pa = dump_p(j-1)
            pb = dump_p(j)
            hp = -dz/alog(pb/pa)
            patm(i) = pa*exp(-(alt(i)-dump_alt(j-1))/hp)
            ta = dump_t(j-1)
            tb = dump_t(j)
            dt = tb - ta
            tatm(i) = ta + dt*(alt(i)-dump_alt(j-1))/dz
            da = dump_d(j-1)
            db = dump_d(j)
            hd = -dz/alog(db/da)
            densite(i) = da*exp(-(alt(i)-dump_alt(j-1))/hd)
            do k=1,nmol+ncfc
               ra = dump_r(k,j-1)
               rb = dump_r(k,j)
               dr = rb - ra
               rapmel(k,i) = ra + dr*(alt(i)-dump_alt(j-1))/dz
            enddo   
            i = i+1
         else
            j = j+1
         endif
      enddo
      if (alt(igsom).eq.dump_alt(j)) j=j+1
      do while(i.le.nalt)
         patm(i) = dump_p(j)
         tatm(i) = dump_t(j)
         densite(i) = dump_d(j)
         do k=1,nmol+ncfc
            rapmel(k,i) = dump_r(k,j)
         enddo            
         i=i+1
         j=j+1
      enddo              
      
     ! *********************************************
     !  On lit le profil cr�� par Na
      do i = 1,nalt
        dump_alt(i) = alt(i)
        dump_p(i) = patm(i)
        dump_t(i) = tatm(i)
        dump_d(i) = densite(i)
        do k=1,nmol+ncfc
           dump_r(k,i) = rapmel(k,i)
        enddo
      enddo 
      open(2,file='DATA/profil_nightglow_Na.txt')
      read(2,*) nc_Na
      ALLOCATE(prof_Na(nc_Na),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation prof_Na dans Lecdo'
        call exit(1)
      ENDIF    
      do i=1,nc_Na
         read(2,*) prof_Na(i)
      enddo
      close (2)
      ! On r�adapte le profil thermodynamique aux couches impos�es par Na
      ! Calcul de l'indice des couches Na
      igb = 1
      igd = 1
      hbg = prof_Na(1)
      hsg = prof_Na(nc_Na)
      do while (dump_alt(igd).lt.hbg)
         alt(igb) = dump_alt(igd)
         igb = igb+1
         igd = igd+1
      enddo
      if (dump_alt(igb).eq.hbg) then
         igd = igd +1
      endif
      alt(igb) = hbg
      igbase = igb  ! igbase = indice de la base de la couche Na
      igb = igb +1
      do k=2,nc_Na    
        do while (dump_alt(igd).lt.prof_Na(k)) 
              alt(igb)= dump_alt(igd)       
              igb = igb +1
              igd = igd +1
        enddo
        alt(igb)= prof_Na(k)       
        igb = igb +1
        if (dump_alt(igd).eq.prof_Na(k)) igd = igd + 1
      enddo   
      igsom = igb - 1   ! igsom = indice du sommet de la couche Na           

      do while (dump_alt(igd).lt.dump_alt(nalt))
        alt(igb) = dump_alt(igd)
   !      indice(igb) = igd
         igb = igb+1
         igd = igd+1
      enddo 
      alt(igb) = dump_alt(igd)
    !  indice(igb) = igd
      nalt = igb
   ! calcul de l'�paisseur de chaque couche �mettrice
      i=1
      do while(i.lt.igbase)
         ep_Na(i) = 0.
         i=i+1
      enddo
      do while(i.lt.igsom)
         ep_Na(i) = alt(i+1) - alt(i)
         i=i+1
      enddo
      do while(i.lt.nalt)
         ep_Na(i) = 0.
         i=i+1
      enddo
   ! calcul des donn�es thermodynamiques aux inter-couches  
      do i=1,igbase-1
         patm(i) = dump_p(i)
         tatm(i) = dump_t(i)
         densite(i) = dump_d(i)
         do k=1,nmol+ncfc
            rapmel(k,i) = dump_r(k,i)
         enddo
      enddo
      ! � ce niveau alt(i-1) = dump-alt(i-1)
      ! et alt(i) = altitude de la base entre dump(i-1) et dump(i)
      ! i = indice de la base
      j = i
      do while(i.le.igsom) 
         if (alt(i).le.dump_alt(j)) then
            dz = dump_alt(j) - dump_alt(j-1)
            pa = dump_p(j-1)
            pb = dump_p(j)
            hp = -dz/alog(pb/pa)
            patm(i) = pa*exp(-(alt(i)-dump_alt(j-1))/hp)
            ta = dump_t(j-1)
            tb = dump_t(j)
            dt = tb - ta
            tatm(i) = ta + dt*(alt(i)-dump_alt(j-1))/dz
            da = dump_d(j-1)
            db = dump_d(j)
            hd = -dz/alog(db/da)
            densite(i) = da*exp(-(alt(i)-dump_alt(j-1))/hd)
            do k=1,nmol+ncfc
               ra = dump_r(k,j-1)
               rb = dump_r(k,j)
               dr = rb - ra
               rapmel(k,i) = ra + dr*(alt(i)-dump_alt(j-1))/dz
            enddo   
            i = i+1
         else
            j = j+1
         endif
      enddo
      if (alt(igsom).eq.dump_alt(j)) j=j+1
      do while(i.le.nalt)
         patm(i) = dump_p(j)
         tatm(i) = dump_t(j)
         densite(i) = dump_d(j)
         do k=1,nmol+ncfc
            rapmel(k,i) = dump_r(k,j)
         enddo            
         i=i+1
         j=j+1
      enddo      
     
     
      !   On ajoute les couches nuageuses
      
      !  Pour comparaison avec Modtran on modifie les valeurs
!      if (itype.eq.1) then
!         epa = 1.0
!         altb = 10.
!         alts = 11.
!      else
!         epa = 0.2
!         altb = 10
!         alts = 10.2
!      endif
      if (inua.gt.0) then
         do i = 1,nalt
            dump_alt(i) = alt(i)
            dump_p(i) = patm(i)
            dump_t(i) = tatm(i)
            dump_d(i) = densite(i)
            do k=1,nmol+ncfc
               dump_r(k,i) = rapmel(k,i)
            enddo
         enddo         
         nomfic= TRIM(ADJUSTL(nom_nua))
         if (inua.eq.1) then
            open(2,file='DATA/'//nomfic)
         else
            open(2,file='UTIL/'//nomfic)
         endif
         ! lecture de l'ent�te
         read(2,*) entete
         ! lecture du nb de couche composant le nuage
         read(2,*) ncn
         read(2,*) (altn(i),i=1,ncn)
   
         !if ((nalt+ncs).gt.laythr+1) then
         !   write(*,*) ' Nombre de couches surdiscr�tis�es incompatible'
         !   stop
         !endif        
         i = 1
         altb = altn(1)
         do while (altb.ge.dump_alt(i))
            alt(i) = dump_alt(i)
            patm(i) = dump_p(i)
            tatm(i) = dump_t(i)
            densite(i) = dump_d(i)
            do k=1,nmol+ncfc
               rapmel(k,i) = dump_r(k,i)
            enddo
            i = i+1
         enddo
         ib = i-1
         ibase = ib
         ialtb = ib+1
         
         j = 1
         ii = ib + 1
         do while (j.le.ncn)
            alt(ii) = altn(j)
           ! if (alt(ii).gt.dump_alt(ib+1)) ib = ib + 1!then
           do while (alt(ii).gt.dump_alt(ib+1))
             ib = ib+1
           enddo
               pa = dump_p(ib)
               pb = dump_p(ib+1)
               dz = dump_alt(ib+1) - dump_alt(ib)
               hp = -dz/alog(pb/pa)
               patm(ii) = pa*exp(-(alt(ii)-dump_alt(ib))/hp)
               ta = dump_t(ib)
               tb = dump_t(ib+1)
               dt = tb - ta
               tatm(ii) = ta + dt*(alt(ii)-dump_alt(ib))/dz
               da = dump_d(ib)
               db = dump_d(ib+1)
               hd = -dz/alog(db/da)
               densite(ii) = da*exp(-(alt(ii)-dump_alt(ib))/hd)
               do k=1,nmol+ncfc
                  ra = dump_r(k,ib)
                  rb = dump_r(k,ib+1)
                  dr = rb - ra
                  rapmel(k,ii) = ra + dr*(alt(ii)-dump_alt(ib))/dz
               enddo    
           ! endif      
            ii = ii+1
            j = j+1
         enddo 
         if (alt(ii-1).ge.dump_alt(ib+1)) ii=ii-1 
         ialts = ii - 1
         
            do i = ib+1,nalt
               alt(ii) = dump_alt(i)
               patm(ii) = dump_p(i)
               tatm(ii) = dump_t(i)
               densite(ii) = dump_d(i)
               do k=1,nmol+ncfc
                  rapmel(k,ii) = dump_r(k,i)
               enddo
               ii = ii + 1
            enddo
            nalt = ii - 1
            ncs = ib - ibase ! nombre de couches suppl�mentaires
           ! modification g�om�trie pour le rayonnement nightglow

           do i=1,nalt-1-ncs
             dump_ep(i) = ep_OH(i)
           enddo
           do i=ncs+1,nalt-1
              ep_OH(i) = dump_ep(i-ncs)
           enddo

      endif
      
      ! �criture des profils dans un fichier pour test
      open(2,file=trim(path)//'\profil_test.xls')
      write(2,'(a80)') 'indice   altitude  pression  temperature  densite  rapport_mel_H2O  epaisseur_couche'
      do i = 1,nalt
         if (i.lt.nalt) epais = alt(i+1) -alt(i)
         write(2,'(i2,7(5x,1e10.3))') i,alt(i),patm(i),tatm(i),densite(i),rapmel(1,i),epais
      enddo
      close(2)
      
         
      DEALLOCATE(dump_alt)
      DEALLOCATE(dump_p)
      DEALLOCATE(dump_t)
      DEALLOCATE(dump_d)
      DEALLOCATE(dump_ep)
      DEALLOCATE(dump_r)
      DEALLOCATE(prof_OH)
      DEALLOCATE(altn)
      end
      
!---------------------------------------------------------     
      subroutine geometrie_obs(tetaobs,h1,h2,pmoy,tmoy,&
      dstmol,dstcfc,dstcont,alt,nlambda,nang,sigmaext,sigmaabs,&
      phase,sigex,sigabs,phaze,nseg,indice,inua,itype,altb,&
      alts,ialtb,ialts,nuaext,nuaabs,nuaphaze,nuamext,nuamabs,&
      nuaphase,hbg,epg,ep_OH,ep_OI,ep_Na,nom_prof,ds,ray_t,path,nom_nua)

!     SOUS PROGRAMME DE CALCUL DES DONNEES THERMODYNAMIQUES
!     LE LONG DU TRAJET ATMOSPHERIQUE

!     Approximations : - un segment par couche
!                      - g�om�trie sph�rique

      include 'parameter.rayjn'

      real alt(laythr+1),patm(laythr+1),tatm(laythr+1)
      real dstair(laythr+1),rapmel(nmolmx,laythr+1)
      real dstcont(laythr,ncont),hum_rel(laythr+1)
      real pmoy(laythr),tmoy(laythr),ds(laythr)
      real dstmol(laythr,nspc),dstcfc(laythr,mmolx)
      
      real sigmaext(klamb,laythr),sigmaabs(klamb,laythr)
      real sigex(klamb,knln),sigabs(klamb,knln)
      real phase(klamb,knang,laythr),phaze(klamb,knang,knln)
      real nuaext(klamb,knln),nuaabs(klamb,knln)
      real nuaphaze(klamb,knang,knln),nuaphase(klamb,knang,laythr)
      real nuamext(klamb,laythr),nuamabs(klamb,laythr)
      
      real tetaobs,hobs,hfin,hbg,epg
      REAL(KIND=8), DIMENSION(laythr) :: ep_OH,ep_OI,ep_Na
      
      real boltz,losch,h1,h2,hmin,hmax,htan,z1,z2,pa,pb,ta,tb,roa,rob
      real hp,hro,dz,dsta,dstb,hd,dstmoy,rapa,rapb,hden,mah2o
      
      integer indice(laythr)
      
      character * 1 type
      character * 50 nom_prof,nom_nua
      character path*30

!     losch = nb de Loschmidt en molecules/cm3 = ro(P0,T0) = P0/(k*T0)
!
!     calcul des valeurs moyennes en utilisant P = ro*boltz*T*cst
!               avec : P = pression en mb = 1hPa = 1E-2 Pa = 1E-2 N/m2
!                      ro = densite moleculaire en molecules/cm3
!                      boltz = k = cste de Boltzmann 
!                            = 1.3807E-23 J/deg. (N.m/deg. car 1 J=1 N.m)
!                      T = temperature en K                 
!                      cst = 1E6/1E2 = 1E4 = conversion -> 1 mb = 1E2 N/m2 (1 hPa) 
!                                                       -> 1 m3 = 1E6 cm-3
      data losch /2.6868E19/
      data cst,p0,t0,avog,mah2o /1.E4,1013.25,273.15,6.022136e+23,18.01534/
      ! mah2o = masse molaire (g) de H2O
      boltz = 1.3807E-23*cst  ! conversion pour travailler en mb et en cm-3
      pi = acos(-1.)
      rad = pi/180.
        
!    lecture des parametres atmospheriques
!     alt = altitude en km
!     patm = pression en mb
!     tatm = temperature en K
!     dstair = densite de l'air en cm-3
!     rapmel = rapport de melange en ppmv

      call lecdo(laythr,nmolmx,nalt,alt,patm,tatm,dstair,rapmel,nmol,ncfc,&
      inua,itype,altb,alts,ialtb,ialts,hbg,epg,ep_OH,ep_OI,ep_Na,nom_prof,0,&
      path,nom_nua)
 
!     Definition du trajet dans la couche
!     epaisseur de la couche  dz

      hobs = h1
      hfin = h2

      ilimb = 0
      if (tetaobs.gt.90.) then
         rap = ray_t/(ray_t + hobs)
         ang_limit = 90. - acos(rap)/rad
         alpha = 180. - tetaobs
         if (alpha.lt.ang_limit) then
            if (hfin.gt.hobs) hfin = 0.
         else
            ! on est dans une vis�e au limbe
            ilimb = 1
            htan = (ray_t + hobs)*sin((180.-tetaobs)*rad) - ray_t
         endif
      else
!         if (hfin.gt.0.) hfin = 120.
      endif

    iilimb = 0
    do while(iilimb.le.ilimb)
       ! hmin = altitude la plus basse dans le trajet
       ! hmax = altitude la plus �l�v�e dans le trajet
       if (ilimb.eq.1) then
          if (iilimb.eq.0) then
             hobs = htan
             hfin = 120.
             ! on calcule le nombre total de segments
             do i=1,nalt-1
                if ((alt(i).le.htan).and.(alt(i+1).gt.htan)) imin = i
                if ((alt(i).lt.h1).and.(alt(i+1).ge.h1)) imax = i     
             enddo
             if (h1.gt.alt(nalt)) then
                imax = nalt-1
             endif
             nseg1 = nalt - imin
             nseg2 = imax - imin + 1
             nseg = nseg1 + nseg2
          else
             hobs = h1
             hfin = htan
          endif
       endif
       hmin = hobs
       hmax = hfin
       if (hobs.gt.hfin) then
          hmin = hfin
          hmax = hobs
       endif
       ! imin = indice de l'altitude du bas de la couche contenant hmin
       ! imax = indice de l'altitude du bas de la couche contenant hmax
       do i=1,nalt-1
         if ((alt(i).le.hmin).and.(alt(i+1).gt.hmin)) imin = i
         if ((alt(i).lt.hmax).and.(alt(i+1).ge.hmax)) imax = i     
       enddo
       if (hmax.gt.alt(nalt)) then
          hmax = alt(nalt)
          imax = nalt-1
       endif
 
       if (ilimb.eq.0) nseg = imax - imin + 1

!      pmoy(i) = pression moyenne dans la couche i
!      tmoy(i) = temperature moyenne dans la couche i
!      dstmoy(i) = densite moleculaire moyenne dans la couche i

     ! Pour le calcul en g�om�trie sph�rique, calcul des c�t�s du triangle initial 
     ! Rayon de la terre = 6378 km
      teta = tetaobs
      if (tetaobs.gt.90.) teta = 180. - tetaobs
      Rp = (ray_t + hmin)*sin(teta*pi/180.)
      R1 = (ray_t + hmin)*cos(teta*pi/180.)
      if (ilimb.eq.1) then
         Rp = (ray_t + hmin)
         R1 = 0.
      endif
         

!     imin = indice de l'altitude du bord inferieur de la premiere couche
!     imax = indice de l'altitude du bord superieur de la derniere couche
!     j    = indice du segment : j=1 au niveau de l'observateur / j=nseg � l'extr�mit� du trajet
      
      if (ilimb.eq.1) then
         if (iilimb.eq.0) then
            j = nseg2
         else
            j = nseg2 + 1
         endif
      else
         j = 0
         if (hobs.gt.hfin) j = nseg + 1
      endif
      do i=imin,imax
         if (hobs.gt.hfin) then
            j = j - 1
         else
            j = j + 1
         endif
         indice(j) = i
         z1 = alt(i)
         z2 = alt(i+1)
         pa = patm(i)
         pb = patm(i+1)
         ta = tatm(i)
         tb = tatm(i+1)
         roa = pa/(boltz*ta)
         rob = pb/(boltz*tb)
         dsta = dstair(i)
         dstb = dstair(i+1)
         dsta = roa
         dstb = rob
         dz = z2 - z1
         if (i.eq.imin) then
            hp = -dz/alog(pb/pa)            
            p2 = pa*exp(-(hmin-z1)/hp)
            dz = z2 - hmin
            pa = p2   ! pa = pression � l'altitude hmin
         endif
         if (i.eq.imax) then
            hp = -dz/alog(pb/pa)            
            p2 = pa*exp(-(hmax-z1)/hp)
            pb = p2   ! pb = pression � l'altitude hmax
            dz = hmax - z1                    
         endif
         hp = -dz/alog(pb/pa)
         hro = -dz/alog(rob/roa)
         hd = -dz/alog(dstb/dsta)
         
! calcul des donn�es thermodynamiques pour le mod�le de bande sur chacun des segments j du trajet
! ***********************************************************************************************
         pmoy(j) = 1./dz*hp*(pa-pb)
         romoy = 1./dz*hro*(roa-rob)
         tmoy(j) = pmoy(j)/(boltz*romoy)
         dstmoy = 1./dz*hd*(dsta-dstb)
         pmoy(j) = pmoy(j)/p0
         
!        calcul des densites  
         roa = pa/(boltz*ta)
         rob = pb/(boltz*tb)
         do k=1,nspc
!           rapport de melange => 1E-6
            rapa = rapmel(k,i)*1E-6
            rapb = rapmel(k,i+1)*1E-6
            if (abs(rapa-rapb).lt.1e-5) then
!              conversion ro(cm-3) -> ro(amagat) : ro(amagat) = ro(cm-3)/losch
               dstmol(j,k) = 0.5*(rapa+rapb)*dstmoy/losch
            else
               hden = -dz/alog(rapb/rapa)
               dstmol(j,k) = 1./dz*hden*(rapa-rapb)*dstmoy/losch
            endif
         enddo

!        calcul des densites des CFC
         do k=1,ncfc    
            kp = nmol + k
!           rapport de melange => 1E-6
            rapa = rapmel(kp,i)*1E-6
            rapb = rapmel(kp,i+1)*1E-6
            if (abs(rapa-rapb).lt.1e-5) then
!              conversion ro(cm-3) -> ro(amagat) : ro(amagat) = ro(cm-3)/losch
               dstcfc(j,k) = 0.5*(rapa+rapb)*dstmoy/losch
            else
               hden = -dz/alog(rapb/rapa)
               dstcfc(j,k) = 1./dz*hden*(rapa-rapb)*dstmoy/losch
            endif
         enddo

!        Calcul des densites des continua    
!        Continuum de N2
!        densite integree en km*amagat**2
         dstcont(j,1) = 0.781*((pmoy(j)*296.15/tmoy(j))**2)
!        Continuum de H2O
         rhoair = pmoy(j)*t0/tmoy(j)
         rhos = dstmol(j,1)
         rhof   = rhoair - rhos
         dstcont(j,2) = losch*rhos*rhos*296./273.15
         dstcont(j,3) = losch*rhos*rhof*296./273.15
         dstcont(j,4) = dstcont(j,2)*(296.0-tmoy(j))/(296.0-260.0)
!        Continuum de O3 
         dstcont(j,5) = dstmol(j,3)
         dstcont(j,6) = dstmol(j,3)*0.269*(tmoy(j)-T0)   
         dstcont(j,7) = dstmol(j,3)*0.269*(tmoy(j)-T0)**2   
!        Continuum de O2 
         dt = tmoy(j) - 220.
         dstcont(j,8) = dstmol(j,7)*pmoy(j)*tmoy(j)
         dstcont(j,9) = dstmol(j,7)*pmoy(j)*dt*dt
         dstcont(j,10) = dstmol(j,7)*pmoy(j)
         dstcont(j,11) = dstmol(j,7)*(1. + 0.83*pmoy(j)*t0/tmoy(j))
!        Diffusion moleculaire
         dstcont(j,12) = pmoy(j)*t0/tmoy(j)
!        Continuum de SO2
         dstcont(j,13) = dstmol(j,9)
!        Continuum de NO2
         dstcont(j,14) = dstmol(j,10)

!        GEOMETRIE : Approximation sph�rique          
!        ***********      
         if (i.eq.imax) z2 = hmax
         ds(j) = sqrt((ray_t + z2)**2 - Rp**2) - R1
         R1 = R1 + ds(j)      
         
         do k=1,nspc
            dstmol(j,k) = dstmol(j,k)*ds(j)*1e5 ! km -> cm   => 1E5 
         enddo
         do k=1,nspecx
            dstcfc(j,k) = dstcfc(j,k)*ds(j)*1e5 ! km -> cm   => 1E5 
         enddo
         do k=1,ncont
!           Continuum de N2 : pas de conversion km -> cm
!           Diffusion moleculaire : pas de conversion km -> cm
            if ((k.eq.1).or.(k.eq.12)) then
               dstcont(j,k) = dstcont(j,k)*ds(j)  
            else
               dstcont(j,k) = dstcont(j,k)*ds(j)*1.e5 ! km -> cm   => 1E5 
            endif           
         enddo
         
   ! Param�tres a�rosols      
         do ilamb=1,nlambda
            ssx = sigex(ilamb,i)
            sab = sigabs(ilamb,i)            
            if (i.eq.imin)then   ! interpolation avec l'altitude
               coef = (hmin-alt(i))/(alt(i+1)-alt(i))
               ssx = sigex(ilamb,i) + (sigex(ilamb,i+1)-sigex(ilamb,i))*coef
               sab = sigabs(ilamb,i) + (sigabs(ilamb,i+1)-sigabs(ilamb,i))*coef
               ssx1 = ssx
               sab1 = sab            
            endif
            if (i.eq.imax)then   ! interpolation avec l'altitude
               coef = (hmax-alt(i))/(alt(i+1)-alt(i))
               ssx = sigex(ilamb,i) + (sigex(ilamb,i+1)-sigex(ilamb,i))*coef
               sab = sigabs(ilamb,i) + (sigabs(ilamb,i+1)-sigabs(ilamb,i))*coef   
               ssx2 = ssx
               sab2 = sab            
            endif          
            if (imin.eq.imax) then
               sigmaext(ilamb,j) = ds(j)*(ssx1 + ssx2)/2.
               sigmaabs(ilamb,j) = ds(j)*(sab1 + sab2)/2.  
            else
               sigmaext(ilamb,j) = ds(j)*(ssx + sigex(ilamb,i+1))/2.
               sigmaabs(ilamb,j) = ds(j)*(sab + sigabs(ilamb,i+1))/2.  
!                    if (ssx.ne.sigex(ilamb,i+1)) then
!                        fact_ech = -ds(j)/alog(sigex(ilamb,i+1)/ssx)
!                        if (fact_ech.gt.0) sigmaext(ilamb,j) = ds(j)*fact_ech*(ssx- sigex(ilamb,i+1))
!                    endif
!                        if (sab.ne.sigabs(ilamb,i+1)) then
!                        fact_ech = -ds(j)/alog(sigabs(ilamb,i+1)/sab)
!                        if (fact_ech.gt.0) sigmaabs(ilamb,j) = ds(j)*fact_ech*(sab- sigabs(ilamb,i+1))
!                    endif
            endif         
            do iang=1,nang
               phase(ilamb,iang,j) = phaze(ilamb,iang,i)
            enddo
         enddo   
   ! Param�tres nuages      
         do ilamb=1,nlambda
            ssx = nuaext(ilamb,i)
            sab = nuaabs(ilamb,i)            
            if (i.eq.imin)then   ! interpolation avec l'altitude
               coef = (hmin-alt(i))/(alt(i+1)-alt(i))
               ssx = nuaext(ilamb,i) + (nuaext(ilamb,i+1)-nuaext(ilamb,i))*coef
               sab = nuaabs(ilamb,i) + (nuaabs(ilamb,i+1)-nuaabs(ilamb,i))*coef   
               ssx1 = ssx
               sab1 = sab               
            endif
            if (i.eq.imax)then   ! interpolation avec l'altitude
               coef = (hmax-alt(i))/(alt(i+1)-alt(i))
               ssx = nuaext(ilamb,i) + (nuaext(ilamb,i+1)-nuaext(ilamb,i))*coef
               sab = nuaabs(ilamb,i) + (nuaabs(ilamb,i+1)-nuaabs(ilamb,i))*coef    
               ssx2 = ssx
               sab2 = sab                
            endif      
            if (imin.eq.imax) then      
               nuamext(ilamb,j) = ds(j)*(ssx1 + ssx2)/2.
               nuamabs(ilamb,j) = ds(j)*(sab1 + sab2)/2. 
            else
               nuamext(ilamb,j) = ds(j)*(ssx + nuaext(ilamb,i+1))/2.
               nuamabs(ilamb,j) = ds(j)*(sab + nuaabs(ilamb,i+1))/2. 
            endif        
            do iang=1,nang
               nuaphase(ilamb,iang,j) = nuaphaze(ilamb,iang,i)
            enddo
         enddo                     
      enddo
      iilimb = iilimb + 1
    enddo
    
      ! �criture des donn�es du trajet dans un fichier pour test
      open(2,file=trim(path)//'\trajet_test.xls')
      write(2,'(a80)') 'indice   altitude  �paisseur_segment  pmoyen  Tmoyen'
      do i = 1,nseg
         j = indice(i)
         write(2,*) i,alt(j),ds(i),pmoy(i),tmoy(i)
      enddo
      close(2)
    
    
    end     

 