      program Ray_J_N
       
c    Programme Ray_j_n (Rayonnement_Jour_Nuit) VERSION 1.0 : 14/04/2010   
c                                                   
c    Fonctionnalit�s : - mod�le de bande statistique
c                      - diffusion multiple calcul�e par mod�le aux ordonn�es discr�tes
c                      - bande spectrale 0.2 - 50 �m / r�solution 1 cm-1
c                      - ajout rayonnement lunaire : mars 2012
c                      - ajout rayonnement stellaire : juillet 2012
c     Modifications f�vrier 2017 : prise en compte OI, Na
C 
c***************************************************************************************
c***************** Version de sauvegarde avant Version 2.0 *****************************
c***************************************************************************************                      

       
      include 'parameter.rayjn'
       
      real sd(ntmx,mmolt2),od(ntmx,mmolt)
      real sdz(ntmx,nipmx),odz(ntmx,nipmx),alf0(mmolt),dopfac(mmolt)
      real colsum(mmolt),dopsum(mmolt),odsum(mmolt),sdsum(mmolt2)
      real pmoy(laythr),dop0(mmolt),odzx(5),sdzx(5),ff(laythr)
      real tband(ntmx),tmoy(laythr),wt(laythr),tcfc(nspecx)
      real tauold(laythr,nspc+2),taunew(laythr,nspc+2),taucont(nspc+2)
      real dstmol(laythr,nspc),dstcfc(laythr,mmolx)
      real ucont(ncont),dstcont(laythr,ncont),tdeb(laythr)
      real solar_spec(nsigmx),alt(laythr+1),hum_rel(laythr+1)
      real atmp(2,knln),OMG(knln,nsigmx),THK(knln,nsigmx)
      real phsf(knang,laythr),phsfn(knang,laythr),rayl(laythr)
      real tran_sol(knln,nsigmx),alum,sigdia(laythr),sigdin(laythr)
      real sigmaext(klamb,laythr),sigmaabs(klamb,laythr)
      real phase(klamb,knang,laythr),alamda(klamb),ang(knang)  
      real nuamext(klamb,laythr),nuamabs(klamb,laythr)
      real nuaphase(klamb,knang,laythr),ds(laythr),albedo(nsigmx)
      real ds0(laythr),alt_ngw(50),prof_ng(50),prf_ng(laythr+1)
      
      real *8 tranc(laythr),trans(nspc),tran_seg(laythr)
      
      real  tetaob,fiobs,heure,hobs,hfin,ang1,ang2,hp,cl,hbg,epg,htan
      real  lat,long
      real  sumwat,sumph,sumrayl
      real  lat_pion(50),lon_pion(50),val_pion(50,50)
      real  freq_pion(2000),spect_pion(2000,50) 
      real  lat_eclip(10),lon_eclip(19),val_zodiac(19,10)
      real  lat_OH,long_OH

      
      real, DIMENSION(:,:),ALLOCATABLE :: sigext,sigabs,nuaext,nuaabs
      real, DIMENSION(:,:,:),ALLOCATABLE :: phaze,nuaphaze       
  
      
      !  Param�tres pour la diffusion calcul�s en double pr�cision
      REAL(KIND=8) :: teta0dm,galbedo,flux0,Glow0,prop_sol
      REAL(KIND=8) :: Tempsol,sigma,dvdm   
      REAL(KIND=8), DIMENSION(knln) :: thkdm,omgdm
      REAL(KIND=8), DIMENSION(laythr) :: ep_OH,ep_gldm_OH,glow_OH,glow
      REAL(KIND=8), DIMENSION(laythr) :: ep_OI,ep_gldm_OI,glow_OI
      REAL(KIND=8), DIMENSION(laythr) :: ep_Na,ep_gldm_Na,glow_Na
      REAL(KIND=8), DIMENSION(kntau) :: UTAU
      REAL(KIND=8), DIMENSION(knang) :: angdm
      REAL(KIND=8), DIMENSION(knang,knln) :: phsfdm
      REAL(KIND=8), DIMENSION(2,knln) :: atmpdm
      REAL(KIND=8), DIMENSION(nsigmx,kntau) :: aj
      REAL(KIND=8), DIMENSION(nsigmx) :: flux,tran_tot
      REAL(KIND=8), DIMENSION(kna1u) :: AM1U
      REAL(KIND=8), DIMENSION(knfi) :: FI
      real(KIND=8), DIMENSION(:,:,:),ALLOCATABLE :: AJJ 
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: lambda,convol,sgma,ver_c
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: lum_sig,lum_lam,flux_lam
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: tran_lam
      real(KIND=8), DIMENSION(:,:,:),ALLOCATABLE :: ver
      Real(KIND=8), DIMENSION(:),ALLOCATABLE :: lam_alb,alb
      real(kind=8) am11,flux_sol,ver_integ,lum_saber,aniv,lux,flux_som
      real(kind=8) lg,bg,longec,latec                
      
      integer ibin(nipmx),imol(nipmx),ialf(nipmx)
      integer iparam(nblkmx),jj(laythr),indice(laythr)
      integer Jday,iv1,iv2,iang_dif
      integer ok,ios
      character * 50 nom_prof,nom_aer,nom_nua,nomfic,ligne
      character * 50 nom_alb,nom_gene
      character * 1 rep,mode
      character * 60 spect_OH
            
      character datej*8, time*10,zone*5
      character path*30,path3*4
      character *2 path1,path2,path4,path5,path6
      integer values(8)
      
      ALLOCATE(ajj(kna1u,knfi,kntau),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation AJJ dans Ray_j_n'
        call exit(1)
      ENDIF       
      ALLOCATE(sigext(klamb,knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation sigex dans Ray_j_n'
        call exit(1)
      ENDIF      
      ALLOCATE(sigabs(klamb,knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation sigex dans Ray_j_n'
        call exit(1)
      ENDIF  
      ALLOCATE(phaze(klamb,knang,knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation phaze dans Ray_j_n'
        call exit(1)
      ENDIF   
      ALLOCATE(nuaext(klamb,knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation nuaext dans Ray_j_n'
        call exit(1)
      ENDIF      
      ALLOCATE(nuaabs(klamb,knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation nuaabs dans Ray_j_n'
        call exit(1)
      ENDIF  
      ALLOCATE(nuaphaze(klamb,knang,knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation nuaphaze dans Ray_j_n'
        call exit(1)
      ENDIF   
                           
       
      pi = acos(-1.)
      RAD=PI/180.0
      
      hp = 6.62606876e-34
      cl = 2.99792458e10  ! attention, c en cm/s
      
      init_alloc = 0
      ialb = 1
      idbg = 1  ! flag pour �criture des param�tres dans un fichier
      
      ! Constitution du r�pertoire contenant les r�sultats
      call DATE_AND_TIME(datej, time, zone, values)
      write(path1,'(i2)') values(3)
      write(path2,'(i2)') values(2)
      write(path3,'(i4)') values(1)
      write(path4,'(i2)') values(5)
      write(path5,'(i2)') values(6)
      write(path6,'(i2)') values(7)
      path = trim(ADJUSTL(path1))//'_'//trim(ADJUSTL(path2))//
     &'_'//path3//'_'//trim(ADJUSTL(path4))//'h'//
     &trim(ADJUSTL(path5))//'mn'//trim(ADJUSTL(path6))//'s'
      path = 'RESU\'//trim(adjustl(path))
      R = system('mkdir '//trim(path))
      R = system('copy Rayjn.data '//trim(path))
      

c ==> Ouverture des fichiers
*****************************
c     Ouverture des fichiers

c     4 : ERR.DAT
c     3 : trans_molecule.xls
c     7 : trans_totale.xls
c     8 : lum_Rayjn.xls
c     9 : DIRAC
c    10 : UFTAPX.asc
c    11 : flux_Rayjn.xls
c    12 : flux_sommet.xls

c    15 : comp_Rayjn.xls
c    16 : Resume.out
c    17 : Convol_Rayjn.xls
c    18 : Flux_conv_Rayjn.xls
c    19 : VER_Saber.xls
c    20 : Trans_conv_Rayjn.xls


      open (4 ,file='RESU/ERR.DAT')
c     Ouverture du fichier transmission mol�culaire
      open(3,file='RESU/trans_molecule.xls')
c     Ouverture du fichier transmission totale
      open(7,file='RESU/trans_totale.xls')      
c     Ouverture du fichier luminance totale
      open(8,file=trim(path)//'\lum_Rayjn.xls')    
      write(8,'(6(a15,10x))') 'Nb d''ondes','Luminance','Luminance',
     & 'Longueur d''onde','Luminance','Luminance'
      write(8,'(a10,16x,a15,5x,a17,5x,a15,14x,a15,10x,a15)') 
     & '(cm-1)','(W/m2/sr/cm-1)','(ph/s/m2/sr/cm-1)','(microns)',
     & '(ph/s/m2/sr/mic)','(R/mic)'
      open(12,file=trim(path)//'\flux_sommet.xls')
      write(12,'(a10,18x,a10)') '(cm-1)','(W/m2/cm-1)'
      write(12,'(2(a15,12x))') 'Nb d''ondes','Eclairement'

      
c     Ouverture du fichier composantes de la luminance
      open(15,file=trim(path)//'\comp_Rayjn.xls')
      write(15,'(70x,a30)') '(luminances en (W/m2/sr/cm-1))'   
      write(15,'(a5,10x,10(a18,5x))') 'cm-1','transmission',
     &'thermique ','diffusion multiple','diffusion simple','nightglow',
     &'stellaire','fond de sol','totale','source exoatmos',
     &'source transmise'
      
c     Ouverture du fichier r�sum� des conditions
      open(16,file=trim(path)//'\Resume_out.txt')
      write(16,*)
      itz = int(values(4)/60)
      write(16,'(a22,i2,a1,i2,a1,i4,a3,i2,a1,i2,a2,i2,a6,i2)') 
     &' Calculs effectues le ',values(3),'/',values(2),'/',
     &values(1),' a ',values(5),'h',values(6),'mn',values(7),
     &'s UTC+',itz
      write(16,*)
      write(16,*)
      write(16,*) '       *********************************************'
      write(16,*) '       ** FICHIER RESUME DES CONDITIONS DE CALCUL **'
      write(16,*) '       *********************************************'        
      
c     Appel des donnees internes au programme pour le mod�le de bande
      call data_interne(dv,dopfac,itb,itbx)

c====>
c         Lecture des donn�es utilisateur    
      iflag = 0
      iflagt0 = 0
      open(2,file='Rayjn.data')
      read(2,'(a50)') ligne       ! date et heure (TU)  
      read(2,*) lat,long          ! latitude d�cimale et longitude d�cimale (+N,-S +O,-E) (coordonn�es g�ocentriques)
      read(2,*) hobs              ! altitude de l'observateur (km)  
      read(2,*) tetaobs,fiobs     ! angle z�nithal et azimut d'observation (azimut par rapport au nord)
      read(2,*) galb,Tsol         ! alb�do, temp�rature du sol
      read(2,*) nom_prof          ! Nom du profil atmosph�rique : Profil_USstd / Profil_MLS / Profil_MLW / Profil_SAW / Profil_TROP
      read(2,*) iaero,nom_aer     ! nom du type d'a�rosol : aerosols_Rural23_E / aerosols_Urbain5_E /aerosols_Fog1_E
      read(2,*) inua,nom_nua              ! inua=1 : calcul avec nuage dans DATA / inua=2 : calcul avec nuage dans UTIL, ! nom du type de nuage : Cirrus_standard
      read(2,*) vmin,vmax         ! nb d'ondes min et max (cm-1) ou longueur d'onde min et max en �m
      read(2,*) iconvol,fwhm,per ! convolution (0=non / 1=cm-1 / 2=�m), largeur � mi-hauteur (cm-1 ou �m)  ! R�solution = (Delta l)/l et pas d'�criture des r�sultats (cm-1 ou �m)
      read(2,*) iecla             ! Si �gal 1 alors calcul �clairement au sol
      read(2,*) mode              ! (S/A) mode Simplifi� ou Avanc�
      htang = -1.
      if ((mode.eq.'A').or.(mode.eq.'a')) then
         read(2,*) iflag,ilum           ! ilum=1 : calcul luminance, sinon transmission uniquement
         if (iflag.eq.0) ilum = 1
         read(2,*) iflagt0,teta0_util   ! teta0_util = angle z�nithal solaire utilisateur
         read(2,*) iflag,hfin           ! altitude de l'extr�mit� du trajet (km)  ; -1 trajet infini
         if (iflag.eq.0) hfin = -1
         read(2,*) htang          ! altitude tangente 
         read(2,*) iflag,alt_sol        ! altitude du sol (km)
         if (iflag.eq.0) alt_sol = 0.
         read(2,*) ialb,nom_alb        ! ialb = 0 pas de fichier spectral
                                       ! ialb = 1 lecture fichier d'alb�do dans DATA
                                       ! ialb = 2 lecture fichier d'alb�do dans UTIL
         read(2,*) iflag,nda,iazim     ! si iazim = 1 prise en compte variation azimutale luminance diffuse/nda = nb de flux
         if (iflag.eq.0) then
            iazim = 0
            nda = 4
         endif
      else
         ilum = 1
         hfin = -1
         alt_sol = 0.
         ialb = 0
         iazim = 0
         nda = 4
      endif
      close (2)
      
      
      
      !!!!!!!  LECTURE HAUTEUR TANGENTE DANS UN FICHIER SEPARE !!!
  !    write(*,*) '****************************************'
  !    write(*,*) 'LECTURE HAUTEUR TANGENTE DANS UN FICHIER SEPARE !!!'
   !   open(2,file='hauteur_tangente')
  !    read(2,*) htang
  !    close(2)
    
      
      if (hobs.lt.alt_sol) then
         hobs = alt_sol
         write(*,*) ' Altitude observateur (Hobs) < altitude sol (alt_so
     &l)'
         write(*,*) ' On impose hobs = alt_sol'
         write(*,*) ' continue ? (o/n)'
         read(*,*) rep
         if (rep.eq.'n') stop
      endif
    
      
      if (nom_alb(1:1).eq.'0') ialb = 0
      if (hfin.lt.0) hfin = 36000.
      if ((hobs.eq.hfin).and.(tetaobs.lt.90)) then
         write(*,*) 'configuration impossible'
         write(*,*) 'hobs=',hobs,' hfin=',hfin,
     &              ' tetaobs=',tetaobs
         stop
      endif 
      if ((hobs.eq.0.).and.(tetaobs.gt.90)) then
         write(*,*) 'configuration impossible'
         write(*,*) 'hobs=',hobs,' tetaobs=',tetaobs
         stop
      endif 
      if (vmin.lt.100.) then
         ivmin = 1e4/vmax
         ivmax = 1e4/vmin
      else
         ivmin = vmin
         ivmax = vmax
      endif
      if (ivmin.gt.ivmax) then
         write(*,*)
         write(*,*) 'Incoherence nombres d''ondes'
         stop
      endif
      if (ivmax.eq.ivmin) then
         ivmax = ivmin + 1
        ! if (vmin.lt.100.) vmax = float(1e4/ivmax)
         write(*,*)
         write(*,*) 'nombre d''onde min et max �gaux'
         write(*,*) 'Nouvelles valeurs :', ivmin, ivmax
      endif
      vmin = 1e4/ivmax
      vmax = 1e4/ivmin
      
      

      ! extraction de la date
      call extract(ligne,heure,imois,iannee,ijour,
     &                   Jday,teta0,date)
c      call param_soleil(heure,imois,iannee,ijour,
c     &Jday,lat,long,teta0,hobs)      
      call ephemerides(heure,imois,iannee,ijour,Jday,
     & lat,long,teta0,hobs,tetal,ang_phas,Rst,Rtl,azsol,azlun) 
                             
      if (iflagt0.gt.0) teta0 = teta0_util
       
      ! calcul longitudes et latitudes galactiques, lg et bg
      call coord_galac(lat,long,heure,imois,iannee,ijour,
     &                 0.,lg,bg)
     
      ! calcul longitudes et latitudes ecliptiques, longec et latec
      call coord_eclip(lg,bg,longec,latec)
      
      
      ! lecture des donn�es de fond galactique de Pionner
      open(2,file='DATA/Pionner_blue_tot.txt')
      read(2,*) (lat_pion(j),j=1,25)
      do i=1,36
          read(2,*) lon_pion(i),(val_pion(i,j),j=1,25)
      enddo
      close (2)
      ! lecture de la variabilit� spectrale du fond galactique 
      open(2,file='DATA/galac_spectral.txt')
      read(2,*)
      do i=1,1526
         read(2,*) freq_pion(i),(spect_pion(i,l),l=1,13)
         freq_pion(i) = freq_pion(i)/10000.  ! conversion en �m
      enddo
      close (2)
      ! lecture des donn�es de lumi�re zodiacale
      open(2,file='DATA/zodiacal_light.txt')
      read(2,*) (lat_eclip(j),j=1,10)
      do i=1,19
          read(2,*) lon_eclip(i),(val_zodiac(i,j),j=1,10)
      enddo
      close (2)
      
       write(*,*)
       write(*,'(a35)') 'Coordonnees galactiques du zenith '
       write(*,'(a35)') '**********************************'
       write(16,*)
       write(16,'(a35)') 'Coordonnees galactiques du zenith '
       write(16,'(a35)') '**********************************'
       minute = (lg - int(lg))*60
       imn = abs(minute)
       write(*,'(1x,a20,i3,a7,i2,a2)') 'Longitude : ',int(lg),
     &                                 'degres',imn,'mn'
       write(16,'(1x,a20,i3,a7,i2,a2)') 'Longitude : ',int(lg),
     &                                 'degres',imn,'mn'
       minute = (bg - int(bg))*60
       imn = abs(minute)
       write(*,'(1x,a20,i3,a7,i2,a2)') 'Latitude : ',int(bg),
     &                                 'degres',imn,'mn'
       write(16,'(1x,a20,i3,a7,i2,a2)') 'Latitude : ',int(bg),
     &                                 'degres',imn,'mn'
       write(*,*)
       write(*,'(a47)') '**********************************************'
       write(*,*) 
       write(*,*)
      
     
c     ! Calcul du rayon terrestre � la latitude
      ray_eq = 6378.137
      ray_po = 6356.752
      exent = sqrt(1. - (ray_po/ray_eq)**2)
      ray_t = ray_po/sqrt(1. - (exent*cos(lat*rad))**2)
      
      ! Test sur le type de vis�e
      ilimb = 0
      isol = 0
      if (tetaobs.gt.90.) then
         rap = ray_t/(ray_t + hobs)
         ang_limit = 90. - acos(rap)/rad
         alpha = 180. - tetaobs
         if (alpha.lt.ang_limit) then
            if (hfin.gt.hobs) then
               isol = 1
               hfin = 0.
            endif
         else
            ! on est dans une vis�e au limbe
            ilimb = 1
            htan = (ray_t + hobs)*sin((180.-tetaobs)*rad) - ray_t
         endif
      endif
      if (htang.gt.0.) then
         ilimb = 1
         htan = htang
         tetaobs = 180. - asin((htang+ray_t)/(hobs+ray_t))/rad
      endif
      
      
      iheure = int(heure)
      minute = (heure - iheure)*60
      seconde = (minute-int(minute))*60
      imn = int(minute)
      isec = int(seconde)
      pos1=' N'
      pos2=' W'
      if (lat.lt.0) pos1=' S'
      if (long.le.0) pos2=' E' 
      write(*,*) 
      write(*,'(10x,a40)') ' Resume des conditions de calcul '
      write(*,'(10x,a40)') '*********************************'
      if ((mode.eq.'A').or.(mode.eq.'a')) then
         write(*,*)
         write(*,'(10x,a20)') ' Mode Avance '
         write(*,'(10x,a20)') '*************'
         write(*,*)
      endif
      write(*,'(1x,a10,i2,a1,i2,a1,i4)') ' Date : ',ijour,
     &      '/',imois,'/',iannee
      write(*,'(1x,a10,i2,a1,i2,a2,i2,a5)') ' Heure :',iheure,'h',
     &      imn,'mn',isec,'s  UT'
      write(*,'(1x,a22,f7.3,a2,a3,f7.3,a2)') ' Position Observateur ',
     &      abs(lat),pos1,' / ',abs(long),pos2
      write(*,'(1x,a30,f7.3,a3)')' Altitude Observateur : ',hobs,' km'
      if (ilimb.eq.1) write(*,'(1x,a36,f7.3,a2)') 
     &    ' Visee au limbe, altitude tangente :',htan,'km'
      write(*,'(1x,a30,f7.3,a4)')' Angle zenithal observation : ',
     &         tetaobs,' deg'
      write(*,'(1x,a30,f7.3,a4)')' Elevation                  : ',
     &         90. - tetaobs,' deg'
      write(*,'(1x,a30,f7.3,a4)')' Angle azimutal observation : ',
     &         fiobs,' deg'
      if (isol.eq.1) write(*,'(1x,a36)') ' Le trajet touche le sol '
      write(*,*)
      if (iecla.eq.1) write(*,'(1x,a36)') ' Calcul eclairement au sol' 
      write(*,*)
      if (ialb.gt.0) then
         write(*,'(a31,a15)') 'fichier albedo utilise : ',
     &                      TRIM(ADJUSTL(nom_alb))
      else
         write(*,'(1x,a30,f7.3,a3)')' Albedo sol : ',galb
      endif
      write(*,'(1x,a30,f7.3,a3)')' Temperature du sol : ',Tsol
      write(*,'(1x,a30,f7.3,a3)')' Altitude sol : ',alt_sol
      write(*,*)
      write(*,'(a31,a25)') 'Profil atmospherique utilise : ',
     &                      TRIM(ADJUSTL(nom_prof))
      write(*,'(a31,a25)') 'Profil aerosol utilise : ',
     &                      TRIM(ADJUSTL(nom_aer))
      if (inua.gt.0) write(*,'(a31,a25)') 'Nuages type : ',
     &                      TRIM(ADJUSTL(nom_nua))
      write(*,*)
      write(*,'(1x,a30,i5,a1,i5,a5)') ' Bande spectrale : ',
     &      ivmin,'-',ivmax,' cm-1'
      write(*,'(31x,f7.4,a1,f7.4,a8)') vmin,'-',
     &                                  vmax,' microns'
      if (iconvol.eq.1) then
         ipas = int(per)
         write(*,'(1x,a26,f7.3,a5)') 
     &                          ' Resolution spectrale : ',fwhm,' cm-1'
         write(*,'(1x,a26,i3,a5)') 
     &                        ' Pas ecriture resultats : ',ipas,' cm-1'
      endif
      if (iconvol.eq.2) then
         write(*,'(1x,a30,f10.5,a8)')
     &                        ' Resolution spectrale : ',fwhm,' microns'
         write(*,'(1x,a32,f10.5,a8)')
     &                       ' Pas ecriture resultats : ',per,' microns'
      endif
     
     
      write(16,*)
      write(16,*) ' G�om�trie d''observation '
      write(16,*) ' ************************ '
      write(16,'(1x,a30,f7.3,a4)')' Angle zenithal observation : ',
     &         tetaobs,' deg'
      write(16,'(1x,a30,f7.3,a4)')' Angle azimutal observation : ',
     &         fiobs,' deg'
      if (isol.eq.1) write(16,'(1x,a36)') ' Le trajet touche le sol '
      if (ilimb.eq.1) write(16,'(1x,a36,f7.3,a2)') 
     &    ' Visee au limbe, altitude tangente :',htan,'km'
      write(16,'(a31,a15)') 'Profil atmospherique utilise : ',
     &                      TRIM(ADJUSTL(nom_prof))
      write(16,'(a31,a15)') 'Profil aerosol utilise : ',
     &                      TRIM(ADJUSTL(nom_aer))
      if (inua.gt.0) write(16,'(a31,a15)') 'Nuages type : ',
     &                      TRIM(ADJUSTL(nom_nua))
      write(16,*)
      write(16,'(1x,a30,i5,a1,i5,a5)') ' Bande spectrale : ',
     &      ivmin,'-',ivmax,' cm-1'
      write(16,'(31x,f7.4,a1,f7.4,a8)') vmin,'-',
     &                                  vmax,' microns'
      if (iconvol.eq.1) then
         ipas = int(per)
         write(16,'(1x,a26,f7.3,a5)') 
     &                          ' Resolution spectrale : ',fwhm,' cm-1'
         write(16,'(1x,a26,i3,a5)') 
     &                        ' Pas �criture r�sultats : ',ipas,' cm-1'
      endif
      if (iconvol.eq.2) then
         write(16,'(1x,a30,f7.3,a8)')
     &                        ' Resolution spectrale : ',fwhm,' microns'
         write(16,'(1x,a32,f7.3,a8)')
     &                       ' Pas �criture r�sultats : ',per,' microns'
      endif
      
      
      write(*,*)
      write(*,'(a40)') ' Vouler vous continuer le calcul ? (o/n)'
      write(*,'(a40)') ' ---------------------------------------'
  !!!!!!!!!!!!!!!!!    read(*,*) rep
      rep = 'o'
      if ((rep.eq.'n').or.(rep.eq.'N')) then
         close (3)
         close (4)
         close (7)
         close (8)
         close (11)
         close (12)
         close (15)
         close (16)
         R = system('rmdir /S/Q '//trim(path))
         stop
      endif

      nomfic= TRIM(ADJUSTL(nom_nua))
      if (nomfic.eq.'Cirrus_standard') ityp_nua = 1
      if (nomfic.eq.'Cirrus_fin') ityp_nua = 2      
     
      open(17,file=trim(path)//'\Lum_convol_Rayjn.xls')
      if (iecla.eq.1) then
         open(11,file=trim(path)//'\flux_Rayjn.xls')
         write(11,'(a10,18x,a15,16x,a15,7x,a15,17x,a15,10x,a15)') 
     &    '(cm-1)','(W/m2/cm-1)','(ph/s/m2/cm-1)','(microns)',
     &    '(W/m2/mic)','(ph/s/m2/mic)'
         write(11,'(6(a15,12x))') 'Nb d''ondes','Eclairement',
     &    'Eclairement','Longueur d''onde','Eclairement','Eclairement'
         open(18,file=trim(path)//'\Flux_conv_Rayjn.xls')
      endif
      open(20,file=trim(path)//'\Trans_conv_Rayjn.xls')
      if (iconvol.eq.1) then
         write(17,'(5(a15,10x))') 'Nb d''ondes','Luminance',
     &    'Luminance','Luminance'
         write(17,'(a10,18x,a15,17x,a15,10x,a15,8x,a15)') 
     & '(cm-1)','(W/m2/sr/cm-1)','(ph/s/m2/sr/cm-1)','(R/cm-1)'
        if (iecla.eq.1) then
           write(18,'(3(a15,10x))') 'Nb d''ondes','Flux','Flux'
           write(18,'(a10,18x,a15,17x,a15)') '(cm-1)',
     &    '(W/m2/cm-1)','(ph/s/m2/cm-1)'
        endif
      else
         write(17,'(5(a15,10x))') 'Longueur d''ondes','Luminance',
     &    'Luminance','Luminance'
         write(17,'(a10,18x,a15,17x,a15,10x,a15,8x,a15)') 
     &   '(mic)','(W/m2/sr/mic)','(ph/s/m2/sr/mic)','(R/mic)'
         if (iecla.eq.1) then
           write(18,'(3(a15,10x))') 'Longueur d''ondes','Flux',
     &      'Flux'
           write(18,'(a10,18x,a15,17x,a15)') 
     &     '(mic)','(W/m2/mic)','(ph/s/m2/mic)'
         endif
      endif
         
      iv1 = ivmin
      iv2 = ivmax
      idvmax = iv2+1-iv1
      if (idvmax.gt.nsigmx) then
         write(4,*) ' Domaine spectral non inclu dans Ray_j_n'
         stop
      endif

c    ==> Lecture des donnees atmosph�riques et calcul des entr�es pour le mod�le de bande          
c           ncouche : nb de couches constituants le profil
c           pmoy : Pression moyenne sur la couche
c           tmoy : Temp�rature moyenne sur la couche
c           atmp : Temp�ratures aux intercouches            
c           ==> les donn�es thermodynamiques sont fournies � partir du sol.
c               D'o� pmoy(1) = pression au sol et pmoy(ncouche) = pression au sommet de l'atmosph�re
c           DSTMOL (densit� mol�culaire) : 1 -> H2O / 2 -> CO2 /3 -> O3 /4 -> N2O /5 -> CO /6 -> CH4 /
c                                       7 -> O2 /8 -> NO /9 -> SO2 /10 -> NO2 /11 -> NH3 /12 -> HNO3 /
c           DSTCFC (densit� CFC) :  CFC
c           DSTCONT : Continua

c  !! Premi�re it�ration : g�om�trie vis�e nadir pour calcul grandeurs optiques de chaque couche 
c     Le nombre de segments optiques = nombre de couches

      iglow = 1  ! Rayonnement nightglow pris en compte

      call geometrie(pmoy,tmoy,dstmol,dstcfc,dstcont,alt,hum_rel,
     &     atmp,ncouche,inua,ityp_nua,altb,alts,ialtb,ialts,hbg,
     &     epg,ep_OH,ep_OI,ep_Na,nom_prof,path,nom_nua,i120)
      nseg = ncouche
      
      ! Calcul de l'indice de l'altitude du sol
      eob = 0
      i = 0
      do while (eob.eq.0)
         i = i + 1
         if (alt_sol.eq.alt(i)) then
            ialt_sol = i
            prop_sol = 0.
            eob = 1
         endif
         if ((alt_sol.gt.alt(i)).and.(alt_sol.lt.alt(i+1))) then
            ialt_sol = i
            prop_sol = (alt_sol-alt(i))/(alt(i+1)-alt(i))
            eob = 1
         endif
      enddo
      ialt_soldm = ncouche + 2 - ialt_sol
      ! fin du calcul 
      
      if (iglow.eq.1) then
         ALLOCATE(sgma(nsigmx),STAT=ok)
         IF (ok /= 0) THEN
            print*,'#ERR : probleme allocation sgma dans Ray_j_n'
            call exit(1)
         ENDIF
         ALLOCATE(ver(nsigmx,knln,nesp),STAT=ok)
         IF (ok /= 0) THEN
            print*,'#ERR : probleme allocation ver dans Ray_j_n'
           call exit(1)
         ENDIF      
  
       ! Initialisation du rayonnement nightglow
         ivc = 0
         do iv = iv1,iv2
            ivc = ivc + 1
            sgma(ivc) = float(iv)
            do l=1,knln
               do iesp=1,nesp
                  ver(ivc,l,iesp) = 0.
               enddo
            enddo
         enddo
         
      ! Recherche du profils nightglow le plus proche des conditions de calcul 
         call intensite_OH(imois,lat,long,spect_OH,n_OH,
     &                     lat_OH,long_OH,isais,heure)
         write(*,*) 'Fichier OH selectionne : ',spect_OH(1:n_OH)
         write(*,*) 'latitude :',lat_OH,'  longitude :',long_OH,
     &              '  saison :',isais+1
      !  Esp�ce nightglow N�1 : OH
      !  Lecture du fichier contenant le rayonnement OH � 1 cm-1 en W/cm3/cm-1
         open(2,file='DATA/'//spect_OH(1:n_OH))
         read(2,*) ivg1,ivg2,hbg,Nv1,Nv2,Nv3,Nv4,Nv5,Nv6,Nv7,Nv8,Nv9
         write(*,'(a38,9(I4,2x))') 
     &            '  Donnees rayonnement OH : [OH(v=1,9)]',Nv1,Nv2,Nv3,
     &                                         Nv4,Nv5,Nv6,Nv7,Nv8,Nv9
         if ((Tc.eq.200.).and.(Tv.eq.9000.).and.(dn0.eq.3.57e4)) then
            write(*,*) ' Donn�es OH article Rousselot'
            call sleep (5)
         endif
         if ((Tc.eq.200.).and.(Tv.eq.10800.).and.(dn0.eq.1.15e4)) then
            write(*,*) ' Donn�es OH article Steed and Baker'
            call sleep (5)
         endif
         if ((Tc.eq.200.).and.(Tv.eq.9000.).and.(dn0.eq.2.8e4)) then
            write(*,*) ' Donn�es OH Saber / La Silla'
            call sleep (5)
         endif
         nvg = ivg2-ivg1+1
         if ((iv2.gt.ivg1).and.(iv1.lt.ivg2)) then
            if (iv1.gt.ivg1) then
               ivc = 0
               do iv=ivg1,ivg2
                  read(2,*) ivg,dump
                  if (ivg.ge.iv1) then
                     ivc = ivc+1
                     ver(ivc,1,1) = dump
                  endif
               enddo
            else
               ivc = 0
               do iv=iv1,iv2
                  ivc=ivc+1
                  if ((iv.ge.ivg1).and.(iv.le.ivg2)) then
                     read(2,*) ivg,dump
                     ver(ivc,1,1)= dump
                  endif
               enddo
            endif
         endif
         close(2)
      
      ! on pond�re le rayonnement par le profil OH
         open(2,file='DATA/profil_nightglow_OH.txt')
         read(2,*) nc_ng
         do i=1,nc_ng
            read(2,*) alt_ngw(i),prof_ng(i)
         enddo
         close (2)
         do ia=1,ncouche+1    ! on recherche les indices des couches inf et sup
            if (alt(ia).le.alt_ngw(1)) ideb = ia
            if (alt(ia).lt.alt_ngw(nc_ng)) ifin = ia + 1
            prf_ng(ia) = 0.
         enddo
         do i=1,nc_ng-1    ! interpolation si n�cessaire du profil sur 
                           ! les altitudes du profils thermodynamique
            alt1 = alt_ngw(i)
            alt2 = alt_ngw(i+1)
            altf = alt_ngw(nc_ng)
            prf1 = prof_ng(i)
            prf2 = prof_ng(i+1)
            dz = alt2 - alt1
            do ia=ideb,ifin
               if (alt(ia).eq.alt1) prf_ng(ia) = prof_ng(i)
               if ((alt(ia).gt.alt1).and.(alt(ia).lt.alt2)) then
                   prf_ng(ia) = prf1 + (prf2-prf1)/dz*(alt(ia)-alt1)
               endif
            enddo
         enddo  
         if (alt(ifin).eq.altf) prf_ng(ifin) = prof_ng(nc_ng)
                       
         do ia=2,ncouche + 1
            ivc = 0
            do iv=iv1,iv2
               ivc=ivc+1
               ver(ivc,ia,1) = ver(ivc,1,1)*prf_ng(ia)
            enddo
           ! write(202,*) alt(ia),ver(1,ia,1)
         enddo
         
!  Esp�ce nightglow N�2 : OI � 577.7 nm
      !  Lecture du fichier contenant le rayonnement OI � 1 cm-1 en W/cm3/cm-1
         open(2,file='DATA/Spectre_OI_1cm-1.txt')
         read(2,*) ivg1,ivg2
         nvg = ivg2-ivg1+1
         if ((iv2.gt.ivg1).and.(iv1.lt.ivg2)) then
            if (iv1.gt.ivg1) then
               ivc = 0
               do iv=ivg1,ivg2
                  read(2,*) ivg,dump
                  if (ivg.ge.iv1) then
                     ivc = ivc+1
                     ver(ivc,1,2) = dump
                  endif
               enddo
            else
               ivc = 0
               do iv=iv1,iv2
                  ivc=ivc+1
                  if ((iv.ge.ivg1).and.(iv.le.ivg2)) then
                     read(2,*) ivg,dump
                     ver(ivc,1,2)= dump
                  endif
               enddo
            endif
         endif
         close(2)
      
      ! on pond�re le rayonnement par le profil OI
         open(2,file='DATA/profil_nightglow_OI.txt')
         read(2,*) nc_ng
         do i=1,nc_ng
            read(2,*) alt_ngw(i),prof_ng(i)
         enddo
         close (2)
         do ia=1,ncouche+1    ! on recherche les indices des couches inf et sup
            if (alt(ia).le.alt_ngw(1)) ideb = ia
            if (alt(ia).lt.alt_ngw(nc_ng)) ifin = ia + 1
            prf_ng(ia) = 0.
         enddo
         do i=1,nc_ng-1    ! interpolation si n�cessaire du profil sur 
                           ! les altitudes du profils thermodynamique
            alt1 = alt_ngw(i)
            alt2 = alt_ngw(i+1)
            altf = alt_ngw(nc_ng)
            prf1 = prof_ng(i)
            prf2 = prof_ng(i+1)
            dz = alt2 - alt1
            do ia=ideb,ifin
               if (alt(ia).eq.alt1) prf_ng(ia) = prof_ng(i)
               if ((alt(ia).gt.alt1).and.(alt(ia).lt.alt2)) then
                   prf_ng(ia) = prf1 + (prf2-prf1)/dz*(alt(ia)-alt1)
               endif
            enddo
         enddo  
         if (alt(ifin).eq.altf) prf_ng(ifin) = prof_ng(nc_ng)
                       
         do ia=2,ncouche + 1
            ivc = 0
            do iv=iv1,iv2
               ivc=ivc+1
               ver(ivc,ia,2) = ver(ivc,1,2)*prf_ng(ia)
          !     if (iv.eq.17930) write(203,*) alt(ia),ver(ivc,ia,2)
            enddo
         enddo

!  Esp�ce nightglow N�3 : Na � 589.3 nm
      !  Lecture du fichier contenant le rayonnement Na � 1 cm-1 en W/cm3/cm-1
         open(2,file='DATA/Spectre_Na_1cm-1.txt')
         read(2,*) ivg1,ivg2
         nvg = ivg2-ivg1+1
         if ((iv2.gt.ivg1).and.(iv1.lt.ivg2)) then
            if (iv1.gt.ivg1) then
               ivc = 0
               do iv=ivg1,ivg2
                  read(2,*) ivg,dump
                  if (ivg.ge.iv1) then
                     ivc = ivc+1
                     ver(ivc,1,3) = dump
                  endif
               enddo
            else
               ivc = 0
               do iv=iv1,iv2
                  ivc=ivc+1
                  if ((iv.ge.ivg1).and.(iv.le.ivg2)) then
                     read(2,*) ivg,dump
                     ver(ivc,1,3)= dump
                  endif
               enddo
            endif
         endif
         close(2)
      
      ! on pond�re le rayonnement par le profil Na
         open(2,file='DATA/profil_nightglow_Na.txt')
         read(2,*) nc_ng
         do i=1,nc_ng
            read(2,*) alt_ngw(i),prof_ng(i)
         enddo
         close (2)
         do ia=1,ncouche+1    ! on recherche les indices des couches inf et sup
            if (alt(ia).le.alt_ngw(1)) ideb = ia
            if (alt(ia).lt.alt_ngw(nc_ng)) ifin = ia + 1
            prf_ng(ia) = 0.
         enddo
         do i=1,nc_ng-1    ! interpolation si n�cessaire du profil sur 
                           ! les altitudes du profils thermodynamique
            alt1 = alt_ngw(i)
            alt2 = alt_ngw(i+1)
            altf = alt_ngw(nc_ng)
            prf1 = prof_ng(i)
            prf2 = prof_ng(i+1)
            dz = alt2 - alt1
            do ia=ideb,ifin
               if (alt(ia).eq.alt1) prf_ng(ia) = prof_ng(i)
               if ((alt(ia).gt.alt1).and.(alt(ia).lt.alt2)) then
                   prf_ng(ia) = prf1 + (prf2-prf1)/dz*(alt(ia)-alt1)
               endif
            enddo
         enddo  
         if (alt(ifin).eq.altf) prf_ng(ifin) = prof_ng(nc_ng)
                       
         do ia=2,ncouche + 1
            ivc = 0
            do iv=iv1,iv2
               ivc=ivc+1
               ver(ivc,ia,3) = ver(ivc,1,3)*prf_ng(ia)
           !    if (iv.eq.16969) write(204,*) alt(ia),ver(ivc,ia,3)
            enddo
         enddo

         
c     ! on v�rifie le VER (W/cm3) avec le filtre OHB de Saber 
c     ! dans Saber, le VER est en ergs/cm3/s d'o� on multiplie par 1e7
         ALLOCATE(ver_c(nsigmx),STAT=ok)
         IF (ok /= 0) THEN
            print*,'#ERR : probleme allocation ver_c dans Ray_j_n'
           call exit(1)
         ENDIF   
         open(19,file='RESU/VER_Saber.xls')
        ! nsig = ivg2 - ivg1 + 1
         nsig = iv2 - iv1 + 1
         it_fi = 1
         write(*,*)
         write(*,*) ' Calcul VER Saber '
         do i=1, ncouche+1
            if (it_fi.eq.1) then
               do j=1,nsig
                  ver_c(j) = ver(j,i,1)
               enddo
               call filtre(nsigmx,sgma,ver_c,nsig,ver_integ,2,it_fi) 
               if (it_fi.eq.1) write(19,*) alt(i),ver_integ*1e7
            endif
         enddo
         write(16,*)
         close (19)
         DEALLOCATE(ver_c)
         
c        !  calcul de la valeur moyenne du VER dans chaque couche que l'on charge 
c         ! apr�s dans le tableau VER
         ivc = 0
         do iv=iv1,iv2
            ivc=ivc+1
            do i=1,ncouche
               if (i.eq.1) then
                  do iesp=1,nesp
                     ver(ivc,i,iesp) = 0.  ! on remet � z�ro la premi�re couche
                  enddo
               endif
               dump1 = ver(ivc,i,1)
               ver(ivc,i,1) = (dump1 + ver(ivc,i+1,1))/2 
               dump2 = ver(ivc,i,2)
               ver(ivc,i,2) = (dump2 + ver(ivc,i+1,2))/2  ! attention, maintenant VER contient la valeur moyenne et va de 1 � ncouche
               dump3 = ver(ivc,i,3)
               ver(ivc,i,3) = (dump3 + ver(ivc,i+1,3))/2 
            enddo
         enddo
         DEALLOCATE(sgma)
      endif

      call donnees_aerosols(iaero,nom_aer,ncouche,nlambda,nang,sigext,
     &     sigabs,phaze,sigmaext,sigmaabs,alamda,ang,phase,
     &     hum_rel,alt)
      call donnees_nuage(inua,nom_nua,ityp_nua,ncouche,nlambda,
     &     nang,nuaext,nuaabs,nuaphaze,nuamext,nuamabs,alamda,ang,
     &     nuaphase,alt,ialtb,ialts,nlambdan,nangn)
     
      if (nangn.ne.nang) then 
         write(*,*) 'Angles fct de phase nuages et aerosols incoherents'
         stop
      else
         nangn = nang
      endif
      if (nlambdan.ne.nlambda) then 
         write(*,*) 'lambda fct de phase nuages et aerosols incoherents'
         stop
      else
         nlambdan = nlambda
      endif
      
     
c  !! Calcul des grandeurs solaires et lunaires :
c         flux solaire ou lunaire F0 au sommet de l'atmosphere
c         angle de diffusion : Hypoth�se trajet rectiligne =>  l'angle de diffusion 
c                              est constant sur le trajet
      if (ilum.eq.1) then
         d_phi = abs(fiobs - azsol)        
         if (teta0.gt.89.0) then
            if (tetal.lt.89.0) then
               teta0 = tetal
               d_phi = abs(fiobs - azlun)
               call solar_depot(Jday,solar_spec,iv1,iv2)
               call depot_lunaire(solar_spec,iv1,iv2,ang_phas,
     &                            Rst,Rtl)
               write(16,'(a37,f10.3)') 
     &                 ' azimut relatif observateur /lune : ',d_phi
               write(*,'(a37,f10.3)') 
     &                 ' azimut relatif observateur /lune : ',d_phi
            else
               teta0 = 89.0
               nv = iv2-iv1+1
               do iv = 1,iv2-iv1+1
                  solar_spec(iv) = 0.
               enddo
            endif
         else
            call solar_depot(Jday,solar_spec,iv1,iv2)
            write(16,'(a37,f10.3)') 
     &              ' azimut relatif observateur /soleil : ',d_phi
            write(*,'(a37,f10.3)') 
     &              ' azimut relatif observateur /soleil : ',d_phi
         endif
            
         amuobs = cos(rad*tetaobs)
         am0 = cos(rad*teta0)
         amu_dif = am0*amuobs +
     &        sqrt((1-am0**2)*(1-amuobs**2))*cos(d_phi*rad)
         if  (abs(amu_dif).gt.1.0) amu_dif = sign(1.0,amu_dif)
         ang_dif = acos(amu_dif)/rad
         do i=1,nang-1
            ang1 = ang(i)
            ang2 = ang(i+1)
            if ((ang_dif.ge.ang1).and.(ang_dif.le.ang2)) iang_dif=i
         enddo
      endif
      
c     Chargement du tableau d'albedo spectral si requis
      if (ilum.eq.1) then
        if (ialb.gt.0) then     
           ALLOCATE(lam_alb(5000),STAT=ok)
           IF (ok /= 0) THEN
              print*,'#ERR : probleme allocation lam_alb dans Rayjn'
              call exit(1)
           ENDIF  
           ALLOCATE(alb(5000),STAT=ok)
           IF (ok /= 0) THEN
              print*,'#ERR : probleme allocation alb dans Rayjn'
              call exit(1)
           ENDIF     
           i=0
           ios = 0
           if (ialb.eq.1) then
              open(2,file='DATA\'//trim(nom_alb))
           else
              open(2,file='UTIL\'//trim(nom_alb))
           endif
           read(2,*)
           read(2,*)
           do while(ios==0)
              i=i+1
              read(2,*,iostat=ios) ipoub,lam_alb(i),alb(i)
           enddo
           nval = i - 1
           close(2)
           ivc = 0
           do iv = iv1,iv2
              ivc = ivc + 1
              ivalb = 1e4/lam_alb(nval)
              if (iv.gt.ivalb) then
                 do i=1,nval-1
                    j=nval+1-i
                    ivalb1 = 1e4/lam_alb(j)
                    ivalb2 = 1e4/lam_alb(j-1)
                    if ((iv.ge.ivalb1).and.(iv.lt.ivalb2)) then
                       alb1 = alb(j)
                       alb2 = alb(j-1)
                       albedo(ivc) = alb1 + float(iv-ivalb1)/
     &                               float(ivalb2-ivalb1)*(alb2 - alb1)
                       albedo(ivc) = albedo(ivc)/100.
                    endif
                 enddo
              else
                 albedo(ivc) = alb(nval)/100.
              endif
              ivalb = 1e4/lam_alb(1)
              if (iv.gt.ivalb) albedo(ivc) = alb(1)/100.
           enddo  
           DEALLOCATE (lam_alb)
           DEALLOCATE (alb) 
        endif
      endif
      
      ! �criture transmission des mol�cules s�par�ment
      open(50,file=trim(path)//'\H2O.xls')
      open(51,file=trim(path)//'\CO2.xls')
      open(52,file=trim(path)//'\O3.xls')
      open(53,file=trim(path)//'\N2O.xls')
      open(54,file=trim(path)//'\CO.xls')
      open(55,file=trim(path)//'\CH4.xls')
      open(56,file=trim(path)//'\O2.xls')
      open(57,file=trim(path)//'\NO.xls')
      open(58,file=trim(path)//'\SO2.xls')
      open(59,file=trim(path)//'\NO2.xls')
      open(60,file=trim(path)//'\NH3.xls')
      open(61,file=trim(path)//'\HNO3.xls')
      
      init = 0
    !! Calcul de la transmission le long de la ligne de vis�e
1     if ((ilum.eq.0).or.(init.eq.2)) then 
         write(*,*) ' CALCUL DU TRAJET OBSERVE'
         call geometrie_obs(tetaobs,hobs,hfin,pmoy,tmoy,
     &dstmol,dstcfc,dstcont,alt,nlambda,nang,sigmaext,sigmaabs,
     &phase,sigext,sigabs,phaze,nseg,indice,inua,ityp_nua,altb,
     &alts,ialtb,ialts,nuaext,nuaabs,nuaphaze,nuamext,nuamabs,
     &nuaphase,hbg,epg,ep_OH,ep_OI,ep_Na,nom_prof,ds,ray_t,path,nom_nua)
      endif
  
      !! Calcul de la transmission le long du trajet solaire  
      if (init.eq.1) then
         htoa = 150.  !! peut �tre un bug sur hobs !!
         write(*,*) ' CALCUL DU TRAJET SOLAIRE'
         hdeb = 0.
         call geometrie_obs(teta0,htoa,hdeb,pmoy,tmoy,
     &dstmol,dstcfc,dstcont,alt,nlambda,nang,sigmaext,sigmaabs,
     &phase,sigext,sigabs,phaze,nseg,indice,inua,ityp_nua,altb,alts,
     &ialtb,ialts,nuaext,nuaabs,nuaphaze,nuamext,nuamabs,nuaphase,
     &hbg,epg,ep_OH,ep_OI,ep_Na,nom_prof,ds0,ray_t,path,nom_nua)   
      endif

      call donnees_parametres(ntemp,ip,ibinx,iprm,iblk,iblock,irecnm,
     &imolx,ialfx,ipx,ibndwd,iv1,ifwhm,mxfreq,nseg,tband,sdz,odz,
     &tmoy,wt,sdzx,odzx,iparam,ibin,imol,ialf,jj,ff,itb,itbx)
     
c     Resolution du modele de bande
      dv = float(ibndwd)
      

c     DEBUT DE BOUCLE SUR LES NOMBRES D''ONDES
      ivc = 0
      sumwat = 0.
      sumph = 0.
      sumflux_ph = 0.
      do iv = iv1,iv2
         ivc = ivc + 1
         if ((iv.eq.ivmin).and.(init.eq.0)) then
            write(*,*) 
            write(*,'(a45,i5,a1,i5)') 
     &          'DEBUT BOUCLE SUR LES NOMBRES D''ONDES IV = ',iv1,
     &          '/',iv2
            write(*,*)
         endif
         ivtest = 1000*int(iv/1000)
         if (ivtest.eq.iv) write(*,'(a11,i5,a1,i5)') '     IV = ',
     &                 iv,'/',iv2
         if ((iv.eq.ivmax).and.(init.eq.2)) 
     &      write(*,*) 'FIN BOUCLE SUR LES NOMBRES D''ONDES IV = ',iv2

         ! Appel du mod�le de bande
         
    
            call mod_bande(ntemp,ip,ibinx,iprm,iblk,iblock,irecnm,
     & imolx,ialfx,ipx,ibndwd,iv1,ifwhm,mxfreq,nseg,tband,sdz,odz,
     & pmoy,tmoy,dstmol,dstcfc,wt,dopfac,sdzx,odzx,
     & iparam,ibin,imol,ialf,jj,ff,itb,itbx,iv,dv,trans,tranc,init)     
     
c        Fin du calcul Haute Resolution dans lequel on a calcule la transmission moyenne
c        cummulee pour la molecule k, trans(k) entre la couche (ou le segment) 1 et la 
c        couche (ou le segment) l, ainsi que la transmission moyenne cummulee pour le CFC kp, 
c        tcfc(k) entre la couche (ou le segment)1 et la couche (ou le segment) l
c        la transmission totale est stock�e dans le tableau tranc.

c        Calcul des continua
         call interp(iv,iv1,ucont,dstcont,taucont,tauold,taunew,tranc,
     &               nseg,init)
         
c        Transmission totale
         transmission_tot = tranc(nseg)
         if (tranc(nseg).gt.0.) then
            tautot = -log(tranc(nseg))
         else
            tautot = 40.
         endif
         if (tautot.gt.40.) transmission_tot = 0.

         do ilamb = 1,nlambda
            V0 = 10000./float(iv)
            vdif = v0 -alamda(ilamb)
            if (vdif.lt.0.) goto 4
         enddo
4        vcoef = vdif/(alamda(ilamb) - alamda(ilamb-1))
         if ((ilum.eq.0).or.(init.gt.0)) then
c           Ecriture de la transmission totale de chaque segment dans le tableau tran_seg
c           tran_seg(l) = transmission cumul�e entre le segment 1 et le segment l
c                      tranc(1) = transmission mol�culaire cumul�e du premier segment � partir de l'observateur 
c                                    au segment nseg (cible)
c                      tranc(nseg) = transmission mol�culaire totale de l'observateur � la cible       
c                      sigmaext(1) = extinction a�rosols au niveau de l'observateur
c                      sigmaext(nseg) = extinction a�rosols au niveau de la cible     
c                      rayl = coefficient de diffusion rayleigh 
c                On cumule donc les extinctions a�rosols entre  l'observateur (l=1) et la cible (nseg) 
c                d'o�  tran_seg(l) la transmission totale cumul�e entre le segment 1 (l'observateur) et le segment l

            tauex = 0.
            do l=1,nseg
                sigex = 0.
                ex_nua = 0.
               sigex = sigmaext(ilamb,l) + 
     &                (sigmaext(ilamb,l) - sigmaext(ilamb-1,l))*vcoef
               ex_nua = nuamext(ilamb,l) + 
     &                (nuamext(ilamb,l) - nuamext(ilamb-1,l))*vcoef     ! nuages
               sigab = sigmaabs(ilamb,l) + 
     &                (sigmaabs(ilamb,l) - sigmaabs(ilamb-1,l))*vcoef
               ab_nua = nuamabs(ilamb,l) + 
     &                (nuamabs(ilamb,l) - nuamabs(ilamb-1,l))*vcoef     ! nuages
               sigdia(l) = sigex - sigab      ! coef diffusion aerosols
               sigdin(l) = ex_nua - ab_nua    ! coef diffusion nuage
c ==>          Diffusion Rayleigh
               rayl(l) = float(iv)**4/(9.38076E+18
     &                              - 1.08426E+09*float(iv)**2)
com            densit� int�gr�e de l'air dans dstcont(12)
               rayl(l) = dstcont(l,12)*rayl(l)
               tauex= tauex + sigex + ex_nua + rayl(l)
               tran_seg(l) = tranc(l)*exp(-tauex)
               if (init.ne.1) then
                  do iang = 1,nang
                     phsf(iang,l) = phase(ilamb,iang,l) +
     &              (phase(ilamb,iang,l) - phase(ilamb-1,iang,l))*vcoef
                     phsfn(iang,l) = nuaphase(ilamb,iang,l) + 
     &              (nuaphase(ilamb,iang,l) - nuaphase(ilamb-1,iang,l))
     &               *vcoef
                  enddo
               else
                  tran_sol(l,ivc) = tran_seg(l)
               endif
            enddo
            
c-------------------------------------------------------------------
C Ecriture des transmissions
C-------------------------------------------------------------------
            
            if (init.ne.1) then
               alam = 1.E4/float(iv)
c           Ecriture de la transmission mol�culaire dans le fichier "trans_molecule"
               write(3,*) iv,alam,transmission_tot
            
c           Ecriture de la transmission totale dans le fichier "trans.totale"
               write(7,*) iv,alam,tran_seg(nseg)
         
c           Ecriture des transmissions dans des fichiers separes pour validation
               do kt=1,nspc+2
                  inum = 20 + kt
                !  write(inum,*) iv,exp(-taucont(kt))
               enddo
               do ific = 1,nspc
                  inum = 49+ific
                  write(inum,*) iv,trans(ific)
               enddo
               do ific = 1,nspecx
                  inum = 49+nspc+ific
               enddo 
            endif         
            
         elseif (init.eq.0) then
           
c ==> Calcul de la diffusion multiple

c        teta0 = angle z�nital solaire
c        thk = epaisseur optique de chaque couche en partant du sommet de l'atmosphere
c        omg = albedo de diffusion simple
c        ang,phsf = angle et valeur de la fonction de phase normalis�e � 1
c        galb = albedo du sol
c        atmp = profil de temperature dans chaque couche
c        tsol = temperature du sol
c        ntau = nb d'epaisseurs optiques ou le calcul des fct source est effectue
c        utau = valeurs des epaisseurs optiques ou le calcul des fct source est effectue
       
c        Calcul des profondeurs optiques
c        tranc(1) = transmission mol�culaire de la couche au niveau du sol        
c        tranc(l) = transmission cumul�e entre la couche 1 et la couche l, la couche 1 �tant la couche au niveau du sol
c        tranc(nseg) = transmission mol�culaire cumul�e entre le segment 1 (sol) et le segment ncouche  au sommet de l'atmosph�re 
c          
c        sigmaext(1) = extinction a�rosols au  niveau du sol           
c        sigmaext(nseg) = extinction a�rosols au sommet de l'atmosph�re 
           
            if (ivc.eq.1) then 
               write(*,*) 'Calcul des parametres pour la diffusion ',
     &         'multiple'
  !      Ecriture des param�tres optiques pour la diffusion pour le premier nombre d'ondes
               write(16,*)
               write(16,*) ' ***********************************'
               write(16,*)
               write(16,*) 'Ecriture des param�tres optiques pour ',
     &         'la diffusion � ',iv,' cm-1' 
               write(16,*)
               write(16,*) 'INFO : la couche n�1 est la couche au ',
     &         'niveau du sol'
               write(16,'(7(a8,2x))') 'n�','tau','ext_aero','dif_aero',
     &         'dif_rayl','omega','epa_OH'
            endif   
           
           
            do l=ncouche,1,-1
               sigex = sigmaext(ilamb,l) + 
     &                (sigmaext(ilamb,l) - sigmaext(ilamb-1,l))*vcoef   ! a�rosols
               ex_nua = nuamext(ilamb,l) + 
     &                (nuamext(ilamb,l) - nuamext(ilamb-1,l))*vcoef     ! nuages
               sigab = sigmaabs(ilamb,l) + 
     &                (sigmaabs(ilamb,l) - sigmaabs(ilamb-1,l))*vcoef   ! a�rosols
               ab_nua = nuamabs(ilamb,l) + 
     &                (nuamabs(ilamb,l) - nuamabs(ilamb-1,l))*vcoef     ! nuages
               sigdia(l) = 0.
               sigdin(l) = 0.
               if (sigex.gt.sigab) sigdia(l) = sigex - sigab      ! coef diffusion aerosols
               if (ex_nua.gt.ab_nua) sigdin(l) = ex_nua - ab_nua    ! coef diffusion nuage
c ==>          Diffusion Rayleigh
               rayl(l) = float(iv)**4/(9.38076E+18
     &                              - 1.08426E+09*float(iv)**2)
com            densit� int�gr�e de l'air dans dstcont(12)
               rayl(l) = dstcont(l,12)*rayl(l)
               thk(l,ivc) = tranc(l) ! �paisseur optique mol�culaire
               thk(l,ivc) = thk(l,ivc) + sigex + ex_nua + rayl(l)  ! �paisseur optique
               omg(l,ivc) = (sigdia(l) + sigdin(l) + rayl(l))/thk(l,ivc)        ! alb�do de diffusion simple
       !    write(78,*) l,thk(l,ivc),omg(l,ivc),sigdia(l),sigdin(l),sigex
               do iang = 1,nang
                  phsf(iang,l) = phase(ilamb,iang,l) +
     &              (phase(ilamb,iang,l) - phase(ilamb-1,iang,l))*vcoef
                  phsn = nuaphase(ilamb,iang,l) + 
     &              (nuaphase(ilamb,iang,l) - nuaphase(ilamb-1,iang,l))
     &               *vcoef
               ! Fonction de phase Rayleigh normalis�e � 1                 
                  cos_ang = cos(ang(iang)*rad)
                  phrl = 3./4.*(1.+cos_ang**2)/(4.*PI)
               ! Fonction de phase totale
                  phsf(iang,l) =
     &                  (phsf(iang,l)*sigdia(l) + phsn*sigdin(l) + 
     &                   phrl*rayl(l))/(sigdia(l) + sigdin(l) + rayl(l))
               enddo
               if (ivc.eq.1) then
  !      Ecriture des param�tres optiques pour la diffusion pour le premier nombre d'ondes
                  write(16,'(i3,6(3x,1e10.4))') l,thk(l,ivc),
     &            sigex+ex_nua,sigdia(l)+ sigdin(l),rayl(l),
     &            omg(l,ivc),ep_OH(l)
               endif
            enddo

                
com      ==> Calcul de la diffusion multiple choix Ordonn�es Discr�tes ou mod�le 2 Flux
com      ==>   IMPORTANT : pas de source Nightglow dans le mod�le 2 Flux
com      ==> On commence le calcul de diffusion au sommet de l'atmosph�re, d'o� on inverse l'ordre des couches
com          AJ fonction source de diffusion multiple � l'inter couche

            idm = i120 - 1 
            if (iv.eq.15873.) idm = ncouche
            do l=1,idm
               thkdm(l) = thk(idm -l+1,ivc)
               omgdm(l) = omg(idm -l+1,ivc)
               ep_gldm_OH(l) = ep_OH(idm -l+1)
               glow_OH(l) = ver(ivc,idm -l+1,1) ! on charge dans glow, la valeur moyenne sur la couche du VER
               ep_gldm_OI(l) = ep_OI(idm -l+1)
               glow_OI(l) = ver(ivc,idm -l+1,2)
               ep_gldm_Na(l) = ep_Na(idm -l+1)
               glow_Na(l) = ver(ivc,idm -l+1,3)
               do iang = 1,nang
                  angdm(iang) = ang(iang)
                  phsfdm(iang,l) = phsf(iang,idm-l+1)
               enddo
               atmpdm(2,l) = atmp(1,idm-l+1)
               atmpdm(1,l) = atmp(2,idm-l+1)
            enddo
            sigma = float(iv)
            galbedo = galb
            if(ialb.gt.0) galbedo = albedo(ivc)
            Tempsol = Tsol
            Flux0 = solar_spec(ivc)  ! en W/m2/cm-1
            dvdm = dv
            teta0dm = teta0         
            
            na1u = 1
            am1u(1) = cos(tetaobs*pi/180.)
            nfi = 1
            FI(1) = d_phi  
            
            iord = 1  ! Choix ordonn�es discr�tes ou 2 flux ! 
                      ! ATTENTION PAS DE SOURCE NIGHTGLOW DANS LE 2 FLUX
                      
                                        
                      
!       Subroutine permettant de jouer les tests de diffusion multiple 
!       issus du livre de J. Lenoble "Radiative transfer in scattering and 
!       absorbing atmospheres : standard computational prcedures"

!        call test_lenoble(nda,na1u,am1u,teta0dm,NFI,FI,ncouche,
!     &       thkdm,omgdm,nang,angdm,phsfdm,galbedo,Flux0,glow,
!     &       atmpdm,Tempsol,sigma,dvdm,ep_gldm,ialt_sol,prop_sol,iazim)
                      
                      
            if (iord.eq.1) then  
               call Ord_Discretes(nda,na1u,am1u,teta0dm,NFI,FI,idm,
     &         thkdm,omgdm,nang,angdm,phsfdm,galbedo,Flux0,glow_OH,
     &         glow_OI,glow_Na,atmpdm,Tempsol,sigma,dvdm,AJJ,ep_gldm_OH,
     &         ep_gldm_OI,ep_gldm_Na,flux_sol,ialt_soldm,prop_sol,
     &         flux_som,iazim)
               flux(ivc) = flux_sol  ! flux total (direct + diffus) au sol
     
     
!           Ecriture du flux montant au sommet de l'atmosph�re
            write(12,*) iv,flux_som

!           Dans ce programme, on a un seul angle zenithal et un seul angle azimuthal
!           => on charge les fonctions sources dans AJ
               do nl=1,idm+1
                  AJ(ivc,nl) = AJJ(1,1,nl)          
               enddo     
         !      do nl=idm+2,ncouche+1
         !         AJ(ivc,nl) = 0.          
         !      enddo          
            else
               am11 = cos(tetaobs*pi/180.)
c               call diffusion(am11,teta0dm,ncouche,thkdm,omgdm,nang,
c     &              angdm,phsfdm,galbedo,Flux0,Glow0,atmpdm,Tempsol,
c     &              ivc,dvdm,sigma,AJ,flux_sol)
c                    flux(ivc) = flux_sol
                    write(6406,*) iv,flux_sol
               do nl=1,ncouche+1
                  if (ivc.eq.1) write(6405,*) nl,AJ(1,nl)*iv*iv/1e8
               enddo
            endif
            if ((idbg.eq.1).and.(ivc.eq.1)) then
               open(6400,file='Fct_source')
               write(6400,*) ' N� couche    tau    AJ'
               utauc = 0
               do nl=1,ncouche+1       
                  write(6400,*) nl,utauc,AJ(1,nl),AJ(1,nl)*iv*iv/1e8  ! on passe en �m pour comparaison avec Modtran
                  utauc = utauc+thkdm(nl)
               enddo
               close (6400)
            endif
         endif   ! Fin du choix calcul luminance ou calcul transmission
         
        
         ! Calcul de la luminance   
         if ((ilum.eq.1).and.(init.eq.2)) then
            do i=1,ncouche
               glow(i) = ver(ivc,i,1) + ver(ivc,i,2) + ver(ivc,i,3)
             enddo    
            if(ialb.gt.0) galb = albedo(ivc)
            call luminance(nseg,ncouche,tran_seg,indice,hobs,hfin,
     & omg,atmp,alt,alum,iv,ivc,aj,tran_sol,solar_spec,ang_dif,nang,
     & ang,phsf,phsfn,iang_dif,sigdia,sigdin,rayl,thk,glow,tetaobs,
     & ep_OH,galb,Tsol,teta0,flux,ds,ds0,ray_t,isol,lg,bg,lat_pion,
     & lon_pion,val_pion,freq_pion,spect_pion,ilimb,htan)

            if (init_alloc.eq.0) then
               ALLOCATE(lum_sig(nsigmx),STAT=ok)
               IF (ok /= 0) THEN
                 print*,'#ERR : probleme allocation lum_sig' ,
     &                  'dans Ray_j_n'
                 call exit(1)
               ENDIF
               ALLOCATE(sgma(nsigmx),STAT=ok)
               IF (ok /= 0) THEN
                 print*,'#ERR : probleme allocation sgma dans Ray_j_n'
                 call exit(1)
               ENDIF
               init_alloc = 1
            endif
            lum_sig(ivc) = alum
            sgma(ivc) = float(iv)
     
c           Ecriture de la luminance totale dans le fichier lum_Rayjn
            e_ph = 6.62606876E-34*2.99e8/alam*1e6
            alum_mic = alum*float(iv)/alam
            alum_ph = alum_mic/e_ph
            alum_rayl = alum_ph*4.*pi/1.e10
            write(8,'(5x,i5,22x,1e10.3,15x,1e10.3,15x,f8.5,22x,1e10.3,
     &             2(18x,1e10.3))')
     &             iv,alum,alum/e_ph,alam,alum_ph,alum_rayl
     
            sumwat = sumwat + alum
            sumph = sumph + alum/e_ph
            sumrayl = sumph*4.*pi/1.e10
          
          
            if (iecla.eq.1) then         
c              calcul de l'�clairement induit par le fond d'�toiles  
               ecl_gal = 0.
               if (float(iv).gt.10000.0) then    
                  call eclairement_galac(lat,long,heure,imois,iannee,
     &                 ijour,lat_pion,lon_pion,val_pion,freq_pion,
     &                 spect_pion,iv,ecl_gal,thk,ray_t,alt,ialt_sol,
     &                 ncouche,ivc,lat_eclip,lon_eclip,val_zodiac)
               endif
c              Ecriture du flux au sol dans le fichier flux_Rayjn
               flux(ivc) = flux(ivc) + ecl_gal
               flux_mic = flux(ivc)*float(iv)/alam
               flux_ph_c = flux(ivc)/e_ph
               flux_ph = flux_mic/e_ph
               flux_rayl = flux_ph*4.*pi/1.e10
               write(11,'(5x,i5,22x,1e10.3,22x,1e10.3,15x,f8.5,
     &              22x,1e10.3,2(18x,1e10.3))') iv,flux(ivc),
     &              flux_ph_c,alam,flux_mic,flux_ph
     
               sumflux_ph = sumflux_ph + flux_ph_c
           endif
         endif
         
      tran_tot(ivc) = tran_seg(nseg)

c     Fin de la boucle sur les nombres d'ondes
      enddo
      if (init.eq.2) then
         write(*,*)
         write(*,*) 'Int�grale luminance : ',sumwat,' W/m2/sr',
     &               sumph,' ph/s/m2/sr',sumrayl,' R'
     
          
          open(1528,file='vis�e_limbe')
          write(1528,*)  sumph,htan  !!! calculs LATMOS2 ***
     
     
         write(16,*)
         write(16,*) 'Int�grale luminance : ',sumwat,' W/m2/sr',
     &               sumph,' ph/s/m2/sr',sumrayl,' R'
     
         if (iecla.eq.1) then
            write(*,*)
            write(*,*) 'Int�grale �clairement : ',sumflux_ph,' ph/s/m2'
            write(16,*)
            write(16,*) 'Int�grale �clairement : ',sumflux_ph,' ph/s/m2'
         endif
     
c     Calcul luminance avec le filtre SABER
        write(*,*)
        write(*,*) ' Calcul luminance Saber'
        write(*,*) '***********************'
        nsig = iv2 - iv1 + 1
        it_fi = 1
        call filtre(nsigmx,sgma,lum_sig,nsig,lum_saber,2,it_fi)     
        if (it_fi.eq.1) then
           write(*,*) ' Calcul luminance avec le filtre OHB ',
     &                 'Saber : ',lum_saber,' W/m2/sr'
           write(16,*) ' Calcul luminance avec le filtre OHB ',
     &                 'Saber : ',lum_saber,' W/m2/sr'
        endif

     
     
        if (iecla.eq.1) then    
c        ! Calcul niveau de nuit


           vpas = float(iv2)
           pas = 1e4/(vpas*vpas)
         
           delam = 1e4/iv1 - 1e4/iv2
           nlamx = delam/pas + 100
           ALLOCATE(lambda(nlamx),STAT=ok)
           IF (ok /= 0) THEN
             print*,'#ERR : probleme allocation lambda dans Ray_j_n'
             call exit(1)
           ENDIF
           ALLOCATE(flux_lam(nlamx),STAT=ok)
           IF (ok /= 0) THEN
             print*,'#ERR : probleme allocation flux_lam dans Ray_j_n'
             call exit(1)
           ENDIF    
           call interp_lambda(nlamx,iv1,iv2,lambda,flux,flux_lam,
     &        nlamb)
           it_fi = 1
           write(*,*)
           write(*,*) ' Calcul niveau de nuit'
           write(*,*) '**********************'
           call filtre(nlamx,lambda,flux_lam,nlamb,aniv,1,it_fi)
         
         ! d�finition : 1 lumen = 1/683W � 0.555016 �m
         !            : 1 lux = 1 lumen/m2

           if (it_fi.eq.1) then 
             lux = aniv*683.002
             if (lux.le.0.0007) ityp = 5
             if ((lux.gt.0.0007).and.(lux.le.0.002)) ityp = 4
             if ((lux.gt.0.002).and.(lux.le.0.01)) ityp = 3
             if ((lux.gt.0.01).and.(lux.le.0.04)) ityp = 2
             if ((lux.gt.0.04).and.(lux.le.2.)) ityp = 1
             write(*,*)
             write(*,*) ' Flux lumineux (W/m2)',aniv
             write(16,*)
             write(16,*) ' Flux lumineux (W/m2)',aniv
             if (lux.le.2) then
                write(*,*)
                write(*,*) ' Eclairement en lux : ',lux,
     &                     ' Nuit de type :',ityp
                write(16,*)
                write(16,*) ' Eclairement en lux : ',lux,
     &                      ' Nuit de type :',ityp
             else
                write(*,*)
                write(*,*) ' Eclairement en lux : ',lux
                write(16,*)
                write(16,*) ' Eclairement en lux : ',lux
             endif
           endif
         
           DEALLOCATE(lambda)
           DEALLOCATE(flux_lam)
         endif
      endif
      if (ilum.eq.0) then
         DEALLOCATE(sigext)
         DEALLOCATE(sigabs)
         DEALLOCATE(phaze)    
         stop    
      endif
      if (init.lt.2) then
         init = init + 1
         if (init.eq.2) DEALLOCATE(ajj)
         goto 1
      endif
      
      DEALLOCATE(sigext)
      DEALLOCATE(sigabs)
      DEALLOCATE(phaze)
      
            
      ! Convolution par une gaussienne
      ! R�solution = (Delta lambda)/lambda

      if (iconvol.eq.1) then
         ALLOCATE(convol(nsigmx),STAT=ok)
         IF (ok /= 0) THEN
           print*,'#ERR : probleme allocation convol dans Ray_j_n'
           call exit(1)
         ENDIF
         nsig = iv2 - iv1 + 1
         call convolution(nsigmx,sgma,lum_sig,nsig,fwhm,convol,kdep)
         sumwat = 0.
         sumph = 0.
         sumrayl = 0.
         istep = ipas
         do k=kdep,nsig-kdep-1
            if (istep.eq.ipas) then 
               Eph = hp*cl*sgma(k)
               write(17,'(3x,f8.1,3(20x,1e10.3))') sgma(k),
     &         convol(k),convol(k)/Eph,convol(k)/Eph*4*pi/1e10               

               istep = 0
            endif
            istep = istep + 1
         enddo
         close (17)
         if (iecla.eq.1) then
            nsig = iv2 - iv1 + 1
            call convolution(nsigmx,sgma,flux,nsig,fwhm,convol,kdep)
            istep = ipas
            do k=kdep,nsig-kdep-1
               if (istep.eq.ipas) then 
                  Eph = hp*cl*sgma(k)
                  write(18,'(3x,f8.1,2(20x,1e10.3))') sgma(k),convol(k),
     &                                                convol(k)/Eph
                  istep = 0
               endif
               istep = istep + 1
            enddo
            close (18)
         endif
         nsig = iv2 - iv1 + 1
         call convolution(nsigmx,sgma,tran_tot,nsig,fwhm,convol,kdep)
         istep = ipas
         do k=kdep,nsig-kdep-1
            if (istep.eq.ipas) then 
               write(20,'(3x,f8.1,2(20x,1e10.3))') sgma(k),convol(k)
               istep = 0
            endif
            istep = istep + 1
         enddo
         close (20)
         
         DEALLOCATE(sgma)
      else if (iconvol.eq.2) then
         ! transformation de la luminance en longueur d'onde avec un pas constant
         pas = 1e4/float(iv2*iv2)
         delam = 1e4/iv1 - 1e4/iv2
         nlamx = delam/pas + 100
         !! calcul du nombre de points dans le pas d'�criture des r�sultats
         ipas = int(per/pas)
         ALLOCATE(lum_lam(nlamx),STAT=ok)
         IF (ok /= 0) THEN
           print*,'#ERR : probleme allocation lum_sig dans Ray_j_n'
           call exit(1)
         ENDIF
         ALLOCATE(lambda(nlamx),STAT=ok)
         IF (ok /= 0) THEN
           print*,'#ERR : probleme allocation lambda dans Ray_j_n'
           call exit(1)
         ENDIF
         ALLOCATE(convol(nlamx),STAT=ok)
         IF (ok /= 0) THEN
           print*,'#ERR : probleme allocation convol dans Ray_j_n'
           call exit(1)
         ENDIF
         write(*,*)
         write(*,*) 'Interpolation en longueur d''onde de la luminance'
         call interp_lambda(nlamx,iv1,iv2,lambda,lum_sig,lum_lam,
     &      nlamb)
         write(*,*)
         write(*,*) 'Convolution de la luminance'
         call convolution(nlamx,lambda,lum_lam,nlamb,fwhm,convol,
     &                    kdep)
         istep = ipas
         do k=kdep,nlamb-kdep-1
            if (istep.eq.ipas) then
               Eph = hp*cl*1e4/lambda(k)
               write(17,'(3x,f10.8,3(20x,1e10.3))') 
     &         lambda(k),convol(k),convol(k)/Eph,convol(k)/Eph*4*pi/1e10
               istep = 0
            endif
            istep = istep + 1
         enddo
         close(17)
         DEALLOCATE(lum_lam)
         if (iecla.eq.1) then
            ALLOCATE(flux_lam(nlamx),STAT=ok)
            IF (ok /= 0) THEN
              print*,'#ERR : probleme allocation flux_sig dans Ray_j_n'
              call exit(1)
            ENDIF
            write(*,*)
            write(*,*) 'Interpolation en longueur d''onde de',
     &                 ' l''eclairement au sol'
            call interp_lambda(nlamx,iv1,iv2,lambda,flux,flux_lam,
     &         nlamb)
            write(*,*)
            write(*,*) 'Convolution de l''eclairement au sol'
            call convolution(nlamx,lambda,flux_lam,nlamb,fwhm,convol,
     &                       kdep)
            istep = ipas
            do k=kdep,nlamb-kdep-1
               if (istep.eq.ipas) then
                  Eph = hp*cl*1e4/lambda(k)
                  write(18,'(3x,f10.8,2(20x,1e10.3))') lambda(k),
     &                    convol(k),convol(k)/Eph
                  istep = 0
               endif
               istep = istep + 1
            enddo
            close(18)
            DEALLOCATE(flux_lam)
         endif
         ALLOCATE(tran_lam(nlamx),STAT=ok)
         IF (ok /= 0) THEN
           print*,'#ERR : probleme allocation trans_lam dans Ray_j_n'
           call exit(1)
         ENDIF
         write(*,*)
         write(*,*) 'Interpolation en longueur d''onde de',
     &              ' la transmission'
         call interp_lambda(nlamx,iv1,iv2,lambda,tran_tot,tran_lam,
     &      nlamb)
         do i=1,nlamb
            tran_lam(i) = tran_lam(i)*lambda(i)**2/1e4
         enddo
         write(*,*)
         write(*,*) 'Convolution de la transmission'
         call convolution(nlamx,lambda,tran_lam,nlamb,fwhm,convol,
     &                    kdep)
         istep = ipas
         do k=kdep,nlamb-kdep-1
            if (istep.eq.ipas) then
               write(20,'(3x,f10.8,2(20x,1e10.3))') lambda(k),
     &                                              convol(k)
               istep = 0
            endif
            istep = istep + 1
         enddo
         close (20)
         DEALLOCATE(tran_lam)
         DEALLOCATE(lambda)
      endif
     
      DEALLOCATE(lum_sig)
      write(*,*) 'Fin du Programme'
      call DATE_AND_TIME(datej, time, zone, values)
      itz = int(values(4)/60)
      write(*,'(a20,i2,a1,i2,a2,i2,a6,i2)') 
     &' Fin du Programme a ',values(5),'h',values(6),'mn',values(7),
     &'s UTC+',itz
  !    pause
      end
c------------------------------------------------------------------------------