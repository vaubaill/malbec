c    Programme MATISSE version recuperee le 29/09/98 avant l'implementation de la diffusion

      subroutine donnees_parametres(ntemp,ip,ibinx,iprm,iblk,iblock,
     &    irecnm,imolx,ialfx,ipx,ibndwd,iv,ifwhm,mxfreq,ncouche,
     &  tband,sdz,odz,tmoy,wt,sdzx,odzx,iparam,ibin,imol,ialf,
     &  jj,ff,itb,itbx)
       
      include 'parameter.rayjn'
       
      real tband(ntmx),sdz(ntmx,nipmx),odz(ntmx,nipmx)
      real tmoy(laythr),wt(laythr)
      real sdzx(5),odzx(5),ff(laythr)
      integer ifreq(nblkmx),iparam(nblkmx)
      integer ibin(nipmx),imol(nipmx),ialf(nipmx),jj(laythr)
       
      character*18  path
       
       
      data tzero/273.15/
      
      path = '/loc/modtran/DATA/'
c     fichier DIRAC : ITB    / fichier UFTAPX.ASC : ITBX 
       
C     ouverture du fichier binaire n0 9 contenant les parametres du modele de bande
      open(itb,file='DATA/DIRAC',status='old',form='unformatted',
     &                                  access='direct',recl=13000)
       
C     ouverture du fichier n0 10 contenant les parametres du modele de bande
c     pour les CFC
      open(itbx,file='DATA/UFTAPX.ASC',status='old',form='formatted')
      rewind (itbx)
       
C     Lecture entete du fichier DIRAC
c         -> IBNDWD = largeur spectrale des parametres (cm-1)
c         -> IBLOCK = nombre de blocks dans le fichier
c         -> IFREQ(IBLK) = frequence du dernier jeu de parametres dans le block IBLK
c         -> IPARAM(IBLK) = nombre de frequences disponibles dans le block IBLK
c         -> NTEMP = nombre de temperatures auxquelles sont calcules les parametres
c         -> TBAND = valeurs des temperatures auxquelles sont calcules les parametres
       
      read(itb,rec=1) ibndwd,iblock,(ifreq(iblk),iparam(iblk),iblk=1,
     &                iblock),ntemp,(tband(it),it=1,ntemp)
       
c     calcul du nombre d'onde initial multiple de 5 (pour s'adapter a LOWTRAN), compte
c     tenu de la largeur de la convolution
       
c     MXFREQ = frequence max a laquelle les parametres modele de bande sont donnes
      mxfreq = ifreq(iblock)
       
c     test sur la longueur d'onde initiale
      if (iv.gt.mxfreq) then
          write(*,*)'Nombre d''onde en dehors des donnees du modele de',
     &               '  bande'
          return
      endif
       
c     recherche du premier block contenant le nb d'onde initial iv
      iblk=0
   20 iblk = iblk + 1
      if (iv.gt.ifreq(iblk)) goto 20
       
      iprm = iparam(iblk)
      irecnm = iblk+1
C     Lecture de tous les parametres du premier block selectionne dans le fichier DIRAC
c         -> IBIN(IP) = nb d'onde correspondant a la ligne IP
c         -> IMOL(IP) = indice de la molecule correspondant a la ligne IP (au nb d'onde IBIN(IP))
c         -> SDZ(IP,IT) = S/d  (cm-1.amagat-1) pour les NTEMP temperatures stockees dans TBAND
c                        (pour la molecule IMOL(IP) et au nb d'onde IBIN(IP))
c         -> IALF(IP) = largeur collisionnelle * 1.E4 a STP
c                        (pour la molecule IMOL(IP) et au nb d'onde IBIN(IP))
c         -> ODZ(IP,IT) = 1/d  (cm) pour les NTEMP temperatures stockees dans TBAND
c                        (pour la molecule IMOL(IP) et au nb d'onde IBIN(IP))
      read(itb,rec=irecnm)
     x (ibin(ip),imol(ip),(sdz(it,ip),it=1,ntemp),ialf(ip),
     1  (odz(it,ip),it=1,ntemp),ip=1,iprm)
       
c     recherche de la ligne ip correspondant au nb d'onde iv
      ip=0
   30 ip=ip+1
      if (iv.gt.ibin(ip)) goto 30
       
c     lecture parametres modele de bande pour les CFC dans le fichier UFTAPX.ASC                     
c         -> IBINX = nb d'onde correspondant a la ligne IPX
c         -> IMOLX = indice de la molecule correspondant au nb d'onde IBINX
c         -> SDZX(IT) = S/d  (cm-1.amagat-1) pour les NTEMP temperatures stockees dans TBAND
c                        (pour la molecule IMOLX et au nb d'onde IBINX)
c         -> IALFX = largeur collisionnelle a STP
c                        (pour la molecule IMOLX et au nb d'onde IBINX)
c         -> ODZX(IT) = 1/d  (cm) pour les NTEMP temperatures stockees dans TBAND
c                        (pour la molecule IMOLX et au nb d'onde IBINX)
      ipx = 0
 35   ipx = 1+ipx
      ibinx =0
      imolx=0
      ialfx=0
      do kinit=1,5
         sdzx(kinit)=0
         odzx(kinit)=0
      enddo
      if(iv.gt.1789) go to 36
      read(itbx,*,err=36,end=36) ibinx,imolx,(sdzx(izx),izx=1,5),
     &                             ialfx,(odzx(izx),izx=1,5)
925   format(i5,i6,5(1PE9.2),i6,5(1PE9.2))
      if (iv .gt. ibinx) go to 35

 36   continue
      
c     calcul des coefficients d'interpolation en temperature pour chaque couche IK
c         -> JJ(IK) = indice correspondant au n0 de la temperature du tableau TBAND 
c                     superieure a la temperature de la couche
c         -> FF(IK) = coefficient d'interpolation pour le calcul de la temperature de
c                     la couche IK comprise entre la temperature TBAND(JJ-1) et TBAND(JJ)
c         -> tmoy(ik) = temperature moyenne de la couche IK = donnees d'entree du sous programme
      do ik=1,ncouche
         tt=tmoy(ik)
         if(tt.le.tband(1))then
            jj(ik)=2
            ff(ik)=1.
         elseif(tt.ge.tband(ntemp))then
                jj(ik)=ntemp
                ff(ik)=0.
         else
            do j=2,ntemp
               if(tt.le.tband(j))goto50
            enddo
   50       jj(ik)=j
            ff(ik)=(tband(j)-tt)/(tband(j)-tband(j-1))
         endif
       
c     pour chaque couche IK, calcul de SQRT(T/T0) et stockage dans WT(IK) 
c        pour le calcul des largeurs
         wt(ik)=sqrt(tt/tzero)
      enddo
      end
       
c----------------------------------------------------------------------------------
      subroutine load(ip,ntemp,imol,sd,od,sdz,odz,alf0,ialf)
       
      include 'parameter.rayjn'
      real sd(ntmx,mmolt2),od(ntmx,mmolt),alf0(mmolt)
      real sdz(ntmx,nipmx),odz(ntmx,nipmx)
      integer ialf(nipmx),imol(nipmx)
       
       
c     -> IM = IMOL(IP) = indice de la molecule correspondant a la ligne IP (au nb d'onde IBIN(IP))
      im=imol(ip)
      if(im.le.nspc)then
c         chargement des tableaux sd (S/d), od (1/d) et alf0 ( gamma Lorentz) au centre de raie
          imtail=im+nspc
          do it=1,ntemp
              sd(it,im)=sdz(it,ip)
              od(it,im)=odz(it,ip)
              sd(it,imtail)=0.
          enddo
          alf0(im)=1.e-04*ialf(ip)
      else
c         chargement du tableau sd (C) pour les ailes de raie  
          jm=ialf(ip)
          if(jm.eq.0)then
       
c            ONE TAIL
             do it=1,ntemp
                sd(it,im)=sdz(it,ip)
             enddo    
          else
       
c            TWO TAILS
             do it=1,ntemp
                sd(it,jm)=odz(it,ip)
                sd(it,im)=sdz(it,ip)
             enddo     
          endif
      endif
      return
      end
c---------------------------------------------------------------------------
       
      subroutine load_cfc(imolx,ntemp,ialfx,sd,od,alf0,sdzx,odzx)
       
      include 'parameter.rayjn'
      real sd(ntmx,mmolt2),od(ntmx,mmolt),alf0(mmolt),sdzx(5),odzx(5)
       
      imx = imolx
      nsped=nspc+nspc
      if(imx.le.nspecx)then
c        chargement des tableaux sdz, od et alf0 au centre de raie
         imtail=imx+nspecx
         do it=1,ntemp
            sd(it,imx+nsped)=sdzx(it)
            od(it,imx+nspc)=odzx(it)
            sd(it,imtail+nsped)=0.
         enddo     
         alf0(imx+nspc)=1.e-04*ialfx
      else
c        chargement du tableau sd (C) pour les ailes de raie  
         jmx=ialfx
         if(jmx.eq.0)then
C      
C           ONE TAIL
            do it=1,ntemp
               sd(it,imx+nsped)=sdzx(it)
            enddo     
         else
C      
C           TWO TAILS
            do it=1,ntemp
               sd(it,jmx+nsped)=odzx(it)
               sd(it,imx+nsped)=sdzx(it)
            enddo         
         endif
      endif
      return
      end
c--------------------------------------------------------------
      subroutine data_interne(dv,gauss_cdr,itb,itbx)
       
      include 'parameter.rayjn'
      real dopfac(mmolt),gauss_cdr(mmolt)
       
c     dopfac (sans unite) = SQRT(2 LN2 R T / M)/C
c     avec T = 273.15K et M le poids moleculaire de l'espece indicee i.
      data (dopfac(i),i=1,nspect) /1.3945e-6, 0.8922e-6, 0.8543e-6,
     & 0.8921e-6, 1.1183e-6,1.4777e-6, 1.0463e-6, 1.0805e-6, 0.7395e-6,
     & 0.8726e-6, 1.4342e-6,0.7456e-6,5.04981e-07, 5.38245e-07,
     & 5.79088e-07, 6.30907e-07,6.36486e-07, 4.32375e-07, 4.52709e-07,
     & 4.76212e-07,5.99528e-07,6.65841e-07,5.83391e-07,4.7721e-07,
     & 5.69488e-07/
       
      dv = 1.
       
c     fichier DIRAC : ITB = 9   / fichier UFTAPX.asc : ITBX = 31
      itb = 9
      itbx= 10
       
      do i=1,12
         gauss_cdr(i) = dopfac(i)
      enddo
      end
       
c------------------------------------------------------------------      
      subroutine bmtran(xstar,acbar,adbar,odbar,dv,ttrans,acbar2)
 
c                                                                       <n>
c     Calcul de la transmission par l'expression transm = (1. -<Wsl>/dv)
c     avec Wsl la largeur equivallente d'une raie isolee calculee par l'expression (6) annexe (2)
c     Wsl = W0 - W1 et <n> = dv*<1/d>
c
c     Donnees d'entrees : 
c                     -> XSTAR   = [Su/d] pour le centre de raie (equation (33) annexe 3)
c                                  (rapport RTS 3/4635 PY Janvier 96)
c                     -> ODBAR   = <1/d> (equation (34) annexe 3)
c                     -> ACBAR   = <gammal/d> (equation (35) annexe 3)
c                     -> ADBAR   = <gammad/d> (equation (36) annexe 3)
c                     -> DV      = largeur de l'intervalle spectral
c                     -> ACBAR2  =  SUM   u*(S/d)*((1/d)**2)*((P/P0)**2)*(T0/T)
c                                  couches

      data p5ln2,c/0.346573590,1.128379167/
       
      xs=xstar/odbar
       
c     Calcul du denominateur la largeur equivallente moyenne collisionnelle
c     expression (10) annexe 2 : Wl = 2 [Su/d]/denom
c     -> pour toutes les molecules sauf O3 : denom = 4 + ( [Su/d]/<gammal/d> )
c     -> pour O3 : denom = 4. + store avec store calcule dans le test suivant
      store=xstar/acbar
      ratio=1.
      if(acbar2.ne.0.)then
          rho=acbar2/acbar**2
          denom=(rho+1)*(rho**2+rho+1)
          f1=rho*(rho-1)*(rho**2+1)/denom
          denom=denom*(rho+1)**2
          f2=4*rho**2*(rho**2+1)/denom
          f3=0.1886+(rho-1)*.7200
          str2pi=.5*store/3.1416
          strnew=store-3.1416*str2pi**2*f1/(1+str2pi*f3+.5*str2pi**2*f2)
          ratio=strnew/store
          store=strnew
      endif
      denom=4.+store
       
c     Calcul de la largeur equivallente W0 expression (7) annexe 2 
c     en utilisant l'approximation de Rogers et Williams ((9) annexe 2)
c        W0 = SQRT( Wl*Wl + Wd*Wd - (Wl*Wd/Su)**2)
c     -> Wl = 2 [Su/d]/denom : denom calcule precedemment
c     -> Wd = SQRT(2/pi)*<gammad/d>*SQRT(ln(1+ln2/2([Su/d]/<gammad/d>)**2)
c             expression (11) annexe 2 
c        Suivant la valeur de [Su/d]/<gammad/d>, Wd est developpe en serie.
      wd=xstar/adbar
      wd=p5ln2*wd*wd
      if(wd.lt..0001)then
          wsl=xs*(1.-.25*wd*(store+(1-ratio)*(1+ratio))/denom)
      elseif(wd.lt..01)then
          store=wd*(.25-wd*(.16666667-.125*wd))*
     1      (store+(1-ratio)*(1+ratio))/denom
          wsl=xs*(1.-store*(1.+.5*store*(1.+store)))
      else
          wd=log(1.+wd)/wd
          wl=4./denom*ratio**2
          wsl=xs*sqrt(wd+wl-wd*wl)
      endif
      wsl0=wsl
       
c     calcul du second terme de l'expression (6) annexe 2 : W1 
c     W1 calcule par l'expression (15) de l'annexe 2.
      xs=xs*ratio
      u0=c*sqrt(xs*acbar/odbar)/dv
      wsl=wsl-dv*(.25*bmerfu(2*u0)+.75*bmerfu(u0/1.5))
       
c     calcul de la transmission   
      if(wsl.ge.dv)then
          ttrans=0.
      else
          ttrans=(1.-wsl/dv)**(dv*odbar)
      endif
      return
      end
c-----------------------------------------------------------
      function bmerfu(y)
C      
C ... APPROXIMATION FOR EXP(-Y*Y) + SQRT(PI) * Y * ERF(Y) - 1
      data p,a1,a2,a3,a4,a5,rtpi/.3275911,.451673692,-.504257335,
     1  2.519390259,-2.575644906,1.881292140,1.772453851/
      if(y.ge.2.33)then
          bmerfu=rtpi*y-1.
          if(y.lt.3.2)then
              t=y*y
	      bmerfu=bmerfu+(1.75+t*.5)/(3.75+t*(5.+t))*exp(-t)
	  endif
	  return
      endif
      t=1./(1.+p*y)
      bmerfu=(1.-y*t*(a1+t*(a2+t*(a3+t*(a4+t*a5)))))*exp(-y*y)+rtpi*y-1.
      return
      end
c------------------------------------------------------------------------
      subroutine luminance(nseg,ncouche,tran_seg,indice,hobs,hfin,
     & omg,atmp,alt,alum,iv,ivc,aj,tran_sol,solar_spec,ang_dif,nang,
     & ang,phsf,phsfn,iang_dif,sigdia,sigdin,rayl,thk,glow,tetaobs,
     & ep_gl,alb_sol,Tsol,teta0,flux,ds,ds0,ray_t,isol,lg,bg,lat_pion,
     & lon_pion,val_pion,freq_pion,spect_pion,ilimb,htan)
      
c     Programme de calcul de la luminance
c     Fonction source calcul�e � partir de la m�thode aux ordonn�es discr�tes
c                 
      include 'parameter.rayjn'

      REAL(KIND=8), DIMENSION(nsigmx,kntau) :: aj
      REAL(KIND=8), DIMENSION(laythr) :: ep_gl,glow
      REAL(KIND=8), DIMENSION(nsigmx) :: flux
      real *8 tran_seg(laythr),lg,bg,amu,amu0
      real atmp(2,knln),OMG(knln,nsigmx),alt(knln),htan
      real ds0(laythr),ds(laythr)
      real solar_spec(nsigmx),tran_sol(knln,nsigmx),rayl(laythr)
      real ang(knang),phsf(knang,laythr),sigdia(laythr),sigdin(laythr)
      real tsol,Jdm,Ji,Jf,Jmoy,dump(kntau),phsfn(knang,laythr)
      real lumth,lumdm,lumds,hobs,hfin,htop,hmin,hmax,THK(knln,nsigmx)
      real lumglo,lumsol,lumgal,lumsolr,lumsole,lumds2,lumds1
      integer indice(laythr)
      
      real  lat_pion(50),lon_pion(50),val_pion(50,50)
      real  freq_pion(2000),spect_pion(2000,50),fnd_gal
            
      
      pi = acos(-1.)  
      RAD=PI/180.0    
      sigma = float(iv)
      
   !  On r�ordonne les fonctions source de diffusion multiple
      nalt = ncouche + 1
      do i=1,nalt
         dump(i) = aj(ivc,i)
      enddo                                             
      do i=1,nalt
         aj(ivc,i) = dump(nalt+1-i)
      enddo      
      
c      hmin = hobs
c      hmax = hfin
c      if (hobs.gt.hfin) then
c         hmin = hfin
c         hmax = hobs
c      endif
      htop = alt(nalt)
      if (hobs.gt.htop) hobs = htop
      if (hfin.gt.htop) hfin = htop
      
      ! pour la vis�e au limbe on recherche l'indice de la couche contenant htan
      ! if ((ilimb.eq.1).and.(ivc.eq.1)) then
      if (ilimb.eq.1) then
         idiff = 1
         ialt0=1000
         l = nseg+1
         do while (idiff.gt.0) 
            l =  l - 1
            ialt = indice(l) 
            idiff = ialt0 - ialt
            ialt0 = ialt
            itan = ialt
         enddo             
      endif
      itan1 = 0
      
   !  Rappel des donn�es optiques le long du trajet
      if (ivc.eq.1) then 
         write(16,*)
         write(16,*) ' ***********************************'
         write(16,*)
         write(16,*) 'Rappel des donn�es optiques le long du trajet � ',
     &   iv,' cm-1'
         write(16,*)
         write(16,*) 'INFO : le segment n�1 est au niveau de l',
     &   'observateur'
         write(16,*) 'INFO : trans : transmission observateur - ',
     &   'segment n� i'
         write(16,*) 'INFO : Planck et fct source moyens sur la couche'
         write(16,*) 'INFO : F0 eclairt solaire au sommet de la couche'         
         write(16,'(7(a8,2x))') 'n�','alt','tau','omega','trans',
     &   'ang_dif','Fct_phase'
      endif              
      alum = 0.   
      lumth = 0.
      lumdm = 0.
      lumds = 0.
      lumds1 = 0.
      lumglo = 0.
      lumsol = 0.
      do l=nseg,1,-1     
         ialt = indice(l) ! indice de la couche atmosph�rique
         fct_phas = phas_interp(ang_dif,nang,ang,phsf,l,iang_dif) 
         fct_phas_n = phas_interp(ang_dif,nang,ang,phsfn,l,iang_dif)  
          ! Fonction de phase Rayleigh                  
         cos_angdif = cos(ang_dif*rad)
         phrl = 3./4.*(1.+cos_angdif**2)/(4.*PI)
          ! Fonction de phase totale
         fct_phas = (fct_phas*sigdia(l) + fct_phas_n*sigdin(l)+ 
     &             phrl*rayl(l))/(sigdia(l) + sigdin(l) + rayl(l))
         omega = omg(ialt,ivc)
         ialt_sol = nalt - (ialt + 1)
         if (ialt_sol.eq.0) then
             f0 = solar_spec(ivc)    ! f0 = �clairement solaire au niveau sup�rieur de la couche
         else
             f0 = solar_spec(ivc)*tran_sol(ialt_sol,ivc)
                write(52,*) l ,tran_sol(ialt_sol,ivc)
         endif
com      L'observateur est au niveau du premier segment:
c           tran_seg(1) = transmission entre l'observateur et la premi�re intercouche
c           tran_seg(i) = transmission entre l'observateur et la fin du segment i
c           tran_seg(nseg) = transmission entre l'observateur et la fin du trajet


c        Calcul de la luminance thermique de l'atmosph�re et diffusion multiple
c        Hypoth�ses :
c                    - la fonction de Planck varie lin�airement avec 
c                      l'�paisseur optique dans la couche (approximation lin�aire en tau)
c                    - M�me hypoth�se pour la diffusion multiple
c                    - l'alb�do de diffusion simple est constant sur la couche
c
c   ! Calcul fonction de Planck
         temp1 = atmp(1,ialt)
         temp2 = atmp(2,ialt)
         if (l.eq.1) then
            temp1 = atmp(1,ialt) +(temp2-atmp(1,ialt))*
c     &          (hmin-alt(ialt))/(alt(ialt+1) - alt(ialt))
     &          (hobs-alt(ialt))/(alt(ialt+1) - alt(ialt))
         endif
         if (l.eq.nseg) then
            temp2 = atmp(1,ialt) +(temp2-atmp(1,ialt))*
c     &          (hmax-alt(ialt))/(alt(ialt+1) - alt(ialt))
     &          (hfin-alt(ialt))/(alt(ialt+1) - alt(ialt))        
         endif
         
         if (tetaobs.le.90.) then
            Bi = Planck(sigma,temp1)        
            Bf = Planck(sigma,temp2)
            ! Diffusion multiple
            Ji = aj(ivc,ialt)
            Jf = aj(ivc,ialt+1)
         else
            Bi = Planck(sigma,temp2)        
            Bf = Planck(sigma,temp1)
            ! Diffusion multiple
            Ji = aj(ivc,ialt+1)
            Jf = aj(ivc,ialt)
         endif 
c         Bmoy = (Bi+Bf)/2.
         tmoy = (temp1+temp2)/2.
         Bmoy = Planck(sigma,tmoy)
         Jmoy = (Ji+Jf)/2.
         
c     ! Transmissions le long du trajet
         if (l.eq.1) then
            trans = 1.
         else
            trans = tran_seg(l-1)
         endif
c    !    write(52,*) nseg+1-l ,tran_seg(nseg+1-l)
         
c     ! Epaisseur optique de chaque segment : on utilise l'�paisseur de la couche pond�r�e par 
c     ! la longueur du segment
         tau = thk(ialt,ivc)/(alt(ialt+1) - alt(ialt))*ds(l)     
             
        
c      ! Approximation de Pade
            Beff = (Bmoy + 0.2*tau*Bi)/(1. + 0.2*tau)
            Jdm  = (Jmoy + 0.2*tau*Ji)/(1. + 0.2*tau)
         

            if (tau.lt.1.e-10) then
               lumth = lumth + trans*tau*(1. - omega)*Beff
               lumdm = lumdm + trans*tau*Jdm
c               lumth = lumth + trans*tau*Bmoy
            else
               lumth = lumth + trans*(1.-exp(-tau))*(1. - omega)*Beff 
               lumdm = lumdm + trans*(1.-exp(-tau))*Jdm
            endif  

c            test = trans*(1.-exp(-tau))                 
  
c    !! Calcul luminance diffusion simple
c      !! epaisseur optique le long du trajet solaire
c         utau = thk(ialt,ivc)
c         if (tran_sol(ialt_sol+1,ivc).gt.0.) then
c             if (ialt_sol.eq.0) then
c               tau0 = -log(tran_sol(ialt_sol+1,ivc))
c            else
c               rap = tran_sol(ialt_sol+1,ivc)/tran_sol(ialt_sol,ivc)
c               if (rap.le.1) then
c                  tau0 = -log(rap)
c               else
c                  tau0 = 0
c               endif
c            endif
c         endif        
c         amu  = abs(cos(rad*tetaobs))
c         amu0 = abs(cos(rad*teta0))
c         if (utau.lt.1e-8)then
c            lumds = lumds +   
c     &      trans*omega*F0*fct_phas*utau/amu
c         elseif (tetaobs.gt.90.) then  
c            arg = 1./amu + 1./amu0
c            lumds  =  trans*omega*F0*fct_phas*amu0*
c     &      (1. -exp(-utau*arg))/(amu + amu0)    
c         else
c            if (tetaobs.eq.teta0) then
c               lumds  = lumds  + trans*omega*F0*fct_phas*utau*
c     &         (exp(-utau/amu))/amu
c            else
  !         arg = 1. + (tau0/tau)
  !          lumds = lumds + 
  !   &           trans*omega*F0*fct_phas*(1. - exp(-tau*arg))/arg      
c                arg = amu - amu0
c               lumds  =  lumds + trans*omega*F0*fct_phas*amu0*
c     &      (exp(-utau/amu) - exp(-utau/amu0))/arg  
   !  &    trans*omega*F0*fct_phas*
   !  &     (exp(tau/cos(tetaobs)) - exp(tau/cos(teta0)))/arg2    
c          else
c               arg = 1. + (tau0/tau)
c               lumds = lumds + 
c     &              trans*omega*F0*fct_phas*tau*exp(-tau)
cc     &              trans*omega*F0*fct_phas*(1. - exp(-tau*arg))/arg
c               arg = 1. + (tau0/tau)
c               lumds = lumds + 
c     &              trans*omega*F0*fct_phas*tau*exp(-tau)
c            endif
c         endif
         ind0 = ncouche + 1 - ialt
         tau0 = thk(ialt,ivc)/(alt(ialt+1) - alt(ialt))*ds0(ind0)
         if (ilimb.eq.0) then
            if (tau.lt.1e-8)then
               lumds = lumds + trans*omega*F0*fct_phas*tau
            elseif (tetaobs.gt.90.) then    !!! vis�e vers le bas !!!!  
               arg = 1 + tau0/tau
               lumds  =  lumds + trans*omega*F0*fct_phas*
     &         (1. -exp(-tau-tau0))/arg    
            else
               if (tetaobs.eq.teta0) then   !!! vis�e vers le haut
                  lumds  = lumds  + 
     &            trans*omega*F0*fct_phas*tau*exp(-tau)
               else 
                  arg = tau0/tau - 1
                  lumds  =  lumds + trans*omega*F0*fct_phas*
     &           (exp(-tau) - exp(-tau0))/arg  
                  if (arg.eq.0) lumds = 0.
               endif
            endif
         else   
      !!    Cas de la vis�e au limbe
      !     On commence par une vis�e vers le haut jusqu'� l'altitude tangente en descendant � reculons
            if (itan.lt.ialt) then
               if (tetaobs.eq.teta0) then   !!! vis�e vers le haut
                  lumds  = lumds  + 
     &                     trans*omega*F0*fct_phas*tau*exp(-tau)
               else 
                  arg = tau0/tau - 1
                  lumds  =  lumds + trans*omega*F0*fct_phas*
     &            (exp(-tau) - exp(-tau0))/arg
               endif
            elseif (itan.eq.ialt) then
               ! on calcule l'�paisseur optique qui est inf�rieure � celle de la couche
                  tau = thk(ialt,ivc)/(alt(ialt+1) - htan)*ds(l)
                  tau0 = thk(ialt,ivc)/(alt(ialt+1) - htan)*ds0(ind0)
                  if (itan1.eq.0) then
                     if (tetaobs.eq.teta0) then   !!! vis�e vers le haut
                        lumds  = lumds  + 
     &                  trans*omega*F0*fct_phas*tau*exp(-tau)
                     else 
                        arg = tau0/tau - 1
                        lumds  =  lumds + trans*omega*F0*fct_phas*
     &                  (exp(-tau) - exp(-tau0))/arg
                     endif
                     itan1=1
                  else
                     if (tau.lt.1e-8)then
                        lumds = lumds + trans*omega*F0*fct_phas*tau
                     elseif (tetaobs.gt.90.) then    !!! vis�e vers le bas !!!!  
                        arg = 1 + tau0/tau
                        lumds  =  lumds + trans*omega*F0*fct_phas*
     &                  (1. -exp(-tau-tau0))/arg  
                     endif
                     itan = 1000
                  endif
            else
               itan = 1000
            !! on continue par une vis�e vers le bas
               if (tau.lt.1e-8)then
                  lumds = lumds + trans*omega*F0*fct_phas*tau
               elseif (tetaobs.gt.90.) then    !!! vis�e vers le bas !!!!  
                  arg = 1 + tau0/tau
                  lumds  =  lumds + trans*omega*F0*fct_phas*
     &            (1. -exp(-tau-tau0))/arg  
               endif
            endif
         endif
               
c         write(*,*) 'utau,tau,utau0,tau0,lumds,lumds1',
c     &utau/amu,tau,utau/amu0,tau0,lumds,lumds1
         
         if (ivc.eq.1) write(16,'(i3,3x,f8.3,3x,1e10.3,4(f8.3,3x))')
c     &                                  3(1e10.3,3x),2(f8.4,3x))')
     &   l,alt(ialt),tau,omega,trans,ang_dif,fct_phas
c     &   Bmoy,Jmoy,F0,ang_dif,fct_phas 
 
c        ! luminance induite par le nightglow
c        ! **********************************
c
c         hco = alt(ialt)
c         Rt = ray_t  ! rayon terrestre
c         teta = tetaobs*pi/180.
c         VR = 1./(sqrt(1.-sin(teta)*sin(teta)*(Rt*Rt/(Rt+hco)**2))) ! Van Rhijn
c         epa = ep_gl(ialt)*1000.   
c         lumglo =  lumglo + trans*glow(ialt)*1.e6*epa*VR/4./pi 

         epa = ds(l)*1000   ! �paisseur du segment dans la couche ialt
        ! on divise par 4*PI pour conversion en steradian
        ! transformation cm3 -> m3 on multiplie par 1e6 
         lumglo =  lumglo + trans*glow(ialt)*1.e6*epa/4./pi     
                     
      enddo    ! fin de boucle sur les segments
      
      ! luminance induite par le fond stellaire
        lumgal = 0.
        if (isol.eq.0) then
            call fond_galac(lg,bg,iv,lat_pion,lon_pion,val_pion,
     &                      freq_pion,spect_pion,fnd_gal)
            lumgal = tran_seg(nseg)*fnd_gal
        endif
               
c     ! luminance induite par le sol
c     ! ****************************         
      lumsol = 0.
      lumsolr = 0.
      ! ( flux(ivc) = flux total (direct + diffus) au sol)         
      if (isol.eq.1) then
         lumsolr = alb_sol/pi*flux(ivc)
         lumsole = (1-alb_sol)*Planck(sigma,Tsol)
         lumsol = lumsolr + lumsole
      endif
!
c    ! Luminance totale
      alum = lumth + lumdm + lumds + lumglo + lumgal + 
     &       lumsol*tran_seg(nseg)
      f0 = solar_spec(ivc)*tran_sol(ncouche,ivc)
      write(15,'(i5,13(14x,1pe10.3))') 
     & iv,tran_seg(nseg),lumth,lumdm,lumds,lumglo,lumgal,
     & lumsol,alum,solar_spec(ivc),f0,lumsolr,lumsole,lumds1
      end
c-------------------------------------------------------------------
      function planck(v,t)
      
c     Fonction de Planck en W/m2.str.cm-1
      
c     c1 = 2 * h * c**2 = 2 * 6.626196e-34 * (2.997925e10)
c     c2 = (h * c)/K = 6.626196e-34 * 2.997925e10 / 1.380622e-23
      c1 =  2. * 6.626196e-34 * (2.997925e10)**2
      c2 =  6.626196e-34 * 2.997925e10 / 1.380622e-23
      planck = c1*v**3/(exp(c2*v/t)-1.0)    
      planck = planck*1.E4  
      return                                                             
      end   
c------------------------------------------------------------------
      
      function emiss(isol,v)
      
      include 'parameter.rayjn'

      real freq(nsol,nsigsol),epsilon(nsol,nsigsol),lamda
      integer nfreq(nsol)
           
      data (nfreq(i),i=1,2)  / 18, 12 /
      
c     neige fraiche      
      data (freq(1,i),i=1,18) / 1.0,1.1,1.2,1.3,1.4,1.46,1.55,1.85,
     &                   1.93,2.08,2.25,2.35,2.45,3.0,3.2,5.,20.,50./
      data (epsilon(1,i),i=1,18) / .2,.1,.35,.44,.52,.95,.95,.76,.98,
     &                           .98,.83,.94,.98,.98,.93,.98,.98,.98/
c     ocean
      data (freq(2,i),i=1,12) /1.,1.5,2.,2.6,2.9,3.4,4.,6.,8.,10.,12.,
     &                         14./
      data (epsilon(2,i),i=1,12) /.97,.97,.98,.99,.95,.98,.98,.98,.99,
     &                          .98,.96,.94/
c     herbe
c      data (freq(3,i),i=1,22) /
c      data (epsilon(3,i),i=1,22) /
     
       lamda = 1.e4/v
       nfm = nfreq(isol)
       if (lamda.le.freq(isol,1)) then
          emiss = epsilon(isol,1)
          return
       endif
       if (lamda.ge.freq(isol,nfm)) then
          emiss = epsilon(isol,nfm)
          return
       endif
       do i=1,nfm-1
          sigma1 = freq(isol,i)
          sigma2 = freq(isol,i+1)
          eps1 = epsilon(isol,i)
          eps2 = epsilon(isol,i+1)
          if ((lamda.ge.sigma1).and.(lamda.le.sigma2)) then
             emiss = eps1 + 
     &              (eps2 - eps1)/(sigma2 - sigma1)*(lamda - sigma1)
             return
          endif
       enddo
       return
       end
c---------------------------------------------------------------------
       subroutine correc_flux_solaire(dist_corr,Jday)

c      calcul de la correction pour le calcul de la densite de flux solaire 
c      pour le jour iday

       real dist_corr,gamma              

       pi = acos(-1.)

c      calcul du rapport (d0/d)**2 avec d0 distance moyenne soleil terre et d la 
c      distance soleil terre au jour Jday

       gamma = 2*pi*float(Jday - 1)/365.
       dist_corr = 1.000110 + 0.034221*cos(gamma) + 0.001280*sin(gamma)
     &     + 0.000719*cos(2*gamma) + 0.000077*sin(2*gamma)
       end

c---------------------------------------------------------------------
       subroutine solar_depot(Jday,solar_spec,iv1,iv2)

c      calcul de l'irradiance solaire au sommet de l'atmosphere
c 
c               --> lecture de la constante solaire (densite de flux en W/m2 recue
c sur la terre a travers un plan perpendiculaire a la direction solaire pour une distance
c moyenne soleil/terre - d0 -) dans le fichier solar_spectra
c
      include 'parameter.rayjn'

      real solar_spec(nsigmx),dist_corr

 
c     calcul de la correction pour le calcul de la densite de flux solaire au jour Jday
      call correc_flux_solaire(dist_corr,Jday)

c     lecture de la constante solaire et insertion dans le tableau solar_spec(i)
c     attention, la constante solaire est donnee en W/cm2/cm-1, il faut la transformer
c     en W/m2/cm-1 => 1E4

      open(2,file='DATA/solar_spectra.txt')
      read(2,*) ivsol1,ivsol2
      ivsol = 0
      do i=ivsol1,ivsol2
         read(2,*) sigma_sol,cst_soleil
         if ((i.ge.iv1).and.(i.le.iv2)) then
            ivsol = ivsol + 1
            solar_spec(ivsol) = cst_soleil*dist_corr*1.E4 
         endif
      enddo
      close (2)
      end
c--------------------------------------------------------------------------
      subroutine depot_lunaire(solar_spec,iv1,iv2,ang_phas,
     &                         Rst,Rtl)
      
c      calcul de l'irradiance lunaire au sommet de l'atmosphere
c       Mod�lisation P. Simoneau

      include 'parameter.rayjn'
      real solar_spec(nsigmx),alb1(5),alb2(5)
      real lambda,lambda1,lambda2,alpha1,alpha2,long_onde
            
      real *8 mag_a(19),mag_b(19),ang(19),pi,conv
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: lam_alb,alb
      integer ios,ok
      
      data Alb1 /-4.944e-2,4.406e-1,-3.150e-1,1.084e-1,0./  
      data Alb2 /-7.317e-1,3.621,-5.656,3.934,-9.999e-1/

      data mag_a /0.,0.30805,0.61802,0.92169,1.23130,1.50230,
     &            1.76490,2.04800,2.36050,2.72480,3.15660,
     &            3.66680,4.27780,4.9,5.555,6.2,6.8,7.4,8.15/
      data mag_b /0.,0.0652190,0.1336200,0.1989200,0.2668300,
     &            0.2961900,0.3078300,0.3204200,0.3320600,
     &            0.3540200,0.3995500,0.4609600,0.5570100,
     &            0.66,0.76,0.86,0.96,1.06,1.16/
      data ang /0.,10.,20.,30.,40.,50.,60.,70.,80.,90.,100.,
     &          110.,120.,130.,140.,150.,160.,170.,180./
      
       pi = dacos(-1.d0)
       conv = pi/180.d0
       
      Rl = 1737.4  ! Rayon lune
      Rt = 6378.14 ! Rayon terre
      Dsl = sqrt(Rst*Rst + Rtl*Rtl + 2*Rst*Rtl*cos(ang_phas*conv))  ! distance soleil - lune
      
      ALLOCATE(lam_alb(200),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation alp dans depot_lunaire'
        call exit(1)
      ENDIF  
      ALLOCATE(alb(200),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation alb dans depot_lunaire'
        call exit(1)
      ENDIF
      
      ! lecture de l'albedo spectral du disque lunaire
      open(2,file = 'DATA/albedo_lune_total.txt')
      ios = 0
      i=0
      do while(ios==0)
         i=i+1
         read(2,*,iostat=ios) lam_alb(i),alb(i)
      enddo
      nval = i - 1
      close(2)
   
      ! Attention, on travaille en micron alors que le spectre solaire est 
      ! en W/m2/cm-1. Ce n'est pas grave car on ne fait que des correction
      ! g�om�triques. Inutile de passer en �m pour repasser en cm-1 � la fin
      ! du calcul.
   
      iiv = 0    
      do iv = iv1,iv2
         iiv = iiv + 1
         sig = float(iv)
         lambda = 1e4/sig
         
         ! calcul de l'albedo spectral pour toutes les longueurs                                                                                d'onde
         alpha = 0.
         if ((lambda.gt.0.3).and.(lambda.lt.0.6)) then
            alpha = alb1(1)
            do i=2,5
               alpha = alpha + alb1(i)*lambda**(i-1)
            enddo
         elseif ((lambda.ge.0.6).and.(lambda.le.1.2)) then
            alpha = alb2(1)
            do i=2,5
               alpha = alpha + alb2(i)*lambda**(i-1)
            enddo
         elseif ((lambda.gt.1.2).and.(lambda.le.lam_alb(1))) then
            lambda1 = 1.2
            alpha1 = 0.193419
            alpha = alb(1) + 
     &      (alpha1 - alb(1))*(lam_alb(1) - lambda)/
     &      (lam_alb(1) - lambda1)
         elseif ((lambda.gt.lam_alb(1)).and.(
     &      lambda.le.lam_alb(nval))) then
            do i=1,nval-1
               lambda1 = lam_alb(i)
               lambda2 = lam_alb(i+1)
               if ((lambda.gt.lambda1).and.(lambda.le.lambda2)) then
                  alpha1 = alb(i)
                  alpha2 = alb(i+1)
                  alpha = alpha2 + (alpha1 - alpha2)*
     &                 (lambda2 - lambda)/(lambda2 - lambda1)   
               endif
            enddo 
         endif
         ! Calcul de la fonction de phase de la lune en fonction de l'angle de phase
         ! Donn�es provenant de Hapke de 0.3 � 1.2�m pour les angles de 0 � 120�, puis
         ! interpol�s au del�
         do i=1,18
           ang1 = ang(i)
           ang2 = ang(i+1)
           aa1 = mag_a(i)
           aa2 = mag_a(i+1)
           bb1 = mag_b(i)
           bb2 = mag_b(i+1)
           if ((ang_phas.ge.ang1).and.(ang_phas.le.ang2)) then
                 aa = aa2 + (ang2-ang_phas)*(aa1-aa2)/(ang2-ang1)
                 bb = bb2 + (ang2-ang_phas)*(bb1-bb2)/(ang2-ang1)
                 if ((lambda.ge.0.3).and.(lambda.le.1.2)) then
                    long_onde = lambda
                 elseif (lambda.lt.0.3) then
                    long_onde = 0.3
                 elseif (lambda.gt.1.2) then
                    long_onde = 1.2
                 endif
           endif
         enddo
         f = 10**(-0.4*(aa-bb*long_onde)) ! fonction de phase lunaire
         
         !irradiance � la surface de la lune
         solar_spec(iiv) = solar_spec(iiv)*(Rst/Dsl)**2
         ! luminance r�fl�chie par la lune (hypoth�se luminance isotrope)
         solar_spec(iiv) = solar_spec(iiv)*alpha/pi
         ! Eclairement lunaire au sommet de l'atmosph�re de la terre 
         ! = luminance r�fl�chie x angle solide de la lune x fonction de phase lunaire
         solar_spec(iiv) = solar_spec(iiv)*f*pi*(Rl/(Rtl-Rt))**2
        ! write(205,*) lambda,alpha,f,sol_sp,solar_spec(iiv)
      enddo
         
      DEALLOCATE (lam_alb)
      DEALLOCATE (alb)
         
      end
c--------------------------------------------------------------------------   
      subroutine coord_galac(lat,long,heure_d,imois,iannee,ijour,
     &                       tetaobs,lg,b)
      
c     Routine de calcul des coordonn�es galactiques � partir des 
c     coordonn�es �quatoriales
c       Mod�lisation P. Simoneau
      
      real  lat,long,heure_d,minute,seconde
      real *8 b,lg,sinb,delta,delta_NGP,alpha,alpha0,diff
      real *8 pi,rad,arg1,arg2,coslml0,l0
      real *8 heure,TD,JD,TsG

      
      pi = dacos(-1.d0)
      RAD=PI/180.0
      
      ! Calcul de l'ascension droite � la position de l'observateur
      ! ***********************************************************
      ! Calcul du temps terrestre
      heure = heure_d
      DT = 67.d0/3600.d0
      heure = heure + DT
 
      ! Calcul du jour Julien : JD
      call Julien(imois,iannee,ijour,heure,JD,TD)
      ! Calcul du temps Sid�ral moyen � Greenwich (angle horaire � Greenwich par rapport au point vernal) : TsG       
      call tsgm(JD,TD,TSG)
      
      AHL = 0. ! angle horaire local = 0 car vis�e au z�nith
      alpha = TSG - long - AHL
        
      
      ! Transformation coordonn�es �quatoriales en coordonn�es galactiques
      ! ******************************************************************
      ! lat, long : latitude longitude de l'observateur
      ! b, lg : latitude et longitude galactiques
      ! delta = d�clinaison = angle entre le point vis� et l'�quateur c�leste (l'�quateur 
      !                       terrestre ici). Equivalent � la latitude 
      delta_NGP = 27.13 ! d�clinaison du pole nord galactique dans le
                        ! rep�re �quatorial(equinox 2000)
      alpha0 = 192.8595 ! ascension droite du pole nord galactique dans le
                        ! rep�re �quatorial(equinox 2000)
      l0 = 33.0         ! noeud ascendant de l'�quateur galactique     
      l0 = 32.93
      
      delta = lat - tetaobs ! d�clinaison pour la direction vis�e

      diff = alpha0-alpha
      sinb = dsin(delta*rad)*dsin(delta_NGP*rad)  
     &       + dcos(delta*rad)*dcos(delta_NGP*rad)*dcos(diff*rad)
      b = dasin(sinb)/rad
      
      arg1 = dcos(diff*rad)*dcos(delta*rad)
      arg2 = dsin(delta*rad)*dcos(delta_NGP*rad)
     &       - dcos(delta*rad)*dsin(delta_NGP*rad)*dsin(diff*rad)
      lg = datan2(arg2,arg1)/rad + 32.93
      
      
      
      arg1 = dsin(diff*rad)
      arg2 = dcos(diff*rad)*dsin(delta_NGP*rad) - 
     &       dtan(delta*rad)*dcos(delta_NGP*rad)
      lg = 303.0 - datan2(arg1,arg2)/rad
      if (lg.gt.360) lg = lg - 360.0
      
      end
c--------------------------------------------------------------------------      

      subroutine fond_galac(lg,bg,iv,lat_pion,lon_pion,val_pion,
     &                      freq_pion,spect_pion,fnd_gal)
      
c     Routine de calcul du fond d'�toiles (Integrated Star Light)
c     Bas�es sur les donn�es de Leinert (1998) et Mattila (1980)
c       Mod�lisation P. Simoneau

      real (kind=8) lg,bg
      real  lat_pion(50),lon_pion(50),val_pion(50,50)
      real  freq_pion(2000),spect_pion(2000,50)
      real  moy,mic,fnd_gal
      
      ! recherche de l'indice de la longitude galactique
      do i=2,36
         if ((lg.ge.lon_pion(i-1)).and.(lg.le.lon_pion(i))) then
            moy = (lon_pion(i-1)+lon_pion(i))/2
            if (lg.lt.moy) then
               ilon = i-1
            else
               ilon = i
            endif
         endif
      enddo
      if (lg.gt.350) ilon=36
      
      ! recherche de l'indice de la latitude galactique
      do i=2,25
         if ((bg.ge.lat_pion(i-1)).and.(bg.le.lat_pion(i))) then
            moy = (lat_pion(i-1)+lat_pion(i))/2
            if (bg.lt.moy) then
               ilat = i-1
            else
               ilat = i
            endif
         endif
      enddo
      if (bg.gt.80) ilat=25
      if (bg.lt.-80) ilat=1
      
      ! Fond galactique Pionneer en W/m2/sr/�m
      fnd_gal = val_pion(ilon,ilat)
      ! Corrections spectrales (donn��es Mattila
      mic = 1e4/float(iv)
      ilatp = ilat - 12
      if (bg.le.0) ilatp = 14 - ilat
      cor = 0.
      do i=2,1526
         if ((mic.ge.freq_pion(i-1)).and.(mic.le.freq_pion(i))) then
            moy = (freq_pion(i-1) + freq_pion(i))/2
            if (mic.lt.moy) then
               ifreq = i-1
            else
               ifreq = i
            endif
            cor = spect_pion(ifreq,ilatp)
         endif
      enddo
      fnd_gal = fnd_gal*cor
      fnd_gal = fnd_gal*mic/float(iv)  ! passage en cm-1
      
      end
c--------------------------------------------------------------------------   
         
      subroutine donnees_aerosols(iaero,nom_aer,ncouche,nlambda,nang,
     & sigex,sigabs,phaze,sigmaext,sigmaabs,alamda,ang,phase,
     & hum_rel,alt)

c  Routine de calcul des donn�es optiques des a�rosols de l'AFRL
c  Fichiers de donn�es bas�s sur : 
c                   33 valeurs de concentrations (naa) de 0 � 100 km => 32 couches
c                    4 r�gion de type d'a�rosols : 0-2km 2-10 km 10-30km 30-100km
c                    4 humidit�s relatives : 0-70-80-99%

      include 'parameter.rayjn'
      parameter (naa=33,nhum=4,nr=4)

      real alamda(klamb),sigmaext(klamb,laythr),sigmaabs(klamb,laythr)
      real phase(klamb,knang,laythr),ang(knang)
      real phas1(knang),phas2(knang)
      real alt(laythr+1),hum_rel(laythr+1)
      real altaer(naa),humr(nhum),altr(nhum)
      
      real sigex(klamb,knln),sigabs(klamb,knln),phaze(klamb,knang,knln)
      
      character * 50 nom_aer,nomfic,entete
      integer * 1 ok
      
      real, DIMENSION(:),ALLOCATABLE :: dstaer,densite
      REAL, DIMENSION(:,:,:),ALLOCATABLE :: ext,abso
      REAL, DIMENSION(:,:,:,:),ALLOCATABLE :: phas   
      
      Integer, DIMENSION(:),ALLOCATABLE :: ireg   
           
      
      ALLOCATE(ext(klamb,nr,nhum),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation ext dans donnees_aerosols'
        call exit(1)
      ENDIF
      ALLOCATE(abso(klamb,nr,nhum),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation abs dans donnees_aerosols'
        call exit(1)
      ENDIF
      ALLOCATE(phas(klamb,knang,nr,nhum),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation phas dans donnees_aerosols'
        call exit(1)
      ENDIF
      ALLOCATE(dstaer(knln),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation dstaer dans donnees_aerosols'
        call exit(1)
      ENDIF      
      ALLOCATE(densite(knln),STAT=ok)                       
      IF (ok /= 0) THEN
        print*,'#ERR :probleme allocation densite dans donnees_aerosols'
        call exit(1)
      ENDIF    
      ALLOCATE(ireg(knln),STAT=ok)                       
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation ireg dans donnees_aerosols'
        call exit(1)
      ENDIF    
                  
      Data altaer/ 0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,
     &12.,13.,14.,15.,16.,17.,18.,19.,20.,21.,22.,23.,24.,25.,30.,35.,
     &40.,45.,50.,70.,100./    
      Data humr/0.,70.,80.,99./   
      Data altr/2.,10.,30.,100/          
                       
c     KNLN = nombre de couche dans le programme de diffusion
c     laythr = nombre de segments le long du trajet            


c     Calcul des densites moyennes dans chaque couche. On suppose que la densite d'aerosols
c     suit une loi exponentielle de la forme :
c                             N(z) = N0 exp(-(z-z0)/H)
c                   avec H = (z1-z0)/Log(N1/N0)
c                        N0 = densite a l'altitude z0         
c                        N1 = densite a l'altitude z1         
c
c           d'ou Nmoy = (H/DZ)*(N0-N1)
c                   avec DZ = z1-z0

c     lecture des coef d'extinction, d'absorption et de la fonction de phase
 
      nomfic= TRIM(ADJUSTL(nom_aer))
      if (iaero.eq.1) then
         open(2,file='DATA/'//nomfic)
      else
         open(2,file='UTIL/'//nomfic)
      endif
    ! lecture de l'ent�te
      read(2,*) entete
    ! lecture des densit�s pour l'ensemble des altitudes naa
      read(2,*) (dstaer(i),i=1,naa)
    ! lecture du nb d'angles, nb de lambda, nb humidit�, val humidit�, nb de r�gions, alt max r�gions
      read(2,*) nang,nlambda
c     lecture des valeurs de l'angle de diffusion
      read(2,*) (ang(i),i=1,nang)
c==>  boucle sur les longueurs d'onde
      do ilamb = 1,nlambda
         do ir=1,nr
            if (ir.le.2) then
               do ih=1,nhum
                  read(2,*) alamda(ilamb),ext(ilamb,ir,ih),
     &            abso(ilamb,ir,ih)
                     !  initialisation pour �viter les valeurs � 0.
                  if (abso(ilamb,ir,ih).eq.0.) abso(ilamb,ir,ih) = 1e-10
                  do iang=1,nang
                     read(2,*) phas(ilamb,iang,ir,ih)
                  enddo
               enddo
            else
               read(2,*) alamda(ilamb),ext(ilamb,ir,1),abso(ilamb,ir,1)
               do iang=1,nang
                  read(2,*) phas(ilamb,iang,ir,1)
               enddo
            endif
         enddo
      enddo
      
      
      
      
   ! Attribution des r�gions d'a�rosols aux altitudes du profil thermodynamique 
      densite(1) = dstaer(1)
      ireg(1) = 1
      ialt = 1
      fdf = 0
      do while (fdf==0)
         ialt = ialt + 1
         altp = alt(ialt)
         if (ialt.eq.ncouche+1) fdf=1
         if (altp.gt.altaer(naa)) exit
         eof =0
         ir = 0
         do while (eof==0) 
            ir = ir + 1
            if (altp.le.altr(ir)) then
               ireg(ialt) = ir
               eof = 1
            elseif (altp.gt.altr(nr)) then
               ireg(ialt) = nr
               densite(ialt) = dstaer(naa)
               eof = 1
            endif
         enddo
         eof = 0
         iaer = 1
         do while (eof==0)
            iaer=iaer+1
            altaer1 = altaer(iaer)
            altaer0 = altaer(iaer-1)
            dst1 = dstaer(iaer)
            dst0 = dstaer(iaer-1)           
            if (altp.eq.altaer1) then
               densite(ialt) = dst1
               eof = 1
            elseif ((altp.lt.altaer1).and.(altp.gt.altaer0)) then
               echelle = -(altaer1-altaer0)/alog(dst1/dst0)
               densite(ialt) = dst0*exp(-(altp-altaer0)/echelle)
               eof = 1
            endif
         enddo
      enddo
      if (ialt.le.ncouche+1) then   ! on extrapole les a�rosols jusqu'� 120 km
         do i=ialt,ncouche+1
            ireg(i) = nr
            densite(i) =  dst0*exp(-(altp-altaer0)/echelle)                         !dstaer(naa)
            ialt = ialt + 1
            altp = alt(ialt)
         enddo
      endif       

   !  Calcul des grandeurs optiques pour chaque altitude du profil thermodynamique
      do ialt=1,ncouche+1
         altp = alt(ialt)
         do ih=1,nhum-1
            if ((humr(ih).le.hum_rel(ialt)).and.
     &          (humr(ih+1).gt.hum_rel(ialt))) then
               x = alog(100. - hum_rel(ialt))
               x1 = alog(100. - humr(ih))
               x2 = alog(100. - humr(ih+1))
               ix1 = ih
            endif
         enddo
         do ilamb=1,nlambda
            ir=ireg(ialt)
            if (ir.le.2) then
               y1 = alog(ext(ilamb,ir,ix1))
               y2 = alog(ext(ilamb,ir,ix1+1))
               y = y1 + (y2-y1)*(x-x1)/(x2-x1)
               sigex(ilamb,ialt) = densite(ialt)*exp(y)
               y1 = alog(abso(ilamb,ir,ix1))
               y2 = alog(abso(ilamb,ir,ix1+1))
               y = y1 + (y2-y1)*(x-x1)/(x2-x1)
               sigabs(ilamb,ialt) = densite(ialt)*exp(y)
               do iang=1,nang
                   y1 = alog(phas(ilamb,iang,ir,ix1))
                   y2 = alog(phas(ilamb,iang,ir,ix1+1))
                   y = y1 + (y2-y1)*(x-x1)/(x2-x1)
                    phaze(ilamb,iang,ialt) = exp(y)
               enddo
            else
               sigex(ilamb,ialt) = densite(ialt)*ext(ilamb,ir,1) 
               sigabs(ilamb,ialt) = densite(ialt)*abso(ilamb,ir,1) 
               do iang=1,nang
                  phaze(ilamb,iang,ialt) = phas(ilamb,iang,ir,1)
               enddo
            endif
         enddo
      enddo
   
   ! Calcul des valeurs moyennes sur les couches   
      
      do ilamb=1,nlambda
         do ialt=1,ncouche
            epa = (alt(ialt+1) - alt(ialt))
            sigmaext(ilamb,ialt) = 0.5*epa*
     &            (sigex(ilamb,ialt) + sigex(ilamb,ialt+1))
            fact_ech = 0.
            if (sigex(ilamb,ialt).ne.sigex(ilamb,ialt+1)) fact_ech = 
     &               -epa/alog(sigex(ilamb,ialt+1)/sigex(ilamb,ialt))
            if (fact_ech.gt.0) sigmaext(ilamb,ialt) = epa*fact_ech*
     &        (sigex(ilamb,ialt)- sigex(ilamb,ialt+1))

            sigmaabs(ilamb,ialt) = 0.5*epa*
     &            (sigabs(ilamb,ialt) + sigabs(ilamb,ialt+1))
                 fact_ech = 0.
            if (sigabs(ilamb,ialt).ne.sigabs(ilamb,ialt+1)) fact_ech = 
     &               -epa/alog(sigabs(ilamb,ialt+1)/sigabs(ilamb,ialt))
            if (fact_ech.gt.0) sigmaabs(ilamb,ialt) = epa*fact_ech*
     &        (sigabs(ilamb,ialt)- sigabs(ilamb,ialt+1))
c            if (sigabs(ilamb,ialt).eq.sigabs(ilamb,ialt+1)) then
               ! couche homog�ne
c               sigmaabs(ilamb,ialt) = sigabs(ilamb,ialt)*epa
c            else
               ! couche h�t�rog�ne : variation exponentielle
c               fact_ech = 
c     &             -epa/alog(sigabs(ilamb,ialt+1)/sigabs(ilamb,ialt))
c               sigmaabs(ilamb,ialt) = epa*fact_ech*
c     &        (sigabs(ilamb,ialt)- sigabs(ilamb,ialt+1))
c            endif
 !              sigmaabs(ilamb,ialt) = 
 !    &        (sigabs(ilamb,ialt)- sigabs(ilamb,ialt+1))/(
 !    &         alog(sigabs(ilamb,ialt)) - alog(sigabs(ilamb,ialt+1)))
             
            do iang=1,nang
               phase(ilamb,iang,ialt) = 
     &            (phaze(ilamb,iang,ialt)+phaze(ilamb,iang,ialt+1))/2.
            enddo
         enddo
      enddo   
   
      DEALLOCATE(ext)
      DEALLOCATE(abso)
      DEALLOCATE(phas)
      DEALLOCATE(dstaer) 
      DEALLOCATE(densite)
      DEALLOCATE(ireg)                   
      end
c------------------------------------------------------------------------------
      function phas_interp(angd,nang,ang,phsf,l,iang_dif)

c Fonction d'interpolation de la fonction de phase

      include 'parameter.rayjn'
c     double precision  ang(knang),phsf(knang,knln)
      real  ang(knang),phsf(knang,laythr)

      if (iang_dif.lt.nang) then
        ang1 = ang(iang_dif)
        ang2 = ang(iang_dif+1)
        ph1 = phsf(iang_dif,l)
        ph2 = phsf(iang_dif+1,l)
        if ((angd.ge.ang1).and.(angd.le.ang2)) then
           phas_interp = ph1 + (angd - ang1)/(ang2-ang1)*(ph2-ph1)
           return
        endif
      else
        phas_interp = phsf(iang_dif+1,l)
      endif
      RETURN
      END
c--------------------------------------------------------------------------------
        subroutine donnees_nuage(inua,nom_nua,itype_nua,ncouche,nlambda,
     &             nang,nuaext,nuaabs,nuaphaze,nuamext,nuamabs,alamda,
     &             ang,nuaphase,alt,ialtb,ialts,nlambdan,nangn)
     
      include 'parameter.rayjn'


      real alamda(klamb)
      real ang(knang)
      real alt(laythr+1)
      
      real nuaext(klamb,knln),nuaabs(klamb,knln)
      real nuaphaze(klamb,knang,knln),nuaphase(klamb,knang,knln)
      real nuamext(klamb,knln),nuamabs(klamb,knln)
      
      character * 50 nom_nua,entete,nomfic
      integer * 1 ok
      
      REAL, DIMENSION(:),ALLOCATABLE :: ext,abso,densite,altn
      REAL, DIMENSION(:,:),ALLOCATABLE :: phas   
      
      
      ALLOCATE(ext(klamb),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation ext dans donnees_nuages'
        call exit(1)
      ENDIF
      ALLOCATE(abso(klamb),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation abs dans donnees_nuages'
        call exit(1)
      ENDIF
      ALLOCATE(phas(klamb,knang),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation phas dans donnees_nuages'
        call exit(1)
      ENDIF
      ALLOCATE(densite(knln),STAT=ok)                       
      IF (ok /= 0) THEN
        print*,'#ERR :probleme allocation densite dans donnees_nuages'
        call exit(1)
      ENDIF    
      ALLOCATE(altn(50),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation altn dans Lecdo'
        call exit(1)
      ENDIF 
  
                
                       
c     KNLN = nombre de couche dans le programme de diffusion
c     laythr = nombre de segments le long du trajet            


c     Calcul des densites moyennes dans chaque couche. On suppose que la densite des nuages
c     suit une loi exponentielle de la forme :
c                             N(z) = N0 exp(-(z-z0)/H)
c                   avec H = (z1-z0)/Log(N1/N0)
c                        N0 = densite a l'altitude z0         
c                        N1 = densite a l'altitude z1         
c
c           d'ou Nmoy = (H/DZ)*(N0-N1)
c                   avec DZ = z1-z0

c     lecture des coef d'extinction, d'absorption et de la fonction de phase

 
      do ialt=1,ncouche+1
         do ilamb=1,nlambda
            nuaext(ilamb,ialt) = 0.
            nuamext(ilamb,ialt) = 0.
            nuaabs(ilamb,ialt) = 0. 
            nuamabs(ilamb,ialt) = 0.
            do iang=1,nang
               nuaphaze(ilamb,iang,ialt) = 0.
               nuaphase(ilamb,iang,ialt) = 0.
            enddo
         enddo
      enddo
      nlambdan = nlambda
      nangn = nang
      if (inua.eq.0) return
 
      do ialt=1,ncouche+1
         densite(ialt) = 1.e-20
      enddo
      nomfic= TRIM(ADJUSTL(nom_nua))
      if (inua.eq.1) then
         open(2,file='DATA/'//nomfic)
      else
         open(2,file='UTIL/'//nomfic)
      endif
    ! lecture de l'ent�te
      read(2,*) entete
    ! lecture du nb de couche composant le nuage
      read(2,*) ncn
      read(2,*) (altn(i),i=1,ncn)
      read(2,*) (densite(i),i=ialtb,ialts)
    ! lecture du nb d'angles de la fonction de phase, nb de lambda
      read(2,*) nangn,nlambdan
c     lecture des valeurs de l'angle de diffusion
      read(2,*) (ang(i),i=1,nangn)
c==>  boucle sur les longueurs d'onde
      do ilamb = 1,nlambdan
         read(2,*) alamda(ilamb),ext(ilamb),abso(ilamb)
         do iang=1,nangn
            read(2,*) phas(ilamb,iang)
         enddo
      enddo
      
   !  Calcul des grandeurs optiques pour chaque altitude du profil thermodynamique
      do ialt=1,ncouche+1
         do ilamb=1,nlambdan
            nuaext(ilamb,ialt) = densite(ialt)*ext(ilamb)
            nuaabs(ilamb,ialt) = densite(ialt)*abso(ilamb)
            do iang=1,nangn
               nuaphaze(ilamb,iang,ialt) = phas(ilamb,iang)
            enddo
         enddo
      enddo
   
   ! Calcul des valeurs moyennes sur les couches   
      
      do ilamb=1,nlambdan
         do ialt=1,ncouche
            epa = (alt(ialt+1) - alt(ialt))
            nuamext(ilamb,ialt) = 
     &      0.5*epa*(nuaext(ilamb,ialt) + nuaext(ilamb,ialt+1))
            nuamabs(ilamb,ialt) = 
     &      0.5*epa*(nuaabs(ilamb,ialt) + nuaabs(ilamb,ialt+1))               
            do iang=1,nangn
               nuaphase(ilamb,iang,ialt) = 
     &        (nuaphaze(ilamb,iang,ialt)+nuaphaze(ilamb,iang,ialt+1))/2.
            enddo
         enddo
      enddo   
   
      DEALLOCATE(ext)
      DEALLOCATE(abso)
      DEALLOCATE(phas)
      DEALLOCATE(densite)   
      DEALLOCATE(altn)               
      end
c-------------------------------------------------------------------------------------

      subroutine interp_lambda(nlamx,iv1,iv2,lambda,lum_sig,lum_lam,
     &                          nlamb)
      
      include 'parameter.rayjn'
            
      REAL(KIND=8) lum1,lum2,lam,lam1,lam2,pas
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: dump
      REAL(KIND=8), DIMENSION(nsigmx) :: lum_sig
      REAL(KIND=8), DIMENSION(nlamx) :: lambda,lum_lam
  
      integer ok

      ALLOCATE(dump(nsigmx),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation dump dans interp_lambda'
        call exit(1)
      ENDIF       

      niv = iv2-iv1+1
      ivc = 0
      do iv=iv1,iv2
         ivc = ivc+1
         lam = 1e4/float(iv)
         dump(ivc) = lum_sig(ivc)*float(iv)/lam
      enddo
      pas = 1e4*(1./float(iv2-1) -1./float(iv2))
      ivc = 1
      lambda(1) = 1e4/float(iv2)
      lum_lam(1) = dump(niv)
      lam2 = 1e4/float(iv1)
      iv = iv2 - 1
      index = niv +1 -iv2 + iv
      do while (index.gt.1)
         ivc = ivc+1
         lambda(ivc) = lambda(ivc-1) + pas
         lam = 1e4/float(iv)
         lam1 = 1e4/float(iv+1)
         dlam = 1e4/float(iv) - 1e4/float(iv+1)
         if (lambda(ivc).le.lam) then
            index = niv +1 -iv2 + iv
            lum1 = dump(index)
            index = index - 1
            lum2 = dump(index)
            lum_lam(ivc) = lum1 + 
     &     (lum2 - lum1)*(lambda(ivc) - lam1)/dlam
         else
            iv = iv-1
            ivc = ivc-1
         endif
      enddo
      nlamb = ivc -1
      end
      
c-------------------------------------------------------------------------------------

      subroutine convolution(nlamx,lambda,lum_lam,nlamb,fwhm,convol,
     &                       kdep)
      
      double precision Gauss
      include 'parameter.rayjn'

            
      REAL(KIND=8) pas,betad,pi,x,h,c,Eph
      REAL(KIND=8), DIMENSION(nlamx) :: lambda,lum_lam,convol
      
      pi = acos(-1.)
      h = 6.62606876e-34 ! J.s
      c = 2.99792458e10    ! cm/s
     
      pas = lambda(2) - lambda(1)
      if (fwhm.le.pas) then
          write(*,*) '** Largeur a mi-hauteur = ',fwhm, ' non ',
     &       'compatible avec le pas de calcul = ',pas, ' ! **'
          return
      endif
      betad = fwhm/1.3862
      reso = 5.*fwhm 
      sigmm = lambda(1) + reso/2.
      kdep = 1
      do while (lambda(kdep).lt.sigmm)
         kdep = kdep + 1
         if (kdep.gt.nlamb) then
            write(*,*) 'Resolution incompatible avec le nombre ',
     &                  'de points'
            stop
         endif
      enddo
    
      jd = 1
      do k=kdep,nlamb-kdep-1
         convol(k) = 0.
         do i=1,2*kdep+1
            ind = k - kdep + i 
            pas = lambda(ind+1) - lambda(ind)  
            x= lambda(ind)
            convol(k) = convol(k) + 
     &                  lum_lam(ind)*gauss(x,lambda(k),fwhm)*pas
         enddo 
      enddo
      end
      
!-----------------------------------------------
      double precision function Gauss(sigma,sigma0,fwhm)

      real (kind=8) betad,sigma,sigma0,k,m,c,h,T

      pi = acos(-1.)
      sqrtpi = sqrt(pi)

c      betad = fwhm/1.665109222
c      gauss = dexp(-((sigma-sigma0)/betad)**2)/betad/sqrtpi

c      Nouvelle d�finition de gauss
      aln2 = 0.69
      gamd = fwhm/2.
      alfa = aln2/gamd**2
      gauss = dexp(-alfa*(sigma-sigma0)**2)/sqrt(pi/alfa)
      end
c!----------------------------------------------------------------
      subroutine niveau_nuit(nlamx,lambda,flux_lam,nlamb) 
      
      include 'parameter.rayjn'

            
      REAL(KIND=8) pi
      REAL(KIND=8), DIMENSION(nlamx) :: lambda,flux_lam
      REAL el(1000),lam_el(1000),lam1,lam2,dum1,dum2,lux,sum
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: dump
      integer ok

      ALLOCATE(dump(nlamx),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation dump dans niveau_nuit'
        call exit(1)
      ENDIF       
      
      pi = acos(-1.)     
      
c      call eff_lum(lam_el,el,nlam)
      iv = 1
      do while (lambda(iv).lt.lam_el(1))
         dump(iv) = 0.
         iv = iv + 1
      enddo
      if (lambda(iv).eq.lam_el(1)) then
         dump(iv) = el(1)
      else
         dum1 = el(1)
         dum2 = el(2)
         dump(iv) = dum1 + 
     &    (dum2-dum1)/(lam_el(2)-lam_el(1))*(lambda(iv)-lam_el(1))
      endif  
      iv1 = iv
      iv = iv + 1
      do i=1,nlam-1
         lam1 = lam_el(i)
         lam2 = lam_el(i+1)
         do while (lambda(iv).lt.lam_el(i+1))          
            dum1 = el(i)
            dum2 = el(i+1)
            dump(iv) = dum1 + 
     &      (dum2-dum1)/(lam2-lam1)*(lambda(iv)-lam1)
            dump(iv) = dump(iv)*flux_lam(iv)
            iv = iv + 1
         enddo
      enddo
      niv = iv-1
      sum = 0.
      do i=iv1,niv-1
         val = (dump(i) + dump(i+1))/2.
         dlam = lambda(i+1) - lambda(i)
         sum = sum + val*dlam
      enddo
 
      lux = sum*683.002
      if (lux.le.0.0007) ityp = 5
      if ((lux.gt.0.0007).and.(lux.le.0.002)) ityp = 4
      if ((lux.gt.0.002).and.(lux.le.0.01)) ityp = 3
      if ((lux.gt.0.01).and.(lux.le.0.04)) ityp = 2
      if ((lux.gt.0.04).and.(lux.le.2.)) ityp = 1
      if (lux.le.2) then
         write(*,*)
         write(*,*) ' Eclairement en lux : ',lux,' Nuit de type :',ityp
         write(16,*)
         write(16,*) ' Eclairement en lux : ',lux,' Nuit de type :',ityp
      else
         write(*,*)
         write(*,*) ' Eclairement en lux : ',lux
         write(16,*)
         write(16,*) ' Eclairement en lux : ',lux
      endif
       
      end
      
!----------------------------------------------------------------
      subroutine lect_fic(lam_el,el,nlam,ifiltre) 
      
      REAL el(1000),lam_el(1000)
      
      integer ios
      
      if (ifiltre.eq.1) open(2,file='Data/Reponse_oeil.txt')
      if (ifiltre.eq.2) open(2,file='Data/Saber_filterB.txt')
      
      ios = 0
      i=0
      do while(ios==0)
         i=i+1
         read(2,*,iostat=ios) lam_el(i),el(i)
      enddo
      close (2)
      nlam = i - 1
      end
c!----------------------------------------------------------------
      subroutine filtre(nfmx,freq,val,nfreq,sum,ifiltre,
     &                  it_fi)
      
      include 'parameter.rayjn'

            
      REAL(KIND=8) pi,sum,lam1,lam2,dum1,dum2,dlam
      REAL(KIND=8), DIMENSION(nfmx) :: freq,val
      REAL frfi(1000),valfi(1000)
      real(KIND=8), DIMENSION(:),ALLOCATABLE :: dump
      integer ok
   
      
      pi = acos(-1.)     
            
      call lect_fic(frfi,valfi,nfi,ifiltre)
      
      if ((freq(1).gt.frfi(1)).or.(freq(nfreq).lt.frfi(nfi))) then
          write(*,*) 'Calcul impossible : bornes spectrales', 
     &                 frfi(1),'-',frfi(nfi),'non adaptees' 
          write(*,*)
          it_fi = 0
          return
      endif
      
      ALLOCATE(dump(nfmx),STAT=ok)
      IF (ok /= 0) THEN
        print*,'#ERR : probleme allocation dump filtre'
        call exit(1)
      ENDIF        
      do i=1,nfreq
         dump(i) = 0.
      enddo
      
      iv = 1
      if (freq(iv).le.frfi(1)) then
c        ! dump(iv) = valeurs du filtre aux fr�quences du mod�le de transfert radiatif
         do while (freq(iv).lt.frfi(1))
            iv = iv + 1
         enddo
         if (freq(iv).eq.frfi(1)) then
            dump(iv) = valfi(1)
         else
            dum1 = valfi(1)
            dum2 = valfi(2)
            dump(iv) = dum1 + 
     &      (dum2-dum1)/(frfi(2)-frfi(1))*(freq(iv)-frfi(1))
c       !  on multiplie la luminance par la valeur du filtre
            dump(iv) = dump(iv)*val(iv)
         endif
      else
         do while (frfi(iv).lt.freq(1))
            iv = iv + 1
         enddo         
      endif      
      iv1 = iv
      iv = iv + 1
      do i=1,nfi-1
         lam1 = frfi(i)
         lam2 = frfi(i+1)
         do while (freq(iv).lt.frfi(i+1))        
            dum1 = valfi(i)
            dum2 = valfi(i+1)
            dump(iv) = dum1 + 
     &      (dum2-dum1)/(lam2-lam1)*(freq(iv)-lam1)
c       !  on multiplie la luminance par la valeur du filtre
            dump(iv) = dump(iv)*val(iv)
            iv = iv + 1
         enddo
      enddo
      niv = iv-1
      if (ifiltre.eq.1) then
      do i=iv1,niv
         write(1958,*) freq(i),val(i),dump(i)
      enddo
      endif
      sum = 0.
      do i=iv1,niv-1
         valdump = (dump(i) + dump(i+1))/2.
         dlam = freq(i+1) - freq(i)
         sum = sum + valdump*dlam
      enddo    
      deallocate (dump)
      end
      
!----------------------------------------------------------------
      subroutine extract(ligne,heure,imois,iannee,ijour,
     &                   Jday,teta0,date)
      
      real heure
      integer Jday,nmois(12),ijour,iannee,iheure,imn
      character * 50 ligne
      character * 8 mois(12)
      data nmois /31,28,31,30,31,30,31,31,30,31,30,31/
      data mois /'janvier','fevrier','mars','avril','mai',
     &           'juin','juillet','aout','septembre',
     &           'octobre','novembre','decembre'/
      
      i = 1
      do while (ligne(i:i).eq.' ')
         i = i+1
      enddo
      i1=i
      read(ligne(i1:i1+1),'(i2)') ijour
      read(ligne(i1+3:i1+4),'(i2)') imois
      read(ligne(i1+6:i1+10),'(i4)') iannee
      i = i1+11
      do while (ligne(i:i).eq.' ')
         i = i+1
      enddo
      i2 = i
      read(ligne(i2:i2+1),'(i2)') iheure
      read(ligne(i2+3:i2+4),'(i2)') imn
      heure = float(iheure) + float(imn)/60.
      Jday = ijour
      if (imois.gt.1) then
         do i=1,imois-1
            Jday = Jday + nmois(i)
         enddo
      endif
      itest = 4*(iannee/4)
      if (iannee.eq.itest) Jday = Jday+1
    !  write(*,'(a15,i2,1x,a8,i4,a3,f7.3,a6,5x,a3,i3,a21)') 
    ! &'Calcul pour le ',ijour,mois(imois),iannee,
    ! &' a ',iheure,' heure',' / ',Jday,' ieme jour de l''annee'
      end
!----------------------------------------------------------------      
      subroutine param_soleil(heure,imois,iannee,ijour,
     &                        j,lati,long,teta0,hobs)
      
      real lati,lat,long
      real correc,ET,heure
      real Para,Refrac,diam,eta1,eta2,hobs
      real *8 va,arg,arg1,arg2,arg3,Dec,Ah,hs
      real * 8 TD,jour,JD,mois,L0,cent,amoy,Lonsol
      real * 8 t0,eps0,U,alpha
      character *4 val
      
      pi = acos(-1.)
      conv = pi/180.
      
      Para = 0  !  sauf pour lune para=57'
      Refrac = 0.56 ! 34'
      diam = 0.53   ! 32'
      eta1 = 0.0193*sqrt(hobs*1000.)
      ! eta2 = atan(D/l)  l=distance � la montagne de hauteur D
      eta2 = 0.
      
      lat = lati*conv
      
      ! Jour Julien : JD
       if (imois.le.2) then
          Y = iannee - 1
          Mois = float(imois + 12)
       else
          Y = iannee
          Mois = float(imois)
       endif
       jour = ijour + heure/24
       A = int(Y/100)
       B = 2 - A + int(A/4)
       JD = int(365.25*(Y+4716)) + int(30.6001*(Mois+1)) 
     &     + jour +B - 1524.5
       
       ! temps en si�cle Julien
       TD = (JD - 2451545.0)/36525
       ! temps sid�ral moyen : t0
        t0 = 280.46061837 + 
     &      360.98564736629*(JD - 2451545.0) +
     &      0.000387933*TD*TD - TD*TD*TD/38710000
       if (t0.lt.0.) then
          it0 = int(abs(t0)/360.) + 1
          t0 = t0 + 360.*float(it0)
       endif
       t0 = t0/15.
       ! obliquit� de l'�cliptique : eps0
       U = TD/100.
       eps0 = 23*3600 + 26*60 + 21.448 - 4680.93*U -
     &         1.55 *u*u + 1999.25*(u**3)- 51.38*(u**4)
     &       - 249.67*(u**5) - 39.05*(u**6) + 7.12*(u**7)
     &       + 27.87*(u**8) + 5.79*(u**9) + 2.45*(u**10)
       eps0 = eps0/3600
       ! longitude g�om�trique moyenne par rapport � l'�quinoxe moyenne : L0
       L0 = 280.46645 + 36000.76983*TD + 0.0003032*TD*TD
       if (L0.gt.360.) then
          il0 = int(L0/360)
          L0 = L0 - 360.*float(il0)
       endif     
       if (L0.lt.0.) then
          il0 = int(abs(L0)/360.) + 1
          L0 = L0 + 360.*float(il0)
       endif
       if (L0.gt.360.) then
           write(*,*) 'Probl�me calcul longitude soleil'
       endif
       ! anomalie moyenne : amoy
       amoy = 357.52910 + 35999.05030*TD - 0.0001559*TD*TD
     &        - 0.00000048*TD*TD*TD  
       if (amoy.gt.360.) then
          im0 = int(amoy/360)
          amoy = amoy - 360.*float(im0)
       endif     
       if (amoy.lt.0.) then
          im0 = int(abs(amoy)/360.) + 1
          amoy = amoy + 360.*float(im0)
       endif
       if (amoy.gt.360.) then
           write(*,*) 'Probl�me calcul M'
       endif
       ! Centre
       cent = (1.914600 - 0.004817*TD - 0.000014*TD*TD)*sin(amoy*conv)
     &        + (0.019993 - 0.00010*TD)*sin(2*amoy*conv) 
     &        + 0.000290*sin(3*amoy*conv)
       ! Longitude solaire : Lonsol
       Lonsol = L0 + cent
       ! ascension droite du soleil : alpha
       arg1 = cos(eps0*conv)*sin(Lonsol*conv)
       arg2 = cos(Lonsol*conv)
       alpha = atan(arg1/arg2)/conv

       if (arg2.lt.0) alpha = alpha + 180.
       if (alpha.lt.0) alpha = 360. + alpha
       alpha = alpha/15.  ! conversion en heure
       imn = (alpha - int(alpha))*60
       write(*,'(a24,i2,a1,i2,a2)') 
     &      'Ascension droite soleil : ',int(alpha),
     &      'h',imn,'mn'
       Dec = asin(sin(eps0*conv)*sin(Lonsol*conv))/conv  
      
      ! Equation du temps : ET
      arg = 360*(j-81)/365*conv
      ET = -9.87*sin(2*arg) + 7.53*cos(arg) + 1.5*sin(arg)
      write(*,*) 'equation du temps',ET
      ! vitesse angulaire moyenne de la terre : va
      va = 360/365.25
      arg1 = va*(j - 2)*conv
      arg2 = (va*(j - (81 - 2*sin(arg1))))*conv
      arg3 = 0.3978*sin(arg2)
      ! D�clinaison solaire : Dec
      Dec = aSin(arg3)
c      dec = asin(0.398*sin((0.985*j-80)*conv))
      ! Temps Solaire Vrai : tsv
      correc = ET/60 - long/15.
      ! correc = 0.
      tsv = heure + correc
      imn = (tsv - int(tsv))*60.
      write(*,'(a24,i2,a1,i2,a2)') 'Temps Solaire Vrai (TU)',
     &  int(tsv),'h',imn,'mn'
      ! Angle solaire ou angle horaire : Ah
      Ah = 360.*(tsv-12.)/24.*conv
      ! hauteur du soleil : hs
      arg = sin(lat)*sin(Dec) + cos(lat)*cos(Dec)*cos(Ah)
      hs = asin(arg)
      teta0 = 90 - hs/conv ! angle z�nithal solaire
      ! Azimuth solaire : az
      arg = cos(Dec)*sin(Ah)/cos(hs)
      az = asin(arg)
      arg = sin(Ah)/(cos(Ah)*sin(lat) - tan(Dec)*cos(lat))
      az = atan(arg) 

 
      write(*,'(a16,f7.3,a8,f7.3)') 'Position - lat :',lati,
     &'  long :',long
      write(*,'(a24,f10.3,a7)') 'Angle horaire :        ',
     &Ah/conv,' degres'
      write(*,'(a24,f10.3,a7)') 'Declinaison solaire :  ',
     &Dec/conv,'degres'
      write(*,'(a24,f10.3,a7)') 'Hauteur du soleil :    ',
     &hs/conv,'degres'
      write(*,'(a24,f10.3,a7)') 'Azimuth du soleil :    ',
     &az/conv,'degres'
      write(*,'(a24,f10.3,a7)') 'Angle zenithal solaire :',
     &teta0,'degres'
     
      arg1 = sin(lat)*sin(Dec)
      arg2 = cos(lat)*cos(Dec)
      h0 = Para - Refrac - 0.5*diam - eta1 + eta2    
      Ah = acos((sin(h0*conv)-arg1)/arg2)/conv/15
      min = (tsl-int(tsl))*60.
      arg = cos(Dec)*sin(Ah)/cos(0.)
      az = asin(arg)
            arg = sin(Ah)/(cos(Ah)*sin(lat) - tan(Dec)*cos(lat))
      az = atan(arg)
      write(*,'(a16,i2,a1,i2,a2,a9,f10.3)') 'Heure lever   :',
     &int(tsl),'h',min,'mn',' Azimut :',az/conv  
      ! coucher du soleil � Greenwitch
      tsc = 12. + Ah
      ! coucher du soleil � la longitude de l'observateur
      tsc = tsc + long/15
      min = (tsc-int(tsc))*60.

      arg = cos(Dec)*sin(Ah)/cos(0.)
      az = asin(arg)
            arg = sin(Ah)/(cos(Ah)*sin(lat) - tan(Dec)*cos(lat))
      az = atan(arg) 
      write(*,'(a16,i2,a1,i2,a2,a9,f10.3)') 'Heure coucher :',
     &int(tsc),'h',min,'mn',' Azimut :',az/conv
      
      tsc = 12 + acos((sin(-6.*conv)-arg1)/arg2)/conv/15. - 
     &      correc
      min = (tsc-int(tsc))*60.
      Ah = 360.*(tsc-12.)/24.*conv
      arg = cos(Dec)*sin(Ah)/cos(-6.*conv)
      az = asin(arg)
            arg = sin(Ah)/(cos(Ah)*sin(lat) - tan(Dec)*cos(lat))
      az = atan(arg) 
      write(*,'(a26,i2,a1,i2,a2,a9,f10.3)') 
     &'cr�puscule civil        :',int(tsc),'h',min,'mn',
     &' Azimuth = ',az/conv  
      tsc = 12 + acos((sin(-12.*conv)-arg1)/arg2)/conv/15. - correc
      min = (tsc-int(tsc))*60.
      Ah = 360.*(tsc-12.)/24.*conv
      arg = cos(Dec)*sin(Ah)/cos(-12.*conv)
      az = asin(arg)
            arg = sin(Ah)/(cos(Ah)*sin(lat) - tan(Dec)*cos(lat))
      az = atan(arg) 
      write(*,'(a26,i2,a1,i2,a2,a9,f10.3)') 
     &'cr�puscule nautique     :',int(tsc),'h',min,'mn',
     &' Azimuth = ',az/conv
      tsc = 12 + acos((sin(-18.*conv)-arg1)/arg2)/conv/15. - correc
      min = (tsc-int(tsc))*60.
      Ah = 360.*(tsc-12.)/24.*conv
      arg = cos(Dec)*sin(Ah)/cos(-12.*conv)
      az = asin(arg)
            arg = sin(Ah)/(cos(Ah)*sin(lat) - tan(Dec)*cos(lat))
      az = atan(arg) 
      write(*,'(a26,i2,a1,i2,a2,a9,f10.3)') 
     &'cr�puscule astronomique :',int(tsc),'h',min,'mn',
     &' Azimuth = ',az/conv
               
      end

!---------------------------------------------------------------------------      
      
        subroutine test_lenoble(nda,na1u,am1u,teta0dm,NFI,FI,ncouche,
     &    thkdm,omgdm,nang,angdm,phsfdm,galbedo,Flux0,glow,
     &    atmpdm,Tempsol,sigma,dvdm,ep_gldm,ialt_sol,prop_sol,iazim)
                          
             include 'parameter.rayjn'
             
      
      !  Param�tres pour la diffusion calcul�s en double pr�cision
      REAL(KIND=8) :: teta0dm,galbedo,flux0,prop_sol,pi
      REAL(KIND=8) :: Tempsol,sigma,dvdm   
      REAL(KIND=8), DIMENSION(knln) :: thkdm,omgdm
      REAL(KIND=8), DIMENSION(laythr) :: ep_gl,ep_gldm,glow
      REAL(KIND=8), DIMENSION(knang) :: angdm
      REAL(KIND=8), DIMENSION(knang,knln) :: phsfdm
      REAL(KIND=8), DIMENSION(2,knln) :: atmpdm
      REAL(KIND=8), DIMENSION(kna1u) :: AM1U
      REAL(KIND=8), DIMENSION(knfi) :: FI     
      
      pi = acos(-1.00)                       
                          
        nda = 4
        na1u = 12
        am1u(1) = -1.
        am1u(2) = -0.8
         am1u(3) = -0.6
         am1u(4) = -0.4
         am1u(5) = -0.2
         am1u(6) = -0.00001
         am1u(7) = 0.00001
         am1u(8) = 0.2
         am1u(9) = 0.4
         am1u(10) = 0.6
         am1u(11) = 0.8
         am1u(12) = 0.9999
 !           
         teta0dm = 0.
         nfi = 1
         fi(1) = 0.
         ncouche = 6
        thkdm(1) = 0.05
         thkdm(2) = 0.05
         thkdm(3) = 0.1
         thkdm(4) = 0.3
         thkdm(5) = 0.25
         thkdm(6) = 0.25
    
       thkdm(1) = 3.2
       thkdm(2) = 3.2
       thkdm(3) = 6.4
       thkdm(4) = 19.2
       thkdm(5) = 16.0
       thkdm(6) = 16.0
    
 !   ncouche = 50
 !   do i=1,50
 !      thkdm(i) = 1/50.
 !   enddo

            
        do i=1,ncouche
       omgdm(i) = 0.9
       enddo
       nang = 1

       flux0 = pi
       do l =1,ncouche
       glow(l) =0.
       ep_gldm(l) = 0.
       atmpdm(1,l) = 300.
       atmpdm(2,l) = 300.
       enddo
       tempsol = 300.
       sigma = 14286  ! 0.7 �m
       dvdm = 1
       ialt_sol = ncouche + 2 - 1
       prop_sol = 0
       galbedo = 0.
   
! lecture des angles et de la fonction de phase
! Haze L = 35 valeurs � partir de la ligne 2
! Cloud C1 = 56 valeurs � partir de la ligne 41
           nl= 40
          nang = 56
        open(2,file='haze.txt')
       do i=1,nl
       read(2,*)
       enddo
       do i=1,nang
       read(2,*) angdm(i),p1,p2,phsfdm(i,1)
       do l=1,ncouche
       phsfdm(i,l) = phsfdm(i,1)
       enddo
       enddo   
      close(2)    
      
      end       
!----------------------------------------------------------------
   
      subroutine eclairement_galac(lat,long,heure,imois,iannee,ijour,
     & lat_pion,lon_pion,val_pion,freq_pion,spect_pion,iv,ecl_gal,
     & thk,ray_t,alt,ialt_sol,ncouche,ivc,lat_eclip,lon_eclip,
     &                          val_zodiac)
        
      include 'parameter.rayjn'
      
      real  lat_pion(50),lon_pion(50),val_pion(50,50)
      real  freq_pion(2000),spect_pion(2000,50),fnd_gal
      real  lat,long,dec,hl,deno,tanhl
      real  alt(laythr+1),THK(knln,nsigmx)
      real(kind=8) lg,bg,longec,bet
      real  lat_eclip(10),lon_eclip(19),val_zodiac(19,10)
   
      pi = acos(-1.0)
      rad = pi/180.
      dteta = 5.0
      nteta = 90./dteta
      dteta = dteta*pi/180.
      dfi = 5.0
      nfi = 360./dfi
      dfi = dfi*pi/180.
      teta = dteta/2.
      ecl_gal = 0.
      zodial = 0.
      do i = 1,nteta
         alfa = pi/2. + teta
         utauc = 0.
         R1 = ray_t + alt(ialt_sol)
         do l=ialt_sol,45 !ncouche
            epa = alt(l+1) - alt(l)
            R2 = R1 + epa
            ds = R1*cos(alfa) + sqrt((cos(alfa)**2 -1)*R1**2 + R2**2) 
            utauc = utauc + thk(l,ivc)/epa*ds
            cosgam = (ds**2 + R2**2 - R1**2)/(2.*ds*R2)
            if (cosgam.gt.1.0) cosgam = 1.
            alfa = pi - acos(cosgam)
            R1 = R2
          enddo
         fi = dfi/2.
         trans = exp(-utauc)
         do j=1,nfi 
            sindec = sin(lat*rad)*sin(teta) - 
     &               cos(lat*rad)*cos(teta)*cos(pi+fi)
         dec = asin(sindec)
         deno = cos(pi+fi)*sin(lat*rad) + tan(teta)*cos(lat*rad)
         tanhl = sin(pi+fi)/deno
         hl = atan(tanhl)
         if (tanhl.lt.0) hl = pi - abs(atan(tanhl))
         if (fi.ge.pi) then
            hl = -hl
         else
            hl = pi - hl
         endif
         hl = long*rad - hl
         if (hl.gt.pi) then
            diff = hl - pi
            hl = diff - pi
         endif
         hl = hl/rad
         dec = dec/rad
    !     write(35,*) teta/rad,fi/rad,dec,hl

           call coord_galac(dec,hl,heure,imois,iannee,ijour,
     &                      0.,lg,bg)
           call fond_galac(lg,bg,iv,lat_pion,lon_pion,val_pion,
     &                      freq_pion,spect_pion,fnd_gal)
 !          call coord_eclip(lg,bg,longec,bet)
 !          call zodiacal_light(longec,bet,iv,lat_eclip,lon_eclip,
 !    &                          val_zodiac,zodial)
            ecl_gal = ecl_gal + 
     &               (fnd_gal+zodial)*sin(teta)*cos(teta)*trans
            fi =fi + dfi
         enddo
         teta =teta+ dteta
      enddo
      ecl_gal = ecl_gal*dteta*dfi
      end
c--------------------------------------------------------------------------      

      subroutine zodiacal_light(lg,bet,iv,lat_eclip,lon_eclip,
     &                          val_zodiac,zodial)
      
c     Routine de calcul de la lumi�re zodiacale
c     Bas�es sur les donn�es de Leinert (1997) et al 
c       Mod�lisation P. Simoneau

      real (kind=8) bet,lg,sinb
      real  lat_eclip(10),lon_eclip(19),val_zodiac(19,10)
      real  moy,mic,fnd_gal
      real beta_NGP
      
      ! recherche de l'indice de la longitude �cliptique
      do i=2,19
         if ((lg.ge.lon_eclip(i-1)).and.(lg.le.lon_eclip(i))) then
            moy = (lon_eclip(i-1)+lon_eclip(i))/2
            if (lg.lt.moy) then
               ilon = i-1
            else
               ilon = i
            endif
         endif
      enddo
      if (lg.gt.350) ilon=19
      
      ! recherche de l'indice de la latitude �cliptique
      do i=2,10
         if ((bet.ge.lat_eclip(i-1)).and.(bet.le.lat_eclip(i))) then
            moy = (lat_eclip(i-1)+lat_eclip(i))/2
            if (bet.lt.moy) then
               ilat = i-1
            else
               ilat = i
            endif
         endif
      enddo
      if (bet.gt.80) ilat=10
      if (bg.lt.-80) ilat=10
      
      ! lumi�re zodiacale en 10-8 W/m2/sr/�m
      zodial = val_zodiac(ilon,ilat)
      ! Corrections spectrales (donn��es Mattila
   !   mic = 1e4/float(iv)
   !   ilatp = ilat - 12
   !   if (bg.le.0) ilatp = 14 - ilat
   !   cor = 0.
   !   do i=2,1526
   !      if ((mic.ge.freq_pion(i-1)).and.(mic.le.freq_pion(i))) then
   !         moy = (freq_pion(i-1) + freq_pion(i))/2
   !         if (mic.lt.moy) then
   !            ifreq = i-1
   !         else
   !            ifreq = i
   !         endif
   !         cor = spect_pion(ifreq,ilatp)
   !      endif
   !   enddo
   !   fnd_gal = fnd_gal*cor
   !   fnd_gal = fnd_gal*mic/float(iv)  ! passage en cm-1
      
      end
 !--------------------------------------------------------------------------   
      subroutine coord_eclip(lg,bg,longec,bet)
      
c     Routine de calcul des coordonn�es �cliptiques � partir des 
c     coordonn�es galactiques
c       Mod�lisation P. Simoneau
      
      real (kind=8) lg,bg,longec,bet
      real *8 beta_NGP,l1,lambda0
      real *8 sinbet,coslml0,sinlml0,rad
      
      pi = dacos(-1.d0)
      RAD=PI/180.0
            
      beta_NGP = 29.81   ! North Galactic Pole
      l1 = 6.38
      lambda0 = 270.02
 
 
 
 
      sinbet = dsin(bg*rad)*dsin(beta_NGP*rad) + 
     &         dcos(bg*rad)*dcos(beta_NGP*rad)*dsin((lg-l1)*rad)
      bet = dasin(sinbet)/rad
     
      coslml0 = dcos((lg-l1)*rad)*dcos(bg*rad)/dcos(bet*rad)
      sinlml0 = (dcos(bg*rad)*dsin(beta_NGP*rad)*dsin((lg-l1)*rad)
     & - dsin(bg*rad)*dcos(beta_NGP*rad))/dcos(bet*rad)
      longec =  datan2(sinlml0,coslml0)/rad + lambda0     
      if (longec.gt.360) longec = longec - 360.0
    
      
      end
      
 !--------------------------------------------------------------------------  
      
      subroutine intensite_OH(imois_obs,lat_obs,long_obs,spect_OH,n_OH,
     &           lat_OH,long_OH,isais,heure)
      
      character * 60 spect_OH,nom(50)
      integer     indice(50)
      real        lat_select(50),long_select(50)
      real        lat(50),long(50),lat_obs,long_obs,lat_OH,long_OH
      
      open(2,file='DATA/liste_donnees_OH.txt')
      read(2,*) n_donnees_OH
      do i=1,n_donnees_OH
         read(2,*) lat(i),long(i),nom(i)
         lat_select(i) = abs(lat_obs - lat(i))
         long_select(i) = abs(long_obs - long(i))       
      enddo
      close(2)
      call classement(n_donnees_OH,lat_select,indice)
      ii = indice(n_donnees_OH)
      im1 = indice(n_donnees_OH - 1)
      
! test si autres donn�es sur une m�me bande de latitude de 20�
      if (lat_select(im1).le.20) then
         long_OH = min(long_select(ii),long_select(im1))
         if (long_OH.eq.long_select(im1)) ii = im1
      endif
! choix de la saison
      if (imois_obs.le.3) then
         isais = 0
      elseif ((imois_obs.gt.3).and.(imois_obs.le.6)) then
         isais = 1
      elseif ((imois_obs.gt.6).and.(imois_obs.le.9)) then
         isais = 2
      else
         isais = 3
      endif
! choix de la p�riode de la nuit
      HL = Heure -(long_obs/15.)
      iheure = 0
      if (HL.ge.24) HL = HL - 24.
      if ((HL.gt.15).and.(HL.le.22)) then
         iheure = 0
      elseif ((HL.gt.22).and.(HL.le.24)) then
         iheure = 1
      elseif ((HL.ge.0).and.(HL.le.2)) then
         iheure = 1
      elseif ((HL.gt.2).and.(HL.le.10)) then
         iheure = 2
      endif
      
! on se positionne au d�but de la zone de texte correspondant � la s�lection
      nl = 3 + n_donnees_OH + 13*(ii-1) + isais*3 + iheure
      open(2,file='DATA/liste_donnees_OH.txt')
      do i=1,nl
         read(2,*) spect_OH
      enddo
      close(2)
      lat_OH = lat(ii)
      long_OH = long(ii)
      do i=1,60
         if (spect_OH(i:i).ne.' ') n_OH = i
      enddo
         
      end
!--------------------------------------------------------------------------    
    
      subroutine classement(ntot,x,indice)
! classement des valeurs de x par ordre décroissant
! sauvegarde de la position initiale dans indice
! Attention le tableau x est détruit en sortie

      Implicit None
      real x(50),temp
      integer indice(50)
      integer::i,itemp,itest,ntot

      Do i=1,ntot
       indice(i) = i
      Enddo

      Do
        itest = 0
        Do i=2,ntot
          If( x(i) > x(i-1) ) Then
           
            itest = 1

            temp = x(i-1)
            x(i-1) = x(i)
            x(i) = temp

            itemp = indice(i-1)
            indice(i-1) = indice(i)
            indice(i) = itemp
          Endif
        Enddo
        If( itest == 0 ) exit
      Enddo

      end

