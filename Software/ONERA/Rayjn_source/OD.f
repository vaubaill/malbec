       subroutine Ord_Discretes(nda,na1u,am1u,teta0,NFI,FI,NLN,THK,OMG,
     &            NANG,ANG,PHSF,GALB,FSOL,glow_OH,glow_OI,glow_Na,ATMP,
     &            TSOL,sigma,dv,AJ,ep_gldm_OH,ep_gldm_OI,ep_gldm_Na,
     &            flux_sol,ialt_sol,prop_sol,flux_som,iazim)

      include 'precision.f'

C==============================================================
C    Modele de transfert radiatif prenant en compte l emission
C    du sol ou de l atmosphere pour chaque sous couche
C    
C==============================================================
C--- HISTORY  ==
C==============
C   90.02.1         :  CREATED
C   97.03.25	    :  Modification de CPCON (precision exposants puissances) 
C		       (Annulation des erreurs IEEE overflow, underflow)
c   20 octobre 1998 :  Transformation pour le programme MATISSE 
c                      Parameter passe dans un Include : parameter.rayjn
c   30 aout 99      :  la fonction source de diffusion multiple AJ est pass�e en param�tre dans matisse.f
c
C   fin 2000        :  modifications pour utiliser la nouvelle version RTRN21,
C                      version pouvant prendre en compte des fonds de mer.
C                      modifications pour travailler en simple/double precision
C                      en modifiant seulement le contenu du fichier precision.f
C                      ajout� par un Include � la plupart des sous-programmes.
C                      modifications pour travailler avec Fortran 90, notamment
C                      dans CPCON + am�nagements dans certains appels � des fonctions
C                      intrins�que (sign, max, alog) pour �viter des probl�mes
C                      avec des constantes ou variables simple/double pr�cision.
c
c   d�but 2002      :  version finalis�e pour MATISSE1.1
c                      1/ calcul des fct sources aux angles utilisateur pour avoir les angles 0 et 180�
c                      2/ correction � la fonction source pour la troncature
c
c   fin 2003        :  corrections de bugs
c                      nettoyage du code     
c
c     Modifications f�vrier 2017 : prise en compte OI, Na          
c
c
C------------------------------------------------------------
C   VARIABLES A MODIFIER ===== TAILLES DES TABLEAUX ANGULAIRES
C------------------------------------------------------------
C Changement dans tout le prog  :
C--------------------------
c
c   KNA1U = nb max de directions zenitales d'observation dans la sphere : doit etre egal a 2*KNA1
c   KNA1 = .........;
c   KNA0 = nb max de directions solaires
c   KNDM = nb max de points de quadrature dans la demi-sphere
c   KNFI = nb max de directions azimutales d'observation         
c   KNLN = nb max de couches atmospherique                       
c   KNTAU = nb max d'epaisseurs optiques le long de la ligne de visee = KNLN + 2
c   KPLK1 = nb max du degre du polynome en tau pour l'approximation lineaire en tau de la fct de Planck                     
c   KNANG = nb max d'angle de diffusion de la fct de phase
c   KLGN1 = nb max de developpement de g : doit etre egal a 2*KNDM
c   KNSB = KNLM + 1                             
c   KNLT = KNLM + 1                             
c      
C------------------------------------------------------------
C 90.11.21      READ NLGN1(L) AS DATA, AND CHANGED RELATED CODINGS

      include 'parameter.rayjn'
C
      PARAMETER (KLGN1=2*KNDM)
      PARAMETER (KNLN1=KNLN+1)
      PARAMETER (PI=3.141592653,RAD=PI/180.0)

C-------------------------------------------------------------------
C AREAS FOR RTRN21.SBR
C-------------------------------------------------------------------
      CHARACTER ERR*64
      DIMENSION AM1U(KNA1U),AM0(KNA0),FI(KNFI),THK(KNLN),OMG(KNLN)
     &,NLGN1(KNLN),G(KLGN1,KNLN),ANG(KNANG),PHSF(KNANG,KNLN)
     &,CPLK(KPLK1,KNLN),UTAU(KNTAU),FLXD(KNA0,KNTAU)
     &,FLXU(KNA0,KNTAU),AI(KNA1U,KNA0,KNFI,KNTAU)
     &,AJ(KNA1U,KNFI,KNTAU),FF(KNLN)
C-------------------------------------------------------------------
C WORK AREAS
C-------------------------------------------------------------------
      CHARACTER HEAD*64
      DIMENSION ATMP(KPLK1,KNLN),ADPT(KPLK1,KNLN)
     &,DPTB(KNLN),B(KPLK1),WK1(KPLK1),WK2(KPLK1,KPLK1),CPL(KPLK1)
      Real(kind=8) Bglow(KPLK1),CGLO(KPLK1,KNLN)
      Real(kind=8) ep_gldm_OH(laythr),glow_OH(laythr)
      Real(kind=8) ep_gldm_OI(laythr),glow_OI(laythr)
      Real(kind=8) ep_gldm_Na(laythr),glow_Na(laythr)
            
      REAL(KIND=8) :: dv,flux_sol,flux_som

C *** Ajout pour impression des param�tres
      SAVE Init0
      DATA Init0/1/

      CHARACTER FMT1*4,FMT2*8,FMT3*8,FMT4*6,FMT5*8,FMT6*12
      DATA FMT1,FMT2,FMT3/"(A)",  "(A,I4,A)","(A,I8,A)"/
      DATA FMT4,FMT5,FMT6/"(A/A)","(13X,A)", "(A,G10.3,A)"/

      initck = 0   ! cette option est utilis�e pour les CK uniquement  
      NOUT = 6
      isot = 1
      open(6,file='Ord_disc.out')


C-----------------------------------------------------------------------
C     Proc�dure de diminution du nombre de couche en fonction 
C     de la profondeur optique.
C     Pas pour la premi�re valeur de CK, car permet de calculer les
C     G pour toutes les couches
C-----------------------------------------------------------------------

	    !!!!! on neutralise cette option pour les calculs Nightglow
	            !!!!! car la couche �missive est � haute altitude !!!
       lmin = 0
       nln_init = nln
       Fsol_init = Fsol
       !prof_opt = 0.
       !do l=1,nln
       !   prof_opt = prof_opt + thk(l)
       !   if (prof_opt.ge.1e-05) then
	   !      lmin = l - 1
	   !      goto 1
	   !    endif
       !enddo
c1      !nln = nln_init - lmin
       !Fsol = Fsol_init*exp(-prof_opt/cos(teta0*rad))
       !do l=1,nln
       !   THK(l) = THK(l+lmin)
	   !   OMG(l) = OMG(l+lmin)
       !enddo
       
C-------------------------------------------------------------------
C PLANCK FUNCTION FITTING.
C-------------------------------------------------------------------

c  Approximation lineaire en tau : NPLK1 = 2
      NPLK1 = 2
      wvn1 = sigma - 0.5*dv
      wvn2 = sigma + 0.5*dv
      DO L=1,NLN
         ICHKB=0
         ICHKG=0
         DO I=1,NPLK1
            ADPT(1,L) = 0.
            ADPT(NPLK1,L) = 1.
            DPTB(I)=THK(L)*ADPT(I,L)
C  D�calage de ATMP sur l'indice des couches	    
            B(I)=PLKAVG(WVN1,WVN2,ATMP(I,lmin+L),ERR)
! === Attention : PLKmoy calcule Planck int�gr� et non Planck moyen
!                 => on divise par la largeur spectrale dv 
    	      B(I)= B(I)/dv
c            B(I)=PLKAVG(WVN1,WVN2,ATMP(I,L),ERR)

c           ! On ajoute une couche de rayonnement Nightglow en la mod�lisant comme le  
c           ! rayonnement corps noir
            epa = ep_gldm_OH(l)*1000. ! epaisseur km
            Bglow(i) = glow_OH(l)*1.e6*epa/4./pi/thk(l)*2.*pi   ! 2*PI (Nakajima)
            epa = ep_gldm_OI(l)*1000. ! epaisseur km
            Bglow(i) = Bglow(i) + glow_OI(l)*1.e6*epa/4./pi/thk(l)*2.*pi
            epa = ep_gldm_Na(l)*1000. ! epaisseur km
            Bglow(i) = Bglow(i) + glow_Na(l)*1.e6*epa/4./pi/thk(l)*2.*pi
            IF(B(I).GT.0) ICHKB=1
            IF(Bglow(I).GT.0) ICHKG=1
         ENDDO
         IF(ICHKB.GT.0) THEN
c           calcul des coefficients CPL du polynome en tau pour l'approximation 
c           de la fct de Planck par un polynome : B = SUM(i=1,NPLK1) CPL(i)*DPTB(i)**(i-1)
            CALL PLYFT2(NPLK1,NPLK1,DPTB,B,CPL,KPLK1,WK1,WK2,ERR)
            IF(ERR.NE.' ') THEN
               WRITE(NOUT,*) '#ERR : ERROR IN PLANK FUNCTION FITTING'
               WRITE(*,*) '#ERR : ERROR IN PLANK FUNCTION FITTING'
               call exit(1)
            ENDIF
            DO I=1,NPLK1
               CPLK(I,L)=CPL(I)
            ENDDO
         ELSE
            DO I=1,NPLK1
               CPLK(I,L)=0
            ENDDO
         ENDIF
         IF(ICHKG.GT.0) THEN
c           calcul des coefficients CPL du polynome en tau pour l'approximation 
c           du rayonnement nightglow par un polynome : Bglow = SUM(i=1,NPLK1) CPL(i)*DPTB(i)**(i-1)
            CALL PLYFT2(NPLK1,NPLK1,DPTB,Bglow,CPL,KPLK1,WK1,WK2,ERR)
            DO I=1,NPLK1
               CGLO(I,L)=CPL(I)
            ENDDO
         ELSE
            DO I=1,NPLK1
               CGLO(I,L)=0
            ENDDO
         ENDIF
         btest = 0.
         do I=1,NPLK1
            btest = btest + cpl(i)*DPTB(i)**(i-1)
         enddo
      ENDDO          
      IF (Tsol.GT.0) THEN
c           fonction de Planck du sol a la temperature Tsol et d'albedo GALB
         BGND=(1-GALB)*PLKAVG(WVN1,WVN2,Tsol,ERR)
! === Attention : PLKmoy calcule Planck int�gr� et non Planck moyen
!                 => on divise par la largeur spectrale dv
         BGND= BGND/dv
      ELSE
         BGND=0.
      ENDIF
c Appel du sous programme mis au point par NAKAJIMA
c
c    Les variables d'entree sont les suivantes :
c
C INDG       I      -1: No ground surface
C                    0: Lambert surface
C                    1: Ocean surface initialization
C                    2: Ocean surface with no initialization
C                    When INDG>0 and IMTHD>0 then single scattering correction
C                      for ocean surface reflection
      INDG = 0
      
C INDA       I       0: FLUX ONLY.
C                    1: INTENSITY USING -AM1U-.
C                    2: INTENSITY AT GAUSSIAN QUADRATURE POINTS.
C                       -NA1U- AND -AM1U- ARE SET AUTOMATICALLY AS
C                       2*NDA AND (-AMUA, +AMUA).
c
c   ***** Cas Rayjn ****   INDA = 1 : On determine les fonctions sources pour les angles 
c                                       sp�cifi�s par l'utilisateur. Permet de calculer pour 0 et 180�
      INDA = 1
c
C INDT       I       1: INTENSITY/FLUX AT USER DEFINED DEPTH -UTAU-.
C                    2: INTENSITY/FLUX AT SUBLAYER INTERFACES.
C                       -NTAU- AND -UTAU- ARE SET AUTOMATICAALY AS
C                       -NLN1- AND -DPT-.
c
c   ***** Cas Rayjn ****   INDT = 2 : fonctions sources calculees a chaque interface                 
      INDT = 2
c
C INDP       I      -1: GIVE -G- AND USE -G- FOR INTENSITY INTERPOLATION
C                       FROM GAUSSIAN POINTS TO USER POINTS.
C                    0: GIVE -G- AND CONSTRUCT PHASE FUNCTION FOR
C                       INTENSITY INTERPOLATION.
C                    1: GIVE PHASE FUNCTION -PHSF-.
c
c   ***** Cas Rayjn ****   INDP = 1 : fonction de phase aerosols exacte                          
      INDP = 1
c
C IMTHD      I      -1: NT,  0: DMS-METHOD  FOR INTENSITY/FLUX
C                    1: MS,  2:TMS,  3:IMS-METHOD FOR INTENSITY.
c
c   ***** Cas Rayjn ****   IMTHD = 2 : valable pour la plupart des geometries selon P.Chervet
      IMTHD = 2
c
C NDA        I       NUNBER OF NADIR-QUADRATURE ANGLES IN THE
C                    HEMISPHERE.
C
c   ***** Cas Rayjn ****   NDA = 4 : nb de flux dans l'hemisphere. 
c                                        
c
C NA1U       I       NUMBER OF EMERGENT NADIR ANGLES IN SPHERE.
c
c   ***** Cas Rayjn ****   NA1U = 1 : angle zenital utilisateur         
c      NA1U pass� en param�tre
c
C AM1U    R(KNA1U)   CONSINE OF THE EMERGENT NADIR ANGLES.
C                      - FOR UPWARD, + FOR DOWNWARD.
c
c   ***** Cas Rayjn ****   AM1U = cosinus de l'angle zenital d'observation : donnee issue du programme Rayjn
C      AM1U(1) = cos(teta1*rad)  
c
C NA0        I       NUMBER OF SOLAR INCIDENCES.
c
c   ***** Cas Rayjn ****   NA0 = 1 : une seule incidence solaire ou lunaire
      NA0 = 1    
c
C AM0     R(NA0)     CONSINE OF SOLAR ZENITH ANGLES .GT. 0.
c
c   ***** Cas Rayjn ****   AM0 = cosinus de l'angle zenital solaire : donnee issue du programme Rayjn
      AM0(1) = cos(teta0*rad)     
c
C NFI        I       NUMBER OF AZIMUTHAL ANGLES.
c
c   ***** Cas Rayjn ****   NFI = 1 : nb d'angle azimutal 
c     NFI = passe en parametre
c
C FI     R(KNFI)     AZIMUTHAL ANGLES IN DEGREES.
c
c   ***** Cas Rayjn ****   FI = difference angle azimutal d'observation angle azimutal solaire 
c                               : donnee issue du programme MATISSE 
c     FI(1) = passe en parametre
c
C NLN        I       NUMBER OF ATMOSPHERIC SUBLAYERS.
c
c   ***** Cas Rayjn ****  NLN = nb de couche atmospheriques dans MATISSE : donnee issue du programme MATISSE
c     NLN = passe en parametre
c
C THK     R(KNLN)    OPTICAL THICKNESS OF SUBLAYERS FROM TOP TO BOTTOM.
c
c   ***** Cas Rayjn ****  THK = epaisseur optique de chaque couche atmospherique 

c     THK(nln) = passe en parametre
c
C OMG     R(KNLN)    SINGLE SCATTERING ALBEDO.
c
c   ***** Cas Rayjn ****   OMG = albedo de simple diffusion : une valeur par couche et par longueur d'onde
c                                  calculee dans le programme de transmission
c     OMG = passe en parametre   
c
C NLGN1   I(KNLN)    MAXIMUM ORDER OF MOMENTS + 1.
C                      GIVE WHEN -INDP- .LE. 0.
C                      GIVE WHEN IMTHD=3 REGARDLESS OF -INDP-.
C                      OTHERWISE, A VALUE .LE. 2*NDA+1 IS GIVEN
C                      AUTOMATICALLY BY THE ROUTINE.
c
c   ***** Cas Rayjn ****   NLGN1 : nombre max par couche de coefficients de Legendre - calcule automatiquement
c                                    dans le cas d'une fct de phase exacte
c     NLGN1(*) = 80
      NLGN1 = KLGN1 ! En F90, initialise tout le tableau NLGN1 � KLGN1
c
C G   R(KLGN1,KNLN)  LEGENDRE MOMENTS OF PHASE FUNCTION.
C                      GIVE WHEN INDP .LE. 0.
c
c   ***** Cas Rayjn ****   G : moments de Legendre , i.e., gi = betai/2i+1 
c                                avec P(cos(teta)) = SUM(i=1,NLGN1) betai * Pi(cos(TETA))
c     G(i,l) = 
c
C NANG       I       NUMBER OF SCATTERING ANGLES FOR PHASE FUNCTIONS.
C                      GIVE WHEN INDP=1.
C                      GIVE WHEN INDP=0 .AND. (INDA.GE.1) .AND.
C                                             (IMTHD.GE.1).
c
c   ***** Cas Rayjn ****   NANG = valeur lue dans le fichier des valeurs de la fct de phase
c     NANG = passe en parametre
c
C ANG     R(KNANG)   SCATTERING ANGLES IN DEGREES.
C                      GIVE WHEN INDP=1.
C                      GIVE WHEN INDP=0 .AND. (INDA.GE.1) .AND.
C                                             (IMTHD.GE.1).
C                      ANGLES SHOULD HAVE ENOUGH RESOLUTION FOR LEGENDRE
C                      EXPANSION (INDP=1) AND INTERPOLATION BY CUBIC
C                      POLYNOMIAL.
c
c   ***** Cas Rayjn ****    ANG = valeurs lues dans le fichier des valeurs de la fct de phase
c     ANG = passe en parametre
c
C PHSF    R(KNANG,   PHASE FUNCTION. GIVE WHEN INDP=1.
c
c   ***** Cas Rayjn ****   PHSF = valeurs de la fonction de phase pour une couche et une longueur d'onde : valeurs
c                                   lues dans le fichier des fonctions de phase
c     PHSF = passe en parametre  
c
C EPSP       R       TRUNCATION CRITERION OF PHASE FUNCTION MOMENTS.
c
c   ***** Cas Rayjn ****   EPSP = troncature de la decomposition des g : valeur varie de 0.001 a 0.00001 d'apres P. Chervet
c                                   faire un etest pour differents cas d'absorption
      EPSP = 0.0001
c
C EPSU       R       CONVERGENCE CRITERION OF INTENSITY (INDA.GT.0)
c
c   ***** Cas Rayjn ****   EPSU = 0. d'apres P. Chervet
      EPSU = 0. 
c
C GALB       R       GROUND ALBEDO (LAMBERTIAN).
c
c   ***** Cas Rayjn ****   GALB = valeur de l'albedo du sol : depend du type de sol et de la longueur d'onde 
c                                   valeur actuellement lue dans un fichier
c     GALB = passe en parametre
c
C FSOL       R       SOLAR IRRADIANCE AT THE SYSTEM TOP.
c
c   ***** Cas Rayjn ****   FSOL = flux solaire spectral au sommet de l'atmosphere = valeur calculee dans le programme 
c                                   de transmission
c     FSOL = passe en parametre
c
C NPLK1      I       NUMBER OF ORDER TO APPROXIMATE PLANK + 1.
C                      IF 0 THEN NO THERMAL.
c
c   ***** Cas Rayjn ****   NPLK1 = nb de temperatures definissant le gradient dans une couche l. Permet d'utiliser  
c                                    l'approximation en tau pour la fonction de Plank. Dans MATISSE, on utilisera les 
c                                    temperature au sommet et a la base de la couche.
c     NPLK1 = 2 : defini au debut du sous programme pour le calcul des coefficients CPLK                  
c
C CPLK R(KPLK1,KNLN) THERMAL EMISSION
C                      = SUM(K1=1,NPLK1) CPLK(K1,L) * TAU**(K1-1)
C                      TAU IS THE OPTICAL DEPTH MEASURED FROM THE TOP
C                      OF THE SUBLAYER (SAME UNIT AS FSOL).
c
c   ***** Cas Rayjn ****   CPKL = coef de la decomposition de la fct de Planck : calcules avec le sous programme PLYFT2
c     CPLK =  calcul� dans diffusion et pass� en parametre vers RTRN1 (ou 21)
c
C BGND       R       THERMAL EMISSION FROM THE GROUND
C                      (SAME UNIT AS FSOL).
c
c   ***** Cas Rayjn ****   BGND = fct de Planck calcule pour la temperature du sol 
c     BGND = (1. - GALB)*PLANCK(IV,Tsol)
c
C NTAU       I       NUMBER OF USER DEFINED OPTICAL DEPTHS.
c
c   ***** Cas Rayjn ****   NTAU = nb d'epaisseurs optiques auxquelles la luminance est determinee.
c     NTAU =  passe en parametre
c
C UTAU    R(KNTAU)   OPTICAL DEPTHS WHERE THE FIELD IS CALCULTED.
C                      TOP TO BOTTOM.
c
c   ***** Cas Rayjn ****   UTAU = valeurs des epaisseur optiques ou les luminances diffuses sont disponubles : donnees
c                                   issues du programme MATISSE : UTAU(1) = sommet de l'atmosphere
c                                                                 UTAU(NTAU) = un point entre le sommet et la base de 
c                                                                              l'atmosphere
c     UTAU = passe en parametre
c
C SCR       R    Relative refractive index of the media
C               About 1.33 for atmosphere to ocean incidence,
C               and 1/1.33 for ocean to atmosphere incidence.
C SCI       R    Relative refractive index for imaginary part
C               M = CR + I*CI

C Eps_src : Epsilon de test de convergence dans les calculs de fonction source
C           Int�gration sur Phi prime par trap�zes.
C           0.01 <=> mieux que 1 % d'une it�ration � la suivante
C           valeurs typiques 0.01 � 0.00001  
C           Influence importante sur le temps de calcul pour 1 longueur d'onde
C           ex : 0.005   => 0.80 s (temps mesur�s sur la machine aaricia)
C                0.0005  => 1.64 s
C                0.00005 => 3.1  s
C                0.000005=> 14.4 s (en fait 11 si 6 it�rations au maximum)

      Eps_src = 0.005  ! pass� en param�tre pour tests.

      SCR = 0.   ! ???
      SCI = 0.
      
C--- OUTPUT
C NA1U, AM1U         SAME AS 2*NDA, (-AUMA, +AMUA) WHEN INDA=2.
C NLGN1, G           NORMALIZED BY G(1), AND CUT OFF BY THE CRITERION
C                     -EPSP-.
C NTAU, UTAU         SAME AS NLN+1 AND -DPT- WHEN INDT=1.
C PHSF               NORMALIZED PHASE FUNCTION (1 OVER UNIT SPHERE).
C                    RECONSTRUCTED WHEN (INDA.GE.1) .AND. (IMTHD.GE.1)
C                    .AND. (INDP=0).
C FLXD    R(KNA0,    DOWNWARD FLUX AT -UTAU-.
C           KNTAU)
C FLXU               UPWARD   FLUX AT -UTAU-.
C AI      R(KNA1U,   I(MU1U(I), MU0(J), FI(K), UTAU(IT))
C           KNA0,    INTENSITY AT -UTAU- (WATT/M2/MICRON).
C           KNFI,
C           KNTAU)
C ERR      C*64      ERROR INDICATER. IF " " THEN NORMAL END.

C-------------------------------------------------------------------
C TRANSFER
C-------------------------------------------------------------------
c *** INDG, SCR et SCX Nouveaux / RTRN1 : a initialiser ?

C *** Impressions des caract�ristiques d'entr�e de RTRN21

      IF (Init0.GT.0) then
         write(*,*) '#INFO : '
         write(*,*) '#INFO : NOMBRE D''ANGLES DE QUADRATURE DANS UN HEMI
     &SPHERE : ',NDA
         write(*,*) '#INFO : '
      endif

      idebug = 1
      IF ((Init0.GT.0).and.(idebug.eq.1)) then
c        write(*,*) '#INFO : '
c        write(*,*) '#INFO : Caractéristiques des calculs pour la',
c     &' diffusion'
c        write(*,*) '#INFO : '

         Select Case (INDG)
           CASE (-1)
             write(6,FMT1) "INDG =  -1 : No ground surface"
           CASE (0)
             write(6,FMT1) "INDG =   0 : Lambert surface"
           CASE (1)
             write(6,FMT1) "INDG =   1 : Ocean surface initialization"
           CASE (2)
             write(6,FMT1) 
     &              "INDG =   2 : Ocean surface with no initialization"
           CASE DEFAULT
             write(6,FMT3) "INDG = ",INDG," : Valeur incorrecte"
         END Select

         Select Case (INDA)
           CASE (0)
             write(6,FMT1) "INDA =   0 : FLUX ONLY."
           CASE (1)
             write(6,FMT1) "INDA =   1 : INTENSITY USING -AM1U-."
           CASE (2)
             write(6,'(A/A/A)') 
     &        "INDA =   2 : INTENSITY AT GAUSSIAN QUADRATURE POINTS.",
     &        "             -NA1U- AND -AM1U- ARE SET AUTOMATICALLY AS",
     &        "             2*NDA AND (-AMUA, +AMUA)."
           CASE DEFAULT
             write(6,FMT3) "INDA = ",INDA," : Valeur incorrecte"
         END Select

         Select Case (INDT)
           CASE (1)
             write(6,FMT1) 
     &       "INDT =   1 : INTENSITY/FLUX AT USER DEFINED DEPTH -UTAU-."
           CASE (2)
             Write(6,'(A/A/A/A,A/)') 
     &       "INDT =   2 : INTENSITY/FLUX AT SUBLAYER INTERFACES.",
     &       "             -NTAU- AND -UTAU- ARE SET AUTOMATICALLY AS",
     &       "             -NLN1- AND -DPT-.",
     &       "    Cas Matisse : fonctions sources calculées à chaque",
     &       " interface "
           CASE DEFAULT
             Write(6,FMT3) "INDT = ",INDT," : Valeur incorrecte"
         END Select

         Select Case (INDP)
           CASE (-1)
             Write(6,FMT4) 
     &   "INDP =  -1 : GIVE -G- AND USE -G- FOR INTENSITY INTERPOLATION"
     &     ,"            FROM GAUSSIAN POINTS TO USER POINTS."
           CASE (0)
             Write(6,FMT4) 
     &         "INDP =   0 : GIVE -G- AND CONSTRUCT PHASE FUNCTION FOR",
     &         "            INTENSITY INTERPOLATION.."
           CASE (1)
             Write(6,FMT4) "INDP =   1 : GIVE PHASE FUNCTION -PHSF-",
     &         "    Cas Matisse : fonction de phase a�rosols exacte"
           CASE DEFAULT
             Write(6,FMT3) "INDP = ",INDP," : Valeur incorrecte"
         END Select

         Write(6,FMT1) " "
         Write(6,FMT1) "M�thode de Calcul :"

         Select Case (IMTHD)
           CASE (-1)
             Write(6,FMT1) "IMTHD=  -1 : NT (Pas de troncature)"
           CASE (0)
             Write(6,FMT1) "IMTHD=   0 : DMS-METHOD FOR INTENSITY/FLUX"
           CASE (1)
             Write(6,FMT1) "IMTHD=   1 : MS"
           CASE (2)
             Write(6,FMT1) "IMTHD=   2 : TMS ; Cas Matisse en Standard"
           CASE (3)
             Write(6,FMT1) "IMTHD=   3 : IMS-METHOD FOR INTENSITY."
           CASE DEFAULT
             Write(6,FMT3) "IMTHD= ",IMTHD," : Valeur incorrecte"
         END Select

         Write(6,FMT1) " "
         Write(6,FMT2) "NDA  =",NDA,
     &      " : NUMBER OF NADIR-QUADRATURE ANGLES IN THE HEMISPHERE."
         Write(6,FMT2) "NA1U =",NA1U,
     &      " : NUMBER OF EMERGENT NADIR ANGLES IN SPHERE."
         Write(6,FMT1) " "
         Write(6,FMT2) "NA0  =",NA0,
     &      " : NUMBER OF SOLAR INCIDENCES."
         Write(6,FMT1) "   i    AM0    Angle(�)"
         DO i=1,NA0
            Write(6,'(I4,F9.5,F9.3)') i,AM0(i),ACOS(AM0(i))/RAD
         Enddo

         Write(6,FMT1) " "
         Write(6,FMT2) "NFI  =",NFI,
     &      " : NUMBER OF AZIMUTHAL ANGLES."
         Write(6,FMT1) "   i  Angle(�)"
         DO i=1,NFI
            Write(6,'(I4,F9.3)') i,FI(i)
         Enddo
         Write(6,FMT1) " "
         Write(6,FMT2) "NLN  =",NLN,
     &      " : NUMBER OF ATMOSPHERIC SUBLAYERS."
         Write(6,FMT2) "NANG =",NANG,
     &      " : NUMBER OF SCATTERING ANGLES FOR PHASE FUNCTIONS."
         Write(6,FMT1) "Valeurs des angles (�) :"
         Write(6,'(10F8.2)') (ANG(i),i=1,NANG)

         Write(6,FMT1) " "
         Write(6,FMT6) "EPSP =",EPSP,
     &      " : TRUNCATION CRITERION OF PHASE FUNCTION MOMENTS."
         Write(6,FMT6) "EPSU =",EPSU,
     &      " : CONVERGENCE CRITERION OF INTENSITY (INDA.GT.0)"

         Write(6,FMT1) " "
         Write(6,FMT2) "NPLK1=",NPLK1,
     &      " : NUMBER OF ORDER TO APPROXIMATE PLANK + 1."
         Write(6,FMT5)   "IF 0 THEN NO THERMAL."
         Write(6,FMT2) "NTAU =",NTAU,
     &      " : NUMBER OF USER DEFINED OPTICAL DEPTHS."
 
         Write(6,FMT1) " "
         Write(6,FMT6) "Eps_src=",Eps_src,
     &      " : Crit�re de convergence/Nombre d'it�rations"
         Write(6,'(21X,A)') "(Rtrn21_New/Old)"

         Write(6,FMT1) " "
         Write(6,FMT6) "SCR  =",SCR,
     &   " : Relative refractive index of the media"
C            About 1.33 for atmosphere to ocean incidence,
C            and 1/1.33 for ocean to atmosphere incidence.
         Write(6,FMT6) "SCI  =",SCI,
     &   " : Relative refractive index for imaginary part"
C            M = CR + I*CI

      Endif

         CALL RTRN21(INDG,INDA,INDT,INDP,IMTHD,NDA,NA1U,AM1U,NA0,AM0,
     &    NFI,FI,NLN,THK,OMG,NLGN1,G,NANG,ANG,PHSF,EPSP,EPSU,GALB,FSOL,
     &    NPLK1,CPLK,BGND,NTAU,UTAU,SCR,SCI,FLXD,FLXU,AI,aj,ERR,ff,
     &    Eps_src,sigma,isot,initck,lmin,nln_init,CGLO,iazim)

          flux_sol = FLXD(1,ialt_sol) + 
     &               prop_sol*(FLXD(1,ialt_sol-1) - FLXD(1,ialt_sol))
          flux_som = FLXU(1,1) ! flux montant au sommet de l'atmosph�re

C *** Impressions des caract�ristiques en sortie de RTRN21

      IF ((Init0.GT.0).and.(idebug.eq.1)) then
         Write(6,'(/A/A/A/)')
     &    "**********************************************************",
     &    "*   Caract�ristiques des calculs en sortie de RTRNxx     *",
     &    "**********************************************************"

         Select Case (INDG)
           CASE (-1)
             Write(6,FMT1) "INDG =  -1 : No ground surface"
           CASE (0)
             Write(6,FMT1) "INDG =   0 : Lambert surface"
           CASE (1)
             Write(6,FMT1) "INDG =   1 : Ocean surface initialization"
           CASE (2)
             Write(6,FMT1) 
     &              "INDG =   2 : Ocean surface with no initialization"
           CASE DEFAULT
             Write(6,FMT3) "INDG = ",INDG," : Valeur incorrecte"
         END Select

         Write(6,FMT2) "NA1U =",NA1U,
     &      " : NUMBER OF EMERGENT NADIR ANGLES IN SPHERE."
         Write(6,FMT1) "AM1U : COSINE OF THE EMERGENT NADIR ANGLES."
         Write(6,FMT1) "       - FOR UPWARD, + FOR DOWNWARD."
         Write(6,FMT4) "Angle de Z�nith Standard (  0=Vertical haut)"
     &                  , "                         (180=Vertical bas )"
         Write(6,FMT1) "   i    AM1U      Angles(�)"
         Write(6,FMT1) "              Nakajima Standard"
         DO i=1,NA1U
            AAA = ACOS(AM1U(i))/RAD
            Write(6,'(I4,F9.5,2F9.3)') i,AM1U(i),AAA,180-AAA
         Enddo

         Write(6,FMT1) " "
         Write(6,FMT2) "NTAU =",NTAU,
     &      " : NUMBER OF USER DEFINED OPTICAL DEPTHS."
         Write(6,FMT5)      "SAME AS NLN+1 AND -DPT- WHEN INDT=1."

         Write(6,'(/)')      ! 2 lignes blanches
         
         write(6,*) 'Sigma   AI(ntau)     AI(1)'

      Endif
      IF (Init0.GT.0) Init0 = 0  ! Ne sera fait que lors du 1er passage
       write(6,*) sigma,ai(1,1,1,ntau),ai(1,1,1,1)
c       write(6,*) sigma,ai(1,1,1,1)
c       atau = 0.
c       conv = sigma*sigma/1e8
c       do it=1,nln+1
c          write(6,*) atau
c          do i=1,na1u
c            write(6,*) -am1u(i),AI(i,1,1,it)*conv
c            write(6,*) atau,AI(i,1,1,it)
c	    enddo
c	    write(6,*)
c	    atau = atau + thk(it)	    
c       enddo
      

c--------------------------------------------------------------------------
C R��criture des AI et AJ avec le nombre initial de couches      
      do i=1,na1u
         do k=1,nfi
            do it=nln+1,1,-1
               AJ(i,k,it+lmin) = AJ(i,k,it)
               AI(i,1,k,it+lmin) = AI(i,1,k,it)
            enddo
	    do it=1,lmin
               AJ(i,k,it) = 0.
               AI(i,1,k,it) = 0.
	    enddo   	    
         enddo
      enddo
      nln = nln_init
      Fsol = Fsol_init       
c--------------------------------------------------------------------------


c***********************
c     Retour des erreurs
c***********************
c     WRITE(NOUT,16) HEAD ! HEAD non initialisé !!!
   16 FORMAT(A64)
      IF(ERR.NE.' ') THEN
        WRITE(NOUT,*) '#ERR : FATAL ERROR IN RTRN21 !!!'
        WRITE(NOUT,9) ' #ERR : '//ERR
        WRITE(*,*)    '#ERR : FATAL ERROR IN RTRN21 !!!'
        WRITE(*,9)    ' #ERR : '//ERR
    9   FORMAT(A64)
        STOP
      ENDIF

      END
c
c------------------------------------------------------------------
c
      SUBROUTINE RTRN21(INDG,INDA,INDT,INDP,IMTHD,NDA,NA1U,AM1U,NA0,AM0
     &,NFI,FI,NLN,THK,OMG,NLGN1,G,NANG,ANG,PHSF,EPSP,EPSU,GALB,FSOL
     &,NPLK1,CPLK,BGND,NTAU,UTAU,SCR,SCI,FLXD,FLXU,AI,aj,ERR,ff,Eps_src,
     & sigma,isot,initck,lmin,nln_init,CGLO,iazim)

      include 'precision.f'
     
C Version �quivallente au RTRN1 modifi� par PS.
C + ajout de Eps_src pour compatibilit� d'appel, mais non utilis� ici.
     
C$NAME  RTRN21
C$LINK  CHKRT CONVU FTRN21 INTCR1 LGNDF3 PINT4 PLGD SEARF1
C
C SOLVE THE RADIATIVE TRANSFER IN ATMOSPHERE SYSTEM.
C  -USE FTRN21-
C Complex refractive index for ocean surface
C Useful for microwave region
C BY TERUYUKI NAKAJIMA
C 94.4.4 observation of FTRN21:
C Insufficeient correction of solar single scattering from ocean surface.
C GRND0 should be corrected in the future.
C This version of GRNDO calculates R(I,J) and SR(I,J0) by integrating
C the reflected intensity at ocean surface over the mu-interval
C (mu(I-1/2), mu(I+1/2)).  After solving the transfer with this
C reflection and source matrices of ocean surface, we subtracted
C the singly scattered intensity with SR(I,J0).  After that,
C we add the true single reflected intensity.
C However, this trick will have some error if mu(I)<>mu(J0),
C since the strongly peaked reflected intensity in the direction
C of -mu(J0).  This is distributed among the light along the
C quadrature stream -mu(I).  In more accurate approximation,
C we should treat the peaked reflected intensity as a reflected
C direct radiation not distributed amond the quadrature streams.
C
C 94.9.2  The symmetric operation for R(ocean) in 'OCEANRF'
C can introduce a difference of order about 0.3 % from a version
C without such symmetric operation.
C
C--- HISTORY
C 89.11. 7 CREATED
C 90.11.22 IF(INDP.LE.0 .AND. (INDA.GE.1 .AND. IMTHD.GE.1)) INDPL=1 ->
C                  EQ
C          CHANGE CODES BETWEEN -DO 4 - AND -DO 8 -;
C          IF(ABS(CS1).GT.1.0) CS1=SIGN(1.0,CS1); CHANGE LOOP-13,14
C          ADDING LOOP-47.

C 2000/12  1.0 remplace par one sinon erreur avec f90

C 93. 4. 5 NSF meaning changed, Set NSF=0 after loop-16
C     5. 4 Naming RTRN2 from Higurashi's RTRN with use of FTRN21
C           and corrected single scattering for ocean surface reflection
C    10.19 OK comparing with Hasumi's program.
C 94. 9.27 debug IF(AM1U(IU).LT.0) then A3= ...
C 95. 6. 2 Gerated from RTRN2 with SCR and SCI
C           Useful for microwave region
C 98.1.7
C   u10 > 0.01 for RSTR and PSTR.  u10 < 0.01 will have significant
C   error in radiances due to angular integration errors for surface
C   reflection.  Recommend that u10 is set to be 0.01 for flat surface
C   case
C--- INPUT
C INDG       I      -1: No ground surface
C                    0: Lambert surface
C                    1: Ocean surface initialization
C                    2: Ocean surface with no initialization
C                    When INDG>0 and IMTHD>0 then single scattering correction
C                      for ocean surface reflection
C INDA       I       0: FLUX ONLY.
C                    1: INTENSITY USING -AM1U-.
C                    2: INTENSITY AT GAUSSIAN QUADRATURE POINTS.
C                       -NA1U- AND -AM1U- ARE SET AUTOMATICALLY AS
C                       2*NDA AND (-AMUA, +AMUA).
C INDT       I       1: INTENSITY/FLUX AT USER DEFINED DEPTH -UTAU-.
C                    2: INTENSITY/FLUX AT SUBLAYER INTERFACES.
C                       -NTAU- AND -UTAU- ARE SET AUTOMATICAALY AS
C                       -NLN1- AND -DPT-.
C INDP       I      -1: GIVE -G- AND USE -G- FOR INTENSITY INTERPOLATION
C                       FROM GAUSSIAN POINTS TO USER POINTS.
C                    0: GIVE -G- AND CONSTRUCT PHASE FUNCTION FOR
C                       INTENSITY INTERPOLATION.
C                    1: GIVE PHASE FUNCTION -PHSF-.
C IMTHD      I      -1: NT,  0: DMS-METHOD  FOR INTENSITY/FLUX
C                    1: MS,  2:TMS,  3:IMS-METHOD FOR INTENSITY.
C                    When INDG>0 and IMTHD>0 then single scattering correction
C                      for ocean surface reflection
C NDA        I       NUNBER OF NADIR-QUADRATURE ANGLES IN THE
C                    HEMISPHERE.
C NA1U       I       NUMBER OF EMERGENT NADIR ANGLES IN SPHERE.
C AM1U    R(KNA1U)   CONSINE OF THE EMERGENT NADIR ANGLES.
C                      - FOR UPWARD, + FOR DOWNWARD.
C NA0        I       NUMBER OF SOLAR INCIDENCES.
C AM0     R(NA0)     CONSINE OF SOLAR ZENITH ANGLES .GT. 0.
C NFI        I       NUMBER OF AZIMUTHAL ANGLES.
C FI     R(KNFI)     AZIMUTHAL ANGLES IN DEGREES.
C NLN        I       NUMBER OF ATMOSPHERIC SUBLAYERS.
C THK     R(KNLN)    OPTICAL THICKNESS OF SUBLAYERS FROM TOP TO BOTTOM.
C OMG     R(KNLN)    SINGLE SCATTERING ALBEDO.
C NLGN1   I(KNLN)    MAXIMUM ORDER OF MOMENTS + 1.
C                      GIVE WHEN -INDP- .LE. 0.
C                      GIVE WHEN IMTHD=3 REGARDLESS OF -INDP-.
C                      OTHERWISE, A VALUE .LE. 2*NDA+1 IS GIVEN
C                      AUTOMATICALLY BY THE ROUTINE.
C G   R(KLGN1,KNLN)  LEGENDRE MOMENTS OF PHASE FUNCTION.
C                      GIVE WHEN INDP .LE. 0.
C NANG       I       NUMBER OF SCATTERING ANGLES FOR PHASE FUNCTIONS.
C                      GIVE WHEN INDP=1.
C                      GIVE WHEN INDP=0 .AND. (INDA.GE.1) .AND.
C                                             (IMTHD.GE.1).
C ANG     R(KNANG)   SCATTERING ANGLES IN DEGREES.
C                      GIVE WHEN INDP=1.
C                      GIVE WHEN INDP=0 .AND. (INDA.GE.1) .AND.
C                                             (IMTHD.GE.1).
C                      ANGLES SHOULD HAVE ENOUGH RESOLUTION FOR LEGENDRE
C                      EXPANSION (INDP=1) AND INTERPOLATION BY CUBIC
C                      POLYNOMIAL.
C PHSF    R(KNANG,   PHASE FUNCTION. GIVE WHEN INDP=1.
C EPSP       R       TRUNCATION CRITERION OF PHASE FUNCTION MOMENTS.
C EPSU       R       CONVERGENCE CRITERION OF INTENSITY (INDA.GT.0)
C GALB       R       Ground albedo if INDG=0
C                    U10 (m/sec)   if INDG>0; u10>0.01
C FSOL       R       SOLAR IRRADIANCE AT THE SYSTEM TOP.
C NPLK1      I       NUMBER OF ORDER TO APPROXIMATE PLANK + 1.
C                      IF 0 THEN NO THERMAL.
C CPLK R(KPLK1,KNLN) Plank function
C                      = SUM(K1=1,NPLK1) CPLK(K1,L) * TAU**(K1-1)
C                      TAU IS THE OPTICAL DEPTH MEASURED FROM THE TOP
C                      OF THE SUBLAYER (SAME UNIT AS FSOL).
C BGND       R       (1-GALB)*B when INDG=0
C                    B          when INDG>0
C                    where B is Plank function
C                      (SAME UNIT AS FSOL).
C NTAU       I       NUMBER OF USER DEFINED OPTICAL DEPTHS.
C UTAU    R(KNTAU)   OPTICAL DEPTHS WHERE THE FIELD IS CALCULTED.
C                      TOP TO BOTTOM.
C SCR       R    Relative refractive index of the media
C               About 1.33 for atmosphere to ocean incidence,
C               and 1/1.33 for ocean to atmosphere incidence.
C SCI       R    Relative refractive index for imaginary part
C               M = CR + I*CI

C--- OUTPUT
C INDG               if 1 then 2
C NA1U, AM1U         SAME AS 2*NDA, (-AUMA, +AMUA) WHEN INDA=2.
C NLGN1, G           NORMALIZED BY G(1), AND CUT OFF BY THE CRITERION
C                     -EPSP-.
C NTAU, UTAU         SAME AS NLN+1 AND -DPT- WHEN INDT=1.
C PHSF               NORMALIZED PHASE FUNCTION (1 OVER UNIT SPHERE).
C                    RECONSTRUCTED WHEN (INDA.GE.1) .AND. (IMTHD.GE.1)
C                    .AND. (INDP=0).
C FLXD    R(KNA0,    DOWNWARD FLUX AT -UTAU-.
C           KNTAU)
C FLXU               UPWARD   FLUX AT -UTAU-.
C AI      R(KNA1U,   I(MU1U(I), MU0(J), FI(K), UTAU(IT))
C           KNA0,    INTENSITY AT -UTAU- (WATT/M2/MICRON).
C           KNFI,
C           KNTAU)
C ERR      C*64      ERROR INDICATER. IF " " THEN NORMAL END.
C--- CORRESPONDENCE BETWEEN VARIABLES AND PARAMETERS
C KNA1U       NA1U
C KNA1        NA1
C KNA0        NA0
C KNDM        NDA
C KNFI        NFI
C KNLN        NLN
C KNLN1       KNLN+1
C KNTAU       NTAU
C KLGN1       MXLGN1 = MAX(NLGN1)
C              THIS SHOULD BE VERY LARGE WHEN IMS-METHD IS USED
C              SO AS TO APPROXIMATE P**2, OR WHEN INDP=1 IS ASSIGNED
C              TO RECONSTRUCT THE ORIGINAL PHASE FUNCTION FROM -G-.
C KLGT1       MXLGT1 = SAME AS MXLGN1 BUT FOR THAT OF TRUNCATED
C              VARIABLES
C KPLK1       NPLK1
C$ENDI
C PARAMETERS
C$PRPC
C *** Ajout Matisse
      include 'parameter.rayjn'
C
      PARAMETER (KLGN1=2*KNDM)

c     PARAMETER (KNA1U=8,KNA1=4,KNA0=2,KNDM=8,KNFI=3)
C$PRPC
c     PARAMETER (KLGN1=200,KNLN=34,KNTAU=34,KPLK1=4)
C$PRPC
c     PARAMETER (KNANG=200,NCHK1=3,NCHK2=3)
      PARAMETER (NCHK1=3,NCHK2=3)
C
      PARAMETER (KNLN1=KNLN+1,KLGT1=2*KNDM)
      PARAMETER (PI=3.141592654,RAD=PI/180.0)
C$PSET CHKRT.KNA1U=KNA1U; CHKRT.KNA0=KNA0
C$PSET CHKRT.KNDM=KNDM;   CHKRT.KNFI=KNFI
C$PSET CHKRT.KLGN1=KLGN1; CHKRT.KNLN=KNLN
C$PSET CHKRT.KNTAU=KNTAU; CHKRT.KPLK1=KPLK1; CHKRT.KNANG=KNANG
C AREAS FOR THIS ROUTINE
      CHARACTER ERR*(*)
      DIMENSION AM1U(KNA1U),AM0(KNA0),FI(KNFI),THK(KNLN),OMG(KNLN)
     &,NLGN1(KNLN),G(KLGN1,KNLN),ANG(KNANG),PHSF(KNANG,KNLN)
     &,CPLK(KPLK1,KNLN),UTAU(KNTAU),FLXD(KNA0,KNTAU)
     &,FLXU(KNA0,KNTAU),AI(KNA1U,KNA0,KNFI,KNTAU)
      real(kind=8) CGLO(KPLK1,KNLN)
     
C AREAS FOR FTRN21
C$PSET FTRN21.KNA1U=KNA1U; FTRN21.KNA1=KNA1;   FTRN21.KNA0=KNA0
C$PSET FTRN21.KNDM=KNDM;   FTRN21.KNLN=KNLN
C$PSET FTRN21.KNTAU=KNTAU; FTRN21.KPLK1=KPLK1
      DIMENSION AMUA(KNDM),WA(KNDM),GT(KLGT1,KNLN),NLGT1(KNLN)
     &,CPLKT(KPLK1,KNLN),OMGT(KNLN),THKT(KNLN),UTAUT(KNTAU)
     &,AIF(KNA1U,KNA0,KNTAU)
     
C *** Ajout Matisse
      dimension ajm(KNA1U,KNTAU),aj(KNA1U,KNFI,KNTAU)

C WORKING AREAS
      DIMENSION CS(KNANG),YY(KNANG),GG(KLGN1),COSM(KNFI)
     &,PHS(KNLN),FF(KNLN),DPT(KNLN1),DPTT(KNLN1)
     &,PHST(KNLN),SGL2(KNTAU),PHSB(KNLN),COR(KNTAU)
       DIMENSION IM1U(KNA1U),JM0(KNA0),BM1U(KNA1U),BM0(KNA0)
     &,EU1(KNA1U),EU0(KNA0),IICHK0(KNA0),IICHK1(KNA1U)
     &,NLGN1P(KNLN),GP(KLGN1,KNLN),PHSFP(KNANG,KNLN)

       SAVE GP,PHSFP,NLGN1P
c      ! save ilum
       save INIT0

       data INIT0/1/
c       data ilum /1/ 

       if ((INIT0.GT.0).and.(INDG.GT.0)) INDG=1
       
C CHECK AND SET OF VARIABLES
      CALL CHKRT(INDA,INDT,INDP,IMTHD,NDA,NA1U,AM1U,NA0,AM0
     &,NFI,FI,NLN,THK,OMG,NLGN1,G,NANG,ANG,PHSF,EPSP,EPSU,GALB,FSOL
     &,NPLK1,CPLK,BGND,NTAU,UTAU,AMUA,WA,DPT,MXLGN2,ERR)
      IF(ERR.NE.' ') RETURN
      NLN1=NLN+1
C FOURIER EXPANSION PARAMETERS
      NDA2=2*NDA
      NDA21=NDA2+1
      MXLGT1=NDA2
      IF(MXLGT1.GT.KLGT1) THEN
        ERR='-KLGT1- IS TOO SMALL'
        RETURN
      ENDIF
CC FLUX
      IF(INDA.LE.0) THEN
        IF(IMTHD.LT.0) THEN
          MXLGN1=MIN(NDA2,MXLGN2)
         ELSE
          MXLGN1=MIN(NDA21,MXLGN2)
        ENDIF
CC INTENSITY
       ELSE
        IF(INDP.LE.0) THEN
          MXLGN1=MXLGN2
         ELSE
          IF(IMTHD.LE.-1) THEN
            MXLGN1=NDA2
           ELSE
            IF(IMTHD.LE.2) THEN
              MXLGN1=NDA21
             ELSE
              MXLGN1=MXLGN2
            ENDIF
          ENDIF
        ENDIF
      ENDIF
      IF(MXLGN1.GT.KLGN1) THEN
        ERR='-KLGN1- IS TOO SMALL'
        RETURN
      ENDIF
C INDICATER FOR RE-CALCULATING PHASE FUNCTION USING -G-
      INDPL=0
      IF(INDP.EQ.0 .AND. (INDA.GE.1 .AND. IMTHD.GE.1)) INDPL=1
C CLEAR VARIABLES
      IF(INDA.GE.1) THEN
        DO IT=1,NTAU
           DO K=1,NFI
              DO I=1,NA1U
                 AJ(i,k,it) = 0.
                 DO J=1,NA0
                    AI(I,J,K,IT)=0
                 enddo
              enddo
           enddo
        enddo
      ENDIF
C SET COS(SCATTERING ANGLE)
      IF((INDP.EQ.1) .OR. (INDPL.GE.1)) THEN
        DO 2 I=1,NANG
    2   CS(I)=COS(ANG(I)*RAD)
      ENDIF
C LOOP FOR SUBLAYERS
      wf0 = 0.
c     if (initck.eq.0) then 
         DO 3 L=1,nln_init
CC LEGENDRE EXPANSION OF THE PHASE FUNCTIONS
CC  WE TRUNCATE THE SERIES WHEN ABS(G) BECOMES SMALLER THAN -EPSP-
CC   FOR SUCCESSIVE THREE TIMES.
           IF(INDP.EQ.1) THEN
             DO 4 I=1,NANG
    4        YY(I)=PHSF(I,L)
             CALL LGNDF3(MXLGN1,NANG,CS,YY,GG)
             GG0=GG(1)
             ICHK=0
             DO 5 K1=1,MXLGN1
         !       write(*,*) GG(K1)*(2*(k1-1)+1)/gg0
                G(K1,L)=GG(K1)/GG0
        !        write(93,*) k1,l,G(K1,L)
                IF(ABS(G(K1,L)).LE.EPSP) THEN
                   ICHK=ICHK+1
                   IF(ICHK.GE.NCHK1) GOTO 6
                ENDIF
    5        CONTINUE
             K1=MXLGN1
    6        NLGN1(L)=K1

C CS IS FROM 1 TO -1 FOR LGNDF3, SO THAT GG0<0 (90.11.22)
             GG0=ABS(GG0)
             DO 10 I=1,NANG
   10        PHSF(I,L)=PHSF(I,L)/GG0/4/PI
CC CONSTRUCTION OF PHASE FUNCTION FROM -G- WHEN INDP=0 AND IMTHD.GE.1
            ELSE
             GG0=G(1,L)
             ICHK=0
             K2=NLGN1(L)
             DO 9 K1=1,K2
             G(K1,L)=G(K1,L)/GG0
             IF(ABS(G(K1,L)).LE.EPSP) THEN
               ICHK=ICHK+1
               IF(ICHK.GE.NCHK1) GOTO 46
             ENDIF
    9        CONTINUE
             K1=MXLGN1
   46        NLGN1(L)=K1
             IF(INDPL.GE.1) THEN
               DO 7 I=1,NANG
               CS1=CS(I)
               INIT=1
               SUM=0
               DO 8 K1=1,NLGN1(L)
    8          SUM=SUM+(2*K1-1)*G(K1,L)*PLGD(INIT,CS1)
    7          PHSF(I,L)=SUM/4/PI
             ENDIF
           ENDIF
           NLGN1P(L) = NLGN1(L)
           do k1=1,NLGN1(L)
              GP(K1,L)=G(K1,L)
           enddo
           do i=1,nang
             PHSFP(I,L) = PHSF(I,L)
           enddo
    3    continue	
c     endif  
      do L=1,NLN
         NLGN1(L) = NLGN1P(L + lmin)
         do k1=1,NLGN1(L)
           G(K1,L)=GP(K1,L+lmin)
         enddo
         do i=1,nang
           PHSF(I,L) = PHSFP(I,L+lmin)
         enddo
CC DELTA-M MOMENTS (ICHKT=1 MEANS WE DO TRUNCATION AT LEAST ONE TIME)
          ICHKT=0
          NLGT1(L)=MIN(MXLGT1,NLGN1(L))
          IF(IMTHD.GE.0 .AND. NLGN1(L).GT.MXLGT1) THEN
             FF(L)=G(MXLGT1+1,L)
             ICHKT=1
          ELSE
             FF(L)=0
          ENDIF
          DO 11 K1=1,NLGT1(L)
   11     GT(K1,L)=(G(K1,L)-FF(L))/(1-FF(L))
          OMGT(L)=(1-FF(L))*OMG(L)/(1-FF(L)*OMG(L))
          THKT(L)=(1-FF(L)*OMG(L))*THK(L)

c       calcul valeur d�terminante pour prise en compte troncature pour les AJ
          wf = OMG(L)*FF(L)
          caltronc = max(wf0,wf)
          wf0 = caltronc
      enddo

c    On fait l'hypoth�se que si sol homog�ne, alors pas besoin de 
c    calculer la luminance, on ne calcule que les fonctions sources
c    sauf si troncature importante.
c     ! if ((caltronc.lt.0.1).and.(isot.eq.1)) then
c     !    ilum = 0
c     ! else
c     !    ilum = 1
c     ! endif

C RESET -MXLGN1- AND -MXLGT1-
      MXLGN1=1
      MXLGT1=1
      DO 12 L=1,NLN
      MXLGN1=MAX(MXLGN1,NLGN1(L))
   12 MXLGT1=MAX(MXLGT1,NLGT1(L))
C SET TRUNCATED USER DEFINED DEPTHS
      DPTT(1)=DPT(1)
      DO 13 L=1,NLN
   13 DPTT(L+1)=DPTT(L)+THKT(L)
      DO 14 IT=1,NTAU
      DO 47 L=1,NLN
      IF((UTAU(IT)-DPT(L))*(UTAU(IT)-DPT(L+1)).LE.0) THEN
        DTAU=UTAU(IT)-DPT(L)
        DTAUT=(1-FF(L)*OMG(L))*DTAU
        UTAUT(IT)=DPTT(L)+DTAUT
      ENDIF
   47 CONTINUE
      IF(UTAUT(IT).GT.DPTT(NLN+1)) UTAUT(IT)=DPTT(NLN+1)
   14 CONTINUE
C SCALING COEFFICIENTS FOR THERMAL EMISSION
      IF(NPLK1.GT.0) THEN
        DO 15 L=1,NLN
        DO 15 K1=1,NPLK1
   15   CPLKT(K1,L)=CPLK(K1,L)/(1-OMG(L)*FF(L))**(K1-1)
      ENDIF
C SET -MMAX1-
      MMAX1=1
c      Si variation azimuthale de la diffusion alors MMAX1 doit etre egal a MXLGT1, sinon MMAX1=1 pas de variation azimuthale (calcul plus rapide).
      IF ((INDA.GE.1).and.(FSOL.gt.0.).and.(sigma.ge.1200.).and.
     &                             (iazim.gt.0).and.(am0(1).lt.1.)) then
         MMAX1=MXLGT1
      endif
C INITIALIZE ANGLE INDEX FOR CHECKING CONVERGENCE
        NB0=NA0
        DO 24 J=1,NA0
        IICHK0(J)=0
        JM0(J)=J
   24   BM0(J)=AM0(J)
      IF(INDA.GT.0) THEN
        NB1U=NA1U
        DO 25 I=1,NA1U
        IICHK1(I)=0
        IM1U(I)=I
   25   BM1U(I)=AM1U(I)
      ENDIF
C FOURIER SUM
      INIT=1
      DO 16 M1=1,MMAX1
        M=M1-1
c       write(*,*) 'COMPOSANTE DE FOURIER:',M
        FM=PI
        IF(M.EQ.0) FM=2*PI
        CALL FTRN21(INDG,INIT,M,INDA,INDT,IMTHD,NB1U,BM1U,NB0,BM0
     &  ,NDA,AMUA,WA,NLN,NLGT1,GT,DPTT,OMGT,NPLK1,CPLKT,GALB,BGND
     &  ,FSOL,NTAU,UTAUT,SCR,SCI,FLXD,FLXU,AIF,ajm,ERR,caltronc,
     &   ilum,CGLO)
        IF(ERR.NE.' ') RETURN
        IF(INDA.GT.0) THEN
          DO 44 JB=1,NB0
   44     EU0(JB)=0
          DO 45 IB=1,NB1U
   45     EU1(IB)=0
          DO 17 K=1,NFI
          FI1=M*FI(K)*RAD
   17     COSM(K)=COS(FI1)/FM
          DO 18 JB=1,NB0
          J=JM0(JB)
          DO 18 IB=1,NB1U
          I=IM1U(IB)
          DO 18 K=1,NFI
          DO 18 IT=1,NTAU
          DAI=AIF(IB,JB,IT)*COSM(K)
          AI(I,J,K,IT)=AI(I,J,K,IT)+DAI
          IF(ABS(AI(I,J,K,IT)).GT.0) THEN
            EAI=ABS(DAI/AI(I,J,K,IT))
           ELSE
            IF(ABS(DAI).LE.0) THEN
              EAI=0
             ELSE
              EAI=100
            ENDIF
          ENDIF
          EU0(JB)=MAX(EU0(JB),EAI)
          EU1(IB)=MAX(EU1(IB),EAI)
   18     CONTINUE
C CHECK CONVERGENCE FOR AM0
          CALL CONVU(NB0,BM0,JM0,EU0,IICHK0,EPSU,NCHK2)
          IF(NB0.LE.0) GOTO 30
          IF(INDA.NE.2) THEN
            CALL CONVU(NB1U,BM1U,IM1U,EU1,IICHK1,EPSU,NCHK2)
            IF(NB1U.LE.0) GOTO 30
          ENDIF
C
c *** Ajout Matisse
c
c  Sommation de Fourier sur les fonctions source de diffusion multiple
          do i=1,na1u
             do k=1,nfi
                do it=1,ntau
                   AJ(i,k,it) = AJ(i,k,it) + AJM(i,it)*cosm(k)		   
                enddo
             enddo
          enddo
        ENDIF
   16 CONTINUE
   
c  Comparaison des luminances avec Disort si INDA=2   
      conv = sigma*sigma/1e8
      do it=1,ntau
       !  write(89,*) utaut(it)
	 do i=1,nda
!	    write(89,*) amua(2*nda+1-i),ai(2*nda+1-i,1,1,it)*conv,
 !    & amua(nda+1-i),AI(nda+1-i,1,1,IT)*conv
	 enddo
	! write(89,*)
      enddo   
      
      IF(INDG.GT.0) INDG=2
      
      ! Correction PS du 6/11/2007
      ! ICHKT = 1 si la derni�re couche test�e (altitude 0 km) necessite de
      ! tronquer la fct de phase => si pas d'a�rosols dans couche limite mais nuages 
      ! au dessus, alors pas de correction dans le nuage
      ! => on retire le test de Nakajima
c   30 IF(IMTHD.LE.0 .OR. INDA.LE.0 .OR. ICHKT.LE.0) RETURN
   30 IF(IMTHD.LE.0 .OR. INDA.LE.0) RETURN


c     Si le terme omega*f est trop petit sur l'ensemble des altitudes,
c     alors on néglige la correction sur les fonctions sources
c     Pas de correction pour les intensités si sol isotrope 
c               (pas de calcul sphère d'éclairement)    

c     if ((caltronc.lt.0.1).and.(isot.eq.1)) return
      
c     if (INIT0.GT.0) then
c           write(*,*) '#INFO : '
c           write(*,*) '#INFO : TRONCATURE FCT PHASE IMPORTANTE'
c           write(*,*) '#INFO : POUR AU MOINS UNE COUCHE => '
c           write(*,*) '#INFO : ON CORRIGE LA FONCTION SOURCE'
c           write(*,*) '#INFO : AUX NOMBRES D''ONDES REQUIS'
c           write(*,*) '#INFO : '
c        endif
	 
C--- INTENSITY CORRECTION.
C *** Ajout Matisse, sinon Pb avec la fonction SIGN sous f90 en double precision
        one = 1.0
        DO 19 IS=1,NA0
        DO 19 IU=1,NA1U
        DO 19 IZ =1,NFI
CC COS(SCATTERING ANGLE)
        CS1=AM0(IS)*AM1U(IU)+SQRT((1-AM0(IS)**2)*(1-AM1U(IU)**2))
     &    *COS(FI(IZ)*RAD)
        IF(ABS(CS1).GT.1.0) CS1=SIGN(one,CS1)
        ANG1=ACOS(CS1)/RAD
        init_GD=1	! Modif GD pour appel � PINT4 plus optimis�
        DO 20 L=1,NLN
        L9=L
        INIT=1
CC INTERPOLATION OF ORIGINAL PHASE FUNCTION FROM GIVEN PHASE FUNCTION.
        IF(INDP.GE.0) THEN
C          PHS(L)=PINT4(INIT,ANG1,KNANG,NANG,ANG,PHSF,L9)
          PHS(L)=PINT4(init_GD,ANG1,KNANG,NANG,ANG,PHSF,L9)
CC INTERPOLATION OF ORIGINAL PHASE FUNCTION FROM -G-.
         ELSE
          SUM=0
          DO 21 K1=1,NLGN1(L)
   21     SUM=SUM+(2*K1-1)*G(K1,L)*PLGD(INIT,CS1)
          PHS(L)=SUM/4/PI
        ENDIF
CC INTERPOLATION OF TRUNCATED PHASE FUNCTION FROM -GT-.
        INIT=1
        SUM=0
        DO 22 K1=1,NLGT1(L)
   22   SUM=SUM+(2*K1-1)*GT(K1,L)*PLGD(INIT,CS1)
        PHST(L)=SUM/4/PI
   20   CONTINUE
c        !if (ilum.eq.1) then
           CALL INTCR1(IMTHD,AM0(IS),AM1U(IU),CS1,NTAU,UTAU,UTAUT
     &      ,NLN,THK,THKT,OMG,OMGT,PHS,PHST,FF,KLGN1,MXLGN1,NLGN1
     &      ,NLGT1,G,EPSP,NCHK1,COR,SGL2,PHSB,ERR)
c        !endif
C
        DO 23 IT=1,NTAU
C Corrected single scattering from ocean surface
           IF(INDG.LE.0) then
             A3=0
           else
             AM11=ABS(AM1U(IU))
             FIS=FI(IZ)*RAD
             IF(AM1U(IU).LT.0) then
               A3=FSOL *SEARF1(AM11,AM0(IS),FIS,GALB,SCR,SCI)
     &         *EXP(-DPTT(NLN1)/AM0(IS)-(DPTT(NLN1)-UTAUT(IT))/AM11)
C    &         *EXP(-(DPTT(NLN1)-UTAUT(IT))*(1/AM0(IS)-1/AM11))
              ELSE
               A3=0
             ENDIF
           endif
c          Modification PS le 23/01/02 pour AI tronqu�e avec correction surface
c          AI(IU,IS,IZ,IT)=AI(IU,IS,IZ,IT)+COR(IT)*FSOL+A3
           AI(IU,IS,IZ,IT)=AI(IU,IS,IZ,IT)+A3
23      continue

c *** Ajout Matisse par PS le 9/01/02
c
c  Correction de la fonction source pour la troncature fonction des fonctions sources et 
c                           luminances tronqu�es                    
c
c         J = w.f.L* + (1-w.f)J* + w.p(teta0).F0.(exp(-tau*/mu0) - exp(-tau/mu0))
c
        sum = 0.
        do l = 1,NLN
c          La fonction source est calculée à chaque intercouche donc pour chaque valeur 
c          de l'épaisseur optique
           W = OMG(l)
           Ftronc = FF(l)
           WF = W*Ftronc
           WSM = W*abs(AM0(IS))
           if (l.eq.1) then
              it = l
              AJtronque = AJ(IU,IZ,IT)
              AItronque = AI(IU,IS,IZ,IT)
              AJ(IU,IZ,IT) = WF*AItronque + (1.-WF)*AJtronque
              if (IMTHD.eq.0) then
                 AJ(IU,IZ,IT) = AJ(IU,IZ,IT) + W*PHS(l)*FSOL*
     &             (exp(-utaut(it)/AM0(IS)) - exp(-utau(it)/AM0(IS)))
              endif
           endif
           IT = L+1
           AJtronque = AJ(IU,IZ,IT)
           AItronque = AI(IU,IS,IZ,IT)
           if (IMTHD.eq.0) then
              AJ(IU,IZ,IT) = AJ(IU,IZ,IT) + W*PHS(l)*FSOL*
     &          (exp(-utaut(it)/AM0(IS)) - exp(-utau(it)/AM0(IS)))
           endif
           if (IMTHD.eq.1) then
              sum = sum + WF*PHS(l)*(thk(l))
              AJ(IU,IZ,IT) = WF*AItronque + (1.-WF)*AJtronque
     &                  + WSM*FSOL*exp(-utau(it)/AM0(IS))*sum
           endif
           if (IMTHD.eq.2) then
              sum = sum + WF/(1.-WF)*PHS(l)*(thkt(l))
c             sum = sum + WF +(1.-Ftronc)*W*PHSt(l)*(thkt(l))
              AJ(IU,IZ,IT) = WF*AItronque + (1.-WF)*AJtronque
     &                  + WSM*FSOL*exp(-utaut(it)/AM0(IS))*sum
           endif
        enddo
c  
c       Correction de la luminance pour la troncature
c        if (ilum.eq.1) then
           do IT=1,NTAU
             AI(IU,IS,IZ,IT)=AI(IU,IS,IZ,IT)+COR(IT)*FSOL
           enddo
c        endif
c  Fin de correction

C ANGULAR LOOP END
   19 CONTINUE
C NORMAL END
c
   
      if (INIT0.GT.0) INIT0=0


      ERR=' '
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE CHKRT(INDA,INDT,INDP,IMTHD,NDA,NA1U,AM1U,NA0,AM0
     &,NFI,FI,NLN,THK,OMG,NLGN1,G,NANG,ANG,PHSF,EPSP,EPSU,GALB,FSOL
     &,NPLK1,CPLK,BGND,NTAU,UTAU,AMUA,WA,DPT,MXLGN2,ERR)

      include 'precision.f'

C$NAME CHKRT
C$LINK CPCON QUADA
C CHECK-IN VARIABLES FOR -RTRN1- AND SET SOME VARIABLES.
C--- HISTORY
C 90. 1.20   CREATED
C    11.22   ADD: ELSE MXLGN2=2*NDA+1
C    12. 1   ADD LOOP-19.
C 93. 5. 4   Delete the condition GALB<=1 because GALB is used as wind
C            velocity when INDG>0.
C INPUT/OUTPUT SEE MAIN ROUTINE.
C$PRPC
      include 'parameter.rayjn'
C
      PARAMETER (KLGN1=2*KNDM)

c     PARAMETER (KNA1U=8,KNA0=2,KNDM=8,KNFI=3)
C$PRPC
c     PARAMETER (KLGN1=200,KNLN=34,KNTAU=34,KPLK1=4)
C$PRPC
c     PARAMETER (KNANG=200)
C
      PARAMETER (KNLN1=KNLN+1,KLGT1=2*KNDM)
      PARAMETER (PI=3.141592654)
C AREAS FOR THIS ROUTINE
      CHARACTER ERR*(*)
      DIMENSION AM1U(KNA1U),AM0(KNA0),FI(KNFI),THK(KNLN),OMG(KNLN)
     &,NLGN1(KNLN),G(KLGN1,KNLN),ANG(KNANG),PHSF(KNANG,KNLN)
     &,CPLK(KPLK1,KNLN),UTAU(KNTAU)
      DIMENSION AMUA(KNDM),WA(KNDM),DPT(KNLN1)
      DIMENSION CCP(3)
      CALL CPCON(CCP)
      EPS=CCP(1)*10
      ERR=' '
C EPSP, EPSU, FSOL
      IF(EPSP.LT.0) THEN
        ERR='ILLEGAL VALUE OF EPSP'
        RETURN
      ENDIF
      IF(EPSU.LT.0) THEN
        ERR='ILLEGAL VALUE OF EPSU'
        RETURN
      ENDIF
      IF(FSOL.LT.0) THEN
        ERR='ILLEGAL VALUE OF FSOL'
        RETURN
      ENDIF
C INDA
      IF(INDA.LT.0 .OR. INDA.GT.2) THEN
        ERR='ILLEGAL VALUE OF INDA'
        RETURN
      ENDIF
C INDT
      IF(INDT.LT.0 .OR. INDT.GT.2) THEN
        ERR='ILLEGAL VALUE OF INDT'
        RETURN
      ENDIF
C INDP
      IF(INDP.LT.-1 .OR. INDP.GT.1) THEN
        ERR='ILLEGAL VALUE OF INDP'
        RETURN
       ENDIF
C IMTHD
      IF(IMTHD.GT.3) THEN
        ERR='ILLEGAL VALUE OF IMTHD'
        RETURN
      ENDIF
C NDA
      IF(NDA.LE.0 .OR. NDA.GT.KNDM) THEN
        ERR='ILLEGAL VALUE OF NDA'
        RETURN
      ENDIF
C SET QUADRATURE
      CALL QUADA(NDA,AMUA,WA)
C NA0, AM0
      IF(NA0.LE.0 .OR. NA0.GT.KNA0) THEN
        ERR='ILLEGAL VALUE OF NA0'
        RETURN
      ENDIF
      DO 20 I=1,NA0
      IF(AM0(I).LE.0.0 .OR. AM0(I).GT.1.0) THEN
        ERR='ILLEGAL VALUE OF AM0'
        RETURN
      ENDIF
   20 CONTINUE
C INDA, AM1U, FI
      IF(INDA.GT.0) THEN
        IF(NFI.LE.0 .OR. NFI.GT.KNFI) THEN
          ERR='ILLEGAL VALUE OF NFI'
          RETURN
        ENDIF
        IF(INDA.EQ.2) NA1U=2*NDA
        IF(NA1U.LE.0 .OR. NA1U.GT.KNA1U) THEN
          ERR='ILLEGAL VALUE OF NA1U'
          RETURN
        ENDIF
        IF(INDA.EQ.2) THEN
          DO 1 I=1,NDA
          AM1U(I)        = -AMUA(I)
    1     AM1U(NA1U+1-I) =  AMUA(I)
        ENDIF
      ENDIF
C NLN
      IF(NLN.LE.0 .OR. NLN.GT.KNLN) THEN
        ERR='ILLEGAL VALUE OF NLN'
        RETURN
      ENDIF
      DPT(1)=0
      DO 17 L=1,NLN
   17 DPT(L+1)=DPT(L)+THK(L)
C NTAU
      IF(INDT.EQ.2) NTAU=NLN+1
      IF(NTAU.LE.0 .OR. NTAU.GT.KNTAU) THEN
        ERR='ILLEGAL VALUE OF NTAU'
        RETURN
      ENDIF
C UTAU
      IF(INDT.EQ.2) THEN
        DO 3 IT=1,NTAU
         UTAU(IT)=DPT(IT)
    3    CONTINUE 
      ENDIF
C NLGN1,  MXLGN2=MAX(NLGN1)
      IF(INDP.LE.0 .OR. (INDA.GT.0 .AND. IMTHD.EQ.3)) THEN
        MXLGN2=1
        DO 4 L=1,NLN
        N1=NLGN1(L)
        IF(N1.LE.0 .OR. N1.GT.KLGN1) THEN
          ERR='ILLEGAL VALUE OF NLGN1'
          RETURN
        ENDIF
        MXLGN2=MAX(MXLGN2,N1)
    4   CONTINUE
       ELSE
        MXLGN2=2*NDA+1
      ENDIF
C NANG
      IF(INDP.GT.0 .OR. (IMTHD.GE.1 .AND. INDA.GE.1)) THEN
        IF(NANG.LE.3 .OR. NANG.GT.KNANG) THEN
          ERR='YOU SHOULD SET AT LEAST FOUR ANGLES FOR THIS CONDITION'
          RETURN
        ENDIF
        DO 12 I=2,NANG
          IF(ANG(I).LE.ANG(I-1)) THEN
            ERR='YOUR SHOULD SET SCATTERING ANGLE FROM 0 TO 180 DEGREES'
            RETURN
          ENDIF
   12   CONTINUE
      ENDIF
C CHECK ORDER OF DPT AND UTAU.
      DO 5 L=1,NLN
      IF(DPT(L+1).LT.DPT(L)) THEN
        ERR='DPT SHOULD BE SET FROM TOP TO BOTTOM'
        RETURN
      ENDIF
    5 CONTINUE
      IF(NTAU.GE.2) THEN
        DO 6 IT=2,NTAU
        IF(UTAU(IT).LT.UTAU(IT-1)) THEN
          ERR='UTAU SHOULD BE SET FROM TOP TO BOTTOM'
          RETURN
        ENDIF
    6   CONTINUE
      ENDIF
      DO 19 IT=1,NTAU
      IF(UTAU(IT).GT.DPT(NLN+1)) THEN
        IF(ABS(UTAU(IT)-DPT(NLN+1)).LE.EPS) THEN
          UTAU(IT)=DPT(NLN+1)
         ELSE
          ERR='UTAU IS OUT OF BOUNDS'
          RETURN
        ENDIF
      ENDIF
   19 CONTINUE
C RESET CPLK AND BGND, GALB
      IF(GALB.LT.0.0) THEN
        ERR='ILLEGAL VALUE OF GALB'
        RETURN
      ENDIF
      IF(NPLK1.GT.KPLK1) THEN
        ERR='TOO LARGE -NPLK1-, CHANGE -KPLK1-'
        RETURN
       ELSE
        IF(NPLK1.LE.0) THEN
          BGND=0
          DO 18 L=1,NLN
          DO 18 K1=1,NPLK1
   18     CPLK(K1,L)=0
        ENDIF
      ENDIF
      RETURN
      END
c
c------------------------------------------------------------------------------
c      
      SUBROUTINE CONVU(N,AM,JJ,ER,IC,EPSU,NCHK)

      include 'precision.f'

C$NAME CONVU
C CHECK CONVERGENCE OF INTENSITY IN THE DIRECTION OF MU
C--- HISTORY
C 90. 1.20 CREATED
C--- INPUT
C N      I        NUMBER OF STREAMS TO BE CHECKED
C AM    R(N)      DIRECTION OF STREAM
C JJ    I(N)      STREAM NUMBER IN THE ORIGINAL ORDER
C ER    R(N)      MAXIMUM ERROR FOR THE STREAM REGARDLESS OF OTHER
C                  ANGLES AND LAYERS
C IC    I(N)      NUMBER OF CONSECTIVE SERIES WITH -ER- LESS THAN -EPSU-
C EPSU    R       CONVERGENCE CRITERION
C NCHK    I       MAX. NUMBER FOR THE VALUE OF -IC- BY WHICH THE
C                  ROUTINE CONFIRMS CONVERGENCE
C--- OUTPUT
C N, AM, JJ, IC   UPDATED AFTER DROPPING CONVERGENT STREAMS
C$ENDI
      DIMENSION AM(*),JJ(*),ER(*),IC(*)
      J1=0
      DO 1 J=1,N
      IF(ABS(ABS(AM(J))-1).LE.0) THEN
        IC(J)=999
        ER(J)=0
      ENDIF
      IF(ER(J).LT.EPSU) THEN
        IC(J)=IC(J)+1
        IF(IC(J).GE.NCHK) GOTO 1
       ELSE
        IC(J)=0
      ENDIF
      J1=J1+1
      IC(J1)=IC(J)
      JJ(J1)=JJ(J)
      AM(J1)=AM(J)
      ER(J1)=ER(J)
    1 CONTINUE
      N=J1
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE INTCR1(IMTHD,AM0,AM1U,CS1,NTAU,UTAU,UTAUT
     &,NLN,THK,THKT,OMG,OMGT,PHS,PHST,FF,KLGN1,MXLGN1,NLGN1
     &,NLGT1,G,EPSP,NCHK1,COR,SGL2,PHSB,ERR)

      include 'precision.f'

C$NAME INTCR1
C$LINK SGLR
C SIGLE AND SECOND SCATTERING CORRECTION
C ASSUME DELTA-M METHOD
C--- HISTORY
C 90. 1.28 CREATED
C--- INPUT
C IMTHD      I         1: MS, 2: TMS, 3: IMS
C AM0        R         COS(SOLAR ZENITH ANGLE)
C AM1U       R         COS(EMERGENT NADIR ANGLE)
C                     .LT.O: UPWARD,  .GT.0: DOWNWARD
C CS1        R         COS(SCATTERING ANGLE)
C NTAU       I         NUMBER OF USER DEFINED ANGLE
C UTAU     R(NTAU)     USER DEFINED OPTICAL DEPTH
C UTAUT    R(NTAU)     TRUNCATED USER DEFINED DEPTH
C NLN        I         NUMBER OF LAYER
C THK      R(NLN)      OPTICAL THICKNESS OF SUBLAYER
C THKT     R(NLN)      TRUNCATED THICKNESS
C OMG      R(NLN)      SINGLE SCATTERING ALEBEDO
C OMGT     R(NLN)      TRUNCATED SINGLE SCATTERING ALBEDO
C PHS      R(NLN)      PHASE FUNCTION  (INTEGRAL = 1 OVER UNIT SPHERE)
C PHST     R(NLN)      TRUNCATED PHASE FUNCTION (1 OVER UNIT SPHERE)
C FF       R(NLN)      TRUNCATION FRACTION
C KLGN1      I         FIRST ARGUMENT SIZE OF G
C MXLGN1     I         MAX(NLGN1) (ONLY FOR IMTHD=3)
C NLGN1    I(NLN)      MAX ORDER OF LEGENDER SERIES +1 IN EACH SUBLAYER
C                        (ONLY FOR IMTHD=3)
C NLGT1    I(NLN)      SAME AS -NLGN1- BUT FOR TRUNCATION
C G      R(KLGN1,NLN)  PHASE FUNCTION MOMENT (ONLY FOR IMTHD=3)
C EPSP       R         CONVERGENCE CRITERION OF LEGENDRE SERIS OF
C                        PHASE FUNCTION ** 2
C NCHK1      I         NUMBER OF CONSECTIVE CONVERGENCES BEFORE
C                        FINAL DECISION
C--- OUTPUT
C COR      R(NTAU)     AI= AI+COR*FSOL IS CORRECTED INTENSITY
C                        AT EACH USER
C ERR      C*64        ERROR INDICATER
C                       DEFINED DEPTH (UTAU)
C--- WORK
C SGL2     R(NTAU)
C PHSB     R(NLN)
C--- AREA FOR THIS ROUTINE
      PARAMETER (PI=3.141592654, RAD=PI/180.0)
      CHARACTER ERR*(*)
      DIMENSION UTAU(NTAU),UTAUT(NTAU),THK(NLN),THKT(NLN)
     &,OMG(NLN),OMGT(NLN),PHS(NLN),PHST(NLN),FF(NLN),NLGN1(NLN)
     &,NLGT1(NLN),G(KLGN1,NLN),COR(NTAU),SGL2(NTAU),PHSB(NLN)
C
      ERR=' '
      DO 1 IT=1,NTAU
    1 COR(IT)=0
      IF(IMTHD.LE.0 .OR. IMTHD.GE.4) RETURN
C--- MS-METHOD IN REFERENCE-NT
      IF(IMTHD.EQ.1) THEN
C SGL2 = U1WAVE OF EQ.(14)
        DO 2 L=1,NLN
    2   PHSB(L)=(1-FF(L))*PHST(L)
c       DO 3 IT=1,NTAU
c   3   SGL2(IT)=SGLR(AM1U,AM0,NLN,THK,OMG,PHSB,UTAU(IT))
        DO 3 IT=1,NTAU
    3   SGL2(IT)=SGLR(AM1U,AM0,NLN,THK,OMG,PHSB,UTAU(IT))
C CORRECTION BY MS-METHOD.  SEE EQ.(14)
        DO 4 IT=1,NTAU
CC SGL1 = U1 OF EQ.(14)
        SGL1=SGLR(AM1U,AM0,NLN,THK,OMG,PHS,UTAU(IT))
    4   COR(IT)=-SGL2(IT)+SGL1
        RETURN
      ENDIF
C--- TMS AND IMS METHODS
C  SGL2 = U1* OF EQ.(15)
c     DO 5 IT=1,NTAU
c   5 SGL2(IT)=SGLR(AM1U,AM0,NLN,THKT,OMGT,PHST,UTAUT(IT))
      DO 5 IT=1,NTAU
    5 SGL2(IT)=SGLR(AM1U,AM0,NLN,THKT,OMGT,PHST,UTAUT(IT))
      DO 6 L=1,NLN
    6 PHSB(L)=PHS(L)/(1-FF(L))
C CORRECTION OF INTENSITY BY TMS-METHOD.  SEE EQ.(15)
      DO 7 IT=1,NTAU
CC  SGL1 = U1WAVE* OF EQ.(15)
      SGL1=SGLR(AM1U,AM0,NLN,THKT,OMGT,PHSB,UTAUT(IT))
    7 COR(IT)=-SGL2(IT)+SGL1
      IF(IMTHD.EQ.2 .OR. AM1U.LE.0.0) RETURN
C--- SECONDARY SCATTERING CORRECTION FOR IMS-METHOD
      DO 8 IT=1,NTAU
        UTAUS=UTAU(IT)
C GETTING MEAN OPTICAL CONSTANTS ABOVE THE USER DEFINED LEVEL-UTAUS
        EH=0
        SH=0
        SHH=0
        PHSPK=0
        DPT2=0
        DO 9 L=1,NLN
          DPT1=DPT2
          DPT2=DPT1+THK(L)
          IF(UTAUS.LE.DPT1) GOTO 10
          IF(UTAUS.LT.DPT2) THEN
            TAU=UTAUS-DPT1
           ELSE
            TAU=THK(L)
          ENDIF
          EH= EH     +       TAU
          SH= SH     +OMG(L)*TAU
          SHH=SHH    +OMG(L)*TAU*FF(L)
    9     PHSPK=PHSPK+OMG(L)*TAU*(PHS(L)-(1-FF(L))*PHST(L))
   10   IF(ABS(EH).LE.0) GOTO 8
C WH: MEAN SINGLE SCATTERING ALBEDO
        WH=SH/EH
C FH: MEAN TRUNCATED FRACTION
        FH=SHH/SH
        IF(FH.LE.0.0) GOTO 8
C PHSPK: MEAN TRUNCETED PEAK OF PHASE FUNCTION
        PHSPK=PHSPK/SHH
C AM3: VARIABLE APPEARING IN EQ.(23)
        AM3=AM0/(1-FH*WH)
C LEGENDRE SUM FOR EQ.(23)
C  WE TRUNCATE THE SERIES WHEN ABS(GPK**2) BECOMES SMALLER THAN -EPSP-
C   FOR SUCCESSIVE THREE TIMES.
        PHSPK2=0
        INIT=1
        ICHK=0
        DO 11 K1=1,MXLGN1
CC PHSPK: 2PHAT-PHAT2 OF EQ.(23)
CC  MEAN VALUE FOR THE LAYER ABOVE THE USER-DEFINED LEVEL
          GPK=0
          DPT2=0
          DO 12 L=1,NLN
          DPT1=DPT2
          DPT2=DPT1+THK(L)
          IF(K1.LE.NLGN1(L) .AND. OMG(L).GT.0) THEN
            IF(UTAUS.LE.DPT1) GOTO 13
            IF(UTAUS.LT.DPT2) THEN
              TAU=UTAUS-DPT1
             ELSE
              TAU=THK(L)
            ENDIF
            IF(K1.LE.NLGT1(L)) THEN
              GP=FF(L)
             ELSE
              GP=G(K1,L)
            ENDIF
            GPK=GPK+GP*OMG(L)*TAU
          ENDIF
   12     CONTINUE
   13     GPK=GPK/SHH
          GPK2=GPK**2
          IF(GPK2.LE.EPSP) THEN
            ICHK=ICHK+1
            IF(ICHK.GE.NCHK1) GOTO 14
          ENDIF
   11     PHSPK2=PHSPK2+(2*K1-1)*GPK2*PLGD(INIT,CS1)
   14   PHSPK2=PHSPK2/4/PI
CC CORRECTION BY UU = UU + UHAT IN IMS-METHOD
CC   SGL3: UHAT OF EQ.(23)
        SGL3=(FH*WH)**2/(1-FH*WH)*(2*PHSPK-PHSPK2)
     &    * HF(UTAUS, AM1U, AM3, AM3)
        COR(IT)=COR(IT)-SGL3
    8 CONTINUE
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION PINT4(INIT,ANG1,KNA,NA,ANG,P,L)

      include 'precision.f'

C$NAME  PINT4
C$LINK  EXPFN
C INTERPOLATION OF THE PHASE FUNCTION.
C--- HISTORY
C 89.11. 8 CREATED FROM PINT3. CHANGE INTERPOLATION X-> ANG
C 90. 1.23 DEBUG
C--- INPUT
C INIT       I      1 THEN SEARCH ANG1-INTERVAL ELSE NOT SEARCH.
C ANG1       R      SCATTERING ANGLE IN DEGREE FOR INTERPOLATION.
C NA         I      NO. OF SCATTERING ANGLES.
C ANG      R(NA)    SCATTERING ANGLES IN DEGREE.
C P     R(KNA,L)    PHASE FUNCTION
C L          I      LAYER NUMBER
C--- OUTPUT VARIABLES
C INIT       I      0
C PINT4      R      INTERPOLATED VALUE OF P AT ANG1.
C$ENDI
C--- VARIABLES FOR THE ROUTINE.
      SAVE I1,I2,I3
      DIMENSION ANG(NA),P(KNA,L)
C--- WORKING AREAS.
      PARAMETER (PI=3.141592654, RAD=PI/180.0)
C
      IF(INIT.GE.1) THEN
        INIT=0
        DO 1 I=1,NA-1
        IF((ANG1-ANG(I))*(ANG1-ANG(I+1)).LE.0.0) GOTO 2
    1   CONTINUE
        I=NA
c    2   IF(I-1) 3,3,4
    2   IF((I-1).le.0) then 
    3      I1=1
        else
	   goto 4
	endif
        I3=3
        GO TO 5
C *
c    4   IF(I-NA+1) 6,7,7
    4   IF((I-NA+1).ge.0) goto 7 
    6   I1=I-1
        I3=I+1
        GO TO 5
C *
    7   I1=NA-2
        I3=NA
    5   I2=I1+1
      ENDIF
      XX=ANG1
      X1=ANG(I1)
      X2=ANG(I2)
      X3=ANG(I3)
      ALP1=P(I1,L)
      ALP2=P(I2,L)
      ALP3=P(I3,L)
      ISIGN=-1
      IF(ALP1.GT.0.0 .AND. ALP2.GT.0.0 .AND. ALP3.GT.0.0) THEN
        ISIGN=1
        ALP1=LOG(ALP1)
        ALP2=LOG(ALP2)
        ALP3=LOG(ALP3)
      ENDIF

      PP=(XX-X2)*(XX-X3)/(X1-X2)/(X1-X3)*ALP1
     &  +(XX-X1)*(XX-X3)/(X2-X1)/(X2-X3)*ALP2
     &  +(XX-X1)*(XX-X2)/(X3-X1)/(X3-X2)*ALP3
      IF(ISIGN.GE.1) PP=EXPFN(PP)
      PINT4=PP
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION HF(TAU,AM1,AM2,AM3)

      include 'precision.f'

C$NAME HF
C$LINK CPCON EXPFN
C GEOMETRICAL FACTOR FOR THE SCONDERY SCATTERING EQ.(24) OF NT.
C    HF=INTEG(0,TAU)DT*INTEG(0,T)DT1*EXP(T*(1/MU1-1/MU2)
C             + T1*(1/MU2-1/MU3))*EXP(-TAU/AM1)/AM1/AM2
C--- REFERENCE
C NT:  T. NAKAJIMA AND M. TANAKA, 1988, JQSRT, 40, 51-69
C--- HISTORY
C 88. 9.22  CREATED BY T. NAKAJIMA
C 89. 5. 4  USE EXPFN
C--- INPUT
C TAU      R         OPTICAL THICKNESS OF THE LAYER.
C AM1      R         COS(ZENITH ANGLE-1).
C AM2      R         COS(ZENITH ANGLE-2).
C AM3      R         COS(ZENITH ANGLE-3).
C--- OUTPUT
C HF       F         GEOMETRICAL FACTOR.
C
      SAVE INIT,EPS
      DIMENSION CCP(3)
      DATA INIT/1/
C
C SET EPS: IF ABS(1/AM1 - 1/AM0)*TAU .LE. EPS THEN
C                      THE ROUTINE SETS ALMUCANTAR CONDITION-IALM.
      IF(INIT.GT.0) THEN
        INIT=0
        CALL CPCON(CCP)
        EPS=CCP(1)*30
      ENDIF
C
      X1=1/AM1-1/AM2
      X2=1/AM2-1/AM3
      X3=1/AM1-1/AM3
      EX1=-TAU/AM1
      EX2=-TAU/AM2
      EX3=-TAU/AM3
      EX1=EXPFN(EX1)
      EX2=EXPFN(EX2)
      EX3=EXPFN(EX3)
C
      IF(ABS(X2*TAU).LE.EPS) GOTO 1
C X2 <> 0
CC I1
      IF(ABS(X3*TAU).LE.EPS) THEN
        AI1=EX1*(TAU+X3*TAU*TAU/2)
       ELSE
        AI1=(EX3-EX1)/X3
      ENDIF
CC I2
      IF(ABS(X1*TAU).LE.EPS) THEN
        AI2=EX1*(TAU+X1*TAU*TAU/2)
       ELSE
        AI2=(EX2-EX1)/X1
      ENDIF
      HF=(AI1-AI2)/AM1/AM2/X2
      RETURN
C X2 =  0
    1 IF(ABS(X1*TAU).LE.EPS) THEN
        HF=TAU**2*(0.5-X1*TAU/3)*EX1/AM1/AM2
       ELSE
        HF=((TAU-1/X1)*EX2+EX1/X1)/AM1/AM2/X1
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE FTRN21(INDG,INIT,M,INDA,INDT,IMTHD,NA1U,AM1U,NA0,AM0
     &,NDA,AMUA,WA,NLN,NLGN1,G,DPT,OMG,NPLK1,CPLK,GALB,BGND
     &,FSOL,NTAU,UTAU,SCR,SCI,FLXD,FLXU,AI,ajm,ERR,caltronc,ilum,
     & CGLO)

      include 'precision.f'
     
C$NAME  FTRN21
C$LINK  ADISC AINT CINGR EQ12 EQ21 EQ32 GRNDO1 GRNDL3 HOMOG2 PHAS2 PLGND
C$LINK  TRN1 CSPLI CSPL1
C
C SOLVE THE RADIATIVE TRANSFER IN ATMOSPHERE SYSTEM FOR EACH FOURIER
C COMPONENT.
C  -DOM AND ADDING METHOD-
C BY TERUYUKI NAKAJIMA
C SINCE THIS SYSTEM DOES NOT INCLUDE A OCEAN SURFACE,
C KNLT=KNLN IN TRN1.
C
C--- HISTORY
C 89.11. 2 CREATED FROM STRN7 WITH THERMAL EMISSION
C 90.12. 1 CALL AINT(M,L,  ->  CALL AINT(M,LT,
C 92.12.   ADD OCEAN SURFACE
C 93. 1.   CHANGE UPWARD ADDING
C            USE CSPL1 AND CSPLI
C     3.10 BUG ( XX,YY USED K1 )
C     4. 5 NSF = 1 for initialization (Terry)
C     5. 4 INDG instead of NSF and LOSW
C          Subtracted single scattering if INDG>0
C 94. 5. 7 Bug for interpolating AIB(J)
C 95. 5.25 Replace GRNDL by GRNDL3
C 95. 6. 2 With GRNDO1 and with SCR and SCI
C 96. 3.17 Add ERT By Takashi Nakajima to debug thermal.
C--- INPUT
C INDG       I       0: Lambert surface
C                    1: Ocean surface initialized
C                    2: Ocean surface with no-initialization
C                    When INDG>0 and IMTHD>0 then single scattering correction
C                      for ocean surface reflection
C INIT       I       1: INITIALIZE THE PART (DEPENDENT AMUA, UTAU)
C                    0:  BYPASS THE M-INDEPENDENT PART.
C M          I       FORIER ORDER.
C INDA       I       0: FLUX ONLY.
C                    1: INTENSITY USING AM1U.
C                    2: NA1U AND AM1U ARE SUPPOSED TO BE
C                       2*NDA AND (-AMUA, +AMUA).
C INDT       I       0: SET USER DEFINED DEPTH.
C                    1: SAME AS ABOVE.
C                    2: SET NTAU AND UTAU AS NLN1 AND DPT.
C IMTHD      I      -1: NT,  0: DMS-METHOD  FOR INTENSITY/FLUX
C                    1: MS,  2:TMS,  3:IMS-METHOD FOR INTENSITY.
C                    When INDG>0 and IMTHD>0 then single scattering correction
C                      for ocean surface reflection
C NA1U       I       NUMBER OF EMERGENT ZENITH ANGLES IN THE SPHERE.
C                      NA1U=2*NDA WHEN INDA=2.
C AM1U    R(KNA1U)   CONSINE OF THE EMERGENT ZENITH ANGLES.
C                      + FOR DOWNWARD, - FOR UPWARD.
C                      AM1U = (-AMUA, +AMUA) WHEN INDA=2.
C NA0        I       NUMBER OF THE SOLAR INCIDENCES.
C AM0     R(NA0)     CONSINE OF SOLAR ZENITH ANGLES .GT.0.
C NDA        I       NO. OF ZENITH-QUADRATURE ANGLES IN THE HEMISPHERE.
C AMUA    R(KNDM)    COSINE OF THE ZENITH-QUADRATURE ANGLES. 1 TO 0.
C WA      R(KNDM)    CORRESPONDING QUADRATURE WEIGHTS.
C NLN        I       NUMBER OF ATMOSPHERIC SUBLAYERS.
C NLGN1   I(KNLN)    MAXIMUM ORDER OF THE LEGENDRE SESIES OF THE PHASE
C                      FUNCTION + 1
C G     R(KLGN1,     LEGENDRE MOMENTS OF PHASE FUNCTION.
C           KNLN)      G(1,L)=1
C DPT     R(KNLN1)   OPTICAL DEPTH AT THE INTERFACES BETWEEN SUBLAYERS.
C                      TOP TO BOTTOM (TOP = 0 FOR NORMAL APPLICATION).
C OMG     R(KNLN)    SINGLE SCATTERING ALBEDO.
C NPLK1      I       NUMBER OF ORDER TO APPROXIMATE PLANK + 1.
C                      IF 0 THEN NO THERMAL.
C CPLK    R(KPLK1    PLANK FUNCTION (B) =
C         ,KNLN)       SUM(IB=1,NPLK1) CPLK(IB,L) TAU**(IB-1).
C                      TAU IS OPTICAL DEPTH MEASURED FROM
C                      THE TOP OF THE SUBSURFACE.
C GALB       R       Ground albedo if INDG=0
C                    U10 (m/sec)   if INDG>0
C BGND       R       (1-GALB)*B when INDG=0
C                    B          when INDG>0
C                    where B is Plank function
C                      (SAME UNIT AS FSOL).
C FSOL       R       SOLAR IRRADIANCE (W/M2/MICRON).
C NTAU       I       NUMBER OF USER DEFINED OPTICAL DEPTHS.
C                      NTAU=NLN+1 WHEN INDT=2.
C UTAU    R(KNTAU)   OPTICAL DEPTHS WHERE THE FIELD IS CALCULTED.
C                      TOP TO BOTTOM.
C                      UTAU=DPT WHEN INDT=2.
C--- OUTPUT
C INDG               if 1 then 2
C INIT               0
C FLXD  R(KNA0,KNTAU) DOWNWARD FLUX AT UTAU.
C FLXU               SAME AS FLXD BUT FOR UPWARD FLUX.
C AI      R(KNA1U,   I(MU1U(I), MU0(J), L)
C           KNA0,    INTENSITY AT UTAU.
C           KNTAU)   Subtracted single scattering if INDG>0
C ERR      C*64      ERROR INDICATER.
C--- CORRESPONDENCE BETWEEN VARIABLES AND PARAMETERS
C KNA1U       NA1U
C KNA1        NA1
C KNA0        NA0
C KNDM        NDA
C KNLN        NLN
C KNLN1       KNLN+1
C KNTAU       NTAU
C KLGN1       NLGN1
C KPLK1       NPLK1
C--- NOTES FOR LOCAL VARIABLES
C NA1        I       NUMBER OF STREAMS FOR AM1.
C AM1     R(KNA1)    ABS(AM1U).
C IIAM1   I(KNA1U)   DIRECTION NUMBER OF AM1 FOR EACH AM1U.
C                      + FOR DOWNWARD, - FOR UPWARD.
C IITAU   I(KNTAU)   SUBLAYER NUMBER FOR USER DEFINED DEPTHS.
C$ENDI

      SAVE EPS,WMP,WMM,IITAU
      
C PARAMETERS
C$PRPC
      include 'parameter.rayjn'
c
c     PARAMETER (KNA1U=8,KNA1=4,KNA0=2,KNDM=8)
C$PRPC
c     PARAMETER (KNLN=34,KNTAU=34,KPLK1=4)
C
      PARAMETER (KNLN1=KNLN+1,KLGN1=2*KNDM)
      PARAMETER (PI=3.141592654,RAD=PI/180.0)
C AREAS FOR THIS ROUTINE
      CHARACTER ERR*(*)
      DIMENSION AM1U(KNA1U),AM0(KNA0),AMUA(KNDM),WA(KNDM)
     & ,NLGN1(KNLN),G(KLGN1,KNLN),DPT(KNLN1),OMG(KNLN)
     &,CPLK(KPLK1,KNLN),UTAU(KNTAU),FLXD(KNA0,KNTAU),FLXU(KNA0,KNTAU)
     &,AI(KNA1U,KNA0,KNTAU),am2u(KNA1U)
      real(kind=8) CGLO(KPLK1,KNLN)
     
C *** Ajout Matisse     
      dimension ajm(KNA1U,KNTAU)
      
C WORKING AREAS
      DIMENSION AM1(KNA1),WMP(KNDM),WMM(KNDM)
     &,PL1(KNA1,KLGN1),PL0(KNA0,KLGN1),PLA(KNDM,KLGN1),GBUF(KLGN1)
     &,PT(KNDM,KNDM),PR(KNDM,KNDM),PT0(KNDM,KNA0),PR0(KNDM,KNA0)
     &,PR1(KNA1,KNDM),PT1(KNA1,KNDM),PR10(KNA1,KNA0),PT10(KNA1,KNA0)
     &,ALFA(KNDM,KNA0),BETA(KNDM,KNA0),UUP(KNDM,KNA0),UDN(KNDM,KNA0)
     &,AII(KNA1U,KNA0,KNLN1),AIB(KNA0),IIAM1(KNA1U),IITAU(KNTAU),CCP(3)
C FOR HOMOG2
C$PSET HOMOG2.KNDM=KNDM;  HOMOG2.KNA0=KNA0;  HOMOG2.KPLK1=KPLK1
      DIMENSION CPL(KPLK1),R(KNDM,KNDM),T(KNDM,KNDM)
     &,ER(KNDM,KNA0),ET(KNDM,KNA0),ZEIG(KNDM)
     &,Q(KNDM,KNDM),QI(KNDM,KNDM),C11(KNDM,KNDM),C22(KNDM,KNDM)
     &,VP(KNDM,KNA0),VM(KNDM,KNA0),DP(KNDM,KPLK1),DM(KNDM,KPLK1)
C
      DIMENSION ERT(KNDM,KNA0)
C FOR TRN1 (INCLUDE GROUND AS A SUBLAYER)
      PARAMETER (KNLNM=KNLN1,KNLNM1=KNLNM+1,KNLTM=KNLN1)
C$PSET   TRN1.KNA0=KNA0;  TRN1.KNDM=KNDM;  TRN1.KNSB=KNLN+1
C$PSET   TRN1.KNLT=KNLN+1
      DIMENSION IUP(KNLNM),IDN(KNLNM),NDD(KNLNM1)
     &, RE(KNDM,KNDM,KNLTM),  TE(KNDM,KNDM,KNLTM)
     &,SER(KNDM,KNA0,KNLNM),SET(KNDM,KNA0,KNLNM)
     &,RUP(KNDM,KNA0,KNLNM1),RDN(KNDM,KNA0,KNLNM1)
C FOR INTENSITY INTERPOLATION IN MULTI-LAYER SYSTEM
      DIMENSION QE(KNDM,KNDM,KNLN),QIE(KNDM,KNDM,KNLN)
     &,C1E(KNDM,KNDM,KNLN),C2E(KNDM,KNDM,KNLN),ZEE(KNDM,KNLN)
     &,DPE(KNDM,KPLK1,KNLN),DME(KNDM,KPLK1,KNLN)
     &,VPE(KNDM,KNA0,KNLN),VME(KNDM,KNA0,KNLN)
C$PSET CINGR.KNA0=KNA0; CINGR.KNDM=KNDM; CINGR.KNLN=KNLN
C$PSET CINGR.KPLK1=KPLK1
C$PSET ADISC.KNLN=KNLN; ADISC.KNA0=KNA0; ADISC.KNDM=KNDM
C$PSET ADISC.KPLK1=KPLK1
C$PSET AINT.KNDM=KNDM; AINT.KNA1U=KNA1U; AINT.KNA1=KNA1
C$PSET AINT.KNA0=KNA0; AINT.KNLN=KNLN; AINT.KPLK1=KPLK1
C$PSET GRNDO1.KNDM=KNDM; GRNDO1.KNA0=KNA0
C$PSET GRNDL3.KNDM=KNDM; GRNDL3.KNA0=KNA0
C SET AND CLEAR VARIABLES
      DIMENSION XX(KNDM),YY(KNDM),A(KNDM),B(KNDM),C(KNDM),D(KNDM)
      ERR=' '

      IF(INIT.GT.0) THEN
        INIT=0
        CALL CPCON(CCP)
        EPS=CCP(1)*10
c   =>  WMP et WMM matrices de transformation d'�chelle
        DO 1 I=1,NDA
        WMP(I)=SQRT(WA(I)*AMUA(I))
    1   WMM(I)=SQRT(WA(I)/AMUA(I))
C SET SUBLAYER WHERE USER-DEFINED DEPTH RESIDES -IITAU-.
        DO 40 IT=1,NTAU
        ODP=UTAU(IT)
        EPS1=ODP*EPS
        DO 41 L=1,NLN
          IF((ODP-DPT(L))*(ODP-DPT(L+1)).LE.0.0) GOTO 42
   41   CONTINUE
        ERR='OUT OF BOUNDS IN LAYER SETTING'
        RETURN
   42   IF(ABS(ODP-DPT(L)).LE.EPS1) THEN
          IITAU(IT)=-L
         ELSE
          IF(ABS(ODP-DPT(L+1)).LE.EPS1) THEN
            IITAU(IT)=-(L+1)
           ELSE
            IITAU(IT)=L
          ENDIF
        ENDIF
   40   CONTINUE
      ENDIF
c  => am2u = valeur absolue du cosinus des angles d'observation utilis� 
c            uniquement pour le calcul des fonctions sources    
      do i2= 1,NA1U
         am2u(i2) = abs(am1u(i2))
      enddo
C SET AM1, IIAM1
c  => AM1 tableau contenant la valeur absolue des angles d'observation
      IF(INDA.GT.0) THEN
        NA1=1
        AM1(1)=ABS(AM1U(1))
        IIAM1(1)= 1
        IF(NA1U.GE.2) THEN
          DO 43 IU=2,NA1U
          X=ABS(AM1U(IU))
          DO 44 J=1,NA1
          IF(ABS(X-AM1(J)).LE.EPS) THEN
            IIAM1(IU)= J
            GOTO 43
          ENDIF
   44     CONTINUE
          NA1=NA1+1
          IF(NA1.GT.KNA1) THEN
            ERR='SETTING ERROR OF AM1U'
            RETURN
          ENDIF
          AM1(NA1)=X
          IIAM1(IU)= NA1
   43     CONTINUE
        ENDIF
      ENDIF
C CHECK MAXIMUM NLGN1
      MXLGN1=0
      DO 2 L=1,NLN
    2 MXLGN1=MAX(MXLGN1,NLGN1(L))
      IF(MXLGN1.LE.0) THEN
        ERR='NLGN1 ARE ALL ZERO'
        RETURN
      ENDIF
      M1=M+1
C SET LEGENDRE POLYNOMIALS.
c            m      
c  => PL0 = Pl(�0) = d�veloppement en polynomes de Legendre de l'angle AM0
      CALL PLGND(M1,MXLGN1,NA0,KNA0,AM0 ,PL0)
c            m      
c  => PLA = Pl(�a) = d�veloppement en polynomes de Legendre pour les valeurs >0 de AMUA 
c           angles de la quadrature
      CALL PLGND(M1,MXLGN1,NDA,KNDM,AMUA,PLA)
C SCALING FOR SYMMETLICITY
c  =>   transformation d'�chelle de PLA               
        DO 5 I=1,NDA
        DO 5 J=1,MXLGN1
    5   PLA(I,J)=WMM(I)*PLA(I,J)
C SOLVE THE EIGENVALUE PROBLEM OF ATMOSPHERIC SUBLAYERS.
      DO 6 L=1,NLN
        LB=L
        NAL1=NLGN1(L)
        NDD(L)=NDA
        IUP(L)=L
        IDN(L)=L
        T1=DPT(L)
        T2=DPT(L+1)
        W=OMG(L)
        IF(NPLK1.GT.0) THEN
          DO 7 I=1,NPLK1
    7     CPL(I)=2*PI*(1-W)*CPLK(I,L) + CGLO(i,l)
        ENDIF
CC SCATTERING MEDIA
CC PT, PR
c  =>   EQ12 : On charge le tableau a deux dimension G(NAL1,NLN) dans le tableau
c       a une dimension GBUF(NAL1) pour chaque couche LB
        CALL EQ12(GBUF,G,NAL1,LB,KLGN1)
c  =>   PHAS2 : Calcul des matrices PT et PR telles que :
c               __  m  m     m                         __  m  m     m
c         PT =  >_ Bl Pl(�j)Pl(�i)  avec �i>0    PR =  >_ Bl Pl(�j)Pl(�i)  avec �i<0
c
c              m
c          et Bl = Bl*(2-delta0m)*(l-m)!/(l+m)!     o� Bl = GBUF(NAL1)*(2l+1)
c
c                                             m
c           PT et PR repr�sentent les termes h(�,�') de l'�quation (7) Nakajima et king
c                               -    +
c           ou plus exactement h et h.

        CALL PHAS2(M1,NAL1,NDA,NDA,KNDM,KNDM,KNDM,PT,PR
     &     ,GBUF,PLA,PLA)
CC PT0, PR0
c  =>   PHAS2 : Calcul des matrices PT0 et PR0 telles que :
c                __  m  m     m                          __  m  m     m
c         PT0 =  >_ Bl Pl(�0)Pl(�i)  avec �i>0    PR0 =  >_ Bl Pl(�0)Pl(�i)  avec �i<0
c             
       
        CALL PHAS2(M1,NAL1,NDA,NA0,KNDM,KNDM,KNA0,PT0,PR0
     &     ,GBUF,PLA,PL0)
CC EIGENVALUE PROBLEM
        CALL HOMOG2(M,T1,T2,W,NDA,AMUA,WMM,NA0,AM0
     &      ,PR,PT,PR0,PT0,FSOL,NPLK1,CPL,R,T,ER,ET,ZEIG
     &      ,Q,QI,C11,C22,VP,VM,DP,DM,ERR)
        IF(ERR.NE.' ') RETURN
        CALL EQ32(RE ,R  ,NDA,NDA,LB,KNDM,KNDM,KNLTM,KNDM,KNDM)
        CALL EQ32(TE ,T  ,NDA,NDA,LB,KNDM,KNDM,KNLTM,KNDM,KNDM)
        CALL EQ32(SER,ER ,NDA,NA0,LB,KNDM,KNA0,KNLNM,KNDM,KNA0)
        CALL EQ32(SET,ET ,NDA,NA0,LB,KNDM,KNA0,KNLNM,KNDM,KNA0)
C STORE EXTRA FOR INTERNAL FIELD AT ARBITRARY DEPTH.
        IF(INDT.NE.2 .OR. INDA.NE.2) THEN
          CALL EQ32(QE ,Q  ,NDA,NDA,LB,KNDM,KNDM,KNLN,KNDM,KNDM)
          CALL EQ32(QIE,QI ,NDA,NDA,LB,KNDM,KNDM,KNLN,KNDM,KNDM)
          CALL EQ32(C1E,C11,NDA,NDA,LB,KNDM,KNDM,KNLN,KNDM,KNDM)
          CALL EQ32(C2E,C22,NDA,NDA,LB,KNDM,KNDM,KNLN,KNDM,KNDM)
          CALL EQ32(VPE,VP ,NDA,NA0,LB,KNDM,KNA0,KNLN,KNDM,KNA0)
          CALL EQ32(VME,VM ,NDA,NA0,LB,KNDM,KNA0,KNLN,KNDM,KNA0)
          CALL EQ21(ZEE,ZEIG,NDA,LB,KNDM)
          IF(NPLK1.GT.0) THEN
            CALL EQ32(DPE,DP,NDA,NPLK1,LB,KNDM,KPLK1,KNLN,KNDM,KPLK1)
            CALL EQ32(DME,DM,NDA,NPLK1,LB,KNDM,KPLK1,KNLN,KNDM,KPLK1)
          ENDIF
        ENDIF
    6 CONTINUE
C LAMBERT SURFACE
      NLN1=NLN+1
      IF(INDG.LT.0) THEN
        NLT=NLN
       ELSE
        NLT=NLN1
        NDD(NLT)=NDA
        IUP(NLT)=NLT
        IDN(NLT)=NLT
        T1=DPT(NLT)
        IF(INDG.LE.0)THEN
          CALL GRNDL3(FSOL,GALB,BGND,T1,M,NDA,AMUA,WA,NA0,AM0
     &       ,R,T,ER,ET)
         ELSE
          CALL GRNDO1(INDG,FSOL,GALB,BGND,T1,M,NDA,WMP,NA0,AM0
     &     ,SCR,SCI,R,T,ER,ERT,ET,AMUA,WA)
        ENDIF
        CALL EQ32(RE , R,NDA,NDA,NLT,KNDM,KNDM,KNLTM,KNDM,KNDM)
        CALL EQ32(TE , T,NDA,NDA,NLT,KNDM,KNDM,KNLTM,KNDM,KNDM)
        CALL EQ32(SER,ER,NDA,NA0,NLT,KNDM,KNA0,KNLNM,KNDM,KNA0)
        CALL EQ32(SET,ET,NDA,NA0,NLT,KNDM,KNA0,KNLNM,KNDM,KNA0)
      ENDIF
      NLT1=NLT+1
      NDD(NLT1)=NDA
C ADDING OF THE SUBLAYERS.
      CALL TRN1(NLT,NDD,NA0,IUP,IDN,RE,TE,SER,SET,RUP,RDN,ERR)
      IF(ERR.NE.' ') RETURN
      IF(INDT.EQ.2 .AND. INDA.NE.1) THEN
        IF(INDA.EQ.2) THEN
          DO 8 L=1,NLN1
          DO 8 J=1,NA0
          IF(IMTHD.LE.0 .OR. INDG.LE.0) THEN
            DO 9 I=1,NDA
    9       AI(I,J,L)=RUP(I,J,L)/WMP(I)
           else
            DO 59 I=1,NDA
C Don't change RUP itself, because it is used for flux calculation later
C 97.3.17 Debug By Takashi Nakajima
C            RUP1=RUP(I,J,L)-SER(I,J,NLN1)
C     &                     *EXP(-(DPT(NLN1)-DPT(L))/AMUA(I))
            RUP1=RUP(I,J,L)+(ERT(I,J)-SER(I,J,NLN1))
     &                     *EXP(-(DPT(NLN1)-DPT(L))/AMUA(I))
C
   59       AI(I,J,L)=RUP1/WMP(I)
          ENDIF
          DO 10 I=1,NDA
   10     AI(NA1U+1-I,J,L)=RDN(I,J,L)/WMP(I)
    8     CONTINUE
c
c *** Ajout Matisse
c         write(*,*) 'Calcul fonctions sources aux angles de quadrature'        
c
c   Calcul des fonctions sources AJM(NA1U,NTAU) pour chaque valeur d'angle de quadrature, � 
c   chaque interface (NLN+1 valeurs) et chaque composante de Fourier
c             Nombre de valeurs de l pour la decomposition en polynomes de Legendre : NAL1
c             PL1 = Plm(AM1) , PlA(= Plm(amua)
c             PT1 = P(mu1u,mua) ; PR1 = P(-mu1u,mua) 
c             AM1U = cosinus des angles d'observation
c             AM0  = cosinus de l'angle solaire
c             AMUA = cosinus des angles de la quadrature de GAUSS
c             !!! attention : PLA = PLA*sqrt(W/M) (scaling)
c             !!! attention scaling sur PT1 et PR1 (facteur sqrt(W/M))

c       PLGND ==> polynome de Legendre PL1 calcul� pour les angles de la quadrature AM1

          CALL PLGND(M1,MXLGN1,NA1,KNA1,AM1,PL1)
      
          do L=1,NLN
             NAL1=NLGN1(L)

c       EQ12  ==> On charge le tableau a deux dimension G(NAL1,NLN) dans le tableau
c             a une dimension GBUF(NAL1) pour chaque couche L

             CALL EQ12(GBUF,G,NAL1,L,KLGN1)

             CALL PHAS2(M1,NAL1,NA1,NDA,KNA1,KNA1,KNDM,PT1,PR1,
     &                  GBUF,PL1,PLA)
             W = OMG(L)
             if (L.eq.1) then
                IT = 1
                DO I=1,NA1
                   CALL fnct_source(NA1U,NDA,I,IT,W,PT1,PR1,Ajm,AI,WMM,
     &                              WA,AM1U,INDA)
                enddo
             endif
             IT = L + 1
             DO I=1,NA1
                CALL fnct_source(NA1U,NDA,I,IT,W,PT1,PR1,Ajm,AI,WMM,WA,
     &                           AM1U,INDA)
             enddo
          enddo
c
c *** fin de l'ajout
c    
        ENDIF
        IF(M.EQ.0) THEN
          DO 11 L=1,NLN1
          DO 11 J=1,NA0
          FU1=0
          FD1=0
          EX1=-UTAU(L)/AM0(J)
          TRNS0=EXPFN(EX1)*FSOL
CC FOR SCALED INTENSITY
          DO 12 I=1,NDA
          FU1=FU1+WMP(I)*RUP(I,J,L)
   12     FD1=FD1+WMP(I)*RDN(I,J,L)
          FLXU(J,L)=FU1
          FLXD(J,L)=FD1+TRNS0*AM0(J)
   11     CONTINUE
        ENDIF
      ENDIF

c
c *** Ajout Matisse par PS le 23/01/02
c         write(*,*) 'Calcul fonctions sources aux angles utilisateur'        
c
c   Calcul des fonctions sources AJM(NA1U,NTAU) pour chaque valeur d'angle specifiee par
c   l'utilisateur, chaque interface (NLN+1 valeurs) et chaque composante de Fourier
c             Nombre de valeurs de l pour la decomposition en polynomes de Legendre : NAL1
c             PL1 = Plm(AM1) , PlA(= Plm(amua)
c             PT1 = P(mu1u,mua) ; PR1 = P(-mu1u,mua)
c             AM1U = cosinus des angles d'observation
c             AM0  = cosinus de l'angle solaire
c             AMUA = cosinus des angles de la quadrature de GAUSS
c             !!! attention : PLA = PLA*sqrt(W/M) (scaling)
c             !!! attention scaling sur PT1 et PR1 (facteur sqrt(W/M))
c
c ==> On commence par calculer la luminance aux angles de quadrature

      DO L=1,NLN1
         DO J=1,NA0
             IF (IMTHD.LE.0 .OR. INDG.LE.0) THEN
                DO I=1,NDA

                   AI(I,J,L)=RUP(I,J,L)/WMP(I)
             !      write(5700,*) 'ai,amua,l',AI(I,J,L),amua(I),l
                ENDDO
              else
                DO I=1,NDA
C Don't change RUP itself, because it is used for flux calculation later
C 97.3.17 Debug By Takashi Nakajima
                   RUP1=RUP(I,J,L)+(ERT(I,J)-SER(I,J,NLN1))
     &                  *EXP(-(DPT(NLN1)-DPT(L))/AMUA(I))
C
                  AI(I,J,L)=RUP1/WMP(I)
                  !  write(5700,*) 'ai,amua,l',AI(I,J,L),
   !  &               amua(I),l
                ENDDO
             ENDIF
             DO I=1,NDA
                AI(2*NDA+1-I,J,L)=RDN(I,J,L)/WMP(I)
           !     write(5700,*) 'ai,amua,l',AI(2*NDA+1-I,J,L),
  !   &               -amua(I),l
             ENDDO
         enddo  
      enddo     

c     PLGND ==> polynome de Legendre PL1 calcule pour les angles utilisateurs AM2U
c               avec AM2U = abs (AM1U)

      CALL PLGND(M1,MXLGN1,NA1U,KNA1U,AM2U,PL1)

      do L=1,NLN
         NAL1=NLGN1(L)

c     EQ12  ==> On charge le tableau a deux dimension G(NAL1,NLN) dans le tableau
c            a une dimension GBUF(NAL1) pour chaque couche L

         CALL EQ12(GBUF,G,NAL1,L,KLGN1)

         CALL PHAS2(M1,NAL1,NA1U,NDA,KNA1,KNA1U,KNDM,PT1,PR1,
     &              GBUF,PL1,PLA)
          W = OMG(L)
          if (L.eq.1) then
             IT = 1
             DO I=1,NA1U
                CALL fnct_source(NA1U,NDA,I,IT,W,PT1,PR1,Ajm,AI,WMM,
     &                           WA,AM1U,INDA)
             enddo
          endif
          IT = L + 1
          DO I=1,NA1U
             CALL fnct_source(NA1U,NDA,I,IT,W,PT1,PR1,Ajm,AI,WMM,WA,
     &                        AM1U,INDA)
          enddo
      enddo
c     ! if (ilum.eq.0) return
c
c *** fin de l'ajout
c

c   Modif PS du 20 mars 2007
c   Calcul du flux diffu descendant au niveau du sol pour la calcul de la sphere d'eclairement
c   dans un cas de reflectivit� du sol isotrope en mode ligne de vis�e
      if (M.EQ.0) then
        SPE=0
        LU=IABS(IITAU(NTAU))        
        EX1=-UTAU(LU)/AM0(1)
        TRNS0=EXPFN(EX1)*FSOL
        DO I=1,NDA
           UDN(I,1) = RDN(I,1,LU)/WMP(I)
           SPE=SPE+AMUA(I)*WA(I)*UDN(I,1)
        enddo
          !   write(6403,*) spe
      endif
 
    
c  TEST pour comparaison des flux avec le modele 2 flux
        IF(M.EQ.0) THEN
          DO L=1,NLN1
          FU1=0
          FD1=0
          EX1=-UTAU(L)/AM0(1)
          TRNS0=EXPFN(EX1)*FSOL
CC FOR SCALED INTENSITY
          DO I=1,NDA
             FU1=FU1+WMP(I)*RUP(I,1,L)
             FD1=FD1+WMP(I)*RDN(I,1,L)
          enddo
          FLXU(1,L)=FU1
          FLXD(1,L)=FD1+TRNS0*AM0(J)
          enddo       
        ENDIF
c      return

C--- INTERPOLATION OF THE FIELD.
      IF(INDA.EQ.1) CALL PLGND(M1,MXLGN1,NA1,KNA1,AM1 ,PL1)

      DO 13 L=1,NLN
        LT=L
        LB=LT+1
        DPTH=DPT(LT)
        TAU=DPT(LB)-DPTH
        T1=DPT(LT)
        T2=DPT(LB)
        W=OMG(L)
C INTEGRAL CONSTANTS:  ALFA, BETA
        CALL CINGR(NDA,NA0,LT,AM0,TAU,RDN,RUP,VPE,VME,C1E,C2E
     &   ,NPLK1,DPE,DME,ALFA,BETA)

C FLUXES OR INTENSITY WHEN INDA=2
        IF(M.EQ.0 .OR. INDA.EQ.2) THEN
          DO 14 IT=1,NTAU
          L1=IABS(IITAU(IT))
          IF(L1.EQ.NLN1) L1=NLN
          IF(L1.NE.L) GOTO 14
          DTAU=UTAU(IT)-DPTH
          IF(IITAU(IT).GT.0) THEN
CC IN THE SUBLAYER
            CALL ADISC(M,L1,NDA,NA0,AM0,WMP,TAU,DTAU,ZEE,QE,QIE
     &      ,VPE,VME,ALFA,BETA,NPLK1,DPE,DME,UDN,UUP)
           ELSE
CC JUST INTERFACE
            LU=IABS(IITAU(IT))
            DO 15 J=1,NA0
            DO 15 I=1,NDA
            UDN(I,J)=RDN(I,J,LU)/WMP(I)
   15       UUP(I,J)=RUP(I,J,LU)/WMP(I)
          ENDIF
CC INTENSITY WHEN INDA=2
          IF(INDA.EQ.2) THEN
            DO 18 J=1,NA0
            IF(IMTHD.LE.0 .OR. INDG.LE.0) THEN
              DO 16 I=1,NDA
   16         AI(I,J,IT)=UUP(I,J)
             ELSE
              DO 56 I=1,NDA
C Don't change UUP itself, because it is used for flux calculation later
C 97.3.17 Debug By Takashi Nakajima
C   56         AI(I,J,IT)=UUP(I,J)-SER(I,J,NLN1)/WMP(I)
C     &                  *EXP(-(DPT(NLN1)-UTAU(IT))/AMUA(I))
   56         AI(I,J,IT)=UUP(I,J)+(ERT(I,J)-SER(I,J,NLN1)/WMP(I))
     &                  *EXP(-(DPT(NLN1)-UTAU(IT))/AMUA(I))
C
            ENDIF
            DO 17 I=1,NDA
   17       AI(NA1U+1-I,J,IT)=UDN(I,J)
             
   18       CONTINUE
          ENDIF

CC FLUX
          IF(M.EQ.0) THEN
            DO 19 J=1,NA0
            FU1=0
            FD1=0
            EX1=-UTAU(IT)/AM0(J)
            TRNS0=EXPFN(EX1)*FSOL
            DO 20 I=1,NDA
            FU1=FU1+AMUA(I)*WA(I)*UUP(I,J)
   20       FD1=FD1+AMUA(I)*WA(I)*UDN(I,J)
            FLXU(J,IT)=FU1
   19       FLXD(J,IT)=FD1+TRNS0*AM0(J)
          ENDIF
   14     CONTINUE
        ENDIF
C--- INTENSITIES IN THE USER DEFINED DIRECTIONS (WITHOUT CONTRIBUTION
C     FROM THE INTERFACES).
C PHASE FUNCTIONS FOR ANGULAR INTERPOLATION
        IF(INDA.EQ.1) THEN
          NAL1=NLGN1(L)
          CALL EQ12(GBUF,G,NAL1,LT,KLGN1)
C PT10, PR10
          CALL PHAS2(M1,NAL1,NA1,NA0,KNA1,KNA1,KNA0,PT10,PR10
     &     ,GBUF,PL1,PL0)
C PT1, PR1
          CALL PHAS2(M1,NAL1,NA1,NDA,KNA1,KNA1,KNDM,PT1,PR1
     &     ,GBUF,PL1,PLA)
          DO 21 IU=1,NA1U
C AT INTERFACE.
          I=IIAM1(IU)
          IF(AM1U(IU).GT.0.0) THEN
CC DOWNWARD INCREMENT AT BOTTOM OF SUBLAYER (STAMNES INTEGRATION)
            CALL AINT(M,LT,NDA,NA0,I,FSOL,AM1U(IU),AM0,W,T2,T1,T2
     &       ,ZEE,QE,QIE,VPE,VME,PT10,PR10,PT1,PR1,ALFA,BETA
     &       ,NPLK1,DPE,DME,CPLK,AIB,CGLO)
            DO 22 J=1,NA0
   22       AII(IU,J,LB)=AIB(J)
           ELSE
CC UPWARD INCREMENT AT TOP OF SUBLAYER (STAMNES INTEGRATION)
            CALL AINT(M,LT,NDA,NA0,I,FSOL,AM1U(IU),AM0,W,T1,T1,T2
     &       ,ZEE,QE,QIE,VPE,VME,PT10,PR10,PT1,PR1,ALFA,BETA
     &       ,NPLK1,DPE,DME,CPLK,AIB,CGLO)
            DO 23 J=1,NA0
   23       AII(IU,J,L)=AIB(J)
          ENDIF
C AT USER DEFINED DEPTH
          DO 24 IT=1,NTAU
          L1=IITAU(IT)
CC  WE DO NOT CALCULATE THE FIELD AT THIS STAGE IF THE USER DEFINED
CC     DEPTH IS EXACTLY ON THE INTERFACES (L1.LT.0).
          IF(L1.EQ.L) THEN
            CALL AINT(M,LT,NDA,NA0,I,FSOL,AM1U(IU),AM0,W,UTAU(IT),T1,T2
     &      ,ZEE,QE,QIE,VPE,VME,PT10,PR10,PT1,PR1,ALFA,BETA
     &      ,NPLK1,DPE,DME,CPLK,AIB,CGLO)
            DO 25 J=1,NA0
   25       AI(IU,J,IT)=AIB(J)
          ENDIF
   24     CONTINUE
   21     CONTINUE
        ENDIF
   13 CONTINUE
      IF(INDA.LE.0 .OR. INDA.EQ.2) RETURN
C--- ADDING INTENSITIES (INDA=1)
      DO 26 IU=1,NA1U
      I=IIAM1(IU)
      IF(AM1U(IU).GT.0.0) THEN
CC DOWNWARD
        DO 27 J=1,NA0
   27   AIB(J)=0
        DO 28 L=1,NLN1
        IF(L.GT.1) THEN
           EX1=-(DPT(L)-DPT(L-1))/AM1(I)
           EX1=EXPFN(EX1)
          DO 29 J=1,NA0
   29     AIB(J)=AIB(J)*EX1+AII(IU,J,L)
        ENDIF
        DO 30 IT=1,NTAU
        LU=IITAU(IT)
        IF(IABS(LU).EQ.L) THEN
          IF(LU.LT.0) THEN
            DO 31 J=1,NA0
   31       AI(IU,J,IT)=AIB(J)
           ELSE
            DTAU=UTAU(IT)-DPT(L)
            EX1=-DTAU/AM1(I)
            EX1=EXPFN(EX1)
            DO 32 J=1,NA0
   32       AI(IU,J,IT)=AI(IU,J,IT)+AIB(J)*EX1
          ENDIF
        ENDIF
   30   CONTINUE
   28   CONTINUE
CC UPWARD ADDING
       ELSE
        DO 33 J=1,NA0
C 93.5.4
        IF(INDG.LT.0) then
          AIB(J)=0
         ELSE
          IF(INDG.EQ.0) THEN
            AIB(J)=RUP(1,J,NLN1)/WMP(1)
           else
C Interpolation of reflected intensities subtracted single scattering
            IF(IMTHD.LE.0) then
              DO 51 K=1,NDA
              K1=NDA-K+1
C 94.5.7 Bug  YY(K1)=RUP(K,J,NLN1)
C Corrected
              YY(K1)=RUP(K,J,NLN1)/WMP(K)
   51         XX(K1)=AMUA(K)
             else
              DO 50 K=1,NDA
              K1=NDA-K+1
C 94.5.7 Bug  YY(K1)=RUP(K,J,NLN1)-SER(K,J,NLN1)
C Corrected
C 97.3.17 Debug By Takashi Nakajima
C              YY(K1)=(RUP(K,J,NLN1)-SER(K,J,NLN1))/WMP(K)
              YY(K1)=(RUP(K,J,NLN1)-SER(K,J,NLN1))/WMP(K)
     &        + ERT(K,J)/WMP(K)
   50         XX(K1)=AMUA(K)
            endif
            CALL CSPL1(NDA,XX,YY,A,B,C,D)
            X1=AM1(I)
C 94.5.7 Bug  AIB(J)=CSPLI(X1,NDA,XX,A,B,C,D)/WMP(1)
C Corrected
            AIB(J)=CSPLI(X1,NDA,XX,A,B,C,D)
          ENDIF
        ENDIF
C 93.5.4 end
   33   CONTINUE
        DO 34 L=NLN1,1,-1
        IF(L.LE.NLN) THEN
           EX1=-(DPT(L+1)-DPT(L))/AM1(I)
           EX1=EXPFN(EX1)
          DO 35 J=1,NA0
   35     AIB(J)=AIB(J)*EX1+AII(IU,J,L)
        ENDIF
        DO 36 IT=1,NTAU
        LU=IITAU(IT)
        IF(LU.LT.0) THEN
          IF(IABS(LU).EQ.L) THEN
            DO 37 J=1,NA0
   37       AI(IU,J,IT)=AIB(J)
          ENDIF
         ELSE
          IF(LU.EQ.L-1) THEN
            DTAU=DPT(L)-UTAU(IT)
            EX1=-DTAU/AM1(I)
            EX1=EXPFN(EX1)
            DO 38 J=1,NA0
   38       AI(IU,J,IT)=AI(IU,J,IT)+AIB(J)*EX1
          ENDIF
        ENDIF
   36   CONTINUE
   34   CONTINUE
      ENDIF

   26 CONTINUE

      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE HOMOG2(M,T1,T2,OMG,N1,AM1,WMM,N2,AM2
     &    ,PR,PT,PR0,PT0,FSOL,NPLK1,CPLK,R,T,ER,ET,ZEIG
     &    ,Q,QI,C11,C22,VP,VM,DP,DM,ERR)

      include 'precision.f'

C$NAME  HOMOG2
C$LINK  AXB  CPCON  CSFN  EXPFN  GETQM  TNVSS2
C SOLVE THE TRANSFER IN A HOMOGENEOUS SCATTERING AND EMITTING
C  MEDIUM BY THE DISCRETE ORDINATE METHOD.
C--- HISTORY
C 89.10.31 CREATED FROM HOMOG1 INCLUDING THERMAL RADIATION.
C 95.11.22 EPS=CCP(1)*10 -> EPS=EXP(LOG(CCP(1))*0.8)
C--- INPUT
C T1         R       OPTICAL DEPTH AT THE LAYER TOP.
C T2         R       OPTICAL DEPTH AT THE LAYER BOTTOM.
C OMG        R       SINGLE SCATTERING ALBEDO.
C N1         I       NO. OF THE QUADRATURE STREAMS.
C AM1     R(KNDM)    MU (I), I=1,N1.
C WMM     R(KNDM)    SQRT(W1/M1)
C N2         I       NO. OF THE SOLAR DIRECTIONS.
C AM2     R(KNA0)    MU0(I), I=1,N2.
C PR      R(KNDM,    SCALED P+-(I,J) I,J=1,N1
C           KNDM)
C PT                 SCALED P++(I,J)
C PR0     R(KNDM,    SCALED P0+-(I,J)  I=1,N1; J=1,N2.
C PT0       KNDM)    SCALED P0++(I,J)  I=1,N1; J=1,N2.
C FSOL       R       SOLAR IRRADIANCE AT THE TOP OF THE SYSTEM.
C NPLK1      I       MAX ORDER OF PLANK FUNCTION EXPANSION BY TAU + 1.
C                      IF NPLK1=0 THEN NO THERMAL.
C CPLK    R(NPLK1)   2*PI*(1-W)*B(N)
C--- OUTPUT
C R       R(KNDM,    REFLECTION   MATRIX   RIJ, I,J=1,N1.
C T         KNDM)    TRANSMISSION MATRIX   TIJ.
C ER      R(KNDM,    UPGOING   SOURCE MATRIX EU(I,J), I=1,N1; J=1,N2.
C ET        KNA0)    DOWNGOING SOURCE MATRIX ED(I,J).
C ZEIG    R(KNDM)    ROOT OF THE EIGENVALUES OF Z.
C Q                  Q-MATRIX
C QI                 INVERSE OF Q
C C11                C1 = INVERSE OF 2A-
C C22                C2 = INVERSE OF 2B-
C VP                 VS+
C VM                 VS-
C DP   R(KNDM,KPLK1) THERMAL EMISSION INTENSITY EXAPNSION (DNWARD)
C DM   R(KNDM,KPLK1) THERMAL EMISSION INTENSITY EXAPNSION (UPWARD)
C ERR      C*64      ERROR INDEX
C--- PARAMETER
C KNDM       I       DECLARED SIZE FOR NDA
C KNA0       I       DECLARED SIZE FOR NA0
C KPLK1      I       DECLARED SIZE FOR NPLK1
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNDM=8,KNA0=2,KPLK1=4)
C$PSET GETQM.KNDM=KNDM
      PARAMETER (PI=3.141592653)
C--- AREAS FOR THE ROUTINE.
      CHARACTER ERR*(*)
      DIMENSION AM1(KNDM),WMM(KNDM),AM2(KNA0)
     &,PR(KNDM,KNDM),PT(KNDM,KNDM),PR0(KNDM,KNA0),PT0(KNDM,KNA0)
     &,CPLK(KPLK1),ZEIG(KNDM),R(KNDM,KNDM),T(KNDM,KNDM)
     &,ER(KNDM,KNA0),ET(KNDM,KNA0),Q(KNDM,KNDM),QI(KNDM,KNDM)
     &,C11(KNDM,KNDM),C22(KNDM,KNDM),VP(KNDM,KNA0),VM(KNDM,KNA0)
     &,DP(KNDM,KPLK1),DM(KNDM,KPLK1)
C--- WORKING AREAS
      PARAMETER (KROWIJ=KNDM*(KNDM+1)/2,KNDM2=2*KNDM)
      DIMENSION CCP(3),X(KNDM,KNDM),Y(KNDM,KNDM),XI(KNDM,KNDM)
     &,SP(KNDM,KNA0),SM(KNDM,KNA0),G1(KNDM,KNA0),GAM(KNDM,KNA0)
     &,E0(KNA0),C(KNDM),SL(KNDM),SL1(KNDM)
     &,AP(KNDM,KNDM),AM(KNDM,KNDM),BP(KNDM,KNDM),BM(KNDM,KNDM)
     &,IW(KNDM2),DP0(KNDM),DP1(KNDM),DM0(KNDM),DM1(KNDM)
C PRECISION
      CALL CPCON(CCP)
C      EPS=CCP(1)*10
      EPS=EXP(LOG(CCP(1))*0.8)
C      
      ERR=' '
      TAU=T2-T1
C X, Y MATRICES   �quations 
      DO 1 I=1,N1
        DO 2 J=1,N1
        X(I,J)=-OMG*(PT(I,J)-PR(I,J))
    2   Y(I,J)=-OMG*(PT(I,J)+PR(I,J))
        X(I,I)=1.0/AM1(I)+X(I,I)
    1   Y(I,I)=1.0/AM1(I)+Y(I,I)
C DECOMPOSITION OF XY
      IF(M.EQ.0 .AND. 1.0-OMG.LE.EPS) THEN
        IW0=1
       ELSE
        IW0=0
      ENDIF
      IF(M.EQ.0 .AND. IW0.EQ.0 .AND. NPLK1.GT.0) THEN
        IPK=1
       ELSE
        IPK=0
      ENDIF
      CALL GETQM(IW0,N1,X,Y,ZEIG,Q,QI,XI,IMN,ERR)  ! calcul de lambda, valeur propre (eq 21)
      IF(ERR.NE.' ') RETURN
C THERMAL SOURCE
CC C
      IF(IPK.EQ.0) THEN
        IF(NPLK1.GT.0) THEN
          DO 25 J=1,NPLK1
          DO 25 I=1,N1
          DP(I,J)=0
   25     DM(I,J)=0
        ENDIF
       ELSE
        DO 3 I=1,N1
          SUM1=0
          DO 4 J=1,N1
    4     SUM1=SUM1+Q(J,I)*WMM(J)
          DO 5 J=NPLK1,1,-1
          IF(J+2.GT.NPLK1) THEN
            CPLK1=0
            ELSE
            CPLK1=DP(I,J+2)
          ENDIF
    5     DP(I,J)=((J+1)*J*CPLK1+SUM1*CPLK(J))/ZEIG(I)**2
    3   CONTINUE
CC D
        CALL AXB(DM,Q,DP,N1,N1,NPLK1,KNDM,KNDM,KNDM)
        DO 6 J=1,NPLK1
          DO 7 I=1,N1
          SUM=0
          IF(J+1.LE.NPLK1) THEN
            DO 8 K=1,N1
    8       SUM=SUM+QI(K,I)*DP(K,J+1)
          ENDIF
          DP(I,J)=DM(I,J)-J*SUM
    7     DM(I,J)=DM(I,J)+J*SUM
    6   CONTINUE
      ENDIF
C SIGMA+ - (FOR SINGLE SCATTERING)
      DO 9 I=1,N1
      DO 9 J=1,N2
      SP(I,J)=OMG*(PT0(I,J)+PR0(I,J))
    9 SM(I,J)=OMG*(PT0(I,J)-PR0(I,J))
C LOWER G
      DO 10 I=1,N1
      DO 10 J=1,N2
      SUM=0
      DO 11 K=1,N1
   11 SUM=SUM+X(I,K)*SP(K,J)
   10 G1(I,J)=-SUM-SM(I,J)/AM2(J)
C GAMMA
      CALL AXB(GAM,QI,G1,N1,N1,N2,KNDM,KNDM,KNDM)
      DO 12 I=1,N1
      DO 12 J=1,N2
   12 GAM(I,J)=GAM(I,J)/(1.0/AM2(J)**2-ZEIG(I)**2)
C VS+ AND -
      DO 14 J=1,N2
      TRNS0=-T1/AM2(J)
      TRNS0=EXPFN(TRNS0)*FSOL
      DO 14 I=1,N1
      SUM1=0.0
      SUM2=0.0
      DO 13 K=1,N1
      SUM1=SUM1+Q (I,K)*GAM(K,J)
   13 SUM2=SUM2+QI(K,I)*GAM(K,J)/AM2(J)+XI(I,K)*SM(K,J)
      VP(I,J)=(SUM1+SUM2)/2.0*TRNS0
   14 VM(I,J)=(SUM1-SUM2)/2.0*TRNS0
C E0
      DO 15 I=1,N2
      EX1=-TAU/AM2(I)
   15 E0(I)=EXPFN(EX1)
C BASE FUNCTION  C(TAU) AND S(TAU).
      DO 16 I=1,N1
   16 CALL CSFN(TAU,TAU,ZEIG(I),C(I),SL(I),SL1(I))
C A+-, B+-
      DO 17 I=1,N1
      DO 17 J=1,N1
      SUM1=Q (I,J)*C  (J)
      SUM2=QI(J,I)*SL (J)
      SUM3=Q (I,J)*SL1(J)
      SUM4=QI(J,I)*C  (J)
      AP(I,J)=SUM1-SUM2
      AM(I,J)=SUM1+SUM2
      BP(I,J)=SUM3-SUM4
   17 BM(I,J)=SUM3+SUM4
C C11 AND C22 -> THEIR INVERSION.
      DO 18 I=1,N1
      DO 18 J=1,N1
      C11(I,J)=2.0*AM(I,J)
   18 C22(I,J)=2.0*BM(I,J)
      CALL TNVSS2(N1,C11,DT,0.0d0,KNDM,IW,ERR)
      IF(ERR.NE.' ') THEN
        ERR='ERROR TO GET -C11- (HOMOG2)'
        RETURN
      ENDIF
      CALL TNVSS2(N1,C22,DT,0.0d0,KNDM,IW,ERR)
      IF(ERR.NE.' ') THEN
        ERR='ERROR TO GET -C22- (HOMOG2)'
        RETURN
      ENDIF
C R, T MATRICES
      DO 19 I=1,N1
      DO 19 J=1,N1
      SUM1=0
      SUM2=0
      DO 20 K=1,N1
      SUM1=SUM1+AP(I,K)*C11(K,J)
   20 SUM2=SUM2+BP(I,K)*C22(K,J)
      R(I,J)=SUM1+SUM2
   19 T(I,J)=SUM1-SUM2
C ER, ET MATRICES
      DO 22 I=1,N1
      DP1(I)=0
      DM1(I)=0
      IF(IPK.EQ.1) THEN
        DO 21 J=1,NPLK1
        DP1(I)=DP1(I)+DP(I,J)*TAU**(J-1)
   21   DM1(I)=DM1(I)+DM(I,J)*TAU**(J-1)
        DP0(I)=DP(I,1)
        DM0(I)=DM(I,1)
       ELSE
        DP0(I)=0
        DM0(I)=0
      ENDIF
   22 CONTINUE
      DO 23 J=1,N2
      DO 23 I=1,N1
      SUM1=0
      SUM2=0
      DO 24 K=1,N1
      VP0=VP(K,J)      +DP0(K)
      VM1=VM(K,J)*E0(J)+DM1(K)
      SUM1=SUM1+R(I,K)*VP0+T(I,K)*VM1
   24 SUM2=SUM2+T(I,K)*VP0+R(I,K)*VM1
      ER(I,J)=VM(I,J)      +DM0(I)-SUM1
   23 ET(I,J)=VP(I,J)*E0(J)+DP1(I)-SUM2
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE GETQM(IW0,N,X,Y,ZEIG,Q,QI,XI,IMN,ERR)

      include 'precision.f'

C$NAME GETQM
C$LINK  AXB  SYMTRX
C SOLVE    XY = Q ZEIG**2 INVERSE(Q)
C ROOT DECOMPOSITION METHOD
C--- HISTORY
C 89. 8. 4 CREATED
C--- INPUT
C IW0       I        IF 1 THEN RENOMALIZATION (FOR M=0 AND W0=1)
C N         I        ORDER OF MATRICES
C X     R(KNDM,KNDM) SYMMETRIC MATRIX
C Y     R(KNDM,KNDM) SYMMETRIC MATRIX
C--- OUTPUT
C ZEIG    R(KNDM)    SQRT(EIGENVALUE)
C Q     R(KNDM,KNDM) ROTATION MATIX
C QI    R(KNDM,KNDM) INVERSE OF Q
C XI    R(KNDM,KNDM) INVERSE OF X
C IMN       I        LOCATION OF MINIMUM EIGENVALUE
C ERR     C*64       ERROR INDICATER
C--- PRPC-PARAMETER
C KNDM      I        DECLARED SIZE OF MATRICES
C
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNDM=8)
      CHARACTER ERR*(*)
      DIMENSION X(KNDM,KNDM),Y(KNDM,KNDM),ZEIG(KNDM),Q(KNDM,KNDM)
     & ,QI(KNDM,KNDM),XI(KNDM,KNDM)
C WORKING AREA
      PARAMETER (KROWIJ=(KNDM*(KNDM+1))/2)
      DIMENSION V(KNDM,KNDM),SQX(KNDM,KNDM),SQXI(KNDM,KNDM)
     & ,ROWIJ(KROWIJ),WK(KNDM)
C ROOT DECOMPOSITION OF X (USE V AND ZEING FOR U AND XEIG).
      ERR=' '
      K=0
      DO 1 I=1,N
      DO 1 J=1,I
      K=K+1
    1 ROWIJ(K)=X(I,J)
      CALL SYMTRX(ROWIJ,N,ZEIG,V,KNDM,WK,IERR)
      IF(IERR.GT.128) THEN
        ERR='ERROR IN DECOMPOSITION OF X IN GETQM'
        RETURN
      ENDIF
      DO 2 I=1,N
      IF(ZEIG(I).LE.0.0) THEN
        ERR='NON-POSITIVE EIGENVALUE OF X'
        RETURN
      ENDIF
    2 ZEIG(I)=SQRT(ZEIG(I))
      DO 5 I=1,N
      DO 5 J=1,N
      SUM1=0
      SUM2=0
      SUM3=0
      DO 6 K=1,N
      SUM1=SUM1+V(I,K)*ZEIG(K)*   V(J,K)
      SUM2=SUM2+V(I,K)/ZEIG(K)*   V(J,K)
    6 SUM3=SUM3+V(I,K)/ZEIG(K)**2*V(J,K)
      SQX (I,J)=SUM1
      SQXI(I,J)=SUM2
    5 XI  (I,J)=SUM3
      CALL AXB(V,SQX,Y,N,N,N,KNDM,KNDM,KNDM)
      CALL AXB(Q,V,SQX,N,N,N,KNDM,KNDM,KNDM)
C ROOT DECOMPOSITION OF Z (USE Q FOR Z).
      K=0
      DO 7 I=1,N
      DO 7 J=1,I
      K=K+1
    7 ROWIJ(K)=Q(I,J)
      CALL SYMTRX(ROWIJ,N,ZEIG,V,KNDM,WK,IERR)
      IF(IERR.GT.128) THEN
        ERR='ERROR IN DECOMPOSITION OF Z IN GETQM'
        RETURN
      ENDIF
C CHECK MINIMUM EIGENVALUE
      IMN=1
      ZMN=ZEIG(1)
      IF(N.GE.2) THEN
        DO 8 J=2,N
        IF(ZEIG(J).LT.ZMN) THEN
          IMN=J
          ZMN=ZEIG(J)
        ENDIF
    8   CONTINUE
      ENDIF
C RENORMALIZATION
      IF(IW0.EQ.1) ZEIG(IMN)=0
C
      DO 9 I=1,N
      IF(ZEIG(I).LT.0.0) THEN
        ERR='NON-POSITIVE EIGENVALUE OF Z'
        RETURN
      ENDIF
    9 ZEIG(I)=SQRT(ZEIG(I))
C Q-MATRICES
      DO 10 I=1,N
      DO 10 J=1,N
      SUM1=0
      SUM2=0
      DO 11 K=1,N
      SUM1=SUM1+SQX(I,K)*V(K,J)
   11 SUM2=SUM2+V(K,I)*SQXI(K,J)
      Q (I,J)=SUM1
   10 QI(I,J)=SUM2
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE CINGR(NDA,NA0,L,AM0,TAU,RDN,RUP,VPE,VME,C1E,C2E
     &  ,NPLK1,DPE,DME,ALFA,BETA)

      include 'precision.f'

C$NAME CINGR
C$LINK  EXPFN
C GET ALFA AND BETA (INTEGRAL CONSTANTS).
C PARAMETERS
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNA0=2,KNDM=8,KNLN=34,KPLK1=4)
C
      PARAMETER (KNLN1=KNLN+1)
      PARAMETER (KNLNM=KNLN1,KNLNM1=KNLNM+1)
      PARAMETER (PI=3.141592653,RAD=PI/180.0)
      DIMENSION AM0(KNA0),ALFA(KNDM,KNA0),BETA(KNDM,KNA0)
     &,RUP(KNDM,KNA0,KNLNM1),RDN(KNDM,KNA0,KNLNM1)
     &,DPE(KNDM,KPLK1,KNLN),DME(KNDM,KPLK1,KNLN)
     &,VPE(KNDM,KNA0,KNLN),VME(KNDM,KNA0,KNLN)
     &,C1E(KNDM,KNDM,KNLN),C2E(KNDM,KNDM,KNLN)
C WORK AREAS
      DIMENSION BUF1(KNDM),BUF2(KNDM)
C
      L1=L+1
      DO 1 J=1,NA0
        EX1=-TAU/AM0(J)
        EX1=EXPFN(EX1)
        DO 2 I=1,NDA
        SUM1=RDN(I,J,L)-VPE(I,J,L)
        SUM2=RUP(I,J,L1)-VME(I,J,L)*EX1
        IF(NPLK1.GT.0) THEN
          SUM1=SUM1-DPE(I,1,L)
          SUM=0
          DO 3 K=1,NPLK1
    3     SUM=SUM+DME(I,K,L)*TAU**(K-1)
          SUM2=SUM2-SUM
        ENDIF
        BUF1(I)=SUM2+SUM1
    2   BUF2(I)=SUM2-SUM1
        DO 4 I=1,NDA
        SUM1=0
        SUM2=0
        DO 5 K=1,NDA
        SUM1=SUM1+C1E(I,K,L)*BUF1(K)
    5   SUM2=SUM2+C2E(I,K,L)*BUF2(K)
        ALFA(I,J)=SUM1
    4   BETA(I,J)=SUM2
    1 CONTINUE
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE ADISC(M,L,NDA,NA0,AM0,WMP,TC,T1,ZEE,QE,QIE
     &  ,VPE,VME,ALFA,BETA,NPLK1,DPE,DME,UDN,UUP)

      include 'precision.f'

C$NAME  ADISC
C$LINK  CSFN  EXPFN
C INTENSITY AT A USER DEFINED DEPTH.
C--- HISTORY
C 87. 3. 9
C 89.11. 2 MODIFIED
C--- INPUT
C M        I          FOURIER ORDER
C L        I          LAYER NUMBER.
C NDA      I          NUMBER OF QUADRATURE POINTS.
C NA0      I          NUMBER OF SOLAR ZENITH ANGLES.
C AM0    R(KNA0)      COS(SOLAR ZENITH ANGLES).
C WMP    R(KNDM)      SQRT(W*M)
C TC       R          OPTICAL THICKNESS OF THE LAYER.
C T1       R          OPITCAL DEPTH OF INTERPOLATION MEASURED FROM TOP.
C ZEE    R(KNDM,KNLN)  EIGENVALUES
C QE    R(KNDM,KNDM,KNLN)
C QIE   R(KNDM,KNDM,KNLN)
C VPE   R(KNDM,KNA0,KNLN)
C VME   R(KNDM,KNA0,KNLN)
C DPE   R(KNDM,KPLK1,KNLN)
C DME   R(KNDM,KPLK1,KNLN)
C ALFA  R(KNDM,KNA0)  INTEGRAL CONSTANT ALFA.
C BETA  R(KNDM,KNA0)  INTEGRAL CONSTATN BETA.
C--- OUTPUT
C UUP   R(KNDM,KNA0)  UPWARD INTENSITY (UNSCALE)
C UDN   R(KNDM,KNA0)  DNWARD INTENSITY (UNSCALE)
C
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNLN=34,KNA0=2,KNDM=8,KPLK1=4)
C--- AREAS FOR THE ROUTINE
      DIMENSION AM0(NA0),ZEE(KNDM,KNLN),WMP(KNDM)
     &,QE(KNDM,KNDM,KNLN),QIE(KNDM,KNDM,KNLN)
     &,VPE(KNDM,KNA0,KNLN),VME(KNDM,KNA0,KNLN)
     &,ALFA(KNDM,KNA0),BETA(KNDM,KNA0)
     &,DPE(KNDM,KPLK1,KNLN),DME(KNDM,KPLK1,KNLN)
     &,UUP(KNDM,KNA0),UDN(KNDM,KNA0)
C--- WORK AREAS
      DIMENSION C(KNDM),SL(KNDM),SL1(KNDM),E0(KNA0)
     & ,AP(KNDM),AM(KNDM),BP(KNDM),BM(KNDM)
C A+-, B+-
      DO 1 I=1,NDA
    1 CALL CSFN(TC,T1,ZEE(I,L),C(I),SL(I),SL1(I))
      DO 2 J=1,NA0
      EX1=-T1/AM0(J)
    2 E0(J)=EXPFN(EX1)
C U+ = S1D,  U- = S1U
      DO 3 I=1,NDA
        DO 4 J=1,NDA
        AP(J)=QE(I,J,L)*C  (J) - QIE(J,I,L)*SL(J)
        AM(J)=QE(I,J,L)*C  (J) + QIE(J,I,L)*SL(J)
        BP(J)=QE(I,J,L)*SL1(J) - QIE(J,I,L)*C (J)
    4   BM(J)=QE(I,J,L)*SL1(J) + QIE(J,I,L)*C (J)
        DP=0
        DM=0
        IF(M.EQ.0 .AND. NPLK1.GT.0) THEN
          TAUN=1
          DO 5 J=1,NPLK1
          DP=DP+DPE(I,J,L)*TAUN
          DM=DM+DME(I,J,L)*TAUN
    5     TAUN=TAUN*T1
        ENDIF
        DO 3 J=1,NA0
        SUM1=0
        SUM2=0
        DO 6 K=1,NDA
        SUM1=SUM1+AP(K)*ALFA(K,J)+BP(K)*BETA(K,J)
    6   SUM2=SUM2+AM(K)*ALFA(K,J)+BM(K)*BETA(K,J)
        UDN(I,J)=(SUM1+VPE(I,J,L)*E0(J)+DP)/WMP(I)
        UUP(I,J)=(SUM2+VME(I,J,L)*E0(J)+DM)/WMP(I)
    3 CONTINUE
      RETURN
      END
c
c-------------------------------------------------------------------------------
c
      SUBROUTINE AINT(M,L,NDA,NA0,I,FSOL,AM1U,AM0,W,T,T1,T2
     &     ,ZEE,QE,QIE,VPE,VME,PT10,PR10,PT1,PR1,ALFA,BETA
     &     ,NPLK1,DPE,DME,CPLK,AI,CGLO)

      include 'precision.f'

C$NAME AINT
C$LINK EXINT CSINT
C--- INPUT
C M       I         FOURIER ORDER
C L       I         LAYER NUMBER
C NDA
C NA0
C FSOL
C I       I         STREAM NUMBER FOR AM1(I)
C AM1U    R         USER DEFINED DIRECTION
C AM0   R(KNA0)
C W       R         SINGLE SCATTERING ALBEDO
C T       R         OPTICAL DEPTH FOR INTERPOLATION
C T1      R         OPTICAL DEPTH AT THE TOP OF THE SUBLAYER
C T2      R         OPTICAL DEPTH AT THE BOTTOM OF THE SUBLAYER.
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNDM=8,KNA1U=8,KNA1=4,KNA0=2,KNLN=34,KPLK1=4)
C
      PARAMETER (PI=3.141592654)
      DIMENSION AM0(KNA0),ZEE(KNDM,KNLN),QE(KNDM,KNDM,KNLN)
     & ,QIE(KNDM,KNDM,KNLN),VPE(KNDM,KNA0,KNLN),VME(KNDM,KNA0,KNLN)
     & ,PT10(KNA1,KNA0),PR10(KNA1,KNA0),PT1(KNA1,KNDM),PR1(KNA1,KNDM)
     & ,DPE(KNDM,KPLK1,KNLN),DME(KNDM,KPLK1,KNLN),AI(KNA0)
     & ,ALFA(KNDM,KNA0),BETA(KNDM,KNA0),CPLK(KPLK1,KNLN)
      real(kind=8) CGLO(KPLK1,KNLN)
C WORKING AREAS
      DIMENSION H1(KNDM),H2(KNDM),C(KNDM),SL(KNDM),SL1(KNDM),EI(KNA0)
     & ,PKI(KPLK1)
      TT1=0
      TT2=T2-T1
      TT =T -T1
      AM1=ABS(AM1U)
      DO 1 J=1,NDA
        SUM1=0
        SUM2=0
        DO 2 K=1,NDA
        SUM1=SUM1+(PT1(I,K)+PR1(I,K))*QE (K,J,L)
    2   SUM2=SUM2+(PT1(I,K)-PR1(I,K))*QIE(J,K,L)
        H1(J)=W*SUM1
    1 H2(J)=W*SUM2
C DOWNWARD INTENSITY
      IF(AM1U.GT.0.0) THEN
        DO 3 J=1,NA0
    3   CALL EXINT(AM1U,AM0(J),TT,TT1,TT,EI(J))
        DO 4 J=1,NDA
    4   CALL CSINT(AM1U,ZEE(J,L),TT2,TT,TT1,TT,C(J),SL(J),SL1(J))
        PDU=0
        IF(M.EQ.0 .AND. NPLK1.GT.0) THEN
          CALL PKINT(AM1U,TT,TT1,TT,NPLK1,PKI)
          DO 6 J=1,NPLK1
          SUM1=0
          DO 5 K=1,NDA
    5     SUM1=SUM1+PT1(I,K)*DPE(K,J,L)+PR1(I,K)*DME(K,J,L)
    6     PDU=PDU+(W*SUM1+2*PI*(1-W)*CPLK(J,L)+CGLO(J,L))*PKI(J)    
        ENDIF
        DO 7 J=1,NA0
        TRNS0=-T1/AM0(J)
        TRNS0=EXPFN(TRNS0)*FSOL
        PVU=PT10(I,J)*TRNS0
        HU=0
        DO 8 K=1,NDA
        HU=HU+(H1(K)*C  (K)-H2(K)*SL(K))*ALFA(K,J)
     &       +(H1(K)*SL1(K)-H2(K)*C (K))*BETA(K,J)
    8   PVU=PVU+PT1(I,K)*VPE(K,J,L)+PR1(I,K)*VME(K,J,L)
        PVU=W*PVU*EI (J)
        AI(J)=(HU+PVU+PDU)/AM1
    7   CONTINUE
       ELSE
C UPWARD INTENSITY
        DO 13 J=1,NA0
   13   CALL EXINT(AM1U,AM0(J),TT,TT,TT2,EI(J))
        DO 14 J=1,NDA
   14   CALL CSINT(AM1U,ZEE(J,L),TT2,TT,TT,TT2,C(J),SL(J),SL1(J))
        PDU=0
        IF(M.EQ.0 .AND. NPLK1.GT.0) THEN
          CALL PKINT(AM1U,TT,TT,TT2,NPLK1,PKI)
          DO 16 J=1,NPLK1
          SUM1=0
          DO 15 K=1,NDA
   15     SUM1=SUM1+PR1(I,K)*DPE(K,J,L)+PT1(I,K)*DME(K,J,L)
   16     PDU=PDU+(W*SUM1+2*PI*(1-W)*CPLK(J,L)+CGLO(J,L))*PKI(J)
        ENDIF
        DO 17 J=1,NA0
        TRNS0=-T1/AM0(J)
        TRNS0=EXPFN(TRNS0)*FSOL
        PVU=PR10(I,J)*TRNS0
        HU=0
        DO 18 K=1,NDA
        HU=HU+(H1(K)*C  (K)+H2(K)*SL(K))*ALFA(K,J)
     &       +(H1(K)*SL1(K)+H2(K)*C (K))*BETA(K,J)
   18   PVU=PVU+PR1(I,K)*VPE(K,J,L)+PT1(I,K)*VME(K,J,L)
        PVU=W*PVU*EI (J)
        AI(J)=(HU+PVU+PDU)/AM1
   17   CONTINUE
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION EXX(EX,T1,T2,X)

      include 'precision.f'

C$NAME  EXX
C$LINK  CPCON
C EXP(EX)*INTEG(FROM T1 TO T2) DT EXP(XT)
C--- HISTORY
C 89.11. 3 MODIFIED
      SAVE EPS,EXMN,INIT
      DIMENSION CCP(3)
      DATA INIT/1/
      IF(INIT.EQ.1) THEN
        INIT=0
        CALL CPCON(CCP)
        EPS=CCP(1)*100
        EXMN=CCP(2)*0.8*2.3
      ENDIF
      EX1=EX+X*T1
      EX2=EXPFN(EX1)
      T22=T2-T1
      IF(ABS(T22*X).LE.EPS) THEN
         EXX=EX2*(T22+X*T22**2/2.0)
        ELSE
         EX3=EX+X*T2
         IF(EX3.LE.EXMN) THEN
           EXX=-EX2/X
          ELSE
           EXX=(EXP(EX3)-EX2)/X
         ENDIF
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE EXINT(AM1U,AM0,DPT,T1,T2,C)

      include 'precision.f'

C$NAME  EXINT
C$LINK  EXX
C INTEGRAL(FROM T1 TO T2) DT EXP(-(THK-T)/AM1U-T/AM0)
C--- HISTORY
C 90. 1.13 CREATED
C--- INPUT
C AM1U     R      USERDEFINED EMERGENT MU.
C                 IF .GT. 0 THEN DOWN,  IF .LT. 0 THEN UPWARD.
C AM0      R      SOLAR DIRECTION.
C DPT      R      OPTICAL DEPTH FOR INTERPOLATION.
C T1       R      LOWER LIMIT OF INTEGRATION.
C T2       R      UPPER LIMIT OF INTEGRATION.
C--- OUTPUT
C C        R      INTEGRATION.
      EX=-DPT/AM1U
      X=1/AM1U-1/AM0
      C=EXX(EX,T1,T2,X)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE CSINT(AM1U,ZE,THK,DPT,T1,T2,C,SL,SL1)

      include 'precision.f'

C INTEGRAL(FROM T1 TO T2) DT EXP(-(DPT-T)/AM1U) C(T, THK)
C   WHERE C(T, THK) = (EXP(-Z*(THK-T)+EXP(-Z*T))/2
C WE ALSO DEFINE THE INTEGRAL FOR THE FUNCTIONS: S*Z, S/Z.
C$NAME  CSINT
C$LINK  CPCON  EXX
C--- HISTORY
C 90. 1.13 CREATED
C--- INPUT
C AM1U     R      USERDEFINED EMERGENT MU.
C                 IF .GT. 0 THEN DOWN,  IF .LT. 0 THEN UPWARD.
C ZE       R      EIGENVALUE (L).
C THK      R      OPTICAL THICKNESS OF LAYER.
C DPT      R      OPTICAL DEPTH FOR INTERPOLATION.
C T1       R      LOWER LIMIT OF INTEGRATION.
C T2       R      UPPER LIMIT OF INTEGRATION.
C--- OUTPUT
C C        R      INTEGRATION OF C.
C SL       R      INTEGRATION OF S*L.
C SL1      R      INTEGRATION OF S/L.
      SAVE EPS,INIT
      DIMENSION CCP(3)
      DATA INIT/1/
      IF(INIT.EQ.1) THEN
        INIT=0
        CALL CPCON(CCP)
        EPS=CCP(1)*100
      ENDIF
C C
      EX=-DPT/AM1U-ZE*THK
      X=1/AM1U+ZE
      EX1=EXX(EX,T1,T2,X)
      EX=-DPT/AM1U
      X=1/AM1U-ZE
      EX2=EXX(EX,T1,T2,X)
      C =(EX1+EX2)/2
      IF(ABS(ZE*T1).GT.EPS .OR. ABS(ZE*T2).GT.EPS) THEN
        SL1=(EX1-EX2)/2.0/ZE
       ELSE
        EX=(T1-DPT)/AM1U
        EX1=EXPFN(EX)
        EX=(T2-DPT)/AM1U
        EX2=EXPFN(EX)
        EX=-DPT/AM1U
        X=1/AM1U
        EX3=EXX(EX,T1,T2,X)
        SL1=AM1U*(T2*EX2-T1*EX1)-(AM1U+THK/2)*EX3
      ENDIF
      SL =ZE**2*SL1
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE PKINT(AM1U,DPT,T1,T2,NPLK1,C)

      include 'precision.f'

C INTEGRAL(FROM T1 TO T2) DT EXP(-(DPT-T)/AM1U) T**N
C$NAME  PKINT
C$LINK  EXX
C--- HISTORY
C 90. 1.13 CREATED
C--- INPUT
C AM1U     R      USERDEFINED EMERGENT MU.
C                 IF .GT. 0 THEN DOWN,  IF .LT. 0 THEN UPWARD.
C DPT      R      OPTICAL DEPTH FOR INTERPOLATION.
C T1       R      LOWER LIMIT OF INTEGRATION.
C T2       R      UPPER LIMIT OF INTEGRATION.
C NPLK1    I      .GE. 1
C--- OUTPUT
C C     R(NPLK1)  INTEGRATIONS
C
      DIMENSION C(*)
      EX=-DPT/AM1U
      X=1/AM1U
      C(1)=EXX(EX,T1,T2,X)
      IF(NPLK1.GE.2) THEN
        EX=(T1-DPT)/AM1U
        EX1=EXPFN(EX)
        EX=(T2-DPT)/AM1U
        EX2=EXPFN(EX)
        DO 1 N1=2,NPLK1
        EX1=T1*EX1
        EX2=T2*EX2
    1   C(N1)=AM1U*(EX2-EX1-(N1-1)*C(N1-1))
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE CSFN(TC,T,ZE,C,SL,SL1)

      include 'precision.f'

C$NAME CSFN
C$LINK CPCON EXPFN
C GET C SL AND SL1 FUNCTIONS.
C--- HISTORY
C 89.11. 2  CREATED
C---
C 2 C = E(TC-T) + E(T),   2S = E(TC-T) - E(T)
C   E(T) = EXP(-ZE*T)
C SL = S L,   SL1 = S L**-1
C
      SAVE INIT,EPS
      DIMENSION CCP(3)
      DATA INIT/1/
      IF(INIT.EQ.1) THEN
        INIT=0
        CALL CPCON(CCP)
        EPS=CCP(1)*10
      ENDIF
      EXP1=-ZE*(TC-T)
      EXP2=-ZE*T
      EXP3=EXPFN(EXP1)
      EXP4=EXPFN(EXP2)
      C  =(EXP3+EXP4)/2
      S  =(EXP3-EXP4)/2
      SL =ZE*S
      IF(ABS(EXP1).LE.EPS .AND. ABS(EXP2).LE.EPS) THEN
        SL1 =T-TC/2
        ELSE
        SL1 =S/ZE
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION BINTP(XX,N,X,Y)

      include 'precision.f'

C$NAME  BINTP
C 2ND ORDER POLYNOMIAL INTERPOLATION
C--- HISTORY
C 88. 1.14 CREATED
C     3. 7 LINEAR EXTRAPOLATION FOR OUT-OU-BOUNDARY DATA
C--- INPUT
C XX    R     INTERPOLATION POINT
C N     R     NBR OF DATA
C X   R(NN)   INDEPENDENT VARIABLE,  X(I) .LT. X(I+1)
C Y   R(NN)   DEPENDENT   VARIABLE
C--- OUTPUT
C BINTP R     OUTPUT
C$ENDI
C
      DIMENSION X(N),Y(N)
C 1 POINT
      IF(N.LE.1) THEN
      BINTP=Y(1)
      RETURN
      ENDIF
C 2 POINTS OR XX.LE.X(1)
      IF(XX.LE.X(1) .OR. N.EQ.2) THEN
      BINTP=Y(1)+(Y(2)-Y(1))*(XX-X(1))/(X(2)-X(1))
      RETURN
      ENDIF
C XX.GE.XX(N)
      IF(XX.GE.X(N)) THEN
      BINTP=Y(N-1)+(Y(N)-Y(N-1))*(XX-X(N-1))/(X(N)-X(N-1))
      RETURN
      ENDIF
C 3 POINTS
      DO 1 I=1,N
      IF(XX.LE.X(I)) GOTO 2
    1 CONTINUE
      I=N
    2 K1=MAX(1,I-1)
      K2=K1+2
      IF(K2.GT.N) THEN
      K2=N
      K1=K2-2
      ENDIF
C
      BINTP=0.0
      I2=K1
      I3=K1+1
      DO 3 I=1,3
      I1=I2
      I2=I3
      I3=MOD(I+1,3)+K1
    3 BINTP=BINTP+(XX-X(I2))*(XX-X(I3))/(X(I1)-X(I2))/(X(I1)-X(I3))
     &     *Y(I1)
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      SUBROUTINE GTPH4B(IUK,WLC,CR3,CI3,PR,X0,GG,RP
     & ,NANG,IPOL,ANG,PH,CEXT,CABS,CG,VL,INTVL,SZP,ERR)

      include 'precision.f'

C$NAME GTPH4B
C$LINK NONS SMLLOP VLSPC2 BINTP
C GET PHASE FUNCTION AND CROSS SECTION FROM TABLE OF INTERPOLATED REFRACTIVE IND
C WITH LOG-REGULAR GRID WAVELENGTHS.
C CR3,CI3 assigned; Assume IPOL=1
C Pollack and Cuzzi method implemented
C--- HISTORY
C 95. 9.14  Created from GETPH3
C 96. 2.29  2ND ORDER POLYNOMIAL INTERPOLATION (T.Y.NAKAJIMA)
C 96. 3.11  Pollack and Cuzzi method implemented (Noguchi, Tokai)
C 96. 4. 4  DCG(IR)=DCG(IR)+CG1 -> F(INDR.LE.1) then ... else...endif
C--- INPUT
C IUK     I      READ UNIT NUMBER OF THE KERNEL FILE.
C WLC     R      WAVELENGTH IN CM
C CR3     R      Real part of refractive index
C CI3     R      Imaginary part of refractive index (positive value)
C PR   R(6,4)    Parameter packet of size distribution (see VLSPC2)
C X0      R      Critical size parameter for Mie scattering (Pollack-Cuzzi)
C                Give 1.0E9 for Mie theory only calculations.
C GG      R      Asymmetry parameter for transmitted ray (Pollcack-Cuzzi)
C                Any value for Mie theory only calculations.
C RP      R      Surface area fraction to that of sphere (about 1.1-1.3)
C                Any value for Mie theory only calculations.
C--- OUTPUT
C NANG    I      NBR OF SCATTERING ANGLE
C INTVL   I      NBR OF SIZE PARAMETER
C IPOL    I      NBR OF POLARIZATION COMPONENT
C ANG  R(NANG)   SCATTERING ANGLE IN DEGREE
C PH   R(KNANG,IPOL) VOLUME PHASE FUNCTION
C CEXT   R       EXTINCTION CROSS SECTION (cm-1)
C CABS   R       ABSORPTION CROSS SECTION (cm-1)
C CG     R       GEOMETRICAL CROSS SECTION (cm-1)
C VL     R       Volume (cm3/cm3)
C INTVL  I       Number of size interval in the kernel file
C SZP  R(INTVL)
C---
      SAVE
C$PRPC
      include 'parameter.rayjn'
      
c      PARAMETER (KNANG=72,KINTVL=75,KPOL=1)
      PARAMETER (KINTVL=75,KPOL=1)
      PARAMETER (KNANG2=KNANG+2)
      CHARACTER ERR*(*)
      DIMENSION PR(6,4),ANG(KNANG),PH(KNANG,KPOL),SZP(KINTVL)
C$PSET NONS.KNANG=KNANG; NONS.KINTVL=KINTVL; NONS.KPOL=KPOL
C WORKING AREA
      PARAMETER (KSEC=3,KR=20,KI=20,KRF=200)
      PARAMETER (PI=3.141592654,RD=PI/180.0)
      CHARACTER CH*1
      DIMENSION CR(KRF),CI(KRF),Q(KINTVL,KPOL,KRF,KNANG2)
     & ,NR(KSEC),NI(KSEC),CRMN(KSEC),CRMX(KSEC),CIMN(KSEC)
     & ,CIMX(KSEC),CRT(KR,KSEC),CIT(KI,KSEC),ICT(KR,KI,KSEC)
     & ,PHM(KNANG)
CC 96.3.11
      DIMENSION PHS(KNANG),DEXT(KINTVL),DABS(KINTVL),DCG(KINTVL)
CC 96.2.29
      DIMENSION Y(3),XR(3),XI(3),XI2(3),IRF(3,3),ZI(3)
C
      DATA   NR/  3,      9,     18/
      DATA   NI/ 16,     10,      4/
      DATA CRMN/  1.3,    1.0,    1.0/
      DATA CRMX/  1.5,    1.8,    2.7/
      DATA CIMN/  1.0E-9, 1.0E-4, 1.0E-1/
      DATA CIMX/  1.0E-4, 1.0E-1, 1.0E+0/
      DATA INIT/1/
C
      IF(INIT.GT.0) THEN
        INIT=0
        REWIND IUK
C GET KERNEL ANG, SZP, AK, CEXT, CABS
        READ(IUK,*) INTVL, NANG, IPOL
        IF (NANG .GT. KNANG) THEN
          ERR = 'NANG.GT.KNANG'
          RETURN
        END IF
        IF (IPOL .GT. KPOL) THEN
          ERR = 'IPOL.GT.KPOL'
          RETURN
        END IF
        IF(IPOL.NE.1) then
          ERR='Assume only IPOL=1 case in GETPH4'
          return
        endif
        READ(IUK,*) (SZP(I),I=1,INTVL)
        READ(IUK,*) (ANG(I),I=1,NANG)
        DEL=LOG(SZP(INTVL)/SZP(1))/(INTVL-1)
        RSZP2=EXP(DEL/2)
        do 35 I=1,NANG
   35   PHM(I)=3.0/16.0/PI*(1+COS(ANG(I)*RD)**2)
CC READ KERNEL
        NRF=0
    7   READ(IUK,2,END=5,ERR=5) CH
    2   FORMAT(A1)
        NRF=NRF+1
        IF(NRF.GT.KRF) THEN
          ERR='NRF .GT. KRF'
          RETURN
        ENDIF
        READ(IUK,*) CR(NRF),CI(NRF)
        CI(NRF)=ABS(CI(NRF))
        DO 60 I = 1, INTVL
        DO 60 J = 1, IPOL
   60     READ(IUK,*) (Q(I,J,NRF,K), K=1,NANG)
        DO 62 K = 1, 2
   62     READ(IUK,*) (Q(I,1,NRF,K+NANG), I=1,INTVL)
        GOTO 7
    5   CONTINUE
CC GET REFRACTIVE INDEX TABLES (3 TABLES)
        DO 8 K=1,KSEC
        DCR=(CRMX(K)-CRMN(K))/(NR(K)-1)
        DO 9 I=1,NR(K)
        CR1=CRMN(K)+DCR*(I-1)
        IF(ABS(CR1-1.0).LE.0) CR1=1.01
    9   CRT(I,K)=CR1
C_GD        DCI=ALOG(CIMX(K)/CIMN(K))/(NI(K)-1)
        DCI=LOG(CIMX(K)/CIMN(K))/(NI(K)-1)
        DO 10 I=1,NI(K)
        CI1=DCI*(I-1)
   10   CIT(I,K)=CIMN(K)*EXP(CI1)
    8   CONTINUE
CC FIND GRIDS FOR KERNEL REFRACTIVE INDICES
        DO 11 K=1,KSEC
        DO 11 I=1,NR(K)
        DO 11 J=1,NI(K)
   11   ICT(I,J,K)=0
        DO 12 L=1,NRF
        CR1=CR(L)
        CI1=CI(L)
        DO 13 K=1,KSEC
        DO 14 I=1,NR(K)
        IF(ABS(CRT(I,K)/CR1-1).LE.0.01) GOTO 15
   14   CONTINUE
        GOTO 13
   15   DO 17 J=1,NI(K)
        IF(ABS(CIT(J,K)/CI1-1).LE.0.01) GOTO 16
   17   CONTINUE
        GOTO 13
   16   ICT(I,J,K)=L
   13   CONTINUE
   12   CONTINUE
        DO 18 K=1,KSEC
        DO 18 I=1,NR(K)
        DO 18 J=1,NI(K)
        IF(ICT(I,J,K).EQ.0) THEN
          ERR='NO MATCHING GRID'
          RETURN
        ENDIF
   18   CONTINUE
C NUMBER OF AVERAGING
        NAV=10
      ENDIF
C Initialization end
      WVN=2*PI/WLC
C Find the domain
      DO 23 KP=1,KSEC
      IF(CI3.LE.CIMX(KP)) GOTO 25
   23 CONTINUE
      KP=KSEC
   25 NS=NR(KP)
      NT=NI(KP)
      DO 26 IP=1,NS-1
      IF(CR3.LE.CRT(IP,KP)) GOTO 27
   26 CONTINUE
      IP=NS
   27 DO 28 JP=1,NT-1
      IF(CI3.LE.CIT(JP,KP)) GOTO 29
   28 CONTINUE
      JP=NT
   29 IP=MAX(1, IP-1)
      JP=MAX(1, JP-1)
C GET GRID POINTS
CC 96.2.29
      NINTPR=3
      NINTPI=3
      IF(NINTPR.GT.NR(KP))NINTPR=NR(KP)
      IF(NINTPI.GT.NI(KP))NINTPI=NI(KP)
      IPT=IP
      JPT=JP
      IF(IP.GE.NR(KP)-1)IPT=NR(KP)-2
      IF(JP.GE.NI(KP)-1)JPT=NI(KP)-2
      DO 72 IG=1,NINTPR
   72 XR(IG)=CR(ICT(IPT+IG-1,JPT     ,KP))
      DO 73 IG=1,NINTPI
   73 XI(IG)=CI(ICT(IPT,     JPT+IG-1,KP))
      DO 74 IX=1,NINTPR
      DO 74 IY=1,NINTPI
   74 IRF(IX,IY)=ICT(IPT+IX-1,JPT+IY-1,KP)
CC 96.2.29
CC      IRF00=ICT(IP  ,JP  ,KP)
CC      IRF10=ICT(IP+1,JP  ,KP)
CC      IRF01=ICT(IP  ,JP+1,KP)
CC      IRF11=ICT(IP+1,JP+1,KP)
C GET INTERPOLATED OPTICAL CONSTANTS
      CG=0
      VL=0
      CEXT=0
      CABS=0
CC 96.3.11
      DCGL=0.0
      DCGS=0.0
      IX0=0
  100 IX0=IX0+1
      IF((SZP(IX0).LE.X0).AND.(IX0.LE.INTVL)) GOTO 100
      DO 20 L=1,NANG
      PHS(L)=0
CC 96.3.11 end
   20 PH(L,1)=0
CC
      RMIN=PR(1,3)
      RMAX=PR(1,4)
      RMIN1=SZP(1)/WVN/RSZP2
      IF(RMIN1.GT.RMIN) then
        INTVL9=LOG(RMIN1/RMIN)/DEL+1
        R2=RMIN1*EXP(-INTVL9*DEL)
       else
        INTVL9=0
      endif
      INTVL8=INTVL+INTVL9
      DO 30 IR8=1,INTVL8
      IR=IR8-INTVL9
      IF(IR.LE.0) THEN
        INDR=1
        R1=R2
        R2=R1*RSZP2**2
       else
CC 96.3.11
        DCG(IR)=0
        INDR=2
        R1=SZP(IR)/WVN/RSZP2
        R2=SZP(IR)/WVN*RSZP2
      endif
      DR=(R2-R1)/NAV
      R3=R1-DR/2
      V1=0.0
      DO 34 J=1,NAV
      R3=R3+DR
C      X=R3*WVN
      DLR=LOG(R3/(R3-DR))
      PR(1,1)=R3
      V2=VLSPC2(PR)
CC 96.3.11
      CG1=V2*3.0/4.0/R3*DLR
      CG=CG+CG1
      VL=VL+V2*DLR
      V1=V1+V2
CC 96.3.11
      IF(IR.LE.IX0)THEN
       DCGS=DCGS+CG1
      ELSE
       DCGL=DCGL+CG1
      ENDIF
CC 96.4.4
      IF(INDR.LE.1) then
        DCG(1)=DCG(1)+CG1
       else
        DCG(IR)=DCG(IR)+CG1
      endif
CCC 96.4.4 end
CC 96.3.11 end
   34 continue
      V1=V1/REAL(NAV)
C INTERPOLATION
      IF(INDR.EQ.1) then
        X=(R1+R2)/2*WVN
        call SMLLOP(X,CR3,CI3,QEXT9,QSCA9)
        QABS9=QEXT9-QSCA9
        COF=DEL*3.0/4.0/X*V1*WVN
        CEXT=CEXT+QEXT9*COF
        CABS=CABS+QABS9*COF
        do 36 I=1,NANG
   36   PH(I,1)=PH(I,1)+PHM(I)*COF*QSCA9
       else
        DO 31 L=1,NANG+2
C INTERPOLATION TO GET CORRESPONDING Q TO CR3 AND CI3
CC 96.2.29
         DO 80 IG=1,NINTPI
   80    XI2(IG)=LOG(XI(IG))
         DO 81 IX=1,NINTPR
         IC=0
         DO 82 IY=1,NINTPI
         IC=IC+1
         IRFI=IRF(IX,IY)
         Y(IC)= LOG(Q(IR, 1, IRFI, L))
   82    CONTINUE
         PINT=LOG(CI3)
         ZI(IX)= BINTP(PINT,NINTPI,XI2,Y)
   81    CONTINUE
         Z=BINTP(CR3,NINTPR,XR,ZI)
CC 96.2.29
CC        PZ00=LOG(Q(IR,1,IRF00,L))
CC        PZ10=LOG(Q(IR,1,IRF10,L))
CC        PZ01=LOG(Q(IR,1,IRF01,L))
CC        PZ11=LOG(Q(IR,1,IRF11,L))
CC        Z1=PZ00+(CR3-CRT(IP,KP))/(CRT(IP+1,KP)-CRT(IP,KP))*(PZ10-PZ00)
CC        Z2=PZ01+(CR3-CRT(IP,KP))/(CRT(IP+1,KP)-CRT(IP,KP))*(PZ11-PZ01)
CC        Z=Z1+LOG(CI3/CIT(JP,KP))/LOG(CIT(JP+1,KP)/CIT(JP,KP))*(Z2-Z1)
        Z=EXP(Z)*V1*WVN
        IF(L.LE.NANG) THEN
          PH(L,1)=PH(L,1)+Z
CC 96.3.11
          IF(IR.LE.IX0) THEN
           PHS(L)=PHS(L)+Z
          ENDIF
CC 96.3.11 end
         ELSE IF(L.EQ.NANG+1) THEN
          CEXT=CEXT+Z
CC 96.3.11
          DEXT(IR)=Z
        ELSE IF(L.EQ.NANG+2) THEN
          CABS=CABS+Z
CC 96.3.11
          DABS(IR)=Z
        ENDIF
   31   CONTINUE
      endif
   30 continue
CC 96.3.11
      IF(INDR.NE.1) THEN
        CALL      NONS(INTVL,IX0,NANG,NAV,CG,CR3,CI3,DCGS,DCGL,GG
     &               ,RMAX,RMIN,RP,WVN,X0,ANG,DABS,DCG,DEXT,PHS,SZP
     &               ,PH,CEXT,CABS,ERR)
      ENDIF
CC 96.3.11 end
      ERR=' '
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      SUBROUTINE NONS(INTVL,IX0,NANG,NAV,CG,CR1,CI1,DCGS,DCGL,GG
     &               ,RMAX,RMIN,RP,WVN,X0,ANG,DABS,DCG,DEXT,PHS,SZP
     &               ,PH,CEXT,CABS,ERR)

      include 'precision.f'
      include 'parameter.rayjn'

C$NAME NONS
C Semi Empirical Theory
C$PRPC

c      PARAMETER (KNANG=72,KINTVL=75,KPOL=1)
      PARAMETER (KINTVL=75,KPOL=1)
      PARAMETER (KNANG2=KNANG+2)
      PARAMETER (PI=3.141592653,RAD=PI/180.0)
      DIMENSION ANG(KNANG),DABS(KINTVL),DCG(KINTVL),DEXT(KINTVL)
     &         ,PHS(KNANG),SZP(KINTVL),PH(KNANG,KPOL)
      CHARACTER ERR*(*)
C Working Area
      DIMENSION DID(KNANG),DIR(KNANG),DIT(KNANG)
C Semi Empirical Theory
      IF(IX0.GT.INTVL) GOTO 999
      DCB=0
      XMIN=RMIN*WVN
      XMAX=RMAX*WVN
      IMN=0
  107 IMN=IMN+1
      IF(SZP(IMN).LT.XMIN) GOTO 107
      IMX=INTVL+1
  108 IMX=IMX-1
      IF(SZP(IMX).GT.XMAX) GOTO 108
      IF(IMN.NE.1) IMN=IMN-1
      IF(IMX.LE.IX0) GOTO 999
      DM2=CR1**2.0+CI1**2.0
      DO 101 IA=1,NANG
       DIDD=0.0
       AZ=ANG(IA)
       IF(AZ.LT.0.018)THEN
        AZ=0.018
       ENDIF
       IF(AZ.GT.179.982)THEN
        AZ=179.982
       ENDIF
       SN=SIN(AZ*RAD)
       SN2=SIN(AZ*RAD/2.0)
       CS=COS(AZ*RAD)
       CS2=COS(AZ*RAD/2.0)
       DO 102 IR=IX0,IMX
         XB=SQRT(RP)*SZP(IR)
         XBZ=XB*SN
         DJ=0.0
         DO 109 JJ=1,NANG-1
          DX1=ANG(JJ+1)*RAD
          DX=ANG(JJ)*RAD
          DY1=COS(DX1-XBZ*SIN(DX1))
          DY=COS(DX-XBZ*SIN(DX))
          DJ=DJ+(DY1+DY)*(DX1-DX)/2.0
  109    CONTINUE
        dd=RP*XB*XB*DJ*DJ*0.5*(1.0+CS**2.0)/(PI*XBZ*XBZ)*DCG(IR)
        DIDD=DIDD+DD
  102  CONTINUE
       IF(ANG(IA).LE.30.0) THEN
        DID(IA)=DIDD
        DCB=DIDD
       ELSE
        DID(IA)=0
       ENDIF
       SQ=SQRT(DM2-1.0+SN2**2.0)
       DIR(IA)=1.0-2.0*SN2*SQ*(1.0/(SN2+SQ)**2.0+DM2/(DM2*SN2+SQ)**2.0)
       DIT(IA)=EXP(1.0-LOG(GG)/LOG(1.53171*PI)*ANG(IA)*RAD)
  101 CONTINUE
      DO 201 I=1,NANG
        IF(DID(I).GT.DCB) THEN
          DID(I)=DID(I)-DCB
        ELSE
          DID(I)=0
        ENDIF
  201 CONTINUE
      ZS=PHINTG(NANG,ANG,PHS)
      ZD=PHINTG(NANG,ANG,DID) !/4.0/PI
      ZR=PHINTG(NANG,ANG,DIR) !/4.0/PI
      ZT=PHINTG(NANG,ANG,DIT) !/4.0/PI
      DO 104 IA=1,NANG
       PHS(IA)=PHS(IA)/ZS
       DID(IA)=DID(IA)/ZD
       DIR(IA)=DIR(IA)/ZR
       DIT(IA)=DIT(IA)/ZT
  104 CONTINUE
      QSS=0.0
      QSA=0.0
      QLS=0.0
      QLA=0.0
      DO 105 IR=IMN,IMX
       IF(IR.LE.IX0)THEN
        QSS=QSS+DEXT(IR)-DABS(IR)
        QSA=QSA+DABS(IR)
       ELSE
        QLS=QLS+DEXT(IR)-DABS(IR)
        QLA=QLA+DABS(IR)
       ENDIF
  105 CONTINUE
      QSS=QSS/DCGS
      QSA=QSA/DCGS
      QLS=QLS/DCGL
      QLA=QLA/DCGL
      FLS=DCGL/CG
      QS=QLS*FLS*RP+(1-FLS)*QSS
      QA=QLA*FLS+(1-FLS)*QSA
      QE=QS+QA
      QD=1.0
      QR=ZR /4.0/PI
      QT=QLS-QD-QR
      DO 106 IA=1,NANG
       PH(IA,1)=PHS(IA)*(1-FLS)*QSS+RP*FLS
     &  *(DID(IA)*QD+DIR(IA)*QR+DIT(IA)*QT)
       PH(IA,1)=PH(IA,1)*CG
  106 CONTINUE
      CEXT=QE*CG
      CABS=QA*CG
  999 ERR=' '
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c
      FUNCTION PHINTG(NANG,ANG,PH)

      include 'precision.f'
      include 'parameter.rayjn'

C---INPUT
C NANG    I      NBR OF SCATTERING ANGLE
C ANG  R(NANG)   INDEPENDENT VARIABLES (SCAT. ANG.)
C PH   R(NANG)   DEPENDENT VARIABLES
C---OUTPUT
C PHINTG  FR     INTEGRAL
C
c      PARAMETER (KNANG =80)
      PARAMETER (PI=3.141592653,RAD=PI/180.0,PSTD=1013.25)
      DIMENSION ANG(KNANG),PH(KNANG)
      PHINTG=0.0
      DO 100 IA=NANG,2,-1
       DX=COS(ANG(IA-1)*RAD)-COS(ANG(IA)*RAD)
       PHINTG=PHINTG+DX*(PH(IA-1)+PH(IA))/2.0
  100 CONTINUE
      PHINTG=PHINTG*2.0*PI
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c
      FUNCTION RAWB(RH1,R,RHO,NAW,AW,RMMD)

      include 'precision.f'

C$NAME  RAWB
C Growth of wet aerosols
C RAW is r(Aw) in eq.(2) in Shettle and Fenn.
C--- HISTORY
C 94.12.26 CREATED by I. Lensky
C 95. 9.13 Modified by T. Nakajima
C 96. 5. 5 Drop CAW, Number of iterations 10 -> 6
C--- input
C RH1    R           relative humidity 0 <= RH1 < 1
C R      R           dry aerosol radius (cm)
C RHO    R           paricle density relative to water
C NAW    I           Number of AW (If 0 then no growth)
C AW     R(KAW)      water activity
C RMMD   R(KAW)      Hanel's water uptake data
C--- output
C RAW    R           r(aw) mode radius of wet aerosols (cm)
C
C$ENDI
      DIMENSION AW(*),RMMD(*)
C
      IF (NAW.LE.0 .or. RH1.LT.AW(1)) THEN
        RAWB = R
        RETURN
      ENDIF
C iteration
      AW1 = RH1
      DO 3 IT=1,6
C Eq.(5)  of Shettle & Fenn
      IF(AW1.LE.AW(1)) then
        IAW=1
       else IF(AW1.GE.AW(NAW)) then
C        IAW=NAW-1
        IAW=NAW
       else
        DO 1 IAW=1,NAW-1
        IF(AW1.GE.AW(IAW) .AND. AW1.LT.AW(IAW+1)) GOTO 2
    1   CONTINUE
      endif
    2 CAW=LOG(RMMD(IAW+1)/RMMD(IAW))/LOG((1-AW(IAW+1))/(1-AW(IAW)))
      R1=RMMD(IAW)*((1-AW1)/(1-AW(IAW)))**CAW
      RAWB=R*(1+RHO*R1)**(1.0/3.0)
C AW1 is Aw, Eq.(4) in Shettle & Fenn. RAWB in cm.
      AW1=RH1*EXP(-0.001056/(10000*RAWB))
    3 CONTINUE
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      FUNCTION VLSPC2(PR)

      include 'precision.f'

C$NAME  VLSPC2
C Volume spectrum of partile polydisperison: v(x) = dV / d ln r
C--- HISTORY
C 95. 9.14 Created with parameter packet only
C--- INPUT
C PR    R(6,4)      Parameter packet
C
C    PR(1,1)=r       Particle radius in cm
C    PR(1,2)=NMODE   Number of mode radius
C    PR(1,3)=rmin    Minimum particle radius in cm
C    PR(1,4)=rmax    Maximum particle radius in cm
C
C    For each j-th mode (<= 4)
C
C  PR(2,j): Type of function (ITP) for the mode.
C   ITP=1: power law
C     PR(3,j)=C, PR(4,j)=R0,  PR(5,j)=P
C     vj = C * (R/R0)**(4-P) if R>R0; = C * (R/R0)**4 if R<R0
C   ITP=2: log-normal
C     PR(3,j)=C, PR(4,j)=S,   PR(5,j)=RM
C     vj = C * exp((ln(R/RM)/ln(S))**2 / 2)
C   ITP=3: modified gamma
C     PR(3,j)=C, PR(4,j)=ALFA, PR(5,j)=BETA, PR(6,j)=GAMMA
C     vj = C * (R1)**(ALFA+4) exp (-BETA*R1**GAMMA) where R1=R*1.0E4
C--- OUTPUT
C VLSPC2   RF       dV/d ln r
C$ENDI
C
      dimension PR(6,4)
C
      R    =PR(1,1)
      NMODE=PR(1,2)+0.001
      RMIN =PR(1,3)
      RMAX =PR(1,4)
      VLSPC2=0
      IF(R.LT.RMIN .OR. R.GT.RMAX) RETURN
      DO 101 M=1,NMODE
      ITP=PR(2,M)+0.001
      IF(ITP.EQ.2) GOTO 4
      IF(ITP.EQ.3) GOTO 5
C POWER LAW
      C    =PR(3,M)
      RC   =PR(4,M)
      PDNDR=PR(5,M)
      IF(R.LE.RC) THEN
      PN=4.0
      ELSE
      PN=4.0-PDNDR
      ENDIF
      E1=PN*LOG(R/RC)
      GOTO 100
C LOG-NORMAL
    4 C =PR(3,M)
      S =PR(4,M)
      RM=PR(5,M)
      E1=-0.5*(LOG(R/RM)/LOG(S))**2
      GOTO 100
C MODIFIED GAMMA
    5 R1=R*1.0E4
      C  =PR(3,M)
      ALF=PR(4,M)
      BET=PR(5,M)
      GAM=PR(6,M)
      E1=(ALF+4)*LOG(R1)-BET*R1**GAM
  100 IF(E1.GT.-100.0) VLSPC2=VLSPC2+C*EXP(E1)
  101 CONTINUE
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION EXPFN(X)

      include 'precision.f'

C$NAME EXPFN
C$LINK CPCON
C EXPONENTIAL FUNCTION WITH OVER/UNDER FLOW SETTING.
C--- HISTORY
C 89. 5. 4   CREATED BY T. NAKAJIMA.
C 90. 1.17   UPDATED WITH CPCON
C--- INPUT
C X        R         INDEPENDENT VARIABLE.
C--- OUTPUT
C EXPFN    F         EXP(X).
C                     IF X.LE. VMN THEN EXP(X) IS RESET AS   0.
C                     IF X.GE. VMX THEN EXP(X) IS RESET AS EXP(VMX).
C--- PARAMETERS
C SYSTEM SET THE -VMN- AND -VMX- BY THE FUNCTION R1MACH.
C
      SAVE INIT,VMN,VMX,EXPMN,EXPMX
      DIMENSION CCP(3)
      DATA INIT/1/
C
C SET VMN      R         ENDERFLOW LIMIT.
C     VMX      R         OVERFLOW LIMT.
      IF(INIT.GT.0) THEN
        INIT=0
        CALL CPCON(CCP)
        VMN=CCP(2)*0.8*2.3
        VMX=CCP(3)*0.8*2.3
        EXPMN=0
        EXPMX=EXP(VMX)
      ENDIF
C
      IF(X.LE.VMN) THEN
        EXPFN=EXPMN
        ELSE
        IF(X.GE.VMX) THEN
          EXPFN=EXPMX
          ELSE
          EXPFN=EXP(X)
        ENDIF
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE LGNDF3(LMAX1,N,X,Y,G)

      include 'precision.f'

C$NAME  LGNDF3
C$LINK  EXPFN  MTQGAUSN
C
C LEGENDRE EXPANSION (SAME AS LGNDF2 BUT GENERATING G-MOMENTS).
C--- HISTORY
C 90. 1.20 CREATED FROM LGNDF2, USE EXPFN.
C          DIRECTION OF INTEGRATION FROM X(1) TO X(N).
C 91. 2.16 STRIP NG FROM SAVE STATEMENT.
C 92. 4. 3 KILL THE STATEMENT OF GW=GW/2 AFTER MTQGAUSN
C     6.22 ADD GW/2 AGAIN BECAUSE THE ABOVE CHANGE IS MISTAKE
C--- INPUT
C LMAX1      I      MAXIMUM ORDER + 1.
C N          I      NUMBER OF DATA ON (-1, 1). .GE. 4.
C X        R(NA)    INDEPENDENT VARIABLES ON (-1, 1)
C Y        R(NA)    Y(X).
C--- OUTPUT
C G    R(LMAX1)     Y = SUM(L1=1,LMAX1) (2*L1-1)*G(L1)*PL(L1)
C                      WHERE PL(L1) IS (L1-1)TH ORDER LEGENDRE
C                      POLYNOMIAL.
C$ENDI
      SAVE INIT,GW,GX
C VARIABLES FOR THE ROUTINE.
      DIMENSION X(N),Y(N),G(LMAX1)
C WORKING AREAS.
      PARAMETER (PI=3.141592653589793, RAD=PI/180.0)
      PARAMETER (NG=5)
      DIMENSION GX(NG),GW(NG)
      DATA INIT/1/
C SHIFTED GAUSSIAN QUADRATURE.
      IF(INIT.GE.1) THEN
        INIT=0
        CALL MTQGAUSN(GW,GX,NG)
        DO 1 I=1,NG
    1   GW(I)=GW(I)/2
      ENDIF
C CLEAR
      DO 2 L1=1,LMAX1
    2 G(L1)=0
C LOOP FOR ANGLE
      DO 3 I=1,N-1
C CUBIC INTERPOLATION
      IF(I .LE. 2) THEN
        I1=1
        I4=4
       ELSE
        IF(I .LE. N-2) THEN
          I1=I-1
          I4=I+2
         ELSE
          I1=N-3
          I4=N
        ENDIF
      ENDIF
CC
      I2=I1+1
      I3=I2+1
      X1=X(I1)
      X2=X(I2)
      X3=X(I3)
      X4=X(I4)
      IF(   (Y(I1) .LE. 0) .OR. (Y(I2) .LE. 0) .OR. (Y(I3) .LE. 0)
     & .OR. (Y(I4) .LE. 0) ) THEN
        ISIGN=-1
        ALP1=Y(I1)
        ALP2=Y(I2)
        ALP3=Y(I3)
        ALP4=Y(I4)
       ELSE
        ISIGN=1
        ALP1=LOG(Y(I1))
        ALP2=LOG(Y(I2))
        ALP3=LOG(Y(I3))
        ALP4=LOG(Y(I4))
      ENDIF
C LOOP FOR GAUSSIAN INTEGRATION
      DO 4 J=1,NG
CC INTERPOLATED VALUE OF Y
      XX=X(I)+GX(J)*(X(I+1)-X(I))
      WW=GW(J)*(X(I+1)-X(I))
      PP=(XX-X2)*(XX-X3)*(XX-X4)/(X1-X2)/(X1-X3)/(X1-X4)*ALP1
     &  +(XX-X1)*(XX-X3)*(XX-X4)/(X2-X1)/(X2-X3)/(X2-X4)*ALP2
     &  +(XX-X1)*(XX-X2)*(XX-X4)/(X3-X1)/(X3-X2)/(X3-X4)*ALP3
     &  +(XX-X1)*(XX-X2)*(XX-X3)/(X4-X1)/(X4-X2)/(X4-X3)*ALP4
      IF(ISIGN .EQ. 1) PP=EXPFN(PP)
C LEGENDRE SUM
      PL1=0
      PL=1
      G(1)=G(1)+PP*WW
      IF(LMAX1.GE.2) THEN
        DO 5 L1=2,LMAX1
        PL2=PL1
        PL1=PL
        PL=((2*L1-3)*XX*PL1-(L1-2)*PL2)/(L1-1)
    5   G(L1)=G(L1)+PP*PL*WW
      ENDIF
    4 CONTINUE
    3 CONTINUE
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION PLGD(INIT,X)

      include 'precision.f'

C$NAME  PLGD
C LEGENDRE POLYNOMIALS
C--- HISTORY
C 87.11.12 CREATED
C 90. 1.16 SAVE STATEMENT
C--- INPUT
C INIT   I    IF 1 THEN L=0
C             IF 0 THEN L=L+1
C X      R    (-1,1)
C--- OUT
C INIT=0
C$ENDI
C
      SAVE L,PL,PL1,PL2
      IF(INIT.GT.0) THEN
        INIT=0
        L=-1
      ENDIF
      L=L+1
      IF(L.EQ.0) THEN
        PL=1
       ELSE
        IF(L.EQ.1) THEN
          PL1=PL
          PL=X
         ELSE
          PL2=PL1
          PL1=PL
          PL=((2*L-1)*X*PL1-(L-1)*PL2)/REAL(L)
        ENDIF
      ENDIF
      PLGD=PL
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      FUNCTION SEARF1(AMUE,AMUI,PHI,U10,CR,CI)

      include 'precision.f'

C$NAME SEARF1
C$LINK ERFC FRNLR
C BIDIRECTIONAL REFLECTIVITY OF OCEAN SURFACE
C SHADOWING FACTOR FOR SINGLE SCATTERING
C SO THAT ENERGY CONSERVATION IS NOT SATISFIED
C--- HISTORY
C 92. 9. 1 CREATED BY HASUMI
C    12.23 MODIFIED BY NAKAJIMA
C 95. 6. 2 Generated from SEAREF with CR and CI
C--- INPUT
C AMUE     R    COSINE OF ZENITH ANGLE OF EMERGENT RAY .GT. 0
C AMUI     R    COSINE OF ZENITH ANGLE OF INCIDENT RAY .GT. 0
C PHI      R    AZIMURTHAL ANGLE  (RADIAN)
C U10      R    WIND VELOCITY IN M/S AT 10M HEIGHT
C CR       R    Relative refractive index of the media
C               About 1.33 for atmosphere to ocean incidence,
C               and 1/1.33 for ocean to atmosphere incidence.
C CI       R    Relative refractive index for imaginary part
C               M = CR + I*CI
C--- OUTPUT
C SEAREF  RF    BIDIRECTIONAL REFLECTION FUNCTION
C
      PARAMETER (PI = 3.141592654,SQRTPI=1.7724539)
c_GD      AMUE1=MAX(AMUE,1.0E-7)
      test = 1.E-07
      AMUE1=MAX(AMUE,test)
c_GD      
      ALPHA=-AMUE1*AMUI+SQRT((1-AMUE1**2)*(1-AMUI**2))*COS(PHI)
      COSW=SQRT((1-ALPHA)/2.0)
      AMUN=(AMUE1+AMUI)/2/COSW
C SHADOWING FACTOR
      SIGMA=SQRT(5.34E-3*U10)
      VIE =SIGMA*SQRT(1-AMUE1**2)/AMUE1
      IF (VIE. GT. 0.15) THEN
        VE =1/VIE
        FE=(EXP(- VE**2)/SQRTPI/VE-ERFC(VE))/2
       ELSE
        FE=0
      ENDIF
      VII =SIGMA*SQRT(1-AMUI**2)/AMUI
      IF (VII. GT. 0.15) THEN
        VI =1/VII
        FI=(EXP(- VI**2)/SQRTPI/VI-ERFC(VI))/2
       ELSE
        FI=0
      ENDIF
      G=1.0/(1+FE+FI)
C
      CALL FRNLR(CR,CI,COSW,COSWT,R1,R2)
      R=(R1+R2)/2
      P=1/PI/SIGMA**2/AMUN**3*EXP(-(1-AMUN**2)/(SIGMA*AMUN)**2)
      SEARF1=1.0/4/AMUE1/AMUN*G*P*R
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE CPCON_Naka(C)

C *** Version originale de CPCON (Nakajima), modifiee par PS     

      include 'precision.f'

C$NAME  CPCON
C MACHINE CONSTANTS OF COMPUTER
C--- OUTPUT
C C     R(3)      (1)  MINIMUM POSITIVE X FOR  1+X      .NE. 1
C                      AVERAGE AS A RESULT OF COMPLEX ARITHMETIC
C                      OPERATIONS.
C                 (2)  MINIMUM EXPONENT Y FOR  10.0**Y  .NE. 0
C                 (3)  MAXIMUM EXPONENT Z FOR  10.0**Z  IS MAX. VALUE
C                  IF INIT=1 (DATA STATEMENT) THEN  SET AS Z=Y
C                  IF INIT=2 THEN THIS ROUTINE GETS ACTUAL VALUE OF Z.
C                  - SEE NOTE -
C--- HISTORY
C 90. 1.20  CREATED
C     6.27  CHANGE THE ALGORITHM TO GET X AND Y TAKING INTO ACCOUNT THE
C           HIGH ACCURACY CO-PROCESSOR AND GRACEFUL UNDERFLOW.
C 92. 3.21  N=1000 from N=200
C     7. 9  BUG IN X-DEFINITION
C
C  97.03.25 : Version SUN - Cette subroutine produisait des erreurs du type
C                 IEEE Underflow, overflow
C                 lors du test de la valeur de l exposant min et max .
C                 Les resultats etaient -40 et +40
C	   Arbitrairement, les exposants ont ete fixes a -35      +35
C------------------------------------------------------------------------            
C--- NOTE
C THIS PROGRAM WILL GENERATE -UNDERFLOW ERROR- AND -OVERFLOW ERROR-
C  MESSAGES.  ON SOME COMPUTER -OVERFLOW ERROR- MESSAGE MAY BE
C  FATAL ERROR.  IN THAT CASE, PLEASE SET INIT = 1 IN THE DATA
C  SATEMENT FOR SUPPRESSING THE PROCEDURE OF GETTING C(3).
      DIMENSION C(*)
      CHARACTER CH*80
C RESOLUTION OF COMPUTATION
      SAVE INIT,X,Y,Z
      DATA INIT/1/
      IF(INIT.LE.0) THEN
        C(1)=X
        C(2)=Y
        C(3)=Z
        RETURN
      ENDIF
CC TEST SUM(K=1,M) COS((2K-1)*PI/(2M+1)) = 0.5
CC SIMPLE CHECK, X = X + E, IS NOT VALIDE WHEN THE COMPUTER
CC  USE A HIGH ACCURATE CO-PROCESSOR.
      N=500
      PI=ASIN(1.0)*2
      M0=10
      X=0
      DO 1 M=1,M0
      Y=0
      DO 2 K=1,M
    2 Y=Y+COS((2*K-1)*PI/(2*M+1))
      Y=ABS(2*Y-1)
      X=X+Y
    1 CONTINUE
      X=X/M0
      C(1)=X
c
C   Test enleve car  la recherche de l exposant max et min produisait une erreur
C
C EXPONENT FOR MINIMUM POSITIVE VALUE
C  THIS PROCEDURE WILL GENERATE -UNDERFLOW ERROR MESSAGE-
c      Y2=1
c      N=1000
c      DO 3 I=1,N
c      Y1=Y2
c      Y3=Y1/10
CC FOR GRACEFUL UNDERFLOW
CC EVEN Y2 BECOMES 0 AS OUTPUT, Y2 IS NOT 0 INSIDE
CC COMPUTER WHEN GRACEFUL UNDERFLOW IS APPLIED.
CC SO WE REPLACE THE VALUE OF Y2 BY OUTPUT.
c      CH='0'
c      WRITE(CH,7) Y3
c    7 FORMAT(1P,E12.5)
c      Y2=0
c      READ(CH,*) Y2
c      IF(ABS(10*Y2/Y1-1) .GT. 5*X) GOTO 4
c    3 CONTINUE
c      I=N+1
c    4 Y=1-I
c      C(2)=Y
C EXPONENT FOR MAXIMUM POSITIVE VALUE
C THIS PROCEDURE WILL GENERATE -OVERFLOW MESSAGE-
c      IF(INIT.LE.1) THEN
c        Z=-Y
c       ELSE
c        Z2=1
c        DO 5 I=1,N
c        Z1=Z2
c        Z2=Z1*10
c        IF(ABS(Z2/Z1/10-1) .GT. 5*X) GOTO 6
c    5   CONTINUE
c        I=N+1
c    6   Z=I-1
c      ENDIF
c      C(3)=Z
        y=-35
        z=35
       c(2)=y
       c(3)=z
C
      INIT=0
      RETURN
      END
c
c-------------------------------------------------------------------------------
c
      SUBROUTINE CPCON(C)

      include 'precision.f'
      
C$NAME  CPCON
C MACHINE CONSTANTS OF COMPUTER
C--- OUTPUT
C C     R(3)      (1)  MINIMUM POSITIVE X FOR  1+X      .NE. 1
C                      AVERAGE AS A RESULT OF COMPLEX ARITHMETIC
C                      OPERATIONS.
C                 (2)  MINIMUM EXPONENT Y FOR  10.0**Y  .NE. 0
C                 (3)  MAXIMUM EXPONENT Z FOR  10.0**Z  IS MAX. VALUE
C                  IF INIT=1 (DATA STATEMENT) THEN  SET AS Z=Y
C                  IF INIT=2 THEN THIS ROUTINE GETS ACTUAL VALUE OF Z.
C                  - SEE NOTE -
C--- HISTORY
C 90. 1.20  CREATED
C     6.27  CHANGE THE ALGORITHM TO GET X AND Y TAKING INTO ACCOUNT THE
C           HIGH ACCURACY CO-PROCESSOR AND GRACEFUL UNDERFLOW.
C
C 2000/12/20 reecrite pour utiliser les fonctions standard du fortran 90
C
C------------------------------------------------------------------------            
      DIMENSION C(*)

C RESOLUTION OF COMPUTATION
      SAVE INIT,X,Y,Z
      DATA INIT/1/
      
      IF(INIT.LE.0) THEN
        C(1)=X
        C(2)=Y
        C(3)=Z
        RETURN
      ENDIF
      A=1.0
      X=EPSILON(A)
      Y=TINY(A)
      Y=INT(LOG10(Y))
      Z=-Y    ! On pourrait aussi utiliser HUGE(A)
      C(1)=X
      C(2)=Y
      C(3)=Z
      INIT=0
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE QUADA(NDA,AMUA,WA)

      include 'precision.f'

C$NAME  QUADA
C$LINK  MTQGAUSN
C CONSTRUCTION OF QUADRATURE IN THE ATMOSPHERE.
C------ HISTORY
C 1986.10.2  USE MTQGAUSN FROM STAMNES AND TSAY.
C--- INPUT
C NDA        I      NO. OF QUADRATURE STREAMS IN THE HEMISPHERE OF ATMOS
C--- OUTPUT
C AMUA    R(KNDA)   MUA(I), I=1,N1    MUA(1) > MUA(2) > ...
C WA      R(KNDA)   CORRESPONDING WEIGHTS.
C$ENDI
C--- VARIABLES FOR THE ROUTINE
      DIMENSION AMUA(NDA),WA(NDA)
C SHIFTED GAUSSIAN QUADRATURE ON (0, 1).
      CALL MTQGAUSN(WA,AMUA,NDA)
C REORDERING.
      DO 3 I=1,NDA/2
      I1=NDA+1-I
      X=AMUA(I)
      AMUA(I)=AMUA(I1)
      AMUA(I1)=X
      X=WA(I)
      WA(I)=WA(I1)
    3 WA(I1)=X
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      FUNCTION SGLR (AM1, AM0, NL, TC, W, P, UT)

      include 'precision.f'

C$NAME SGLR
C$LINK CPCON EXPFN
C SINGLY SCATTERED INTENSITY (MULTI-LAYER) BY EQ. (12) OF NT.
C--- REFERENCE
C NT:  T. NAKAJIMA AND M. TANAKA, 1988, JQSRT, 40, 51-69
C--- HISTORY
C 89. 2.22   CREATED BY T. NAKAJIMA
C              FROM AIDS BY CHANGING THE MEANING OF T, W, P,
C              AND UT.  UT IS A LEVEL INSIDE THE MULTI-LAYER SYSTEM.
C     5. 4   PATCH FOR EXPONENTIAL OVERFLOW.
C     6.20   REFORM STYLE.
C    12.20   WITH CPEX(3)
C 90.12. 1   PUT IF(IALM... OUT OF DO-LOOP (FOR OPTIMIZATION)
C FOLLOWINGS ARE DEFINITIONS OF SYMBOLS IN THE ROUTINE, WITH THE FORMAT:
C NAME    TYPE       CONTENT
C WHERE TYPE = S (SUBROUTINE), F (FUNCTION), I (INTEGER), R (REAL).
C FOR THE TYPE = I AND R, THE SIZE OF ARRAY IS SHOWN BY ( ).
C--- INPUT
C AM1      R         COS(EMERGENT ZENITH ANGLE) WITH THE SIGN OF
C                    TRANSMITTED( .GT.0),  REFLECTED( .LT.0).
C AM0      R         COS(INCIDENT ZENITH ANGLE) .GT. 0.
C NL       I         NO. OF SUBLAYERS.
C TC     R(NL)       OPTICAL THICKNESS OF SUBLAYER.
C W      R(NL)       SINGLE SCATTERING ALBEDO OF SUBLAYER.
C P      R(NL)       PHASE FUNCTION OF SUBLAYER.
C                    INTEGRATION OVER UNIT SPHERE SHOULD BE 1.
C UT       R         USER DEFINED OPTICAL DEPTH FOR THE INTENSITY
C                    WITHIN THE MULTI-LAYERED SYSTEM.
C--- OUTPUT
C SGLR     F         SINGLY SCATTERED INTENSITY.
      SAVE INIT,EPS
      DIMENSION TC(NL),W(NL),P(NL),CCP(3)
      LOGICAL IALM,IST
      DATA INIT/1/
      IF(INIT.GT.0) THEN
        INIT=0
        CALL CPCON(CCP)
        EPS=CCP(1)*30
      ENDIF
C SET EPS: IF ABS(1/AM1 - 1/AM0) .LE. EPS THEN
C                      THE ROUTINE SETS ALMUCANTAR CONDITION-IALM.
      SGLR=0
      X=1/AM1-1/AM0
      IF(ABS(X).LE.EPS) THEN
        IALM=.TRUE.
        ELSE
        IALM=.FALSE.
      ENDIF
      T2=0
      IST=.TRUE.
C X=0 (TRANSMISSION)
      IF(IALM) THEN
        DO 11 L=1,NL
        T1=T2
        T2=T1+TC(L)
        IF(UT.LE.T1) GOTO 13
        IF(UT.LT.T2) T2=UT
        IF(IST) THEN
          IST=.FALSE.
          E2=T1
        ENDIF
        E1=E2
        E2=T2
        SGLR=SGLR+W(L)*P(L)*(E2-E1)
 11     CONTINUE
       ELSE
C X<>0
      DO 21 L=1,NL
        T1=T2
        T2=T1+TC(L)
CC REFLECTION
        IF(AM1.LT.0) THEN
          IF(UT.GE.T2) GOTO 21
          IF(UT.GT.T1) T1=UT
CC TRANSMISSION
         ELSE
          IF(UT.LE.T1) GOTO 13
          IF(UT.LT.T2) T2=UT
        ENDIF
C
        IF(IST) THEN
          IST=.FALSE.
          E2=T1*X
          E2=EXPFN(E2)/X
        ENDIF
        E1=E2
        E2=T2*X
        E2=EXPFN(E2)/X
        SGLR=SGLR+W(L)*P(L)*(E2-E1)
 21   CONTINUE
      ENDIF
 13   E1=-UT/AM1
      SGLR=SGLR*EXPFN(E1)/ABS(AM1)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  EQ12
      SUBROUTINE EQ12(A,B,NI,J,NB1)

      include 'precision.f'

C  A(*) = B(*, J)
C--- HISTORY
C 88. 6. 6  REGISTERED BY T. NAKAJIMA
C--- INPUT
C B      R(NB1,*)    2-DIM ARRAY  B.
C NI        I        A(I) = B(I,J),   I=1,NI
C J         I
C--- OUTPUT
C A       R(NI)      1-DIM ARRAY   A=B(*,J)
C$ENDI
      DIMENSION A(NI),B(NB1,J)
      DO 1 I=1,NI
    1 A(I)=B(I,J)
      RETURN
      END
C
C -----------------------------------------------------------------------------
C
C$NAME  EQ21
      SUBROUTINE EQ21(A,B,NI,J,NA1)

      include 'precision.f'

C  A(*,J) = B(*)
C--- HISTORY
C 88. 6. 6  REGISTERED BY T. NAKAJIMA
C B      R(NI)     SOURCE 1-DIM ARRAY  B.
C NI       I       A(I,J) = B(I),  I=1,NI
C J        I
C MA1      I       SIZE FOR DIMENSION A(NA1,*)
C--- OUTPUT
C A     R(NA1,*)   DESTINATION 2-DIM ARRAY A(*,J)=B
C$ENDI
      DIMENSION A(NA1,J),B(NI)
      DO 1 I=1,NI
    1 A(I,J)=B(I)
      RETURN
      END
C
C ---------------------------------------------------------------------------
C      
C$NAME  EQ32
      SUBROUTINE EQ32(A,B,NI,NJ,K,NA1,NA2,NA3,NB1,NB2)

      include 'precision.f'

C  A(*,*,K)=B(*,*)
C--- HISTORY
C 88. 5. 6   REGISTERED BY T. NAKAJIMA
C--- INPUT
C B      R(NB1,NB2)    SOURCE 2-DIM ARRAY
C NI,NJ,K   I          A(I,J,K)=B(I,J),   I=1,NI;  J=1,NJ
C NA1,NA2,NA3  I       DIM A(NA1,NA2,NA3)
C NB1,NB2      I       DIM B(NB1,NB2)
C--- OUTPUT
C A    R(NA1,NA2,NA3)  DESTINATION 3-DIM ARRAY A(*,*,K)=B(*,*)
C$ENDI
      DIMENSION A(NA1,NA2,NA3),B(NB1,NB2)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J,K)=B(I,J)
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      SUBROUTINE GRNDO1(INDG,FSOL,U10,BGND,DPT,M,NDA,WMP,NA0,AM0
     &   ,CR,CI,R,T,ER,ERT,ET,AMUA,WA)

      include 'precision.f'

C$NAME GRNDO1
C$LINK  OCNR11 OCNR31 EQ32
C--- HISTORY
C 89.11. 2
C
C 93. 2.26  BUG BG
C     3. 2  CHENGED CALCULATION OF EMISSIVITY
C     3.24  SUB.OCNRF3 Changed parameter ( add wa )
C     4. 5  Put SAVE
C     4. 5 NSF = 1 for initialization (Terry)
C     5. 4 NSF -> INDG; N1, N0 change name as NDA, NA0
C          IF(NSF.GT.0) -> IF(INDG.EQ.1)
C 95. 6. 2 Generated from GRNDO with CR and CI
C 96. 1.12  EMS(KNA0)
C 97. 3.17 Add ERT By Takashi Nakajima to Thermal.
C 98. 2. 4  Replace EMS(KNA0)->EMS(KNDM)
C--- INPUT
C INDG       I           1 then Initialization of ocean surface matrix
C                        Otherwise use the calculated matrices.
C FSOL       R           SOLAR IRRADIANCE AT THE SYSTEM TOP.
C U10        R           WIND VELOCITY IN M/S AT 10M HEIGHT
C BGND       R           Plank INTENSITY FROM THE SURFACE
C                        Emitted thermal is calculated as (1-r)*BGND
C                        where r is unidirectional reflectivity.
C                        Note the definition of BGND is different
C                        from that for GRNDL.
C DPT        R           OPTICAL DEPTH AT THE SURFACE.
C M          I           FOURIER ORDER.
C NDA        I           NUMBER OF QUADRATURE POINTS.
C WMP     R(KNDM)        SQRT(WM)
C NA0        I           NUMBER OF SOLAR ANGLES.
C AM0     R(KNA0)        COS(SOLAR ZENITH ANGLE)
C                        Needs all the m-Fourier components
C CR         R           Relative refractive index of the media
C                        About 1.33 for atmosphere to ocean incidence,
C                        and 1/1.33 for ocean to atmosphere incidence.
C CI         R           Relative refractive index for imaginary part
C                        M = CR + I*CI
C--- OUTPUT
C R      R(KNDM,KNDM)    SCALED REFELCTION MATRIX.
C T      R(KNDM,KNDM)    SCALED TRANSMISSION MATRIX.
C ER     R(KNDM,KNA0)    SCALED SOURCE MATRIX FOR UPWARD RADIANCE.
C ET     R(KNDM,KNA0)    SCALED SOURCE MATRIX FOR DOWNWARD RADIANCE.
C
C OCEAN SURFACE
      SAVE ORF,SRR
C$PSET OCNR11.KNDM=KNDM
C$PSET OCNR31.KNDM=KNDM
C$PRPC
      include 'parameter.rayjn'
c     ! PARAMETER (KNDM=8,KNA0=2)
      PARAMETER (PI=3.141592654)
      PARAMETER (KNDM2=KNDM*2)
      DIMENSION R(KNDM,KNDM),T(KNDM,KNDM),ER(KNDM,KNA0),ET(KNDM,KNA0)
     &         ,AM0(KNA0),WMP(KNDM),AMUA(KNDM),WA(KNDM)
C98.2.4 Replace EMS(KNA0)->EMS(KNDM)
      DIMENSION ORF(KNDM,KNDM,KNDM2),EMS(KNDM),SR(KNDM)
     &         ,SRR(KNDM,KNA0,KNDM2)

      DIMENSION ERT(KNDM,KNA0)

      M1=M+1
      IF(INDG.EQ.1) THEN
        CALL OCNR11(M,NDA,AMUA,WA,CR,CI,U10,R)
        CALL EQ32(ORF,R,NDA,NDA,M1,KNDM,KNDM,KNDM2,KNDM,KNDM)
C 93.3.2
        DO 5 J=1,NA0
        CALL OCNR31(M,NDA,AMUA,WA,AM0(J),CR,CI,U10,SR)
        DO 5 I=1,NDA
    5   SRR(I,J,M1)=SR(I)
C
      ENDIF
      DO 2 I=1,NDA
      DO 2 J=1,NDA
      T(I,J)=0.0
   2  R(I,J)=WMP(I)*ORF(I,J,M1)/WMP(J)
C 93.3.2
      IF(M.EQ.0)THEN
        BG=2*PI*BGND

C98.2.4 Replace 'DO 3'  by T.Y.Nakajima
C Refer Katagiri's Modification.
C        DO 3 J=1,NA0
C        SUM=0
C        DO 4 I=1,NDA
C   4    SUM=SUM+AMUA(I)*WA(I)*SRR(I,J,1)
C   3    EMS(J)=1-SUM/AM0(J)

         DO 3 I=1,NDA
         SUM=0.
         DO 4 J=1,NDA
    4    SUM=SUM+ORF(I,J,1)
    3    EMS(I)=1-SUM
C
      ELSE
        BG=0
      ENDIF
        DO 1 J=1,NA0
          X=-DPT/AM0(J)
          TRNS0=EXP(X)*FSOL
          DO 1 I=1,NDA
          ET(I,J)=0.
C 98.2.4 Replace 'ERT=' by T.Y.Nakajima
C Refer Katagiri's Modification.
C 97.3.17 Add ERT By Takashi Nakajima
C        ERT(I,J)=WMP(I)*(EMS(J)*BG)
C  1     ER(I,J)=WMP(I)*(SRR(I,J,M1)*TRNS0+EMS(J)*BG)

         ERT(I,J)=WMP(I)*(EMS(I)*BG)
    1    ER(I,J)=WMP(I)*(SRR(I,J,M1)*TRNS0+EMS(I)*BG)

      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      SUBROUTINE GRNDL3(FSOL,GALB,BGND,DPT,M,N1,AM,W,N0,AM0,R,T,ER,ET)

      include 'precision.f'

C$NAME GRNDL3
C$LINK  EQ20
C--- HISTORY
C 95. 5.26 Generated from GRNDL introducing AM and W
C            and R=2*GALB... -> SUMWM*GALB...
C--- INPUT
C FSOL       R           SOLAR IRRADIANCE AT THE SYSTEM TOP.
C GALB       R           FLUX REFLECTIVITY OF THE SURFACE.
C BGND       R           THERMAL INTENSITY FROM THE SURFACE=(1-r)*PLANK.
C DPT        R           OPTICAL DEPTH AT THE SURFACE.
C M          I           FOURIER ORDER.
C N1         I           NUMBER OF QUADRATURE POINTS.
C AM      R(KNDM)        MU(I)
C W       R(KNDM)        W (I)
C N0         I           NUMBER OF SOLAR ANGLES.
C AM0     R(KNA0)        COS(SOLAR ZENITH ANGLE)
C--- OUTPUT
C R      R(KNDM,KNDM)    SCALED REFELCTION MATRIX.
C T      R(KNDM,KNDM)    SCALED TRANSMISSION MATRIX.
C ER     R(KNDM,KNA0)    SCALED SOURCE MATRIX FOR UPWARD RADIANCE.
C ET     R(KNDM,KNA0)    SCALED SOURCE MATRIX FOR DOWNWARD RADIANCE.
C
C LAMBERT SURFACE
      PARAMETER (PI=3.141592654)
C$PRPC
      include 'parameter.rayjn'
c      
c      PARAMETER (KNDM=8,KNA0=2)
      DIMENSION R(KNDM,KNDM),T(KNDM,KNDM),ER(KNDM,KNA0),ET(KNDM,KNA0)
     & ,AM0(KNA0),AM(KNDM),W(KNDM)
      SUMWM=0
      DO 3 I=1,N1
    3 SUMWM=SUMWM+AM(I)*W(I)
      IF(GALB.LE.0.0 .OR. M.GT.0) THEN
        CALL EQ20( R,0.0d0,N1,N1,KNDM,KNDM)
        CALL EQ20( T,0.0d0,N1,N1,KNDM,KNDM)
       ELSE
        DO 2 I=1,N1
        DO 2 J=1,N1
        T (I,J)=0
    2   R (I,J)=GALB*SQRT(AM(I)*W(I)*AM(J)*W(J))/SUMWM
      ENDIF
      IF(M.GT.0) THEN
        CALL EQ20(ER,0.0d0,N1,N0,KNDM,KNA0)
        CALL EQ20(ET,0.0d0,N1,N0,KNDM,KNA0)
       ELSE
        DO 1 J=1,N0
          TRNS0=EXP(-DPT/AM0(J))*FSOL
          X=GALB*AM0(J)*TRNS0/SUMWM+2*PI*BGND
          DO 1 I=1,N1
          ET(I,J)=0
    1     ER(I,J)=SQRT(AM(I)*W(I))*X
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE PHAS2(M1,MMAX1,N1,N2,KNP,KN1,KN2,PT,PR,G
     & ,PL1,PL2)

      include 'precision.f'

C$NAME  PHAS2
C$LINK  EQ20
C--- HISTORY
C 90. 1.20  GENERATED FROM PHASE (SAME AS PHASE BUT USING THE MOMENTS G)
C--- INPUT
C M1         I       M + 1    FOURIER ORDER + 1.
C MMAX1      I       MMAX+1   MAX FOURIER ORDER + 1.
C N1         I       NO. OF EMERGENT ZENITH ANGLES.
C N2         I       NO. OF INCIDENT ZENITH ANGLES.
C KNP        I       SIZE OF N1 FOR ARRAY PT AND PR.
C KN1        I       SIZE OF N1 FOR ARRAY PL1.
C KN2        I       SIZE OF N2 FOR ARRAY PL2.
C G     R(MMAX1)     LEGENDRE MOMENTS OF PHASE FUNCTION.
C                      G(1)=1
C PL1   R(KN1,MMAX1) LEGENDRE POLYNOMIALS FOR EMERGENT DIRECTION.
C                    (I,M1) = (EMERGENT DIRECTIONS, ORDER+1).
C PL2   R(KN2,MMAX1) LEGENDRE POLYNOMIALS FOR INCIDENT DIRECTION.
C--- OUTPUT
C PT    R(KNP,N2)    PHASE MATRIX FOR TRANSMISSION.
C PR    R(KNP,N2)    PHASE MATRIX FOR REFLECTION.
C$ENDI
C
      PARAMETER (PI=3.141592653)
      DIMENSION PT(KNP,N2),PR(KNP,N2),G(MMAX1)
     & ,PL1(KN1,MMAX1),PL2(KN2,MMAX1)
C
      CALL EQ20(PT,0.0d0,N1,N2,KNP,N2)
      CALL EQ20(PR,0.0d0,N1,N2,KNP,N2)
      IF(M1.LE.MMAX1) THEN
        DO 1 J=1,N2
        DO 1 I=1,N1
        SIGN=-1
        DO 1 K1=M1,MMAX1
        SIGN=-SIGN
        C4=(2*K1-1)/2.0*G(K1)*PL1(I,K1)*PL2(J,K1)
        PT(I,J)=PT(I,J)+C4
    1   PR(I,J)=PR(I,J)+SIGN*C4
      ENDIF
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE PLGND(M1,MMX1,NX,NX0,X,PL)

      include 'precision.f'

C$NAME  PLGND
C NORMALIZED ASSOCIATED LEGENDRE POLYNOMIALS
C------ HISTORY
C 1987.03.04
C------ INPUT VARIABLES
C VARIABLE  TYPE    INTERPRETATION
C M1         I      ORDER OF THE FOURIER SERIES + 1
C MMX1       I      MAX ORDER OF M  + 1
C NX         I      NBR OF X
C NX0        I      DECLARED NBR OF NX
C X       R(NX)     XI, I=1,NX
C------ OUTPUT VARIABLES
C PL      R(NX0,    NORMALIZED ASSOCIATED LEGENDRE POLYNOMIALS
C           MMX1)
C$ENDI
C------ GLOBAL PARAMETER
      PARAMETER (PI=3.141592653)
C------ VARIABLES FOR THE ROUTINE
      DIMENSION X(NX),PL(NX0,MMX1)
C------ NORMALIZED ASSOCIATED LEGENDRE POLYNOMIALS.
C  CC=P(M,L,X)*SQRT((L-M)|/(L+M)|), WHERE X= COS(MU).
      M=M1-1
      DO 15 L1=M1,MMX1
      L=L1-1
c      IF(L-(M+1)) 16,17,18
      IF((L-(M+1)).lt.0) goto 16
      IF((L-(M+1)).eq.0) goto 17
      IF((L-(M+1)).gt.0) goto 18
c   16 IF(M) 21,21,22
   16 IF(M.gt.0) goto 22
   21 DO 2 I=1,NX
    2 PL(I,L1)=1.0
      GOTO 15
   22 K=2
      ETA=1.0
      DO 1 J=1,M
      EPSI=1.0-1.0/REAL(K)
      ETA=ETA*EPSI
    1 K=K+2
      ETA=SQRT(ETA)
      DO 24 I=1,NX
      IF(X(I).GE.1.0) THEN
      PL(I,L1)=0.0
      ELSE
      EXPCC=(REAL(M)*LOG(ABS(1.0-X(I)**2)))/2.0
      IF(EXPCC.LE.-100.0) THEN
      PL(I,L1)=0.0
      ELSE
      PL(I,L1)=ETA*EXP(EXPCC)
      ENDIF
      ENDIF
   24 CONTINUE
      GO TO 15
   17 C0=SQRT(REAL(2*M+1))
      DO 26 I=1,NX
   26 PL(I,L1)=C0*PL(I,L1-1)*X(I)
      C0=SQRT(REAL((L-M)*(L+M)))
      GO TO 15
   18 C1=SQRT(REAL((L-M)*(L+M)))
      C2=REAL(2*L-1)
      DO 27 I=1,NX
   27 PL(I,L1)=(C2*X(I)*PL(I,L1-1)-C0*PL(I,L1-2))/C1
      C0=C1
   15 CONTINUE
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE TRN1(NSB,NDD,NA0,IUP,IDN,RE,TE,SER,SET,RUP,RDN,ERR)

      include 'precision.f'

C$NAME  TRN1
C$LINK  AAPB  ADD  EQ22 EQ23  EQ32  MULTI  RP33  RP30
C SOLVE THE RADIATIVE TRANSFER IN THE MULTI-SUBLAYER SYSTEM
C  BY THE ADDING METHOD.
C--- HISTORY
C 87. 3. 4 FOR INTENSITY CALCULATION.
C 89.10.30 RE-EDIT.
C 96.01.19   EQ22(S2U,ST,N2,NA0,KNDM,KNDM,KNDM,KNDM)
C          ->EQ22(S2U,ST,N2,NA0,KNDM,KNA0,KNDM,KNA0)
C--- INPUT
C NSB       I              NUMBER OF SUBLAYERS.
C NDD    R(NSB1)           NUMBER OF QUADRATURE POINTS AT INTERFACES.
C IUP    I(NSB)            ELEMENT NUMBER OF UPWELLING OPERATORS.
C IDN    I(NSB)            ELEMENT NUMBER OF DOWNGOING OPERATORS.
C NA0       I              NUMBER OF SOLAR ZENITH ANGLES.
C RE    R(KNDM,KNDM,KNLT)  REFLECTION MATRICES OF SUBLAYERS.
C TE    R(KNDM,KNDM,KNLT)  TRANSMISSION MATRICES OF SUBLAYERS.
C SER   R(KNDM,KNA0,KNSB)  UPWELLING SOURCE MATRICES.
C SET   R(KNDM,KNA0,KNSB)  DOWNGOING SOURCE MATRICES.
C--- OUTPUT
C RUP   R(KNDM,KNA0,KNSB1) UPWELLING INTERNAL INTENSITIES.
C RDN   R(KNDM,KNA0,KNSB1) DOWNGOING INTERNAL INTENSITIES.
C ERR      C*64            ERROR INDEX.
C--- PARAMETER
C KNA0      I              NUMBER OF SOLAR ZENITH ANGLES.
C KNDM      I              NUMBER OF QUADRATURE POINTS.
C KNSB      I              NUMBER OF SUBLAYERS.
C KNLT      I              TOTAL NUMBER OF ELEMENTARY OPERATORS
C                           TAKING PORALITY INTO ACCOUNT .GE. KNSB.
C--- AREAS FOR THIS ROUTINE
C$PRPC
      include 'parameter.rayjn'
C
      PARAMETER (KNSB=KNLN+1,KNLT=KNLN+1)
c     PARAMETER (KNA0=32,KNDM=32,KNSB=11,KNLT=11)
C$PSET  MULTI.KNDM=KNDM
C$PSET  ADD.KNA0=KNA0;  ADD.KNDM=KNDM
      PARAMETER (PI=3.141592653)
      PARAMETER (KNSB1=KNSB+1)
C--- AREAS FOR THE ROUTINE
      CHARACTER ERR*64
      DIMENSION NDD(KNSB1),IUP(KNSB),IDN(KNSB)
     &, RE(KNDM,KNDM,KNLT),  TE(KNDM,KNDM,KNLT)
     &,SER(KNDM,KNA0,KNSB), SET(KNDM,KNA0,KNSB)
     &,RUP(KNDM,KNA0,KNSB1),RDN(KNDM,KNA0,KNSB1)
C--- WORKING AREAS
      DIMENSION R1D(KNDM,KNDM),R1U(KNDM,KNDM),T1D(KNDM,KNDM)
     &,T1U(KNDM,KNDM),S1D(KNDM,KNA0),S1U(KNDM,KNA0)
     &,R2D(KNDM,KNDM),T2D(KNDM,KNDM),T2U(KNDM,KNDM)
     &,S2D(KNDM,KNA0),S2U(KNDM,KNA0),TU(KNDM,KNDM),RD(KNDM,KNDM)
     &,SD(KNDM,KNA0),SU(KNDM,KNA0),RL(KNDM,KNDM,KNSB),SL(KNDM,KNA0,KNSB)
     &,RT(KNDM,KNDM),ST(KNDM,KNA0)
C--- UPWARD ADDING
      NSB1=NSB+1
      ID=IDN(NSB)
      N2=NDD(NSB)
      N3=NDD(NSB1)
      CALL RP33(RL, RE,N2, N2,NSB, ID,KNDM,KNDM,KNSB,KNDM,KNDM,KNLT)
      CALL RP33(SL,SER,N2,NA0,NSB,NSB,KNDM,KNA0,KNSB,KNDM,KNA0,KNSB)
      IF(NSB.GE.2) THEN
        DO 52 L=NSB-1,1,-1
        LB=L
        L1=L+1
        N1=NDD(L)
        IU=IUP(L)
        ID=IDN(L)
        N2=NDD(L1)
        CALL EQ23(R1D, RE,N1, N1,ID,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(R1U, RE,N2, N2,IU,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(T1D, TE,N2, N1,ID,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(T1U, TE,N1, N2,IU,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(S1D,SET,N2,NA0,LB,KNDM,KNA0,KNDM,KNA0,KNSB)
        CALL EQ23(S1U,SER,N1,NA0,LB,KNDM,KNA0,KNDM,KNA0,KNSB)
        CALL EQ23(R2D, RL,N2, N2,L1,KNDM,KNDM,KNDM,KNDM,KNSB)
        CALL EQ23(S2U, SL,N2,NA0,L1,KNDM,KNA0,KNDM,KNA0,KNSB)
        CALL ADD(0,NA0,N1,N2,N3,R1D,R1U,T1D,T1U,S1D,S1U
     &   ,R2D,T2D,T2U,S2D,S2U,TU,RD,SD,SU,ERR)
        IF(ERR.NE.' ') THEN
          ERR='ERROR IN ADD FOR UPWARD ADDING (TRN1)'
          RETURN
        ENDIF
        CALL EQ32( RL, RD,N1, N1,LB,KNDM,KNDM,KNSB,KNDM,KNDM)
        CALL EQ32( SL, SU,N1,NA0,LB,KNDM,KNA0,KNSB,KNDM,KNA0)
   52   CONTINUE
      ENDIF
C FIELD AT THE TOP OF THE SYSTEM
      N1=NDD(1)
      CALL RP33(RUP, SL,N1,NA0,1,1,KNDM,KNA0,KNSB1,KNDM,KNA0,KNSB)
      CALL RP30(RDN,0.0d0,N1,NA0,1,  KNDM,KNA0,KNSB1)
C--- DOWNWARD ADDING
      IU=IUP(1)
      N1=NDD(1)
      N2=NDD(2)
      CALL EQ23(RT, RE,N2, N2,IU,KNDM,KNDM,KNDM,KNDM,KNLT)
      CALL EQ23(ST,SET,N2,NA0, 1,KNDM,KNA0,KNDM,KNA0,KNSB)
      IF(NSB.GE.2) THEN
        DO 26 L=2,NSB
        LB=L
        N2=NDD(L)
C INTERNAL FIELD
        CALL EQ23(S2U,SL,N2,NA0,LB,KNDM,KNA0,KNDM,KNA0,KNSB)
        CALL EQ23(R2D,RL,N2, N2,LB,KNDM,KNDM,KNDM,KNDM,KNSB)
        CALL AXB(SD,RT,S2U,N2,N2,NA0,KNDM,KNDM,KNDM)
        CALL AAPB(SD,ST,N2,NA0,KNDM,KNDM)
        CALL AXB(RD,RT,R2D,N2,N2,N2,KNDM,KNDM,KNDM)
        CALL MULTI(N2,RD,R1D,ERR)
        IF(ERR.NE.' ') THEN
          ERR='ERROR IN MULTI FOR INTERNAL FIELD (TRN1)'
          RETURN
        ENDIF
        CALL AXB(S1D,R1D,SD,N2,N2,NA0,KNDM,KNDM,KNDM)
        CALL AXB(S1U,R2D,S1D,N2,N2,NA0,KNDM,KNDM,KNDM)
        CALL AAPB(S1U,S2U,N2,NA0,KNDM,KNDM)
        CALL EQ32(RDN,S1D,N2,NA0,LB,KNDM,KNA0,KNSB1,KNDM,KNA0)
        CALL EQ32(RUP,S1U,N2,NA0,LB,KNDM,KNA0,KNSB1,KNDM,KNA0)
C UPSIDE DOWN DIRECTION FOR APPLICATION OF THE ROUTINE ADD.
        IU=IUP(L)
        ID=IDN(L)
        N3=NDD(L+1)
        CALL EQ23(R1D,RE,N3,N3,IU,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(R1U,RE,N2,N2,ID,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(T1D,TE,N2,N3,IU,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ23(T1U,TE,N3,N2,ID,KNDM,KNDM,KNDM,KNDM,KNLT)
        CALL EQ22(R2D,RT,N2,N2,KNDM,KNDM,KNDM,KNDM)
        CALL EQ23(S1D,SER,N2,NA0,LB,KNDM,KNA0,KNDM,KNA0,KNSB)
        CALL EQ23(S1U,SET,N3,NA0,LB,KNDM,KNA0,KNDM,KNA0,KNSB)
C Bug9601       CALL EQ22(S2U,ST,N2,NA0,KNDM,KNDM,KNDM,KNDM)
        CALL EQ22(S2U,ST,N2,NA0,KNDM,KNA0,KNDM,KNA0)
        CALL ADD(0,NA0,N3,N2,N1,R1D,R1U,T1D,T1U,S1D,S1U
     &  ,R2D,T2D,T2U,S2D,S2U,TU,RT,SD,ST,ERR)
        IF(ERR.NE.' ') THEN
          ERR='ERROR IN ADD FOR DOWNWARD ADDING (TRN1)'
          RETURN
        ENDIF
   26   CONTINUE
      ENDIF
C FIELD AT THE BOTTOM OF THE SYSTEM.
      CALL EQ32(RDN,ST,N3,NA0,NSB1,KNDM,KNA0,KNSB1,KNDM,KNA0)
      CALL RP30(RUP,0.0d0,N3,NA0,NSB1,KNDM,KNA0,KNSB1)
C--- DOWNWARD ADDING END
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      FUNCTION CSPLI(X1,N,X,A,B,C,D)

      include 'precision.f'

C$NAME  CSPLI
C GET INTERPOLATED VALUE USING CUBIC SPLINE (PAIR WITH CSPL1)
C--- HISTORY
C 88. 6. 6  REGISTERED BY T. NAKAJIMA
C--- INPUT
C X1      R     INTERPOLATION POINT
C N       I     NO. OF INTERVALS + 1
C X     R(N)    DIVISION POINTS OF THE INTERVALS.
C A, B, C, D
C       R(N)    Y=A+X*(B+X*(C+X*D)))
C--- OUTPUT
C CSPLI  RF     INTERPOLATED VALUE
C$ENDI
      DIMENSION X(N),A(N),B(N),C(N),D(N)
      DO 7 J=1,N
      IF(X1.LE.X(J)) GOTO 8
    7 CONTINUE
      J=N
    8 CSPLI=A(J)+X1*(B(J)+X1*(C(J)+X1*D(J)))
      RETURN
      END
c
c???-------------------------------------------------------------------------------      
c
      SUBROUTINE CSPL1(N,X,Y,A,B,C,D)

      include 'precision.f'

C$NAME  CSPL1
C GETTING COEFFICIENTS OF NATURAL CUBIC SPLINE FITTING
C USE A LINEAR INTERPOLATION OUTSIDE THE MEANINGFUL RANGE OF X
C X-increasing order
C--- HISTORY
C 88. 1. 4  CREATED
C--- INPUT
C N      I     NBR OF DATA
C X    R(N)    INDEPENDENT VARIABLE DATA
C              X-increasing order
C Y    R(N)      DEPENDENT VARIABLE DATA
C--- OUTPUT
C A    R(N)    LAMBDA -> A   WHERE  Y=A+X*(B+X*(C+D*X))
C B    R(N)    D      -> B   I-TH FOR RANGE (X(I-1), X(I))
C C    R(N)    M         C   (A,B,C,D FOR I=1 ARE SAME AS THOSE FOR I=2)
C D    R(N)    M         D
C
C--- NOTES
C REF-1   P. F. DAVIS AND PHILIP RABINOWITZ (1984)
C         METHODS PF NUMERICAL INTEGRATION, SECOND EDITION
C         ACADEMIC PRESS, INC., PP612.
C$ENDI
C
      DIMENSION X(N),Y(N),A(N),B(N),C(N),D(N)
C
      GOTO (1,2,3,4), N
C N > 4
      A(1)=0.0
      A(N)=1.0
      B(1)=0.0
      B(N)=0.0
      H2=X(2)-X(1)
      S2=(Y(2)-Y(1))/H2
      DO 5 I=2,N-1
      H1=H2
      H2=X(I+1)-X(I)
      S1=S2
      S2=(Y(I+1)-Y(I))/H2
      A(I)=1.0/(1.0+H1/H2)
    5 B(I)=6.0*(S2-S1)/(H2+H1)
CC
      Q2=0.0
      U2=0.0
      DO 6 I=1,N
      Q1=Q2
      U1=U2
      P2=(1.0-A(I))*Q1+2.0
      Q2=-A(I)/P2
      U2=(B(I)-(1.0-A(I))*U1)/P2
      A(I)=Q2
    6 B(I)=U2
      C(N)=B(N)
      DO 7 I=N-1,1,-1
    7 C(I)=A(I)*C(I+1)+B(I)
CC
      X2=X(1)
      C2=C(1)/6.0
      DO 8 I=2,N
      H2=X(I)-X(I-1)
      X1=X2
      X2=X(I)
      C1=C2
      C2=C(I)/6.0
      P1=Y(I-1)/H2-C1*H2
      P2=Y(I  )/H2-C2*H2
      A(I)=    ( C1*X2**3-C2*X1**3)/H2+P1*X2-P2*X1
      B(I)=3.0*(-C1*X2**2+C2*X1**2)/H2-P1   +P2
      C(I)=3.0*( C1*X2   -C2*X1   )/H2
    8 D(I)=    (-C1      +C2      )/H2
      GOTO 11
C N=1
    1 A(1)=Y(1)
      B(1)=0.0
      C(1)=0.0
      D(1)=0.0
      RETURN
C N=2
    2 X1=X(1)
      X2=X(2)
      Z1=Y(1)/(X1-X2)
      Z2=Y(2)/(X2-X1)
      A(2)=-X2*Z1-X1*Z2
      B(2)=Z1+Z2
      C(2)=0.0
      D(2)=0.0
      GOTO 11
C N=3
    3 X1=X(1)
      X2=X(2)
      X3=X(3)
      Z1=Y(1)/(X1-X2)/(X1-X3)
      Z2=Y(2)/(X2-X3)/(X2-X1)
      Z3=Y(3)/(X3-X1)/(X3-X2)
      A(2)=X2*X3*Z1+X3*X1*Z2+X1*X2*Z3
      B(2)=-(X2+X3)*Z1-(X3+X1)*Z2-(X1+X2)*Z3
      C(2)=Z1+Z2+Z3
      D(2)=0.0
      A(3)=A(2)
      B(3)=B(2)
      C(3)=C(2)
      D(3)=D(2)
      GOTO 11
C N=4
    4 X1=X(1)
      X2=X(2)
      X3=X(3)
      X4=X(4)
      Z1=Y(1)/(X1-X2)/(X1-X3)/(X1-X4)
      Z2=Y(2)/(X2-X3)/(X2-X4)/(X2-X1)
      Z3=Y(3)/(X3-X4)/(X3-X1)/(X3-X2)
      Z4=Y(4)/(X4-X1)/(X4-X2)/(X4-X3)
      A(2)=-X2*X3*X4*Z1-X3*X4*X1*Z2-X4*X1*X2*Z3-X1*X2*X3*Z4
      B(2)=(X2*X3+X3*X4+X4*X2)*Z1+(X3*X4+X4*X1+X1*X3)*Z2
     &    +(X4*X1+X1*X2+X2*X4)*Z3+(X1*X2+X2*X3+X3*X1)*Z4
      C(2)=-(X2+X3+X4)*Z1-(X3+X4+X1)*Z2-(X4+X1+X2)*Z3-(X1+X2+X3)*Z4
      D(2)=Z1+Z2+Z3+Z4
      DO 10 I=3,4
      A(I)=A(2)
      B(I)=B(2)
      C(I)=C(2)
   10 D(I)=D(2)
C
   11 A(1)=A(2)
      B(1)=B(2)
      C(1)=C(2)
      D(1)=D(2)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  AXB
      SUBROUTINE AXB(C,A,B,NI,NK,NJ,NCI,NAI,NBI)

      include 'precision.f'

C  C = A*B
C--- HISTORY
C 88. 6. 6   REGISTERED BY T. NAKAJIMA
C--- INPUT
C A      R(NAI,*)     2-DIM ARRAY  A.
C B      R(NBI,*)     2-DIM ARRAY  B.
C NI, NK, NJ  I       C(I,J) = A(I,K)*B(K,J)
C                     I=1,NI; K=1,NK; J=1,NJ
C NCI      I          SIZE FOR C(NCI,*)
C NAI      I          SIZE FOR A(NAI,*)
C NBI      I          SIZE FOR B(NBI,*)
C--- OUTPUT
C C      R(NCI,*)     A*B.
C$ENDI
      DIMENSION A(NAI,NK),B(NBI,NJ),C(NCI,NJ)
      DO 1 I=1,NI
      DO 1 J=1,NJ
      S=0.0
      DO 2 K=1,NK
    2 S=S+A(I,K)*B(K,J)
    1 C(I,J)=S
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE    TNVSS2(N,A,DT,E,NN,IW,ERR)

      include 'precision.f'

C$NAME  TNVSS2
C     INVERSION OF REAL MATRIX. SWEEP OUT, COMPLETE POSITIONING.
C--- HISTORY
C 71. 4.30 CREATED BY SAKATA MASATO
C 89.11.10 ADDED ERR
C 90. 1. 6 ERR*(*)
C--- INPUT
C N       I        DIMENSION OF THE MATRIX
C A     R(NN,N)    MATRIX
C NN      I        SIZE OF FIRST ARGUMENT
C E       R        CONVERGENCE CRITERION (0 IS OK)
C--- OUTPUT
C DT      R        DETERMINATION OF THE MATRIX
C IW    I(2*N)     WORKING AREA
C ERR    C*64      ERROR INDICATER. IF ' ' then no error.
C$ENDI
      CHARACTER ERR*(*)
      DIMENSION     A(NN,N)  ,IW(*)
      ERR=' '
c      IF(N-1)    910,930,101
      IF((N-1).lt.0) goto 910
      IF((N-1).eq.0) goto 930
      IF((N-1).gt.0) goto 101

  101 IF(N.GT.NN)    GO TO  900
      EPS=0.0
      DT=1.0
      DO  100     K=1,N
      PIV=0.0
      DO  110       I=K,N
      DO  110       J=K,N
      IF(ABS(A(I,J)).LE.ABS(PIV))   GO TO  110
      IPIV=I
      JPIV=J
      PIV=A(I,J)
  110 CONTINUE
      DT=DT*PIV
      IF(ABS(PIV).LE.EPS)  GO TO 920
      IF(K.EQ.1)   EPS=ABS(PIV)*E
      IF(IPIV.EQ.K)      GO TO 130
      DT=-DT
      DO 120   J=1,N
      WORK=A(IPIV,J)
      A(IPIV,J)=A(K,J)
  120 A(K,J)=WORK
  130 IF(JPIV.EQ.K)      GO TO  150
      DT=-DT
      DO 140   I=1,N
      WORK=A(I,JPIV)
      A(I,JPIV)=A(I,K)
  140 A(I,K)=WORK
  150 IW(2*K-1)=IPIV
      AA=1.0/PIV
      IW(2*K)=JPIV
      DO 210   J=1,N
  210 A(K,J)=A(K,J)*AA
      DO 220  I=1,N
      IF(I.EQ.K)    GO TO  220
      AZ=A(I,K)
      IF(AZ.EQ.0.0)   GO TO  220
      DO 230   J=1,N
  230 A(I,J)=A(I,J)-A(K,J)*AZ
      A(I,K)=-AA*AZ
  220 CONTINUE
  100 A(K,K)=AA
      DO  400 KK=2,N
      K=N+1-KK
      IJ=IW(2*K)
      IF(IJ.EQ.K)   GO TO  420
      DO 410   J=1,N
      WORK=A(IJ,J)
      A(IJ,J)=A(K,J)
  410 A(K,J)=WORK
  420 IJ=IW(2*K-1)
      IF(IJ.EQ.K)   GO TO  400
      DO 430   I=1,N
      WORK=A(I,IJ)
      A(I,IJ)=A(I,K)
  430 A(I,K)=WORK
  400 CONTINUE
      RETURN
  910 ERR='ERROR IN TINVSS: N.LE.0'
      RETURN
  900 ERR='ERROR IN TINVSS: N.GT.NN'
      RETURN
  920 DT=0.0
      INDER=N-K+1
      NNN=K-1
      WRITE(ERR,1) NNN
    1 FORMAT('TINVSS: ILL CONDITIONED MATRIX WITH RANK ',I5)
      RETURN
  930 DT=A(1,1)
      K=1
      IF(DT.EQ.0.0)     GO TO  920
      A(1,1)=1.0/A(1,1)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE SYMTRX(ROWIJ,M,ROOT,EIGV,NI,WK,IER)

      include 'precision.f'

C$NAME  SYMTRX
C$LINK  CPCON
C SOLVES EIGENFUNCTION PROBLEM FOR SYMMETRIC MATRIX
C--- HISTORY
C 89.12. 4 MODIFIED WITH CNCPU
C 90. 1. 6 CNCPU IS REPLACED BY PREC.
C--- INPUT
C M       I      ORDER OF ORIGINAL SYMMETRIC MATRIX
C NI      I      INITIAL DIMENSION OF -ROOT-, -EIGV- AND -WK-
C ROWIJ  R(*)    SYMMETRIC STORAGE MODE OF ORDER M*(M+1)/2
C--- OUTPUT
C EIGV   R(NI,M) EIGENVECTORS OF ORIGINAL SYMMETRIC MATRIX
C IER     I      INDEX FOR ROOT(J) FAILED TO CONVERGE (J=IER-128)
C ROOT   R(M)    EIGENVALUES OF ORIGINAL SYMMETRIC MATRIX
C ROWIJ          STORAGE OF HOUSEHOLDER REDUCTION ELEMENTS
C WK             WORK AREA
C$ENDI
c_GD      REAL ROWIJ(*),ROOT(*),WK(*),EIGV(NI,*),CCP(3)
      dimension ROWIJ(*),ROOT(*),WK(*),EIGV(NI,*),CCP(3)
C+++ ADD EPSCP
C     DATA  RDELP/ 1.1921E-07 /
      CALL CPCON(CCP)
      RDELP=CCP(1)*10
C+++
      IER = 0
      MP1 = M + 1
      MM = (M*MP1)/2 - 1
      MBEG = MM + 1- M
C
C+---------------------------------------------------------------------+
C|          LOOP-100 REDUCE -ROWIJ- (SYMMETRIC STORAGE MODE) TO A      |
C|          SYMMETRIC TRIDIAGONAL FORM BY HOUSEHOLDER METHOD           |
C|                      CF. WILKINSON, J.H., 1968,                     |
C|              THE ALGEBRAIC EIGENVALUE PROBLEM, PP 290-293.          |
C|          LOOP-30&40 AND 50 FORM ELEMENT OF A*U AND ELEMENT P        |
C+---------------------------------------------------------------------+
      DO 100 II=1,M
      I = MP1 - II
      L = I - 1
      H = 0.0
      SCALE = 0.0
      IF (L.LT.1) THEN
C|          SCALE ROW (ALGOL TOL THEN NOT NEEDED)
      WK(I) = 0.0
      GO TO 90
      END IF
      MK = MM
      DO 10 K=1,L
      SCALE = SCALE + ABS(ROWIJ(MK))
      MK = MK - 1
   10 CONTINUE
      IF (SCALE.LE.0.0) THEN
      WK(I) = 0.0
      GO TO 90
      END IF
C
      MK = MM
      DO 20 K = 1,L
      ROWIJ(MK) = ROWIJ(MK)/SCALE
      H = H + ROWIJ(MK)*ROWIJ(MK)
      MK = MK - 1
   20 CONTINUE
      WK(I) = SCALE*SCALE*H
      F = ROWIJ(MM)
      G = - SIGN(SQRT(H),F)
      WK(I) = SCALE*G
      H = H - F*G
      ROWIJ(MM) = F - G
      IF (L.GT.1) THEN
      F = 0.0
      JK1 = 1
      DO 50 J=1,L
      G = 0.0
      IK = MBEG + 1
      JK = JK1
      DO 30 K=1,J
      G = G + ROWIJ(JK)*ROWIJ(IK)
      JK = JK + 1
      IK = IK + 1
   30 CONTINUE
      JP1 = J + 1
      IF (L.GE.JP1) THEN
      JK = JK + J - 1
      DO 40 K=JP1,L
      G = G + ROWIJ(JK)*ROWIJ(IK)
      JK = JK + K
      IK = IK + 1
   40 CONTINUE
      END IF
      WK(J) = G/H
      F = F + WK(J)*ROWIJ(MBEG+J)
      JK1 = JK1 + J
   50 CONTINUE
      HH = F/(H+H)
C
      JK = 1
      DO 70 J=1,L
      F = ROWIJ(MBEG+J)
      G = WK(J) - HH*F
      WK(J) = G
      DO 60 K=1,J
      ROWIJ(JK) = ROWIJ(JK) - F*WK(K) - G*ROWIJ(MBEG+K)
      JK = JK + 1
   60 CONTINUE
   70 CONTINUE
      END IF
C
      DO 80 K=1,L
      ROWIJ(MBEG+K) = SCALE*ROWIJ(MBEG+K)
   80 CONTINUE
   90 ROOT(I) = ROWIJ(MBEG+I)
      ROWIJ(MBEG+I) = H*SCALE*SCALE
      MBEG = MBEG - I + 1
      MM = MM - I
  100 CONTINUE
C
C+---------------------------------------------------------------------+
C|          LOOP-210 COMPUTE EIGENVALUES AND EIGENVECTORS              |
C|          SETUP WORK AREA LOCATION EIGV TO THE IDENTITY MATRIX       |
C|          LOOP-140 FOR FINDING SMALL SUB-DIAGONAL ELEMENT            |
C|          LOOP-160 FOR CONVERGENCE OF EIGENVALUE J (MAX. 30 TIMES)   |
C|          LOOP-190 FOR QL TRANSFORMATION AND LOOP-180 FORM VECTORS   |
C+---------------------------------------------------------------------+
      DO 110 I=1,M-1
  110 WK(I) = WK(I+1)
      WK(M) = 0.0
      B = 0.0
      F = 0.0
      DO 130 I=1,M
      DO 120 J=1,M
  120 EIGV(I,J) = 0.0
      EIGV(I,I) = 1.0
  130 CONTINUE
C
      DO 210 L=1,M
      J = 0
      H = RDELP*(ABS(ROOT(L))+ABS(WK(L)))
      IF (B.LT.H) B = H
      DO 140 N=L,M
      K = N
      IF (ABS(WK(K)).LE.B) GO TO 150
  140 CONTINUE
  150 N = K
      IF (N.EQ.L) GO TO 200
C
  160 CONTINUE
      IF (J.EQ.30) THEN
      IER = 128 + L
      RETURN
      END IF
C
      J = J + 1
      L1 = L + 1
      G = ROOT(L)
      P = (ROOT(L1)-G)/(WK(L)+WK(L))
      R = ABS(P)
      IF (RDELP*ABS(P).LT.1.0) R = SQRT(P*P+1.0)
      ROOT(L) = WK(L)/(P+SIGN(R,P))
      H = G - ROOT(L)
      DO 170 I=L1,M
      ROOT(I) = ROOT(I) - H
  170 CONTINUE
      F = F + H
C
      P = ROOT(N)
      C = 1.0
      S = 0.0
      NN1 = N - 1
      NN1PL = NN1 + L
      IF (L.LE.NN1) THEN
      DO 190 II=L,NN1
      I = NN1PL - II
      G = C*WK(I)
      H = C*P
      IF (ABS(P).LT.ABS(WK(I))) THEN
      C = P/WK(I)
      R = SQRT(C*C+1.0)
      WK(I+1) = S*WK(I)*R
      S = 1.0/R
      C = C*S
      ELSE
      C = WK(I)/P
      R = SQRT(C*C+1.0)
      WK(I+1) = S*P*R
      S = C/R
      C = 1.0/R
      END IF
      P = C*ROOT(I) - S*G
      ROOT(I+1) = H + S*(C*G+S*ROOT(I))
      IF (NI.GE.M) THEN
      DO 180 K=1,M
      H = EIGV(K,I+1)
      EIGV(K,I+1) = S*EIGV(K,I) + C*H
      EIGV(K,I) = C*EIGV(K,I) - S*H
  180 CONTINUE
      END IF
  190 CONTINUE
      END IF
      WK(L) = S*P
      ROOT(L) = C*P
      IF (ABS(WK(L)).GT.B) GO TO 160
  200 ROOT(L) = ROOT(L) + F
  210 CONTINUE
C
C+---------------------------------------------------------------------+
C|          BACK TRANSFORM EIGENVECTORS OF THE ORIGINAL MATRIX FROM    |
C|          EIGENVECTORS 1 TO M OF THE SYMMETRIC TRIDIAGONAL MATRIX    |
C+---------------------------------------------------------------------+
      DO 250 I=2,M
      L = I - 1
      IA = (I*L)/2
      IF (ABS(ROWIJ(IA+I)).GT.0.0) THEN
      DO 240 J=1,M
      SUM = 0.0
      DO 220 K=1,L
      SUM = SUM + ROWIJ(IA+K)*EIGV(K,J)
  220 CONTINUE
      SUM = SUM/ROWIJ(IA+I)
      DO 230 K=1,L
      EIGV(K,J) = EIGV(K,J) - SUM*ROWIJ(IA+K)
  230 CONTINUE
  240 CONTINUE
      END IF
  250 CONTINUE
C
      RETURN
      END
c
c???-------------------------------------------------------------------------------      
c
      subroutine SMLLOP(X,CR,CI,QEXT,QSCA)

      include 'precision.f'

C$NAME SMLLOP
C Optical cross sections for small particles (X<0.1)
C In this approximation
C     Q(ANG,1)=3.0/8.0/PI*QSCA
C     Q(ANG,2)=Q(ANG,1)*COS(ANG)**2
C     Q(ANG,3)=Q(ANG,1)*COS(ANG)
C     Q(ANG,4)=0
C--- history
C 95. 9.19 Created
C--- input
C X       R       Size parameter (<1.5))
C CR      R       M= CR - i CI
C CI      R       CI>0
C--- output
C QEXT    R       Extinction efficiency factor
C QSCA    R       Scattering efficiency factor
C---
      A=(CR**2+CI**2)**2
      B=CR**2-CI**2
      G=CR*CI
      Z1=A+4*B+4
      Z2=4*A+12*B+9
      E1=24*G/Z1
      E3=G*(4.0/15.0+20.0/3.0/Z2+24.0/5.0/Z1**2*(7*A+4*B-20))
      E4=8.0/3.0/Z1**2*((A+B-2)**2-36*G**2)
      S4=8.0/3.0/Z1**2*((A+B-2)**2+36*G**2)
      QEXT=E1*X+E3*X**3+E4*X**4
      QSCA=S4*X**4
      return
      end
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE  MTQGAUSN( GWT, GMU, M )

      include 'precision.f'

C$NAME  MTQGAUSN
c   Nom de subroutine mofifie� le 18/02/05 
C  COMPUTE WEIGHTS AND ABSCISSAE FOR ORDINARY GAUSSIAN QUADRATURE
C   (NO WEIGHT FUNCTION INSIDE INTEGRAL) ON THE INTERVAL (0,1)
C--- HISTORY
C 90. 1.17  REGISTERED
C--- INPUT
C M        I       ORDER OF QUADRATURE RULE
C--- OUTPUT
C GMU    R(M)      ARRAY OF ABSCISSAE (0, 1)
C GWT    R(M)      ARRAY OF WEIGHTS   SUM=1
C--- NOTES
C REFERENCE:  DAVIS,P.J. AND P. RABINOWITZ, METHODS OF NUMERICAL
C             INTEGRATION,ACADEMIC PRESS, NEW YORK, 1975, PP. 87
C METHOD:     COMPUTE THE ABSCISSAE AS ROOTS OF THE LEGENDRE
C             POLYNOMIAL P-SUB-N USING A CUBICALLY CONVERGENT
C             REFINEMENT OF NEWTON'S METHOD.  COMPUTE THE
C             WEIGHTS FROM EQ. 2.7.3.8 OF DAVIS/RABINOWITZ.
C             ACCURACY:  AT LEAST 13 SIGNIFICANT DIGITS.
C--- INTERNAL VARIABLES
C PM2,PM1,P : 3 SUCCESSIVE LEGENDRE POLYNOMIALS
C PPR       : DERIVATIVE OF LEGENDRE POLYNOMIAL
C P2PRI     : 2ND DERIVATIVE OF LEGENDRE POLYNOMIAL
C TOL       : CONVERGENCE CRITERION
C X,XI      : SUCCESSIVE ITERATES IN CUBICALLY-
C             CONVERGENT VERSION OF NEWTON'S METHOD
C            ( SEEKING ROOTS OF LEGENDRE POLYNOMIAL )
C$ENDI
      dimension     GMU( M ), GWT( M )
c_GD      REAL     CONA, GMU( M ), GWT( M ), PI, T
      INTEGER  LIM, M, NP1
      DOUBLE   PRECISION  EN, NNP1, P, PM1, PM2, PPR, P2PRI, PROD,
     &                    TMP, TOL, X, XI
      DATA     TOL / 1.0D-13 /
      DATA     PI  / 3.1415926535898 /
C
      IF ( M.LE.1 )  THEN
         M = 1
         GMU( 1 ) = 0.5
         GWT( 1 ) = 1.0
         RETURN
      END IF
C
      EN   = M
      NP1  = M + 1
      NNP1 = M * NP1
      CONA = REAL( M-1 ) / ( 8 * M**3 )
C+---------------------------------------------------------------------+
C|         INITIAL GUESS FOR K-TH ROOT OF LEGENDRE POLYNOMIAL,         |
C|         FROM DAVIS/RABINOWITZ  EQ. (2.7.3.3A)                       |
C+---------------------------------------------------------------------+
      LIM  = M / 2
      DO 30  K = 1, LIM
         T = ( 4*K - 1 ) * PI / ( 4*M + 2 )
         X = COS ( T + CONA / TAN( T ) )
C
C+---------------------------------------------------------------------+
C|             RECURSION RELATION FOR LEGENDRE POLYNOMIALS             |
C|       INITIALIZE LEGENDRE POLYNOMIALS: (PM2) P-SUB-0, (PM1) P-SUB-1 |
C+---------------------------------------------------------------------+
 10       PM2 = 1.D0
         PM1 = X
         DO 20 NN = 2, M
            P   = ( ( 2*NN - 1 ) * X * PM1 - ( NN-1 ) * PM2 ) / NN
            PM2 = PM1
            PM1 = P
 20       CONTINUE
C
         TMP   = 1.D0 / ( 1.D0 - X**2 )
         PPR   = EN * ( PM2 - X * P ) * TMP
         P2PRI = ( 2.D0 * X * PPR - NNP1 * P ) * TMP
         XI    = X - ( P / PPR ) * ( 1.D0 +
     &               ( P / PPR ) * P2PRI / ( 2.D0 * PPR ) )
C
         IF ( DABS(XI-X) .GT. TOL ) THEN
C|          CHECK FOR CONVERGENCE
            X = XI
            GO TO 10
         END IF
C
C       ** ITERATION FINISHED--CALC. WEIGHTS, ABSCISSAE FOR (-1,1)
         GMU( K ) = - X
         GWT( K ) = 2.D0 / ( TMP * ( EN * PM2 )**2 )
         GMU( NP1 - K ) = - GMU( K )
         GWT( NP1 - K ) =   GWT( K )
 30    CONTINUE
C
      IF ( MOD( M,2 ) .NE. 0 )  THEN
C|       SET MIDDLE ABSCISSA AND WEIGHT FOR RULES OF ODD ORDER
         GMU( LIM + 1 ) = 0.0
         PROD = 1.D0
         DO 40 K = 3, M, 2
            PROD = PROD * K / ( K-1 )
 40       CONTINUE
         GWT( LIM + 1 ) = 2.D0 / PROD**2
      END IF
C
      DO 50  K = 1, M
C|       CONVERT FROM (-1,1) TO (0,1)
         GMU( K ) = 0.5 * GMU( K ) + 0.5
         GWT( K ) = 0.5 * GWT( K )
 50    CONTINUE
C
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      FUNCTION ERFC(X)

      include 'precision.f'

C$NAME ERFC
C Complementary error function=(2/sqrt(pi) integral(x,inf) exp(-t**2)dt
C 92.12.22: Created BY NAKAJIMA
C--- INPUT
C X       R      Independent variable 0 to inf
C--- OUTPUT
C ERFC   RF      Complementary error function
C
      PARAMETER (C1 = 7.05230784E-2, C2 = 4.22820123E-2)
      PARAMETER (C3 = 9.2705272E-3,  C4 = 1.520143E-4)
      PARAMETER (C5 = 2.765672E-4,   C6 = 4.30638 E-5)
      V=ABS(X)
      IF(V.LE.7) THEN
        ERFC=1.0/(1+V*(C1+V*(C2+V*(C3+V*(C4+V*(C5+C6*V))))))**16
       ELSE
        ERFC=0
      ENDIF
      IF(X.LT.0) ERFC=2-ERFC
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME FRNLR
      SUBROUTINE FRNLR(CR,CI,WI,WR,R1,R2)

      include 'precision.f'

C FRESNEL REFLECTION COEFFICIENTS
C--- HISTORY
C 88. 6.16  CREATED
C--- INPUT
C CR      R      REAL PART OF THE COMPLEX REFRACTION INDEX
C CI      R      IMAGINARY PART M = CR + I*CI
C WI      R      INCIDENT ANGLE IN DEGREES
C--- OUTPUT
C WR      R      REFRACTION ANGLE IN DEGREES
C                IF TOTAL REFLECTION, THEN WR=999
C R1      R      REFLECTIVITY FOR POLARIZATION
C R2      R      REFLECTIVITY FOR POLARIZATION
C--- REFERENCE
C K.-N. LIOU
C$ENDI
      PARAMETER (PI=3.141592653, RD=PI/180.0)
      CI1=-CI
      C=COS(WI*RD)
      S=SIN(WI*RD)
      WR=S/CR
      IF(WR.GT.1.0) THEN
      WR=999
      ELSE
      WR=ASIN(WR)/RD
      ENDIF
      EN1=CR**2-CI1**2
      EN2=2*CR*CI1
      EN3=EN1-S**2
      U=SQRT(ABS(EN3+SQRT(EN3**2+EN2**2))/2)
      V=SQRT(ABS(U**2-EN3))
      U2=U**2+V**2
      R1=1/(U*C/(U2+C**2)+0.5)-1
      R2=1/((EN1*U+EN2*V)*C/((EN1**2+EN2**2)*C**2+U2)+0.5)-1
      RETURN
      END
c
c???-------------------------------------------------------------------------------
c      
      SUBROUTINE OCNR11(M,NDA,AMUA,WA,CR,CI,U10,R)

      include 'precision.f'

C$NAME OCNR11
C$LINK MTQGAUSN SEARF1
C REFLECTION MATRIX OF OCEAN SURFACE
C--- HISTORY
C 92. 9. 1 CREATED BY HASUMI
C    12.23 MODIFIED BY NAKAJIMA
C 95. 6. 2 Generated from OCNRF1
C--- INPUT
C M      I     FOURIER ORDER
C KNDM   I     DECLARED DIMENSION OF R AND AMUA
C NDA    I     USED DIMENSION OF R AND AMUA
C AMUA  R(NDA) QUADRATURE POINTS IN HEMISPHERE
C              DECREASING ORDER (ZENITH TO HORIZON, OR, 1 -> 0)
C WA    R(NDA) QUADRATURE WEIGHTS
C EM     R     RELATIVE REFRACTIVE INDEX
C              ABOUT 1.33 FROM ATMOSPHERE TO OCEAN
C              ABOUT 1/1.33 FROM OCEAN TO ATMOSPHERE
C U10    R     WIND VELOCITY AT 10 M ABOVE THE SURFACE
C CR       R    Relative refractive index of the media
C               About 1.33 for atmosphere to ocean incidence,
C               and 1/1.33 for ocean to atmosphere incidence.
C CI       R    Relative refractive index for imaginary part
C               M = CR + I*CI
C--- OUTPUT
C R    R(KNDA,NDA)  REFLECTION MATRIX FOR M-TH FOURIER ORDER
C                    ur = R * ui
C$END
      SAVE
C$PRPC
      include 'parameter.rayjn'
      
c     PARAMETER (KNDM=10)
      PARAMETER(PI=3.141592654)
      DIMENSION AMUA(KNDM),WA(KNDM),R(KNDM, KNDM)
C LOCAL VARIABLES
      PARAMETER (KN=30,KNDM1=KNDM+1)
      DIMENSION X(KNDM1),GX(KN),GW(KN),FI1(KN),FI2(KN),COSM1(KN)
     & ,COSM2(KN),XB(5,2),NS(2),RR(KNDM, KNDM)
      DATA INIT/1/
C
      IF(INIT.GT.0) THEN
        N=KN
        INIT=0
        CALL MTQGAUSN(GW, GX, N )
      ENDIF
C Parameters for integration
      SIGMA=SQRT(0.00534*U10)
      DISPA=ATAN(SIGMA)
      COSDS=COS(DISPA)
      DO 4 K=1,N
      DFI2=PI-DISPA
      FI1(K)=DISPA+DFI2*GX(K)
      FI2(K)=DISPA*GX(K)
      COSM1(K)=2*COS(FI1(K)*M)*DFI2 *GW(K)
    4 COSM2(K)=2*COS(FI2(K)*M)*DISPA*GW(K)
      X(1)=1
      IF(NDA.GE.2) THEN
        DO 1 I = 2, NDA
    1   X(I)=(AMUA(I-1)+AMUA(I))/2
      ENDIF
      X(NDA+1)=0
C
      DO 2 I = 1, NDA
CC X1=COS(THETA(I)-DISPA), X2=COS(THETA(I)+DISPA)
CC   WHERE AMUA(I)=COS(THETA(I))
      B=SQRT((1-AMUA(I)**2)*(1-COSDS**2))
      X1=AMUA(I)*COSDS+B
      X2=AMUA(I)*COSDS-B
CC SETING MU-BAOUDARY FOR MU-INEGRATION
      NS1=1
      XB(1,1)=X(I)
      IF(X1.LT.X(I)) THEN
        NS1=NS1+1
        XB(NS1,1)=X1
      ENDIF
      NS1=NS1+1
      XB(NS1,1)=AMUA(I)
      IF(X2.GT.X(I+1)) THEN
        NS1=NS1+1
        XB(NS1,1)=X2
      ENDIF
      NS1=NS1+1
      XB(NS1,1)=X(I+1)
      NS(1)=NS1-1
      NS(2)=1
      XB(1,2)=X(I)
      XB(2,2)=X(I+1)
C
      DO 2 J = 1, NDA
      IF(I.EQ.J) THEN
        IEQ=1
       ELSE
        IEQ=2
      ENDIF
CC MU-INTEGRATION
      RIJ=0
      AMI = AMUA(J)
        DO 3 IS=1,NS(IEQ)
        DX=XB(IS,IEQ)-XB(IS+1,IEQ)
        DO 3 II = 1, N
          AME=XB(IS+1,IEQ)+DX*GX(II)
          W=DX*GW(II)*AME
          DO 3 K = 1, N
    3     RIJ=RIJ+(COSM1(K)*SEARF1(AME,AMI,FI1(K),U10,CR,CI)
     &            +COSM2(K)*SEARF1(AME,AMI,FI2(K),U10,CR,CI))*W
    2 RR(I,J)=RIJ/WA(I)
C SYMMETRIC OPERATION
      DO 5 I=1,NDA
      DO 5 J=1,I
      RRR=(RR(I,J)+RR(J,I))/2
      R(I,J)=RRR/AMUA(I)*WA(J)
    5 R(J,I)=RRR/AMUA(J)*WA(I)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE OCNR31(M,NDA,AMUA,WA,AM0,CR,CI,U10,SR)

      include 'precision.f'

C$NAME OCNR31
C$LINK MTQGAUSN SEARF1
C REFLECTION MATRIX OF OCEAN SURFACE
C--- HISTORY
C 92. 9. 1 CREATED BY HASUMI
C    12.23 MODIFIED BY NAKAJIMA
C 93. 3.22 /WA(I) debugged by Takashi
C     3.29 AMI -> AM0
C--- INPUT
C M      I     FOURIER ORDER
C KNDM   I     DECLARED DIMENSION OF R AND AMUA
C NDA    I     USED DIMENSION OF R AND AMUA
C AMUA  R(NDA) QUADRATURE POINTS IN HEMISPHERE
C              DECREASING ORDER (ZENITH TO HORIZON, OR, 1 -> 0)
C WA    R(NDA) Quadrature weights
C AM0    R     Cos (Solar Zenith Angle)
C CR       R    Relative refractive index of the media
C               About 1.33 for atmosphere to ocean incidence,
C               and 1/1.33 for ocean to atmosphere incidence.
C CI       R    Relative refractive index for imaginary part
C               M = CR + I*CI
C U10    R     WIND VELOCITY AT 10 M ABOVE THE SURFACE
C--- OUTPUT
C SR    SR(KNDA)  REFLECTION SOURCE MATRIX FOR M-TH FOURIER ORDER
C$END
      SAVE
C$PRPC
      include 'parameter.rayjn'
      
c     PARAMETER (KNDM=10)
      PARAMETER(PI=3.141592654)
      DIMENSION AMUA(KNDM),WA(KNDM),SR(KNDM)
C LOCAL VARIABLES
      PARAMETER (KN=30,KNDM1=KNDM+1)
      DIMENSION X(KNDM1),GX(KN),GW(KN),FI1(KN),FI2(KN),COSM1(KN)
     & ,COSM2(KN),XB(5,2),NS(2)
      DATA INIT/1/
C
      IF(INIT.GT.0) THEN
        N=KN
        INIT=0
        CALL MTQGAUSN(GW, GX, N )
      ENDIF
C Parameters for integration
      SIGMA=SQRT(0.00534*U10)
      DISPA=ATAN(SIGMA)
      COSDS=COS(DISPA)
      DO 4 K=1,N
      DFI2=PI-DISPA
      FI1(K)=DISPA+DFI2*GX(K)
      FI2(K)=DISPA*GX(K)
      COSM1(K)=2*COS(FI1(K)*M)*DFI2 *GW(K)
    4 COSM2(K)=2*COS(FI2(K)*M)*DISPA*GW(K)
      X(1)=1
      IF(NDA.GE.2) THEN
        DO 1 I = 2, NDA
    1   X(I)=(AMUA(I-1)+AMUA(I))/2
      ENDIF
      X(NDA+1)=0
C
      DO 2 I = 1, NDA
CC X1=COS(THETA(I)-DISPA), X2=COS(THETA(I)+DISPA)
CC   WHERE AMUA(I)=COS(THETA(I))
      B=SQRT((1-AMUA(I)**2)*(1-COSDS**2))
      X1=AMUA(I)*COSDS+B
      X2=AMUA(I)*COSDS-B
CC SETING MU-BAOUDARY FOR MU-INEGRATION
      NS1=1
      XB(1,1)=X(I)
      IF(X1.LT.X(I)) THEN
        NS1=NS1+1
        XB(NS1,1)=X1
      ENDIF
      NS1=NS1+1
      XB(NS1,1)=AMUA(I)
      IF(X2.GT.X(I+1)) THEN
        NS1=NS1+1
        XB(NS1,1)=X2
      ENDIF
      NS1=NS1+1
      XB(NS1,1)=X(I+1)
      NS(1)=NS1-1
      NS(2)=1
      XB(1,2)=X(I)
      XB(2,2)=X(I+1)
C
      IF(AMUA(I).EQ.AM0) THEN
        IEQ=1
       ELSE
        IEQ=2
      ENDIF
CC MU-INTEGRATION
      RIJ=0
        DO 3 IS=1,NS(IEQ)
        DX=XB(IS,IEQ)-XB(IS+1,IEQ)
        DO 3 II = 1, N
          AME=XB(IS+1,IEQ)+DX*GX(II)
          W=DX*GW(II)*AME
          DO 3 K = 1, N
    3     RIJ=RIJ+(COSM1(K)*SEARF1(AME,AM0,FI1(K),U10,CR,CI)
     &            +COSM2(K)*SEARF1(AME,AM0,FI2(K),U10,CR,CI))*W
    2 SR(I)=RIJ/AMUA(I)/WA(I)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  EQ20
      SUBROUTINE EQ20(A,B,NI,NJ,NA1,NA2)

      include 'precision.f'

C A(*,*) = B
C--- HISTORY
C B        R      SOURCE SCALER.
C NI       I      A(I,J)= B,    I=1,NI;   J=1,NJ
C NJ       I
C NA1      I      SIZE FOR A(NA1,NA2)
C NA2      I
C--- OUTPUT
C A   R(NA1,NA2)  DESTINATION 2-DIM ARRAY   A(*,*) = B
C$ENDI
      DIMENSION A(NA1,NA2)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J)=B
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  AAPB
      SUBROUTINE AAPB(A,B,NI,NJ,NAI,NBI)

      include 'precision.f'

C A=A+B
C--- HISTORY
C 88. 6. 6  REGISTERED BY T.NAKAJIMA
C--- INPUT
C A    R(NAI,*)    2-DIM ARRAY  A.
C B    R(NBI,*)    2-DIM ARRAY  B.
C NI      I        SUM (I,J) I=1,NI
C NJ      I        SUM (I,J) J=1,NI
C--- OUTPUT
C A                A+B
C$ENDI
      DIMENSION A(NAI,NJ),B(NBI,NJ)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J)=A(I,J)+B(I,J)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE ADD(IT,NA0,N1,N2,N3,R1D,R1U,T1D,T1U,S1D,S1U
     & ,R2D,T2D,T2U,S2D,S2U,TU,RD,SD,SU,ERR)

      include 'precision.f'

C$NAME  ADD
C$LINK  AAPB  APB  AXB  MULTI
C ADDING TWO LAYERS 1 AND 2.
C--- HISTORY
C 86.10.15  CHECK OK.
C 89.10.30  RE-EDIT.
C 90. 2. 2  ELEMINATE KND00
C--- INPUT
C IT         I      INDICATOR FOR CALCULATION OF TU AND SD.
C NA0        I      NO. OF SOLAR DIRECTIONS.
C N1,N2,N3   I       ----------------------------     MU1(I), I=1,N1
C R1D, R1U R(KNDM,   R1D, R1U, T1D, T1U, S1D, S1U    (LAYER-1)
C T1D, T1U   KNDM)   ----------------------------     MU2(I), I=1,N2
C R2D, R2U           R2D, R2U, T2D, T2U, S2D, S2U    (LAYER-2)
C T2D, T2U           ----------------------------     MU3(I),I=1,N3
C S1D, S1U R(KNDM,   SUFFIX U = UPGOING,   D = DOWNGOING INCIDENCES.
C S2D, S2U   KNA0)          R = REFLECTION,T = TRANSMISSION MATRICES.
C                           S = SOURCE MATRIX.
C--- OUTPUT
C RD       R(KNDM,   -----------------    MU1(I), I=1,N1
C TU         KNDM)   RD, TU, SD, SU       (LAYER 1+2)
C SD       R(KNDM,   -----------------    MU3(I), I=1,N3
C SU         KNA0)
C ERR       C*64     ERROR INDEX.
C--- PARAMETER
C KNA0       I       NUMBER OF SOLAR ZENITH ANGLES.
C KNDM       I       NUMBER OF QUADRATURE POINTS.
C--- AREAS FOR THIS ROUTINE
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNA0=32,KNDM=32)
C
      CHARACTER ERR*64
      DIMENSION R1D(KNDM,KNDM),R1U(KNDM,KNDM),T1D(KNDM,KNDM)
     &,T1U(KNDM,KNDM),S1D(KNDM,KNA0),S1U(KNDM,KNA0)
     &,R2D(KNDM,KNDM),T2D(KNDM,KNDM),T2U(KNDM,KNDM)
     &,S2D(KNDM,KNA0),S2U(KNDM,KNA0)
     &,TU(KNDM,KNDM),RD(KNDM,KNDM),SD(KNDM,KNA0),SU(KNDM,KNA0)
C--- WORKING AREAS
      DIMENSION AA(KNDM,KNA0),BB(KNDM,KNDM),CC(KNDM,KNDM),DD(KNDM,KNDM)
C
      CALL AXB(AA,R2D,S1D,N2,N2,NA0,KNDM,KNDM,KNDM)
      CALL APB(SU,AA,S2U,N2,NA0,KNDM,KNDM,KNDM)
      CALL AXB(CC,R2D,R1U,N2,N2,N2,KNDM,KNDM,KNDM)
      CALL MULTI(N2,CC,BB,ERR)
      IF(ERR.NE.' ') THEN
        ERR='ERROR IN MULTI OF ADD'
        RETURN
      ENDIF
      CALL AXB(SD,BB,SU,N2,N2,NA0,KNDM,KNDM,KNDM)
      CALL AXB(SU,T1U,SD,N1,N2,NA0,KNDM,KNDM,KNDM)
      CALL AAPB(SU,S1U,N1,NA0,KNDM,KNDM)
      CALL AXB(CC,T1U,BB,N1,N2,N2,KNDM,KNDM,KNDM)
      CALL AXB(DD,R2D,T1D,N2,N2,N1,KNDM,KNDM,KNDM)
      CALL AXB(BB,CC,DD,N1,N2,N1,KNDM,KNDM,KNDM)
      CALL APB(RD,R1D,BB,N1,N1,KNDM,KNDM,KNDM)
      IF(IT.LE.0) RETURN
      CALL AXB(TU,CC,T2U,N1,N2,N3,KNDM,KNDM,KNDM)
      CALL AXB(AA,R1U,SD,N2,N2,NA0,KNDM,KNDM,KNDM)
      CALL AAPB(AA,S1D,N2,NA0,KNDM,KNDM)
      CALL AXB(SD,T2D,AA,N3,N2,NA0,KNDM,KNDM,KNDM)
      CALL AAPB(SD,S2D,N3,NA0,KNDM,KNDM)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  EQ22
      SUBROUTINE EQ22(A,B,NI,NJ,NA1,NA2,NB1,NB2)

      include 'precision.f'

C  A = B
C--- HISTORY
C 88. 6. 6  REGISTERED BY T. NAKAJIMA
C--- INPUT
C B      R(NB1,NB2)     SOURCE 2-DIM ARRAY  B.
C NI       I           A(I,J) = B(I,J),  I=1,NI;  J=1,NJ
C NJ       I
C NA1      I           DIM  A(NA1,NA2)
C NA2      I
C NB1      I           DIM  B(NB1,NB2)
C--- OUTPUT
C A     R(NA1,NA2)     DESTINATION 2-DIM ARRAY  A = B.
C$ENDI
      DIMENSION A(NA1,NA2),B(NB1,NB2)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J)=B(I,J)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  EQ23
      SUBROUTINE EQ23(A,B,NI,NJ,K,NA1,NA2,NB1,NB2,NB3)

      include 'precision.f'

C  A(*,*)= B(*,*,K)
C--- HISTORY
C 88. 6. 6  REGISTERED BY T. NAKAJIMA
C--- INPUT
C B      R(NB1,NB2,NB3)     SOURCE 3-DIM ARRAY B.
C NI         I              A(I,J)= B(I,J,K)
C NJ         I              I=1,NI;  J=1,NJ
C K          I
C NA1        I              DIM A(NA1, NA2)
C NA2        I
C NB1        I              DIM B(NB1,NB2,NB3)
C NB2, NB3   I
C--- OUTPUT
C A     R(NA1,NA2)          DESTINATION 2-DIM ARRAY A(*,*) = B(*,*,K)
C$ENDI
      DIMENSION A(NA1,NA2),B(NB1,NB2,NB3)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J)=B(I,J,K)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
      SUBROUTINE MULTI(ND,CD,CC,ERR)

      include 'precision.f'

C$NAME  MULTI
C$LINK  TNVSS2
C CALCULATION OF MULTIPLE REFLECTION BETWEEN TWO LAYERS.
C--- HISTORY
C 86.10.15  CHECK OK.
C 89.10.30  RE-EDIT.
C 94. 5. 7  ERR*64 -> ERR*(*)
C--- INPUT
C ND          I     NO. OF STREAMS AT THE INTERFACE BETWEEN TWO LAYERS.
C CD      R(KNDM,   R = R1 * R2.
C           KNDM)
C--- OUTPUT
C CC      R(KNDM,   CC = ( 1 - CD )**-1 = 1 + CD + CD**2 + CD**3 + ...
C           KNDM)
C ERR     C*64      ERROR INDEX.
C--- PARAMETER
C KNDM        I     NUMBER OF QUADRATURE POINTS.
C--- AREAS FOR THIS ROUTINE
C$PRPC
      include 'parameter.rayjn'
C
c     PARAMETER (KNDM=32)
C
      CHARACTER ERR*(*)
      DIMENSION CD(KNDM,KNDM),CC(KNDM,KNDM)
C--- WORKING AREA
      PARAMETER (KNDM2=2*KNDM)
      DIMENSION IW(KNDM2)
C---
      DO 1 I=1,ND
      DO 2 J=1,ND
    2 CC(I,J)= -CD(I,J)
    1 CC(I,I)=1 - CD(I,I)
C INVERSION OF -CC-.
      EPS=0
      CALL TNVSS2(ND,CC,DT,EPS,KNDM,IW,ERR)
      IF(ERR.NE.' ') ERR='ERROR IN MULTI'
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  RP33
      SUBROUTINE RP33(A,B,NI,NJ,K,L,NA1,NA2,NA3,NB1,NB2,NB3)

      include 'precision.f'

C A(*,*,K) = B(*,*,L)
C--- HISTORY
C 88. 5. 6  REGISTERED BY T. NAKAJIMA
C--- INPUT
C B      R(NB1,NB2,NB3)     SOURCE 3-DIM ARRAY  B.
C NI,NJ,K,L    I            A(I,J,K)=B(I,J,L), I=1,NI; J=1,NJ
C NA1,NA2,NA3  I            DIM A(NA1,NA2,NA3)
C NB1,NB2,NB3  I            DIM B(NB1,NB2,NB3)
C--- OUTPUT
C A      R(NA1,NA2,NA3)     DESTINATION 3-DIM ARRAY A(*,*,K)=B(*,*,L)
C$ENDI
      DIMENSION A(NA1,NA2,NA3),B(NB1,NB2,NB3)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J,K)=B(I,J,L)
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  RP30
      SUBROUTINE RP30(A,B,NI,NJ,K,NA1,NA2,NA3)

      include 'precision.f'

C A(*,*,K) = B
C--- HISTORY
C 88. 5. 6   REGISTERED BY T. NAKAJIMA
C--- INPUT
C B            R       SOURCE SCALER
C NI,NJ,K      I       A(I,J,K)=B,  I=1,NI; J=1,NJ
C NA1,NA2,NA3  I       DIM A(NA1,NA2,NA3)
C--- OUTPUT
C A    R(NA1,NA2,NA3)  DESTINATION 3-DIM ARRAY A(*,*,K)=B
C$ENDI
      DIMENSION A(NA1,NA2,NA3)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 A(I,J,K)=B
      RETURN
      END
c
c-------------------------------------------------------------------------------
c      
C$NAME  APB
      SUBROUTINE APB(C,A,B,NI,NJ,NCI,NAI,NBI)

      include 'precision.f'

C C=A+B
C--- HISTORY
C 88. 6. 6  REGISTERED BY T. NAKAJIMA
C--- INPUT
C  A       R(NAI,*)     2-DIM ARRAY A.
C  B       R(NBI,*)     2-DIM ARRAY B.
C NI         I          SUM (I,J) , I=1,NI
C NJ         I          SUM (I,J) , J=1,NJ
C NCI        I          SIZE FOR C(NCI,*)
C NAI        I          SIZE FOR A(NAI,*)
C NBI        I          SIZE FOR B(NBI,*)
C--- OUTPUT
C  C       R(NCI,*)     A+B
C$ENDI
      DIMENSION A(NAI,NJ),B(NBI,NJ),C(NCI,NJ)
      DO 1 J=1,NJ
      DO 1 I=1,NI
    1 C(I,J)=A(I,J)+B(I,J)
      RETURN
      END
c
c------------------------------------------------------------------
c
      FUNCTION BPLNK(W,T)

      include 'precision.f'

C$NAME BPLNK
C$LINK EXPFN
C PLANK FUNCTION WITH RESPECT TO WAVELENGTH
C--- HISTORY
C 89. 8. 1  CREATED
C--- INPUT
C W      R      WAVELENGTH (MICRON)
C T      R      ABSOLUTE TEMPERATURE (K)
C--- OUTPUT
C BPLNK  RF     PLANK FUNCTION (W/M2/STR/MICRON)
      IF(W*T.LE.0.0) THEN
        BPLNK=0
       ELSE
        X=1.438786E4/W/T
        BPLNK=1.1911E8/W**5/(EXPFN(X)-1)
      ENDIF
      RETURN
      END
c
c pas dans rstar5b ------------------------------------------------------------------
c     
      FUNCTION  PLKAVG (WN1,WN2,T,ERR)

      include 'precision.f'

C$NAME PLKAVG
C$LINK CPCON
C COMPUTES PLANCK FUNCTION INTEGRATED BETWEEN TWO WAVENUMBERS
C--- HISTORY
C 90. 1.26   ORIGINALLY DEVELOPED BY STAMNES AND TSAY.
C            EDITTED BY NAKAJIMA
C    12. 1   CHANGE IF X.EQ.0 STATEMENTS.
C--- INPUT
C WN1     R      LOWER WAVENUMBER ( INV CM ) OF SPECTRAL INTERVAL
C WN2     R      UPPER WAVENUMBER
C T       R      TEMPERATURE (K)
C--- OUTPUT
C PLKAVG  RF     INTEGRATED PLANCK FUNCTION ( WATTS/SQ M )
C                           = INTEGRAL (WN1 TO WN2) OF
C                              2H C**2  NU**3 / ( EXP(HC NU/KT) - 1)
C                              (WHERE H=PLANCKS CONSTANT, C=SPEED OF
C                              LIGHT, NU=WAVENUMBER, T=TEMPERATURE,
C                              AND K = BOLTZMANN CONSTANT)
C ERR     C*64   ERROR INDICATOR
C--- NOTES
C  REFERENCE : SPECIFICATIONS OF THE PHYSICAL WORLD: NEW VALUE
C                 OF THE FUNDAMENTAL CONSTANTS, DIMENSIONS/N.B.S.,
C                 JAN. 1974
C  METHOD :  FOR  -WN1-  CLOSE TO  -WN2-, A SIMPSON-RULE
C            QUADRATURE IS DONE TO AVOID ILL-CONDITIONING; OTHERWISE
C            (1)  FOR WAVENUMBER (WN1 OR WN2) SMALL,
C                 INTEGRAL(0 TO WNUM) IS CALCULATED BY EXPANDING
C                 THE INTEGRAND IN A POWER SERIES AND INTEGRATING
C                 TERM BY TERM;
C            (2)  OTHERWISE, INTEGRAL(WN1/HI TO INFINITY) IS
C                 CALCULATED BY EXPANDING THE DENOMINATOR OF THE
C                 INTEGRAND IN POWERS OF THE EXPONENTIAL AND
C                 INTEGRATING TERM BY TERM.
C  ACCURACY :  AT LEAST 6 SIGNIFICANT DIGITS, ASSUMING THE
C              PHYSICAL CONSTANTS ARE INFINITELY ACCURATE
C  ERRORS WHICH ARE NOT TRAPPED:
C      * POWER OR EXPONENTIAL SERIES MAY UNDERFLOW, GIVING NO
C        SIGNIFICANT DIGITS.  THIS MAY OR MAY NOT BE OF CONCERN,
C        DEPENDING ON THE APPLICATION.
C      * SIMPSON-RULE SPECIAL CASE IS SKIPPED WHEN DENOMINATOR OF
C        INTEGRAND WILL CAUSE OVERFLOW.  IN THAT CASE THE NORMAL
C        PROCEDURE IS USED, WHICH MAY BE INACCURATE IF THE
C        WAVENUMBER LIMITS (WN1, WN2) ARE CLOSE TOGETHER.
C--- LOCAL VARIABLES
C A1,2,...         POWER SERIES COEFFICIENTS
C C2               H * C / K, IN UNITS CM*K (H = PLANCKS CONSTANT,
C                      C = SPEED OF LIGHT, K = BOLTZMANN CONSTANT)
C D(I)             EXPONENTIAL SERIES EXPANSION OF INTEGRAL OF
C                       PLANCK FUNCTION FROM WN1 (I=1) OR WN2
C                       (I=2) TO INFINITY
C EPSIL            SMALLEST NUMBER SUCH THAT 1+EPSIL .GT. 1 ON
C                       COMPUTER
C EX               EXP( - V(I) )
C EXM              EX**M
C MMAX             NO. OF TERMS TO TAKE IN EXPONENTIAL SERIES
C AMV              MULTIPLES OF 'V(I)'
C P(I)             POWER SERIES EXPANSION OF INTEGRAL OF
C                       PLANCK FUNCTION FROM ZERO TO WN1 (I=1) OR
C                       WN2 (I=2)
C SIGMA            STEFAN-BOLTZMANN CONSTANT (W/M**2/K**4)
C SIGDPI           SIGMA / PI
C ISMLLV           NUMBER OF TIMES THE POWER SERIES IS USED (0,1,2)
C V(I)             C2 * (WN1(I=1) OR WN2(I=2)) / TEMPERATURE
C VCUT             POWER-SERIES CUTOFF POINT
C VCP              EXPONENTIAL SERIES CUTOFF POINTS
C VMAX             LARGEST ALLOWABLE ARGUMENT OF 'EXP' FUNCTION
C
      SAVE     PI, CONC, VMAX, EPSIL, SIGDPI
      PARAMETER  ( A1 =1.0/3.0,    A2 =-1.0/8.0,     A3 =1.0/60.0
     &            ,A4 =-1.0/5040.0,A5 = 1.0/272160.0
     &            ,A6 = -1.0/13305600.0)
      CHARACTER ERR*(*)
      DIMENSION D(2), P(2),V(2), VCP(7), CCP(3)
      DATA     C2 / 1.438786 /,  SIGMA / 5.67032E-8 /,
     &         VCUT / 1.5 /, VCP / 10.25, 5.7, 3.9, 2.9, 2.3, 1.9, 0.0 /
      DATA     PI / 0.0 /
      F(X) = X**3 / ( EXP(X) - 1 )
C
      ERR=' '
      IF ( PI .LE. 0.0 )  THEN
         PI = 2 * ASIN( 1.0 )
         CALL CPCON(CCP)
         VMAX = CCP(3)*0.7*2.3
         EPSIL = CCP(1)*10
         SIGDPI = SIGMA / PI
         CONC = 15/ PI**4
      END IF
      IF( T .LT. 0.0 .OR. WN2 .LE. WN1 .OR. WN1 .LT. 0.0) THEN
        ERR='F  PLKAVG--TEMPERATURE OR WAVENUMS. WRONG'
        RETURN
      ENDIF
      IF ( T .LT. 1.0E-4 )  THEN
         PLKAVG = 0
         RETURN
      ENDIF
      V(1) = C2 * WN1 / T
      V(2) = C2 * WN2 / T
      IF ( V(1) .GT. EPSIL .AND. V(2) .LT. VMAX .AND.
     &     (WN2-WN1)/WN2 .LT. 1.0E-2 )  THEN
C                          ** WAVENUMBERS ARE VERY CLOSE.  GET INTEGRAL
C                          ** BY ITERATING SIMPSON RULE TO CONVERGENCE.
         HH = V(2) - V(1)
         OLDVAL = 0
         VAL0 = F( V(1) ) + F( V(2) )
         DO  2  N = 1, 10
            DEL = HH / (2*N)
            VAL = VAL0
            DO  1  K = 1, 2*N-1
               VAL = VAL + 2*(1+MOD(K,2)) * F( V(1) + K*DEL )
    1       CONTINUE
            VAL = DEL/3*VAL
            IF ( ABS( (VAL-OLDVAL)/VAL ) .LE. 1.0E-6 )  GO TO 3
            OLDVAL = VAL
    2    CONTINUE
         ERR= 'W  PLKAVG--SIMPSON RULE DIDNT CONVERGE'
    3    PLKAVG = SIGDPI * T**4 * CONC * VAL
         RETURN
      END IF
      ISMLLV = 0
      DO  50  I = 1, 2
         IF( V(I).LT.VCUT )  THEN
C                                   ** USE POWER SERIES
            ISMLLV = ISMLLV + 1
            VSQ = V(I)**2
            P(I) =  CONC * VSQ * V(I) * ( A1 + V(I) * ( A2 + V(I) *
     &                ( A3 + VSQ * ( A4 + VSQ * ( A5 + VSQ*A6 )))))
         ELSE
C                    ** USE EXPONENTIAL SERIES
            MMAX = 0
C                                ** FIND UPPER LIMIT OF SERIES
   20       MMAX = MMAX + 1
               IF ( V(I).LT.VCP( MMAX ) )  GO TO 20
            EX = EXP( - V(I) )
            EXM = 1
            D(I) = 0
            DO  30  M = 1, MMAX
               AMV = M * V(I)
               EXM = EX * EXM
               D(I) = D(I) +
     &                EXM*(6+AMV*(6+AMV*(3+AMV)))/M**4
   30       CONTINUE
            D(I) = CONC * D(I)
         END IF
   50 CONTINUE
      IF ( ISMLLV .EQ. 2 ) THEN
C                                    ** WN1 AND WN2 BOTH SMALL
         PLKAVG = P(2) - P(1)
      ELSE IF ( ISMLLV .EQ. 1 ) THEN
C                                    ** WN1 SMALL, WN2 LARGE
         PLKAVG = 1. - P(1) - D(2)
      ELSE
C                                    ** WN1 AND WN2 BOTH LARGE
         PLKAVG = D(1) - D(2)
      END IF
      PLKAVG = SIGDPI * T**4 * PLKAVG
      IF( PLKAVG.LE.0.0 )
     &  ERR='W  PLKAVG--RETURNS ZERO; POSSIBLE UNDERFLOW'
      RETURN
      END
c
c pas dans rstar5b ------------------------------------------------------------------
c
      SUBROUTINE PLYFT2(M1,N,X,Y,A,KM1,WK1,WK2,ERR)

      include 'precision.f'

C$NAME  PLYFT2
C$LINK  TNVCH2
C FITTING BY POLYNOMIALS
C--- HISTORY
C 88. 2.25 CREATED
C--- INPUT
C M1     I      ORDER OF POLYNOMIALS + 1.
C N      I      NBR OF DATA.
C X    R(N)     DATA-X.
C Y    R(N)     DATA-Y.
C KM1    I      FIRST DIMENSION SIZE FOR WK2.
C WK1  R(M1)    WORKING AREA.
C WK2 R(KM1,M1) WORKING AREA.
C--- OUTPUT
C A    R(M1)    Y = SUM(I=1,M1) ( A(I)*X**(I-1)).
C ERR   C*64    ERROR INDICATOR.
C$ENDI
C
      CHARACTER ERR*(*)
      DIMENSION X(N),Y(N),A(M1),WK1(M1),WK2(KM1,M1)
C
      DO 1 I=1,M1
      DO 1 J=1,I
      S=0
      KK=I+J-2
      IF(KK .LE. 0) THEN
        DO 7 K=1,N
    7   S=S+1
       ELSE
        DO 2 K=1,N
    2   S=S+X(K)**KK
      ENDIF
      WK2(I,J)=S
    1 WK2(J,I)=S
      CALL TNVCH2(M1,WK2,DT,KM1,ERR)
      IF(ERR.NE.' ') RETURN
C
      DO 3 I=1,M1
      S=0
      KK=I-1
      IF(KK.EQ.0) THEN
        DO 8 J=1,N
    8   S=S+Y(J)
       ELSE
        DO 4 J=1,N
    4   S=S+Y(J)*X(J)**KK
      ENDIF
    3 WK1(I)=S
      DO 5 I=1,M1
      S=0
      DO 6 J=1,M1
    6 S=S+WK2(I,J)*WK1(J)
    5 A(I)=S
C
      RETURN
      END
c
c pas dans rstar5b ------------------------------------------------------------------
c
      SUBROUTINE      TNVCH2(N,A,DT,NN,ERR)

      include 'precision.f'

C$NAME  TNVCH2
C INVERSION OF SYMMETRIC POSITIVE DEFINITE MATRIX
C     SQUARE ROOT METHOD
C     MATRIX A IS SYMMETRIC AND POSITIVE DEFINITE
C--- HISTORY
C 71. 4.30 CREATED BY SAKATA MASATO
C 89.11.10 ADDED ERR.
C 90. 1.17  REGISTERED
C--- INPUT
C N        I        DIMENSION OF THE MATRIX
C A      R(NN,N)    MATRIX
C NN       I        SIZE OF THE FIRST ARGUMENT OF A
C--- OUTPUT
C A                 INVERSE OF THE MATRIX
C DT       R        DETERMINATION OF THE MATRIX
C INDER    I        0: NORMAL TERMINATION
C$ENDI
      CHARACTER ERR*(*)
      DIMENSION    A(NN,N)
      ERR=' '
c      IF(N-1)   1,2,3
      IF((N-1).lt.0) goto 1
      IF((N-1).eq.0) goto 2
      IF((N-1).gt.0) goto 3

    1 ERR='ERROR IN TINVCH: N .LE.0'
      RETURN
    2 DT=A(1,1)
      A(1,1)=1.0/A(1,1)
      IF(DT.LE.0.0)   GO TO  60
      RETURN
    4 ERR='ERROR IN TINVCH: N.GT.NN'
      RETURN
    3 IF(N.GT.NN)    GO TO  4
      DT=A(1,1)
      IF(DT.LE.0.0)   GO TO  60
      A(1,1)=SQRT(DT)
      DO  5  J=2,N
    5 A(1,J)=A(1,J)/A(1,1)
      DO  10  K=2,N
      Z=A(K,K)
      K1=K-1
      DO  11   J=1,K1
   11 Z=Z-A(J,K)*A(J,K)
      IF(Z.LE.0.0)   GO TO  60
      DT=DT*Z
      A(K,K)=SQRT(Z)
      IF(K.EQ.N) GO TO 10
      Y=1.0/A(K,K)
      J1=K+1
      DO 13   J=J1,N
      Z=A(K,J)
      DO  14  I=1,K1
   14 Z=Z-A(I,K)*A(I,J)
   13 A(K,J)=Z*Y
   10 CONTINUE
      A(N,N)=1.0/A(N,N)
      DO   40   IA=2,N
      I=N-IA+1
      Y=1.0/A(I,I)
      A(I,I)=Y
      I1=I+1
      DO  41    JA=I1,N
      J=N-JA+I1
      Z=0.0
      DO  42    K=I1,J
   42 Z=Z-A(I,K)*A(K,J)
   41 A(I,J)=Z*Y
   40 CONTINUE
      DO  50  J=1,N
      DO  51   I=J,N
      Z=0.0
      DO  52   K=I,N
   52 Z=Z+A(I,K)*A(J,K)
   51 A(I,J)=Z
      DO  53  I=1,J
   53 A(I,J)=A(J,I)
   50 CONTINUE
      RETURN
   60 ERR='ERROR IN TINVCH: GIVEN MATRIX IS NOT POSITIVE DEFINITE'
      RETURN
      END
C 
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++++++++++++++ Subroutines  et fonctions +++++++++++++++++++++++++++
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
c      
c--------------------------------------------------------------------------------------
c
      SUBROUTINE fnct_source_PS(NDA,I,IT,W,PT1,PR1,AJM,AI,WMM,WA,
     &             NPLK1,CPLK,M,L,
     &FSOL,PT10,PR10,UTAU,AM0)

      include 'precision.f'
      
c *** Version originale de Pierre Simoneau
      
c
c       Sous programme de calcul de la fonction source Jm a une epaisseur optique TAU.
c       
c          Soit l'equation de TR : mu*(du/dtau) = u - J
c                                         _2*PI  _ +1
c                                        |      |
c                                        |      |
c             avec J(tau,mu,phi) = omega*| dphi'| dmu'u(tau,mu',phi')*p(tau,mu,phi,mu',phi')
c                                        |      |
c                                       _|0    _|-1
c
c                                + omega*F0*P(mu,mU0)*exp(-tau/mu0) + (1-omega)*B(T)
c
c      Apres decomposition en serie de Fourier, on a l'equation :
c
c                                       __
c                                       \   
c                  Jm(tau,mui) = omega* <_ (P(mui,muj)*u(muj)*wj + P(mui,-muj)*u(-muj)*wj)
c                                       J
c                                               + Qs + Qth
c                                       
c                   avec u( muj) = u+ = A+(tau)*alfa+B+(tau)*beta + V+*E0(tau)
c                   avec u(-muj) = u- = A-(tau)*alfa+B-(tau)*beta + V-*E0(tau)
c 
c                            A+ = [Q*C(tau) - Q~*LS(tau)]/sqrt(WM)
c                            A- = [Q*C(tau) + Q~*LS(tau)]/sqrt(WM)
c                            B+ = [Q*(L**-1)*S(tau) - Q~*C(tau)]/sqrt(WM)
c                            B- = [Q*(L**-1)*S(tau) + Q~*C(tau)]/sqrt(WM)
c                            V+ = [Q*gamma + Q~*gamma/M0 + (X**-1) sigma-]/2/sqrt(WM)
c                            V- = [Q*gamma - Q~*gamma/M0 - (X**-1) sigma-]/2/sqrt(WM)
c
c ************************************************************************************
c             PT1 = P(mu1u,mua) ; PR1 = P(-mu1u,mua) 
c
c       on utilise les relations : P( mui,-muj) = P(-mui,muj)
c
c       On obtient en sortie :
c                              J(-mua,tau) = AJM(    I,IT)
c                              J( mua,tau) = AJM(NDA+I,IT)
c
c
C--- INPUT
C M       I         FOURIER ORDER
C L       I         LAYER NUMBER
C NDA
C NA0
C FSOL
C I       I         STREAM NUMBER FOR AM1(I)
C AM1U    R         USER DEFINED DIRECTION
C AM0   R(KNA0)
C W       R         SINGLE SCATTERING ALBEDO
 
      include 'parameter.rayjn'
C
c     PARAMETER (KNDM=100,KNA1U=80,KNA1=40,KNA0=2,KNLN=3,KPLK1=4)
C
      PARAMETER (PI=3.141592654)
      DIMENSION CPLK(KPLK1,KNLN),WMM(KNDM),
     & PT1(KNA1,KNDM),PR1(KNA1,KNDM),THK(KNLN)
      DIMENSION AI(KNA1U,KNA0,KNTAU),AJM(KNA1U,KNTAU),wa(kndm)                             

      DIMENSION AM0(KNA0),
     &UTAU(KNTAU)
C WORKING AREAS
      DIMENSION PR10(KNA1,KNA0),PT10(KNA1,KNA0)


c==> indice angle solaire Jsol (si Jsol > 1 alors debut de boucle
c    jusqu'a la fin du calcul de AJM)
      Jsol = 1

      AJdm1=0
      AJdm2=0
      DO J=1,NDA
         KP = 2*NDA + 1 - J
         Pplus = PT1(I,J)/WMM(J)
         Pmoin = PR1(I,J)/WMM(J)
         AJdm1 = AJdm1 +
     &         WA(J)*(Pplus*AI(KP,Jsol,IT)+Pmoin*AI(J,Jsol,IT))
         AJdm2 = AJdm2 +  
     &         WA(J)*(Pmoin*AI(KP,Jsol,IT)+Pplus*AI(J,Jsol,IT))
      enddo
      AJdm1 = w*AJdm1
      AJdm2 = w*AJdm2
    

c==> TRNS0 = exp(-tau/mu0)
c     TRNS0=-UTAU(IT)/AM0(Jsol)
c     TRNS0=EXPFN(TRNS0)

c==> source thermique
c    Fonction de Planck sous la forme d'un polynome en tau : 
c    Qth(tau) = CPLK(1) + CPLK(2)*tau + CPLK(3)*tau**2 + ...
c    !!! � l'interface tau = 0 ou tau = 1 : premi�re couche tau=0 et les autres tau=1
c     if (it.eq.1) then
c        tau = 0.
c     else
c        tau = utau(it) - utau(it-1)
c     endif
c     Qth = 0.
c     IF (M.EQ.0 .AND. NPLK1.GT.0) THEN
c        DO J=1,NPLK1
c           dpt = tau**(j-1)
c           Qth = Qth + 2*PI*(1-W)*CPLK(J,L)*dpt        
c        enddo
c     ENDIF

C DOWNWARD INTENSITY

c==> calcul du terme source diffusion simple solaire
c    Qs = P(mu,mu0)*F0*exp(-tau/mu0)
c     Qs=W*FSOL*PT10(I,Jsol)*TRNS0 
c     Diffusion multiple seule ==> Qs = 0.   Qth = 0.

c     AJM(2*NDA+1-I,IT) = AJdm1 + Qs + Qth
      AJM(2*NDA+1-I,IT) = AJdm1

C UPWARD INTENSITY

c ==> calcul du terme source diffusion simple solaire
c     Qs = P(-mu,mu0)*F0*exp(-tau/mu0)
c     Qs=W*FSOL*PR10(I,Jsol)*TRNS0 
c     Diffusion multiple seule ==> Qs = 0.   Qth = 0.

c     AJM(I,IT) = AJdm2 + Qs + Qth
      AJM(I,IT) = AJdm2

c==> fin de boucle sur l'angle solaire                   

      RETURN
      END
c      
c--------------------------------------------------------------------------------------
c
      SUBROUTINE fnct_source(NA1U,NDA,I,IT,W,PT1,PR1,AJM,AI,WMM,WA,
     &                       AM1U,INDA)
      
      include 'precision.f'
 
c       Sous programme de calcul de la fonction source Jm a une epaisseur optique TAU.
c       
c          Soit l'equation de TR : mu*(du/dtau) = u - J
c                                         _2*PI  _ +1
c                                        |      |
c                                        |      |
c             avec J(tau,mu,phi) = omega*| dphi'| dmu'u(tau,mu',phi')*p(tau,mu,phi,mu',phi')
c                                        |      |
c                                       _|0    _|-1
c
c
c      Apres decomposition en serie de Fourier, on a l'equation :
c
c                                       __
c                                       \   
c                  Jm(tau,mui) = omega* <_ (P(mui,muj)*u(muj)*wj + P(mui,-muj)*u(-muj)*wj)
c                                       J
c                                               
c                                       
c                   avec u( muj) = u+ = A+(tau)*alfa+B+(tau)*beta + V+*E0(tau)
c                   avec u(-muj) = u- = A-(tau)*alfa+B-(tau)*beta + V-*E0(tau)
c 
c                            A+ = [Q*C(tau) - Q~*LS(tau)]/sqrt(WM)
c                            A- = [Q*C(tau) + Q~*LS(tau)]/sqrt(WM)
c                            B+ = [Q*(L**-1)*S(tau) - Q~*C(tau)]/sqrt(WM)
c                            B- = [Q*(L**-1)*S(tau) + Q~*C(tau)]/sqrt(WM)
c                            V+ = [Q*gamma + Q~*gamma/M0 + (X**-1) sigma-]/2/sqrt(WM)
c                            V- = [Q*gamma - Q~*gamma/M0 - (X**-1) sigma-]/2/sqrt(WM)
c
c ************************************************************************************
c             PT1 = P(mu1u,mua) ; PR1 = P(-mu1u,mua) 
c
c       on utilise les relations : P( mui,-muj) = P(-mui,muj)
c
c       On obtient en sortie :
c                              J(-mua,tau) = AJM(    I,IT)
c                              J( mua,tau) = AJM(NDA+I,IT) ???
c                              plut�t      = AJM(2*NDA+1-I,IT)
c
c
C--- INPUT
C NDA
C I       I         STREAM NUMBER FOR AM1(I)
C W       R         SINGLE SCATTERING ALBEDO
 
      include 'parameter.rayjn'

      PARAMETER (PI=3.141592654)
      DIMENSION WMM(KNDM),
     & PT1(KNA1,KNDM),PR1(KNA1,KNDM),AM1U(KNA1U)
      DIMENSION AI(KNA1U,KNA0,KNTAU),AJM(KNA1U,KNTAU),wa(kndm)                             


c==> indice angle solaire Jsol 
      Jsol = 1

      AJdm1=0
      AJdm2=0
      DO J=1,NDA
         KP = 2*NDA + 1 - J
         Pplus = PT1(I,J)/WMM(J)
         Pmoin = PR1(I,J)/WMM(J)
         AJdm1 = AJdm1 +
     &         WA(J)*(Pplus*AI(KP,Jsol,IT)+Pmoin*AI(J,Jsol,IT))
         AJdm2 = AJdm2 +  
     &         WA(J)*(Pmoin*AI(KP,Jsol,IT)+Pplus*AI(J,Jsol,IT))
      enddo
      AJdm1 = w*AJdm1
      AJdm2 = w*AJdm2

      if (INDA.EQ.1) then
         if (am1u(i).gt.0) then
C           DOWNWARD INTENSITY
            AJM(I,IT) = AJdm1                
         else
C          UPWARD INTENSITY
           AJM(I,IT) = AJdm2                
         endif
      else
         AJM(NA1U+1-I,IT) = AJdm1
         AJM(I,IT) = AJdm2                
      endif

      RETURN
      END
