#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 09:12:53 2022

@author: vaubaill

Compute the S/N for a given meteorite in wind tunnel, as observed as real meteor.
"""
import os
import glob
import argparse
import itertools
import numpy as np
from astropy.constants import h,c
import astropy.units as u
from astropy.table import QTable, vstack
from astropy.time import Time
from astropy.modeling.physical_models import BlackBody
from astropy.coordinates import EarthLocation, AltAz, SkyCoord, get_sun
import matplotlib.pyplot as plt

from rayjn_log import log
from read_rayjn import read_rayjn_comp,read_rayjn_lum,plot_lum,plot_comp
#from Read_file_spectrometer_ONERA_METEOR_JV import read_spectrometer_data

# ============= USER input =============
# path
home = os.getenv('HOME')+'/'

# ========== END OF USER INPUT =========

def get_s2n_file_name(rayjn_dir,rad,T):
    """
    
    Parameters
    ----------
    rayjn_dir : string
       Rayjn simulation output directory. 
    rad : astropy.units.Quantity object
        meteoroid radius.
    T : astropy.units.Quantity object
        meteoroid temperature.
    
    Returns
    -------
    data_file_name : string
        output data file name.
    meta_file_name : string
        output meta-data file name.
        
    """
    file_root = rayjn_dir + 'XXX_r'+f"{rad.to('m').value:.0E}"+'_T'+\
                                    f"{T.to('K').value:.0f}"
    data_file_name = file_root.replace('XXX','s2n_pix')+'.dat'
    meta_file_name = file_root.replace('XXX','meta')+'.dat'
    return data_file_name,meta_file_name


def get_s2n_all_file_name(rayjn_dir_root):
    """
    Get all s/n data file name

    Parameters
    ----------
    rayjn_dir_root : string
        All Rajn output data directory.

    Returns
    -------
    s2n_all_file_name : string
        File name of all data.

    """
    s2n_all_file_name = rayjn_dir_root + 's2n_all.dat'
    return s2n_all_file_name

def compute_s2n(rayjn_dir,wavelength,meteoroid,camera,lens,
                lum_file='lum_Rayjn.xls',com_file='comp_Rayjn.xls',skip=False):
    """Compute S/N of a meteor casued by a meteoroid, given sky background.
    
    Sky background is estimated using the Rayjn software, developed by ONERA.

    Parameters
    ----------
    rayjn_dir : string
        Rayjn simulation output directory.
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    meteoroid : dict
        Meteoroid parameters:
            rad: astropy.units.Quantity object
                meteoroid radius.
            T: astropy.units.Quantity object
                meteoroid surface temperature.
            alt: astropy.units.Quantity object.
                Meteoroid altitude above sea level.
    camera : dict
        camera parameters.
            x: float:
                sensor number of pixels in horizontal direction.
            y: float
                sensor number of pixels in vertical direction.
            sz: astropy.units.Quantity object
                Square pixel size, in [m].
            bit: int
                Number of bits.
            QE_file: string
                File including QE as a function of wavelength in csv format.
    lens : dict
        Lens parameters:
            F: astropy.units.Quantity object
                Focal lens.
            N: float
                Lens aperture.
    lum_file : string, optional
        Rayjn luminance data file. Default is 'lum_Rayjn.xls'.
    com_file : string, optional
        Rayjn transmission data file. Default is 'comp_Rayjn.xls'.
    skip : bollean, optional
        If True and output file already exist, computation is skipped. Default is False.
    
    Returns
    -------
    None. Data and graphs are saved in the rayjn_dir directory.

    """  
    # store data
    # meteoroid size and surface temperature
    met_rad = meteoroid['rad']
    # meteoroid surface Temperature
    met_T = meteoroid['T']
    # camera features
    # cam_x = camera['x']
    # cam_y = camera['y']
    cam_sz = camera['sz']
    # cam_bit = camera['bit']
    cam_QE_file = camera['QE_file']
    # lens features
    lens_F = lens['F']
    lens_N = lens['N']
    log.info('rayjn_dir: '+rayjn_dir)
    # rename files
    if lum_file=='lum_Rayjn.xls':
        lum_file = rayjn_dir+'lum_Rayjn.xls'
    if com_file=='comp_Rayjn.xls':
        com_file = rayjn_dir+'comp_Rayjn.xls'
    
    # initial check list
    if not os.path.isdir(rayjn_dir):
        msg = '*** FATAL ERROR: directory '+rayjn_dir+' does not exist'
        log.error(msg)
        raise IOError(msg)
    for f in [lum_file,com_file,cam_QE_file]:
        if not os.path.isfile(f):
            msg = '*** FATAL ERROR: file '+f+' does not exist'
            log.error(msg)
            raise IOError(msg)
    
    # create output file root name
    data_file_name,meta_file_name = get_s2n_file_name(rayjn_dir,met_rad,met_T)
    
    # skip of files already exist
    if (skip  and os.path.isfile(data_file_name) and os.path.isfile(meta_file_name)):
        log.warning('Files already exist: skipping')
        return
    
    #str(met_rad.to('m').value)+'-'+\
    #str(met_T.to('K').value)
    
    # read luminance in [ph.m-2.s-1.sr-1.um-1]
    wvlth,sky_lum = read_rayjn_lum(lum_file)
    plot_lum(lum_file,logplot=True,watt=True)
    # read transmission, no unit
    wvlth,sky_tra,tmp = read_rayjn_comp(com_file)
    plot_comp(com_file)
    # get camera altitude and elevation above horizon from rayjn_dir name
    alt = float(rayjn_dir.split('ALT')[1].split('AZ')[0])*u.km
    elv = float(rayjn_dir.split('EL')[1].split('/')[0])*u.deg
    # Vertical distance between the camera and top of the sky
    dvert = meteoroid['alt']-alt
    # compute distance between camera and meteor
    dist = dvert / np.sin(elv.to('rad'))
    #log.info('dist: '+str(dist.to('km')))
    # lens diameter
    lens_D = lens_F / lens_N
    # lens collecting area
    lens_S = lens_D**2 * np.pi
    # read camera features
    cam_QE = QTable.read(cam_QE_file,format='ascii.csv',delimiter=';')
    cam_QE = QTable([cam_QE['lbd(µm)'],
                     cam_QE['Reponse_Spectrale_energetique']/np.max(cam_QE['Reponse_Spectrale_energetique'])],
                    names=('lbd','QE'),
                    units=['um',''])
    # sort as increasing wavelength: needed for the interpolation process
    cam_QE.sort('lbd')
    # interpolate QE at sky_lum  (from comp_Rayjn.txt) wavelengths
    cam_QE = np.interp(wvlth.to('um'), cam_QE['lbd'].to('um'), cam_QE['QE'])

    # === compute meteoroid emission ===
    # meteoroid apparent surface
    met_sfc = met_rad**2
    # compute Black body radiation
    met_bb = BlackBody(met_T)
    # compute Planck's law for wavelength range, in frequency unit
    met_bb_nu = met_bb(wvlth).to('J.m-2.Hz-1.s-1.sr-1')
    # spectral range in frequency [Hz]
    nu_rng = (c / wvlth.to('m')).to('Hz')
    # convert Planck's law to wavelength i.e. in ['J.s-1.sr-1.m-2.um-1']
    met_bb_lbd = (met_bb_nu * nu_rng**2 / c).to('J.s-1.sr-1.m-2.um-1')
    # compute photon energy
    energy = h*c / wvlth.to('m')
    # compute meteoroid surface emission, in [ph.s-1.sr-1.m-2.um-1]
    met_bb_ph = met_bb_lbd / energy * u.ph
    # compute meteoroid surface emission, in [ph.s-1.sr-1.um-1]
    met_bb_sfc = met_bb_ph * met_sfc.to('m2')
    # compute number of meteoroid photon at camera entrance
    met_bb_dst = met_bb_sfc * sky_tra  / dist.to('m').value**2
    # compute meteoroid  total emission (i.e. in 4xpi sterad), in [ph.m-2.s-1.um-1] ; NOT used later
    met_bb_pix = met_bb_dst * 4*np.pi*u.sr
    
    
    # === compute sky emission ===
    # solid angle viewed by one pixel, in [sr]
    pix_ang = (2*np.arctan(cam_sz.to('m') / (2*lens_F.to('m')))**2).to('sr')
    # sky luminance in pixel solid angle
    # sky_lum_pix = sky_lum * pix_ang * lens_S.to('m2')

    
    # compute spectric S/N at pixel level : 
    # meteoroid_surface / distance^2 * lens_F / pixel_size^2  * meteoroid surface emission * transmission / sky_luminance
    s2n_pix = met_sfc.to('m2') / dist.to('m')**2 * (lens_F.to('m') / cam_sz.to('m'))**2 * met_bb_ph * sky_tra / sky_lum
    
    # save data: create QTable
    s2n_pix_data = QTable([wvlth.to('um'),s2n_pix],names=('wavelength','s2n_pix'))
    # write data
    s2n_pix_data.write(data_file_name,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('s2n_pix saved in '+data_file_name)
    
    # Meta-data
    # determine max s2n_pix value in the range [3;5] um
    mask = (s2n_pix_data['wavelength']>wavelength['lambda_min']) & (s2n_pix_data['wavelength']<wavelength['lambda_max'])
    max_s2n_mir = np.max(s2n_pix_data['s2n_pix'][mask])
    med_s2n_mir = np.median(s2n_pix_data['s2n_pix'][mask])
    # save meta-data
    meta = QTable([[dist],[max_s2n_mir],[med_s2n_mir]],names=('Dist','s/n_max','s/n_med'))
    #create metadata output file name
    meta.write(meta_file_name,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('metadata saved in '+meta_file_name)
    
    # Note : considerer le temps pendant lequel le meteor reste dans le pixel et comparer avec le temps de pose
    # => considérer le mouvement apparent du météore
    # Note : Sylvain va demander me S/N courant pour la détection
    # Note : 
    
    # create plots
    # plot QE=f(wavelength)
    # plotfile = 'QE.png'
    # fig=plt.figure()
    # plt.scatter(wvlth.to('um'),cam_QE,color='cornflowerblue',s=1,marker='P')
    # plt.title('QE vs lambda',color='indianred')
    # plt.xlabel('Lambda [um]')
    # plt.ylabel('QE')
    # plt.legend(loc='lower right')
    # plt.savefig(plotfile,dpi=300)
    # plt.close(fig)
    # log.info('Fig saved in '+plotfile)
    # plot s2n at pixel level
    plotfile = data_file_name.replace('.dat','.png')
    fig=plt.figure()
    plt.scatter(wvlth.to('um'),s2n_pix,color='cornflowerblue',s=1,marker='P')
    plt.ylim([0, max_s2n_mir])
    plt.title('S/N at pixel vs lambda',color='indianred')
    plt.xlabel('Lambda [um]')
    plt.ylabel('S/N')
    plt.legend(loc='lower right')
    plt.savefig(plotfile,dpi=300)
    plt.close(fig)
    log.info('Fig saved in '+plotfile)
        
    return 


def rayjn_dir_name2param(rayjn_dir_name):
    """
    
    Parameters
    ----------
    rayjn_dir_name : string
        Rayjn simulation output directory name.

    Returns
    -------
    date : astropy.time.Time object
        Date of simulation.
    alt : astropy.units.Quantity object
        Altitude of the camera.
    az : astropy.units.Quantity object
        Camera pointing azimuth (from North).
    el : astropy.units.Quantity object
        Campera pointing elevation (from horizon).

    """
    subd = rayjn_dir_name.split('/')[-2]
    timestr,params = subd.split('_UT')
    timeisot = timestr[:4]+'-'+timestr[4:6]+'-'+timestr[6:8]+'T'+\
               timestr[9:11]+':'+timestr[11:13]+':'+timestr[13:15]
    date = Time(timeisot)
    alt = float(params.split('-ALT')[-1].split('AZ')[0]) *u.km
    az,el = params.split('AZ')[-1].split('EL')
    az = float(az) * u.deg
    el = float(el) * u.deg
    
    return date,alt,az,el

def launch_s2n(rayjn_dir_root,wavelength,meteoroid,camera,lens,skip=False):
    """
    Launch all computations of S/N for all simulated cases.

    Parameters
    ----------
    rayjn_dir_root : string
        Rayjn simulation root directory.
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    meteoroid : dict
        Meteoroid parameters:
            rad: astropy.units.Quantity object
                meteoroid radius.
            T: astropy.units.Quantity object
                meteoroid surface temperature.
            eps: float
                Meteoroid emissivity (dimensionless).
            alt: astropy.units.Quantity object
                Meteoroid altitude above sea level, in [km].
    camera : dict
        camera parameters.
            x: float:
                sensor number of pixels in horizontal direction.
            y: float
                sensor number of pixels in vertical direction.
            sz: astropy.units.Quantity object
                Square pixel size, in [m].
            bit: int
                Number of bits.
            QE_file: string
                File including QE as a function of wavelength in csv format.
    lens : dict
        Lens parameters:
            F: astropy.units.Quantity object
                Focal lens.
            N: float
                Lens aperture.
    skip : bollean, optional
        If True and output file already exist, computation is skipped. Default is False.

    Returns
    -------
    None.

    """     
    # list all rayjn directories
    list_rayjn = glob.glob(rayjn_dir_root+'*UT-ALT*')
    list_rayjn.sort(reverse=True)
        
    log.info('Nb of directories to process: '+str(len(list_rayjn)))
    for rayjn_dir in list_rayjn:
        if not rayjn_dir.endswith('/'):
            rayjn_dir = rayjn_dir + '/'
        log.info('====== processing directory: '+rayjn_dir)
        # retrieve simulation parameters
        #date,alt,az,el = rayjn_dir_name2param(rayjn_dir)
        # compute max s/n
        compute_s2n(rayjn_dir,wavelength,meteoroid,camera,lens,skip=skip)
    log.info('done')    
    
    return

def get_Solar_dist(rayjn_dir):
    """
    Get Solar distance from pointing direction.

    Parameters
    ----------
    rayjn_dir : string
        Rayjn simulation root directory.

    Returns
    -------
    solar_dist : TYPE
        DESCRIPTION.

    """
    # get date and pointing direction
    date,alt,az,el = rayjn_dir_name2param(rayjn_dir)
    # read Rayjn parameters file to get location
    rayjn_conf = rayjn_dir + '/Rayjn.data'
    if not os.path.isfile(rayjn_conf):
        msg = '*** FATAL ERROR: Rayjn configuration data '+rayjn_conf+' does not exist'
        log.error(msg)
        raise IOError(msg)
    # read Rayjn config file
    log.info('Now reading Rayjn config file: '+rayjn_conf)
    with open(rayjn_conf, mode='r', encoding="latin-1") as fin:
        nline = 0
        for line in fin.readlines():
            nline = nline+1
            if nline==4:
                l = line.split('lat')[0]
                for i in range(10):
                    l = l.replace('  ',' ')
                lat,lon = l.split(' ')[:2]
                break
    # convert lon,lat
    lon = float(lon)*u.deg
    lat = float(lat)*u.deg
    loc = EarthLocation.from_geodetic(lon=lon,lat=lat,height=alt)
    # create altAz frame
    alataz_frame = AltAz(az=az,alt=el,obstime=date,location=loc)
    print(str(alataz_frame))
    # get Sun coordinats
    radec_sun = get_sun(date)
    # compute azimuth and elevation of the Sun
    azel_sun = radec_sun.transform_to(alataz_frame)
    print(str(azel_sun))
    # computa angular separation with the Sun
    solar_dist = alataz_frame.separation(azel_sun)
    return solar_dist

def get_max_s2n(rayjn_dir_root,wavelength,meteoroid,camera,lens):
    """
    Read metadata and get max S/N.

    Parameters
    ----------
    rayjn_dir_root : string
        Rayjn simulation root directory.
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    meteoroid : dict
        Meteoroid parameters:
            rad: astropy.units.Quantity object
                meteoroid radius.
            T: astropy.units.Quantity object
                meteoroid surface temperature.
            eps: float
                Meteoroid emissivity (dimensionless).
            alt: astropy.units.Quantity object
                Meteoroid altitude above sea level, in [km].
    camera : dict
        camera parameters.
            x: float:
                sensor number of pixels in horizontal direction.
            y: float
                sensor number of pixels in vertical direction.
            sz: astropy.units.Quantity object
                Square pixel size, in [m].
            bit: int
                Number of bits.
            QE_file: string
                File including QE as a function of wavelength in csv format.
    lens : dict
        Lens parameters:
            F: astropy.units.Quantity object
                Focal lens.
            N: float
                Lens aperture.

    Returns
    -------
    None.

    """
    # make output data file name
    all_data_file_name = get_s2n_all_file_name(rayjn_dir_root)
    # create output table
    data = QTable(names=('date','alt','az','el','dist','Sun_dist','met_r','met_T','pix_sz','F','max_s2n','med_s2n'),
                  dtype=['object','float','float','float','float','float','float','float','float','float','float','float'])
    
    # list all rayjn directories
    list_rayjn = glob.glob(rayjn_dir_root+'*UT-ALT*')
    list_rayjn.sort() 
    log.info('Nb of directories to process: '+str(len(list_rayjn)))
    # loop over all directories
    for rayjn_dir in list_rayjn:
        if not rayjn_dir.endswith('/'):
            rayjn_dir = rayjn_dir + '/'
        log.info('====== processing directory: '+rayjn_dir)
        # retrieve simulation parameters
        date,alt,az,el = rayjn_dir_name2param(rayjn_dir)
        # get data file names
        data_file_name,meta_file_name = get_s2n_file_name(rayjn_dir,meteoroid['rad'],meteoroid['T'])
        # get Solar distance from pointing direction
        solar_dist = get_Solar_dist(rayjn_dir) 
        # get max s/n
        log.info('Read: '+meta_file_name)
        meta_data = QTable.read(meta_file_name,format='ascii.fixed_width_two_line')
        dist,max_s2n_mir,med_s2n_mir = meta_data[0]
        log.info('max_s2n_mir: '+str(max_s2n_mir))
        log.info('dist: '+str(dist))
        # store data
        data.add_row([date,alt,az,el,dist,solar_dist,meteoroid['rad'],meteoroid['T'],camera['sz'],lens['F'],max_s2n_mir,med_s2n_mir])
    log.info('done')    
    
    # sort data
    data.sort(('date','alt','az','el'))
    # set formats
    data['alt'].info.format = '.0f'
    data['az'].info.format = '.0f'
    data['el'].info.format = '.0f'
    data['dist'].info.format = '.0f'
    data['Sun_dist'].info.format = '.0f'
    data['met_r'].info.format = '.0f'
    data['met_T'].info.format = '.0f'
    data['pix_sz'].info.format = '.0f'
    data['F'].info.format = '.0f'
    data['max_s2n'].info.format = '.1f'
    data['med_s2n'].info.format = '.1f'
    # save data
    data.write(all_data_file_name,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('Data save in '+all_data_file_name)
    return

def plot_data(rayjn_dir_root,file_pat='max_s2n_*.png'):
    """Plot all S/N data.

    Parameters
    ----------
    rayjn_dir_root : string
        Rayjn simulation root directory.
    data : astropy.table.QTable object
        Metadata.
    file_pat : string, optional
        Output plot file name pattern.

    Returns
    -------
    None.

    """
    # read all_data file
    all_data_file_name = get_s2n_all_file_name(rayjn_dir_root)
    log.info('Now read: '+all_data_file_name)
    # ('date','alt','az','el','dist','met_r','met_T','pix_sz','F','max_s2n')
    data = QTable.read(all_data_file_name,format='ascii.fixed_width_two_line')
    data.sort(['date','alt','az','el','Sun_dist'])
    
    # define unique values
    azimuths = np.unique(np.sort(data['az']))
    elevations = np.unique(np.sort(data['el']))
    altitudes = np.unique(np.sort(data['alt']))
    
    # define masks
    seasons = {'summer':'2020-06-21T12:00:00.000',
               'winter':'2020-12-21T12:00:00.000',
               'night':'2020-03-21T00:00:00.000'}    
    
    # create itertools for graphs
    markers = ('+', 'x', 'o', '*','D','s','v','X','^','<','>')
    #marker = itertools.cycle(markers)#,4,5,6,7))
    colors = ('b', 'g', 'r', 'c','m','y','k','grey','orange','deepskyblue','purple','tomato','olivedrab','pink')
    #color = itertools.cycle(colors)
    
    # loop over seasons
    for season in seasons:
        mask_season = data['date']==seasons[season]
        data_sea = data[mask_season]
        # plot influence of az
        plotfile = season + '_'+file_pat.replace('*','alt')
        fig=plt.figure()
        color = itertools.cycle(colors[:len(azimuths)])
        for azim in azimuths:
            col = next(color)
            data_az = data_sea[data_sea['az']==azim]
            marker = itertools.cycle(markers[:len(elevations)])
            for elv in elevations:
                data_el = data_az[data_az['el']==elv]
                if (len(data_el)<4):
                    msg = season+': missing data for el: '+str(elv)+' az: '+str(azim)
                    log.error(msg)
                    log.error(str(data_el))
                    raise ValueError(msg)
                plt.plot(data_el['alt'],data_el['med_s2n'],color=col,\
                         marker=next(marker),markersize=5, linewidth=0.5,\
                         label='el: '+"{:.0f}".format(elv)+\
                               ' deg az: '+"{:.0f}".format(azim)+\
                               ' deg d:'+"{:.0f}".format(data_el['dist'][0])+\
                               ' km, Sun:'+"{:.0f}".format(data_el['Sun_dist'][0])+' deg')
        #plt.ylim([0, max_s2n_mir])
        # add horizontal dash line at S/N=1
        plt.axhline(1.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.axhline(2.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.title('median(s/n) vs Altitude',color='indianred')
        plt.xlabel('Altitude [km]')
        plt.ylabel('median (S/N)')
        plt.yscale('log')
        plt.legend(loc='upper left', fontsize="4")
        plt.savefig(plotfile,dpi=300)
        plt.close(fig)
        log.info('Fig saved in '+plotfile)
    
        # plot influence of el
        plotfile = season + '_'+file_pat.replace('*','alt-el')
        fig=plt.figure()
        color = itertools.cycle(colors[:len(elevations)])
        for elv in elevations:
            col = next(color)
            data_el = data_sea[data_sea['el']==elv]
            marker = itertools.cycle(markers[:len(elevations)])
            for azim in azimuths:
                data_az = data_el[data_el['az']==azim]
                if (len(data_el)<4):
                    msg = season+': missing data for el: '+str(elv)+' az: '+str(azim)
                    log.error(msg)
                    log.error(str(data_el))
                    raise ValueError(msg)
                plt.plot(data_az['alt'],data_az['med_s2n'],color=col,\
                         marker=next(marker),markersize=5, linewidth=0.5,\
                         label='el: '+"{:.0f}".format(elv)+\
                               ' deg az: '+"{:.0f}".format(azim)+\
                               ' deg d:'+"{:.0f}".format(data_el['dist'][0])+\
                               ' km, Sun:'+"{:.0f}".format(data_el['Sun_dist'][0])+' deg')
        #plt.ylim([0, max_s2n_mir])
        # add horizontal dash line at S/N=1
        plt.axhline(1.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.axhline(2.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.title('median(s/n) vs Altitude',color='indianred')
        plt.xlabel('Altitude [km]')
        plt.ylabel('median (S/N)')
        plt.yscale('log')
        plt.legend(loc='upper left', fontsize="4")
        plt.savefig(plotfile,dpi=300)
        plt.close(fig)
        log.info('Fig saved in '+plotfile)
        
        # plot influence of Sun_dist
        plotfile = season + '_'+file_pat.replace('*','alt-Sun')
        fig=plt.figure()
        colordata = data_sea['Sun_dist'].value
        plt.scatter(data_sea['alt'],data_sea['med_s2n'],c=colordata, cmap='rainbow',) #\
                 #marker=next(marker),markersize=5, linewidth=0.5)
        #plt.ylim([0, max_s2n_mir])
        # add horizontal dash line at S/N=1
        plt.axhline(1.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.axhline(2.0, linestyle='--', linewidth=0.5,color=next(color))
        cbar = plt.colorbar()
        cbar.set_label('Sun distance [deg]')
        plt.title('median(s/n) vs Altitude',color='indianred')
        plt.xlabel('Altitude [km]')
        plt.ylabel('median (S/N)')
        plt.yscale('log')
        plt.legend(loc='upper left', fontsize="4")
        plt.savefig(plotfile,dpi=300)
        plt.close(fig)
        log.info('Fig saved in '+plotfile)    
    log.info('Plots done.') 
    return


def info(wavelength,lens,camera,meteoroid):
    """
    Long informations about the simulation.
    
    """
    log.info('***** Details of the simulation ***')
    for obj in (wavelength,lens,camera,meteoroid):
        for k in obj.keys():
            log.info(str(k)+'  '+str(obj[k]))
    return

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='compute S/N arguments.')
    parser.add_argument('-d',default=home+'/malbec/Software/ONERA/Pierre/DATA/',help='Rayjn data directory. Default is: home+/malbec/Software/ONERA/Pierre/DATA/')
    parser.add_argument('-r',default=10.0,help='Meteoroid size in [mm]. Default is: 10 mm',type=float)
    parser.add_argument('-T',default=2000,help='Meteoroid temperature in [K]. Default is: 2000 K',type=float)
    parser.add_argument('-epsilon',default=1.0,help='Meteoroid emissivity. Default is: 100.',type=float)
    parser.add_argument('-a',default=100.0,help='Meteoroid altitude in [km]. Default is: 1.',type=float)
    parser.add_argument('-s',default=15,help='Camera pixel size, in [um]. Default is: 15 um',type=float)
    parser.add_argument('-x',default=640,help='Camera horizontal number of pixel. Default is: 640',type=float)
    parser.add_argument('-y',default=512,help='Camera horizontal number of pixel. Default is: 512',type=float)
    parser.add_argument('-b',default=8,help='Camera number of bits. Default is: 8',type=int)
    parser.add_argument('-F',default=13,help='Lens focal length, in [mm]. Default is: 13 mm',type=float)
    parser.add_argument('-N',default=2,help='Lens aperture number. Default is: 2',type=float)
    parser.add_argument('-q',default=home+'/PROJECTS/PODET/PODET-MET/MISCHE/hefdig/ONERA-MidIR/Reponse_Spectrale_Energetique_et_Photonique_NoxCam_620_BB_HSi_17011_Objectif13mm_sans_Densite_avec_filtre_3_5.csv',help='Camera QE measurements. Default is: $home+/PROJECTS/PODET/PODET-MET/MISCHE/hefdig/ONERA-MidIR/Reponse_Spectrale_Energetique_et_Photonique_NoxCam_620_BB_HSi_17011_Objectif13mm_sans_Densite_avec_filtre_3_5.csv')
    parser.add_argument('-v',default=30,help='Meteoroid atmosphere entre velocity in [km/s]. Default is: 30 km/s.',type=float)
    parser.add_argument('-t',default=45,help='Radiant angular distance in [deg]. Default is: 45 deg.',type=float)
    parser.add_argument('-lambda_min',default=3.0,help='Minimum wavelength in [um]. Default is: 3 um.',type=float)
    parser.add_argument('-lambda_max',default=5.0,help='Maximum wavelength in [um]. Default is: 5 um.',type=float)
    parser.add_argument('-exp_time',default=5.0,help='Exposure time in [ms]. Default is: 5 ms', type=float)
    parser.add_argument('--all',action="store_true",default=False,help='Launch all processes. Default is: False')
    parser.add_argument('--s2n',action="store_true",default=False,help='Launch the computation of S/N for each subdirectory data only. Default is: False')
    parser.add_argument('--max',action="store_true",default=False,help='Launch the concatenation of max of S/N for computed data only. Default is: False')
    parser.add_argument('--plt',action="store_true",default=False,help='Launch the plots. Default is: False')
    parser.add_argument('--skip',action="store_true",default=False,help='Skip the computation if output files already exist. Default is: False')
    
    # retrieve arguments
    args = parser.parse_args()
    # set units
    args.r = args.r*u.mm
    args.T = args.T*u.K
    args.a = args.a*u.km
    args.x = args.x*u.pix
    args.y = args.y*u.pix
    args.s = args.s*u.um
    args.F = args.F*u.mm
    args.v = args.v*u.km/u.s
    args.t = args.t*u.deg
    args.lambda_min = args.lambda_min * u.um
    args.lambda_max = args.lambda_max * u.um
    args.exp_time = args.exp_time * u.ms
    
    args.s2n=True
    # store computation options        
    if (args.all):
        args.s2n = True
        args.max = True
        args.plt = True    
    
    # set data directory
    rayjn_dir_root = args.d
    #wnd_dir = home + '/PROJECTS/PODET/PODET-MET/MISCHE/hefdig/ONERA-MidIR/Data_METEOR/spectrometer/'
    # make wavelength
    wavelength = {'lambda_min': args.lambda_min, 'lambda_max':args.lambda_max}
    # meteoroid size and surface temperature
    meteoroid = {'rad':args.r,'T':args.T, 'eps': args.epsilon, 'alt':args.a.to('km')}
    # camera features
    camera = {'x':args.x,'y':args.y,'sz':args.s,'bit':args.b,
              'QE_file':args.q}
    # lens features
    Dpup = args.F / args.N
    lens = {'F':args.F,'N':args.N,'Dpup':Dpup}    
    # geometry definition
    orbital_geometry = {'met_Vatm':args.v,'radiant_dist':args.t, 'Lobs':None}
    
    # launch the computation of S/N for all simulated cases
    if args.s2n:
        launch_s2n(rayjn_dir_root,wavelength,meteoroid,camera,lens,skip=args.skip)
        log.info('Computation of S/N done')
    else:
        log.warning('Computation of S/N NOT performed.')
    
    # get max S/N
    if args.max:
        get_max_s2n(rayjn_dir_root,wavelength,meteoroid,camera,lens)
        log.info('Computation of max S/N done')
    else:
        log.warning('Computation of max S/N NOT performed.')
    
    # plot data
    if args.plt:
        plot_data(rayjn_dir_root)
        log.info('Plots done')
    else:
        log.warning('Plots NOT performed.')
    
    log.info('All done')

else:
    log.debug('successfully imported')


