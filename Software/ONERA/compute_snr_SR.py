#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 09:12:53 2022

@author: vaubaill

Compute the S/N for a given meteorite in wind tunnel, as observed as real meteor.
"""
import os
import glob
import argparse
import itertools
import numpy as np
import astropy.units as u
from astropy.table import QTable
from astropy.time import Time
from astropy.coordinates import EarthLocation, AltAz, get_sun
import matplotlib.pyplot as plt

from rayjn_log import log
#from read_rayjn import read_rayjn_comp,read_rayjn_lum,plot_lum,plot_comp
from function_snr_SR import func_snr_SR,func_snr_SR_spectral

# ============= USER input =============
# path
home = os.getenv('HOME')+'/'

# ========== END OF USER INPUT =========

def get_s2n_SR_file_name(rayjn_dir,rad,T,spectral=False):
    """
    
    Parameters
    ----------
    rayjn_dir : string
       Rayjn simulation output directory. 
    rad : astropy.units.Quantity object
        meteoroid radius.
    T : astropy.units.Quantity object
        meteoroid temperature.
    spectral : boolean, optional.
        If True, set name for spectral data. Default if False.
    
    Returns
    -------
    data_file_name : string
        output data file name.
        
    """
    file_root = rayjn_dir + 'XXX_r'+f"{rad.to('m').value:.0E}"+'_T'+\
                                    f"{T.to('K').value:.0f}"
    if spectral:
        data_file_name = file_root.replace('XXX','s2n_pix_SR_spectral')+'.dat'
    else:
        data_file_name = file_root.replace('XXX','s2n_pix_SR')+'.dat'        
    return data_file_name


def get_s2n_all_file_name(rayjn_dir_root):
    """
    Get all s/n data file name

    Parameters
    ----------
    rayjn_dir_root : string
        All Rajn output data directory.

    Returns
    -------
    s2n_all_file_name : string
        File name of all data.

    """
    s2n_all_file_name = rayjn_dir_root + 's2n_all_SR.dat'
    return s2n_all_file_name


def compute_s2n_SR(rayjn_dir,wavelength,lens,sensor,meteoroid,orbital_geometry,com_file='comp_Rayjn.xls',
                   skip=False):
    """
    
    
    """
    log.info('rayjn_dir: '+rayjn_dir)
    # check com_file
    if com_file=='comp_Rayjn.xls':
        com_file = rayjn_dir+'comp_Rayjn.xls'
    if not os.path.isfile(com_file):
        msg = '*** FATAL ERROR: file '+com_file+' does not exist'
        log.error(msg)
        raise IOError(msg)
    # set meteoroid size
    # create output file root name
    data_file_name = get_s2n_SR_file_name(rayjn_dir,meteoroid['x'],meteoroid['T'])
    data_spectral_file_name = get_s2n_SR_file_name(rayjn_dir,meteoroid['x'],meteoroid['T'],spectral=True)
    
    # skip of files already exist
    if (skip  and os.path.isfile(data_file_name)):
        log.warning('Files already exist: skipping')
        return
    
    # get camera altitude and elevation above horizon from rayjn_dir name
    alt = float(rayjn_dir.split('ALT')[1].split('AZ')[0])*u.km
    elv = float(rayjn_dir.split('EL')[1].split('/')[0])*u.deg
    # Vertical distance between the camera and top of the sky
    dvert = meteoroid['alt'] - alt
    # compute distance between camera and meteor
    dist = dvert / np.sin(elv.to('rad'))
    log.info('data file name: '+data_file_name)
    log.info('camera altitude: '+str(alt.to('km')))
    log.info('camera pointing elevation: '+str(elv.to('deg')))
    log.info('meteoroid altitude: '+str(meteoroid['alt'].to('km')))
    log.info('dist: '+str(dist.to('km')))
    
    # complement orbital_geometry dict
    orbital_geometry['Lobs']=dist
    
    # compute S/N at pixel level for the whole band
    s2n_SR = func_snr_SR(wavelength,lens,sensor,meteoroid,orbital_geometry,
                         comp_file=com_file,outdir=rayjn_dir)
    log.info('s2n_SR: '+str(s2n_SR))
    
    # save data
    s2n_pix_data = QTable(names=('s2n_SR','dist [km]'))
    s2n_pix_data.add_row([s2n_SR, dist.to('km')])
    s2n_pix_data.write(data_file_name,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('s2n_SR saved in '+data_file_name)
    
    
    # compute spectral S/N at pixel level for the whole band
    s2n_SR_spectral,wavelength_spectral = func_snr_SR_spectral(wavelength,lens,sensor,meteoroid,orbital_geometry,
                         comp_file=com_file,outdir=rayjn_dir)
    log.debug('s2n_SR_spectral: '+str(s2n_SR_spectral))
    
    # save data
    s2n_pix_data_spectral = QTable([s2n_SR_spectral, wavelength_spectral.to('um')],names=('s2n_SR','wavelength [um]'))
    s2n_pix_data_spectral.write(data_spectral_file_name,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('s2n_SR_spectral saved in '+data_spectral_file_name)
    
    return 


def rayjn_dir_name2param(rayjn_dir_name):
    """
    
    Parameters
    ----------
    rayjn_dir_name : string
        Rayjn simulation output directory name.

    Returns
    -------
    date : astropy.time.Time object
        Date of simulation.
    alt : astropy.units.Quantity object
        Altitude of the camera.
    az : astropy.units.Quantity object
        Camera pointing azimuth (from North).
    el : astropy.units.Quantity object
        Campera pointing elevation (from horizon).

    """
    subd = rayjn_dir_name.split('/')[-2]
    timestr,params = subd.split('_UT')
    timeisot = timestr[:4]+'-'+timestr[4:6]+'-'+timestr[6:8]+'T'+\
               timestr[9:11]+':'+timestr[11:13]+':'+timestr[13:15]
    date = Time(timeisot)
    alt = float(params.split('-ALT')[-1].split('AZ')[0]) *u.km
    az,el = params.split('AZ')[-1].split('EL')
    az = float(az) * u.deg
    el = float(el) * u.deg
    
    return date,alt,az,el

def launch_s2n_SR(rayjn_dir_root,wavelength,lens,sensor,meteoroid,
                  orbital_geometry,skip=False):
    """
    Launch all computations of S/N for all simulated cases.

    Parameters
    ----------
    rayjn_dir_root : string
        Rayjn simulation root directory.
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    lens : dict
        Lens parameters:
            Dpup: astropy.units.Quantity object
                Pupil diamater, in [m] or [mm].
            FocL: astropy.units.Quantity object
                Focal lens. unit=[m].
            Nopt: float
                Lens aperture (no unit).
            Topt: float
                Lens transmission (no unit).
    sensor : dict
        Sensor parameters:
            pix_sz: astropy.units.Quantity object
                Detector size, in [m] or [um].
            pix_pitch: astropy.units.Quantity object
                Sampling pitch, in [m] or [um].
            pix_num: integer
                Number of samples, i.e. number of pixels, assuming a square sensor. no unit.
            QE: float
                Quantum efficiency over the whole wavelength band range. no unit.
            exp_time: astropy.units.Quantity object
                Integration time, in [s] or [us].
            pix_T: astropy.units.Quantity object
                Sensor temperature, in [K].
            Idark: astropy.units.Quantity object
                Sensor dark current, in [A] or [pA].
            Rsh: astropy.units.Quantity object
                Dynamic resistance of the photodiode, in [Ohm].
            VSF: astropy.units.Quantity object
                Output amplifier noise, in [V] or [uV].
            CsG: astropy.units.Quantity object
                Storage capacity at the output follower in [F] or [pF].
            Delat_Vs: astropy.units.Quantity object
                Excursion voltage of the output follower, in [V]
            nbbit: integer
                Number of digitization bits (dimensionless).
    meteoroid: dict
        Meteoroid parameters:
            x: astropy.units.Quantity object
                Meteoroid size in x direction, in [m].
            y: astropy.units.Quantity object
                Meteoroid size in y direction, in [m].
            T: astropy.units.Quantity object
                Meteoroid surface temperature, in [K].
            eps: float
                Meteoroid emissivity (dimensionless).
            alt: astropy.units.Quantity object
                Meteoroid altitude, in [km].
    orbital_geometry: dict
        Orbital geometry parameters.
            met_Vatm: astropy.units.Quantity object
                Meteoroid entry velocity w.r.t Earth., in [km/s] or [m/s].
            radiant_dist: astropy.units.Quantity object
                Angular distance between pointing direction and meteoroid radiant, in [rad] or [deg].
            Lobs: astropy.units.Quantity object
                Physical distance between the camera and the meteoroid, in [m] or [km].
    skip : bollean, optional
        If True and output file already exist, computation is skipped. Default is False.

    Returns
    -------
    None.

    """     
    # list all rayjn directories
    list_rayjn = glob.glob(rayjn_dir_root+'*UT-ALT*')
    list_rayjn.sort(reverse=True)
        
    log.info('Nb of directories to process: '+str(len(list_rayjn)))
    for rayjn_dir in list_rayjn:
        if not rayjn_dir.endswith('/'):
            rayjn_dir = rayjn_dir + '/'
        log.info('====== processing directory: '+rayjn_dir)
        # compute s/n
        compute_s2n_SR(rayjn_dir,wavelength,lens,sensor,meteoroid,orbital_geometry,skip=skip)
    log.info('done')    
    
    return

def get_Solar_dist(rayjn_dir):
    """
    Get Solar distance from pointing direction.

    Parameters
    ----------
    rayjn_dir : string
        Rayjn simulation root directory.

    Returns
    -------
    solar_dist : TYPE
        DESCRIPTION.

    """
    # get date and pointing direction
    date,alt,az,el = rayjn_dir_name2param(rayjn_dir)
    # read Rayjn parameters file to get location
    rayjn_conf = rayjn_dir + '/Rayjn.data'
    if not os.path.isfile(rayjn_conf):
        msg = '*** FATAL ERROR: Rayjn configuration data '+rayjn_conf+' does not exist'
        log.error(msg)
        raise IOError(msg)
    # read Rayjn config file
    log.info('Now reading Rayjn config file: '+rayjn_conf)
    with open(rayjn_conf, mode='r', encoding="latin-1") as fin:
        nline = 0
        for line in fin.readlines():
            nline = nline+1
            if nline==4:
                l = line.split('lat')[0]
                for i in range(10):
                    l = l.replace('  ',' ')
                lat,lon = l.split(' ')[:2]
                break
    # convert lon,lat
    lon = float(lon)*u.deg
    lat = float(lat)*u.deg
    loc = EarthLocation.from_geodetic(lon=lon,lat=lat,height=alt)
    # create altAz frame
    alataz_frame = AltAz(az=az,alt=el,obstime=date,location=loc)
    log.debug(str(alataz_frame))
    # get Sun coordinats
    radec_sun = get_sun(date)
    # compute azimuth and elevation of the Sun
    azel_sun = radec_sun.transform_to(alataz_frame)
    log.debug(str(azel_sun))
    # computa angular separation with the Sun
    solar_dist = alataz_frame.separation(azel_sun)
    return solar_dist

def get_max_s2n_SR(rayjn_dir_root,wavelength,lens,sensor,meteoroid,orbital_geometry,outdir=None):
    """
    Read metadata and get max S/N.

    Parameters
    ----------
    rayjn_dir_root : string
        Rayjn simulation root directory.
    wavelength: dict
        Wavelength to consider for the computation of the signal to noise ratio.
            lambda_min: astropy.units.Quantity object
                Shortest wavelength, in [m] or [um].
            lambda_max: astropy.units.Quantity object
                Longest wavelength, in [m] or [um].
    lens : dict
        Lens parameters:
            Dpup: astropy.units.Quantity object
                Pupil diamater, in [m] or [mm].
            FocL: astropy.units.Quantity object
                Focal lens. unit=[m].
            Nopt: float
                Lens aperture (no unit).
            Topt: float
                Lens transmission (no unit).
    sensor : dict
        Sensor parameters:
            pix_sz: astropy.units.Quantity object
                Detector size, in [m] or [um].
            pix_pitch: astropy.units.Quantity object
                Sampling pitch, in [m] or [um].
            pix_num: integer
                Number of samples, i.e. number of pixels, assuming a square sensor. no unit.
            QE: float
                Quantum efficiency over the whole wavelength band range. no unit.
            exp_time: astropy.units.Quantity object
                Integration time, in [s] or [us].
            pix_T: astropy.units.Quantity object
                Sensor temperature, in [K].
            Idark: astropy.units.Quantity object
                Sensor dark current, in [A] or [pA].
            Rsh: astropy.units.Quantity object
                Dynamic resistance of the photodiode, in [Ohm].
            VSF: astropy.units.Quantity object
                Output amplifier noise, in [V] or [uV].
            CsG: astropy.units.Quantity object
                Storage capacity at the output follower in [F] or [pF].
            Delat_Vs: astropy.units.Quantity object
                Excursion voltage of the output follower, in [V]
            nbbit: integer
                Number of digitization bits (dimensionless).
    meteoroid: dict
        Meteoroid parameters:
            x: astropy.units.Quantity object
                Meteoroid size in x direction, in [m].
            y: astropy.units.Quantity object
                Meteoroid size in y direction, in [m].
            T: astropy.units.Quantity object
                Meteoroid surface temperature, in [K].
            eps: float
                Meteoroid emissivity (dimensionless).
            alt: astropy.units.Quantity object
                Meteoroid altitude above sea level, in [km].
    orbital_geometry: dict
        Orbital geometry parameters.
            met_Vatm: astropy.units.Quantity object
                Meteoroid entry velocity w.r.t Earth., in [km/s] or [m/s].
            radiant_dist: astropy.units.Quantity object
                Angular distance between pointing direction and meteoroid radiant, in [rad] or [deg].
            Lobs: astropy.units.Quantity object
                Physical distance between the camera and the meteoroid, in [m] or [km].

    Returns
    -------
    None.

    """
    if outdir is None:
        outdir = rayjn_dir_root
    # make output data file name
    all_data_file_name = get_s2n_all_file_name(outdir)
    # create output table
    data = QTable(names=('date'  ,'alt'  ,'az'   ,'el'   ,'dist' ,'Sun_dist','met_r','met_T','pix_sz','F'    ,'s2n'),
                  dtype=['object','float','float','float','float','float'   ,'float','float','float' ,'float','float'])
    
    # list all rayjn directories
    list_rayjn = glob.glob(rayjn_dir_root+'*UT-ALT*')
    list_rayjn.sort() 
    log.info('Nb of directories to process: '+str(len(list_rayjn)))
    # loop over all directories
    for rayjn_dir in list_rayjn:
        if not rayjn_dir.endswith('/'):
            rayjn_dir = rayjn_dir + '/'
        log.info('====== processing directory: '+rayjn_dir)
        # retrieve simulation parameters
        date,alt,az,el = rayjn_dir_name2param(rayjn_dir)
        # get data file names
        data_file_name = get_s2n_SR_file_name(rayjn_dir,meteoroid['x'],meteoroid['T'])
        # get Solar distance from pointing direction
        solar_dist = get_Solar_dist(rayjn_dir) 
        # get max s/n
        log.info('Read: '+data_file_name)
        data_comp = QTable.read(data_file_name,format='ascii.fixed_width_two_line')
        dist,s2n_mir = data_comp['dist [km]'][0],data_comp['s2n_SR'][0]
        # store data
        data.add_row([date,alt,az,el,dist,solar_dist,meteoroid['x'],meteoroid['T'],sensor['pix_sz'],lens['FocL'],s2n_mir])
    log.info('done')    
    
    # sort data
    data.sort(('date','alt','az','el'))
    # set formats
    data['alt'].info.format = '.0f'
    data['az'].info.format = '.0f'
    data['el'].info.format = '.0f'
    data['dist'].info.format = '.0f'
    data['Sun_dist'].info.format = '.0f'
    data['met_r'].info.format = '.0f'
    data['met_T'].info.format = '.0f'
    data['pix_sz'].info.format = '.0f'
    data['F'].info.format = '.0f'
    data['s2n'].info.format = '.2f'
    # save data
    data.write(all_data_file_name,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('Data save in '+all_data_file_name)
    return

def plot_data_SR(rayjn_dir_root,file_pat='s2n_SR_*.png'):
    """Plot all S/N data.

    Parameters
    ----------
    rayjn_dir_root : string
        Rayjn simulation root directory.
    file_pat : string, optional
        Output plot file name pattern.
    outdir : string, optional
        Output directory name. Default is None, meaning that rayjn_dir_root is the output directory.

    Returns
    -------
    None.

    """
    # read all_data file
    all_data_file_name = get_s2n_all_file_name(rayjn_dir_root)
    log.info('Now read: '+all_data_file_name)
    # ('date','alt','az','el','dist','met_r','met_T','pix_sz','F','max_s2n')
    data = QTable.read(all_data_file_name,format='ascii.fixed_width_two_line')
    data.sort(['date','alt','az','el','Sun_dist'])
    
    # define unique values
    azimuths = np.unique(np.sort(data['az']))
    elevations = np.unique(np.sort(data['el']))
    altitudes = np.unique(np.sort(data['alt']))
    
    # define masks
    seasons = {'summer':'2020-06-21T12:00:00.000',
               'winter':'2020-12-21T12:00:00.000',
               'night':'2020-03-21T00:00:00.000'}    
    
    # create itertools for graphs
    markers = ('+', 'x', 'o', '*','D','s','v','X','^','<','>')
    #marker = itertools.cycle(markers)#,4,5,6,7))
    colors = ('b', 'g', 'r', 'c','m','y','k','grey','orange','deepskyblue','purple','tomato','olivedrab','pink')
    #color = itertools.cycle(colors)
    
    # loop over seasons
    for season in seasons:
        mask_season = data['date']==seasons[season]
        data_sea = data[mask_season]
        # plot influence of az
        plotfile = rayjn_dir_root + season + '_'+file_pat.replace('*','alt-az')
        fig=plt.figure()
        color = itertools.cycle(colors[:len(azimuths)])
        for azim in azimuths:
            col = next(color)
            data_az = data_sea[data_sea['az']==azim]
            marker = itertools.cycle(markers[:len(elevations)])
            for elv in elevations:
                data_el = data_az[data_az['el']==elv]
                if (len(data_el)<4):
                    msg = season+': missing data for el: '+str(elv)+' az: '+str(azim)
                    log.error(msg)
                    log.error(str(data_el))
                    raise ValueError(msg)
                plt.plot(data_el['alt'],data_el['s2n'],color=col,\
                         marker=next(marker),markersize=5, linewidth=0.5,\
                         label='el: '+"{:.0f}".format(elv)+\
                               ' deg az: '+"{:.0f}".format(azim)+\
                               ' deg d:'+"{:.0f}".format(data_el['dist'][0])+\
                               ' km, Sun:'+"{:.0f}".format(data_el['Sun_dist'][0])+' deg')
        #plt.ylim([0, s2n_mir])
        # add horizontal dash line at S/N=1
        plt.axhline(1.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.axhline(2.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.title('S/N vs Altitude',color='indianred')
        plt.xlabel('Altitude [km]')
        plt.ylabel('S/N')
        plt.yscale('log')
        plt.legend(loc='upper left', fontsize="4")
        plt.savefig(plotfile,dpi=300)
        plt.close(fig)
        log.info('Fig saved in '+plotfile)
    
        # plot influence of el
        plotfile = rayjn_dir_root + season + '_'+file_pat.replace('*','alt-el')
        fig=plt.figure()
        color = itertools.cycle(colors[:len(elevations)])
        for elv in elevations:
            col = next(color)
            data_el = data_sea[data_sea['el']==elv]
            marker = itertools.cycle(markers[:len(elevations)])
            for azim in azimuths:
                data_az = data_el[data_el['az']==azim]
                if (len(data_el)<4):
                    msg = season+': missing data for el: '+str(elv)+' az: '+str(azim)
                    log.error(msg)
                    log.error(str(data_el))
                    raise ValueError(msg)
                plt.plot(data_az['alt'],data_az['s2n'],color=col,\
                         marker=next(marker),markersize=5, linewidth=0.5,\
                         label='el: '+"{:.0f}".format(elv)+\
                               ' deg az: '+"{:.0f}".format(azim)+\
                               ' deg d:'+"{:.0f}".format(data_el['dist'][0])+\
                               ' km, Sun:'+"{:.0f}".format(data_el['Sun_dist'][0])+' deg')
        #plt.ylim([0, s2n_mir])
        # add horizontal dash line at S/N=1
        plt.axhline(1.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.axhline(2.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.title('S/N vs Altitude',color='indianred')
        plt.xlabel('Altitude [km]')
        plt.ylabel('S/N')
        plt.yscale('log')
        plt.legend(loc='upper left', fontsize="4")
        plt.savefig(plotfile,dpi=300)
        plt.close(fig)
        log.info('Fig saved in '+plotfile)
        
        # plot influence of Sun_dist
        plotfile = rayjn_dir_root + season + '_'+file_pat.replace('*','alt-Sun')
        fig=plt.figure()
        colordata = data_sea['Sun_dist'].value
        plt.scatter(data_sea['dist'],data_sea['s2n'],c=colordata, cmap='rainbow',) #\
                 #marker=next(marker),markersize=5, linewidth=0.5)
        #plt.ylim([0, s2n_mir])
        # add horizontal dash line at S/N=1
        plt.axhline(1.0, linestyle='--', linewidth=0.5,color=next(color))
        plt.axhline(2.0, linestyle='--', linewidth=0.5,color=next(color))
        cbar = plt.colorbar()
        cbar.set_label('Sun distance [deg]')
        plt.title('S/N vs Distance',color='indianred')
        plt.xlabel('Distance [km]')
        plt.ylabel('S/N')
        plt.yscale('log')
        plt.legend(loc='upper left', fontsize="4")
        plt.savefig(plotfile,dpi=300)
        plt.close(fig)
        log.info('Fig saved in '+plotfile)    
    log.info('Plots done.') 
    return

def info(wavelength,lens,sensor,meteoroid,orbital_geometry):
    """
    Long informations about the simulation.
    
    """
    log.info('***** Details of the simulation ***')
    for obj in (wavelength,lens,sensor,meteoroid,orbital_geometry):
        for k in obj.keys():
            log.info(str(k)+'  '+str(obj[k]))
    return


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='compute S/N arguments.')
    parser.add_argument('-d',default=home+'/malbec/Software/ONERA/Pierre/DATA/',help='Rayjn data directory. Default is: $home+/malbec/Software/ONERA/Pierre/DATA/')
    parser.add_argument('-r',default=10.0,help='Meteoroid size in [mm]. Default is: 10 mm',type=float)
    parser.add_argument('-T',default=2000,help='Meteoroid temperature in [K]. Default is: 2000 K',type=float)
    parser.add_argument('-epsilon',default=1.0,help='Meteoroid emissivity. Default is: 100.',type=float)
    parser.add_argument('-a',default=100.0,help='Meteoroid altitude in [km]. Default is: 1.',type=float)
    parser.add_argument('-s',default=15,help='Camera pixel size, in [um]. Default is: 15 um',type=float)
    parser.add_argument('-x',default=640,help='Camera horizontal number of pixel. Default is: 640',type=float)
    parser.add_argument('-y',default=512,help='Camera horizontal number of pixel. Default is: 512',type=float)
    parser.add_argument('-b',default=14,help='Camera number of bits. Default is: 8',type=int)
    parser.add_argument('-F',default=100,help='Lens focal length, in [mm]. Default is: 13 mm',type=float)
    parser.add_argument('-N',default=2,help='Lens aperture number. Default is: 2',type=float)
#    parser.add_argument('-q',default=home+'/PROJECTS/PODET/PODET-MET/MISCHE/hefdig/ONERA-MidIR/Reponse_Spectrale_Energetique_et_Photonique_NoxCam_620_BB_HSi_17011_Objectif13mm_sans_Densite_avec_filtre_3_5.csv',help='Camera QE measurements. Default is: home+/PROJECTS/PODET/PODET-MET/MISCHE/hefdig/ONERA-MidIR/Reponse_Spectrale_Energetique_et_Photonique_NoxCam_620_BB_HSi_17011_Objectif13mm_sans_Densite_avec_filtre_3_5.csv')
    parser.add_argument('-q',default=0.8,help='Sensor quantum efficiency. Default is 0.8',type=float)
    parser.add_argument('-v',default=30,help='Meteoroid atmosphere entre velocity in [km/s]. Default is: 30 km/s.',type=float)
    parser.add_argument('-t',default=45,help='Radiant angular distance in [deg]. Default is: 45 deg.',type=float)
    parser.add_argument('-lambda_min',default=3.0,help='Minimum wavelength in [um]. Default is: 3 um.',type=float)
    parser.add_argument('-lambda_max',default=5.0,help='Maximum wavelength in [um]. Default is: 5 um.',type=float)
    parser.add_argument('-exp_time',default=5.0,help='Exposure time in [ms]. Default is: 5 ms', type=float)
    parser.add_argument('-o',default=None,help='Results output directory. Default is: None, meaning the -d option is used as output directory.')
    
    parser.add_argument('--all',action="store_true",default=False,help='Launch all processes. Default is: False')
    parser.add_argument('--s2n',action="store_true",default=False,help='Launch the computation of S/N for each subdirectory data only. Default is: False')
    parser.add_argument('--max',action="store_true",default=False,help='Launch the concatenation of max of S/N for computed data only. Default is: False')
    parser.add_argument('--plt',action="store_true",default=False,help='Launch the plots. Default is: False')
    parser.add_argument('--skip',action="store_true",default=False,help='Skip the computation if output files already exist. Default is: False')
    
    # retrieve arguments
    args = parser.parse_args()
    # set units
    args.r = args.r*u.mm
    args.T = args.T*u.K
    args.a = args.a*u.km
    args.x = args.x*u.pix
    args.y = args.y*u.pix
    args.s = args.s*u.um
    args.F = args.F*u.mm
    args.v = args.v*u.km/u.s
    args.t = args.t*u.deg
    args.lambda_min = args.lambda_min * u.um
    args.lambda_max = args.lambda_max * u.um
    args.exp_time = args.exp_time * u.ms
    
    # set output directory
    if args.o is None:
        args.o = args.d
    if not os.path.isdir(args.o):
        os.mkdir(args.o)
        log.info('Making Output directory: '+args.o)
    if not args.o.endswith('/'):
        args.o = args.o + '/'
    
    # store computation options        
    if (args.all):
        args.s2n = True
        args.max = True
        args.plt = True    
    
    # set data directory
    rayjn_dir_root = args.d
    # make data dict
    # make wavelength
    wavelength = {'lambda_min': args.lambda_min, 'lambda_max':args.lambda_max}
    # meteoroid size and surface temperature
    meteoroid = {'x':args.r, 'y':args.r, 'T':args.T, 'eps': args.epsilon, 'alt':args.a.to('km')}

    # sensor parameters
    # fixed parameters:
    pix_T = 80*u.K
    Idark = (2 * u.pA).to('A')  # Dark current
    Rsh = 1e9 * u.Ohm  # Dynamic resistance of the photodiode
    VSF = (100 * u.uV).to('V')  # Output amplifier noise
    CsG = (5.2 * u.pF).to('F')  # Storage capacity at the output follower
    Delat_Vs = 2.2 * u.V   # Excursion voltage of the output follower
    nbbit = args.b  # Number of digitization bits (dimensionless)
    sensor = {'pix_sz':args.s, 'pix_pitch':args.s, 'pix_num':args.x, 'QE':args.q,
              'exp_time':args.exp_time, 'pix_T':pix_T, 'Idark':Idark,
              'Rsh':Rsh, 'VSF':VSF, 'CsG':CsG, 'Delat_Vs': Delat_Vs,
              'nbbit': nbbit}

    # lens parameters
    # fixed parameters:
    Topt = 0.8 # optical transmission
    # computed parameters
    Dpup = args.F / args.N
    lens = {'Dpup':Dpup,'FocL':args.F,'Topt':Topt}

    # geometry definition
    orbital_geometry = {'met_Vatm':args.v,'radiant_dist':args.t, 'Lobs':None}
    
    # display informations about the simulation
    info(wavelength,lens,sensor,meteoroid,orbital_geometry)
    
    # launch the computation of S/N for all simulated cases
    if args.s2n:
        launch_s2n_SR(rayjn_dir_root,wavelength,lens,sensor,meteoroid,orbital_geometry,skip=args.skip)
        log.info('Computation of S/N done')
    else:
        log.warning('Computation of S/N NOT performed.')
    
    # get max S/N
    if args.max:
        get_max_s2n_SR(rayjn_dir_root,wavelength,lens,sensor,meteoroid,orbital_geometry,outdir=args.o)
        log.info('Computation of max S/N done')
    else:
        log.warning('Computation of max S/N NOT performed.')
    
    # plot data
    if args.plt:
        plot_data_SR(args.o)
        log.info('Plots done')
    else:
        log.warning('Plots NOT performed.')
    
    info(wavelength,lens,sensor,meteoroid,orbital_geometry)
    log.info('All done')

else:
    log.debug('successfully imported')


