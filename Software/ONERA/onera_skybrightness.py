import os
import sys
import logging
import argparse
import numpy as np
import astropy.units as u
from astropy.time import Time,TimeDelta
from astropy.table import QTable
import matplotlib.pyplot as plt
import glob


from bands import bands
from missionplanner.utils import name2time,time2name

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

def read_onera_data(filein,oldfmt=False):
    """Read ONERA (P. Simoneau) generated sky brightness file
    
    Parameters
    ----------
    filein : str
        ONERA file name.
    oldfmt : boolean, optional
        if True, old ONERA file format is considered. 
        Default is False.
    
    Returns
    -------
    data : astropy.table.QTable
        Raw ONERA data.
        Columns are:
            Wave_number : wave number in [cm-1]
            wavelength : wavelength in [um]
            Luminance1 : Luminance in [W m-2 sr-1 cm]
            Luminance2 : Luminance in [ph s-1 m-2 sr-1 cm]
            Luminance3 : Luminance in [W m-2 sr-1 um-1]
            Luminance4 : Luminance in [ph s-1 m-2 sr-1 um-1]
            Luminance5 : Luminance in [R um-1]
            dwavelength : difference between wavelength and previous one, in [um]
            radiance1 : radiance in [W m-2 sr-1]
            radiance2 : radiance in [ph s-1 m-2 sr-1]
    
    """
    if not os.path.exists(filein):
        msg = '*** FATAL ERROR: file '+filein+' does not exist'
        log.error(msg)
        raise IOError(msg)
    # get altitude from file name
    alt_str = filein.split('ALT')[1].split('AZ')[0]
    
    # set names
    names = ['Wave_number','Luminance1','Luminance2','wavelength','Luminance3','Luminance4','Luminance5']
    log.debug('Reading file: '+filein)
    data = QTable().read(filein,format='ascii.csv',delimiter=',',names=names,data_start=2)
    # sort data
    data.sort('wavelength')
    # set units
    if data['Luminance5'][0].mask:
        data['Wave_number'].unit = 1.0/u.cm
        data['Luminance1'].unit = 'W m-2 sr-1 cm' #'W/m2/sr/cm-1'
        data['Luminance2'].unit = 'ph s-1 m-2 sr-1 cm' # 'ph/s/m2/sr/cm-1'
        data['wavelength'].unit = u.um
        # save data in temporary variable
        tmp3 = data['Luminance3']
        tmp4 = data['Luminance4']
        tmp3.unit = 'ph s-1 m-2 sr-1 um-1'
        tmp4.unit = 'R um-1'
        data['Luminance3'] = data['Luminance1'] * data['Wave_number'] / data['wavelength']
        data['Luminance4'] = tmp3  # 'ph/s/m2/sr/um' 
        data['Luminance5'] = tmp4
    else:
        data['Wave_number'].unit = 1.0/u.cm
        data['Luminance1'].unit = 'W m-2 sr-1 / cm-1' #'W/m2/sr/cm-1'
        data['Luminance2'].unit = 'ph s-1 m-2 sr-1 cm' # 'ph/s/m2/sr/cm-1'
        data['wavelength'].unit = u.um
        data['Luminance3'].unit = 'W m-2 sr-1 um-1' # 'W/m2/sr/um'
        data['Luminance4'].unit = 'ph s-1 m-2 sr-1 um-1'  # 'ph/s/m2/sr/um'
        data['Luminance5'].unit = 'R um-1' #'R/um'
    dim = len(data['Wave_number'])
    
    # compute radiance: sum over bandwidth of all Luminances
    # note: the data have been sorted by wavelength
    dwavelength = data['wavelength'][1:dim] - data['wavelength'][0:dim-1]
    dwavelength = np.append(dwavelength,dwavelength[-1])
    radiance1 = data['Luminance3'].to('W m-2 sr-1 um-1') * dwavelength.to('um')
    radiance2 = data['Luminance4'].to('ph s-1 m-2 sr-1 um-1') * dwavelength.to('um')
    data.add_column(dwavelength,name='dwavelength')
    data.add_column(radiance1,name='radiance1')
    data.add_column(radiance2,name='radiance2')
    return data

def get_onera_skybgns(filein,fileout='same',mkplot=False,autorng=True):
    """Get ONERA sky brightness from a file.
    
    Parameters
    ----------
    filein : string
        Input file
    fileout: string, optional
        output file name. Default is 'same', meaning
    mkplt : logical
        If True, plot of sky brightness as a function of wavelength is created.
        Default is False.
    autorng : logical
        If True and mkplt is True, plot y-range is automatic.
        Default is True, implying the y-range is [-4,2]
    
    Returns
    -------
    None.
    
    """
    # get altitude from file name
    alt_str = filein.split('ALT')[1].split('AZ')[0]
    
    # read the file
    data = read_onera_data(filein)
    dim = len(data['Wave_number'])
    
    # plot Luminance as a function of wavelength
    figfile = os.path.abspath(filein).replace('.csv','.png')
    fig=plt.figure()
    plt.plot(data['wavelength'].to('um').value,np.log10(data['Luminance3'].to('W m-2 sr-1 um-1').value),'b-') # 'r+'
    #plt.plot(wavelength_mean.to('um').value,np.log10(radiance_total.to('W m-2 sr-1').value),'r+') # 'r+'
    
    plt.title('Brightness at '+alt_str+' km alt. (ONERA)')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
    plt.xlabel('Wavelength [um]')
    plt.ylabel('log10(Luminance [W m-2 sr-1 um-1])')
    if autorng:
        plt.ylim(-5, 2)
    plt.savefig(figfile)
    plt.close(fig)
    log.info('Figure saved in '+figfile)
    
    """
    # plot Luminance as a function of wavelength
    figfile = os.path.abspath(filein).replace('.csv','-radiance-sr.png')
    fig=plt.figure()
    plt.plot(data['wavelength'].to('um').value,np.log10(radiance.to('W m-2 sr-1').value),'b-') # 'r+'
    plt.title('Log10 Sky Luminance at '+alt_str+' km alt. (credit ONERA)')
    plt.xlabel('Wavelength [um]')
    plt.ylabel('radiance [W m-2 sr-1]')
    plt.savefig(figfile)
    plt.close(fig)
    log.info('Figure saved in '+figfile)
    """
    
    return

def get_onera_file_list(onera_path='../../DATA/ONERA',onera_pattern='lum_Rayjn-*.csv'):
    """Get list of qll ONERA generated sky brightness files.
    
    Parameters
    ----------
    onera_path : string, optional.
        Path where ONERA files are located.
        Default is: '../../DATA/ONERA'.
    onera_pattern : string, optional.
        ONERA file pattern. Default is 'lum_Rayjn-*.csv'.
    
    Returns
    -------
    onera_file_list : list of string
        ONERA files list.
    
    """
    onera_file_list = glob.glob(onera_path+'/'+onera_pattern)
    log.debug('onera_file_list'+str(onera_file_list))
    return onera_file_list

def name2datealtazelband(onera_file,onera_pattern='lum_Rayjn-*.csv'):
    """Convert ONERA file name into date, altitude and band.
    
    Parameters
    ----------
    onera_file : string
        ONERA data file name.
    
    Returns
    -------
    date : astropy.time.Time object
        Time for which the data needs to be retrieved.
    alt : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    az : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    el : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    bandbound : 2-array of astropy.units.Quantity objects.
        Wavelength boundaries included in the file.
    
    """
    [pat,datestr,altazelext] = onera_file.split('-')
    time = name2time(datestr)
    altazel = altazelext.split('.')[0]
    [altstr,azel] = altazel.split('AZ')
    alt = float(altstr.split('ALT')[1]) *u.km
    [azstr,elstr] = azel.split('EL')
    az = float(azstr)*u.deg
    el = float(elstr)*u.deg
    # now get band boundaries
    data = read_onera_data(onera_file)
    bandbound = [np.min(data['wavelength'].to('um')),
                 np.max(data['wavelength'].to('um'))]
    return (time,alt,az,el,bandbound)

def get_all_onera_file_info(onera_path='../../DATA/ONERA',onera_pattern='lum_Rayjn-*.csv'):
    """Get all ONERA files info.
    
    Parameters
    ----------
    onera_path : string, optional.
        Path where ONERA files are located.
        Default is: '../../DATA/ONERA'.
    onera_pattern : string, optional.
        ONERA file pattern. Default is 'lum_Rayjn-*.csv'.
    
    Returns
    -------
    data : astropy.table.QTable
        All info in a QTable.
    
    """
    # make output QTable
    data = QTable(names=['name','date','alt','az','el','lambda_min','lambda_max'],
                  dtype=['str','object','float','float','float','float','float'])
    # get all ONERA files list
    onera_file_list = get_onera_file_list()
    # for each file get date, alt, band
    for onera_file in onera_file_list:  
        (time,alt,az,el,bandbound) = name2datealtazelband(onera_file)
        data.add_row([os.path.basename(onera_file),time,alt,az,el,bandbound[0],bandbound[1]])
    data['alt'].unit = u.km
    data['az'].unit = u.deg
    data['el'].unit = u.deg
    data['lambda_min'].unit = u.um
    data['lambda_max'].unit = u.um
    return data

def select_onera_file(time,alt,time_crit=20*u.d,alt_crit=4*u.km,onera_path='../../DATA/ONERA/'): # ,band):
    """Retrieve the name of ONERA file of interest given parameters.
    
    Parameters
    ----------
    time : astropy.time.Time object
        Time for which the data needs to be retrieved.
    alt : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    band : reduction.bands.Bands object
        Band for which the data needs to be retrieved.
    time_crit : astropy.units.Quantity object
        Time criterion. Default is 20 days.
    alt_crit : astropy.units.Quantity object
        Altitude criterion. Default is 4 km.
    onera_path : string, optional.
        Path where ONERA files are located.
        Default is: '../../DATA/ONERA'.
    
    Returns
    -------
    onera_file : str
        Full path ONERA data file name.
    
    """
    # get all data file info
    data_allf = get_all_onera_file_info()
    # select data 
    delta_time = (time - Time(data_allf['date'])).to('d')
    delta_alt = alt - data_allf['alt']
    mask = np.asarray([ (np.abs(delta_time.to('d')) < time_crit.to('d')) & (np.abs(delta_alt) < alt_crit) ])
    loc = mask.nonzero()[1]
    nloc = len(loc)
    # check the selection is successful
    if not nloc:
        log.error('For the reminder: all ONERA file names: '+str(data_allf['name']))
        mindelta_t = np.min(np.abs(delta_time.to('d')))
        mindelta_alt = np.min(np.abs(delta_alt.to('km')))
        msg = '*** FATAL ERROR: no ONERA file found for time: '+time.isot+' alt: '+str(alt.to('km'))
        msg = msg + ' for the given criteria: time_crit: '+str(time_crit.to('d'))+' alt_crit: '+str(alt_crit.to('km'))
        msg = msg + ' min(delta_t): '+str(mindelta_t)
        msg = msg + ' min(delta_alt): '+str(mindelta_alt)
        log.error(msg)
        raise ValueError(msg)
    
    # select data
    selected_files = data_allf['name'][loc]
    #log.debug('selected_files: '+str(selected_files))
    # choose first selected data
    onera_file = selected_files[0]
    log.debug('chosen file: '+onera_file)
    return onera_path+'/'+onera_file

def get_onera_data(date,alt):
    """Retrieve ONERA data for a given date, altitude and all bands.
    
    Parameters
    ----------
    date : astropy.time.Time object
        Time for which the data needs to be retrieved.
    alt : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    
    Returns
    -------
    outdata : astropy.table.QTable Object
        subset of ONERA data for given selection.
        Columns are:
            Wave_number : wave number in [cm-1]
            wavelength : wavelength in [um]
            Luminance1 : Luminance in [W m-2 sr-1 cm]
            Luminance2 : Luminance in [ph s-1 m-2 sr-1 cm]
            Luminance3 : Luminance in [W m-2 sr-1 um-1]
            Luminance4 : Luminance in [ph s-1 m-2 sr-1 um-1]
            Luminance5 : Luminance in [R um-1]
    
    """
    # choose the ONERA file
    onera_file = select_onera_file(date,alt)
    log.info('Read ONERA file: '+onera_file)
    # get ONERA data
    data = read_onera_data(onera_file)
    
    # check the selection went ok
    if not len(data['wavelength']):
        msg = '*** WARNING: no data ONERA data found for date: '+date.isot+' alt: '+str(alt)
        log.warning(msg)
    return data

def get_onera_data_per_band(date,alt,band_name):
    """Retrieve ONERA data for a given date, altitude and band.
    
    Parameters
    ----------
    date : astropy.time.Time object
        Time for which the data needs to be retrieved.
    alt : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    band_name : string
        Band name for which the data needs to be retrieved.
    
    Returns
    -------
    outdata : astropy.table.QTable Object
        subset of ONERA data for given selection.
        Columns are:
            Wave_number : wave number in [cm-1]
            wavelength : wavelength in [um]
            Luminance1 : Luminance in [W m-2 sr-1 cm]
            Luminance2 : Luminance in [ph s-1 m-2 sr-1 cm]
            Luminance3 : Luminance in [W m-2 sr-1 um-1]
            Luminance4 : Luminance in [ph s-1 m-2 sr-1 um-1]
            Luminance5 : Luminance in [R um-1]
    
    """
    # choose the ONERA file
    onera_file = select_onera_file(date,alt)
    log.info('Read ONERA file: '+onera_file)
    # get ONERA data
    data = read_onera_data(onera_file)
    # choose Band
    mask_band = np.array(bands['name']==band_name)
    loc = mask_band.nonzero()[0]
    slct_band = bands[loc]
    log.debug('slct_band: '+str(slct_band))
    # select data for particular band
    mask = np.asarray([ (data['wavelength'] > slct_band['lambda_min']) & 
                        (data['wavelength'] < slct_band['lambda_max'])
                     ])
    # make output data
    outdata = data[mask.nonzero()[1]]
    
    # check the selection went ok
    if not len(outdata['wavelength']):
        msg = '*** WARNING: no data ONERA data found for date: '+date.isot+' alt: '+str(alt)+' band_name: '+band_name
        log.warning(msg)
    return outdata

def get_onera_data_per_lambda(date,alt,lambda_min,lambda_max):
    """Retrieve ONERA data for a given date, altitude and band.
    
    Parameters
    ----------
    date : astropy.time.Time object
        Time for which the data needs to be retrieved.
    alt : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    lambda_min : astropy.units.Quantity object
        minimum wavelength to consider.
    lambda_max : astropy.units.Quantity object
        maximum wavelength to consider.
    
    Returns
    -------
    outdata : astropy.table.QTable Object
        subset of ONERA data for given selection.
        Columns are:
            Wave_number : wave number in [cm-1]
            wavelength : wavelength in [um]
            Luminance1 : Luminance in [W m-2 sr-1 cm]
            Luminance2 : Luminance in [ph s-1 m-2 sr-1 cm]
            Luminance3 : Luminance in [W m-2 sr-1 um-1]
            Luminance4 : Luminance in [ph s-1 m-2 sr-1 um-1]
            Luminance5 : Luminance in [R um-1]
    
    """
    # choose the ONERA file
    onera_file = select_onera_file(date,alt)
    log.info('Read ONERA file: '+onera_file)
    # get ONERA data
    data = read_onera_data(onera_file)
    # select data for particular band
    mask = np.asarray([ (data['wavelength'] > lambda_min) & 
                        (data['wavelength'] < lambda_max) ])
    # make output data
    outdata = data[mask.nonzero()[1]]
    
    # check the selection went ok
    if not len(outdata['wavelength']):
        msg = '*** WARNING: no data ONERA data found for date: '+date.isot+' alt: '+str(alt)+' lambda_min: '+str(lambda_min)+' lambda_max: '+str(lambda_max)
        log.warning(msg)
    return outdata

def get_sky_brightness(date,alt,band_name='',band_bound=[0.0*u.m,0.0*u.m]):
    """Compute sky brightness as a function of date, altitude and wavelength band.
    
    Parameters
    ----------
    date : astropy.time.Time object
        Time for which the data needs to be retrieved.
    alt : astropy.units.Quantity object
        Altitude for which the data needs to be retrieved.
    band_name : string, optional
        Band name for which the data needs to be retrieved.
        Useful if band_bound is not set. Default is ''.
    band_bound : 2-array of astropy.units.Quantity Objects, optional.
        Band boundaries, in [um]. Useful if band_name is not set.
        Default is [0.0*u.m,0.0*u.m].
    
    Returns
    -------
    sky_brightness : astropy.units.Quantity Objects
        Sky brightness in [W m-2 sr-1]
    
    """
    if band_name=='':
        msg = '*** FATAL ERROR: please specify a band name'
        log.error(msg)
        raise ValueError(msg)
    # get data subset
    data = get_onera_data_per_band(date,alt,band_name)
    # make sum of luminance
    sum_radiance1 = np.sum( data['radiance1'] )
    sum_radiance2 = np.sum( data['radiance2'] )
    
    return (sum_radiance1,sum_radiance2)

if __name__ == '__main__':
    # Argument parsing
    parser = argparse.ArgumentParser(description='Read sky brightness estimate performed by P. Simoneau, ONERA.')
    parser.add_argument('filein',help='File name to process',type=str)
    parser.add_argument('-out',help='output file name',default='same')
    parser.add_argument('-v','--verbose', help="Increase output verbosity",action="store_true")
    # get args
    args = parser.parse_args()
    # change logger setting if asked
    if args.verbose:
        log.setLevel(logging.DEBUG)
    # store arguments into local variables
    filein = args.filein
    fileout = args.out
    
    log.debug('filein: '+str(filein))
    log.debug('fileout: '+str(fileout))
    
    # launche main program
    get_onera_skybgns(filein)
else:
    log.info("successfully imported.")
