# MALBEC M-IR meteor signal to noise computation

import os
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
from astropy.constants import c, h, k_B
from astropy.table import QTable

# System Characteristics
Dpup = 50 * u.mm  # Diameter of the pupil
Fopt = 100 * u.mm  # Focal length
Topt = 0.8  # Optical transmission (dimensionless)
lambda_bas = 3 * u.um  # Lower cut-off wavelength
lambda_haut = 5 * u.um  # Upper cut-off wavelength
lambda_mean = (lambda_haut + lambda_bas) / 2  # Wavelength for diffraction calculation

a_det = 15 * u.um  # Detector size
p_ech = 15 * u.um  # Sampling pitch
nb_ech = 640  # Number of samples (dimensionless)
eta = 80 / 100  # Quantum efficiency (dimensionless)
ti_opt = 5 * u.ms  # Integration time
Temp_det = 80 * u.K  # Cooling temperature of the focal plane

Iobsc = (2 * u.pA).to('A')  # Dark current
Rsh = 1e9 * u.Ohm  # Dynamic resistance of the photodiode
VSF = (100 * u.uV).to('V')  # Output amplifier noise
CsG = (5.2 * u.pF).to('F')  # Storage capacity at the output follower
Delat_Vs = 2.2 * u.V   # Excursion voltage of the output follower
n_num = 14  # Number of digitization bits (dimensionless)

Cciblex = 1 * u.cm  # Target size in x direction
Ccibley = 1 * u.cm  # Target size in y direction
Lobs = 50 * u.km  # Observation distance TODO: re-compute this quantity
V_cible = 10 * u.km / u.s  # Relative speed of the target perpendicular to the line of sight
Temp_cible = 2000 * u.K  # Target temperature
epsilon_cible = 1  # Target emissivity (dimensionless)

radiant_dist = 45.0 * u.deg # angular distance between the radiant and the target
V_cible_app = V_cible * np.sin(radiant_dist.to('rad')) # apparent target velocity

# electron electric charge. unit=[C]
q = (1*u.eV).to('J').value * u.C

# Associated Values
Nopt = Fopt / Dpup  # numerical aperture
Spup = np.pi * (Dpup / 2)**2  # Area of the pupil
Spix = p_ech**2  # Area of a pixel
Tirage = Fopt  # Assumption for distance from the pupil to the detector
Gpix = (Spup.to('m2') * Spix.to('m2') / Tirage.to('m')**2)
omega_pup = np.arctan(Dpup.value/(2.0*Tirage.value))
omega_ciblex = np.arctan(Cciblex.to('m').value/(2*Lobs.to('m').value))
omega_cibley = np.arctan(Ccibley.to('m').value/(2*Lobs.to('m').value))
Gcible = (4 * np.sin(omega_ciblex) * np.sin(omega_cibley) * Spup).to('m2')


##############################################################################
# Define a function to calculate the spectral luminance of a black body
def spectral_luminance(T, wavelength):
    spc_lum = (2 * c / (wavelength.to('m')**4 * (np.exp((h * c) / (wavelength.to('m') * k_B * T.to('K')))  - 1))) *  u.ph / u.sr # unit-=[ph / s.sr.m3]
    return spc_lum
##############################################################################

##### Compute target radiance
# Define wavelengths range
wavelengths = np.linspace(lambda_bas.value, lambda_haut.value, 1000) * u.micron
# Calculate the spectral radiance at each wavelength
cible_spectral_luminance = spectral_luminance(Temp_cible, wavelengths)
# Integrate over the wavelength range to get total radiance
cible_band_luminance = np.trapz(cible_spectral_luminance, wavelengths.to('m'))
print('cible_band_luminance = '+str(cible_band_luminance))


# Read Rayjn data from comp_Rayjn.txt type file
comp_file = './comp_Rayjn.txt'
if not os.path.exists(comp_file):
    raise IOError(comp_file+' does not exist')
colstart = [0,20,44,61,86,116,139,160,188,202,224,260,284,308]
# colunits = (u.dimensionless_unscaled/u.cm,u.dimensionless_unscaled,u.W/(u.m**2 * u.sr),
#             u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr),
#             u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr), u.W / (u.m**2 * u.sr),
#             u.W / (u.m**2 * u.sr),u.W / (u.m**2 * u.sr),
#             u.dimensionless_unscaled,u.dimensionless_unscaled,u.dimensionless_unscaled)
colnames = [
    'wavenumber', 'transmission', 'thermique', 'diffusion_multiple', 'diffusion_simple', 
    'nightglow', 'stellaire', 'fond_de_sol', 'totale', 'source_exoatmos', 
    'source_transmise','?0','?1','?2']
data = QTable.read(comp_file,format='ascii.fixed_width',data_start=2,
                   col_starts=colstart,
                   names=colnames) #,
                   # units=colunits)
# sort data as a function of wavelength (inverse of wave number) for later compatibility
data['wavelength'] = (1/(data['wavenumber']/u.cm)).to('um')
data.sort('wavelength')
# Extract relevant columns and convert to astropy units
wavenumber = data['wavenumber'] / u.cm
transmission = data['transmission'] # dimensionless
thermique = data['thermique'] * (u.W / (u.m**2 * u.sr)) * u.cm
diffusion_multiple = data['diffusion_multiple'] * (u.W / (u.m**2 * u.sr)) * u.cm
diffusion_simple = data['diffusion_simple'] * (u.W / (u.m**2 * u.sr)) * u.cm
nightglow = data['nightglow'] * (u.W / (u.m**2 * u.sr)) * u.cm
stellaire = data['stellaire'] * (u.W / (u.m**2 * u.sr)) * u.cm
fond_de_sol = data['fond_de_sol'] * (u.W / (u.m**2 * u.sr)) * u.cm
lum_totale = data['totale'] * (u.W / (u.m**2 * u.sr)) * u.cm
source_exoatmos = data['source_exoatmos'] * (u.W / (u.m**2 * u.sr)) * u.cm
source_transmise = data['source_transmise'] * (u.W / (u.m**2 * u.sr)) * u.cm


# Extract required atmospheric and photonic luminance values
wavelength = data['wavelength'] # unit=[um]
lambda_factor = wavenumber.to('cm-1') / wavelength.to('um') # unit=[1/cm.um]
luminance_spectrale = (lum_totale * lambda_factor).to('W/sr.um.m2') # unit=[W/sr.um.m2]
energy_photon = (h*c/wavelength).to('W.s.m/um') # unit=[W.m/s.um]=[J]
luminance_photonique_spectrale = luminance_spectrale / energy_photon.to('W.s') * u.ph # unit=[ph / (s sr m2 um)]

# Retrieve wavelenghts in band
wavelength.sort()
mask = (wavelength >= lambda_bas) & (wavelength <= lambda_haut)
wavelength_band = wavelength[mask]

# compute luminance in band
luminance_photonique_spectrale_band = luminance_photonique_spectrale[mask] # unit=[ph / (s sr m2 um)]
luminance_photonique_band = np.sum(luminance_photonique_spectrale_band * np.diff(wavelength_band,append=lambda_haut)) # unit=[ph / (s sr m2)]

# get transmission for the considered band
trans_atm_spectrale_band = transmission[mask]
# TODO: plot transmission atmosphérique pour la bande considérée
trans_atm_band = np.sum(trans_atm_spectrale_band) / len(trans_atm_spectrale_band)

# plot luminance spectrale
figfile = 'Luminance_spectrale_band.png'
fig=plt.figure()
plt.plot(wavelength_band.to('um').value,luminance_photonique_spectrale_band.value,'r-') # 'r+'
plt.title('Spectral Luminance')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
plt.xlabel('Wavelength [um]')
plt.ylabel('Luminance [ph / (s sr m2 um)]')
plt.savefig(figfile)
plt.close(fig)
print('Figure saved in '+figfile)
# plot Transmission as a function of wavelength
figfile = 'Transmission_spectrale_band.png'
fig=plt.figure()
plt.plot(wavelength_band.to('um').value,trans_atm_spectrale_band,'r-') # 'r+'
plt.title('Transmision')# log10(total): '+str("{:.2f}".format(np.log10(radiance_total.to('W m-2 sr-1').value))))
plt.xlabel('Wavelength [um]')
plt.ylabel('Transmission []')
plt.savefig(figfile)
plt.close(fig)
print('Figure saved in '+figfile)

print('luminance_photonique_band: '+str(luminance_photonique_band))
print('trans_atm_band: '+str(trans_atm_band))


####### Estimation des énergies photoniques
# Photon flux calculations

# for debug purpose TODO: remove !!!!
luminance_photonique_band = 2.481E+17 * u.ph / (u.s*u.sr*u.m**2)

Flux_fond_hors_cible = (Gpix - Gcible).to('m2') * luminance_photonique_band * Topt
Flux_fond_avant_cible = Gcible.to('m2') * luminance_photonique_band * Topt
Flux_cible = epsilon_cible * cible_band_luminance * trans_atm_band * Gcible.to('m2') * Topt
Flux_pix_fond = Gpix * luminance_photonique_band * Topt
Flux_pix_cible = Flux_fond_hors_cible + Flux_fond_avant_cible + Flux_cible
Flux_utile = Flux_pix_cible - Flux_pix_fond

print('Flux_fond_hors_cible = '+str(Flux_fond_hors_cible))
print('Flux_fond_avant_cible = '+str(Flux_fond_avant_cible))
print('Flux_cible = '+str(Flux_cible))
print('Flux_pix_fond = '+str(Flux_pix_fond))
print('Flux_pix_cible = '+str(Flux_pix_cible))
print('Flux_utile = '+str(Flux_utile))


# Compute the number of photons. Unit=[ph]
N_transfert_max = (CsG * Delat_Vs / q  ).decompose() # unit=[]
N_phot_obscurite = (Iobsc / q * ti_opt).decompose() * u.ph # unit=[ph]
N_phot_fond_hors_cible = eta * Flux_fond_hors_cible * ti_opt.to('s') * u.sr # unit=[ph]
N_phot_fond_hors_cible = (eta * Flux_fond_avant_cible * ti_opt.to('s')).to('ph/sr') * u.sr# unit=[ph]
N_phot_cible = eta * Flux_cible * ti_opt.to('s') * u.sr # unit=[ph]
N_phot_pix_fond = eta * Flux_pix_fond * ti_opt.to('s') * u.sr + N_phot_obscurite # unit=[ph]
N_phot_estime_fond_et_cible = eta * Flux_pix_cible * ti_opt.to('s') * u.sr + N_phot_obscurite # unit=[ph]
N_phot_utile = eta * Flux_utile * ti_opt.to('s') * u.sr # unit=[ph]

# compute voltage
factor_V = q / CsG / u.ph
V_utile = (N_phot_utile * factor_V).to('V') # unit=[V]
V_obscurite = (N_phot_obscurite * factor_V).to('V')
V_pix_fond = (N_phot_pix_fond * factor_V).to('V')
V_estime_fond_et_cible = (N_phot_estime_fond_et_cible * factor_V).to('V')
V_pix_fond = (N_phot_pix_fond * factor_V).to('V')

# Compute noise. unit=[photo-electron rms]
sigma_obscurite = np.sqrt(N_phot_obscurite / u.ph)
sigma_phot_fond = np.sqrt(N_phot_pix_fond / u.ph)
sigma_estime_fond_et_cible = np.sqrt(N_phot_estime_fond_et_cible / u.ph)
# Johnson noise
sigma_johnson = (np.sqrt( 2*k_B*Temp_det * ti_opt.to('s') /  Rsh ) / q).decompose()
# total noise at photo-diode output
sigma_PV_cible = np.sqrt(sigma_estime_fond_et_cible**2 + sigma_johnson**2)
sigma_PV_fond = np.sqrt( sigma_phot_fond**2 + sigma_johnson**2)
# Read out noise
sigma_lec = (CsG * VSF / q).decompose()
nb_niv = 2**n_num
sigma_num = N_transfert_max / (nb_niv * np.sqrt(12))
# output noise
sigma_total_cible = np.sqrt( sigma_PV_cible**2 + sigma_lec**2 + sigma_num**2 ) 
sigma_total_fond = np.sqrt( sigma_PV_fond**2 + sigma_lec**2 + sigma_num**2 )
# equivalent voltage noise
factor_V_sigma = q / CsG
sigma_V_obscurite = (sigma_obscurite * factor_V_sigma).to('V')
sigma_V_lec = (sigma_lec * factor_V_sigma).to('V')
sigma_V_num = (sigma_num * factor_V_sigma).to('V')
sigma_V_total_cible = (sigma_total_cible * factor_V_sigma).to('V')
sigma_V_total_fond = (sigma_total_fond * factor_V_sigma).to('V')


# Several other quantities
# apparent lenght of target displacement
taille_file = V_cible_app.to('m/s') * ti_opt.to('s') # unit = [m]
# diffraction diameter
diam_diffraction = 2.44 * lambda_mean * Nopt # unit=[um]
# pixel size at target
a_det_cible = (a_det * Lobs / Fopt).decompose() # unit=[m])
# camera total FOV
theta_tot = (np.arctan(p_ech * nb_ech / Fopt)).to('deg') # unit=[deg]
# Current from background only
I_pix_fond = ((Flux_pix_fond * u.sr / u.ph) * q).to('A') + Iobsc

# compute signal to noise ratios
R_signal_util_sur_bruit = V_utile / np.sqrt(sigma_total_cible**2+sigma_total_fond**2) # unit=[V]
R_signal_util_sur_signal_fond_atmo = V_utile / (V_pix_fond - V_obscurite)

# output
print('R_signal_util_sur_bruit = '+str(R_signal_util_sur_bruit))
print('R_signal_util_sur_signal_fond_atmo = '+str(R_signal_util_sur_signal_fond_atmo))

