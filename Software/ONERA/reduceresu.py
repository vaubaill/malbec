import os
import glob
from shutil import copyfile

path = "C:\\Users\\Soho\\Desktop\\Jerem\\ONERA\\Pierre\\Rayjn2\\"
conf_file = 'Rayjn.data'
pattern = path + "*2409*-*.*" + '\\RESU\\'+ "*h*mn*" + '\\' + conf_file

listconf = glob.glob(pattern)
print('listconf: '+str(listconf))
print(len(listconf))

for conf in listconf:
    print('Now reading file: '+conf)
    data_file = os.path.dirname(conf) + '\\lum_Rayjn.xls'
    print('data_file: '+data_file)
    if not os.path.exists(data_file):
        msg = 'data_file: '+data_file+' does not exist'
        print(msg)
        raise IOError(msg)
    altset = False
    azset = False
    with open(conf, mode='r') as f:
        for line in f.readlines():
            if 'date' in line:
                (dd,hh) = line.split()[0:2]
                ddall = dd.split('/')
                hhall = hh.split(':')
                datestr = ddall[2]+ddall[1]+ddall[0]+'T'+hhall[0]+hhall[1]+'00_UT'
            elif 'altitude de l' in line:
                if not altset:
                    alt = line.split()[0].split('.')[0]
                    altset = True
            elif 'nithal et azimut ' in line:
                if not azset:
                    (zen,az) = line.split()[0:2]
                    azset = True
                    az = az.split('.')[0]
                    zen = zen.split('.')[0]
                    zenf = 90 - int(zen)
                    zenstr = str(zenf)
    print('date: '+datestr+'  hh: '+hh+ ' alt: '+alt+ ' zen: '+zen+' az: '+az)
    outname = path + 'lum_Rayjn-'+datestr+'-ALT'+alt+'AZ'+az+'EL'+zenstr+'.xls'
    print('Copying data_file '+data_file+' into outname : '+outname)
    copyfile(data_file, outname)
