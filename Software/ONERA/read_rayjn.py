#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 09:49:55 2024

@author: vaubaill
"""

from astropy.table import QTable
import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np
from astropy.constants import h,c

from rayjn_log import log

def read_rayjn_lum(lum_file,watt=False):
    """Read luminance file.

    Parameters
    ----------
    lum_file : string
        Luminance File created by Rayjn.
    watt : bolean, optional
        If True, provides the luminance in [W.m-2.sr-1.um-1']. Default is False.
    
    Returns
    -------
    wavelength : astropy.units.Quantity object.
        Wavelength.
    luminance : astropy.units.Quantity object.
        Luminance, in 

    """
    # Luminance type file
    log.info('Now reading luminance file '+lum_file)
    colstart = [0,26,46,74,97,130]
    colunits = (1/u.cm,u.W/u.m**2/u.sr*u.cm,u.ph/u.s/u.m**2/u.sr*u.cm,u.um,u.ph/u.s/u.m**2/u.sr/u.um,u.R/u.um)
    data = QTable.read(lum_file,format='ascii.fixed_width',data_start=2,
                       col_starts=colstart,
                       units=colunits)
    wavelength = data['Longueur d\'onde']
    luminance = data['Luminance_2']
    if watt:
         E = h*c / wavelength
         luminance = data['Luminance_2'] * E.to('J') / u.ph
         luminance = luminance.to('W.m-2.sr-1.um-1')
         #luminance = data['Luminance'] 
    return wavelength.to('um'),luminance

def plot_lum(lum_file,logplot=False,watt=False):
    """Plot luminance.

    Parameters
    ----------
    lum_file : string
        Luminance File created by Rayjn.
    logplot : boolean, optional
        If True, plot in log. Default is False

    Returns
    -------
    None.

    """    
    # plot luminance=f(wavelength)
    plotfile = lum_file.replace('.xls','.png')
    if watt:
        wavelength,luminance = read_rayjn_lum(lum_file,watt=True)
    else:
        wavelength,luminance = read_rayjn_lum(lum_file)
    fig=plt.figure()
    
    if logplot:
        plt.scatter(wavelength.to('um'),np.log10(luminance.value),color='cornflowerblue',s=1,marker='P')
    else:
        plt.scatter(wavelength.to('um'),luminance,color='cornflowerblue',s=1,marker='P')
    plt.title('Luminance vs lambda',color='indianred')
    plt.xlabel('Lambda [um]')
    plt.ylabel('Luminance ['+luminance.unit.to_string()+']')
    plt.legend(loc='lower right')
    plt.savefig(plotfile,dpi=300)
    plt.close(fig)
    log.info('Fig saved in '+plotfile)
    return 

def read_rayjn_comp(comp_file):
    """Read comp file.

    Parameters
    ----------
    comp_file : string
        Comp File created by Rayjn.

    Returns
    -------
    wavelength : astropy.units.Quantity object.
        Wavelength.
    transmission : astropy.units.Quantity object.
        transmission.

    """
    log.info('Now reading Comp file '+comp_file)
    # Comp type file
    colstart = [0,20,44,61,86,116,139,160,188,202,224,260,284,308]
    colnames = ['WvNum','transmission','thermique','diffusion multiple','diffusion simple','nightglow ','stellaire','fond de sol','totale','source exoatmos','source transmise','?0','?1','?2']
    data = QTable.read(comp_file,format='ascii.fixed_width',data_start=2,col_starts=colstart,names=colnames)
    data['WvNum'].info.unit = 1/u.cm
    wavelength = (1/data['WvNum']).to('um')
    transmission = data['transmission']
    luminance = data['totale'] * (u.W / (u.m**2 * u.sr)) * u.cm
    lambda_factor = data['WvNum'].to('cm-1') / wavelength.to('um') # unit=[1/cm.um]
    luminance_spectrale = (luminance * lambda_factor).to('W/sr.um.m2') 
    return wavelength.to('um'),transmission, luminance_spectrale

def plot_comp(comp_file,logplot=False):
    """Read comp file.

    Parameters
    ----------
    comp_file : string
        Comp File created by Rayjn.
    logplot : boolean, optional
        If True, plot in log. Default is False
    Returns
    -------
    None
    
    """
    # read data
    wavelength,transmission,luminance = read_rayjn_comp(comp_file)
    
    # plot transmission=f(wavelength)
    plotfile = comp_file.replace('.xls','_trans.png')
    fig=plt.figure()
    plt.scatter(wavelength.to('um'),transmission,color='cornflowerblue',s=1,marker='P')
    if logplot:
        plt.yscale('log')
    plt.title('Transmission vs lambda',color='indianred')
    plt.xlabel('Lambda [um]')
    plt.ylabel('Transmission')
    plt.legend(loc='lower right')
    plt.savefig(plotfile,dpi=300)
    plt.close(fig)
    log.info('Fig saved in '+plotfile)
    
    # plot luminance
    plotfile = comp_file.replace('.xls','_lum.png')
    fig=plt.figure()
    if logplot:
        plt.scatter(wavelength.to('um'),np.log10(luminance.value),color='cornflowerblue',s=1,marker='P')
    else:
        plt.scatter(wavelength.to('um'),luminance,color='cornflowerblue',s=1,marker='P')
    plt.title('Luminance vs lambda',color='indianred')
    plt.xlabel('Lambda [um]')
    plt.ylabel('Luminance ['+luminance.unit.to_string()+']')
    plt.legend(loc='lower right')
    plt.savefig(plotfile,dpi=300)
    plt.close(fig)
    log.info('Fig saved in '+plotfile)
    
    return