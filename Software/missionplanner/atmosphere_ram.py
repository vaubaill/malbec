import os
import logging
import pygrib
import numpy as np
from scipy import interpolate
import astropy.units as u

from missionplanner.utils import log

def compute_g(lat):
    """Compute acceleration of gravity at a given latitude.
    
    Parameters
    ----------
    lat : astropy.units.Quantity object
        latitude in deg.
    
    Returns
    -------
    g : astropy.units.Quantity object
        acceleration of gravity [m/s^2]
    
    
    """
    # compute acceleration of gravity at latitude, according to WGS84 formula
    # see: https://en.wikipedia.org/wiki/Gravitational_acceleration
    # acceleration of gravity at the equator
    g_equ = 9.780 * u.m/u.s**2
    # acceleration of gravity at the pole
    g_pol = 9.8321849378*u.m/u.s**2
    # acceleration of gravity at 45deg lat
    g_45 = 9.806 *u.m/u.s**2
    
    latitude = lat.to('rad')
    
    # compute g at latitude lat
    g = g_45-0.5*(g_pol-g_equ)*np.cos(2.0*latitude)
    return g


def closest_level(level,list_level,method='absolute'):
    """Get the closest level from a list of level.
    
    The "method" option allows one to get the closest level
    according to: 'close': closest in absolute value,
    'upper': upper level, 'lower': lower level.
    
    Parameters
    ----------
    level : astropy.units.Quantity object.
        Level for which we want the closest level
    list_level : list of astropy.units.Quantity
        list of levels
    method : string, optional
        method to retrieve the level.
        Default is 'absolute'
    
    Returns
    -------
    close_level : astropy.units.Quantity Object
        Closest level in list_level from level
    
    """
    diff_level = []
    for lvl in list_level:
        diff_level.append(level - lvl)
    
    if method=='absolute':
        close_level = list_level[np.argmin(np.abs(diff_level))]
    
    if method=='upper':
        upper_idx = np.where(np.asarray(diff_level) > 0.0)
        #log.debug('upper_idx='+str(upper_idx))
        upper_level = list_level[upper_idx]
        #log.debug('upper_level='+str(upper_level))
        close_level = np.max(upper_level)
    
    if method=='lower':
        lower_idx = np.where(np.asarray(diff_level) < 0.0)
        log.debug('lower_idx='+str(lower_idx))
        lower_level = list_level[lower_idx]
        log.debug('lower_level='+str(lower_level))
        close_level = np.min(lower_level)
    
    log.debug('close_level='+str(close_level))
    
    return close_level

def alt2geoalt(alt,lat):
    """Convert altitude in [m] into Geopotential altitude.
    
    Parameters
    ----------
    alt : astropy.units.Quantity object
        Altitude
    lat : astropy.coordinates.Angle Object
        Latitude
    
    Returns
    -------
    alt_geo : astropy.units.Quantity object
        Geopotential altitude [m2/s2].
    
    """
    g = compute_g(lat)
    alt_geo = alt.to('m') * g
    return alt_geo

def geoalt2alt(geoalt,lat):
    """Convert geopotential altitude into altitude.
    
    Parameters
    ----------
    geoalt : astropy.units.Quantity Object
        Geopotential altitude [[m2/s2]
    lat : astropy.coorinates.Angle Object
        Latitude
    
    Returns
    -------
    alt : astropy.units.Quantity Object.
        Altitude
    
    
    """
    g = compute_g(lat)
    alt = geoalt.to('m2/s2') / g
    return alt

def get_round(x,step):
    """rounds up x modulo step.
    
    Parameters
    ----------
    x : float
        Number to be rounded.
    step : float
        step about which x is to be rounded up.
    
    Returns
    -------
    xround :  float
        rounded x modulo step.
    
    """
    i = x//step                 # integer division 
    xround= round(i*step,3)     # return 3 digits   
    return xround

def grb_to_grid(grb_obj):
    """Make a grid/cube out of a grib Object.
    
    Takes a single grb object containing multiple levels.
    Assumes same time, pressure levels. Compiles to a cube.
    
    Parameters
    ----------
    grb_obj : pygrib.open(filename).select(name='Cloud mixing ratio')
    
    Returns
    -------
    cube_dict : dict
        cube_dict['data'] : numpy.array of data
            Data, sorted in lat,lon (lon is changing the most rapidly).
        cube_dict['units'] : astropy.units.Unit object
            unit of data.
        cube_dict['levels'] : numpy.array
            levels for each data.
    
    """
    n_levels = len(grb_obj)
    levels = np.array([grb_element['level'] for grb_element in grb_obj])
    indexes = np.argsort(levels)[::-1] # highest pressure first
    cube = np.zeros([n_levels, grb_obj[0].values.shape[0], grb_obj[1].values.shape[1]])
    for i in range(n_levels):
        cube[i,:,:] = grb_obj[indexes[i]].values
    cube_dict = {'data' : cube, 'units' : grb_obj[0]['units'],
                 'levels' : levels[indexes]}
    return cube_dict


def make_alt_cube(cube_geolat_dict,latitudes):
    """Make Altitude cube.
    
    Parameters
    ----------
    cube_geolat_dict : dict
        Geolatitude created with the grb_to_grid function.
    latitudes : astropy.units.Quantity object.
        Latitude of geolatitudes.
    
    Returns
    -------
    cube_dict : dict
        Altitude dictionnary organized as follow:
        cube_dict['data'] : numpy.array of data
            Data, sorted in lat,lon (lon is changing the most rapidly).
        cube_dict['units'] : astropy.units.Unit object
            unit of data.
        cube_dict['levels'] : numpy.array
            levels for each data.
    
    """
    # convert geo_altitude cube into altitude cube
    cube_alt = np.array([])
    for (cz,lvl) in zip(cube_geolat_dict['data'],cube_geolat_dict['levels']):
        alts = geoalt2alt(cz.flatten()*u.m**2/(u.s)**2,latitudes.to('deg')).to('m').value
        cube_alt = np.append(cube_alt,alts)
    cube_alt = np.reshape(cube_alt,cube_geolat_dict['data'].shape)
    cube_dict = {'data' : cube_alt, 'units' : u.m,
                 'levels' : cube_geolat_dict['levels']}
    return cube_dict

######################################
#
# Definition of an Atmosphere Object.
#
######################################

class Atmosphere (object):
    """Represent the atmoshere state.
    
    The whole data of a grib-type file are loaded in numpy.array.
    
    Parameters
    ----------
    gribfile : string
        name of the grib type weather file where data are taken from.
    grbs : pygrib.open object
        grib data
    params : list of strings
        Name of parameters in the grib file.
    levels : list of integers
        levels in the grib data.
    nlevel : integer
        number of levels in the grib data.
    min_level : 
        Minimum lavel in grib data.
    max_level : 
        Maximum level in grib data
    reglevel : list of float
        Regularly spaced levels (used for interpolation).
    idx_name : pyrgib.index object
        index to be seeked.
    allz : pygrib.gribmessage object
        gribmessages of all Z (isobar) parameters.
    iDirectionIncrementInDegrees : astropy.units.Quantity object
        Direction of increment of first dimension [deg]
    jDirectionIncrementInDegrees : astropy.units.Quantity object
        Direction of increment of second dimension [deg]
    latitudes : astropy.units.Quantity object
        latitudes in data [deg]
    longitudes : astropy.units.Quantity object
        longitudes in data  [deg]
    longitudeOfLastGridPointInDegrees : astropy.units.Quantity object
        Longitude of last grid point [deg]
    longitudeOfFirstGridPointInDegrees : astropy.units.Quantity object
        Longitude of first grid point [deg]
    latitudeOfLastGridPointInDegrees : astropy.units.Quantity object
        Latitude of last grid point [deg]
    latitudeOfFirstGridPointInDegrees : astropy.units.Quantity object
        Latitude of first grid point [deg]
    min_geo_alt : astropy.units.Quantity object
         Minimum geo-latitude [m2/s2]
    max_geo_alt : astropy.units.Quantity object
         Maximum geo-latitude [m2/s2]
    max_alt : astropy.units.Quantity object
         Maximum altitude [m]
    min_alt : astropy.units.Quantity object
         Minimum altitude [m]
    cube : cube.Cube Object or flat_cube.FlatCube object.
        3D data cube.
    cube_is_flat : logical
        True if cube is a flat_cube.FlatCube object, False otherwise.
    pressure: astropy.units.Quantity object
        Pressure in [Pa]
    temperature: astropy.units.Quantity object
        Temperature in [K]
    U: astropy.units.Quantity object
        zonal wind, positive in the West [m/s]
    V: astropy.units.Quantity object
        meridional wind, positive to the South [m/s]
    W: astropy.units.Quantity object
        vertical wind [m/s]
    
    """
    gribfile = None
    grbs = None
    cube = None
    params = None
    levels = None
    nlevel = None
    min_level = None
    max_level = None
    P = 0 *u.Pa
    T = 0 *u.K
    U  = 0 *u.m/u.s     # zonal wind, positive in the West (x2-check)
    V  = 0 *u.m/u.s     # meridional wind, positive to the South (x2-check)
    W  = 0 *u.m/u.s
    cube_is_flat = False
    
    def __init__(self,grib_file,log_file='atmosphere.log',grb_params=['z','t','u','v','w']):
        """Instanciates an Atmoshere Object.
        
        Parameters
        ----------
        grib_file : string
            grib format weather file.
        log_file : string, optional
            log file name. Default is 'atmosphere.log'.
        
        Returns
        -------
        None : 
            Atmosphere Object is instanciated.
        
        """
        # check log_file
        if log_file:
            self.set_log(log_file)
            log.debug('log file: '+log_file+' ok')
        
        # check grib file
        self.gribfile = grib_file
        if not os.path.exists(self.gribfile):
            raise IOError('*** FATAL ERROR: file '+self.gribfile+" does not exist")
        log.debug('Weather data: '+self.gribfile+' was found')
        
        # open grib file
        self.grbs = pygrib.open(self.gribfile)
        
        # check that the file contains the parameters needed for maballtrap
        self.check_data_parameters(grb_params=grb_params)
        
        # get boundaries
        self.set_boudaries()
        
        # make interpolators
        self.make_interpolators()
        
        return
    
    def set_log(self,log_file):
        """Set Logger object.
        
        Parameters
        ----------
        log_file : string
            Log file name.
        
        Returns
        -------
        None.
        
        """
        # create FileHandler
        hdlr = logging.FileHandler(log_file)
        hdlr.setLevel(logging.DEBUG)
        hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
        # log file name
        log.debug('Log file: '+log_file)
        return
    
    def check_data_parameters(self,grb_params=['z','t','u','v','w']):
        """Check that the data are the expected ones, i.e. Z, T, U, V, W.
        
        Sets self.params and self.levels.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # initializations
        allnames=[]
        alllevels=[]
        param_check = np.sort(grb_params)
        
        # set pointer to start of file
        self.grbs.seek(0)
        # get the name of all parameters
        for grb in self.grbs:
            allnames.append(grb.shortName)
            alllevels.append(grb.level)
        self.grbs.seek(0)
        
        # sort allnames and alllevels
        self.params = np.unique(np.sort(allnames))
        self.levels = np.unique(np.sort(alllevels))
        self.nlevel = len(self.levels)
        self.min_level = np.min(self.levels)
        self.max_level = np.max(self.levels)
        log.debug('self.levels = '+str(self.levels))
        log.debug('self.params = '+' '.join(self.params))
        log.debug('param_check = '+' '.join(param_check))
        
        # check that the parameters are the ones expected for MaBallTraP
        check_param = (self.params==param_check)
        log.debug('check_param = '+str(check_param))
        if not all(check_param):
            msg = '*** FATAL ERROR: weather parameters MUST be '+' '.join(param_check)+' not '+' '.join(self.params)
            log.error(msg)
            raise IOError(msg)
        
        return
    
    
    def set_boudaries(self):
        """Get and set parameters boundaries for later process.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # create index on shortName
        self.idx_name=pygrib.index(self.gribfile,'shortName')
        # select geopotential data for later use
        self.allz = self.idx_name.select(shortName='z')
        
        # set internal parameters
        self.iDirectionIncrementInDegrees = self.allz[0].iDirectionIncrementInDegrees* u.deg
        self.jDirectionIncrementInDegrees = self.allz[0].jDirectionIncrementInDegrees* u.deg
        self.latitudes = self.allz[0].latitudes* u.deg
        self.longitudes = self.allz[0].longitudes* u.deg
        self.longitudeOfLastGridPointInDegrees = self.allz[0].longitudeOfLastGridPointInDegrees* u.deg
        self.longitudeOfFirstGridPointInDegrees = self.allz[0].longitudeOfFirstGridPointInDegrees* u.deg
        self.latitudeOfLastGridPointInDegrees = self.allz[0].latitudeOfLastGridPointInDegrees* u.deg
        self.latitudeOfFirstGridPointInDegrees = self.allz[0].latitudeOfFirstGridPointInDegrees* u.deg
        
        # check Longitude for > 180 deg values
        if self.longitudeOfLastGridPointInDegrees>180.0*u.deg:
            self.longitudeOfLastGridPointInDegrees = self.longitudeOfLastGridPointInDegrees-360.0*u.deg
        if self.longitudeOfFirstGridPointInDegrees>180.0*u.deg:
            self.longitudeOfFirstGridPointInDegrees = self.longitudeOfFirstGridPointInDegrees-360.0*u.deg
            
        
        # retrieve max geopotential altitudes
        allmaxz = []
        allminz = []
        for z in self.allz:
            allmaxz.append(z.maximum)
            allminz.append(z.minimum)
        # set min/max geoaltitude
        self.min_geo_alt = np.min(allmaxz) * (u.m/u.s)**2 # take the minimum of maximum altitudes to be sure to create flat_cube if max altitude is reached
        self.max_geo_alt = np.max(allminz) * (u.m/u.s)**2 # take maximum of minimum altitudes for same reason
        self.max_alt = geoalt2alt(self.max_geo_alt,(self.latitudeOfLastGridPointInDegrees+self.latitudeOfFirstGridPointInDegrees)/2.0) -10.0*u.m
        self.min_alt = geoalt2alt(self.min_geo_alt,(self.latitudeOfLastGridPointInDegrees+self.latitudeOfFirstGridPointInDegrees)/2.0) +10.0*u.m
        
        log.debug('self.iDirectionIncrementInDegrees = '+str(self.iDirectionIncrementInDegrees))
        log.debug('self.jDirectionIncrementInDegrees = '+str(self.jDirectionIncrementInDegrees))
        log.debug('self.longitudeOfLastGridPointInDegrees  = '+str(self.longitudeOfLastGridPointInDegrees ))
        log.debug('self.longitudeOfFirstGridPointInDegrees = '+str(self.longitudeOfFirstGridPointInDegrees))
        log.debug('self.latitudeOfLastGridPointInDegrees   = '+str(self.latitudeOfLastGridPointInDegrees))
        log.debug('self.latitudeOfFirstGridPointInDegrees  = '+str(self.latitudeOfFirstGridPointInDegrees ))
        log.debug('self.min_level = '+str(self.min_level))
        log.debug('self.max_level = '+str(self.max_level))
        log.debug('self.max_geo_alt = '+str(self.max_geo_alt))
        log.debug('self.min_geo_alt = '+str(self.min_geo_alt))
        
        return
    
    def check_boundaries(self,lon,lat,alt):
        """Check that the location for which the data are asked are within data range.
        
        A Value exception is raise if location is out of range.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            longitude coordinate
        lat : astropy.coordinates.Angle Object
            latitude coordinate
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        None.
        
        """
        # set check=True by default
        check = True
        msg = 'all ok'
        
        # check longitude
        if (lon < self.longitudeOfFirstGridPointInDegrees):
            msg = 'Longitude '+str(lon)+' is out of range. lon<'+str(self.longitudeOfFirstGridPointInDegrees)
            check = False
        # else:
        #     log.debug('Lon='+str(lon)+' is well > min Lon='+str(self.longitudeOfFirstGridPointInDegrees))
        
        if (lon > self.longitudeOfLastGridPointInDegrees):
            msg = 'Longitude '+str(lon)+' is out of range. lon>'+str(self.longitudeOfLastGridPointInDegrees)
            check = False
        # else:
        #     log.debug('Lon='+str(lon)+' is well < max Lon='+str(self.longitudeOfLastGridPointInDegrees))
       
        # check Latitude
        if (lat>self.latitudeOfFirstGridPointInDegrees):
            msg = 'Latitude '+str(lat)+' is out of range. lat>'+str(self.latitudeOfLastGridPointInDegrees)
            check = False
        # else:
        #     log.debug('Lat='+str(lat)+' is well >'+str(self.latitudeOfFirstGridPointInDegrees))
        if (lat<self.latitudeOfLastGridPointInDegrees):
            msg = 'Latitude '+str(lat)+' is out of range. lat<'+str(self.latitudeOfLastGridPointInDegrees)
            check = False
        # else:
        #     log.debug('Lat='+str(lat)+' is well <'+str(self.latitudeOfLastGridPointInDegrees))
        
        # if check is False raise exception
        if not check:
            raise UnboundLocalError(msg)
            
        return
    
    def check_altitude_boundaries(self,alt):
        """Check wheather the requested altitude lies within the altitude range.
        
        Parameters
        ----------
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        check : boolean
            True if the requested location is within the grib file.
            False otherwise.
        comment : string
            If check is True, set to empty string. Otherwise, if alt<minimum(alt)
            then set to 'low' ; if alt>max(alt), set to 'high'.
        
        """
        # set check=True by default
        check = True
        comment = ''
        # check altitude
        if (alt < self.min_alt):
            comment = 'Altitude '+str(alt)+' is out of range. alt<'+str(self.min_alt)
            check = False
        if (alt > self.max_alt):
            comment = 'Altitude '+str(alt)+' is out of range. alt>'+str(self.max_alt)
            check = False
        
        return (check,comment)
    
    
    def make_interpolators(self):
        """Make a data cube.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
            The self.interpolators objects are created.
        
        """
        # select data from parameter
        grb_t = self.grbs.select(shortName='t')
        grb_u = self.grbs.select(shortName='u')
        grb_v = self.grbs.select(shortName='v')
        grb_w = self.grbs.select(shortName='w')
        grb_z = self.grbs.select(shortName='z')
        
        # make grib/cube for each parameter
        self.cube_t = grb_to_grid(grb_t)
        self.cube_u = grb_to_grid(grb_u)
        self.cube_v = grb_to_grid(grb_v)
        self.cube_w = grb_to_grid(grb_w)
        self.cube_z = grb_to_grid(grb_z)
        
        # make altitude cube
        self.cube_alt = make_alt_cube(self.cube_z,self.latitudes)
        
        # make flat data
        flt_lats = np.tile(self.latitudes.to('deg').value,len(self.cube_t['levels']))
        flt_lons = np.tile(self.longitudes.to('deg').value,len(self.cube_t['levels']))
        flt_alts = self.cube_alt['data'].flatten()
        #flt_geoa = self.cube_z['data'].flatten()
        flt_lvls = np.repeat(self.cube_t['levels'],self.latitudes.shape)
        
        # make grids from flat data
        grid_alt = np.asarray([flt_lons,flt_lats,flt_alts]).transpose()
        #grid_geo = np.asarray([flt_lons,flt_lats,flt_geoa]).transpose()
        
        # debug messages
        #log.debug('grid_alt: '+str(grid_alt))
        #log.debug('cube_t[data]: '+str(self.cube_t['data']))
        #log.debug('cube_alt[data]: '+str(self.cube_alt['data']))
        log.debug('grid.shape: '+str(grid_alt.shape))
        log.debug('cube_t[data].shape: '+str(self.cube_t['data'].shape))
        log.debug('cube_alt[data].shape: '+str(self.cube_alt['data'].shape))
        
        # make interpolator objects
        self.Tinterpolator = interpolate.LinearNDInterpolator(grid_alt,self.cube_t['data'].flatten())
        self.Uinterpolator = interpolate.LinearNDInterpolator(grid_alt,self.cube_u['data'].flatten())
        self.Vinterpolator = interpolate.LinearNDInterpolator(grid_alt,self.cube_v['data'].flatten())
        self.Winterpolator = interpolate.LinearNDInterpolator(grid_alt,self.cube_w['data'].flatten())
        self.Zinterpolator = interpolate.LinearNDInterpolator(grid_alt,self.cube_z['data'].flatten())
        self.Pinterpolator = interpolate.LinearNDInterpolator(grid_alt,flt_lvls)
        
        log.debug('Interpolator object successfully created.')
        
        return
    
    
    def get_data(self,lon,lat,alt,check=False):
        """Get the data for a given location.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            longitude coordinate
        lat : astropy.coordinates.Angle Object
            latitude coordinate
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        Z : astropy.units.Quantity object
            geopotential altitude [m]
        P : astropy.units.Quantity object
            pressure in Hectopascal [hPa]
        T : astropy.units.Quantity object
            temperature [K]
        U : astropy.units.Quantity object
            zonal wind, positive in the West [m/s]
        V : astropy.units.Quantity object
            meridional wind, positive to the South [m/s]
        W : astropy.units.Quantity object
            vertical wind  [m/s]
        
        """
        # check longitude, latitude boundaries
        self.check_boundaries(lon,lat,alt)
        # note: if the previous line is not passed the program raises ValueException.
        
        # check altitude boundaries
        (check_alt,comment) = self.check_altitude_boundaries(alt)
        
        # convert Quantity into float
        data_in = np.array([lon.to('deg').value,lat.to('deg').value,alt.to('m').value])
        
        # now interpolate the cube data at chosen locations
        P = self.Pinterpolator(data_in)[0] * u.hPa                # Pressure in [Pa]
        Z = self.Zinterpolator(data_in)[0] * (u.m/u.s)**2.0       # geopotential altitude of isobar [m*G]=[m^2/s^2]
        T = self.Tinterpolator(data_in)[0] * u.K                  # Temperature [K]
        U = self.Uinterpolator(data_in)[0] * u.m/u.s              # zonal wind, positive in the West [m/s]
        V = self.Vinterpolator(data_in)[0] * u.m/u.s              # meridional wind, positive to the South [m/s]
        W = self.Winterpolator(data_in)[0] * u.m/u.s              # vertical wind  [m/s]
        
        """
        log.debug('Z = '+str(Z))
        log.debug('P = '+str(P))
        log.debug('T = '+str(T))
        log.debug('U = '+str(U))
        log.debug('V = '+str(V))
        log.debug('W = '+str(W))
        """
        
        if not self.check_value([Z,P,T,U,V,W],stopifnan=False):
            if alt < self.min_alt:
                P = np.max(self.levels) * u.hPa
                Z = np.min(self.cube_z['data']) * (u.m/u.s)**2.0
                T = np.max(self.cube_t['data']) * u.K
                U = 0.0*u.m/u.s
                V = 0.0*u.m/u.s
                W = 0.0*u.m/u.s
            if alt > self.max_alt:
                P = np.min(self.levels) * u.hPa
                Z = np.max(self.cube_z['data']) * (u.m/u.s)**2.0
                T = np.min(self.cube_t['data']) * u.K
                U = 0.0*u.m/u.s
                V = 0.0*u.m/u.s
                W = 0.0*u.m/u.s
        
        # re-check values
        self.check_value([Z,P,T,U,V,W])
        
        return (Z,P,T,U,V,W)
    
    def check_value(self,ZPTUVW,names=['Z','P','T','U','V','W'],stopifnan=True):
        """Check numerical values of array.
        
        Raise ValueError if NaN is found.
        
        Parameters
        ----------
        ZPTUVW : list of astropy.units.Quantity objects.
            list of quantities to check.
        names : list of strings
            List of name of physical quantities.
        stopifnan : logical
            If True and ZPTUVW contains at least one NaN value, a ValueError is raised.
            Otherwise a simple warning is produced.
            Default is True.
        
        Returns
        -------
        None.
        
        """
        for (Qty,nam) in zip(ZPTUVW,names):
            if np.isnan(Qty).any():
                msg = '*** Quantity '+nam+' is NaN'
                log.warning(msg)
                if stopifnan:
                    log.error(msg)
                    raise ValueError(msg)
        
        return
    
    def get_windPT(self,lon,lat,alt):
        """Get the data for a given location.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            longitude coordinate
        lat : astropy.coordinates.Angle Object
            latitude coordinate
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        P : astropy.units.Quantity object
            pressure in Hectopascal [hPa].
        T : astropy.units.Quantity object
            temperature [K]
        W : astropy.units.Quantity object
            wind 3-vector [m/s] DIRECTION WHERE THE WIND IS GOING TO (not where it's coming form)
            U: zonal wind, positive in the East [m/s]
            V: meridional wind, positive to the North [m/s]
            W: vertical wind positive towards the Zenith [m/s]
        
        """
        # check boundaries
        (Z,P,T,U,V,W) = self.get_data(lon,lat,alt)
        
        # get the wind vector in form of a 3-Dimensional array in direction of the fall
        wind_3D = np.stack([-U, -V, W]).value
        
        return (wind_3D*u.m/u.s,P,T)
