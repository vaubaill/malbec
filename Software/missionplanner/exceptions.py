#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 08:53:22 2021

@author: vaubaill
"""

class MissionPlanError(Exception):
    """Base class for exceptions in this module.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    None.
    
    """
    pass

class NoGroundDataError(MissionPlanError):
    """Exception raised when missionplanner is run in an area where no IGN data
    is available.

    Attributes:
        message: string
            Explanation of the error
    """

    def __init__(self,message):
        self.message = message
        self.msg = message
