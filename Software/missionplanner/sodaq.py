"""SODAQ card communcation tools.

Authors
-------
J. Vaubaillon, IMCCE, 2019

Project
-------
The MALBEC project.

Version
-------
1.0


"""

import os
import sys
import time
import serial
import subprocess
import logging
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
import numpy as np
from astropy.coordinates import EarthLocation
from configparser import ConfigParser, ExtendedInterpolation
import missionplan

from missionplanner.utils import log


# get MissionPlanner configuration file from argument
def launch_sodaq(config_file,test_mode,port='/dev/ttyAMA0',baud=9600):
    """Launches the communication with SODAQ card and run mission planner when needed.
    
    Parameters
    ----------
    config_file : string
        Configuration file for MissionPlanner1.1 object.
    test_mode : boolean
        If True, MissionPlanner is not run and a dummy position is sent back to SODAQ card.
    port : string, optional
        Serial port name. Default is RaspBerry-pi zero '/dev/ttyAMA0'.
    baud : integer, optional
        Serial port baud rate. Default is 9600.
    
    """
    if not os.path.exists(config_file):
        msg = '*** FATAL ERROR: config file '+config_file+' does not exist'
        raise IOError(msg)
    
    # create ConfigParser Object
    config = ConfigParser(interpolation=ExtendedInterpolation())
    # read configuration file
    config.read(config_file)
    # set outut file (full) name
    trajectory_rlt_file = config['CAMPAIGN']['trajectory_rlt_file']
    log.info('Trajectory realtime position file: '+trajectory_rlt_file)
    
    # create MissionPlanner Object
    if not test_mode:
        log.debug('Creating MissionPlanner object')
        missplan = missionplan.MissionPlanner(config_file)
    
    
    # Open serial port
    log.debug('opening port: '+port+' at baud rate: '+str(baud))
    serial_obj = serial.Serial(port,baud)
    
    # initialization: sends back theincoming message to make sure everything is working fine.
    log.debug('Initialization: waiting for first message to be received')
    (time_sodaq,lon_sodaq,lat_sodaq,alt_sodaq) = get_position_from_sodaq(serial_obj)
    log.debug('Message received: bouncing it back.')
    success_send = send_land_position(serial_obj,time_sodaq,lon_sodaq,lat_sodaq,alt_sodaq)
    log.debug('success_send='+str(success_send))
    
    # set number of positions from sodaq required before MissionPlanner launch can be decded
    npos = 4
    
    
    try:
        # ============== TEST CASE ==============
        if test_mode:
            # make infinite loop
            while True:
                # empty port in order not to mess up with previous frames
                serial_obj.flushInput()
                
                # get n positions before you can compute the velocity
                get_n_push_positions(serial_obj,trajectory_rlt_file,npos=npos)
                
                # decide if it is time to launch MissionPlanner
                go = launch_MissionPlanner(trajectory_rlt_file)
                log.debug('go: '+str(go))
                
                # create dummy landing position
                land_time=Time('2019-10-24T08:51:35.000',format='isot')
                land_loc = EarthLocation(lon=-0.200*u.deg,lat=43.700*u.deg,height=80.0*u.m)
                log.debug('land_t: '+land_time.isot)
                log.debug('land_lat: '+str(land_loc.lat.to('deg')))
                log.debug('land_lon: '+str(land_loc.lat.to('deg')))
                log.debug('land_lat: '+str(land_loc.height.to('m')))
                
                # send landing site to SODAQ card
                success_send = send_land_position(serial_obj,land_time,land_loc.lat,land_loc.lon,land_loc.height)
                log.debug('success_send='+str(success_send))
        # ============== END OF TEST CASE ==============
        
        # ============== IN FLIGHT CASE ==============
        # enter infinite loop
        while True:
            # empty port in order not to mess up with previous frames
            serial_obj.flushInput()
            
            # get n positions before you can compute the velocity
            get_n_push_positions(serial_obj,trajectory_rlt_file,npos=npos)
            
            # decide if it is time to launch MissionPlanner
            go = launch_MissionPlanner(trajectory_rlt_file)
            log.debug('go: '+str(go))
            
            if (go):
            # launches MissionPlanner
                log.debug('Now launches Mission Planner')
                (land_t,land_loc) = missplan.launch()
                log.debug('land_t: '+land_t.isot)
                log.debug('land_lat: '+str(land_loc.lat.to('deg')))
                log.debug('land_lon: '+str(land_loc.lat.to('deg')))
                log.debug('land_lat: '+str(land_loc.height.to('m')))
            
            # send landing site to SODAQ
            success_send = send_land_position(serial_obj,land_t,land_loc.lat,land_loc.lon,land_loc.height)
            log.debug('success_send='+str(success_send))
            
    except KeyboardInterrupt:
        serial_obj.close()
    except:
        msg = '*** FATAL ERROR: something went wrong...'
        log.error(msg)
    
    
    
    return

def get_n_push_positions(serial_obj,out_file,npos=4):
    """Get several positions and pushes them into output file.
    
    Parameters
    ----------
    serial_obj : serial.Serial object
        Serial object with open UART port.
    out_file : string
        Output file where the positions will be appended.
    npos : integer, optional
        Number of positions to append to output file. Default is 4.
    
    """
    # get n positions before you can compute the velocity
    for iget in np.arange(npos):
        # get latest position
        log.debug('getting position from sodaq')
        (time_sodaq,lon_sodaq,lat_sodaq,alt_sodaq) = get_position_from_sodaq(serial_obj)
        
        # puts latest position into output file
        log.debug('put last position into '+out_file)
        put_latest_position_into_file(out_file,time_sodaq,lon_sodaq,lat_sodaq,alt_sodaq)
    
    return

def get_position_from_sodaq(serial_obj):
    """Get the current position from SODAQ arduino-like card.
    
    Parameters
    ----------
    serial_obj : serial.Serial object
        Serial object with open UART port.
    
    Returns
    -------
    time_sodaq : astrop.time.Time object
        Time
    lon_sodaq : astropy.units.Quantity object
        Longitude in deg
    lat_sodaq : astropy.units.Quantity object
       Latitude in deg
    alt_sodaq : astropy.units.Quantity object
        Altitude in m
    """
    # communication with SODAQ card
    sodaq_msg = serial_obj.readline()
    log.debug('msg: '+str(sodaq_msg))
    
    # example of SODAQ message:
    # b'4512098765,212387654,1200,2019-10-24T08:51:35.\n'
    # explode sodaq message to retrieve each parameter
    sep = ','
    [sodaq_lat,sodaq_lon,sodaq_alt,sodaq_time] = str(sodaq_msg).split("'")[1][:-5].split(sep)
    sodaq_time = sodaq_time.split('\n')[0] + '000'
    """
    log.debug('sodaq_time='+str(sodaq_time))
    log.debug('sodaq_lat='+sodaq_lat)
    log.debug('sodaq_lon='+sodaq_lon)
    log.debug('sodaq_alt='+sodaq_alt)
    """
    # convert time into astropy.time.Time object
    time_sodaq = Time(sodaq_time,format='isot')
    # convert lon,lat, alt into an astropy.units.Quantity object
    lat_sodaq = float(sodaq_lat) / 1.0E+07 * u.deg
    lon_sodaq = float(sodaq_lon) / 1.0E+07 * u.deg
    alt_sodaq = float(sodaq_alt) / 1.0E+03 * u.m
    
    #  returns latest position
    return (time_sodaq,lon_sodaq,lat_sodaq,alt_sodaq)


def put_latest_position_into_file(filename,time_sodaq,lon_sodaq,lat_sodaq,alt_sodaq):
    """Put latest position at the end of a trajectory file.
    
    Parameters
    ----------
    filename : string
        output file name (full path)
    time_sodaq : astrop.time.Time object
        Time
    lon_sodaq : astropy.units.Quantity object
        Longitude in deg
    lat_sodaq : astropy.units.Quantity object
       Latitude in deg
    alt_sodaq : astropy.units.Quantity object
        Altitude in m
    
    Returns
    -------
    None.
    
    """
    # double check existence of file and set write mode
    if not os.path.exists(filename):
        openmode = 'w'
        log.info('File '+filename+' will be created')
    else:
        openmode = 'a+'
    
    # write into file
    try:
        with open(filename,openmode) as f:
            f.write(time_sodaq.isot+' '+str(lon_sodaq.to('deg').value)+' '+str(lat_sodaq.to('deg').value)+' '+str(alt_sodaq.to('m').value)+'\n')
            log.debug('latest position saved in '+filename)
    # handle all possible errors
    except(IOError):
        msg = '*** FATAL ERROR: impossible to save into '+filename
        log.error(msg)
        raise IOError(msg)
    except(ValueError):
        msg = '*** Value Error: double check every variable.'
        log.error('time = '+str(time_sodaq))
        log.error('lon = '+str(lon_sodaq))
        log.error('lat = '+str(lat_sodaq))
        log.error('alt = '+str(alt_sodaq))
        raise ValueError(msg)
    except:
        msg = '*** FATAL ERROR: something wrong happened///'
        raise ValueError(msg)
    
    return

def tail(filename,n,offset=0):
    """Get the last n lines of a file
    
    Parameters
    ----------
    filename : file object
        File for whic we want the last n lines.
    n : integer
        number of lines
    offset : integer
        Offset.
    
    Returns
    -------
    lines : list of strings
        Last n lines of the file.
    """
    proc = subprocess.Popen(['tail', '-n', n + offset, filename], stdout=subprocess.PIPE)
    lines = proc.stdout.readlines()
    return lines[:,-offset]

def read_sodaq_traj(trajectory_rlt_file,n=None):
    """Read SODAQ trajectory file.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file.
    n : integer, optional
        If not None, read the latest n lines of trajectory_rlt_file.
        Default is None.
    
    Returns
    -------
    data : astropy.table.QTalbe
        All data in trajectory_rlt_file.
    
    """
    log.debug('Reading: '+trajectory_rlt_file)
    # set n lies to read
    if n:
        outf = 'junk'
        proc = subprocess.Popen(['tail', '-n', n, trajectory_rlt_file,' > ',outf], stdout=subprocess.PIPE)
        file_to_read = outf
    else:
        file_to_read = trajectory_rlt_file
    # read trajectory file and set units
    data = QTable.read(trajectory_rlt_file,format='ascii',names=['Time','lon','lat','alt'])
    data['lon'].unit=u.deg
    data['lat'].unit=u.deg
    data['alt'].unit=u.m
    # convert time into Time object
    time = Time(data['Time'],format='isot')
    data['Time'] = time
    return data

def launch_MissionPlanner(trajectory_rlt_file):
    """Decide to launch MissionPLanner.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file.
    
    Returns
    -------
    launch : boolean
        True if descending phase. False otherwise.
    
    """
    # read trajectory file and set units
    data = read_sodaq_traj(trajectory_rlt_file)
    # compute time and altitude difference
    dt = data['Time'][-3:] - data['Time'][-4:-1]
    da = data['alt'][-3:] - data['alt'][-4:-1]
    # compute vertical velocity
    Vvert = da/dt
    log.info('Vvert = '+str(Vvert.to('m/s')))
    # if vertical velocity is negative the fall has started => time to predict landing point.
    if np.all(Vvert >  0.0*u.m/u.s):
        launch = False
    else:
        launch = True
    return launch

def get_initcond(trajectory_rlt_file):
    """Get initial conditions.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file from SODAQ card measurements.
    
    Returns
    -------
    last_point : astropy.coordinates.EarthLocation object
        Last measured point.
    last_time : astropy.time.Time object
        Tim of last point.
    Vvert : astropy.units.Quantity object
        Median vertical velocity in [m/s].
    
    """
    # read trajectory file and set units
    data = read_sodaq_traj(trajectory_rlt_file)
    # compute time and altitude difference
    dt = data['Time'][-3:] - data['Time'][-4:-1]
    da = data['alt'][-3:] - data['alt'][-4:-1]
    # compute vertical velocity
    Vvert = [0.0,0.0,np.median(da/dt).to('m/s').value]*u.m/u.s
    # set last position
    last_point = EarthLocation(lon= data['lon'][-1],
                               lat=data['lat'][-1],
                               height=data['alt'][-1])
    # set last time
    last_time = data['Time'][-1]
    
    return (last_time,last_point,Vvert)

def send_land_position(serial_obj,land_time,land_lat,land_lon,land_alt):
    """Send predicted landing position to ground via LoRa network, through SODAQ card.
    
    Parameters
    ----------
    serial_obj : serial.Serial object
        Serial object with open UART port.
    land_time : astrop.time.Time object
        Time
    land_lon : astropy.units.Quantity object
        Longitude in deg
    land_lat : astropy.units.Quantity object
       Latitude in deg
    land_alt : astropy.units.Quantity object
        Altitude in m
    
    Returns
    -------
    success_send : boolean
        True is message sent successfully. False otherwise.
    
    """
    # separator
    sep = ','	
    sodaq_time = land_time.isot
    # convert lon,lat, alt into an astropy.units.Quantity object
    sodaq_lat = str(int(land_lat.to('deg').value * 1.0E+07))
    sodaq_lon = str(int(land_lon.to('deg').value * 1.0E+07))
    sodaq_alt = str(int(land_alt.to('m').value * 1.0E+03))
    
    # format the message: must be a long string
    sodaq_msg = sep.join([sodaq_lat,sodaq_lon,sodaq_alt])+','+sodaq_time+'\n'
    log.debug('sodaq_msg: '+sodaq_msg)
    
    # send to SODAQ card
    success_send = send_msg_twice(serial_obj,sodaq_msg)
    
    return success_send

def send_msg_twice(serial_obj,sodaq_msg):
    """Send a message to SODAQ card twice to make sure it receives it.
    
    Parameters
    ----------
    serial_obj : serial.Serial object
        Serial object with open UART port.
    sodaq_msg : string
        Message to send to SODAQ card.
    
    Returns
    -------
    success_send : boolean
        True is messages were sent successfully. False otherwise.
    
    """
    try:
        import time
        # send to SODAQ card twice with a 1-second interval.
        log.debug('msg about to be sent')
        serial_obj.write(bytes(sodaq_msg,'utf-8'))
        log.debug('msg written to port once')
        # wait for 1 sec before sending the 2nd message.
        time.sleep(1)
        serial_obj.write(bytes(sodaq_msg,'utf-8'))
        log.debug('msg written to port twice')
        success_send = True
    except:
        success_send = False
    return success_send
