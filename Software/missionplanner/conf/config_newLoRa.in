
[USER]
# HOME directory: Set to 'auto' to get it automatically
home = auto
# Project directory
proj_dir = ${home}/malbec/
# MissionPlanner Software directory
soft_dir = ${proj_dir}/Software/
# configuration directory
conf_dir = ${soft_dir}/conf/
# Data sub-directory
data_dir = ${proj_dir}/DATA/
# log sub-directory
log_dir = ${soft_dir}/../LOG/
# log level
log_level = INFO
# SPICE kernel path
kernel_path = /astrodata/kernels/
# SPICE utilities path
spice_exe_path = /astrodata/exe/

[LO]
# MQTT user name
username = application
# MQTT password
#password = 21054490cb234f16947383673bdbc5be
password = cf45825f4d384f55ac8f04dca0cbfbaa
# MQTT IP
ip = liveobjects.orange-business.com
# MQTT port
port = 8883
# MQTT Keep alive
keepalive = 60
# MQTT FIFO
fifo = malbec2022
# number of positions from GPS required before MissionPlanner launch can be decided ; useful if flight_type=inflight
nbpos = 2

[CAMPAIGN]
# name of the flight campaign: this MUST be in format yyyymmdd
name = 20230506
# simulation sub-directory name
sim_dir_name = MissionPlanSim_Paris_test/
# nacelle SPICE name
nacelle_name = MALBEC1
# nacelle SPICE id ; for more details see: https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/naif_ids.html#Earth%20Orbiting%20Spacecraft.
nacelle_id = -1399001
# data output directory full path
out_dir = ${USER:data_dir}/${name}/${sim_dir_name}/
# logging file name root
log_file_root = ${USER:log_dir}MissPlan-YYYYMMDDTHHMMSS.log
# Launch sites configuration file
sites_file = ${USER:conf_dir}/sites.in
# Payload configuration file: payload-freefall.in or payload-glide.in
payload_file = ${USER:conf_dir}/payload-freefall.in
# real time trajectory, from GPS measurement file: useful if flight_type=inflight or flight_type=inflight_lora
trajectory_rlt_file = ${out_dir}/trajectory_lora_descent_low_test.dat
# real time trajectory file format: choice is: grib, CNES2017, CNES2018, CNES2019, ODROID, LORA OR SIMU
trajectory_rlt_fmt = LORA
# simulated trajectory output file name
trajectory_sim_file = ${out_dir}/trajectory_from_lora_test.dat

[FLIGHT]
# type of flight to simulate: 'prevision' or 'inflight' (for postfilght CNES measurement) or 'inflight_lora' (if you are using a LoRa card).
flight_type = inflight_lora
# trajectory type: if flight_type='prevision' AND DEBUG PURPOSE ONLY. Choice is: ASCEND or FREEFALL or GLIDE
trajectory_type = FREEFALL

[WEATHER]
# weather data file full path name MUST exists EVEN if flight_type='inflight', for backup reasons.
#weather_file = ${USER:data_dir}/${CAMPAIGN:name}/Weather/20230506.grib
weather_file = ${USER:data_dir}/${CAMPAIGN:name}/Weather/testParis.grib
# weather file format: 'grib' (if flight_type = prevision), 'CNES2017', 'CNES2018', 'CNES2019', (*.cor file) or 'ODROID' (from Philippe Deverchere script) or 'lora'.
weather_format = lora

[DTM]
# Digital Terrain Model directory
dtm_dir = ${USER:data_dir}//MNT/

[INITCOND]
# NOTE: this section is NOT relevant if flight_type=inflight
# initial condition: location of start=site id: useful if flight_type=prevision
initcond_site = 31
#initial conditions: time of start, in ISOT format ; useful if flight_type=prevision, irrelevant if flight_type=inflight
initcond_time = 2023-05-06T02:00:00.000


[SIMULATION]
# type of ascend
ascend_type = CONSTANT
# default time step for ASCEND [s]
time_step_up = 1.0
# default time step for FREEFALL or GLIDE [s]
time_step_dwn = 0.5
# minimum altitude to reach the ground [m]
HXmin = 300 
# maximum explosion altitude [m] WARNING: THIS CONSTRAINS ASCEND INFLIGHT CALCULATIONS
HXmax = 35000.0
# optimal ASCENDING vertical velocity [m/s]
Vopt = 5.0
# altitude below which the ground altitude is computed [m]. Note: Pyrenees highest mountain is 3400 m.
MaxGroundAlt = 3500.0
