\begintext 
  Definition of the pointing direction and FOV of MALBEC camera.
  
  Author: J. Vaubaillon - IMCCE - 2020 
  MALBEC project 

\begindata
 
   NAIF_BODY_NAME                      += 'MALBEC_CAMERA'
   NAIF_BODY_CODE                      += -100399001

\begintext 

Definition of the MALBEC FOV: 
 
\begindata 
INS-100999001_NAME = 'MALBEC_CAMERA' 
INS-100999001_FOV_FRAME = 'MALBEC_FRAME' 
INS-100999001_FOV_SHAPE = 'RECTANGLE' 
INS-100999001_BORESIGHT = ( 0.0,   0.0 ,  1.0 ) 
INS-100999001_FOV_CLASS_SPEC = 'ANGLES' 
INS-100999001_FOV_REF_VECTOR = ( 0.0,   1.0 ,  0.0 )  
INS-100999001_FOV_REF_ANGLE =  20.0 
INS-100999001_FOV_CROSS_ANGLE =  20.0 
INS-100999001_FOV_ANGLE_UNITS =  'DEGREES' 
