[USER]
# HOME directory: Set to 'auto' to get it automatically
home = auto
# Project directory
proj_dir = ${home}/malbec/
# MissionPlanner Software directory
soft_dir = ${proj_dir}/Software/
# configuration directory
conf_dir = ${soft_dir}/conf/
# Data sub-directory
data_dir = ${proj_dir}/DATA/
# log sub-directory
log_dir = ${soft_dir}/../LOG/
# log level
log_level = DEBUG
# SPICE kernel path
kernel_path = /astrodata/kernels/
# SPICE utilities path
spice_exe_path = /astrodata/exe/

[LO]
# MQTT user name
username = FW
# MQTT password
password = 543c50656cb7483f9bba5a63539a5ba2
# MQTT IP
ip = liveobjects.orange-business.com
# MQTT port
port = 1883
# MQTT Keep alive
keepalive = 60
# MQTT FIFO
fifo = MALBEC2E4
# number of positions from GPS required before MissionPlanner launch can be decided ; useful if flight_type=inflight
nbpos = 4

[CAMPAIGN]
# name of the flight campaign: this MUST be in format yyyymmdd
name = 20200622
# simulation sub-directory name
sim_dir_name = Trajectory
# nacelle SPICE name
nacelle_name = MALBEC_NACELLE1
# nacelle SPICE id ; for more details see: https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/naif_ids.html#Earth%20Orbiting%20Spacecraft.
nacelle_id = -1399001
# data output directory full path
out_dir = ${USER:data_dir}/${name}/${sim_dir_name}/
# logging file name root
log_file_root = ${USER:log_dir}MissPlan-YYYYMMDDTHHMMSS.log
# Launch sites configuration file
sites_file = ${USER:conf_dir}/sites.in
# Payload configuration file: payload-freefall.in or payload-glide.in
payload_file = ${USER:conf_dir}/payload-freefall.in
# real time trajectory, from GPS measurement file: useful if flight_type=inflight
trajectory_rlt_file = ${out_dir}/trajectory_rlt_sodaq.dat
# real time trajectory file format: choice is: CNES2017, CNES2018, CNES2019 or SODAQ
trajectory_rlt_fmt = sodaq
# simulated trajectory output file name
trajectory_sim_file = ${out_dir}/trajectory.dat

[FLIGHT]
# type of flight to simulate: 'prevision' or 'inflight' or 'inflight_sodaq' (if you are using a Sodaq card).
flight_type = prevision
# trajectory type: if flight_type='prevision' AND DEBUG PURPOSE ONLY. Choice is: ASCEND or FREEFALL or GLIDE
trajectory_type = ASCEND

[WEATHER]
# weather data file full path name MUST exists EVEN if flight_type='inflight', for backup reasons.
weather_file = ${USER:data_dir}/${CAMPAIGN:name}/Weather/PA_EUROC25_20062212061295993.KEYudfvDAABB0O9Uu729Ud.grib
# weather file format: 'grib' (if flight_type = prevision), 'CNES2017', 'CNES2018', 'CNES2019', (*.cor file) or 'ODROID' (from Philippe Deverchere script).
weather_format = grib

[DTM]
# Digital Terrain Model directory
dtm_dir = ${USER:data_dir}//MNT/

[INITCOND]
# NOTE: this section is NOT relevant if flight_type=inflight
# initial condition: location of start=site id: useful if flight_type=prevision
initcond_site = 26
#initial conditions: time of start, in ISOT format ; useful if flight_type=prevision, irrelevant if flight_type=inflight
initcond_time = 2020-06-22T11:00:00.000
# FOR DEBUG PURPOSE: set full initial conditions: lon [deg], lat [deg], alt [m]
# longitude, positive EAST of Greenwich [deg]
#lon = -0.251241993165
# latitude, positive NORTH of equator [deg]
#lat = 43.7064811197
# altitude above sea level [m]
#alt = 8333.00


[SIMULATION]
# type of ascend
ascend_type = CONSTANT
# default time step for ASCEND [s]
time_step_up = 1.0
# default time step for FREEFALL or GLIDE [s]
time_step_dwn = 0.5
# minimum altitude to reach the ground [m]
HXmin = 300 
# maximum explosion altitude [m] WARNING: THIS CONSTRAINS ASCEND INFLIGHT CALCULATIONS
HXmax = 30000.0
# optimal ASCENDING vertical velocity [m/s]
Vopt = 5.5
# altitude below which the ground altitude is computed [m]. Note: Pyrenees highest mountain is 3400 m.
MaxGroundAlt = 3500.0
