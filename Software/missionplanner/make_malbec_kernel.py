"""Make one/all the spice kernels for each and every MALBEC station.

The sites.in file must be present (see inside code).
Kernels are saved by default in /astrodata/kernel/ path.

Parameters
----------
None.

Returns
-------
None.

DEPENDENCIES:
-spice librairie
-spice pinpoint utility program installed


Authors:
A. Rietze, Univ. Oldenburg, Germany
J. Vaubaillon, IMCCE, 2020
"""

#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import os
import astropy.units as u
import simplekml

from missionplanner.utils import log

home = os.getenv('HOME')+'/'

#--------------------------------------------------------------------------------
# Define default input values
# (maybe better: configuration file?)
#--------------------------------------------------------------------------------
# set kernel path and SPICE utilities path -> better in configuration file
# SPICE default kernel path
kernel_path = '/astrodata/kernels/'
# SPICE utilities path
spice_exe_path = '/astrodata/exe/'

# set MALBEC common configuration directory
conf_dir =  home + '/malbec/Software/conf/'

# set input file with information about sites:
sites_file = conf_dir + 'sites.in'

# make KML file name
kml_file = sites_file.replace('.in','.kml')
kml = simplekml.Kml()

# check that the sites file exists
if not os.path.exists(sites_file):
    msg = '*** FATAL ERROR: site file '+sites_file+' does not exist'
    log.error(msg)
    raise IOError(msg)

# sets input standard pck kernel
inputpckfile = kernel_path + '/pck00010.tpc'

# sets output files
outputdefnsfile = conf_dir + '/malbec_site.defns'
outputspkfile = conf_dir + '/malbec_site.bsp'
outputfkfile = conf_dir + '/malbec_site.fk'


# delete previously created cabernet kernel files
if os.path.exists(outputdefnsfile):
    os.remove(outputdefnsfile)
if os.path.exists(outputspkfile):
    os.remove(outputspkfile)
if os.path.exists(outputfkfile):
    os.remove(outputfkfile)

# read sites file
sitesconf = ConfigParser()
log.info('Reading sites file: '+sites_file)
sitesconf.read(sites_file)

# determine the total number of sites
nsites = 0
keepgoing = True
while keepgoing:
    param = 'SITE'+str(nsites+1)
    # try if the SITE number is present in the file
    try:
        tmp = sitesconf[param]
        nsites = nsites+1
        log.info('SITE'+param+' found in '+sites_file)
    except:
        break
log.info('There are '+str(nsites)+ ' possible landing sites recorded in the sites_file')

# check that there its at least one possible landing site
if not nsites:
    msg = '*** FATAL ERROR: there is no possible landing site in '+sites_file
    log.error(msg)
    raise ValueError(msg)

# create dictionary of landing sites
landingsites = {}
# list of keys to input into landing site dictionary
list_keys = ['id', 'lon', 'lat', 'alt', 'type', 'name', 'tel']

# now starts printing the outputdefnsfile file
out = open(outputdefnsfile, 'w')
out.write("\\begindata \n")
out.write("SITES = ( \n")
# writes the name of all MALBEC stations as SITE1, SITE2,..., SITE24
for id_site in range(nsites):
    param = 'SITE'+str(id_site+1)
    out.write("        "+repr(param) + ", \n")
out.write(") \n")

# loop to write the outputdefnsfile for all MALBEC stations
# NOTE: this loop is only for 24 existing MALBEC sites -> should be modified
for id_site in range(nsites):
    param = 'SITE'+str(id_site+1)
    log.debug('writing setup for site: '+param)
    lon = float(sitesconf[param]['lon']) * u.deg
    lat = float(sitesconf[param]['lat']) * u.deg
    alt = float(sitesconf[param]['alt']) * u.m
    # now prints the outputdefnsfile file
    out.write("\n")
    out.write("      " + str(param) + "_CENTER = 399 \n")
    out.write("      " + str(param) + "_FRAME = " + repr('ITRF93') + "\n")
    out.write("      " + str(param) + "_IDCODE = " + str(399200+float(sitesconf[param]['id'])) + "\n")
    out.write("      " + str(param) + "_LATLON = (" + str(lat.to('deg').value) + ',' + str(lon.to('deg').value) + ',' + str(alt.to('km').value) + ")\n")
    out.write("      " + str(param) + "_UP = " + repr('Z') + "\n")
    out.write("      " + str(param) + "_NORTH = " + repr('X') + "\n")
    out.write(" \n")
    
    # put site info into kml data
    pnt = kml.newpoint(name=param,coords=[(lon.to('deg').value,lat.to('deg').value,alt.to('m').value)],extrude=1)
    pnt.altitudemode = simplekml.AltitudeMode.absolute
    # set icon
    # CNES balloon launch site
    if id_site==0:
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/wht-circle.png'
    # Mountain sites
    elif (id_site==11) or (id_site==12) or (id_site==13) or (id_site==15):
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/red-circle.png'
    # first corona sites
    elif id_site<7:
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/ylw-circle.png'
    # other sites
    elif id_site<100:
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/ltblu-circle.png'
    
    
    # put site infos into a dictionnary
    local_dict = {}
    for key in list_keys:
        try:
            local_dict[key] = sitesconf[param][key]
            landingsites[param] = local_dict
            log.debug(key+ ' : '+ sitesconf[param][key])
        except:
            msg = '*** FATAL ERROR: '+key+' was not found in '+param
            log.error(msg)
            raise ValueError(msg)
# closes the outputdefnsfile
out.close()

# now executes the pinpoint program to create the spk kernel file
cmd = spice_exe_path + "pinpoint -def " + outputdefnsfile + " -spk " + outputspkfile + " -pck " + inputpckfile + " -fk " + outputfkfile + " -batch"
log.info("cmd=" + cmd)
os.system(cmd)
log.info("Sites kernel data saved in " + outputspkfile + " and " + outputfkfile)

# save KML file 
kml.save(kml_file)
log.info("Sites data saved in " + kml_file)
