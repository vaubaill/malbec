""" Python routine to test missionplanner with a single simulation

    single_simulation_test of missionplan.py

    MissionPlanner is a Python library for dealing with MALBEC project trajectory prediction.
    ____________
    Installation

    the sources are found under gitlab (authorization required).
    The command to download the whole project is:

    ```bash
    git clone https://gitlab.com/vaubaill/malbec.git
    ```

    Then:
    ```bash
    cd malbec/Software/MissionPlanner1.2
    ```
    The configuration files are located in the 'conf' sub-directory.

    ___________
    Usage

    First you need to set up the configuration file, hereafter called 'config.in'.
    Here is a description of the different sections:

    USER : path definitions.
    You might want to change the 'sim_dir_name' to a sub-directory name where simulation data will be saved.

    LO : for LoRa communications with SODAQ electronic card (see https://sodaq.com) and the LiveObject utilities (see https://liveobjects.orange-business.com/). If you are running simulations you do not need this one.

    CAMPAIGN : path and file definitions
    payload_file : several choices are possible: payload-freefall.in if you use a free falling payload, or payload-glide.in if yo u use a glider.
    sites_file : name of file containing all possible launch/land stations.
    nacelle_name : the simulated trajectory can be converted into a JPL/NAIF/SPICE spk kernel file for future processing. This is the SPICE name of the MALBEC nacelle. See also https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/naif_ids.html#Earth%20Orbiting%20Spacecraft.
    nacelle_id : the nacelle SPICE id. See nacelle_name. Default is -1399001.

    FLIGHT : flight profile
    flight_type = 'prevision' for previsions before the flight, 'inflight_sodaq' if during the flight and using a Sodaq card.

    WEATHER : path definition where weather data are located.
    weather_format = grib by default. Others are used for debug purpose.

    DTM : path definition

    INITCOND : initial conditions.
    initcond_site : id of the site (see CAMPAIGN:sites_file) where nacelle starts.
    initcond_time : time, in  UT and ISOT format (e.g.: '2020-01-01T12:01:12.345')
    Alternatively, for tests or debug purpose, you can specify the start location with the lon, lat, alt keywords. Units are [deg,deg,m].

    SIMULATION : simulation type
    ascend_type : default is CONSTANT ; in this version, no other value is possible. This might change i future more realistic versions.
    time_step_up : for consistency and accuracy purpose, this is tuned to 1s. You might try slightly less or more, but 5s is already too much from our experience.
    time_step_dwn : same, default is 0.5s for some reasons.
    Vopt : vertical speed in ascending phase. Usual valu ranges between 4 and76 m/s.
    HXmin : deprecated.
    HXmax : altitude where the balloon explosion is considered to happen. Unit is [m].

    __________
    Contributing
    J. Vaubaillon (IMCCE, Observatoire de Paris, PSL, France), D. Zilkova (IMCCE, France / U. Comenius, Slovakia), A. Rietze (IMCCE, France / U. Oldenburg, Germany)

    __________
    License
    [MIT](https://choosealicense.com/licenses/mit/)
"""

# To run a single simulation:

from missionplanner import missionplan

# set configuration file name
# config_file = './conf/config20190523-testpredict_ar.in'
config_file = '../conf/config20191202-testpredict_ar.in'

# create MissionPlanner Object
missplan = missionplan.MissionPlanner(config_file)

# launch the simulation
missplan.launch()


#To run a whole bunch of simulations in order to get a probable landing area (used for prevision before flight):

#```python
#import missionplan
#import launch_simulti

# set configuration template file directory
#config_dir = './conf/conf_gen'
# set configuration template file name
#config_file = config_dir+'/template-freefall.in'
# minimum balloon explosion altitude, in [m]
#Hmin=25000.0
# maximum balloon explosion altitude, in [m]
#Hmax=40000.0
# explosion altitude step, in [m]
#Hstep=5000.0
# minimum ascending velocity, in [m/s]
#Vmin=5.0
# maximum ascending velocity, in [m/s]
#Vmax=7.0
# ascending velocity step, in [m/s]
#Vstep=0.5
# launch all the simulations
#launch_simulti.launch_simulti(confgen_dir=config_dir,template_file=config_file,Hmin=Hmin,Hmax=Hmax,Hstep=Hstep,Vmin=Vmin,Vmax=Vmax,Vstep=Vstep)
#```



