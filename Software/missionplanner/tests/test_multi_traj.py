

import trajectory
import multi_traj

# user input 20171212

"""
trajfile1 = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20171212/Trajectory-H35000V5.0/trajectory.dat'
trajfile2 = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20171212/Trajectory-2ndsite/trajectory.dat'
outdir = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20171212/Trajectory-comp/'
"""

# user input 20190523
trajfile1 = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/Trajectory-H35000V6.5/trajectory.dat'
trajfile2 = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/Trajectory-2ndsite/trajectory.dat'
outdir = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/Trajectory-comp/'


outfile = 'trajcomp.dat'

traj1=trajectory.Trajectory()
traj2=trajectory.Trajectory()
traj1.load(trajfile1)
traj2.load(trajfile2)
multi_traj.mutlitraj2azel(traj1,traj2,outfile,out_dir=outdir)
