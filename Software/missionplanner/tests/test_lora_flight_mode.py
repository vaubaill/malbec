#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  3 11:09:50 2022

Test LoRa flight mode.

@author: vaubaill
"""


import os
import sys
import time
import subprocess
import logging
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
import numpy as np
from astropy.coordinates import EarthLocation
from configparser import ConfigParser, ExtendedInterpolation
import paho.mqtt.client as mqtt
import json

from missionplanner import missionplan

# create default logger object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
# create StreamHandler
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
#log.addHandler(sdlr)


######## USERS CONFIG
# missplanner config file
configfile = '../config.in'

# start of the program
# first launch the LoRa data simulator
#os.system('python3 test_create_realtime_file.py')


log.debug('Creating MissionPlanner object')
missplan = missionplan.MissionPlanner(configfile)

# frequency at which the process is checking for flight status update, in [s]
wait = 30
# number of tests to conduct
nb = 10
i=0
log.info('======== Entering the loop =======')
while i<nb:
    i = i+1
    log.info('======== Getting latest position =======')
    # get latest position
    missplan.set_initialcond()
    log.info('trajectory_type: '+missplan.trajectory_type)
    # if trajectory type changes to FREEFALL then launch computation of trajectory
    if missplan.trajectory_type=='FREEFALL':
        log.debug('LAUNCH Mission Planner')
        (land_t, land_loc) = missplan.launch()
        log.debug('Back from Mission Planner')
        log.debug('land_t: ' + land_t.isot)
        log.debug('land_lat: ' + str(land_loc.lat.to('deg').value))
        log.debug('land_lon: ' + str(land_loc.lon.to('deg').value))
        log.debug('land_alt: ' + str(land_loc.height.to('m').value))
    time.sleep(wait)



