#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 09:15:43 2022

@author: vaubaill
"""

# test msg liveobject

from astropy.time import Time
import astropy.units as u
from missionplanner.liveobjects import send_baliselora_landing_msg, make_waze_url,send_waze_msg
#import requests


time = Time.now()+4.0*u.hr #('2022-10-10T01:23:45.432')
lat = 44.0*u.deg
lon = 2.2*u.deg
alt = 60.0*u.m
msg = make_waze_url(time,lat,lon,alt)
success = send_waze_msg(msg)
#success = send_baliselora_landing_msg(time,lat,lon,alt)
print(time.isot)
print('Time stamp:'+'"' + time.isot + 'Z"')
print('success: ',success)


    
    
    