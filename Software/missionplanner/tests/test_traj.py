import trajectory

# user input 20190523
#trajfile1 = '/Users/vaubaill/malbec/DATA/20180627/Trajectory-H30000V6.5/trajectory.dat'
trajfile1 = '/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20180627/Trajectory/trajectory.dat'
spk_file1 = trajfile1.replace('.dat','.spk')

traj1=trajectory.Trajectory()
traj1.load(trajfile1)


#https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/naif_ids.html#Earth%20Orbiting%20Spacecraft.
#NAIF ID = -100000 - NORAD ID code
nacelle_id =-1399002
nacelle_name = 'MALBEC'

traj1.to_spk(nacelle_name,nacelle_id,spk_file1)
