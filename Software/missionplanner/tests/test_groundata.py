# test groundata.py

import logging
import numpy as np
import astropy.units as u
import groundata

# create Logger Object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
# log format
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
# create StreamHandler
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
# add StreamHandler and FileHandler into logger
log.addHandler(sdlr)

gd_path='/home/vaubaill/PROJECTS/PODET/PODET-MET/FRIPON/Soft/friponsvn/fripipe_data/Topography/IGN/MNT/'

GD=groundata.GroundData(log)

lon=-0.0006389723129166212*u.deg
lat=42.928654471921064*u.deg

lon=-0.1405867299817189*u.deg
lat=42.76125088038465*u.deg

GD.extract_ign_data(gd_path,lon,lat)
groundalt = GD.interpol_ign_data(lon,lat)

print(groundalt)
pouet

all_lon_test = np.arange(0.0,0.05,0.01,dtype='float')*u.deg
all_lat_test = np.arange(43.6,43.8,0.01,dtype='float')*u.deg

for lon in all_lon_test:
    for lat in all_lat_test:
        log.info('lon='+str(lon.to('deg'))+' lat='+str(lat.to('deg')))
        GD.extract_ign_data(gd_path,lon,lat)
        groundalt = GD.interpol_ign_data(lon,lat)
        log.info('================================ groundalt: '+str(groundalt))

log.info('Test passed. ok!')
