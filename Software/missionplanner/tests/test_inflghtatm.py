""" test inflightatmosphere functions

"""


import numpy as np
import inflight_atmosphere



def printdata(Atm,lon,lat,alt):
    (wind3D,P,T) = Atm.get_windPT(lon,lat,alt)
    print('alt:'+str(alt)+' wind3D: '+str(wind3D))
    #print('P: '+str(P))
    #print('T: '+str(T))



"""
inflight_atm_file = '/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/Weather/real_time_test.dat'
inflight_atm_fmt = 'CNES2019'

Atm_flt = inflight_atmosphere.InFlight_Atmosphere(inflight_atm_file,inflight_atm_fmt)

print('get last point: ')
(last_loc,last_time,last_P,last_T) = Atm_flt.get_lastpoint()
print('Last loc : '+str(last_loc.lon.to('deg'))+' '+str(last_loc.lat.to('deg'))+' '+str(last_loc.height.to('m')))

print('get last velocity: ')
print(Atm_flt.get_last_velocity())
"""

inflight_atm_file = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20171212/Trajectory-rltmtest-freefall-fromtop/AS2017121209_1-11000.cor'
inflight_atm_fmt = 'CNES2017'

Atm_flt = inflight_atmosphere.InFlight_Atmosphere(inflight_atm_file,inflight_atm_fmt)
Atm_flt.plot()

lon=1.6244722582452527
lat=43.34331020142213

for alt in np.arange(10300,11000,10):
    printdata(Atm_flt,lon,lat,alt)


