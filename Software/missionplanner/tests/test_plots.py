
import trajectory

outdir = '/Users/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20180627/Trajectory/'
traj_file = outdir+'trajectory.dat'

traj = trajectory.Trajectory()
traj.load(traj_file)

(eloc,t)=traj.get_last_point()

traj.plot_all(outdir)

traj.to_kml(outdir+'/Trajectory.kml')

print('ok!')
