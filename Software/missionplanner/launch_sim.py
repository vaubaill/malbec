#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 15:11:09 2019

@author: dzilkova, J. Vaubaillon, IMCCE

MALBEC project, IMCCE, KYU-ACE, ScotopicLab, CNES
"""

# launche MissionPlanner for a chosen configuration, defined by config files.


from missionplanner import missionplan
#from missionplanner import atmosphere_comp

# configuration definitions


#2017/12/12

#config_file = './conf/config20171212.in'
#config_file = './conf/config20171212-2ndsite.in'
#config_file = './conf/config20171212-glide.in'
#config_file = './conf/test_realtime.in'
#config_file = './conf/test_realtime-freefall.in'


#2018
#config_file = './conf/config2018-descent.in'
"""
config_file = './conf/config2018.in'
flt_file = '../../../DATA/20180627/CNES/DDV_BLD_2018_N30/Donnees/AS2018062706_2.dat'
flt_fmt = 'CNES2018'
"""

# 20190104 test January : Quadrantids
"""
config_file = './conf/config20190104.in'
"""


# 20190219
"""
#config_file = './conf/conf_gen/configH35000V5.0.in'
config_file = './conf/config20190219.in'
flt_file = '../../DATA/20190219/CNES/AS2019021912_1.dat'
flt_fmt = 'CNES2019'
"""

# 20190523
#config_file = './conf/config20190523-2ndsite-glide.in'
#config_file = './conf/config20190523-glide.in'
#config_file = './conf/config20190523.in'
#config_file = './conf/config20190523-freefall.in'
#config_file = './conf/config20191123-testLoRa.in'
#config_file = './conf/config20191202-testLoRa_et_LO.in'
#flt_file = '../../DATA/20190523/CNES/AS2019052306_1.dat'
#flt_fmt = 'CNES2019'
#config_file = './conf/config20190523-testpredict.in'
#config_file = './conf/config20190523-site1.in'
#config_file = './conf/config.in'
#config_file = './conf/config_GEM2016.in'
config_file = './conf/config_ceiling.in'

# create Object
missplan = missionplan.MissionPlanner(config_file)
# launch simulation
missplan.launch()

"""
config_file = './conf/config20190523-site2.in'
# create Object
missplan = missionplan.MissionPlanner(config_file)
# launch simulation
missplan.launch()
"""


# compare predicted and real atmosphere
#atmosphere_comp.atm_comp(config_file,flt_file,flt_fmt)
