#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 14:52:30 2023

@author: vaubaill
"""

import os
import argparse
from astropy.table import QTable
import logging

import utils


log = logging.getLogger(__name__)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
log.setLevel('DEBUG')
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))


def traj2lora(traj_file,lora_file):
    """Transform a trajectory file into a LoRa trajectory file.

    Parameters
    ----------
    traj_file : string
        Trajectory file name.
    lora_file : string
        LoRa trajectory file name.

    Returns
    -------
    None.

    """
    utils.check_file(traj_file,log)
    log.info('Read trajectory file: '+traj_file)
    traj = QTable().read(traj_file,format='ascii.fixed_width_two_line')
    lora = QTable(names=['Time','Longitude','Latitude','Altitude'],
                  dtype=['object','float','float','float'])
    i = 0
    for data in traj:
        i = i+1
        if ((i/60)>=1.0):
            i = 0
            lora.add_row([data['Time'],data['Longitude'],data['Latitude'],data['Altitude']*1000])
    # lora['Time'] = traj['Time']
    # lora['Lon'] = traj['Longitude']
    # lora['Lat'] = traj['Latitude']
    # lora['Alt'] = lora['Alt']*1000
    lora.write(lora_file,format='ascii.fast_no_header',formats={'Altitude':'%.0f'},overwrite=True)
    log.info('LoRa file saved in '+lora_file)
    
    return




if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(description='traj2lora arguments.')
    parser.add_argument('-i',default='trajectory.dat',help='configuration file. Default is: trajectory.dat')
    parser.add_argument('-o',default='trajectory.dat',help='configuration file. Default is: trajectory.dat')
    args = parser.parse_args()
    
    # set configuration file
    traj_file = args.i
    lora_file = args.o
    
    # launch program
    traj2lora(traj_file,lora_file)
else:
    log.debug('successfully imported')