#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 11:33:25 2019

@author: jvaubaillon
"""

import astropy.units as u
import numpy as np

class GroundNoData (object):
    """GroundNoData object
    
    Parameters
    ----------
    ign_all_lat : astropy.coordinates.Angle object
        array of latitudes
    ign_all_lon : astropy.coordinates.Angle object
        array of longitudes
    ign_all_alt : astropy.units.Quantity object
        array of altitudes
    
    """
    
    ign_all_lat = []
    ign_all_lon = []
    ign_all_alt = []
    
    
    def __init__(self,log):
        """Instantiates a GroundData object.
        
        Parameters
        ----------
        log : logging.Logger object
            Logger.
        
        """
        self.log=log
        #self.log.setLevel(logging.DEBUG)
        self.log.debug('GroundNoData Object successfully created.')
        return
    
    def extract_ign_data(self,path_mnt,input_longitude,input_latitude):
        """extract data from IGN data files.
        
        Parameters
        ----------
        path_mnt : string
            name of path where the IGN files are located
        input_longitude : astropy.units.Quantity object
            longitude where data are to be extracted
        input_latitude : astropy.units.Quantity object
            latitude where data are to be extracted
        
        Returns
        -------
        None
            GroundNoData object data are initialized:
            ign_all_lat : all latitudes astropy.coordinates.Angle object [deg]
            ign_all_lon : all longitude astropy.coordinates.Angle object [deg]
            ign_all_alt : all heights astropy.units.Quantity [m]
        """
        # sort the array in ascending order of latitude
        self.all_lat = np.array([0,0])* u.deg
        self.all_lon = np.array([0,0])* u.deg
        self.all_alt = np.array([0,0])* u.m
        # set boundaries
        self.max_lat = 0.0*u.deg
        self.max_lon = 0.0*u.deg
        self.min_lat = 0.0*u.deg
        self.min_lon = 0.0*u.deg
        
        return
    
    def interpol_ign_data(self,input_longitude,input_latitude):
        """Interpolate IGN data.
        
        Parameters
        ----------
        input_longitude : astropy.units.Quantity object
            longitude at which the IGN data are to be interpolated
        input_latitude : astropy.units.Quantity object
            latitude at which the IGN data are to be interpolated
        
        Returns
        -------
        ground_alt : astropy.units.Quantity object
            height of ground above sea level [m]
        
        """
        return 0.0*u.m



