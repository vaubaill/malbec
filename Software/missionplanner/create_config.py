#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 14:48:25 2019

@author: dzilkova. J. Vaubaillon, IMCCE
"""

# create config files
import os
import sys
import shutil
import logging
import numpy as np
import astropy.units as u
import spiceypy as sp
from configparser import ConfigParser, ExtendedInterpolation

def create_config(template,H,V,outputdir):
    """Creates a MissionPlanner configuration file.
    
    Parameters
    ----------
    template : string
        Configuration file template full name.
    H : astropy.units.Quantity
        Maximum Height in [m]
    V : astropy.units.Quantity
        ascending velocity in [m/s]
    
    Returns
    -------
    outputfile : string
        Output configuration file name.
    VHname : string
        Velocity-Height identifier.
    
    """
    # convert H and V into strings
    Hstr = str("{0:.0f}".format(H.to('m').value))
    Vstr = str("{0:.1f}".format(V.to('m/s').value))
    # make name based on V and H
    VHname = 'H' + Hstr + 'V' + Vstr
    
    # read template file 
    config = ConfigParser()
    config.read(template)
    # create output configParser
    outconfig = ConfigParser()
    # set the home variable
    config['USER']['home'] = os.getenv('HOME')
    
    # loop over config keys
    for section in config.sections():
        outconfig.add_section(section)
        # loop over config keys-keys
        for item in config.items(section):  
            if (item[0]==str('vopt')):
                config[section][item[0]] = str(V.to('m/s').value)
                config.set(section, 'hxmax', str(H.to('m').value))
    
    # set log level
    config.set('USER', 'log_level','ERROR')
    
    # set log file
    config.set('CAMPAIGN', 'log_file_root','${USER:log_dir}MissPlan-YYYYMMDDTHHMMSS-'+VHname+'.log')
    
    # set/update output directory name
    traj_root = config['CAMPAIGN']['sim_dir_name']
    config.set('CAMPAIGN', 'sim_dir_name',traj_root+'-'+VHname)
    
    # set output trajectory name
    config.set('CAMPAIGN','trajectory_sim_file','${out_dir}/trajectory-'+VHname+'.dat')
    
    # set output config file name
    outputfile = outputdir + 'config' + VHname + '.in'
    
    # write output config file
    with open(outputfile,'w') as outconfigfile:
        config.write(outconfigfile)
    
    return (outputfile,VHname)
