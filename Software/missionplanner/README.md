# MALBEC MissionPlanner project.

## Meteor Automated Light Balloon Experimental Camera project.
J. Vaubaillon, D. Zilkova, A. Rietze, IMCCE, 2019-2020

## Scope
The missionplanner package simulates the trajectory of a stratospheric balloon, given the initial conditions (time and location), weather (wind) and nacelle configuration (classic or glider).
The output are SPICE kernels of the trajectory.


# Dependencies

## missionplanner program dependencies:

    The following NASA/NAIF/SPICE utilities are used by missionplan: pinpoint, mkspk.
    See also: https://naif.jpl.nasa.gov/naif/utilities.html.



## missionplanner PYTHON packages dependencies:

    astropy: www.astropy.org

    numpy: https://numpy.org/ 

    scipy: https://www.scipy.org/

    simplekml: https://simplekml.readthedocs.io/en/latest/

    matplotlib: https://matplotlib.org/

    pygc: https://pypi.org/project/pygc/

    pygrib: https://jswhit.github.io/pygrib/docs/

    spiceypy: https://spiceypy.readthedocs.io/en/master/documentation.html

    paho.mqtt: https://pypi.org/project/paho-mqtt/





# Repository organization : 

## conf : configuration files

    conf_gen : generic configuration files useful to create many different trajectory
        simulations with similar but different initial conditions.

## tests : routines test scripts.

## ../conf : common configuration files
    Include the following configuration files:
        sites.in : coordinates of possible launch / landing sites.
        fixed_stations.in : corrdinates of ground stations.
        payload-freefall.in : nacelle payload configuration file.
        spice_kernels directory : includes all basic and necessary SPICE kernels.

## ../../DATA : output data directory.
    Shared data are located by default in ../../DATA, but this can be changed
    in the missionplan configuration file.
    
    Sub-directory include:
        YYYYMMDD : each observation campaign must be identified with a date.
        e.g. 20170813 contains the data (simulations and real flight data)
        for the 13th August 2017 flight. It is set in the missionplan configuration file.
        
        Sub-directory include:
            Weather : retrieved weather data (in grib format) to make trajectory estimate.
                 It is set in the missionplan configuration file.
            Trajectory : trajectory estimates and related plots and data.
                 It is set in the missionplan configuration file.
        
        MNT : Digital terrain model data directory.
            So far the format is that provided by IGN (France).
            This sub-directory location is set in the missionplan configuration file.


# HOW TO launch missionplan : 

## Configuration file :
    The first step is to change the configuration file to fit your needs.
    See the section "Configuration file" below for further information.
    
## Typical missionplan commands :
    There are several ways to run missionplan.

        - 1 simply run: python3 missionplan.py [-conf $config]
        with $config the configuration file full path.
        The default configuration file is: ./conf/config.in.
    
        - 2 open the launch_sim.py script, manually set the configuration file and run python3 launch_sim.py.
        This is useful for debug/tests purpose.
    
        - 3 In order to compute the possible landing zone, multiple simulation (hereafter simulti) instances of missionplan.
        must be run with different ascending velocity and balloon explosion altitude.
        You need first to set ranges of ascending velocity and balloon explosion altitude
        in a simulti file. By default, this simulti configuration file is ./conf/config_simulti.in.
        A full description of the simulti configuration file is provided below.
        The launch_simulti.py script will create all different missionplan configuration
        files and run missionplan for each of them.
        All these missionplan configuration files are located in ./conf/conf_gen/ by default,
        but this is set changed in the simulti configuration file.
        The template used to create these missionplan configuration files is by default
        ./conf/conf_gen/template.in (but is set changed in simulti configuration file).
        All the (simulti) multiple simulations can be run at once with the command:
        python3 launch_simulti.py [-conf ./conf/config_simulti.in]
        with: config_simulti.in: the simulti configuration file.
        A full description of the simulti configuration file is provided below.


# missionplanner Configuration file description:

Here is an example and full description of a missionplan configuration file.

    [USER]
    # This section sets the path needed for the missionplan script.
    # HOME directory: Set to 'auto' to get it automatically. This might be useful when running missionplan from different users.
    home = auto
    # Project directory: this is where the whole gitlab project has been downloaded.
    proj_dir = ${home}/malbec/
    # missionplan Software directory : this is where the 'missionplanner' directory (containing all the missionplan scripts) can be found.
    soft_dir = ${proj_dir}/Software/
    # configuration directory: this is the subdirectory where all the configuration files can be found.
    conf_dir = ${soft_dir}/conf/
    # Data sub-directory: this is the directory where all simulated data as well as weather and topographic data can be found.
    data_dir = ${proj_dir}/DATA/
    # log sub-directory: this is where log files will be put.
    log_dir = ${soft_dir}/../LOG/
    # log level: log level. Choice is DEBUG, INFO, WARNING or ERROR: see: the python logging package for more info.
    log_level = DEBUG
    # SPICE kernel path: this is where the NASA/NAIF/ SPICE kernels are located. See also: https://naif.jpl.nasa.gov/naif/data_generic.html
    kernel_path = /astrodata/kernels/
    # SPICE utilities path: this is where the NASA/NAIF/ SPICE utilities programs are located. See also: https://naif.jpl.nasa.gov/naif/utilities.html
    spice_exe_path = /astrodata/exe/

    [LO]: 
    # This section is optional.
    # Orange LoRa card allowing us to communicate with the nacelle.
    # In particular, it allows us to detect the explosion and compute the expected landing point at the last minute.
    # This section is useful ONLY if flight_type=inflight or for test/debug purpose.
    # See also https://www.orange-business.com/fr/reseau-iot
    # MQTT user name
    username = XXX
    # MQTT password
    password = xxxxxxxxxxxxxxxxxxxxxxxx
    # MQTT IP
    ip = liveobjects.orange-business.com
    # MQTT port
    port = 9999
    # MQTT Keep alive
    keepalive = 00
    # MQTT FIFO
    fifo = MALBECXXX
    # number of positions from GPS required before missionplan launch can be decided ; useful if flight_type=inflight
    nbpos = 4

    [CAMPAIGN]
    # simulated campaign
    # name of the flight campaign: this MUST be in format yyyymmdd. The directory ${USER:data_dir}/${name} MUST exist and contain at least the weather file (see below).
    name = 20200622
    # simulation sub-directory name: this is the name of the output sub-directory, meaning that created data will be saved in ${USER:data_dir}/${name}/${sim_dir_name} directory
    sim_dir_name = Trajectory1
    # nacelle SPICE name
    nacelle_name = MALBEC_NACELLE1
    # nacelle SPICE id ; for more details see: https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/naif_ids.html#Earth%20Orbiting%20Spacecraft.
    nacelle_id = -1001
    # data output directory full path
    out_dir = ${USER:data_dir}/${name}/${sim_dir_name}/
    # logging file name root
    log_file_root = ${USER:log_dir}MissPlan-YYYYMMDDTHHMMSS.log
    # Launch sites configuration file: this file contains all possible launch sites. See example below.
    sites_file = ${USER:conf_dir}/sites.in
    # Payload configuration file: choice is either payload-freefall.in or payload-glide.in. See example below.
    payload_file = ${USER:conf_dir}/payload-freefall.in
    # real time trajectory, from GPS measurement file: useful if flight_type=inflight. This is the file where onboard GPS measurements will be stored.
    trajectory_rlt_file = ${out_dir}/trajectory_rlt_sodaq.dat
    # real time trajectory file format: choice is: CNES2017, CNES2018, CNES2019 or SODAQ. By default use sodaq: other exists for historycal reasons.
    trajectory_rlt_fmt = sodaq
    # simulated trajectory output file name: this is the trajectory output text/ascii file.
    trajectory_sim_file = ${out_dir}/trajectory.dat

    [FLIGHT]
    # This section deals with the kind of flight to simulate. Some options are for debug or test purpose only.
    # type of flight to simulate: 'prevision' or 'inflight' or 'inflight_sodaq' (if you are using a Sodaq card). If you are make a landing zone estimate, use 'prevision'.
    flight_type = prevision
    # trajectory type: this is telling the program how to start the computation: either ascending ('ASCEND') or descending (in either free fall 'FREEFALL' or gliding 'GLIDE' mode) if flight_type='prevision' or for DEBUG PURPOSE. Choice is: ASCEND or FREEFALL or GLIDE
    trajectory_type = ASCEND

    [WEATHER]
    # this section deals with the weather prediction file.
    # weather data file full path name MUST exists EVEN if flight_type='inflight', for backup reasons. Weather file MUST be in grib format.
    weather_file = ${USER:data_dir}/${CAMPAIGN:name}/Weather/PA_EUROC25_20200622T0600+06.grib
    # weather file format: 'grib'. Other options might be found if digging in missionplan.py script, but they exist for historical reasons.
    weather_format = grib

    [DTM]
    # this section deals with the Digital Terrain Model provided by IGN. So far only the French NTM providede by IGN is available.
    # IGN Digital Terrain Model directory
    dtm_dir = ${USER:data_dir}//MNT/

    [INITCOND]
    # this section deals with the simulation initial conditions
    # NOTE: this section is NOT relevant if flight_type=inflight
    # initial condition: location of start=site id: useful if flight_type=prevision (i.e. not useful if flight_type=inflight).
    # see also the description of the Launch site configuration file below.
    initcond_site = 1
    # initial conditions: time of start, in ISOT format ; useful if flight_type=prevision, irrelevant if flight_type=inflight
    initcond_time = 2020-06-22T11:00:00.000

    [SIMULATION]
    # This section deals with the simluation itself.
    # type of ascend: only one option: CONSTANT ; other options might be available for historical reasons, but are NOT recommended for this version.
    ascend_type = CONSTANT
    # default time step for ASCEND [s]: this dictates the integration accuracy and execution time. A good compromise is 1.0 sec.
    time_step_up = 1.0
    # default time step for FREEFALL or GLIDE [s]: this dictates the integration accuracy and execution time. A good compromise is 0.5 sec. It is shorter than for the ascending phase since the physics is more complicated.
    time_step_dwn = 0.5
    # maximum explosion altitude [m] WARNING: this quantity highly influences the location of the landing point when flight_type=prevision
    HXmax = 30000.0
    # optimal ASCENDING vertical velocity [m/s]: typical values range from ~4 to 7 m/s.
    Vopt = 5.5
    # altitude below which the ground altitude is computed thanks to IGN data [m]. Note: Pyrenees highest mountain is 3400 m.
    MaxGroundAlt = 3500.0




# Simulti configuration file : 

The simulti configuration file allows a user to run several instances of missionplan with different initial conditions.
This is useful to compute a possible landing zone.

    [USER]
    # this section sets the path needed for the launch_simulti.py script.
    # configuration diretory name: by default this is the same as for missionplan.
    conf_dir = conf/
    # configuration generator directory name: this is where all the creates configuration files will be saved.
    confgen_dir = ${conf_dir}/conf_gen/
    # configuration template file name: this is the missionplan configuration file template.
    template_file_name = template.in

    [SIM]
    # This section deals with the range of possible value of several physical quantities.
    # Minimum altitude of explosion in [m]
    Hmin = 25000.0
    # Maximum altitude of explosion in [m]
    Hmax = 35000.0
    # Maximum altitude step in [m]: this has a direct influence on the total number of simulations to run.
    Hstep=2500.0
    # Minimum ascending velocity in [m/s]
    Vmin=3.5
    # Maximum ascending velocity in [m/s]
    Vmax=5.5
    # ascending velocity step in [m/s]: this has a direct influence on the total number of simulations to run.
    Vstep=0.5

    [OUT]
    # set the name of the landingzone kml file. The location of this file is built as: ['CAMPAIGN']['out_dir']+'../' from the missionplan template configuration file.
    kml_file_name=Landingzone.kml






# Launch site configuration file : 

The launch site configuration file includes the geographic coordinates of all possible take-off landing sites.
The file is simply a list of coordinates and meta-info.

Example of one entry:

    [SITE1]
    # this section sets the data for site number 1
    # longitude [deg], latitude [deg], altitude [m] of landing site 1
    # the id is a identification number
    id = 1
    # longitude in [deg]
    lon = -0.2512
    # latitude in [deg]
    lat = 43.7064
    # altitude in [m]
    alt = 81.0
    # type: choice is: airfield or field or sea-shore.
    type = airfield
    # name of the side
    name = CNES
    # contact telephone number. If none, put whatever like e.g. 0599999999.
    tel = 0612345678

    [SITE2]
    id = 2
    lon = 0.187648
    lat = 43.866949
    alt = 128.0
    type = field
    name = East-1
    tel = 0599999999

etc.



# Payload configuration file : 

The payload configuration file tells missionplan how to deal with the descending phase: either a free fall or a glided fall.

Example of payload-freefall.in

    [MASS]
    # Mass [kg]
    mass_total = 1.80

    [FREEFALL]
    # equivalent sphere radius [m]
    equ_rad = 0.30
    # drag coefficient: 0.47 for a sphere, 0.5 for a cone
    cd = 0.5


## Example of payload-glide.in

    [MASS]
    # Mass [kg]
    mass_total = 1.95

    [FREEFALL]
    # equivalent sphere radius [m]
    equ_rad = 0.62
    # drag coefficient: 0.47 for a sphere, 0.5 for a cone
    cd = 0.5

    [GLIDE]
    # This section is optional.
    # Effective flying altitude [m]: this is the altitude below which the glider can glide.
    glide_alt = 8000.0
    # Effective horizontal velocity [km/h]: Note: it is considered constant over the whole flight regime. In reality it might vary with the altitude.
    glide_vel = 80.0
    # Lift-to-Drag ratio []: Note: it is considered constant over the whole flight regime. In reality it might vary with the altitude.
    lift2drag = 15.0
    # integration time step for gliding [s]: influences the accuracy and execution time of the missionplan script. Default is 10 sec.
    time_step = 10.0
    # minimum altitude margin below which site is considered unreachable [m]
    min_margin = 100.0


