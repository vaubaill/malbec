# InFlight Object definition
import os
import logging
import matplotlib.pyplot as plt
from astropy.coordinates import EarthLocation, cartesian_to_spherical, Longitude, Latitude
import astropy.units as u
from astropy.table import QTable
from astropy.time import Time
import numpy as np
from scipy import interpolate
from pygc import great_distance, great_circle
import simplekml

import utils
from missionplanner.utils import log

class InFlight_Atmosphere (object):
    """Represent the atmoshere state taken from in flight data.
    
    Parameters
    ----------
    pressure: astropy.units.Quantity object
        Pressure in [Pa]
    temperature: astropy.units.Quantity object
        Temperature in [K]
    windF : astropy.units.Quantity object
        Magnitude of wind vector [m/s]
    windD : astropy.units.Quantity object
        Direction of wind vector 
    
    
    max_alt, max_altid
    break_point, break_time
    
    last_point, last_time, last_P_pa, last_T
    dh, trajectory_type: 'ASCEND' or 'DESCEND' or 'AT THE TOP'
    
    
    # TODO: complete the list of parameters!!!!
    
    """
    
    P = 0 *u.Pa
    T = 0 *u.K
    windF  = 0 *u.m/u.s
    windD  = 0 *u.deg 
    max_alt = None
    max_altid = None
    glide_alt = 0.0*u.m
    
    break_ok = False
    break_point = None
    break_time = None
    
    
    
    def __init__(self,data_file,file_format,backup_atm,log_file='inflight_atmosphere.log',glide_alt=0.0*u.m):
        """Instanciates the InFlight_Atmosphere Object.
        
        Parameters
        ----------
        data_file : string
            in flight atmosphere weather file.
        file_format : string
            weather file format type. Choice is: 'CNES2017', 'CNES2018', 'CNES2019', 'ODROID', 'SIMU', 'LORA'
        log_file : string, optional
            log file name. Default is 'inflight_atmosphere.log'.
        glide_alt : astropy.units.Quantity object.
            Altitude below which the trajectory is 'GLIDE' instead of 'FREEFALL'.
            Default is 0.0 [m].
        backup_atm : atmosphere.Atmosphere Object
            Backup Atmosphere object from weather models.
        
        Returns
        -------
        None : 
            InFlight_Atmosphere Object is instanciated.
        
        """
        # store data
        self.glide_alt = glide_alt
        self.backup_atm = backup_atm
        # check log_file
        if log_file:
            self.set_log(log_file)
            log.debug('log file: '+log_file+' ok')
        
        # check data file
        self.datafile = data_file
        self.file_format = file_format
        utils.check_file(self.datafile,log)
        log.debug('Weather data: '+self.datafile+' was found')
        self.check_file_format()
        
        # sets parameters to read data file:
        if (file_format=='CNES2017'):
            names = ['Temps', 'Altitude', 'Latitude', 'Longitude', 'VE', 'VN', 'VVert', 'VHor', 'VDir', 'DP', 'T', 'U', 'Press', 'Flag']  # names for 2017 
            #cols=(0, 9, 18 ,27, 39, 45, 52, 59, 66,  73, 80, 87, 94, 101)   # col for 20171212
            self.data = QTable().read(data_file,format='ascii', names=names)#, col_starts=cols, names=names, delimiter=' ')
            Lchange_time = True
            Lrad = True
            Lchange_temp = True
            Lrename = True
            Lset_units = True
            Ldeg = True
        if (file_format=='CNES2018'):
            names=['Time','Altitude','Latitude','Longitude','VE','VN','Ascent','WindF','WindD','DP','T','U','Press','Flag']     # name for 2018/2019
            #cols=(0, 26, 37, 53, 71, 80, 89, 99, 107, 115, 124, 133, 140, 149)  #  col for 20180627
            self.data = QTable().read(data_file,format='ascii', names=names)#, col_starts=cols, names=names, delimiter=' ')
            Lchange_time = True
            Lrad = True
            Lchange_temp = True
            Lrename = False
            Lset_units = True
            Ldeg = True
        if (file_format=='CNES2019'):
            names=['Time','Altitude','Latitude','Longitude','VE','VN','Ascent','WindF','WindD','DP','T','U','Press','Flag']     # name for 2018/2019
            #cols=(0, 26, 37, 53, 74, 83, 92, 102, 110, 118, 127, 136, 144, 152)  # col for 20190219
            self.data = QTable().read(data_file,format='ascii', names=names)#, col_starts=cols, names=names, delimiter=' ')
            Lchange_time = True
            Lrad = True
            Lchange_temp = True
            Lrename = False
            Lset_units = True
            Ldeg = True
        if (file_format=='SIMU'):
            names = ['Time','Longitude','Latitude','Altitude','Vx','Vy','Vz','U','V','W','relWindU','relWindV','relWindW','sound_speed','Mach','Pressure','Temperature','density']
            log.debug('Reading file: '+data_file)
            self.data = QTable().read(data_file,format='ascii.fixed_width_two_line')
            self.data['Time'] = Time(self.data['Time'],format='isot')
            Lrad = False
            Lchange_time = False
            Lchange_temp = True
            Lrename = False
            Lset_units = True
            Ldeg = True
        if (file_format=='LORA'):
            names = ['Time','Longitude','Latitude','Altitude']
            log.debug('Reading file: '+data_file)
            self.data = QTable(names=names,dtype=['object','float','float','float'])
            self.data = self.data.read(data_file, format='ascii',names=names,delimiter=' ')
            self.data['Latitude'].unit = u.deg
            self.data['Longitude'].unit = u.deg
            self.data['Altitude'].unit=u.m
            self.data['Altitude'] = self.data['Altitude'].to('m')
            self.data['Time'] = Time(self.data['Time'],format='isot')
            # compute missing physical quantities from backup Atmosphere
            self.data = self.compute_wind_from_inflight_data(self.data)
            self.data = self.compute_PT_from_backup(self.data,self.backup_atm)
            self.data.sort('Time')
            # clean data: remove double measurements
            self.data = self.clean_lora_data(self.data)
            all_data_file = os.path.splitext(data_file)[0]+'_W3DPT.dat'
            # set format
            self.data['Latitude'].info.format = '10.6f'
            self.data['Longitude'].info.format = '9.6f'
            self.data['Altitude'].info.format = '6.1f'
            self.data['WindF'].info.format = '5.1f'
            self.data['WindD'].info.format = '5.1f'
            self.data['Press'].info.format = '7.2f'
            self.data['T'].info.format = '5.1f'
            self.data.write(all_data_file,format='ascii.fixed_width_two_line',overwrite=True)
            log.info('Whole data saved in '+all_data_file)
            self.savetraj(all_data_file)
            Lrad = False
            Lchange_time = False
            Lchange_temp = False
            Lrename = False
            Lset_units = False
            Ldeg = False
        
        # change time
        if Lchange_time:
            datestr     = data_file.split('/')[-1].split('-')[0].split('AS')[1]
            time_start  = Time(datestr[0:4]+'-'+datestr[4:6]+'-'+datestr[6:8]+'T00:00:00')
            try:
                self.data.rename_column('Temps','Time')
            except:
                log.debug('Time column already exist')
            self.data['Time'] = time_start + self.data['Time']*u.s
        
        # rename some columns
        if Lrename:
            self.data.rename_column('VHor', 'WindF')
            self.data.rename_column('VDir', 'WindD')
        
        # change temperature
        if Lchange_temp:
            self.data['T']=(self.data['T']+273.0)*u.K
            
        # set units
        if Lset_units:
            self.data['Press'].unit=u.hPa
            self.data['WindF'].unit=u.m/u.s
            self.data['WindD'].unit=u.deg
        self.ndata = len(self.data['T'])
        
        # set angle units to radian
        if Lrad:
            self.data['Latitude'].unit = u.rad
            self.data['Longitude'].unit = u.rad
        if Ldeg:
            self.data['Latitude'].unit = u.deg
            self.data['Longitude'].unit = u.deg
        
        # sort data with time
        self.data.sort('Time')
        
        # set max_alt
        self.get_maxh(mini=True)
        
        # create interpolators objects
        self.create_interpolator()
        self.get_trajectory_type()
        
        return
    
    def clean_lora_data(self,data):
        """
        clean LoRa data, in particular, removes duplicates

        Parameters
        ----------
        data : astropy.table.QTable object.
            Inflight data measurements: time, lon, lat, alt.
            
        Returns
        -------
        data_clean : astropy.table.QTable object.
            Inflight data with redundant entries removed.

        """
        trhdF = 0.01*u.m/u.s
        trhdD = 0.001*u.deg
        mask = (np.abs(data['WindF'])>trhdF) & (np.abs(data['WindD']-180.0*u.deg)>trhdD)
        data_clean = data[mask.nonzero()]
        return data_clean
    
    def compute_wind_from_inflight_data(self,data):
        """Compute horizontal wind from inflight data.
        
        Parameters
        ----------
        data : astropy.table.QTable object.
            Inflight data measurements: time, lon, lat, alt.

        Returns
        -------
        data : astropy.table.QTable object.
            Inflight data augmented with horizontal wind.

        """
        # convert coordinates into EarthLocation object
        elocs = EarthLocation.from_geodetic(lat=data['Latitude'].to('deg'),
                                            lon=data['Longitude'].to('deg'),
                                            height=data['Altitude'].to('m'))
        # compute distance between 2 points
        dist,az = utils.distaz(elocs,np.roll(elocs,-1))
        dt = data['Time']-np.roll(data['Time'],-1)
        # computes 2D velocity vector
        data['WindF'] = -dist.to('m') / dt.to('s')
        data['WindD'] = (az.to('deg')+180.0*u.deg)%(360.0*u.deg)
        return data
    
    def compute_PT_from_backup(self,data,backup_atm):
        """Compute Pressure and Temperature profile from predicted atmosphere.
        
        Note that the Pressure and Temperature are derived for the last [lon,lat].
        
        Parameters
        ----------
        data : astropy.table.QTable object.
            Inflight data measurements: time, lon, lat, alt, WindF, WindD
        
        Returns
        -------
        data : astropy.table.QTable object.
            Inflight data augmented with Pressure and Temperature.
        
        """
        # sort data with altitude
        #data.sort('Altitude')
        # create a P,T QTable
        PT = QTable(names=['Press','T'])
        # set a ramdom location
        (lon,lat) = (data['Longitude'][-1],
                     data['Latitude'][-1])
        
        # first check if the location is covered by the backup Atmosphere
        try:
            backup_atm.check_boundaries(lon,lat,data['Altitude'][-1])
        except UnboundLocalError:
            log.warning('*** Inflight atmosphere out of grib atmosphere lon/lat')
            # set location to middle area
            lon = backup_atm.longitudeOfFirstGridPointInDegrees + (backup_atm.longitudeOfLastGridPointInDegrees - backup_atm.longitudeOfFirstGridPointInDegrees) / 2.0
            lat = backup_atm.latitudeOfFirstGridPointInDegrees + (backup_atm.latitudeOfLastGridPointInDegrees - backup_atm.latitudeOfFirstGridPointInDegrees) / 2.0
            # log.debug('backup_atm.longitudeOfFirstGridPointInDegrees: '+str(backup_atm.longitudeOfFirstGridPointInDegrees))
            # log.debug('backup_atm.longitudeOfLastGridPointInDegrees: '+str(backup_atm.longitudeOfLastGridPointInDegrees))
            # log.debug('backup_atm.latitudeOfFirstGridPointInDegrees: '+str(backup_atm.latitudeOfFirstGridPointInDegrees))
            # log.debug('backup_atm.latitudeOfLastGridPointInDegrees: '+str(backup_atm.latitudeOfLastGridPointInDegrees))
        log.debug('lon: '+str(lon))
        log.debug('lat: '+str(lat))
        # loop over all available altitudes
        for alt in data['Altitude']:
            #log.debug('get P,T from backup Atm for Alt= '+str(alt.to('m')))
            W,P,T = backup_atm.get_windPT(lon,lat,alt)
            if (np.isnan(P) or np.isnan(T)):
                msg = '*** FATAL ERROR: backup atmosphere P or T is NaN'
                log.error(msg)
                raise ValueError(msg)
            log.debug('alt:'+str(alt)+'  P: '+str(P)+'  T: '+str(T))
            PT.insert_row(len(PT),vals=[P,T])
        data['Press'] = PT['Press']
        data['T'] = PT['T']
        data['Press'].unit = u.hPa
        data['T'].unit = u.K
        log.debug(str(data))
        return data
    
    def plot(self,out_dir='./'):
        """Plot the data in file.
        
        Parameters
        ----------
        out_dir : string, optional
            Output directory name.
            Default is './'.
        
        Returns
        -------
        None.
        
        """
        # compute relative time
        t0 = np.min(self.data['Time'])
        time_rel = self.data['Time'] - t0
        self.data['Time_rel'] = time_rel.to('min')
        
        # Plots Quantity vs Time
        qt2plot = ['Longitude','Latitude','Altitude','WindF','WindD']
        qtunits = ['deg','deg','m','m/s','deg']
        for (qtname,unit) in zip(qt2plot,qtunits):
            print('Now ploting '+qtname+' in '+unit+' vs Time')
            fig=plt.figure()
            plt.scatter(self.data['Time_rel'].to('min').value,self.data[qtname].to(unit).value,color='cornflowerblue',s=1,marker='P',label=qtname)
            plt.title(qtname+' vs Time',color='indianred')
            plt.xlabel('Time [min]')
            plt.ylabel(qtname+' ['+unit+']')
            plt.legend(loc='lower right')
            plt.savefig(out_dir+'/'+qtname+'-T-inflight.png',dpi=300)
            plt.close(fig)
        # Plots Quantity vs Altitude
        qt2plot = ['WindF','WindD']
        qtunits = ['m/s','deg']
        for (qtname,unit) in zip(qt2plot,qtunits):
            print('Now ploting '+qtname+' in '+unit+' vs Altitude')
            fig=plt.figure()
            plt.scatter(self.data[qtname].to(unit).value,self.data['Altitude'].to('m').value,color='cornflowerblue',s=1,marker='P',label=qtname)
            plt.title(qtname+' vs Alt',color='indianred')
            plt.xlabel(qtname+' ['+unit+']')
            plt.ylabel('Alt [m]')
            plt.legend(loc='lower right')
            plt.savefig(out_dir+'/'+qtname+'-Alt-inflight.png',dpi=300)
            plt.close(fig)
        return
    
    def savetraj(self,data_file):
        """Save in trajectory measurements into kml and spice kernel files.
        
        Parameters
        ----------
        data_file : string
            Input data file including position measurements.

        Returns
        -------
        None.

        """
        # save in kml format
        kmlfile = data_file.replace('.dat','.kml')
        # convert into kml data and save into output file
        kml = simplekml.Kml()
        coords = []
        for (t,lon,lat,alt) in zip(self.data['Time'],self.data['Longitude'].to('deg').value,self.data['Latitude'].to('deg').value,self.data['Altitude'].to('m').value):
          coords.append((lon,lat,alt))
          pnt = kml.newpoint(name="",coords=[(lon,lat,alt)],extrude=1)
          pnt.altitudemode = simplekml.AltitudeMode.absolute
          pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
          pnt.timestamp.when = t # .isot
        lin = kml.newlinestring(name="Path", description="Path",coords=coords)
        lin.altitudemode = simplekml.AltitudeMode.absolute
        kml.save(kmlfile)
        log.info('KML file saved in '+kmlfile)
        
        return
    
    
    def dict_file_format(self):
        """Returns True if self.flight_type is correct, False otherwise.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        return{
                'CNES2017': True,
                'CNES2018': True,
                'CNES2019': True,
                'ODROID': True,
                'SIMU': True,
                'LORA': True
            }.get(self.file_format, False)
    
    def check_file_format(self):
        """Check the value of flight_type in config file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.debug('file_format: '+self.file_format)
        if not self.dict_file_format():
            msg = '*** FATAL ERROR: file_format is incorrect: '+self.file_format+'. Choice is limited to : CNES2017, CNES2018, CNES2019, ODROID, SIMU or LORA'
            log.error(msg)
            raise ValueError(msg)
        return
    
    def get_maxh(self,mini=False):
        """Get and set the maximum altitude max_alt parameter.
        
        Parameters
        ----------
        mini : logical
            If True, minimum values are also computed.
            Deafult is False.
        
        Returns
        -------
        None.
        
        """
        self.max_alt = np.amax(self.data['Altitude'].to('m'))
        self.max_altid = np.argmax(self.data['Altitude'])
        log.debug('max_alt: '+str(self.max_alt.to('m')))
        if mini:
            self.min_alt = np.amin(self.data['Altitude'].to('m'))
            self.min_altid = np.argmin(self.data['Altitude'])
            log.debug('min_alt: '+str(self.min_alt.to('m')))
            
        return self.max_alt
    
    def get_maxpoint(self):
        """Get the maximum altitude in the real time trajectory file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # set self.max_altid
        if (self.max_alt is None) or (self.max_altid is None):
            self.get_maxh()
        # set maximum point and time
        self.max_point = EarthLocation.from_geodetic(lat=self.data['Latitude'][self.max_altid].to('deg'),
                                                     lon=self.data['Longitude'][self.max_altid].to('deg'),
                                                     height=self.data['Altitude'][self.max_altid].to('m'))
        self.max_time = Time(self.data['Time'][self.max_altid], format = 'isot')
        return (self.max_point,self.max_time)
    
    def get_breakpoint(self):
        """Get the breakpoint when separation occurs (balloon explosion).
        
        Sets the max_altid, break_ok, break_point, break_time parameters.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        break_ok : logical
            True if break has already happened. False otherwise.
        break_point : astropy.coordinates.EarthLocation object
            Location of the break point.
        break_time : astropy.time.Time object
            Time of the break.
        
        """
        # Set break at maximum altitude by default
        (self.break_point,self.break_time) = self.get_maxpoint()
        # if maximum altitude is the last point, break has not happened yet
        if (self.max_altid == self.ndata-1):
            (self.break_point,self.break_time) = (None,None)
            self.break_ok = False
            msg = 'Break has not yet happened'
            log.debug(msg)
        else:
            self.break_ok = True
            log.debug('Break_point: '+str(self.break_point.lon.to('deg').value)+' '+
                                     str(self.break_point.lat.to('deg').value)+' '+
                                     str(self.break_point.height.to('m').value))
            log.debug('Break_time : '+str(self.break_time))
        return (self.break_ok,self.break_point,self.break_time)
     
    def get_lastpoint(self,i=1):
        """Get the latest position.
        
        Sets the last_point, last_time, last_P_pa, last_T parameters.
        
        Parameters
        ----------
        i : int, optional
            index of the i-th last point to provide.
            default is 1, i.e. the very last point.
        
        Returns
        -------
        last_point : astropy.coordinates.EarthLocation object
            Location of last point
        last_time : astropy.time.Time object
            Time of last point
        last_P_pa : astropy.units.Quantity object
            Pressure of last point [Pa]
        last_T : astropy.units.Quantity object
            Temperature of last point [K]
        
        """
        self.last_point = EarthLocation.from_geodetic(lat=self.data['Latitude'][-i].to('deg'),
                                                      lon=self.data['Longitude'][-i].to('deg'),
                                                      height=self.data['Altitude'][-i].to('m'))
        self.last_time = Time(self.data['Time'][-i], format = 'isot')
        self.last_P = self.data['Press'][-i].to('Pa')
        self.last_T = self.data['T'][-i]
        log.debug('last_time : '+str(self.last_time))
        log.debug('last_point: '+str(self.last_point.lon.to('deg').value)+' '+
                                 str(self.last_point.lat.to('deg').value)+' '+
                                 str(self.last_point.height.to('m').value))
        return (self.last_point, self.last_time, self.last_P, self.last_T)
    
    def get_trajectory_type(self):
        """Determine the stage of flight.
        
        Sets the dh and trajectory_type parameters.
        trajectory_type takes the following possible values:
        'ASCEND' for ascending stage, or
        'FREEFALL' for free fall (descending) stage or
        'GLIDE' if the altitude is below self.glide_alt and the payload is descending.
        
        Parameters
        ----------
        None but glide_alt must be set.
        
        Returns
        -------
        trajectory_type : string
            trajectory type.
        
        """
        # sets altitude step
        self.dh = self.data['Altitude'][-1].to('m') - self.data['Altitude'][-2].to('m')
        log.debug('dh='+str(self.dh.to('m')))
        log.debug('self.data[Altitude][-1]='+str(self.data['Altitude'][-1].to('m')))
        log.debug('self.glide_alt='+str(self.glide_alt.to('m')))
        # set trajectory type
        if self.dh < 0.0*u.m:
            if self.data['Altitude'][-1] < self.glide_alt:
                 self.trajectory_type = 'GLIDE'
            else:
                self.trajectory_type = 'FREEFALL'
        else:
            self.trajectory_type = 'ASCEND'
        log.info('trajectory_type : '+self.trajectory_type)
        
        return self.trajectory_type
        
    def get_last_vertical_velocity(self,nlast=4):
        """Sets the velocity of last recorded point in self.initvel parameter.
        
        Parameters
        ----------
        nlast : integer, optional
            Number or points from which the vertical velocity is computed. Default is 4.
            This value may be updated if not enough measurements are present
            in the trajectory file.
        
        Returns
        -------
        self.initvel : 3-vector of float
            Last velocity.
        
        """
        # determine the trajectory type
        self.get_trajectory_type()
        # get break point, if any
        self.get_breakpoint()
        # determine if we are at the top (balloon break)
        if self.break_ok and (self.last_point.x == self.break_point.x):
                self.trajectory_type = str('FREEFALL')
                self.initvel = [0.0, 0.0, 0.0]
                log.info('Break point is the last point = INITIAL VELOCITY IS ZERO')
        else:
            # number of points to consider for the computation of the velocity
            npnts = np.min([nlast,self.ndata-1])
            log.info('Computation of initial velocity from the '+str(npnts)+' last measured point')
            last_times = []
            last_h = np.array([])
            for ipnt in np.arange(npnts)+1:
                (last_point,last_time,last_P,last_T) = self.get_lastpoint(i=ipnt)
                log.debug('ipnt: '+str(ipnt))
                log.debug('last_point.h: '+str(last_point.height.to('m')))
                log.debug('last_time: '+last_time.isot)
                last_times.append(last_time.isot)
                last_h = np.append(last_h,last_point.height.to('m').value)
            log.debug('last_h : '+str(last_h))
            dt = Time(last_times[:-1]) - Time(last_times[1:])
            log.info('dt: '+str(dt.to('s')))
            dh = (last_h[:-1]-last_h[1:])*u.m
            log.info('dh : '+str(dh))
            self.initvel = np.median(dh.to('m')/dt.to('s'))
            log.info('initvel : '+str(self.initvel))
        return self.initvel
    
    def set_log(self,log_file):
        """Set Logger object.
        
        Parameters
        ----------
        log_file : string
            Log file name.
        
        Returns
        -------
        None.
        
        """
        # create FileHandler
        hdlr = logging.FileHandler(log_file)
        hdlr.setLevel(logging.DEBUG)
        hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
        # log file name
        log.info('Log file: '+log_file)
        return
   
    def create_interpolator(self):
        """Create the interpolator objects for wind, pressure and temp
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        Interpolator objects
        
        """
        # construct the interpolator object for the wind
        self.windF_function = interpolate.interp1d(np.asarray(self.data['Altitude'].to('m').value),
                                                   np.asarray(self.data['WindF'].to('m/s').value),fill_value='extrapolate')
                                                   #fill_value=(self.data['WindF'][0].to('m/s').value,
                                                   #            self.data['WindF'][-1].to('m/s').value))    # wind velocity [m/s]
        self.windD_function = interpolate.interp1d(np.asarray(self.data['Altitude'].to('m').value),
                                                   np.asarray(self.data['WindD'].to('rad').value),fill_value='extrapolate')
                                                   #fill_value=(self.data['WindD'][0].to('rad').value,
                                                   #            self.data['WindD'][-1].to('rad').value))    # wind direction [m/s]
        self.Press_function = interpolate.interp1d(np.asarray(self.data['Altitude'].to('m').value),
                                                   np.asarray(self.data['Press'].to('Pa').value) ,fill_value='extrapolate')
                                                   #fill_value=(self.data['Press'][0].to('Pa').value,
                                                   #          self.data['Press'][-1].to('Pa').value))    # pressure [m/s]
        self.Temp_function  = interpolate.interp1d(np.asarray(self.data['Altitude'].to('m').value),
                                                   np.asarray(self.data['T'].to('K').value)      ,fill_value='extrapolate')
                                                   #fill_value=(self.data['T'][0].to('K').value,
                                                   #           self.data['T'][-1].to('K').value))            # temperature [m/s]
        
        return
    
    def get_windPT(self,lon,lat,alt):
        """Get the data for a given location.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            longitude coordinate
        lat : astropy.coordinates.Angle Object
            latitude coordinate
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        Press : astropy.units.Quantity
            Pressure in Pascal [hPa].
        Temp : astropy.units.Quantity
            Temperature [K].
        Wind3D : astropy.units.Quantity object 3-vector.
            Wind vector: i.e. where the wind GOES TO [m/s].
        
        """
        # get max altitude
        (loc_max,time_max) = self.get_maxpoint()
        # if altitude out of bound, query backup Atmosphere
        if (alt>loc_max.height):
            log.debug('Alt: '+str(alt)+' >alt_max '+str(loc_max.height)+' => query backup Atm')
            (Wind3D,Press,Temp) = self.backup_atm.get_windPT(lon,lat,alt)
        else:
            # query interpolation function
            WindF = self.windF_function(alt)*u.m/u.s
            WindD = self.windD_function(alt)*u.rad
            Press = self.Press_function(alt)*u.Pa
            Temp = self.Temp_function(alt)*u.K
            
            Wind3D = [(WindF.to('m/s').value*np.sin(WindD.to('rad').value)),
                      (WindF.to('m/s').value*np.cos(WindD.to('rad').value)),
                      0.0]*u.m/u.s
        
        return (Wind3D,Press.to('hPa'),Temp)
