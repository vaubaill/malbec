"""Live Objects MQTT communication tools.

Authors
-------
J. Vaubaillon, IMCCE, 2019

Project
-------
The MALBEC project.

Version
-------
1.0


"""

import os
import time
import logging
import numpy as np
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import EarthLocation
from configparser import ConfigParser, ExtendedInterpolation
import paho.mqtt.client as mqtt
import json
import requests

from missionplanner.utils import log


# Global constants
# mode d'execution
exe_test_mode = False
# MQTT constants
lo_mqtt_fifo = ""
# number of positions from GPS required before MissionPlanner launch can be decided. This value will be updated from the configuration file.
npos = 0
# current number of positions from GPS required before MissionPlanner launch can be decded
npos_current = 0
# set output file (full) name
trajectory_rlt_file = ""
# mission planner object
missplan = None
# fichier configuration
configfile = ""

# MQTT : the callback for when the client receives a CONNACK response from the server
def on_connect_mqtt(client, userdata, flags, rc):
    global lo_mqtt_fifo
    log.info("On connect MQTT with result code " + str(rc))
    if rc == 0:
        # Subscribing in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed
        log.info("MQTT subscribing to " + lo_mqtt_fifo)
        client.subscribe("fifo/" + lo_mqtt_fifo)

# MQTT : the callback for when a PUBLISH message is received from the server.
def on_message_mqtt_pipo(client, userdata, msg):
    log.info(msg.topic + " --> trashed")

# MQTT : the callback for when a PUBLISH message is received from the server.
def on_message_mqtt(client, userdata, msg):
    global exe_test_mode
    global npos
    global npos_current
    global trajectory_rlt_file
    global missplan
    global configfile
    global missplan
    
    log.info("*************************************************************************")
    log.info("Message MQTT topic " + msg.topic)
    payload = (msg.payload).decode("utf-8")
    #log.debug("payload " + payload)
    json_data = json.loads(payload)
    log.debug("json_data : "+str(json_data))
    streamId = json_data["streamId"]
    log.info("streamId " + streamId)
    npos_current = npos_current + 1
    log.info("metadata " + str(json_data["metadata"]))
    log.info("network " + str(json_data["metadata"]["network"]))
    log.info("lora " + str(json_data["metadata"]["network"]["lora"]))
    fcnt = json_data["metadata"]["network"]["lora"]["fcnt"]
    log.info("MQTT message num" + str(npos_current) + "   -   fcnt " + str(fcnt))
    log.debug("npos current " + str(npos_current) + " / npos " + str(npos))    
    
    # save position/velocity into file
    push_position(json_data, trajectory_rlt_file)
    
    # create MissionPlanner Object
    if missplan is None:
        log.debug('Creating MissionPlanner object')
        missplan = missionplan.MissionPlanner(configfile)
        
    # check if it's time to launch trajectory computation
    go = go_nogo(trajectory_rlt_file,min_npos=npos)
    log.debug('go: ' + str(go))
    if exe_test_mode:
        # ============== TEST CASE ==============
        # create dummy landing position
        land_time = Time('2023-04-14T12:00:35.000', format='isot')
        land_loc = EarthLocation(lon=1.234 * u.deg, lat=45.678901 * u.deg, height=111.0 * u.m)
        log.debug('*** DUMMY landing position ***')
        # get initial condition for debug purpose
        (last_time, last_point, Vvert) = get_initcond(trajectory_rlt_file)
        log.debug('Vvert: '+str(Vvert))
    if go:
        # ============== IN FLIGHT CASE ==============
        log.debug('Now launches Mission Planner')
        (land_time, land_loc) = missplan.launch()
        log.debug('*** Back from Mission Planner ***')
    if go or exe_test_mode:
        log.debug('land_t: ' + land_time.isot)
        log.debug('land_lat: ' + str(land_loc.lat.to('deg')))
        log.debug('land_lon: ' + str(land_loc.lon.to('deg')))
        log.debug('land_alt: ' + str(land_loc.height.to('m')))
        
        # send landing (or dummy) site to LORA card
        success_send = send_land_position(land_time, land_loc.lat, land_loc.lon, land_loc.height)
        log.debug('success_send=' + str(success_send))
    
    log.info("END on message MQTT. Waiting for next message")

    
    
def send_land_position(time,lat, lon, height):
    """Send landing location.
    
    Parameters
    ----------
    time : astrop.time.Time object
        Time
    lon : astropy.units.Quantity object
        Longitude in deg
    lat : astropy.units.Quantity object
       Latitude in deg
    alt : astropy.units.Quantity object
        Altitude in m

    Returns
    -------
    success_send : Boolean
        True if communication successful.    
    
    
    """
    # send landing site position and estimated time
    #msg = make_waze_url(time, lat, lon, height)
    #success_send = send_waze_msg(msg)
    #log.debug('Waze success_send=' + str(success_send))
    success_send = send_baliselora_landing_msg(time, lat, lon, height)
    log.debug('Baliselora success_send=' + str(success_send))
    
    return success_send

def send_waze_msg(msg):
    """Send waze-type message.

    Parameters
    ----------
    msg : string
        Message to send.

    Returns
    -------
    success : Boolean
        True if sending process is successful.

    """
    success = False
    url = "https://api.twilio.com/2010-04-01/Accounts/AC856ab928fae59ede42fc42d21b829795/Messages"

    payload={'To': '+33689465853',
             'From': '+33644609375',
             'Body': msg}
    files=[]
    headers = {
      'Authorization': 'Basic QUM4NTZhYjkyOGZhZTU5ZWRlNDJmYzQyZDIxYjgyOTc5NTo3NDdmOThiYWQ1OTM4MWU4MWM2N2ZkNDI0NjI5OTE3MQ=='
    }
    try:
        response = requests.request("POST", url, headers=headers, data=payload, files=files)
    except:
        msg = '*** FATAL ERROR impossible to send texto'
        log.error(msg)
        raise ValueError(msg)
    
    log.debug(response.text)
    success = True
    return success

def make_waze_url(land_time, land_lat, land_lon, land_alt):
    """Make a waze compatible URL from the predicted landing position.
    
    Parameters
    ----------
    land_time : astrop.time.Time object
        Time
    land_lon : astropy.units.Quantity object
        Longitude in deg
    land_lat : astropy.units.Quantity object
       Latitude in deg
    land_alt : astropy.units.Quantity object
        Altitude in m
    
    Returns
    -------
    url : string
        Waze type URL with landing position.
    
    """
    log.debug('entering make_waze_url')
    # separator
    sep = ','
    waze_time = land_time.isot
    # convert lon,lat, alt into an astropy.units.Quantity object
    waze_lat = str(land_lat.to('deg').value)
    waze_lon = str(land_lon.to('deg').value)
    waze_alt = str(land_alt.to('m').value)
    
    # format the message: must be a long string
    waze_msg = sep.join([waze_lat, waze_lon, waze_alt,waze_time]) + '\n'
    log.debug('waze_msg: ' + waze_msg)
    
    # make Waze URL ; ex : https://www.waze.com/ul?ll=44%2C2.2&navigate=yes&zoom=17
    url='https://waze.com/ul?ll='+sep.join([waze_lat,'%2C',waze_lon,'&navigate=yes&zoom=17'])
    
    log.info('URL : '+url)
    
    return url

def send_baliselora_landing_msg(time,lat,lon,alt):
    """
    

    Parameters
    ----------
    time : astrop.time.Time object
        Time
    lon : astropy.units.Quantity object
        Longitude in deg
    lat : astropy.units.Quantity object
       Latitude in deg
    alt : astropy.units.Quantity object
        Altitude in m.

    Returns
    -------
    success : boolean
        Message success.

    """
    server = 'https://dev.baliselora.fr/pushhttp/virtuel.php?deveui=0004A30B001EF562'
    # url = server + '&lat=' + str(lat.to('deg').value) + \
    #                 '&lng=' + str(lon.to('deg').value) + \
    #                 '&alt=' + str(alt.to('m').value)
    url = server + '&lat=' + "{:6.6f}".format(lon.to('deg').value) + \
                    '&lng=' + "{:6.6f}".format(lon.to('deg').value) + \
                    '&alt=' + "{:6.0f}".format(alt.to('m').value)
    req = requests.get(url)
    
    # check request
    req.raise_for_status()
    # check request success
    success = req.status_code == requests.codes.ok
    if not success:
        log.debug('request status code: ',req.status_code)
        log.debug('request headers:',req.headers)
        log.debug('request text:',req.text)
        log.debug('request content:',req.content)
    else:
        log.debug('Baliselora push request successful')
    
    return success

def send_baliselora_landing_msg_old(time,lat,lon,alt):
    """Send message to communicate landing location.
    
    Location is accessible at https://suividevol.microtrak.fr.

    Parameters
    ----------
    time : astrop.time.Time object
        Time
    lon : astropy.units.Quantity object
        Longitude in deg
    lat : astropy.units.Quantity object
       Latitude in deg
    alt : astropy.units.Quantity object
        Altitude in m

    Returns
    -------
    success : boolean
        Message success.

    """
    #url = 'https://www.baliselora.fr/pushhttp/pushhttp.php'
    url = 'https://dev.baliselora.fr/pushhttp/pushhttp.php'
    headers = {'Content-Type':'application/json',
               'Accept':'application/json',
               'X-Apikey':'MaKeyApi2020'}
    data = {"type":"dataMessage",
            "version":1,
            "streamId":"urn:lora:0004A30B001EF562!uplink",
            "timestamp":time.isot + 'Z',
            "location":{"lat":lat.to('deg').value,
                        "lon":lon.to('deg').value,
                        "alt":alt.to('m').value,
                        "accuracy":5000.0,
                        "provider":"lora"},
            "model":"model_abeeway_microtracker_v1",
            "value":{"payload":"02fe",
                     "triggerEvent":"TRACKER_MODE",
                     "battery":{"charging":"false",
                                "unmeasurable":"false",
                                "energyLevel":254},
                     "messageType":"TRACKER_POSITION",
                     "trackerMode":"CLASSIC",
                     "location":{"lat":lat.to('deg').value,
                                 "lon":lon.to('deg').value,}
                     },
            "tags":["sitedev"],
            "extra":{},
            "metadata":{"source":"urn:lo:nsid:lora:0004A30B001EF562",
                        "group":{"id":"root",
                                 "path":"/"},
                        "encoding":"abeeway_microtracker_v1.0",
                        "connector":"lora",
                        "network":{"lora":{"devEUI":"0004A30B001EF562",
                                           "port":10,
                                           "fcnt":5582,
                                           "rssi":-101.0,
                                           "snr":-7.0,
                                           "esp":-108.79,
                                           "sf":10,
                                           "frequency":866.7,
                                           "signalLevel":2,
                                           "ack":"false",
                                           "messageType":"UNCONFIRMED_DATA_UP",
                                           "location":{"lat":lat.to('deg').value,
                                                       "lon":lon.to('deg').value,
                                                       "alt":alt.to('m').value,
                                                       "accuracy":5000.0},
                                           "gatewayCnt":1}
                                   }
                        }
            }
            
    log.debug('data: '+json.dumps(data))
    log.debug('Sending data to '+url)
    log.debug('Whole url: '+''.join([url,json.dumps(data)]).replace(' ',''))
    #req = requests.post(url,json.dumps(data),headers=headers)
    req = requests.post(url,data=data,headers=headers)
    
    # check request
    req.raise_for_status()
    # check request success
    success = req.status_code == requests.codes.ok
    if not success:
        log.debug('request status code: ',req.status_code)
        log.debug('request headers:',req.headers)
        log.debug('request text:',req.text)
        log.debug('request content:',req.content)
    else:
        log.debug('Baliselora push request successful')
    return success


# get MissionPlanner configuration file from argument
def launch_liveobjects(config_file, test_mode):
    """Launches the communication with Live Objects and run mission planner when needed.
    
    Parameters
    ----------
    config_file : string
        Configuration file for MissionPlanner1.1 object.
    test_mode : boolean
        If True, MissionPlanner is not run and a dummy position is sent back to lora card.
    
    """
    global lo_mqtt_fifo
    global exe_test_mode
    global trajectory_rlt_file
    global npos
    global missplan
    global configfile
    configfile = config_file
    exe_test_mode = test_mode
    if not os.path.exists(config_file):
        msg = '*** FATAL ERROR: config file ' + config_file + ' does not exist'
        raise IOError(msg)
    
    # create ConfigParser Object
    config = ConfigParser(interpolation=ExtendedInterpolation())
    # read configuration file
    config.read(config_file)
    # set the home variable
    config['USER']['home'] = os.getenv('HOME')
    # create and add FileHandler
    strt = Time.now().isot.replace('-','').replace(':','').split('.')[0]
    log_file = config['CAMPAIGN']['log_file_root'].replace('YYYYMMDDTHHMMSS',strt).replace('-','-Liveobj-')
    hdlr = logging.FileHandler(log_file)
    hdlr.setLevel(logging.DEBUG)
    hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    
    # set outut file (full) name
    trajectory_rlt_file = config['CAMPAIGN']['trajectory_rlt_file']
    log.info('Trajectory realtime position file: ' + trajectory_rlt_file)
    # check that directory
    if not (os.path.exists(os.path.dirname(trajectory_rlt_file))):
        log.warning('*** Making trajectory file path: '+os.path.dirname(trajectory_rlt_file))
        os.mkdir(os.path.dirname(trajectory_rlt_file))
    # set nb positions
    npos = int(config['LO']['nbpos'])
    log.info('Nb positions needed to launch MissionPlanner: ' + str(npos))
    
    # MQTT initialization
    log.debug('MQTT vidange')
    mqtt_port = int(config['LO']['port'])
    mqtt_keepalive = int(config['LO']['keepalive'])
    lo_mqtt_fifo = config['LO']['fifo']
    
    client_pipo = mqtt.Client()
    client_pipo.on_connect = on_connect_mqtt
    client_pipo.on_message = on_message_mqtt_pipo
    client_pipo.username_pw_set(config['LO']['username'], password=config['LO']['password'])
    client_pipo.tls_set()
    client_pipo.connect(config['LO']['ip'], mqtt_port, mqtt_keepalive)
    client_pipo.loop_start()
    # Wait for 10 seconds
    log.debug('LiveObject is waiting for 10 sec...')
    time.sleep(10)
    client_pipo.loop_stop(force=False)
    client_pipo.disconnect()
    
    log.debug('MQTT initialization')
    mqtt_client = mqtt.Client()
    mqtt_client.on_connect = on_connect_mqtt
    mqtt_client.on_message = on_message_mqtt
    mqtt_client.username_pw_set(config['LO']['username'], password=config['LO']['password'])
    log.debug('MQTT connect')
    mqtt_client.connect(config['LO']['ip'], mqtt_port, mqtt_keepalive)
    
    # Blocking call that processes network traffic, dispatches callbacks and handles reconnecting
    mqtt_client.loop_forever()
        
    return True


def push_position(json_data, out_file):
    """Get one position and push it into output file.
    
    Parameters
    ----------
    json_data : data sent by the device
        Json object
    out_file : string
        Output file where the positions will be appended.
    
    Returns
    -------
    flight_mode : int
        Flight mode. 0: groundm 50: flight, 100: descending.
    """
    # get latest position
    log.debug('getting position from live objects')
    (time_lo,lon_lo, lat_lo, alt_lo) = get_position_from_liveobjects(json_data)
    
    # puts latest position into output file
    log.debug('put last position into ' + out_file)
    put_latest_position_into_file(out_file, time_lo, lon_lo, lat_lo, alt_lo)
    return

def get_position_from_liveobjects_old(json_data):
    """Get the current position from live objects.
    
    Parameters
    ----------
    json_data : data sent by the device
        Json object
    
    Returns
    -------
    time_lora : astropy.time.Time object
        Time
    lon_lo : astropy.units.Quantity object
        Longitude in deg
    lat_lo : astropy.units.Quantity object
       Latitude in deg
    alt_lo : astropy.units.Quantity object
        Altitude in m
    flight_mode : int
        Flight mode. 0: groundm 50: flight, 100: descending.
    """
    # communication with Live Object
    # example of json data
    # { "timestamp":"2019-11-26T16:20:40.778Z", "value":{"payload":"0010141d1675d3015e2df00001ced7","hour":16.0,"lati":48.8011219,"longi":2.294936,"alti":118.487,"frame":0.0,"minute":20.0}}
    # log.debug('json:' + str(json_data))
    time_lo = json_data["timestamp"]
    # withdraw of the last char Z
    time_lo = time_lo[:-1]
    #flight_mode = json_data["value"]["fmode"]
    lat_lo = json_data["value"]["la"]
    lon_lo = json_data["value"]["lo"]
    alt_lo = json_data["value"]["msl"]
    log.debug('time: ' + time_lo +  ' '+" lon: "+str(lon_lo) + " lat: "+str(lat_lo) + " alt: "+str(alt_lo))
    #  returns latest position
    return time_lo, lon_lo, lat_lo, alt_lo

def get_position_from_liveobjects(json_data):
    """Get the current position from live objects.
    
    Parameters
    ----------
    json_data : data sent by the device
        Json object
    
    Returns
    -------
    time_lora : astrop.time.Time object
        Time
    lon_lo : astropy.units.Quantity object
        Longitude in deg
    lat_lo : astropy.units.Quantity object
       Latitude in deg
    alt_lo : astropy.units.Quantity object
        Altitude in m
    flight_mode : int
        Flight mode. 0: groundm 50: flight, 100: descending.
    """
    # communication with Live Object
    # example of json data
    # { "timestamp":"2019-11-26T16:20:40.778Z", "value":{"payload":"0010141d1675d3015e2df00001ced7","hour":16.0,"lati":48.8011219,"longi":2.294936,"alti":118.487,"frame":0.0,"minute":20.0}}
    # log.debug('json:' + str(json_data))
    time_lo = json_data["timestamp"]
    # withdraw of the last char Z
    time_lo = time_lo[:-1]
    #flight_mode = json_data["value"]["fmode"]
    lat_lo = json_data["location"]["lat"]
    lon_lo = json_data["location"]["lon"]
    alt_lo = json_data["value"]["gpsExtraData"]["altitude"]["value"]
    log.debug('time: ' + time_lo +  ' '+" lon: "+str(lon_lo) + " lat: "+str(lat_lo) + " alt: "+str(alt_lo))
    #  returns latest position
    return time_lo, lon_lo, lat_lo, alt_lo

def put_latest_position_into_file(filename, time_lo, lon_lo, lat_lo, alt_lo):
    """Put latest position at the end of a trajectory file.
    
    Parameters
    ----------
    filename : string
        output file name (full path)
    time_lo : string
        Time iso format
    lon_lo : int
        Longitude in deg
    lat_lo : int
       Latitude in deg
    alt_lo : int
        Altitude in m
    
    Returns
    -------
    None.
    
    """
    # double check existence of file and set write mode
    if not os.path.exists(filename):
        openmode = 'w'
        log.info('File ' + filename + ' will be created')
    else:
        openmode = 'a+'
    
    # write into file
    try:
        with open(filename, openmode) as f:
            f.write(time_lo + ' ' + '{:.7f}'.format(lon_lo) + ' ' + '{:.7f}'.format(lat_lo) + ' ' + '{:.0f}'.format(alt_lo)+ '\n')
            log.debug('latest position saved in ' + filename)
    # handle all possible errors
    except(IOError):
        msg = '*** FATAL ERROR: impossible to save into ' + filename
        log.error(msg)
        raise IOError(msg)
    except(ValueError):
        msg = '*** Value Error: double check every variable.'
        log.error('time = ' + time_lo)
        log.error('lon = ' + str(lon_lo))
        log.error('lat = ' + str(lat_lo))
        log.error('alt = ' + str(alt_lo))
        raise ValueError(msg)
    except:
        msg = '*** FATAL ERROR: something wrong happened while trying to put position into '+filename
        raise ValueError(msg)
    
    return

def read_lora_traj(trajectory_rlt_file):
    """Read lora trajectory file.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file.
    
    Returns
    -------
    data : astropy.table.QTable
        All data in trajectory_rlt_file.
    
    """
    log.debug('Reading: ' + trajectory_rlt_file)
    # read trajectory file and set units
    data = QTable().read(trajectory_rlt_file, format='ascii', names=['Time', 'lon', 'lat', 'alt'])
    data.sort('Time')
    data['lon'].unit = u.deg
    data['lat'].unit = u.deg
    data['alt'].unit = u.m # was mm in previous version
    data['alt'] = data['alt'].to('m')
    # convert time into Time object
    time = Time(data['Time'], format='isot')
    data['Time'] = time
    #log.debug('Data: '+str(data))
    return data


def go_nogo(trajectory_rlt_file,min_npos=4):
    """Decide to launch MissionPLanner.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file.
    min_npos : int
        Minimum number of position needed to launch missionplanner. Default is 4.
    
    Returns
    -------
    launch : boolean
        True if descending phase. False otherwise.
    
    """
    log.debug('min_npos '+str(min_npos))
    if not os.path.exists(trajectory_rlt_file):
        msg = 'FATAL ERROR: file '+trajectory_rlt_file+' does not exist'
        log.error(msg)
        raise IOError(msg)
    # read trajectory file and set units
    data = read_lora_traj(trajectory_rlt_file)
    # get number of lines
    dim = len(data['Time'])
    if dim<min_npos:
        msg = 'Warning: only '+str(dim)+' data in '+trajectory_rlt_file+' is not enough'
        log.error(msg)
        launch = False
    else:
        log.debug('There are '+str(dim)+' data in '+trajectory_rlt_file)
        log.debug('Last 4 data: ')
        log.debug(str(data['Time'][-min_npos:]))
        log.debug(str(data['alt'][-min_npos:]))
        # compute time and altitude difference
        dt = data['Time'][-min_npos+1:] - data['Time'][-min_npos:-1]
        da = data['alt'][-min_npos+1:] - data['alt'][-min_npos:-1]
        log.debug('dt='+str(dt))
        log.debug('da='+str(da))
        # compute vertical velocity
        Vvert = da / dt
        log.info('Vvert = ' + str(Vvert.to('m/s')))
        # if vertical velocity is negative the fall has started => time to predict landing point.
        if np.all(Vvert < 0.0 * u.m / u.s):
            launch = True
        else:
            launch = False
            log.warning('*** Not ALL Vvert are < 0.0 m/s, waiting for descend.')
    return launch

def compute_Vvert(data):
    """Compute Vertical velocity from altitude data.
    
    Parameters
    ----------
    data : astropy.table.QTable
        All data in trajectory_rlt_file.
    
    Returns
    -------
    Vvert : astropy.coordinates.Quantity Object
        Vertical velocity in [m/s]
    
    """
    # get data dimension
    dim = len(data)
    if dim<2:
        Vvert = 0.0*u.m/u.s
    else:
        #log.debug('data='+str(data))
        Vvert = (data['alt'][-1] - data['alt'][-2]) / (data['Time'][-1] - data['Time'][-2])
    log.debug('Vvert: '+str(Vvert.to('m/s')))
    return Vvert.to('m/s')
    
def get_initcond(trajectory_rlt_file):
    """Get initial conditions.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file from lora card measurements.
    
    Returns
    -------
    last_point : astropy.coordinates.EarthLocation object
        Last measured point.
    last_time : astropy.time.Time object
        Tim of last point.
    Vvert : astropy.units.Quantity object
        Median vertical velocity in [m/s].
    
    """
    log.debug('Reading '+trajectory_rlt_file)
    # read trajectory file and set units
    data = read_lora_traj(trajectory_rlt_file)
    # set last time
    last_time = data['Time'][-1]
    # set last position
    last_point = EarthLocation(lon=data['lon'][-1],
                               lat=data['lat'][-1],
                               height=data['alt'][-1])
    # compute Vertical velocity
    Vvert = compute_Vvert(data)
    # get vertical velocity sign
    # if Vvert==0:
    #     Vsgn = 1.0
    # else:
    #     Vsgn = -1.0
    # Vvert = Vsgn * Vvert
    # get vertical velocity absolute value
    Vvert = [0.0, 0.0, Vvert.to('m/s').value] * u.m / u.s
    
    log.debug('last_time '+str(last_time))
    log.debug('last_point '+str(last_point))
    log.debug('Vvert '+str(Vvert))
    
    return (last_time, last_point, Vvert)
