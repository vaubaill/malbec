"""MALBEC Pod Trajectory object.

Author
------
J. Vaubaillon, IMCCE, 2019

"""
import os
import shutil
import simplekml
import numpy as np
import matplotlib.pyplot as plt
from astropy.table import QTable
from astropy.time import Time
import astropy.units as u
from astropy.coordinates import EarthLocation, cartesian_to_spherical

from missionplanner.utils import log

class Trajectory (object):
    """Trajectory object.
    
    A Trajectory object contains all the trajectory parameters of a balloon nacelle during its flight.
    
    Parameters
    ----------
    TBF
    
    """
    # number of points in the data
    npts = 0
    # relative time ; array of astropy.time.Time object
    time_rel = None
    # array of astropy.coordinates.EarthLocation object
    eloc = None
    
    def __init__(self):
        # array of string objects
        times = '2020-01-01T00:00:00.000'
        # initializes with stupid values on purpose
        lons = -9.0
        lats = -9.0
        alts = -9.0
        velx = -9.0
        vely = -9.0
        velz = -9.0
        windU = -9.0
        windV = -9.0
        windW = -9.0
        relWindU = -9.0
        relWindV = -9.0
        relWindW = -9.0
        sound_speed = -9.0
        Mach = -9.0
        press = -9.0
        temp = -9.0
        density = -9.0
        
        names = ['Time','Longitude','Latitude','Altitude','Vx','Vy','Vz','U','V','W',
                 'relWindU','relWindV','relWindW','sound_speed','Mach',
                 'Pressure','Temperature','density']
                 #,'Radius','Azimuth','zenith_angle']
        
        self.data = QTable([[times],[lons],[lats],[alts],
                           [velx],[vely],[velz],
                           [windU],[windV],[windW],
                           [relWindU],[relWindV],[relWindW],
                           [sound_speed],[Mach],
                           [press],[temp],[density]],   # [azimuth],[zenith_angle],
                            names=names,
                            dtype = ('str', 'float','float','float',
                                    'float','float','float',
                                    'float','float','float',
                                    'float','float','float',
                                    'float','float',
                                    'float','float','float'))
        self.data.remove_row(0)
        return
    
    def add_data(self,data):
        """Add
        
        Parameters
        ----------
        data : 1D numpy array
            Data to add to the PodTra object.
        
        Returns
        -------
        None.
        
        
        """
        self.data.add_row(data)
        self.npts = self.npts + 1
        return
    
    def compute_eloc(self):
        """Compute astropy.coordinates.EarthLocation from data.
        
        This is helpful in particular for the conversion to SPICE spk file.
        Note that the self.data table must be filled in.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        if not self.npts:
            msg = 'WARNING: there is no data in Trajectory Object.'
            log.warning(msg)
        else:
            # convert lon,lat,alt into EarthLocation
            self.eloc = EarthLocation.from_geodetic(self.data['Longitude'], self.data['Latitude'], height=self.data['Altitude'])
        # for completion purpose, also computes the velocity Vloc
        (self.Vloc_x,self.Vloc_y,self.Vloc_z) = self.compute_Vloc()
        return
    
    def load(self,filein):
        """Load data from a file.
        
        Parameters
        ----------
        filein : string
            Data file name to load.
        
        Returns
        -------
        None.
        
        """
        if not os.path.exists(filein):
            msg = '*** FATAL ERROR: file '+filein+' does not exist.'
            raise IOError(msg)
        log.info('Loading trajectory file: '+filein)
        self.data = QTable.read(filein,format='ascii.fixed_width_two_line')
        self.set_units()
        self.npts = len(self.data['Time'])
        log.info(str(self.npts)+' trajectory points loaded from '+filein)
        return
    
    def set_units(self):
        """Set units for all columns.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        self.data['Longitude'].unit = 'deg'
        self.data['Latitude'].unit = 'deg'
        self.data['Altitude'].unit = 'm'
        self.data['Vx'].unit = 'm/s'
        self.data['Vy'].unit = 'm/s'
        self.data['Vz'].unit = 'm/s'
        self.data['U'].unit = 'm/s'
        self.data['V'].unit = 'm/s'
        self.data['W'].unit = 'm/s'
        self.data['relWindU'].unit = 'm/s'
        self.data['relWindV'].unit = 'm/s'
        self.data['relWindW'].unit = 'm/s'
        self.data['sound_speed'].unit = 'm/s'
        #self.data['Mach'].unit = ''
        self.data['Pressure'].unit = 'Pa'
        self.data['Temperature'].unit = 'K'
        self.data['density'].unit = 'kg/m3'
        
        # set format
        self.data['Longitude'].info.format = '10.5f'
        self.data['Latitude'].info.format = '10.5f'
        self.data['Altitude'].info.format = '8.1f'
        self.data['Vx'].info.format = '7.3f'
        self.data['Vy'].info.format = '7.3f'
        self.data['Vz'].info.format = '7.3f'
        self.data['U'].info.format = '7.3f'
        self.data['V'].info.format = '7.3f'
        self.data['W'].info.format = '7.3f'
        self.data['relWindU'].info.format = '7.3f'
        self.data['relWindV'].info.format = '7.3f'
        self.data['relWindW'].info.format = '7.3f'
        self.data['sound_speed'].info.format = '7.3f'
        self.data['Mach'].info.format = '7.3f'
        self.data['Pressure'].info.format = '9.3f'
        self.data['Temperature'].info.format = '7.3f'
        self.data['density'].info.format = '7.3f'
        return
    
    def to_kml(self,kmlfile):
        """Convert location into kml data and save into kml file.
        
        Parameters
        ----------
        kmlfile : string
            KML file full name.
        
        Returns
        -------
        None.
        
        """
        # convert into kml data and save into output file
        kml = simplekml.Kml()
        coords = []
        for (t,lon,lat,alt) in zip(self.data['Time'],self.data['Longitude'].to('deg').value,self.data['Latitude'].to('deg').value,self.data['Altitude'].to('m').value):
          coords.append((lon,lat,alt))
          pnt = kml.newpoint(name="",coords=[(lon,lat,alt)],extrude=1)
          pnt.altitudemode = simplekml.AltitudeMode.absolute
          pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
          pnt.timestamp.when = t # .isot
        lin = kml.newlinestring(name="Path", description="Path",coords=coords)
        lin.altitudemode = simplekml.AltitudeMode.absolute
        kml.save(kmlfile)
        return
    
    def compute_Vloc(self):
        """Computes cartesian velocity from EarthLocation coordinates.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        Vloc : list of 3-astropy.units.quantity.Quantity Object.
            Cartesian Velocity from astropy.coordinates.EarthLocation object.
        
        """
        # compute eloc if necessary
        if not self.eloc:
            self.compute_eloc()
        # copy time and position into local array
        pos = [self.eloc.x.to('km').value,
               self.eloc.y.to('km').value,
               self.eloc.z.to('km').value]
        t_cur = Time(self.data['Time'])
        t_prec = Time(np.roll(t_cur,-1))
        pos_prec = np.roll(pos,-1)
        # compute relative coordinates
        t_rel = (t_cur - t_prec).to('s').value
        p_rel = pos - pos_prec
        # compute cartesian velocity
        Vloc = p_rel / t_rel
        # change boundaries
        Vloc[0][0] = Vloc[0][1]
        Vloc[1][0] = Vloc[1][1]
        Vloc[2][0] = Vloc[2][1]
        Vloc[0][self.npts-1] = Vloc[0][self.npts-2]
        Vloc[1][self.npts-1] = Vloc[1][self.npts-2]
        Vloc[2][self.npts-1] = Vloc[2][self.npts-2]
        return (Vloc[0]*u.km/u.s,Vloc[1]*u.km/u.s,Vloc[2]*u.km/u.s)
    
    def to_spk(self,spice_name,spice_id,spk_file,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',tmpdir='/tmp/'):
        """Convert trajectory into SPICE spk file.
        
        Parameters
        ----------
        spice_id : integer
            SPICE id number of the object. See SPICE documentation for more info.
        spk_file : string
            SPICE kernel spk file full name.
        kernel_path : string, optional
            SPICE kernel files directory full name.
            Default is '/astrodata/kernels/'
        spice_exe_path : string, optional
            SPICE kernel executables directory full name.
            Default is '/astrodata/exe/'
        tmpdir : string, optional
            Temporary directory where files are created. This is useful if the path to the files
            are very long, since it causes the mkspk utility program to crash.
            Temporary files (e.g. trajectory in ascii format) andoutput files are first
            created in tmpdir directory, and then the output is saved in spk_file.
            Default is '/tmp/'.
        
        Returns
        -------
        None.
        
        """
        # first compute EarthLocation and Vloc
        try:
            junk = self.eloc.x.to('km').value + self.Vloc.to('km/s').value
            log.debug('no need to compute eloc. junk='+str(junk))
        except:
            self.compute_eloc()
        # creates the ephemeris data file
        names=['EPOCH','X','Y','Z','VX','VY','VZ']
        # get intermediate spk_dta file name
        data_file = tmpdir+os.path.basename(spk_file.replace('.spk','_spk.dat'))
        # intermediate setup file name
        setup_file = tmpdir+os.path.basename(spk_file.replace('.spk','.setup'))
        # intermediate output file
        tmpout_file = tmpdir+os.path.basename(spk_file)
        # double check that we are not about the erase an existing file
        for f2chk in [data_file,setup_file]:
            if f2chk==spk_file:
                msg = 'FATAL ERROR: '+spk_file+' is about to be erased: double check file name and make sure it contains spk'
                log.error(msg)
                raise ValueError(msg)
        log.debug('spk_file: '+spk_file)
        log.debug('data_file: '+data_file)
        log.debug('setup_file: '+setup_file)
        log.debug('tmpout_file: '+tmpout_file)
        
        # srt data with time
        self.data.sort(['Time'])
        # write intermediate raw spk data
        with open(data_file,'w') as out:
            out.write("# "+' '.join(names)+" \n")
            for (t,x,y,z,vx,vy,vz) in zip(self.data['Time'],
                          self.eloc.x.to('km').value,
                          self.eloc.y.to('km').value,
                          self.eloc.z.to('km').value,
                          self.Vloc_x.to('km/s').value,
                          self.Vloc_y.to('km/s').value,
                          self.Vloc_z.to('km/s').value):
                # write time
                out.write(Time(t,scale='utc').iso+"\n")
                # format data
                xstr = "{:.9f}".format(x)
                ystr = "{:.9f}".format(y)
                zstr = "{:.9f}".format(z)
                vxstr = "{:.9f}".format(vx)
                vystr = "{:.9f}".format(vy)
                vzstr = "{:.9f}".format(vz)
                # write data
                out.write(' '.join([xstr,ystr,zstr,vxstr,vystr,vzstr])+" \n")
        log.info("raw spk data saved in: "+data_file)
        
        # make the intermediate setup file
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
            out.write("   OUTPUT_SPK_TYPE   = 13\n")
            out.write("   OBJECT_ID         = "+str(spice_id)+"\n")
            out.write("   OBJECT_NAME       = "+repr(spice_name)+"\n")
            out.write("   CENTER_ID         = 399\n")
            out.write("   REF_FRAME_NAME    = "+repr('ITRF93')+"\n")
            out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
            out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
            out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
            out.write("   LEAPSECONDS_FILE  = "+repr(kernel_path+'naif0012.tls')+"\n")
            out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
            out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
            out.write("   PCK_FILE          = "+repr(kernel_path+'pck00010.tpc')+"\n")
            out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
            out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
            out.write("   IGNORE_FIRST_LINE = 1 \n")
            out.write("   LINES_PER_RECORD  = 2 \n")
            out.write("   POLYNOM_DEGREE    = 3 \n")
            out.write("   SEGMENT_ID        = "+repr(str(spice_id))+"\n")
            out.write(" \n")
        log.info("setup saved in: "+setup_file)
        
        # remove spk file if it already exists
        if os.path.exists(tmpout_file):
            os.remove(tmpout_file)
        # build the command
        cmd = spice_exe_path+"mkspk -setup "+setup_file
        log.info("cmd="+cmd)
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
            log.error(msg)
            raise IOError(msg)
        # double check that the SPK file was successfully created
        if not os.path.exists(tmpout_file):
            msg='*** FATAL ERROR: temporary SPK file: '+spk_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        # now copy temporary output file into spk_file
        outdir = os.path.dirname(spk_file)+'/'
        for file2move in [setup_file,data_file,tmpout_file]:
            dst = outdir+os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            log.debug('Moving file '+file2move+' into '+dst)
            try:
                # remark: os.remove does not always work when trying to move file from one FS to another...
                shutil.copy(file2move,dst)
            except:
                msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
                log.error(msg)
                raise IOError(msg)
        log.info("SPICE SPK kernel saved in: "+spk_file)
        return
    
    
    def save(self,outfile):
        """Save all the physical parameters into a file.
        
        Parameters
        ----------
        outfile : string
            Output file full name.
            
        Returns
        -------
        None.
        
        """
        self.set_units()
        #if self.data['Mach'][0]==-9.0:
        #    self.data.remove_row(0)
        self.compute_relative_time()
        # data foramt
        self.data.write(outfile,format='ascii.fixed_width_two_line',
                        formats={'Time':'%23s'},overwrite=True)
        return
    
    def compute_relative_time(self,t0=None):
        """Compute relative time and set self.time_rel paramter.
        
        Parameters
        ----------
        t0 : astropy.time.Time object, optional
            reference time from which relative time is computed.
            Default is None, meaning that the minimum time of the Trajectory object is considered as reference time.
        
        Returns
        -------
        None.
        """
        Times = Time(self.data['Time'],format='isot')
        if t0 is None:
            t0 = np.min(Times)
        time_rel = Times - t0
        self.data['Time_rel'] = time_rel.to('min')
        self.data['Time_rel'].info.format = '6.2f'
        return
    
    def plot_all(self,out_dir):
        """Makes plot of all quantities.
        
        Parameters
        ----------
        outdir : string
            Name of directory where plots will be saved.
        
        Returns
        -------
        None.
        """
        # compute relative time
        self.compute_relative_time()
        
        # Plots Quantity vs Time
        qt2plot = ['Longitude','Latitude','Altitude','Vx','Vy','Vz']
        qtunits = ['deg','deg','m','m/s','m/s','m/s']
        for (qtname,unit) in zip(qt2plot,qtunits):
            log.info('Now ploting '+qtname+' in '+unit+' vs Time')
            fig=plt.figure()
            plt.scatter(self.data['Time_rel'].to('min').value,self.data[qtname].to(unit).value,color='cornflowerblue',s=1,marker='P',label=qtname)
            plt.title(qtname+' vs Time',color='indianred')
            plt.xlabel('Time [min]')
            plt.ylabel(qtname+' ['+unit+']')
            plt.legend(loc='lower right')
            plt.savefig(out_dir+'/'+qtname+'-T.png',dpi=300)
            plt.close(fig)
        
        # Plots Quantity vs Altitude
        qt2plot = ['Vx','Vy','Vz','U','V','W','relWindU','relWindV','relWindW','sound_speed','Pressure','Temperature','density']
        qtunits = ['m/s','m/s','m/s','m/s','m/s','m/s','m/s','m/s','m/s','m/s','Pa','K','kg/m3']
        for (qtname,unit) in zip(qt2plot,qtunits):
            log.info('Now ploting '+qtname+' in '+unit+' vs Altitude')
            fig=plt.figure()
            plt.scatter(self.data[qtname].to(unit).value,self.data['Altitude'].to('m').value,color='cornflowerblue',s=1,marker='P',label=qtname)
            plt.title(qtname+' vs Alt',color='indianred')
            plt.xlabel(qtname+' ['+unit+']')
            plt.ylabel('Alt [m]')
            plt.legend(loc='lower right')
            plt.savefig(out_dir+'/'+qtname+'-Alt.png',dpi=300)
            plt.close(fig)
        # compute and plot wind forace and direction
        (windF,windL,windD )= cartesian_to_spherical(self.data['U'],self.data['V'],self.data['W'])
        windD = (windD.to('deg')+180.0*u.deg)%(360.0*u.deg)
        qt2plot = ['WindF','WindD']
        qtunits = ['m/s','deg']
        for (data,qtname,unit) in zip([windF,windD],qt2plot,qtunits):
            log.info('Now ploting '+qtname+' in '+unit+' vs Altitude')
            fig=plt.figure()
            plt.scatter(data.to(unit).value,self.data['Altitude'].to('m').value,color='cornflowerblue',s=1,marker='P',label=qtname)
            plt.title(qtname+' vs Alt',color='indianred')
            plt.xlabel(qtname+' ['+unit+']')
            plt.ylabel('Alt [m]')
            plt.legend(loc='lower right')
            plt.savefig(out_dir+'/'+qtname+'-Alt.png',dpi=300)
            plt.close(fig)
        return
    
    def get_last_point(self):
        """Get last point of the trajectory.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        last_time : astropy.time.Time object
            Time of last point
        last_point : astropy.coordinates.EarthLocation object
            Location of last point
        
        """
        log.info(self.npts)
        log.info(self.data['Time'][-1])
        log.info(self.data['Latitude'][-1])
        log.info(self.data['Longitude'][-1])
        log.info(self.data['Altitude'][-1])
        last_time = Time(self.data['Time'][-1], format = 'isot')
        last_point = EarthLocation.from_geodetic(lat=self.data['Latitude'][-1],
                                                 lon=self.data['Longitude'][-1],
                                                 height=self.data['Altitude'][-1])
        return (last_time,last_point)
    
