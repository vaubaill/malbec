#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 15:35:21 2019

@author: dzilkova, J. Vaubaillon, IMCCE, 2019
"""
import numpy as np
import astropy.units as u
import matplotlib.pyplot as plt
from astropy.table import QTable
from configparser import ConfigParser, ExtendedInterpolation

# some missionplanner tools
from missionplanner import utils
from missionplanner.utils import log
from missionplanner import atmosphere_ram as atmosphere
from missionplanner import inflight_atmosphere

def atm_comp(config_file,flight_file,flight_fmt):
    # create logger
    dft_log_file = 'atm_comp.log'
    utils.add_FileHandler(dft_log_file)
    
    # check file existence
    utils.check_file(config_file,log)
    utils.check_file(flight_file,log)
    
    # read configuration file
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_file)
    
    # change log file name
    log_file=config['USER']['log_dir']+'/test_inflight.log'
    utils.change_logfile(log,dft_log_file,log_file)
    
    # set config, output directory and output file names
    grib_file = config['WEATHER']['weather_file']
    out_dir = config['CAMPAIGN']['out_dir']+'/'
    out_file = out_dir+'Atmdiff.dat'
    
    # reference position
    ref_lon = float(config['INITCOND']['lon'])*u.deg
    ref_lat = float(config['INITCOND']['lat'])*u.deg
    ref_alt = float(config['INITCOND']['alt'])*u.m
    
    # create atmosphere objects
    Atm_pre = atmosphere.Atmosphere(grib_file,log_file=log_file)
    Atm_flt = inflight_atmosphere.InFlight_Atmosphere(flight_file,flight_fmt,log_file=log_file)
    
    # hightest reached altitude
    top_height = Atm_flt.get_maxh()
    
    # make empty lists
    V_diff = QTable(names=['Alt','Diff_U','Diff_V','Diff_W','Diff_P','Diff_T'])
    V_diff['Alt'].unit=u.m
    V_diff['Diff_U'].unit=u.m/u.s
    V_diff['Diff_V'].unit=u.m/u.s
    V_diff['Diff_W'].unit=u.m/u.s
    V_diff['Diff_P'].unit=u.Pa
    V_diff['Diff_T'].unit=u.K
    
    #loop for increasing altitude until the heighest altitude with step 100m)
    for height in np.arange(ref_alt.to('m').value,top_height.to('m').value-100.0,100.0,dtype='float'):
        log.info('Getting wind data for altitude: '+str(height))
        # get wind speed vector
        wind_flt, P_flt, T_flt= Atm_flt.get_windPT(ref_lon,ref_lat,height*u.m)
        wind_pre, P_pre, T_pre = Atm_pre.get_windPT(ref_lon,ref_lat,height*u.m)
        """
        if (height*u.m >= Atm_pre.max_alt):
        g = atmosphere.compute_g(ref_lat)
        P_pre,Rho,T_pre = density.getPRhoT(height*u.m,g)
        """
        
        # compute the differences
        wind_diff = (wind_flt- wind_pre)
        P_diff = (P_flt-P_pre).to('Pa')
        T_diff = (T_flt-T_pre).to('K')
        
        # put differences into output table
        V_diff.add_row([height*u.m,wind_diff[0],wind_diff[1],wind_diff[2],P_diff,T_diff])
    
    # save wind difference data
    V_diff.write(out_file,format='ascii.fixed_width',delimiter=' ',overwrite=True)
    log.info('Diff data saved in '+out_file)
    
    # Plot difference
    qt2plot = ['Diff_U','Diff_V','Diff_W','Diff_P','Diff_T']
    qtunits = ['m/s','m/s','m/s','Pa','K']
    for (qtname,unit) in zip(qt2plot,qtunits):
        log.info('Now ploting '+qtname+' in '+unit+' vs Altitude')
        fig=plt.figure()
        plt.scatter(V_diff[qtname].to(unit).value,V_diff['Alt'].to('m').value,color='cornflowerblue',s=1,marker='P',label=qtname)
        plt.title(qtname+' vs Alt',color='indianred')
        plt.xlabel(qtname+' ['+unit+']')
        plt.ylabel('Alt [m]')
        plt.legend(loc='lower right')
        plot_file = out_dir+'/'+qtname+'-Alt.png'
        plt.savefig(plot_file,dpi=300)
        plt.close(fig)
        log.info('Plot saved in '+plot_file)
    
    log.info('Done.')
    return
    
