"""Mission Planner

Author
------
J. Vaubaillon, D. Zilkova - IMCCE, 2019

Version
-------
1.3

"""
# import all necessary tools
import os
import time
import copy
import shutil
import argparse
import logging
import numpy as np
from scipy.integrate import odeint
from configparser import ConfigParser, ExtendedInterpolation
# astropy
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable, vstack
from astropy.coordinates import EarthLocation, cartesian_to_spherical

# some missionplanner tools
from missionplanner import utils
from missionplanner import density
# Orange LoRa Liveobjects
from missionplanner import liveobjects
#import landzone
#import atmosphere
from missionplanner import atmosphere_ram as atmosphere
from missionplanner import trajectory
from missionplanner import inflight_atmosphere
from missionplanner.groundata import GroundData
from missionplanner.grounnodata import GroundNoData
from missionplanner.exceptions import NoGroundDataError

from missionplanner.utils import log

###################################################
# 
# DEFINITION OF THE MissionPlanner OBJECT
# 
###################################################

def compute_new_position(loc, vel_Qt, dt):
    """Compute new position given velocity.
    
    Parameters
    ----------
    loc : astropy.coordinates.EarthLocation Object
        location of the falling object to integrate.
    vel_Qt : astropy.coordinates.Quantity Object
        3D-velocity vector in [m/s]
        Velocity frame: [East,North,Zenith]
    dt : astropy.time.DeltaTime Object
        tmie step
    
    Returns
    -------
    newloc :astropy.coordinates.EarthLocation Object
        new location of the falling object to integrate.
    
    """
    R_pla = (1.0*u.R_earth).to('m')
    vel   = [vel_Qt[0].to('m/s').value,vel_Qt[1].to('m/s').value,vel_Qt[2].to('m/s').value]
    # convert velocity into spherical coordinates,
    # so that the longitude is the geographic azimuth
    # => this is why vel[0] and vel[1] are switched when calling cartesian_to_spherical
    Vrlatlon = cartesian_to_spherical(vel[1],vel[0],vel[2])# [norm,
                                                            # latitude = angle from North-East plane to velocity vector,
                                                            # longitude = angle from North, positive towards the East]
    # velocity geographic azimuth [deg]
    geogr_azimuth = Vrlatlon[2].to('deg')
    # velocity elevation from North-East plane, positive towards the zenith [deg]
    elevation = Vrlatlon[1].to('deg')
    # velocity modulus in [m/s]
    norm = Vrlatlon[0]*u.m/u.s
    
    # velocity projected in the North-East plan [m/s]
    Vxy = norm * np.cos(elevation.to('rad'))
    # Displacement in North-East plan [m]
    Dxy = Vxy * dt.to('s')
    # displacement towards the North [m]
    dN = -Dxy * np.cos(geogr_azimuth.to('rad'))
    # displacement towards the East [m]
    dE = -Dxy * np.sin(geogr_azimuth.to('rad'))
    # change in longitude [rad]
    dlat = (dN / (R_pla+loc.height))*u.rad
    # change in latitude
    dlon = (dE / (R_pla+loc.height))*u.rad
    # change in altitude [m]
    dh = vel_Qt[2].to('m/s') * dt.to('s')
    # compute new location
    newloc = EarthLocation.from_geodetic(loc.lon+dlon.to('deg'),
                                         loc.lat+dlat.to('deg'),
                                         loc.height+dh.to('m'))
    return newloc

def compute_relative_wind(vel,wind_3D):
    """Compute the relative wind vector.
    
    Parameters
    ----------
    vel : 3-vector of floats or astropy.units.Quantity object 3-vector
        Velocity vector in [m/s]
    wind_3D : astropy.units.Quantity object 3-vector
        Absolute wind velocity vector [m/s]
    
    Returns
    -------
    relWvel : astropy.units.Quantity object 3-vector.
        Relative velocity vector in [m/s]
    relwindnorm : astropy.units.Quantity object.
        Relative velocity norm in [m/s]
    relwindunit : 3-vector of floats.
        relative wind UNIT VECTOR.
    
    """
    # convert vel into float if necessary
    try:
        vel_ms = np.array([vel[0].to('m/s').value,
                           vel[1].to('m/s').value,
                           vel[2].to('m/s').value])
    except:
        vel_ms = vel
    # convert wind vector into float
    wnd_ms = np.array([wind_3D[0].to('m/s').value,
                       wind_3D[1].to('m/s').value,
                       wind_3D[2].to('m/s').value])
    # compute relative wind vector
    relWvel =  wnd_ms  - vel_ms
    # compute norm of relative wind vector
    relwindnorm = np.linalg.norm(np.array([relWvel[0],
                                           relWvel[1],
                                           relWvel[2]]))
    # compute unit vector
    if (relwindnorm == 0.0):
        relwindunit = np.zeros(3)
    else:
        relwindunit = relWvel / relwindnorm
    return (relWvel*u.m/u.s,relwindnorm*u.m/u.s,relwindunit)

def site_decide(decision_table):
    """Decide which site to reach, based on feasibility and margin.
    
    If no site is reacheable, return -1.
    
    Parameters
    ----------
    decision_table : astropy.table.QTable
        Decision table including Site id, Feasibility, Time of arrival and margin.
    
    Returns
    -------
    site_id : int
        Site id where the glider should go to.
    """
    # sort table with margin
    decision_table.sort('margin',reverse=True)
    # select reachable sites only
    decision_feasible = decision_table[0]['Feasible']
    # get first reachable site
    decide_id = decision_table[0]['id']
    decide_time = Time(decision_table[0]['Time'],format='isot')
    decide_margin = decision_table[0]['margin']
    return (decision_feasible,decide_id,decide_time,decide_margin)



class MissionPlanner (object):
    """MissionPlanner Object definition.
    
    Parameters
    ----------
    log_set : boolean
        True if logger is set. False at start of the program.
    
    Atm_grb : malbec.missionplan.atmosphere_ram.Atmosphere Object
        Atmosphere created from grib2 type file.
    Atm_flt : malbec.missionplan.inflight_atmosphere.Atmosphere Object
        Atmopshere created from in-flight data.
    
    
    """
    
    # initializes some variables with default values, such as e.g None
    __version__='1.3'
    
    log_set = False # True if logger is set.
    
    Atm_grb = None # Atmosphere from grib file.
    Atm_flt = None # Atmosphere from in-flight data
    atm_type = None # type of Atmosphere considered for the calculation.
    
    wind_3D = None
    Rho = None
    h_step = None
    t_step = None
    HXmax = None
    HX_step = None
    Vmin = None
    Vmax = None
    Vopt = None
    
    Eloc_start = None
    Time_start = None
    trajectory_type = None
    vel_start = None
    
    PressHe = None
    RhoHe = None
    TempHe = None
    rad_baln = None
    
    check_list = False
    
    GD = None
    
    def __init__(self, config_file):
        """Initialize MissionPlanner Object.
        
        Parameters
        ----------
        config : string
            full name of configuration parameters file. Default is None.
        
        Returns
        -------
        None
        
        """
        # creates configparser.ConfigParser() object for the configuration file
        self.config = ConfigParser(interpolation=ExtendedInterpolation())
        
        # if set, read configuration file
        if config_file:
            self.set_and_read_config(config_file)
        
        # run check list
        #self.run_check_list()
        
        return
    
    def set_and_read_config(self,config_file):
        """Set and read the configuration file.
        
        Parameters
        ----------
        config_file : string, optional
            full name of configuration parameters file.
        
        Returns
        -------
        None.
        
        """
        # set config_file.
        self.config_file = config_file
        # test if config_file file exists.
        utils.check_file(self.config_file,log)
        # read the configuration file BEFORE setting the logger since log file name is in config file.
        self.config.read(self.config_file)
        # set the home variable
        self.config['USER']['home'] = os.getenv('HOME')
        
        # set logger object BEFORE checking the configuration file so log messages can be output.
        logdict = {'DEBUG' : logging.DEBUG,
                   'INFO' : logging.INFO,
                   'WARNING' : logging.WARNING,
                   'ERROR' : logging.ERROR}
        if not self.log_set:
            try:
                log_level = self.config['USER']['log_level']
                self.set_log(level=logdict[log_level])
            except:
                self.set_log(level=logging.DEBUG)
        
        # check the configuration file
        self.check_config()
        
        return
    
    def check_config(self):
        """Check and reads the configuration file.
        
        Make sure all necessary parameters are present.
        Raise ValueError if a parameters is missing.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        # check the presence of necessary paths names in config file
        listkeys = ['home','proj_dir','soft_dir','conf_dir','data_dir','log_dir','kernel_path','spice_exe_path']
        for key in listkeys:
            log.debug('now checking presence of : '+key+' in '+self.config_file)
            try:
                tmp = self.config['USER'][key]
            except:
                msg = '*** FATAL ERROR: '+key+' is missing from USER section of configuration file '+self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # check existence of directories
        listkey_dirtocheck = ['home','proj_dir','soft_dir','conf_dir','data_dir','log_dir','kernel_path','spice_exe_path']
        for key in listkey_dirtocheck:
            log.debug('now checking existence of : '+self.config['USER'][key])
            if not os.path.exists(self.config['USER'][key]):
                msg = '*** FATAL ERROR: directory '+self.config['USER'][key]+' does not exists'
                log.error(msg)
                raise IOError(msg)
        
        # save SPICE paths into global variable
        self.kernel_path = self.config['USER']['kernel_path']
        self.spice_exe_path = self.config['USER']['spice_exe_path']
        
        # check the presence of necessary keys in CAMPAIGN config file
        listkeys = ['name','out_dir','payload_file','sim_dir_name','nacelle_name','nacelle_id','sites_file','trajectory_sim_file']
        for key in listkeys:
            log.debug('now checking presence of : '+key+' in '+self.config_file)
            try:
                tmp = self.config['CAMPAIGN'][key]
            except:
                msg = '*** FATAL ERROR: '+key+' is missing from CAMPAIGN section of configuration file '+self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        
        # make directories if necessary
        listkey_dirtocheck = ['out_dir']
        for key in listkey_dirtocheck:
            log.debug('now checking existence of directory: '+self.config['CAMPAIGN'][key])
            if not os.path.isdir(self.config['CAMPAIGN'][key]):
                msg = 'Warning: directory '+self.config['CAMPAIGN'][key]+' does not exists -> making one now.'
                log.warning(msg)
                os.makedirs(self.config['CAMPAIGN'][key])
        
        # copy configuration file into output directory
        src = self.config_file
        dst = self.config['CAMPAIGN']['out_dir']+'/'+os.path.basename(src)
        try:
            shutil.copyfile(src,dst)
        except shutil.SameFileError:
            log.warning('Config files are the same -> not copied')
        except:
            msg = '*** FATAL ERROR: unable to copy '+src+' into '+dst
            log.error(msg)
            raise IOError(msg)
        log.info('File :'+src+' has been copied to '+dst)
        
        # check existence of payload and site files
        list_filetocheck = ['payload_file','sites_file']
        for key in list_filetocheck:
            log.debug('now checking existence of configuration file: '+self.config['CAMPAIGN'][key])
            # check existence of file
            utils.check_file(self.config['CAMPAIGN'][key],log)
            # copy config file into output directory
            src = self.config['CAMPAIGN'][key]
            dst = self.config['CAMPAIGN']['out_dir']+'/'+os.path.basename(src)
            shutil.copyfile(src,dst)
            log.info('File :'+src+' has been copied to '+dst)
        # creates configparser.ConfigParser() object for the payload and sites files
        self.payload = ConfigParser()
        self.sitesconf = ConfigParser()
        # read the payload and site configuration files
        self.payload.read(self.config['CAMPAIGN']['payload_file'])
        self.sitesconf.read(self.config['CAMPAIGN']['sites_file'])
        # check payload and sites files
        self.check_payload()
        self.check_sites()
        
        # check Flight type (Note: has to be performed BEFORE weather check since Atmosphere object depends on flight type).
        try:
            tmp = self.config['FLIGHT']['flight_type']
        except:
            msg = '*** WARNING: flight_type is missing from configuration file '+self.config_file
            log.error(msg)
            raise ValueError(msg)
        # set the flight_type
        self.flight_type = self.config['FLIGHT']['flight_type']
        # check the flight_type
        self.check_flight_type()
        
        # check Weather file
        for key in ['weather_file','weather_format']:
            try:
                tmp = self.config['WEATHER'][key]
            except:
                msg = '*** FATAL ERROR: '+key+' is missing from configuration file '+self.config_file
                log.error(msg)
                raise ValueError(msg)
        self.check_and_read_weather()
        
        # set trajectory_rlt_file file name
        if (self.flight_type=='inflight' or self.flight_type=='inflight_lora'):
            for key in ['trajectory_rlt_file','trajectory_rlt_fmt']:
                try:
                    tmp = self.config['CAMPAIGN'][key]
                    log.debug(key+' : '+tmp)
                except:
                    msg = '*** WARNING: trajectory_rlt_* keys are missing from configuration file '+self.config_file
                    log.error(msg)
                    raise ValueError(msg)
            self.trajectory_rlt_file = self.config['CAMPAIGN']['trajectory_rlt_file']
            self.trajectory_rlt_fmt  = self.config['CAMPAIGN']['trajectory_rlt_fmt'].upper()
            utils.check_file(self.trajectory_rlt_file,log)
            self.check_realtime_format()
        
        # Set the Simulation configuration
        try:
            self.outdir = self.config['CAMPAIGN']['out_dir']
            self.trajectory_sim_file = self.config['CAMPAIGN']['trajectory_sim_file']
            self.gd_path = self.config['DTM']['dtm_dir']
            self.time_step_up = float(self.config['SIMULATION']['time_step_up'])*u.s
            self.time_step_dwn = float(self.config['SIMULATION']['time_step_dwn'])*u.s
            self.HXmax = float(self.config['SIMULATION']['HXmax'])*u.m
            self.Vopt = float(self.config['SIMULATION']['Vopt'])*u.m/u.s
            self.MaxGroundAlt = float(self.config['SIMULATION']['MaxGroundAlt'])*u.m
        except:
            msg = '*** FATAL ERROR: configuration file is missing too many keys: '+self.config_file
            log.error(msg)
            raise ValueError(msg)
        
        # check Ground data directory
        utils.check_dir(self.gd_path,log)
        
        # if necessary make SPICE id
        try:
            self.nacelle_name = self.config['CAMPAIGN']['nacelle_name']
            self.nacelle_id = int(self.config['CAMPAIGN']['nacelle_id'])
        except:
            msg = '*** WARNING: no nacelle SPICE name/id is defined -> set to default: MALBEC/-1399001.'
            log.warning(msg)
            self.nacelle_name = 'MALBEC'
            self.nacelle_id = -1399001
        return
    
    def set_log(self,level=logging.INFO,nosdlr=False,nohdlr=False):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG
        nosdlr : boolean, optional
            If True, no stream handler is created. Default is False.
        nohdlr  : boolean, optional
            If True, no file handler is created. Default is False.
        
        Returns
        -------
        None.
        
        
        """
        # set log level
        log.setLevel(level)
        log.propagate = True
        
        # log format
        log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
        
        # create StreamHandler
        sdlr = logging.StreamHandler()
        sdlr.setLevel(level)
        sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
        
        # create FileHandler
        strt = Time.now().isot.replace('-','').replace(':','').split('.')[0]
        self.log_file = self.config['CAMPAIGN']['log_file_root'].replace('YYYYMMDDTHHMMSS',strt)
        hdlr = logging.FileHandler(self.log_file)
        hdlr.setLevel(level)
        hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
            
        # if LOG directory does not exist make one
        if not os.path.isdir(self.config['USER']['log_dir']):
            os.makedirs(self.config['USER']['log_dir'])
        
        # set the log_set parameter
        self.log_set = True
        
        # log the log file name
        log.info('Log file: '+self.log_file)
        
        return
    
    def dict_flight_type(self):
        """Returns True if self.flight_type is correct, False otherwise.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        return{
                'prevision': True,
                'inflight': True,
                'inflight_lora': True
            }.get(self.flight_type, False)
    
    def check_flight_type(self):
        """Check the value of flight_type in config file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        if not self.dict_flight_type():
            msg = '*** FATAL ERROR: flight_type is incorrect: '+self.flight_type+'. Choice is limited to : prevision or inflight or inflight_lora.'
            log.error(msg)
            raise ValueError(msg)
        return
    
    # def traj2timestep(self):
    #     """Set Time step as a function of trajectory type.
        
    #     Parameters
    #     ----------
    #     None.

    #     Returns
    #     -------
    #     None.

    #     """
    #     self.trajectory_type.upper()=='ASCEND' and self.time_step = self.time_step_up
    #     self.trajectory_type.upper()=='GLIDE' and self.time_step = self.time_step_up
    #     self.trajectory_type.upper()=='CEILING' and self.time_step = self.time_step_up
    #     self.trajectory_type.upper()=='FREEFALL' and self.time_step = self.time_step_up
        

        
    #     return
    
    def dict_trajectory_type(self):
        """Returns True if self.trajectory_type is correct, False otherwise.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        return{
                'ASCEND': True,
                'GLIDE': True,
                'CEILING': True,
                'FREEFALL': True
            }.get(self.trajectory_type.upper(), False)
    
    def check_trajectory_type(self):
        """Check the value of trajectory_type in config file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        if not self.dict_trajectory_type():
            msg = '*** FATAL ERROR: flight_type is incorrect: '+self.trajectory_type+'. Choice is limited to : ASCEND or FREEFALL or GLIDE.'
            log.error(msg)
            raise ValueError(msg)
        return
    
    def dict_realtime_format(self):
        """Returns True if self.flight_type is correct, False otherwise.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        return{
                'grib': True,
                'CNES2017': True,
                'CNES2018': True,
                'CNES2019': True,
                'ODROID': True,
                'LORA': True,
                'lora': True,
                'SIMU': True
            }.get(self.trajectory_rlt_fmt.upper(), False)
    
    def check_realtime_format(self):
        """Check the value of flight_type in config file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        if not self.dict_realtime_format():
            msg = '*** FATAL ERROR: trajectory real time format is incorrect: '+self.trajectory_rlt_fmt+'. Choice is restricted to : grib, CNES2017, CNES2018, CNES2019, ODROID, LORA OR SIMU.'
            log.error(msg)
            raise ValueError(msg)
        return
    
    def check_payload(self):
        """Check each useful parameter of the malbec configuration file.
        
        Raises ValueError if any error is encountered.
        Note that the existence and opening of the file have already been performed.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # check the presence of keywords
        # check the mass
        try:
            self.payld_mass = float(self.payload['MASS']['mass_total'])*u.kg
        except:
            msg = '*** FATAL ERROR: mass is missing from MASS section of payload file '+self.config['CAMPAIGN']['payload_file']
            log.error(msg)
            raise ValueError(msg)
        
        # Check the FREEFALL keywords
        listkeys = ['equ_rad','cd',]
        for key in listkeys:
            log.debug('now checking presence of : '+key+' in '+self.config['CAMPAIGN']['payload_file'])
            try:
                tmp = self.payload['FREEFALL'][key]
            except:
                msg = '*** FATAL ERROR: '+key+' is missing from FREEFALL section of payload file '+self.config['CAMPAIGN']['payload_file']
                log.error(msg)
                raise ValueError(msg)
        
        # check the CEILING keyword
        # first check if present at all
        try:
            tmp = self.payload['CEILING']
            payload_ceiling = True
        except:
            payload_ceiling = False
        if payload_ceiling:
            listkeys = ['time_step_dwn','ceil_dur','Vdesc']
            for key in listkeys:
                log.debug('now checking presence of : '+key+' in '+self.config['CAMPAIGN']['payload_file'])
                try:
                    tmp = self.payload['CEILING'][key]
                    log.debug(key+' : '+tmp)
                except:
                    msg = '*** FATAL ERROR: '+key+' is missing from CEILING section of payload file '+self.config['CAMPAIGN']['payload_file']
                    log.error(msg)
                    raise ValueError(msg)
            # read glide parameters
            try:
                self.time_step_ceil = float(self.payload['CEILING']['time_step_dwn'])*u.s
                self.ceil_dur = float(self.payload['CEILING']['ceil_dur'])*u.min
                self.ceil_Vdesc = float(self.payload['CEILING']['Vdesc'])*u.m/u.s
            except:
                msg = '*** FATAL ERROR: GLIDE section keywords are incorrect or missing in payload file: '+self.config['CAMPAIGN']['payload_file']
                log.error(msg)
                raise ValueError(msg)
        # payload cannot ceil at all
        else:
            self.time_step_ceil = 0.0*u.s
            self.ceil_dur = 0.0*u.s
            self.ceil_Vdesc = 0.0*u.m/u.s
        # end of if payload_ceiling
        
        # Check the GLIDE keywords
        # first check if present at all
        try:
            tmp = self.payload['GLIDE']
            payload_glide = True
        except:
            payload_glide = False
            self.time_step_glide = 0.0*u.s # dummy value
        # payload can indeed glide:
        if payload_glide:
            listkeys = ['glide_alt','glide_vel','lift2drag','min_margin']
            for key in listkeys:
                log.debug('now checking presence of : '+key+' in '+self.config['CAMPAIGN']['payload_file'])
                try:
                    tmp = self.payload['GLIDE'][key]
                    log.debug(key+' : '+tmp)
                except:
                    msg = '*** FATAL ERROR: '+key+' is missing from GLIDE section of payload file '+self.config['CAMPAIGN']['payload_file']
                    log.error(msg)
                    raise ValueError(msg)
            # read glide parameters
            try:
                self.glide_alt = float(self.payload['GLIDE']['glide_alt'])*u.m
                self.glide_vel = (float(self.payload['GLIDE']['glide_vel'])*u.km/u.h).to('m/s')
                self.lift2drag = float(self.payload['GLIDE']['lift2drag'])
                self.time_step_glide = float(self.payload['GLIDE']['time_step'])*u.s
                self.min_margin = float(self.payload['GLIDE']['min_margin'])*u.m
            except:
                msg = '*** FATAL ERROR: GLIDE section keywords are incorrect or missing in payload file: '+self.config['CAMPAIGN']['payload_file']
                log.error(msg)
                raise ValueError(msg)
        # payload cannot glide and GLIDE keywords do not appear in payload configuration file.
        else:
            self.glide_alt = -10000.0*u.m
            self.glide_vel = 0.01*u.m/u.s
            self.lift2drag = 0.01
        # end of if payload_glide
        
        # now read the other parameters
        try:
            # Read the properties of the nacelle for free fall phase.
            self.equ_rad = float(self.payload['FREEFALL']['equ_rad'])*u.m
            self.cd = float(self.payload['FREEFALL']['cd'])
        except:
            msg = '*** FATAL ERROR: FREEFALL keywords are incorrect or missing in payload file: '+self.config['CAMPAIGN']['payload_file']
            log.error(msg)
            raise ValueError(msg)
        
        return
    
    def check_sites(self):
        """Check each useful parameter of the sites configuration file.
        
        Sets the self.landingsites and self.nsites parameters.
        Raises ValueError if any error is encountered.
        Note that the existence and opening of the file have already been performed.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # create dictionnary of landing sites
        self.landingsites = {}
        # list of key to input into landing site dictionnary
        listkey = ['id','lon','lat','alt','type','name','tel']
        self.nsites = 0
        keepgoing = True
        while keepgoing:
            param = 'SITE'+str(self.nsites+1)
            log.debug('checking site: '+param)
            # check that the SITE number is present in the file
            try:
                site = self.sitesconf[param]
                # test each of the key needed for each site
                for key in listkey:
                    try:
                        tmp = site[key]
                        log.debug('Site '+param+' has required key '+key+' : '+tmp)
                    except:
                        msg = '*** FATAL ERROR: site '+param+' is lacking key: '+key
                        log.error(msg)
                        raise ValueError(msg)
            except:
                log.warning('*** Impossible to find '+param+' in '+self.config['CAMPAIGN']['sites_file'])
                break
            self.nsites = self.nsites+1
            local_dict = {}
            for key in listkey:
                try:
                    local_dict[key] = self.sitesconf[param][key]
                    self.landingsites[param] = local_dict
                    log.debug(key+' : '+self.sitesconf[param][key])
                except:
                    msg = '*** FATAL ERROR: '+key+' was not found in '+param
                    log.error(msg)
                    raise ValueError(msg)
            self.landingsites[param]['eloc'] = EarthLocation(lat=float(self.landingsites[param]['lat'])*u.deg,
                                                             lon=float(self.landingsites[param]['lon'])*u.deg,
                                                             height=float(self.landingsites[param]['alt'])*u.m)
        log.info('There are '+str(self.nsites)+' possible landing sites recorded in '+self.config['CAMPAIGN']['sites_file'])
        return
    
    def check_and_read_weather(self):
        """Check and read the weather data files.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('Creating Atmosphere object')
        # check weather file existence
        utils.check_file(self.config['WEATHER']['weather_file'],log)
        # creates the Atmosphere object, whatever the flight_type, for backup purpose.
        self.Atm_grb = atmosphere.Atmosphere(self.config['WEATHER']['weather_file'])#,self.log_file)
        log.info('Default Atm_grb object successfully created')
        # inflight flight type
        if ('inflight' in self.flight_type):
            # create InFlight_Atmosphere object
            self.Atm_flt = inflight_atmosphere.InFlight_Atmosphere(self.config['CAMPAIGN']['trajectory_rlt_file'],
                                                                   self.config['CAMPAIGN']['trajectory_rlt_fmt'],
                                                                   self.Atm_grb,
                                                                   log_file=self.log_file,
                                                                   glide_alt=self.glide_alt)
            self.Atm_flt.plot(out_dir=self.config['CAMPAIGN']['out_dir'])
            log.info('InFlight_Atmosphere object successfully created.')
        return
    
    def make_sites_from_eloc(self,eloc,az_step=45.0*u.deg,dist=1000.0*u.km):
        """Make landing sites from an EarthLocation.
        
        This is useful to compute a possible landing zone if the payload is a glider.
        The defined landing sites form an ellipse re-shaped by the altitude wind.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            current / starting longitude coordinate
        lat : astropy.coordinates.Angle Object
            current / starting latitude coordinate
        az_step : astropy.units Quantity object, optional
           Azimuth step to compute the ellipse. Default is 45.0*u.deg.
        dist : astropy.units Quantity object, optional
           Site distance. Default is 1000*u.km
        
        Returns
        -------
        None.
            self.landingsites and self.nsites are redefined.
        
        """
        # reet landingsites and nsites
        self.landingsites = {}
        self.nsites = 0
        # set all possible azimuths
        az_all = np.arange(0.0, 360.0, az_step.to('deg').value) *u.deg
        # loop over all possible azimuths
        for az in az_all:
            # increase the number of sites
            self.nsites = self.nsites + 1
            # compute site location
            eloco = utils.distsite(eloc,az,dist)
            # set landingsites entry key
            param = 'SITE'+str(self.nsites)
            # make dict of dict
            self.landingsites[param] = {}
            # set landingsites parameters
            self.landingsites[param]['id'] = str(self.nsites)
            self.landingsites[param]['eloc'] = eloco
            self.landingsites[param]['lon'] = eloco.lon
            self.landingsites[param]['lat'] = eloco.lat
            self.landingsites[param]['alt'] = eloco.height
            self.landingsites[param]['type'] = 'ellipse'
            self.landingsites[param]['name'] = 'ellipse_'+str(self.nsites)
            self.landingsites[param]['tel'] = '0599999999'
        
        # now log all possible landing sites
        for param in  self.landingsites:
            log.info('=== Ellipse landing site '+str(param))
            for key in self.landingsites[param]:
                log.info(str(self.landingsites[param][key]))
        return
    
    
    
    def get_windRho(self, lon, lat, alt):
        """Get the wind vector and density.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            longitude coordinate
        lat : astropy.coordinates.Angle Object
            latitude coordinate
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        wind_3D : array of astropy.units.Quantity objects.
            wind 3-vector [m/s] DIRECTION WHERE THE WIND IS GOING TO (not where it's coming form)
            U: zonal wind, positive in the East [m/s]
            V: meridional wind, positive to the North [m/s]
            W: vertical wind positive towards the Zenith [m/s]
        Rho : astropy.units.Quantity object
            air density [kg/m-3].
        """
        # get the wind, pressure and temperature from the selected atmosphere object
        # there is a problem at 30km = FIXXXXX or get data (P n T) from ISA, keep the wind the same
        (self.wind_3D, self.P, self.T) = self.Atm.get_windPT(lon,lat,alt)
        
        # compute equivalent density
        self.Rho = utils.compute_rhoatmo(self.P, self.T)
        return (self.wind_3D, self.Rho)
    
    def get_atm_param(self, lon, lat, alt):
        """Get all the atmospheric parameters.
        
        Parameters
        ----------
        lon : astropy.coordinates.Angle Object
            longitude coordinate
        lat : astropy.coordinates.Angle Object
            latitude coordinate
        alt : astropy.units Quantity object
            Altitude coordinate.
        
        Returns
        -------
        wind_3D : array of astropy.units.Quantity objects.
            wind 3-vector [m/s] DIRECTION WHERE THE WIND IS GOING TO (not where it's coming form)
            U: zonal wind, positive in the East [m/s]
            V: meridional wind, positive to the North [m/s]
            W: vertical wind positive towards the Zenith [m/s]
        P : astropy.units.Quantity object
            air Pressure [Pa]
        T : astropy.units.Quantity object
            air Temperature [K]
        Rho : astropy.units.Quantity object
            air density [kg/m-3].
        """
        # get the wind, pressure and temperature from the selected atmosphere object
        # there is a problem at 30km = FIXXXXX or get data (P n T) from ISA, keep the wind the same
        (wind_3D,P,T) = self.Atm.get_windPT(lon,lat,alt)
        # compute equivalent density
        Rho = utils.compute_rhoatmo(P,T)
        log.debug('wind_3D: '+str(wind_3D)+' P: '+str(P)+' T: '+str(T)+' Rho: '+str(Rho))
        return (wind_3D,P,T,Rho)
    
    def set_default_initcond(self):
        """Set default initial conditions.
        
        Sets self.Eloc_start and self.Time_start parameters.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.debug('*** Default Initial conditions:')
        log.debug('flight_type : '+str(self.flight_type))
        
        if (self.flight_type == 'prevision'):
            launch_site = self.landingsites['SITE'+self.config['INITCOND']['initcond_site']]
            self.Eloc_start = EarthLocation(lon=float(launch_site['lon'])*u.deg,
                                            lat=float(launch_site['lat'])*u.deg,
                                            height=float(launch_site['alt'])*u.m)
            self.Time_start = Time(self.config['INITCOND']['initcond_time'],format='isot')
        
        if (self.flight_type == 'inflight'):
            # Get the initial coordinates from Atmosphere object
            (self.last_point, self.last_time, self.P, self.T) = self.Atm_flt.get_lastpoint()
            self.Eloc_start = self.last_point
            self.Time_start = self.last_time
        
        if (self.flight_type == 'inflight_lora'):
            (self.Time_start,self.Eloc_start,self.vel_start) = liveobjects.get_initcond(self.trajectory_rlt_file)
        
        # double check that self.Eloc_start and self.Time_start are set
        for key in [self.Eloc_start,self.Time_start]:
            if key is None:
                msg = '*** FATAL ERROR: Default initial conditions failed to set.'
                log.error(msg)
                raise ValueError(msg)
        # output initial conditions
        log.debug('Time_start='+self.Time_start.isot)
        log.debug('Eloc_start.lon='+str(self.Eloc_start.lon))
        log.debug('Eloc_start.lat='+str(self.Eloc_start.lat))
        log.debug('Eloc_start.height='+str(self.Eloc_start.height))
        return
    
    def choose_atmosphere_object(self, alt=None, alt_threshold=10*u.m):
        """Choose which atmosphere object to use: Atm_grb or Atm_flt.
        
        Parameters
        ----------
        alt : astropy.units.Quantity object
            Current altitude [m]
        alt_threshold : astropy.units.Quantity object
            Altitude threshold to decide which atmosphere object to choose.
        
        Returns
        -------
        None.
        
        """
        # by default choose the previsions
        self.Atm = self.Atm_grb
        self.atm_type = 'grib'
        
        # if 'inflight', try to use direct GPS measurements
        if ('inflight' in self.flight_type):
            self.Atm = self.Atm_flt
            self.atm_type = 'realtime'
            log.info('Atm_flt is chosen.')
            # set last and maximum altitude
            (self.Eloc_start, self.Time_start, self.P_start, self.T_start) = self.Atm.get_lastpoint()
            # double check for altitute: this is useful only if the GPS cannot fix and serves as a backup solution.
            if (self.Eloc_start.height > (self.Atm.get_maxh() + alt_threshold)):
                self.Atm = self.Atm_grb
                self.atm_type = 'grib'
                log.warning('Atm_grb is chosen because altitude > max measured altitude -> Check GPS FIX!!!')
        
        # double check that the atmosphere object is not None
        if self.Atm is None:
            msg = '*** FATAL ERROR: Atm is None'
            log.error(msg)
            raise ValueError(msg)
        log.info('Atm type: '+self.atm_type)
        return
    
    def set_stage(self):
        """Decide the current stage of flight.
        
        Sets trajectory_type paramters.
        
        Parameters
        ----------
        None.
        
        Returns:
        None.
            
        """
        # case Atmosphere type: inflight
        if (self.atm_type == 'realtime'):
            self.Atm.get_lastpoint()
            (self.Eloc_start, self.Time_start, self.P_start, self.T_start) = self.Atm.get_lastpoint()
            self.MaxAlt = self.Atm_flt.get_maxh()
            self.trajectory_type = self.Atm.get_trajectory_type()
            log.info('trajectory_type: '+self.trajectory_type)
        
        # if grib-type atmosphere is selected trajectory type is set in config.in file 
        if (self.atm_type == 'grib'):
            # For debug purpose: check if trajectory_type is present in configuration file
            try:
                self.trajectory_type = self.config['FLIGHT']['trajectory_type'].upper()
                log.info ('Trajectory type ' + self.trajectory_type)
            except:
                msg = '*** trajectory_type not found in configuration file '+self.config_file
                msg = msg + ' trajectory_type is set to ASCEND by default.'
                log.warning(msg)
                self.trajectory_type = str('ASCEND')
        
        log.debug('trajectory_type: '+self.trajectory_type)
        # double check trajectory type
        self.check_trajectory_type()        
        return
        
    def set_initialcond(self):
        """Set initial conditions and decide ascend/freefall/glide flight type.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.debug('atm_type: '+str(self.atm_type))
        # if position is given in real time in CNES format:
        if (self.atm_type == 'realtime'):
            (self.Eloc_start, self.Time_start, self.P_start, self.T_start) = self.Atm.get_lastpoint()
            init_vertvel = self.Atm.get_last_vertical_velocity()
            self.vel_start = [0.0,0.0,init_vertvel.to('m/s').value]*u.m/u.s
        else:
            # flight_type is inflight_lora
            if (self.flight_type=='inflight_lora'):
                log.debug('flight_type==inflight_lora')
                # retrieve (again) the latest position
                [self.Time_start,
                 self.Eloc_start,
                 self.vel_start] = liveobjects.get_initcond(self.trajectory_rlt_file)
                if self.vel_start[2].value>0.0:
                    log.debug('ASCENDING flight detected')
                    self.trajectory_type = str('ASCEND')
                    self.PreFlightAscend_init()
                else:
                    log.debug('DESCENDING/FREEFALL flight detected')
                    self.trajectory_type = str('FREEFALL')
                    self.PreFlightDescend_init()
                log.warning('*** trajectory_type was changed to: '+self.trajectory_type)
                    
            if (self.trajectory_type == str('ASCEND')):
                self.PreFlightAscend_init()
            else:
                self.PreFlightDescend_init()
        # output initial conditions
        log.debug('*** Initial conditions:')
        log.debug('Time_start='+self.Time_start.isot)
        log.debug('Eloc_start.lon='+str(self.Eloc_start.lon))
        log.debug('Eloc_start.lat='+str(self.Eloc_start.lat))
        log.debug('Eloc_start.height='+str(self.Eloc_start.height))
        log.debug('vel_start='+str(self.vel_start))
        return
     
    def PreFlightAscend_init(self):
        """Set initial conditions for preflight ascend.
        
        Set the following variables: ascend_type and vel_start parameters.
        Eloc_start and Time_start are set in the set_default_initcond() method.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # Note: self.Eloc_start was set in the set_default_initcond() method.
        log.info('Now setting initial conditions for: '+ self.trajectory_type + ', for '+self.atm_type+' type Atmosphere')
        # For preflight computation decide the type of ascend -> CONSTANT velocity
        self.ascend_type = self.config['SIMULATION']['ascend_type']
        # sets initial velocity
        self.vel_start = [0.0, 0.0, self.Vopt.value]*u.m/u.s
        self.time_step = self.time_step_up
        return
    
    def PreFlightDescend_init(self):
        """Set initial conditions for preflight descend.
        
        Set the following variables: last_point, last_time, vel_start.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('Now setting initial conditions  for: '+ self.trajectory_type + ', for grib Atmosphere')
        # DEBUG: case prevision: this case can only happen in debug mode
        if (self.flight_type=='prevision'):
            # set initial conditions with config file
            self.Eloc_start = EarthLocation(lon=float(self.config['INITCOND']['lon'])*u.deg,
                                            lat=float(self.config['INITCOND']['lat'])*u.deg,
                                            height=float(self.config['INITCOND']['alt'])*u.m)
            self.Time_start = Time(self.config['INITCOND']['initcond_time'],format='isot')
            # Computation from the TOP so initvel is zero
            self.vel_start = [0.0, 0.0, 0.0]*u.m/u.s
        
        # if in flght conditions
        if (self.flight_type=='inflight_lora'):
            [self.Time_start,
             self.Eloc_start,
             self.vel_start] = liveobjects.get_initcond(self.trajectory_rlt_file)
        # set integration time step
        self.time_step = self.time_step_dwn
        return
    
    def run_check_list(self):
        """Run a list of checks before launching the computation.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        chkl_time = time.time()
        # Note: existence and reading of config, payload, sites and weather files is perform at initialization:
        # => no need to redo it here.
        
        log.info('******* NOW RUNNING CHECK-LIST ******')
        
        # check configuration and weather files
        self.check_and_read_weather()
        
        # if needed creates output directory
        utils.check_output_dir(self.outdir,log)
        
        # check disk space to save data
        utils.check_disk(log)
        
        # set default initial conditions
        self.set_default_initcond()
        
        # makes GroundData object: this takes 'a lot' of time i.e. several seconds...
        if not self.GD:
            log.info('Creating GroundData object')
            self.GD	= GroundData(log)
            try:
                self.GD.extract_ign_data(self.gd_path,self.Eloc_start.lon,self.Eloc_start.lat)
            except NoGroundDataError as nogrnd:
                log.error('No Ground data found => GroundNoData object created')
                log.error(nogrnd.msg)
                self.GD = GroundNoData(log)
                self.MaxGroundAlt = 1.0*u.m
                log.info('MaxGroundAlt : '+str(self.MaxGroundAlt))  
            except: # Exception as e:
                msg = '*** Something wrong happened while extracting data from GroundData Object'
                log.error(msg)
                #log.error(e.__class__)
                #log.error(e.msg)
                raise ValueError(msg) 
                #raise e
        
        # choose atmosphere object and read weather condition
        self.choose_atmosphere_object(self.Eloc_start.height)
        
        # check stage of flight for integration: ASCEND, FREEFALL or GLIDE
        self.set_stage()
        
        # read initial conditions
        self.set_initialcond()
        
        # compute initial ground altitude
        groundalt = self.GD.interpol_ign_data(self.Eloc_start.lon,self.Eloc_start.lat)
        log.debug('groundalt='+str(groundalt))
        
        #sets the check_list variable to indicate that the check-list has been run.
        self.check_list = True
        
        log.info('==== Check list successfully passed.')
        log.debug('The check_list took '+"{:6.1f}".format((time.time()-chkl_time))+' sec to run.')
        return
    
    def change_config(self,config_file):
        """Change the configuration file.
        
        Set and reads the new configuration file and run check-list.s
        
        Parameters
        ----------
        config_file : string, optional
            full name of configuration parameters file.
        
        Returns
        -------
        None.
        
        """
        # read configuration file for MissionPlanner Object
        self.set_and_read_config(config_file)
        self.run_check_list()
        
        return
    
    def launch(self):
        """Launches the computation of the trajectory.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('========= LAUNCH MISSIONPLAN ==========')
        launch_time = time.time()
        # run check-list before anything.
        if not self.check_list:
            self.run_check_list()
        
        # if flight_type=inflight_lora, re-compute the initial conditions
        if (self.flight_type == 'inflight_lora'):
            self.set_initialcond()
        
        # launches the integration
        (landing_time,landing_Eloc) = self.integration()
        
        # save the data
        self.data_save(self.traj,self.trajectory_sim_file)
        
        log.info('Simulation done.')
        log.info('The Simulation took '+str(time.time()-launch_time)+' to run.')
        
        return (landing_time,landing_Eloc)
    
    def vel2vel_ms(self,vel):
        """Convert vel to vel_ms
        
        Parameters
        ----------
        vel : astropy.units.Quantity
            Velocity 3-vector
        
        Returns
        -------
        vel_ms : 3-array of float
            Velocity 3-vector values in m/s, without unit.
        
        """
        vel_ms = np.array([vel[0].to('m/s').value,
                           vel[1].to('m/s').value,
                           vel[2].to('m/s').value])
        return vel_ms
    
    def vel_ms2vel(self,vel_ms):
        """Convert vel_ms to vel
        
        Parameters
        ----------
        vel_ms : 3-array of float
            Velocity 3-vector values in m/s, without unit.
        
        Returns
        -------
        vel : astropy.units.Quantity
            Velocity 3-vector
        
        """
        vel = vel_ms * u.m/u.s
        return vel
    
    
    def integration(self):
        """ Numerical integration for prediction of new location for any flight phase.
        
        Parameters
        -----------
        None.
            
        Returns
        -------
        None.
        
        """
        # start software timer for debug purpose.
        self.integ_time = time.time()
        
        # Set the initial conditions for the integration
        self.time_rel = 0.0*u.s
        self.time_utc = copy.copy(self.Time_start)
        
        # Set the position the integration at the last point
        self.loc = self.Eloc_start
        # Set the velocity
        self.vel = self.vel_start
        self.vel_ms = self.vel2vel_ms(self.vel)
        # Set the acceleration
        self.acc = [0.0,0.0,0.0]*u.m/u.s**2
        
        # Set the gravitational acceleration
        self.g = atmosphere.compute_g(self.loc.lat)
        self.g_acc = [0.0,0.0,self.g.to('m/s2').value]*u.m/u.s**2
        
        # Compute wind_3D and Rho for current position
        (self.wind_3D,self.P,self.T,self.Rho) = self.get_atm_param(self.loc.lon, self.loc.lat, self.loc.height)
        # correct Atmosphere data if out of range
        if (self.trajectory_type == 'ASCEND') and (self.loc.height >= self.Atm.max_alt):
            self.P, self.Rho, self.T = density.getPRhoT(self.loc.height,self.g)
            log.debug('********** GETTING ISA parameters: *********')
            log.debug(self.loc.height)
            log.debug(self.P)
            log.debug(self.T)
            log.debug(self.Rho)
        
        # Compute initial relative wind
        (self.relWvel,self.relwindnorm,self.relwindunit) = compute_relative_wind(self.vel_ms,self.wind_3D)
        # compute speed of sound and Mach number
        self.c_sound,self.Mach = utils.compute_c_M(self.relwindnorm,self.P,self.Rho)
        
        # Creates Trajectory object.
        self.traj = trajectory.Trajectory()
        # insert initial data into Trajectory
        data = [self.time_utc.isot, self.loc.lon, self.loc.lat, self.loc.height,
                self.vel_ms[0],self.vel_ms[1],self.vel_ms[2],
                self.wind_3D[0],self.wind_3D[1],self.wind_3D[2],
                self.relWvel[0],self.relWvel[1],self.relWvel[2],
                self.c_sound,self.Mach,
                self.P, self.T,self.Rho]
        self.traj.add_data(data)
        
        # computes ground altitude
        log.debug('Getting ground alt')
        groundalt = self.GD.interpol_ign_data(self.loc.lon,self.loc.lat)
        log.debug('groundalt='+str(groundalt))
        
        # make time_step dict
        time_step = {'ASCEND' : self.time_step_up,
                     'FREEFALL' : self.time_step_dwn,
                     'CEILING' : self.time_step_ceil,
                     'GLIDE' :  self.time_step_glide}
        
        vertical_speed = {'ASCEND' : self.Vopt,
                          'CEILING' : -self.ceil_Vdesc}
        
        ###################### INTEGRATION LOOP ################333
        keep_going=True
        while(keep_going):
            # set time step as a function of trajectory type
            self.time_step = time_step[self.trajectory_type]
            
            # forces velocity if trajectory is 'ASCEND' or 'CEILING'
            if (self.trajectory_type == 'ASCEND') or (self.trajectory_type == 'CEILING'):
                # new velocity = absolute wind velocity
                self.vel_ms = [self.wind_3D[0].to('m/s').value,
                               self.wind_3D[1].to('m/s').value,
                               vertical_speed[self.trajectory_type].to('m/s').value]
            
            else: # DESCENDING
                # case free fall
                if (self.trajectory_type == 'FREEFALL'):
                    # Numerical integration - 2nd order
                    self.ODE_output = odeint(self.Forces_freefall,self.vel_ms,[0.0,self.time_step.to('s').value],
                                     args=(self.relwindnorm,self.relwindunit,self.g_acc),
                                     full_output=True,
                                     hmax=self.time_step.to('s').value,
                                     hmin = 0.01)
                    # new velocity
                    self.vel_ms = [self.ODE_output[0][1][0],self.ODE_output[0][1][1],self.ODE_output[0][1][2]]
            
            # Computation of new state:
            self.compute_new_state(self.traj)
            
            # check if trajectory is CEILING
            if (self.trajectory_type == 'CEILING'):
                # check for end of ceiling phase with ceiling_time and duration
                if self.ceil_time.to('s')<=self.ceil_dur.to('s'):
                    self.ceil_time = self.ceil_time + self.time_step
                    log.debug('Ceil time: '+str(self.ceil_time.to('min')))
                else:
                    # change of trajectory type: from ceiling to free fall
                    log.info('========= MAXIMUM CEILING TIME REACHED: CHANGING TRAJECTORY TYPE')
                    # Set ceiling duration to 0 to make sure the trajectory type is no more changed.
                    #self.ceil_dur = 0.0 * u.min
                    self.trajectory_type = 'FREEFALL'
                    log.info('NEW TRAJECTORY TYPE: '+self.trajectory_type)
                    
            # ====== check change of flight phase ======
            # Check if at top: NOTE: for inflight trajectory you might be above HXmax
            if (self.trajectory_type == 'ASCEND'):
                if (self.loc.height >= self.HXmax):
                    log.info('========= MAXIMUM ALTITUDE REACHED: CHANGING TRAJECTORY TYPE')
                    # change trajectory type to 'CEILING' or 'FREEFALL'
                    if self.ceil_dur.value:
                        self.trajectory_type = str('CEILING')
                        self.ceil_time = 0.0*u.s
                    else:
                        self.trajectory_type = str('FREEFALL')
                    log.info('NEW TRAJECTORY TYPE: '+self.trajectory_type)
            
            
            # check for the need to compute ground altitude
            if (self.trajectory_type == 'FREEFALL') or (self.trajectory_type == 'GLIDE'):
                if (self.loc.height < self.MaxGroundAlt):
                    groundalt = self.GD.interpol_ign_data(self.loc.lon,self.loc.lat)
                    self.check_groundalt(groundalt)
                    log.debug('groundalt: '+str(groundalt))
                    # Stop the integration if at ground
                    if (self.loc.height < groundalt):
                        keep_going = False
                        log.info('You have (hopefully softly) hit the ground!')
                        landing_time = self.time_utc
                        landing_Eloc = self.loc
            
            # check if get under the gliding altitude
            if (self.trajectory_type == 'FREEFALL') and (self.loc.height < self.glide_alt):
                log.info('=========================================')
                log.info('============== GLIDING PHASE ============')
                log.info('=========================================')
                # stop the main while loop
                keep_going=False
                self.trajectory_type = 'GLIDE'
                # increase time step
                self.time_step = time_step[self.trajectory_type]
                # compute minimum distance to site under which it is useless to keep the computation
                min_dist_site = self.glide_vel.to('m/s') * self.time_step.to('s')
                # save initial condition at transition, so we can loop over all the sites.
                self.fall2glide_loc = self.loc
                self.fall2glide_time = self.time_utc
                self.fall2glide_time_rel = self.time_rel
                self.fall2glide_wind3D = self.wind_3D
                self.fall2glide_rho = self.Rho
                
                # if computing an ellpise zone, re-make the landing site dict
                Lmake_site = False
                try:
                    if (self.config['CAMPAIGN']['try_land']=='ellipse'):
                        log.warning('*** Re-make landing sites')
                        Lmake_site = True
                    else:
                        log.info('Considering possible landing sites in '+self.config['CAMPAIGN']['sites_file'])
                except:
                    log.info('Keyword try_land not found in '+self.config['CAMPAIGN']['sites_file'])
                if Lmake_site:
                    self.make_sites_from_eloc(self.fall2glide_loc)
               
                # sort landing site as a function of distance
                sorted_site_idazdist = self.sort_sites(self.fall2glide_loc)
                # initializes variable
                decision_tab = QTable(names=['id','Feasible','Time','margin'],dtype=['int','bool','object','float'])
                glide_trajs = {}
                # stop the loop in order to save CPU time
                stop_loop = False
                # minimum number of feasible sites to stop the loop
                min_site_ok = 3
                num_site_ok = 0
                # loop over all landing sites
                for site in sorted_site_idazdist:
                    log.info('***************************************************')
                    log.info('*** NOW TESTING SITE: '+str(site['id'])+' **********')
                    log.info('***************************************************')
                    # set initial conditions to fall2glide
                    self.loc = self.fall2glide_loc
                    self.time_utc = self.fall2glide_time
                    self.time_rel = self.fall2glide_time_rel
                    self.wind_3D = self.fall2glide_wind3D
                    self.Rho = self.fall2glide_rho
                    glide_trajs[site['id']] = trajectory.Trajectory()
                    # launches the integration
                    keep_gliding = True
                    while(keep_gliding):
                        # new velocity = towards landing point, taking into account the wind.
                        self.vel_ms = self.get_glide_vel(self.loc,site['id'],self.wind_3D)
                        self.compute_new_state(glide_trajs[site['id']])
                        # compute ground altitude
                        groundalt = self.GD.interpol_ign_data(self.loc.lon,self.loc.lat)
                        self.check_groundalt(groundalt)
                        log.info('                                                               groundalt: '+str(groundalt))
                        # stop rule 1: hit the ground
                        if (self.loc.height < groundalt):
                            log.info('You have (hopefully softly) hit the ground!')
                            decision_tab.add_row([site['id'],False,self.time_utc.isot,0.0])
                            keep_gliding = False
                        # stop rule 2: above the landing site
                        (dist,az) = utils.distaz(self.loc,self.landingsites['SITE'+str(site['id'])]['eloc'])
                        if (dist < min_dist_site):
                            log.info('You have reached site # '+str(site['id'])+' at '+self.time_utc.isot+' ; fyi launch site:'+self.config['INITCOND']['initcond_site'])
                            log.info('Your margin is : '+str(self.loc.height-groundalt))
                            decision_tab.add_row([site['id'],True,self.time_utc.isot,(self.loc.height-groundalt).to('m')])
                            keep_gliding = False
                            # decide to keep this one for decision
                            if ((self.loc.height-groundalt)>self.min_margin):
                                num_site_ok = num_site_ok+1
                                log.info('num_site_ok = '+str(num_site_ok))
                            # if take off base is ok, then return to base and stop computation
                            if (site['id']==self.config['INITCOND']['initcond_site']):
                                log.warning('Take off base reachable: no need to further compute')
                                stop_loop = True
                            # double check if it is worth keeping computing possible landing site
                            if (num_site_ok>=min_site_ok):
                                log.warning('Minimum number of feasible landing site reached: no need to keep computing.')
                                stop_loop = True
                    # end of while keep_gliding
                    # save glide trajectory if (self.flight_type == 'prevision'):
                    if (self.flight_type == 'prevision'):
                         self.traj.data = vstack((self.traj.data,glide_trajs[site['id']].data))
                    # stop the for loop if we have enough possible landing sites
                    if stop_loop:
                        log.warning('Breaking the loop')
                        break
                
                # store output
                decision_file = self.outdir + 'decision.dat'
                decision_tab.write(decision_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
                log.info('Decision table saved in '+decision_file)
                # decide which site to reach
                (decision_feasible,decide_id,decide_time,decide_margin) = site_decide(decision_tab)
                log.info('Decide to reach site: #'+str(decide_id)+' : '+self.landingsites['SITE'+str(decide_id)]['name'])
                log.info('Feasibility: '+str(decision_feasible))
                log.info('Expected time at site vertical: '+decide_time.isot)
                log.info('Vertical velocity: '+str(self.glide_vel.to('m/s')/self.lift2drag))
                landing_time = decide_time + decide_margin*u.m / (self.glide_vel.to('m/s')/self.lift2drag)
                log.info('Expected time to land: '+landing_time.isot)
                log.info('Margin: '+str(decide_margin))
                landing_Eloc = self.landingsites['SITE'+str(decide_id)]['eloc']
                # append trajectory to chosen site to main trajectory
                self.traj.data = vstack((self.traj.data,glide_trajs[decide_id].data))
                # send site decision to ground
                # send_decision(site_to_reach)
        # end of the integration
        
        # show how long was the numerical integration
        log.debug('The computation took :'+str(time.time()-self.integ_time)+' sec to run')
        
        return (landing_time,landing_Eloc)
    
    def compute_new_state(self,traj):
        """Compute new state.
        
        Uses global variables only.
        
        Parameters
        ----------
        traj : trajectory.Trajectory Object
            Trajectory to add data to.
        
        Returns
        -------
        None.
        
        """
        # new time
        self.time_rel = self.time_rel + self.time_step
        self.time_utc = self.time_utc + self.time_step
        # set new velocity
        self.vel = self.vel_ms2vel(self.vel_ms)
        # new location
        newloc = compute_new_position(self.loc,self.vel,self.time_step)
        self.loc = copy.copy(newloc)
        # Set the gravitational acceleration
        self.g = atmosphere.compute_g(self.loc.lat)
        self.g_acc = [0.0, 0.0, self.g.to('m/s2').value]*u.m/u.s**2
        # new wind_3D, P, T and Rho
        self.wind_3D,self.P,self.T,self.Rho = self.get_atm_param(self.loc.lon, self.loc.lat, self.loc.height)
        # correct Atmosphere data if out of range
        if (self.loc.height >= self.Atm.max_alt):
            self.P, self.Rho, self.T = density.getPRhoT(self.loc.height,self.g)
            log.debug('*** Altitude higher than Atmosphere data: GETTING ISA parameters')
        # new wind relative velocity
        (self.relWvel,self.relwindnorm,self.relwindunit) = compute_relative_wind(self.vel,self.wind_3D)
        # new speed of sound and Mach number
        self.c_sound,self.Mach = utils.compute_c_M(self.relwindnorm,self.P,self.Rho)
        # add all new data to Trajectory object
        traj.add_data([self.time_utc.isot, self.loc.lon, self.loc.lat, self.loc.height,
                       self.vel[0],self.vel[1],self.vel[2],
                       self.wind_3D[0],self.wind_3D[1],self.wind_3D[2],
                       self.relWvel[0],self.relWvel[1],self.relWvel[2],
                       self.c_sound,self.Mach,
                       self.P, self.T,self.Rho])
        # ctrl output
        log.debug('------------- NEW STATE --------------')
        log.debug('Time:'+self.time_utc.isot)
        log.info('Lon: '+str(self.loc.lon.to('deg').value)+' lat: '+str(self.loc.lat.to('deg').value)+' alt: '+str(self.loc.height.to('m').value))
        log.debug('VELOCITY: '+str(self.vel))
        log.debug('ACCELERATION: '+str(self.acc.to('m/s2')))
        log.debug('wind_3D: '+str(self.wind_3D))
        log.debug('P: '+str(self.P)+' T: '+str(self.T)+' rho: '+str(self.Rho))
        return
    
    def Forces_freefall(self,vel_ms,t,relwindnorm,relwindunit,g_3D): 
        """ Computation of the forces acting on either the balloon or payload.
        
        Parameters
        ----------
        vel_ms : 3-vector of floats
            Velocity [m/s]
        t : float
            relative time.
        relwindnorm : astropy.units.Quantity
            Relative wind vector norm in [m/s].
        relwindunit : 3-vector of float
            Relative wind unit vector.
        g_3D : astropy.units.Quantity 3-vector.
            Gravity acceleration in [m/s2]
        
        Returns
        -------
        self.acc : 3-vector of floats
            Acceleration [m/s**2]
        
        """
        # Gravitational force
        #log.debug('self.payld_mass='+str(self.payld_mass))
        #log.debug('g_3D='+str(g_3D))
        self.Fg_3D = -(self.payld_mass)*g_3D
        #log.debug('self.Fg_3D='+str(self.Fg_3D))
        
        # Drag force
        self.F_d = 0.5 * self.cd * self.Rho * 2 * np.pi * self.equ_rad**2 * relwindnorm**2
        #log.debug('self.F_d='+str(self.F_d))
        self.Fdrag = self.F_d * relwindunit
        #log.debug('self.Fdrag='+str(self.Fdrag))
        self.Fd_3D = np.array(self.Fdrag.value)*u.N
        #log.debug('self.Fd_3D='+str(self.Fd_3D))
        
        # Sum of all forces
        self.F_sum = self.Fd_3D + self.Fg_3D
        
        # acceleration from equation of motion
        self.acc = self.F_sum / self.payld_mass
        #log.debug('self.acc='+str(self.acc))
        
        return self.acc
    
    def get_glide_vel(self,cur_eloc,site_id,wind_3D):
        """Compute velocity in GLIDE phase.
        
        Parameters
        ----------
        cur_eloc : astropy.coordinates.EarthLocation object
            Current Location
        site_id : int
            Site id towards which the payload is gliding.
        
        Returns
        -------
        vel_ms : 3-vector of floats
            Velocity towards site.
        
        """
        site_param = 'SITE'+str(site_id)
        # compute azimuth and distance fo site
        (self.dist_tosite,self.az_tosite) = self.distaz2site(cur_eloc,site_param)
        # compute glider velocity at given altitude
        Rho0 = 1.225 * u.kg/u.m**3
        new_glide_vel = np.sqrt(Rho0 / self.Rho) * self.glide_vel.to('m/s')
        log.info('Alt: '+str(cur_eloc.height)+'  rho: '+str(self.Rho)+'  glide_vel: '+str(new_glide_vel))
        
        # velocity of the glider flying at glide_vel, without wind consideration
        vel_wownd = np.array([new_glide_vel.to('m/s').value*np.cos(3.0*np.pi/2.0-self.az_tosite.to('rad').value),
                              new_glide_vel.to('m/s').value*np.sin(3.0*np.pi/2.0-self.az_tosite.to('rad').value),
                             -new_glide_vel.to('m/s').value/self.lift2drag])
        # convert wind vector into float: WHERE THE WIND S BLOWING TO!!!
        wnd_ms = np.array([wind_3D[0].to('m/s').value,
                           wind_3D[1].to('m/s').value,
                           wind_3D[2].to('m/s').value])
        # subtract the wind velocity
        vel_ms = vel_wownd + wnd_ms
        
        log.debug('vel_wownd: '+str(vel_wownd))
        log.debug('wnd_ms: '+str(wnd_ms))
        log.debug('vel_ms: '+str(vel_ms))
        
        return vel_ms
    
    def data_save(self,traj,outfile):
        """Save the Trajectory data into output file.
        
        Parameters
        ----------
        traj : trajectory.Trajectory object
            Trajectory to save.
        outfile : string
            Output file name.
        
        Returns
        -------
        None.
        """
        # Save all data into trajectory output file
        traj.save(outfile)
        log.info('Data saved in '+outfile)
        
        # save trajectory into kml file
        kmlfile = outfile.replace('dat','kml')
        self.traj.to_kml(kmlfile)
        log.info('KML file saved in '+kmlfile)
        
        # save trajectory as SPICE kernel
        if '.dat' in outfile:
            spk_file = outfile.replace('.dat','.spk')
        else:
            spk_file = outfile + '.spk'
        log.info('now saving trajectory into SPK file: '+spk_file)
        # save trajectory as SPICE kernel
        self.traj.to_spk(self.nacelle_name,self.nacelle_id,spk_file,kernel_path=self.kernel_path,spice_exe_path=self.spice_exe_path)
        
        # make some plots of the trajectory
        self.traj.plot_all(self.outdir)
        log.info('All plots saved in '+self.outdir)
        return
    
    def load_traj(self,traj_file):
        """Load a Trajectory object.
        
        Parameters
        ----------
        traj_file : string
            Trajectory file name
        
        
        Returns
        -------
        None.
        
        """
        self.traj = trajectory.Trajectory()
        self.traj.load(traj_file)
        log.info('Trajectory from file '+traj_file+' loaded')
        return
    
    def get_last_point(self):
        """Get last point of the trajectory.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        last_time : astropy.time.Time object
            Time of last point
        last_point : astropy.coordinates.EarthLocation object
            Location of last point
        
        """
        return self.traj.get_last_point()
    
    def distaz2site(self,cur_eloc,param_site):
        """Get the distance and azimuth from current location to landing site.
        
        Parameters
        ----------
        cur_eloc : astropycoordinates.EarthLocation object
            current locations
        param_site : string
            Landing Site name, i.e. 'SITE'+str(site_id)
        
        Returns
        -------
        dist : astropy.units.Quantity
            Distance between cur_eloc and param_site [m].
        az : astropy.units.Quantity
            azimuth between cur_eloc and param_site [deg].
        
        """
        (dist,az) = utils.distaz(cur_eloc,self.landingsites[param_site]['eloc'])
        log.info('                           az: '+str(az.to('deg'))+' dist: '+str(dist.to('km')))
        return (dist,az)
    
    def sort_sites(self,cur_eloc):
        """Get the distance and azimuth of all possible landing sites, and sort them as a function of distance.
        
        Parameters
        ----------
        cur_eloc : astropycoordinates.EarthLocation object
            current location.
        
        Returns
        -------
        list of sites sorted by the distance from current location.
        
        """
        log.info('*** Now sorting the sites')
        idazdist = QTable(names=['id','az','dist','name'],dtype=['int','float','float','object'])
        idazdist['dist'].unit = u.m
        idazdist['az'].unit = u.deg
        # loop over the landingsites
        for param_site in self.landingsites:
            id_site = self.landingsites[param_site]['id']
            name_site = self.landingsites[param_site]['name']
            log.debug('name_site: '+name_site)
            (dist,az) = self.distaz2site(cur_eloc,param_site)
            idazdist.add_row([id_site,az,dist,name_site])
        # clean and sort table
        idazdist.sort('dist')
        # put launch site id at first
        index_launch_site = np.where(idazdist['id']==int(self.config['INITCOND']['initcond_site']))[0][0]
        log.info('index_launch_site : '+str(index_launch_site))
        launch_row = idazdist[index_launch_site]
        # save the data for later put into the table
        launch_id = launch_row['id']
        launch_az = launch_row['az']
        launch_dist = launch_row['dist']
        launch_name = launch_row['name']
        log.info('launch_row : '+str(launch_row))
        # put launch site at beginning: 1st reverse, then add, then reverse again.
        idazdist.reverse()
        idazdist.add_row([launch_id,launch_az,launch_dist,launch_name])
        idazdist.reverse()
        idazdist.remove_row(index_launch_site+1)
        
        log.info('Sorted sites:')
        for site in idazdist:
            log.info(str(site['id'])+' '+str(site['dist'].to('km'))+' '+str(site['az'])+' '+site['name'])
        
        return idazdist
    
    def check_groundalt(self,groundalt):
        """Check ground altitude for NaN.
        
        Parameters
        ----------
        groundalt : astropy.units.Quantity object.
            Ground altitude  [m]
        
        Returns
        -------
        groundalt : astropy.units.Quantity object.
            Ground altitude  [m]
        
        """
        if (np.isnan(groundalt.value)):
            msg = '*** NAN DETECTED!!!'
            log.error(msg)
            groundalt = -1 * u.m
            #raise ValueError(msg)
        
        return groundalt



if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='missionplanner arguments.')
    parser.add_argument('-conf',default='./config.in',help='configuration file. Default is: ./conf/config.in')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.conf
    
    # create Object
    missplan = MissionPlanner(config_file)
    # launch simulation
    missplan.launch()
else:
    log.debug('successfully imported')

