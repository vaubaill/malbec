#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 11:46:07 2023

@author: vaubaill
"""

# read grib file and output a table ascii file
import numpy as np
from astropy import units as u
from missionplanner import atmosphere_ram as atmosphere
from missionplanner import utils
from astropy.table import QTable


def get_atm_param(Atm,lon, lat, alt):
    """Get all the atmospheric parameters.
    
    Parameters
    ----------
    lon : astropy.coordinates.Angle Object
        longitude coordinate
    lat : astropy.coordinates.Angle Object
        latitude coordinate
    alt : astropy.units Quantity object
        Altitude coordinate.
    
    Returns
    -------
    wind_3D : array of astropy.units.Quantity objects.
        wind 3-vector [m/s] DIRECTION WHERE THE WIND IS GOING TO (not where it's coming form)
        U: zonal wind, positive in the East [m/s]
        V: meridional wind, positive to the North [m/s]
        W: vertical wind positive towards the Zenith [m/s]
    P : astropy.units.Quantity object
        air Pressure [Pa]
    T : astropy.units.Quantity object
        air Temperature [K]
    Rho : astropy.units.Quantity object
        air density [kg/m-3].
    """
    # get the wind, pressure and temperature from the selected atmosphere object
    # there is a problem at 30km = FIXXXXX or get data (P n T) from ISA, keep the wind the same
    (wind_3D,P,T) = Atm.get_windPT(lon,lat,alt)
    # compute equivalent density
    Rho = utils.compute_rhoatmo(P,T)
    return (wind_3D,P,T,Rho)

grib_file='/Users/vaubaill/Downloads/PA_EUROC25_23040408002467896.KEYuUddDxxODBO2fv7A9uu.grib'
data_file = grib_file.replace('.grib','.dat')
atm = atmosphere.Atmosphere(grib_file)

lat = 44.43*u.deg   # positive in North
lon = -0.50*u.deg # positive is East

data = QTable(names=['alt [m]','U [m/s]','V [m/s]','W [m/s]','P [Pa]','T [K]','rho [kg/m3]'])

for alt in np.arange(1,40,0.1)*u.km:
    (wind_3D,P,T,Rho) = get_atm_param(atm, lon, lat, alt)
    data.add_row([alt.to('km'),
                  wind_3D[0].to('m/s'),
                  wind_3D[1].to('m/s'),
                  wind_3D[2].to('m/s'),
                  P.to('Pa'),
                  T.to('K'),
                  Rho.to('kg/m3')
                  ])
    print(wind_3D[0].to('m/s').value,
          wind_3D[1].to('m/s').value,
          wind_3D[2].to('m/s').value,
          P.to('Pa').value,
          T.to('K').value,
          Rho.to('kg/m3').value)

data.write(data_file,format='ascii.fixed_width_two_line',overwrite=True)
print('data saved in '+data_file)