#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 10:43:02 2019

@author: dzilkova
"""
import astropy.units as u
from math import *
import numpy as np

from astropy.io import ascii
from astropy.time import Time as Time
from astropy.time import TimeDelta as TimeDelta
from astropy.coordinates import cartesian_to_spherical
from astropy.units.function.logarithmic import LogQuantity
from astropy.table import Table

from missionplanner.atmosphere_ram import compute_g

R_earth = 6.3781*10**6 *u.m
R=287.05 *u.J/(u.K*u.kg)



def getGeopotential(h_Gm):
    """Calculate geopotential altitude from Geometric altitude.
    
    Parameters
    ----------
    h_Gm : float
         geometric altitude (altitude from flying object to the center of earth) [m]
         
    Returns
    -------
    h_Gp : float
        geopotential altitude (accounts for g at certain altitude -> from hydrostatic equation) [m]
        
    Derivation:
    ----------
    http://www.pdas.com/geopot.pdf
    
    """   
    h_Gp = (R_earth/(R_earth+h_Gm))*h_Gm
    return h_Gp

def getGeometric(h_Gp):    
    """Calculate geometric altitude from Geopotential altitude.
    
    Parameters
    ----------
    h_Gp : float
         geopotential altitude (accounts for g at certain altitude -> from hydrostatic equation) [m]
         
    Returns
    -------
    h_Gm : float
        geometric altitude (altitude from flying object to the center of earth) [m]
        
    Derivation:
    ----------
    http://www.pdas.com/geopot.pdf
    
    """
    h_Gm = (R_earth/(R_earth-h_Gp))*h_Gp
    return h_Gm


def getPRhoT(h,g):
    """Calculate the density according to the altitude.
    
    Parameters
    ----------
    h : astropy.units.Quantity object.
        geopotential altitude [m]
    g : astropy.units.Quantity object.
        acceleration of gravity [m/s**2]
    
    Returns
    -------
    Rho : float
          air density [kg/m-3]
    
    
    """
    # select altitude case
    if  0 *u.m < h < 11000.*u.m:
        h0 = 0 *u.m
        a = -0.0065 *u.K/u.m
        T0 = 288.15 *u.K
        Rho_init = 1.225 *u.kg/u.m**3
        P_init = 101325 *u.Pa
        T = T0 + a*(h-h0)
        Rho = Rho_init*((T/T0)**(-((g/(a*R))+1)))
        Press = P_init*((T/T0)**(-(g/(a*R))))
    if 11000.*u.m <= h < 20000.*u.m:
        # a = 0
        h0 = 11000.*u.m
        Rho_init = 0.363918*u.kg/u.m**3
        T = 216.65*u.K
        P_init = 22632.1*u.Pa
        Rho = Rho_init * e**(-(g/(R*T))*(h-h0))
        Press = P_init * e**(-(g/(R*T))*(h-h0))
    elif 20000.*u.m <= h < 32000.*u.m:
        h0 = 20000.*u.m
        a = 0.001*u.K/u.m
        T0 = 216.65*u.K
        Rho_init = 0.0880349*u.kg/u.m**3
        P_init = 5474.86*u.Pa
        T = T0 + a*(h-h0)
        Rho = Rho_init*((T/T0)**(-((g/(a*R))+1)))
        Press = P_init*((T/T0)**(-(g/(a*R))))
        
    elif 32000.*u.m<= h < 47000.*u.m:
        h0 = 32000.*u.m
        a = 0.0028*u.K/u.m
        Rho_init = 0.013225*u.kg/u.m**3
        T0 = 228.65*u.K   
        P_init = 868.003*u.Pa
            
        T = T0 + a*(h-h0)
        Rho = Rho_init*((T/T0)**(-((g/(a*R))+1)))
        Press = P_init*((T/T0)**(-(g/(a*R))))
        
    return (Press, Rho, T)

def getAlt(P): 
    """Calculate the density form meassured pressure.
    
    Parameters
    ----------
    P : float
        pressure [Pa]
    T : float
        temperature [K]
        
    Returns
    -------
    Altitude : float
    	altitude [m]
    """
    
    # initialize Altitude
    Altitude = P.value*0.0 *u.m
    
    mask=np.asarray([(101325 > P.to('Pa').value) & (P.to('Pa').value >22632.06)])
    loc=mask.nonzero()[1]
    h0 = 0 *u.m
    a = -0.0065 *u.K/u.m
    T0 = 288.15 *u.K
    P_init = 101325*u.Pa
    
    Altitude[loc] = ((((P[loc].to('Pa')/P_init)**(-(R*a/g)))-1)*(T0/a))+h0
    
    
    ########
    mask=np.asarray([(22632.06 > P.to('Pa').value) & (P.to('Pa').value >5474.86)]  )
    loc=mask.nonzero()[1] 
    # a = 0
    h0 = 11000.*u.m
    P_init = 22632.06*u.Pa
    T = 216.65*u.K
        
    Altitude[loc] = h0 + ((-R*T/g)*(np.log(P[loc]/P_init)))
    
    
    ########
    mask=np.asarray([(5474.86 > P.to('Pa').value) & (P.to('Pa').value >868.003)])
    loc=mask.nonzero()[1] 
    h0 = 20000.*u.m
    a = 0.001*u.K/u.m
    T0 = 216.65*u.K
    P_init = 5474.86*u.Pa
    
    Altitude[loc] = ((((P[loc]/P_init)**(-(R*a/g)))-1)*(T0/a))+h0

    
    mask=np.asarray([(868.003 > P.to('Pa').value) & (P.to('Pa').value > 110.826)])
    loc=mask.nonzero()[1] 
    h0 = 32000.*u.m
    a = 0.0028*u.K/u.m
    T0 = 228.65*u.K 
    P_init = 868.003*u.Pa
        
        
    Altitude[loc] = ((((P[loc]/P_init)**(-(R*a/g)))-1)*(T0/a))+h0      
    """
    h0 = 0 *u.m
    a = -0.0065 *u.K/u.m
    T0 = 288.15 *u.K
    P_init = 101325*u.Pa

    Altitude = ((((P.to('Pa')/P_init)**(-(R*a/g)))-1)*(T0/a))+h0
    """
    return Altitude
