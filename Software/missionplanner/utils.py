import logging
import sys
import os
import shutil
import numpy as np
from pygc import great_distance, great_circle
from astropy.time import Time
from astropy.coordinates import EarthLocation
import astropy.units as u
import spiceypy as sp
from configparser import ConfigParser, ExtendedInterpolation

# make default logger object
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
# default logger format
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
# default log file name from current time
strt = 'log-' + Time.now().isot.replace('-','').replace(':','').split('.')[0] 
# create StreamHandler
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
# add StreamHandler into logger
log.addHandler(sdlr)  


###################################################
# 
# LOGGER-RELATED ROUTINES
# 
################################################### 
def make_name(log_dir='./',root='',ext='.log',Notime=False):
    """Make a log file name from the date the program is launched.
    
    Parameters
    ----------
    log_dir : string, optional
        log file directory. Default is './'
    root : string, optional
        root of the file name (prepended to the file name). Default is empty string.
    ext : string, optional
        extension of the file name (appended to the file name). Default is '.log'
    Notime : Boolean
        If set to False (Default) a time string is appended to the name of the filename.
        If True, the filename has no time info.
    
    Returns
    -------
    filename : string
        File name based on current time, build according to: log_dir + root + time + ext if Notime is False,
        and to log_dir + root + ext if Notime is True.
    
    """
    if Notime:
       filename = log_dir + root + ext
    else:
       filename = log_dir + root + strt + ext
    return filename

def add_FileHandler(filename=None):
    """Make a logging.Logger Object
    
    Parameters
    ----------
    filename : string, optional
        Log full path file name. If not set, an automated name will be created.
    
    Returns
    -------
    log : logging.Logger Object
        Logger object
    
    """    
    # set filename from config object
    if not filename:
            filename = make_name(root='log')
    print('filename: '+filename)
    
    # create FileHandler
    hdlr = logging.FileHandler(filename)
    hdlr.setLevel(logging.DEBUG)
    hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    print('add hdlr')
    log.addHandler(hdlr)
    return 

def get_log_fmt():
    """Get default log format.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    None.
    
    """
    return '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '

def change_alllog(name):
    """Change all the log settings
    
    Parameters
    ----------
    name : string
        name of the log root
    
    Returns
    -------
    None
        
    """
    cleaname = name.replace(' ','').replace('(','').replace(')','')
    change_logfile(cleaname)
    return

def change_logfile(log,old_file_name,new_file_name):
    """Change Fripipe default output log file name.
    
    Parameters
    ----------
    log : logging.Logger Object
        logger for which the file stream handler name should be changed.
    old_file_name : string
        name of the old log file.
    new_file_name : string
        name of the new log file.
    
    Returns
    -------
    None
        
    """
    # create file handler objects
    hdlr_old = logging.FileHandler(old_file_name)
    hdlr_new = logging.FileHandler(new_file_name)
    
    # close and remove old fileHandler
    hdlr_old.close()
    log.removeHandler(hdlr_old)
    
    # rename the output file
    log.info('renaming '+old_file_name+' into '+new_file_name)
    try:
        shutil.move(old_file_name,new_file_name)
    except:
        msg = '*** FATAL ERROR: impossible to move '+old_file_name+' into '+new_file_name
        log.error(msg)
        raise IOError(msg)
    
    # set up new file handler
    hdlr_new.setLevel(logging.DEBUG)
    hdlr_new.setFormatter(logging.Formatter(fmt=get_log_fmt()))
    log.addHandler(hdlr_new)
    
    log.info('Log file changed to '+new_file_name)
    return

def get_module_logger(mod_name,logfile='',addsldr=True):
    """Create a logger.
    
    Parameters
    ----------
    mod_name : string
        name of logger object.
    logfile : string
        name of the new log file.
    addsldr : Logical, optional
        if True, a StreamHandler is created. Default is False.
	
    Returns
    -------
    None
        
    """
    # create logger
    log = logging.getLogger(mod_name)
    # log format
    log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
    # set log level
    log.setLevel(logging.DEBUG)
    log.propagate = True
    # create StreamHandler
    sdlr = logging.StreamHandler()
    sdlr.setLevel(logging.DEBUG)
    sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    if addsldr:
        log.addHandler(sdlr)
    # create FileHandler
    if logfile:
        hdlr = logging.FileHandler(logfile)
        hdlr.setLevel(logging.DEBUG)
        hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
        log.addHandler(hdlr)
    # log the log file name
    log.info('Log file: '+logfile)
    
    return log

###################################################
# 
# FILE HANDLING ROUTINES
# 
################################################### 
def check_file(filename,log):
    """Check existence of a file.
    Usefull especially for the several configuration files.
    Raise IOError if file does not exist.
    
    Parameters
    ----------
    filename : string
        Full path name of the file to check.
    log : logging.Logger object
        Logger whre debug info are sent to.
    
    Returns
    -------
    None.
    
    
    """
    if not os.path.exists(filename):
        msg = '*** fatal error: file '+filename+' does not exist.'
        log.error(msg)
        raise IOError(msg)
    log.debug(filename+' ok')
    return

def check_dir(dirname,log):
    """Check existence of a directory.
    
    Usefull especially for the several configuration files.
    Raise IOError if directory does not exist.
    
    Parameters
    ----------
    dirname : string
        Full path name of the directory to check.
    log : logging.Logger Object
        Logger to put debug message.
    
    Returns
    -------
    None.
    
    
    """
    log.debug('checking existence of directory: '+dirname)
    if not os.path.isdir(dirname):
        msg = '*** fatal error: directory '+dirname+' does not exist.'
        log.error(msg)
        raise IOError(msg)
    log.debug(dirname+' ok')
    return

def check_output_dir(outdir,log):
    """
    Check and if does not exist create output directory.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    Output dir
           
    """
    # if needed create the output directory
    if not os.path.exists(outdir):
        log.info('now creating output directory: '+outdir)
        os.makedirs(outdir)
    log.debug('Output directory: '+outdir+' ok')
    return

def check_disk(log,limit=1024.0):
    """Check if there is enough disk space left on device.
    
    If not a warning is logged and the data saving procedure is not launched
    but the computation can still happen.
    
    Parameters
    ----------
    limit : float, optional
        Disk space limit under which it is considered that there is not enough
        space to save the data, in Mb. Default is 1024.0
    
    Returns
    -------
    None.
    
    """
    pyversion = sys.version[0:1]
    if '3' in pyversion:
        total, used, free = shutil.disk_usage('/home')
    log.debug('Disk space check:')
    log.debug('total:'+str(total))
    log.debug('used:'+str(used))
    log.debug('free:'+str(free))
    if (free<limit):
        msg = '*** FATAL ERROR: not enough free disk space'
        log.error(msg)
        raise IOError(msg)
    log.debug('Disk ok')
    return



def name2time(name):
    """Transform a file name into an astropy.time.Time object.
    
    Parameters
    ----------
    name : string
        File name of type: yyyymmddThhmmss_sss
    
    Returns
    -------
    timeout : astropy.time.Time Object
        Time of the observation.
    
    """
    #log.debug('name: '+name)
    timestr = name[0:4]+'-'+name[4:6]+'-'+name[6:8]+'T'+name[9:11]+':'+name[11:13]+':'+name[13:15]
    if '.' in name:
        timestr = timestr + '.'+name[16:19]
    #log.debug('timestr: '+timestr)
    
    try:
        timeout = Time(timestr.replace(' ',''),format='isot')
    except ValueError:
        msg = '*** FATAL ERROR: timestr='+timestr+' is NOT isot format'
        log.error(msg)
        raise ValueError(msg)
    return timeout

def time2name(time,extn='.fit'):
    """Transform an astropy.time.Time Object into a file name.
    
    Parameters
    ----------
    t : astropy.time.Time Object
        Time of the observation.
    
    Returns
    -------
    name : string
        File name of type: yyyymmdd_hhmmss_sss.extn
    
    """
    timestr = time.isot
    name = timestr[0:4]+timestr[5:7]+timestr[8:10]+'_'+timestr[11:13]+timestr[14:16]+timestr[17:19]+'_'+timestr[20:23]
    return name+extn



###################################################
# 
# DEFINITION OF A FEW PHYSICS ROUTINES
# 
################################################### 

def compute_rhoatmo(P,T,method='SI'):
    """Compute air density in kg/m-3
    
    Parameters
    ----------
    P : float
        Pressure [Pa]
    T :  float
        Temperature [K]
    model : str, optional
        model to compute the density: choice between:
        'SI' => rhoatmo= P / R.T with R=287.05 [u.J/(u.K*u.kg)] for dry air.
        'Ce' => from Ceplecha 1987 [1]_:
        rhoatmo=(34.83676*(u.K/u.Pa)*self.datameteo[0].to('Pa')/self.datameteo[1].to('K'))
        #  rho=3.483676*P/T x1.0E-04 g/cm^3 with P in [hPa]
        => rho=3.483676*P/T x10 kg/m^3 with P in [Pa]
    
    Return
    ------
    rhoatmo : astropy.units.Quantity object
        air density [kg/m-3].
    
    Notes
    -----
    Details of the algorythm can be found in [1]_.
    
 .. [1] Ceplecha, 1987, Geometric, dynamic, orbital and photometric data on meteoroids
    from photographic fireball networks. Astronomical Institutes of Czechoslovakia,
    Bulletin (ISSN 0004-6248), vol. 38, July 1987, p. 222-234.
    http://cdsads.u-strasbg.fr/abs/1987BAICz..38..222C
    
    See Also
    --------
    https://wahiduddin.net/calc/density_altitude.htm
    https://en.wikipedia.org/wiki/Density_of_air
    
    """
    # note: specific gas constant for dry air=287.05 J/(kg.K)
    R_air = 287.05 *u.J/(u.K*u.kg)
    # D=P/RT
    rhoatmo_SI = (P.to('Pa')/(R_air.decompose() * T.to('K'))).to('kg/m3')
    
    if (method=='Ce'):
        #    rho=3.483676*P/T x1.0E-04 g/cm^3 with P in [hPa]
        # => rho=3.483676*P/T x10 kg/m^3 with P in [Pa]
        rhoatmo_Ce = (34.83676*(u.K/u.Pa)*P.to('Pa')/T.to('K')).to('kg/m3')
        rhoatmo = rhoatmo_Ce.to('kg/m3')
    else:
        #pthf.fri_log.info('P,T,d='+str([P.to('Pa'),T.to('K'),rhoatmo_SI.to('kg/m3')]))
        rhoatmo = rhoatmo_SI.to('kg/m3')
    return rhoatmo

def compute_sound_speed(P,rho):
    """Compute speed of sound
    
    Parameters
    ----------
    P : astropy.units.Quantity 
        Pressure, in [Pa]
    rho : astropy.punits.Quantity Object
        atmospheric density
    
    Returns
    -------
    c_sound : astropy.units.Quantity 
        speed of sound [m/s]
    
    """
    c_sound = np.sqrt(1.4 * P.decompose() / rho).to('m/s')
    return c_sound

def compute_Mach(V,P,rho):
    """Compute Mach number.
    
    Parameters
    ----------
    V : astropy.coordinates.Quantity Object
        velocity norm, in [m/s]
    P : astropy.units.Quantity 
        Pressure, in [Pa]
    rho : astropy.punits.Quantity Object
        atmospheric density
    
    Returns
    -------
    Mach : float
        Mach number - no dimension
    
    """
    c_sound = compute_sound_speed(P,rho)
    Mach = V.to('m/s') / c_sound.to('m/s')
    return Mach

def compute_c_M(V,P,rho):
    """Compute speed of sound and Mach number.
    
    Parameters
    ----------
    V : astropy.coordinates.Quantity Object
        velocity norm, in [m/s]
    P : astropy.units.Quantity 
        Pressure, in [Pa]
    rho : astropy.punits.Quantity Object
        atmospheric density
    
    Returns
    -------
    Mach : float
        Mach number - no dimension
    
    """
    c_sound = compute_sound_speed(P,rho)
    Mach = V.to('m/s') / c_sound.to('m/s')
    return (c_sound,Mach)

def distaz(eloc1,eloc2):
    """Compute distance and azimuth between 2 locations.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocation
        Location 1.
    eloc2 : astropy.coordinates.EarthLocation
        Location 2.
    
    Returns
    -------
    dist : astropy.units.Quantity
        Distance between eloc1 and eloc2 [m].
    az : astropy.units.Quantity
        azimuth between eloc1 and eloc2 [deg].
    
    """
    azdistrevaz = (great_distance(start_latitude=eloc1.lat.to('deg').value,start_longitude=eloc1.lon.to('deg').value,
                                 end_latitude=eloc2.lat.to('deg').value,  end_longitude=eloc2.lon.to('deg').value))
    dist = azdistrevaz['distance']*u.m
    azimuth = azdistrevaz['azimuth']*u.deg
    
    return (dist,azimuth)


def distsite(eloci,az,dist):
    """Compute new point from initial point, distance, and azimuth
    
    Parameters
    ----------
    eloci : astropy.coordinates.EarthLocation
        Locationof starting point.
    az_step : astropy.units Quantity object, optional
       Azimuth step to compute the ellipse. Default is 45.0*u.deg.
    dist : astropy.units Quantity object, optional
       Site distance. Default is 1000*u.km
    
    Returns
    -------
    eloco : astropy.coordinates.EarthLocation
        Locationof end pointm at sea level.
    
    """
    latlonrevaz = great_circle(distance=dist.to('m').value,
                                    azimuth=az.to('deg').value, 
                                    latitude=eloci.lat.to('deg').value,
                                    longitude=eloci.lon.to('deg').value)
    eloco = EarthLocation.from_geodetic(latlonrevaz['longitude']*u.deg,
                                        latlonrevaz['latitude']*u.deg,
                                        height=0.0*u.m)
    
    return eloco

###################################################
# 
# SPHERICAL TRIGONOMETRY
# 
################################################### 

def angular_distance(eloc1,eloc2):
    """Compute angular distance between 2 locations.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point.
    eloc2 : astropy.coordinates.EarthLocations
        Location of first point.
    
    Returns
    -------
    c : astropy.coordinate.Angle object.
        Angular distance between eloc1 and eloc2.
    
    See also
    --------
        https://www.johndcook.com/blog/spherical_trigonometry/
    """
    a = eloc2.lon.to('rad') - eloc1.lon.to('rad')
    b = eloc2.lat.to('rad') - eloc1.lat.to('rad')
    c = np.arccos(np.cos(a)*np.cos(b))
    return c

def elocs32area(eloc1,eloc2,eloc3,r=1.0):
    """Compute the area of a spherical triangle.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point.
    eloc2 : astropy.coordinates.EarthLocations
        Location of second point.
    eloc3 : astropy.coordinates.EarthLocations
        Location of third point.
    r : astropy.units.Quantity object, optional.
        Radius of the sphere. Default is 1.0 (without unit).
    
    Returns
    -------
    area : astropy.units.Quantity object.
        Area of spherical triangle.
    
    See also
    --------
        https://mathworld.wolfram.com/SphericalTriangle.html
        https://mathworld.wolfram.com/SphericalTriangle.html
    
    """
    # compute angular lengths of triangle, in [rad]
    a = angular_distance(eloc2,eloc3)
    b = angular_distance(eloc1,eloc3)
    c = angular_distance(eloc1,eloc2)
    # compute alpha, beta, gamma, in [rad]
    gamma = np.arccos( (np.cos(c)-np.cos(a)*np.cos(b) ) / (np.sin(a)*np.sin(b)) )
    beta  = np.arcsin(np.sin(b)*np.sin(gamma)/np.sin(c))
    alpha = np.arcsin(np.sin(a)*np.sin(gamma)/np.sin(c))
    # compute spherical excess in [rad]
    E = (alpha + beta + gamma) - np.pi*u.rad
    # compute area in unit of r
    area = r**2 * E.value
#    if np.isnan(area):
#        log.info('eloc1: '+str(eloc1.to_geodetic()))
#        log.info('eloc2: '+str(eloc2.to_geodetic()))
#        log.info('eloc3: '+str(eloc3.to_geodetic()))
#        log.info('a.to(deg): '+str(a.to('deg')))
#        log.info('b.to(deg): '+str(b.to('deg')))
#        log.info('c.to(deg): '+str(c.to('deg')))
#        log.info('a_dist: '+str(a.to('rad')*r))
#        log.info('b_dist: '+str(b.to('rad')*r))
#        log.info('c_dist: '+str(c.to('rad')*r))
#        log.info('alpha: '+str(alpha.to('deg')))
#        log.info('beta : '+str(beta.to('deg')))
#        log.info('gamma: '+str(gamma.to('deg')))
#        log.info('alpha + beta + gamma: '+str((alpha + beta + gamma).to('deg')))
#        log.info('E: '+str(E))
#        log.info('area: '+str(area))
#        raise ValueError()
    return area

def swith_elocs(eloc1,eloc2,eloc3):
    """Switch elocs.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point.
    eloc2 : astropy.coordinates.EarthLocations
        Location of second point.
    eloc3 : astropy.coordinates.EarthLocations
        Location of third point.
    
    Returns
    -------
    eloc2 : astropy.coordinates.EarthLocations
        Location of second point.
    eloc3 : astropy.coordinates.EarthLocations
        Location of third point.
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point.
    
    """
    return (eloc2,eloc3,eloc1)

def elocs42area(eloc1,eloc2,eloc3,eloc4,r=1.0):
    """Compute the area of a spherical rectangle.
    
    elocs MUST be sorted clock wise.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point.
    eloc2 : astropy.coordinates.EarthLocations
        Location of second point.
    eloc3 : astropy.coordinates.EarthLocations
        Location of third point.
    eloc3 : astropy.coordinates.EarthLocations
        Location of fourth point.
    r : astropy.units.Quantity object, optional.
        Radius of the sphere. Default is 1.0 (without unit).
    
    Returns
    -------
    area : astropy.units.Quantity object, optional.
        Area of spherical triangle.
    
    """
    # compute area in unit of r
    for nswitch in range(3):
        area1 = elocs32area(eloc3,eloc1,eloc2,r=r)
        if area1.value<0.0 or np.isnan(area1.value):
            #log.warning('*** switching elocs123')
            (eloc1,eloc2,eloc3) = swith_elocs(eloc1,eloc2,eloc3)
        else:
            #log.debug('ok area1: '+str(area1))
            break
    for nswitch in range(3):
        area2 = elocs32area(eloc1,eloc3,eloc4,r=r)
        if area2.value<0.0 or np.isnan(area2.value):
            #log.warning('*** switching elocs134')
            (eloc1,eloc3,eloc4) = swith_elocs(eloc1,eloc3,eloc4)
        else:
            #log.debug('ok area2: '+str(area2))
            break
    area = area1 + area2
    if np.isnan(area):
        raise ValueError('area is nan')
    return area

def elocs2mid(eloc1,eloc2):
    """Compute the middle point of a spherical rectangle.
    
    eloc1 and eloc2 MUST have the same altitude.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point.
    eloc2 : astropy.coordinates.EarthLocations
        Location of second point.
    
    Returns
    -------
    elocmid : astropy.coordinates.EarthLocations
        Location of middle point.
    
    """
    # compute distance and azimuth from eloc1 to eloc3
    (dist,az) = distaz(eloc1,eloc2)
    # compute mid-point
#    print(dist)
#    print(az)
#    print(eloc1.lat.to('deg').value)
#    print(eloc1.lon.to('deg').value)
    elocmid_dict = great_circle(distance=dist.to('m').value, azimuth=az.to('deg').value,
                                latitude=eloc1.lat.to('deg').value,
                                longitude=eloc1.lon.to('deg').value)
    elocmid = EarthLocation.from_geodetic(elocmid_dict['longitude']*u.deg,
                                          elocmid_dict['latitude']*u.deg,
                                          height=eloc1.height)
    return elocmid


def arc_intersection(eloc1,eloc2,eloc3,eloc4):
    """Computes the intersection of 2 arcs that lie on a sphere.
    
    Parameters
    ----------
    eloc1 : astropy.coordinates.EarthLocations
        Location of first point of first arc.
    eloc2 : astropy.coordinates.EarthLocations
        Location of second point of first arc.
    eloc3 : astropy.coordinates.EarthLocations
        Location of first point of second arc.
    eloc4 : astropy.coordinates.EarthLocations
        Location of second point of second arc.
    
    Returns
    -------
    eloco : astropy.coordinates.EarthLocations
        Location of intersection point.
    
    See also
    --------
        https://blog.mbedded.ninja/mathematics/geometry/spherical-geometry/finding-the-intersection-of-two-arcs-that-lie-on-a-sphere/
    """
    # initialize output
    eloco = None
    # make cartesian vectors
    Pa11 = np.array([eloc1.x.to('km').value,eloc1.y.to('km').value,eloc1.z.to('km').value])
    Pa12 = np.array([eloc2.x.to('km').value,eloc2.y.to('km').value,eloc2.z.to('km').value])
    Pa21 = np.array([eloc3.x.to('km').value,eloc3.y.to('km').value,eloc3.z.to('km').value])
    Pa22 = np.array([eloc4.x.to('km').value,eloc4.y.to('km').value,eloc4.z.to('km').value])
    # compute cross product
    N1 = np.cross(Pa11,Pa12)
    N2 = np.cross(Pa21,Pa22)
    # compute cross product of cross product
    L = np.cross(N1,N2)
    # compute the 2 possible points of intersection
    I1 = L / np.linalg.norm(L)
    I2 = -I1
    LI1 = check_intersect(Pa11,Pa12,I1)
    LI2 = check_intersect(Pa11,Pa12,I2)
    if LI1:
        eloco = LI1
    if LI2:
        eloco = LI2
    # check if a result was found
    if not eloco:
        msg = '*** FATAL ERROR: no point of intersection was found'
        log.error(msg)
        raise ValueError(msg)
    return eloco

def check_intersect(arc_srt,arc_end,I,epsilon=1.0E-06):
    """Check if a point I is within arc defined by an arc.
    
    Parameters
    ----------
    arc_srt : 3-float vector
        Arc starting point cartesian coordinates.
    arc_end : 3-float vector
        Arc ending point cartesian coordinates.
    I : 3-float vector
        Intersection point cartesian coordinates.
    epsilon : float
        Tolerance below which the intersection is considered false.
        Default is 1.0E-06
    
    See also
    --------
        https://blog.mbedded.ninja/mathematics/geometry/spherical-geometry/finding-the-intersection-of-two-arcs-that-lie-on-a-sphere/
    """
    # initializes output
    Lintersect = False
    # compute angles between components:
    theta_srt_i = np.arccos( arc_srt * I / (np.linalg.norm(arc_srt) * np.linalg.norm(I)) )
    theta_end_i = np.arccos( arc_end * I / (np.linalg.norm(arc_end) * np.linalg.norm(I)) )
    theta_srt_e = np.arccos( arc_srt * arc_end / (np.linalg.norm(arc_srt) * np.linalg.norm(arc_end)) )
    # compute difference of theta, in deg to make it 'large'
    sumtheta = np.abs(theta_srt_i + theta_end_i - theta_srt_e) * 180.0 / np.pi
    # check sum
    if (sumtheta.to('deg').value > epsilon):
        Lintersect = True
    return Lintersect

def latrad(lat,planet='EARTH'):
    """Compute planet radius at given latitude.
    
    Parameters
    ----------
    lat : float
        Latitude, in radians.
    planet : string, optional
        Planet name. Default is 'EARTH'.
    
    Returns
    -------
    r_pla : astropy.units.Quantity object
        Planet radius at latitude lat.
    
    """
    # retrieve planet radii
    pla_radii = sp.bodvrd( planet, 'RADII', 3)[1]*u.km
    # store equatorial and polar radii
    [r_eq,r_po] = [pla_radii[0],pla_radii[-1]]
    # compute planet radius at latitude lat
    R_pla = np.sqrt( (  (r_eq**2*np.cos(lat))**2 + (r_po**2*np.sin(lat))**2  ) / 
                     (  (r_eq   *np.cos(lat))**2 + (r_po   *np.sin(lat))**2  ))
    return R_pla














