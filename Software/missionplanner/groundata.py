#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 11:33:25 2019

@author: jvaubaillon
"""

import os
import astropy.units as u
import numpy as np
from scipy.spatial import Delaunay
import scipy.interpolate as interpolate

from missionplanner.exceptions import NoGroundDataError


class GroundData (object):
    """GroundData object
    
    Parameters
    ----------
    ign_all_lat : astropy.coordinates.Angle object
        array of latitudes
    ign_all_lon : astropy.coordinates.Angle object
        array of longitudes
    ign_all_alt : astropy.units.Quantity object
        array of altitudes
    
    """
    
    ign_all_lat = []
    ign_all_lon = []
    ign_all_alt = []
    
    
    def __init__(self,log):
        """Instantiates a GroundData object.
        
        Parameters
        ----------
        log : logging.Logger object
            Logger.
        
        """
        self.log=log
        #self.log.setLevel(logging.DEBUG)
        self.log.debug('GroundData Object successfully created.')
        return
    
    def get_neighbour_files(self,input_longitude,input_latitude):
        """Get the names of the files where data are seeked as well as neighbour files.
        
        Parameters
        ----------
        input_longitude : astropy.units.Quantity object
            longitude where data are to be extracted
        input_latitude : astropy.units.Quantity object
            latitude where data are to be extracted
        
        Returns
        -------
        names : list of strings
            names of file names to load
        """
        # convert lon/lat into int, then str
        lon_int = int(input_longitude.to('deg').value)
        lat_int = int(input_latitude.to('deg').value)
        all_lat_str = (np.array([lat_int-1,lat_int,lat_int+1])).astype('str')
        all_lon_str = (np.array([lon_int-1,lon_int,lon_int+1])).astype('str')
        # add '-0' in all_lon_str if needed
        if '0' in all_lon_str:
            all_lon_str = np.insert(all_lon_str,np.where(all_lon_str=='0')[0],['-0'])
            self.log.warning('Addin -0 to list of longitudes')
        self.log.debug('all_lat_str: '+str(all_lat_str))
        self.log.debug('all_lon_str: '+str(all_lon_str))
        
        # now builds the list of files to load
        file_list = []
        for lat_str in all_lat_str:
            path = self.path_mnt+ 'MNT'+lat_str
            for lon_str in all_lon_str:
                filename = path+'/mnt'+lat_str+'_'+lon_str
                file_list.append(filename)
                self.log.debug('Appending file: '+filename)
        
        self.log.debug('file_list: '+str(file_list))
        return file_list
    
    def load_file(self,filename,ign_data=None):
        """Load data in a file.
        
        Parameters
        ----------
        filename : string
            Full path file name to load.
        ign_data : numpy.array of data, optional
            if present, file data are appended to ign_data.
        
        Returns
        -------
        ign_data : numpy.array of data, optional
            data in filename (might be appended to already existing data).
        
        """
        self.log.info('reading: '+filename)
        self.loaded.append(filename)
        ign_file_data = np.transpose(np.loadtxt(filename))
        if ign_data is None:
            ign_data = ign_file_data
        else:
            ign_data = np.concatenate((ign_data,ign_file_data),axis=1)
        return ign_data
    
    def extract_ign_data(self,path_mnt,input_longitude,input_latitude):
        """extract data from IGN data files.
        
        Parameters
        ----------
        path_mnt : string
            name of path where the IGN files are located
        input_longitude : astropy.units.Quantity object
            longitude where data are to be extracted
        input_latitude : astropy.units.Quantity object
            latitude where data are to be extracted
        
        Returns
        -------
        None
            GroundData object data are initialized:
            ign_all_lat : all latitudes astropy.coordinates.Angle object [deg]
            ign_all_lon : all longitude astropy.coordinates.Angle object [deg]
            ign_all_alt : all heights astropy.units.Quantity [m]
        """
        self.log.info( 'input lon/lat='+str(input_longitude)+' ('+str(input_longitude.to('deg').value)+' deg) '+
                                        str(input_latitude)+' ('+str(input_latitude.to('deg').value)+' deg) ')
        # save the MNT path
        self.path_mnt = path_mnt
        
        # double check longitude/latitude range
        self.lat_range=np.arange(41,52) # [deg]
        self.lon_range=np.arange(-9,10) # [deg]
        
        if ((input_longitude.to('deg').value < np.min(self.lon_range)) or (input_longitude.to('deg').value > np.max(self.lon_range)) or (input_latitude.to('deg').value < np.min(self.lat_range)) or (input_latitude.to('deg').value > np.max(self.lat_range))):
            msg = "*** FATAL ERROR:  input longitude and latitude ("+str(input_longitude)+" "+str(input_latitude)+") outside IGN range: ["+str(np.min(self.lon_range))+";"+str(np.max(self.lon_range))+"] ["+str(np.min(self.lat_range))+";"+str(np.max(self.lat_range))+"]"
            self.log.error(msg)
            raise NoGroundDataError(msg)
        self.log.debug('lon/lat range check ok')
        
        self.file_list = self.get_neighbour_files(input_longitude,input_latitude)
        self.log.info('Files to load: '+str(self.file_list))
        
        self.loaded=[]
        ign_data = None
        for file_name in self.file_list:
            if file_name in self.loaded:
                self.log.debug(file_name+' already loaded: no need to load again.')
                continue
            if not os.path.isfile(file_name) :
                self.log.warning('***IGN '+file_name+' does not exist')
                continue
            filesz = os.path.getsize(file_name)
            if filesz==0:
                self.log.warning('IGN file '+file_name+' is empty and ignored')
                continue
            ign_data = self.load_file(file_name,ign_data=ign_data)
            self.loaded.append(file_name)
            self.log.debug('size(ign_data): '+str(len(ign_data))+' '+str(len(ign_data[0])))
        
        # sort the array in ascending order of latitude
        all_lat = np.asarray(ign_data[0,:])
        all_lon = np.asarray(ign_data[1,:])
        all_alt = np.asarray(ign_data[2,:])
        sortlat = np.argsort(all_lat)
        # set global variables
        self.ign_all_lat = all_lat[sortlat] * u.deg
        self.ign_all_lon = all_lon[sortlat] * u.deg
        self.ign_all_alt = all_alt[sortlat] * u.m
        # set boundaries
        self.max_lat = np.max(self.ign_all_lat)
        self.max_lon = np.max(self.ign_all_lon)
        self.min_lat = np.min(self.ign_all_lat)
        self.min_lon = np.min(self.ign_all_lon)
        
        return
    
    def interpol_ign_data(self,input_longitude,input_latitude):
        """Interpolate IGN data.
        
        Parameters
        ----------
        input_longitude : astropy.units.Quantity object
            longitude at which the IGN data are to be interpolated
        input_latitude : astropy.units.Quantity object
            latitude at which the IGN data are to be interpolated
        
        Returns
        -------
        ground_alt : astropy.units.Quantity object
            height of ground above sea level [m]
        
        """
        # check boundaries: if needed, re-construct the whole data set.
        if (input_longitude < self.min_lon) or (input_longitude > self.max_lon) or (input_latitude < self.min_lat) or (input_latitude > self.max_lat):
            self.extract_ign_data(self.path_mnt,input_longitude,input_latitude)
        # get index of data of interest
        index           = (np.abs(self.ign_all_lat.to('deg').value-input_latitude.to('deg').value)).argmin()
        nindices        = 1250//2  # interpolation will be performed within 1.25 deg in lat and lon from the desired point, in each direction
        ign_all_lat1    = self.ign_all_lat[(index-nindices):(index+nindices)]
        ign_all_lon1    = self.ign_all_lon[(index-nindices):(index+nindices)]
        ign_all_alt1    = self.ign_all_alt[(index-nindices):(index+nindices)]
        self.log.debug('min/max lon1: '+str(np.min(ign_all_lat1))+' '+str(np.max(ign_all_lat1)))
        self.log.debug('min/max lat1: '+str(np.min(ign_all_lon1))+' '+str(np.max(ign_all_lon1)))
        self.log.debug('min/max alt1: '+str(np.min(ign_all_alt1))+' '+str(np.max(ign_all_alt1)))
        delaunay_array  = np.transpose([ign_all_lat1.value,ign_all_lon1.value])
        trigrid         = Delaunay(delaunay_array)
        ground_int      = interpolate.LinearNDInterpolator(trigrid.points,ign_all_alt1.value)
        ground_alt      = ground_int(((input_latitude.to('deg').value,input_longitude.to('deg').value))) * u.m
        self.log.debug('ground_alt: '+str(ground_alt))
        if (np.isnan(ground_alt.value)):
            msg = '*** NAN DETECTED!!!'
            self.log.error(msg)
            #raise ValueError(msg)
        return ground_alt



