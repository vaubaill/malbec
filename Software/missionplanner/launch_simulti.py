#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 15:11:09 2019

@author: D. Zilkova, J. Vaubaillon, IMCCE

MALBEC project, IMCCE, KYU-ACE, ScotopicLab, CNES
"""
import os
import time
from configparser import ConfigParser, ExtendedInterpolation
from astropy.coordinates import EarthLocation
from astropy.table import QTable
from astropy.time import Time
import astropy.units as u
import numpy as np
import multiprocessing
import argparse

from missionplanner import create_config,missionplan,landzone,utils
from missionplanner.utils import log


def launch_simulti(confgen_dir='./conf/conf_gen/',template_file_name='template.in',Hmin=25000.0*u.m,Hmax=40000.0*u.m,Hstep=5000.0*u.m,Vmin=4.5*u.m/u.s,Vmax=7.0*u.m/u.s,Vstep=0.5*u.m/u.s,kml_file_name='Landingzone.kml'):
    """Launch a multitude of simulations and creates a LandZone object.
    
    The simulations are performed for a range of explosion altitude and
    ascending velocity boundaries. The step for each quantity is tunable.
    This is useful before the flight when assessing the probable landing zone
    and help decide on a go/no-go.
    
    Parameters
    ----------
    confgen_dir : string
        Configuration file directory where all created configuration files will be put.
        It must contain a template_file file.
    template_file : string
        Configuration file template file name. It must be located in the confgen_dir directory.
    Hmin : astropy.units.Quantity, optional
        Minimum ballon explosion altitude. Default is 25000 m.
    Hmax : astropy.units.Quantity, optional
        Minimum ballon explosion altitude. Default is 40000 m.
    Hstep :  : astropy.units.Quantity, optional
        Ballon explosion altitude step. Default is 5000 m.
    Vmin : astropy.units.Quantity, optional
        Minimum ascending velocity. Default is 5 m/s.
    Vmax : astropy.units.Quantity, optional
        Minimum ascending velocity. Default is 7 m/s.
    Vstep :  : astropy.units.Quantity, optional
        Ascending velocity step. Default is 0.5 m/s.
    kml_file_name : stringm optional
        Output trajectory KML file name. The full path is defined in the configuration file.
        Default is 'Landingzone.kml'
    
    Returns
    -------
    lndzn : landzone.LandZone Object.
        Area where the balloon is expected to fall.
    
    """
    # read configuration file
    template = ConfigParser(interpolation=ExtendedInterpolation())
    template_file = confgen_dir + template_file_name
    log.debug('Now reading template configuration file: '+template_file)
    if not os.path.exists(template_file):
        msg = '*** FATAL ERROR: config file '+template_file+' does not exist'
        log.error(msg)
        raise IOError(msg)
    template.read(template_file)
    # set HOME variable
    template['USER']['home'] = os.getenv('HOME')
    # creates landing zone output directory name
    up_outdir = '/'.join(template['CAMPAIGN']['out_dir'].split('/')[:-2])+'/'
    log.info('Data will be saved in: '+up_outdir)
    # creates a landzone object
    lndzn = landzone.LandZone(outdir=up_outdir)
    # set output kml file
    kml_file = up_outdir+kml_file_name
    
    # build H & V range
    n_H = int((Hmax.to('m').value  -Hmin.to('m').value)   / Hstep.to('m').value  ) + 1
    n_V = int((Vmax.to('m/s').value-Vmin.to('m/s').value) / Vstep.to('m/s').value) + 1
    Hlist = np.linspace(Hmin.to('m').value  ,Hmax.to('m').value  ,n_H,endpoint=True)*u.m
    Vlist = np.linspace(Vmin.to('m/s').value,Vmax.to('m/s').value,n_V,endpoint=True)*u.m/u.s
    # control
    log.debug('Hlist: '+str(Hlist))
    log.debug('Vlist: '+str(Vlist))
    
    # list of all config file
    list_conf = []
    list_VHname = []
    # loop over H & V
    for V in Vlist:
        for H in Hlist:
            # create config file from model file
            (config_file,VHname) = create_config.create_config(template_file,H,V,confgen_dir)
            log.info(config_file+' successfully created.')
            list_conf.append(config_file)
            list_VHname.append(VHname)
    log.info('list_conf : '+str(list_conf))
    log.info('list_VHname : '+str(list_VHname))
    
    # get total number of processors
    num_cores = multiprocessing.cpu_count()
    # greate Pool
    pool = multiprocessing.Pool(num_cores)
    # launches missionplan in parallel.
    # note: if the simulation already exists, the trajectory is read to get the landing point.
    pool.map(run_missionplan,zip(list_conf,list_VHname))
    
    # now loads trajectories to make landing zone
    for (conf,VHname) in zip(list_conf,list_VHname):
        log.info('Load simulation: '+VHname)
        confprs = ConfigParser(interpolation=ExtendedInterpolation())
        confprs.read(conf)
        confprs['USER']['home'] = os.getenv('HOME')
        traj_file = confprs['CAMPAIGN']['trajectory_sim_file']
        traj_data = QTable().read(traj_file,format='ascii.fixed_width_two_line')
        land_eloc = EarthLocation.from_geodetic(lat=traj_data['Latitude'][-1]*u.deg,
                                                lon=traj_data['Longitude'][-1]*u.deg,
                                                height=traj_data['Altitude'][-1]*u.m)
        land_t = Time(traj_data['Time'][-1], format = 'isot')
        # put landing point into lndzn
        lndzn.add_data(land_eloc,land_t,name=VHname)
    
    # save all possible landing points into a kml file
    lndzn.to_kml(kml_file)
    log.info('Landing zone saved in '+kml_file)
    log.info('Done.')
    
    return lndzn


def run_missionplan(conf_VH):
    """Run an instance of MissionPlanner.
    
    Parameters
    ----------
    config_file : string
        Configuration file name.
    
    Returns
    -------
    None.
    
    
    """
    config_file = conf_VH[0]
    #VHname = conf_VH[1]
    log.info('======================================================')
    log.info('Lauch MissionPlanne for config file: '+config_file)
    log.info('======================================================')
    # create MissionPlanner object
    missplan = missionplan.MissionPlanner(config_file)
    # try to load trajectory and if not run the simulation
    try:
        missplan.load_traj(missplan.trajectory_sim_file)
        log.debug('Simulation: loaded from config: '+config_file)
    except IOError:
        # launch simulation
        log.info('LAUNCH simulation for config: '+config_file)
        missplan.launch()
        log.info('Simulation done for config: '+config_file)
    
    return

if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(description='launch_simulti arguments.')
    parser.add_argument('-conftemp',default='./conf/config_simulti.in',help='launch_simulti configuration file. Default is: ./conf/config_simulti.in')
    args = parser.parse_args()
    
    # set configuration template file.
    cnfprs_file = args.conftemp
    # make sure configuration template file exists.
    utils.check_file(cnfprs_file,log)
    
    # create ConfigParser Object.
    cnfprs = ConfigParser(interpolation=ExtendedInterpolation())
    log.debug('Now reading template configuration file: '+cnfprs_file)
    # red configuration file
    cnfprs.read(cnfprs_file)
    # get the time for performance info
    launch_time = time.time()
    
    # launch simulti with arguments
    launch_simulti(confgen_dir=cnfprs['USER']['confgen_dir'],
                   template_file_name=cnfprs['USER']['template_file_name'],
                   Hmin=float(cnfprs['SIM']['Hmin'])*u.m,
                   Hmax=float(cnfprs['SIM']['Hmax'])*u.m,
                   Hstep=float(cnfprs['SIM']['Hstep'])*u.m,
                   Vmin=float(cnfprs['SIM']['Vmin'])*u.m/u.s,
                   Vmax=float(cnfprs['SIM']['Vmax'])*u.m/u.s,
                   Vstep=float(cnfprs['SIM']['Vstep'])*u.m/u.s,
                   kml_file_name=cnfprs['OUT']['kml_file_name'])
    # log the duration of the whole calculation
    log.info('All the Simulation took '+str(time.time()-launch_time)+' to run.')
else:
    log.debug('successfully imported')
