"""MALBEC Landing Zone object.

Author
------
J. Vaubaillon, IMCCE, 2019

"""
import simplekml

class LandZone (object):
    """LandZone object.
    
    A LandZone object contains all the location of possible fall of the payload.
    
    Parameters
    ----------
    outdir : string
        Output directory name.
    npts : int
        number of landing points.
    elocs : list of astropy.coordinates.EarthLocation object
        location of landing points.
    times : list of astropy.time.Time object
        Time of landing.
    names : string, optional
        Name of landing point. Default is empty string.
    
    
    """
    def __init__(self,outdir='./'):
        # output directory
        self.outdir = outdir
        # number of points in the LandZone object
        self.npts = 0
        # array of astropy.coordinates.EarthLocation object
        self.elocs = []
        # array of Time objects
        self.times = []
        # array of names of each location of the LandZone object.
        self.names = []
        return
    
    def add_data(self,eloc,t,name=''):
        """
        Add a point in the strewn field
        
        Parameters
        ----------
        eloc : astropy.coordinates.EarthLocation object
            Landing location of one clone.
        time: astropy.time.Time object
            Time of landing
        
        Returns
        -------
        None:
            the location eloc and time t are added in the LandZone object
        """
        self.elocs.append(eloc)
        self.times.append(t)
        self.names.append(name)
        self.npts = self.npts+1
        return
    
    def to_kml(self,kmlfile):
        """Save the LandZone into a kml file.
        
        Parameters
        ----------
        kmlfile : string
            KML file full name.
        
        Returns
        -------
        None.
        
        """
        # convert into kml data and save into output file
        kml = simplekml.Kml()
        coords = []
        for (t,eloc,name) in zip(self.times,self.elocs,self.names):
          (lon,lat,alt)=(eloc.lon.to('deg').value,eloc.lat.to('deg').value,eloc.height.to('m').value)
          coords.append((lon,lat,alt))
          pnt = kml.newpoint(name=name,coords=[(lon,lat,alt)],extrude=1)
          pnt.altitudemode = simplekml.AltitudeMode.absolute 
          pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_square_highlight.png'
          pnt.timestamp.when=t.isot
        kml.save(kmlfile)
        return


