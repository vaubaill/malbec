import tkinter as tk
from tkinter import filedialog
import os
import zipfile
import xml.etree.ElementTree as ET

def process_kmz_file():
    # Open file dialog to select KMZ file
    kmz_file_path = filedialog.askopenfilename(title="Select a KMZ file", filetypes=[("KMZ files", "*.kmz")])
    
    # Check if file is selected
    if kmz_file_path:
        # Extract KML file from KMZ
        with zipfile.ZipFile(kmz_file_path, 'r') as zip_ref:
            kml_file = [file for file in zip_ref.namelist() if file.endswith('.kml')][0]
            zip_ref.extract(kml_file)
        
        # Parse KML file to extract coordinates
        tree = ET.parse(kml_file)
        root = tree.getroot()
        
        # Extract coordinates (longitude, latitude, altitude)
        for placemark in root.iter('{http://www.opengis.net/kml/2.2}Placemark'):
            name = placemark.find('{http://www.opengis.net/kml/2.2}name').text
            coordinates = placemark.find('{http://www.opengis.net/kml/2.2}Point').find('{http://www.opengis.net/kml/2.2}coordinates').text
            longitude, latitude, altitude = map(float, coordinates.split(','))
            
            # Create output file name with .in extension
            output_file_path = os.path.splitext(kmz_file_path)[0] + ".in"
            
            # Write coordinates to output file
            with open(output_file_path, 'a') as f:
                f.write(f"Name: {name}\n")
                f.write(f"Longitude: {longitude}\n")
                f.write(f"Latitude: {latitude}\n")
                f.write(f"Altitude: {altitude}\n\n")
        
        # Remove temporary KML file
        os.remove(kml_file)
        
        # Inform user about successful operation
        status_label.config(text=f"Coordinates saved to {output_file_path}")

# Create Tkinter window
root = tk.Tk()
root.title("KMZ File Processor")

# Create a button to trigger file selection
select_file_button = tk.Button(root, text="Select KMZ File", command=process_kmz_file)
select_file_button.pack(pady=10)

# Label to display status
status_label = tk.Label(root, text="")
status_label.pack()

# Run the Tkinter event loop
root.mainloop()
