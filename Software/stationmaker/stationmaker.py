"""Station Maker

Author
------
J. Vaubaillon - IMCCE, 2021

Version
-------
1.0

"""
import os
#import re
import shutil
import argparse
import logging
from configparser import ConfigParser, ExtendedInterpolation
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation

from missionplanner import utils
from missionplanner.utils import log,add_FileHandler

# create default logger object
# log = logging.getLogger(__name__)

class StationMaker (object):
    """Station Maker for fixed ground based observations.
    
    """
    log_set = False
    
    def __init__(self, config_file='config.in'):
        """Instantiates StationMaker object.
        
        Parameters
        ----------
        config_file : string, optional.
            Configuration file. Default is 'config.in'.
        
        Returns
        -------
        None.
        
        """
        # creates configparser.ConfigParser() object for the configuration file
        self.config = ConfigParser(interpolation=ExtendedInterpolation())
        # store config file name
        self.config_file = config_file
        # make log and test if config file exists
        add_FileHandler(self.config_file)
        # read configuration file
        self.config.read(self.config_file)
        
        # set the home variable
        self.config['USER']['home'] = os.getenv('HOME')
        
        # set logger object BEFORE checking the configuration file so log messages can be output.
        logdict = {'DEBUG' : logging.DEBUG,
                   'INFO' : logging.INFO,
                   'WARNING' : logging.WARNING,
                   'ERROR' : logging.ERROR}
        try:
            log_level = self.config['USER']['log_level']
            self.set_log(level=logdict[log_level])
        except:
            self.set_log(level=logging.DEBUG)   
        
        # check configuration data
        self.check_config()
        
        return
      
    
    
    def set_log(self,level=logging.INFO,nosdlr=False,nohdlr=False):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG
        nosdlr : boolean, optional
            If True, no stream handler is created. Default is False.
        nohdlr  : boolean, optional
            If True, no file handler is created. Default is False.
        
        Returns
        -------
        None.
        
        
        """
        # set log level
        log.setLevel(level)
        
        # create FileHandler
        #strt = Time.now().isot.replace('-','').replace(':','').split('.')[0]
        #self.log_file = self.config['CAMPAIGN']['log_file_root'].replace('YYYYMMDDTHHMMSS',strt)
        #utils.change_logfile(log,log.file_handler.baseFilename,self.log_file)
            
        # if LOG directory does not exist make one
        if not os.path.isdir(self.config['USER']['log_dir']):
            os.makedirs(self.config['USER']['log_dir'])
        
        # set the log_set parameter
        self.log_set = True
        
        # log the log file name
        #log.info('Log file: '+self.log_file)
        
        return
    
    def check_config(self):
        """Check and reads the configuration file.
        
        Make sure all necessary parameters are present.
        Raise ValueError if a parameters is missing.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        # check the presence of necessary paths names in config file
        listkeys = ['home','proj_dir','soft_dir','conf_dir','data_dir','log_dir','kernel_path','spice_exe_path']
        for key in listkeys:
            log.debug('now checking presence of : '+key+' in '+self.config_file)
            try:
                tmp = self.config['USER'][key]
            except:
                msg = '*** FATAL ERROR: '+key+' is missing from USER section of configuration file '+self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # check existence of directories
        listkey_dirtocheck = ['home','proj_dir','soft_dir','conf_dir','data_dir','log_dir','kernel_path','spice_exe_path']
        for key in listkey_dirtocheck:
            log.debug('now checking existence of : '+self.config['USER'][key])
            if not os.path.exists(self.config['USER'][key]):
                msg = '*** FATAL ERROR: directory '+self.config['USER'][key]+' does not exists'
                log.error(msg)
                raise IOError(msg)
        
        # save SPICE paths into global variable
        self.kernel_path = self.config['USER']['kernel_path']
        self.spice_exe_path = self.config['USER']['spice_exe_path']
        
        
        # check the presence of necessary keys in CAMPAIGN config file
        listkeys = ['name','out_dir','sim_dir_name','sites_file','site_id','nacelle_name','nacelle_id','trajectory_sim_file','initcond_time','endicond_time','freq']
        for key in listkeys:
            log.debug('now checking presence of : '+key+' in '+self.config_file)
            try:
                tmp = self.config['CAMPAIGN'][key]
                log.debug('key: '+tmp)
            except:
                msg = '*** FATAL ERROR: '+key+' is missing from CAMPAIGN section of configuration file '+self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        
        # make directories if necessary
        listkey_dirtocheck = ['out_dir']
        for key in listkey_dirtocheck:
            log.debug('now checking existence of directory: '+self.config['CAMPAIGN'][key])
            if not os.path.isdir(self.config['CAMPAIGN'][key]):
                msg = 'Warning: directory '+self.config['CAMPAIGN'][key]+' does not exists -> making one now.'
                log.warning(msg)
                os.makedirs(self.config['CAMPAIGN'][key])
        
        # copy configuration file into output directory
        src = self.config_file
        dst = self.config['CAMPAIGN']['out_dir']+'/'+os.path.basename(src)
        try:
            shutil.copyfile(src,dst)
        except shutil.SameFileError:
            log.warning('Config files are the same -> not copied')
        except:
            msg = '*** FATAL ERROR: unable to copy '+src+' into '+dst
            log.error(msg)
            raise IOError(msg)
        log.info('File :'+src+' has been copied to '+dst)
        
        # read station sites file
        self.sites_file = self.config['CAMPAIGN']['sites_file']
        utils.check_file(self.sites_file,log)
        log.info('Read station sites: '+self.sites_file)
        sitesconf = ConfigParser()
        sitesconf.read(self.sites_file)
        # list of keys to input into landing site dictionary
        list_keys = ['id', 'lon', 'lat', 'alt', 'type', 'name', 'tel']
        # initializations
        nsites = 0
        site_found = False
        # read station sites
        for nsites in range(100):
            site_id = nsites + 1
            param = 'SITE' + str(nsites + 1)
            log.debug('checking site: ' + param)
            try:
                for key in list_keys:
                    testkey = sitesconf[param][key]
                    log.debug('checking '+param+'['+key+'] = '+testkey)
            except:
                log.warning('*** Impossible to find ' + param + ' in ' + 'sites_file')
                break
            # store station info if applicable
            if (site_id==int(self.config['CAMPAIGN']['site_id'])):
                log.info('Station site id '+str(site_id)+' found!')
                site_found = True
                self.eloc = EarthLocation.from_geodetic(lon=float(sitesconf[param]['lon'])*u.deg,
                                        lat=float(sitesconf[param]['lat'])*u.deg,
                                        height=float(sitesconf[param]['alt'])*u.m)

        if not nsites:
            msg = '*** FATAL ERROR: there is no possible landing site in ' + self.sites_file
            log.error(msg)
            raise ValueError(msg)
        log.info('There are ' + str(nsites) + ' possible landing sites recorded in the sites_file')
        
        if not site_found:
            msg = '*** FATAL ERROR: station site '+self.config['CAMPAIGN']['site_id']+' not found in ' + self.sites_file
            log.error(msg)
            raise ValueError(msg)
            
        
        # now store configuration data into StationMaker object data
        self.out_dir = self.config['CAMPAIGN']['out_dir']+'/'
        self.nacelle_id = int(self.config['CAMPAIGN']['nacelle_id'])
        self.nacelle_name = self.config['CAMPAIGN']['nacelle_name']
        self.spk_file = self.config['CAMPAIGN']['trajectory_sim_file'].replace('.dat','.spk')
        self.timei = Time(self.config['CAMPAIGN']['initcond_time'],scale='utc')
        self.timef = Time(self.config['CAMPAIGN']['endicond_time'],scale='utc')
        self.freq = float(self.config['CAMPAIGN']['freq'])*u.min
        
        """
        # double check that the nacelle_name includes a number, for obsplanner compatibility
        try:
            nacelle_number = re.findall(r'\d+',self.nacelle_name)[0]
            log.debug('nacelle_number: '+nacelle_number)
        except:
            msg = '*** FATAL ERROR: nacelle_name MUST include a number, for obsplanner compatibility'
            log.error(msg)
            raise ValueError(msg)
        """
        
        return
    
        
    def make(self,tmpdir='/tmp/'):
        """Convert trajectory into SPICE spk file.
        
        Parameters
        ----------
        tmpdir : string, optional
            Temporary directory where files are created. This is useful if the path to the files
            are very long, since it causes the mkspk utility program to crash.
            Temporary files (e.g. trajectory in ascii format) andoutput files are first
            created in tmpdir directory, and then the output is saved in spk_file.
            Default is '/tmp/'.
        
        Returns
        -------
        None.
        
        """
        # creates the ephemeris data file header line
        names=['EPOCH','X','Y','Z','VX','VY','VZ']
        # intermediate output file
        tmpout_file = tmpdir+os.path.basename(self.spk_file)

        # get intermediate spk_dta file name
        data_file = tmpdir+os.path.basename(self.spk_file.replace('.spk','.dat'))
        # intermediate setup file name
        setup_file = tmpdir+os.path.basename(self.spk_file.replace('.spk','.setup'))
        # intermediate output file
        tmpout_file = tmpdir+os.path.basename(self.spk_file)
        
        log.debug('spk_file: '+self.spk_file)
        log.debug('data_file: '+data_file)
        log.debug('setup_file: '+setup_file)
        log.debug('tmpout_file: '+tmpout_file)
        
        # format data
        xstr = "{:.9f}".format(self.eloc.x.to('km').value)
        ystr = "{:.9f}".format(self.eloc.y.to('km').value)
        zstr = "{:.9f}".format(self.eloc.z.to('km').value)
        vxstr = "{:.9f}".format(0.0)
        vystr = "{:.9f}".format(0.0)
        vzstr = "{:.9f}".format(0.0)
       
        # write intermediate raw spk data
        with open(data_file,'w') as out:
            out.write("# "+' '.join(names)+" \n")
            t = self.timei
            while t<=self.timef:
                print("write raw data at time: "+t.iso)
                # write time
                out.write(Time(t,scale='utc').iso+"\n")
                # write data
                out.write(' '.join([xstr,ystr,zstr,vxstr,vystr,vzstr])+" \n")
                # increase time
                t = t + self.freq
        log.info("raw spk data saved in: "+data_file)
        
        # make the intermediate setup file
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
            out.write("   OUTPUT_SPK_TYPE   = 13\n")
            out.write("   OBJECT_ID         = "+str(self.nacelle_id)+"\n")
            out.write("   OBJECT_NAME       = "+repr(self.nacelle_name)+"\n")
            out.write("   CENTER_ID         = 399\n")
            out.write("   REF_FRAME_NAME    = "+repr('ITRF93')+"\n")
            out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
            out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
            out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
            out.write("   LEAPSECONDS_FILE  = "+repr(self.kernel_path+'naif0012.tls')+"\n")
            out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
            out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
            out.write("   PCK_FILE          = "+repr(self.kernel_path+'pck00010.tpc')+"\n")
            out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
            out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
            out.write("   IGNORE_FIRST_LINE = 1 \n")
            out.write("   LINES_PER_RECORD  = 2 \n")
            out.write("   POLYNOM_DEGREE    = 3 \n")
            out.write("   SEGMENT_ID        = "+repr(str(self.nacelle_id))+"\n")
            out.write(" \n")
        log.info("setup saved in: "+setup_file)
        
        # remove spk file if it already exists
        if os.path.exists(tmpout_file):
            os.remove(tmpout_file)
        # build the command
        cmd = self.spice_exe_path+"mkspk -setup "+setup_file
        log.info("cmd="+cmd)
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
            log.error(msg)
            raise IOError(msg)
        # double check that the SPK file was successfully created
        if not os.path.exists(tmpout_file):
            msg='*** FATAL ERROR: temporary SPK file: '+tmpout_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        # now copy temporary output file into spk_file
        for file2move in [setup_file,data_file,tmpout_file]:
            dst = self.out_dir+os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            log.debug('Moving file '+file2move+' into '+dst)
            try:
                # remark: os.remove does not always work when trying to move file from one FS to another...
                shutil.copy(file2move,dst)
            except:
                msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
                log.error(msg)
                raise IOError(msg)
        log.info("SPICE SPK kernel saved in: "+self.spk_file)        
        
        return
    
    
    
if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='stationmaker arguments.')
    parser.add_argument('-conf',default='./config-OHP.in',help='configuration file. Default is: ./config.in')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.conf
    
    # create Object
    stnmker = StationMaker(config_file)
    # launch simulation
    stnmker.make()
else:
    log.debug('successfully imported')
