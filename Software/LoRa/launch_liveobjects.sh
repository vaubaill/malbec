#!/bin/bash

#
# Version Live Objects
# Duplicate launch_sodaq.h and adapted to Live Objects
#
# set the environment
py=`which python3`
malbec_dir="$HOME/malbec/Software/MissionPlanner/"
logfile="$malbec_dir/launch_liveobjects.log"
workdir="$malbec_dir"

# set launch_sodaq options
script="$workdir/launch_liveobjects.py"
config_file="$workdir/conf/config.in"
test_key="False"

# put date into log file
date > $logfile
# double checking everything:
echo "python: $py" >> $logfile
echo "workdir: $workdir" >> $logfile
echo "script: $script" >> $logfile
echo "config_file: $config_file" >> $logfile
echo "test_key: $test_key" >> $logfile

# change directory
cd $workdir
# lanches the launch_sodaq.py script
echo "$py $script $config_file $test_key" >> $logfile
$py $script $config_file $test_key >> $logfile

echo "launch_liveobjects.sh done" >> $logfile
