"""Live Objects MQTT communication tools.

Authors
-------
J. Vaubaillon, IMCCE, 2019

Project
-------
The MALBEC project.

Version
-------
1.0


"""

import os
import sys
import time
import subprocess
import logging
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
import numpy as np
from astropy.coordinates import EarthLocation
from configparser import ConfigParser, ExtendedInterpolation
import missionplan
import paho.mqtt.client as mqtt
import json

from missionplanner.utils import log

# Global constants
# mode d'execution
exe_test_mode = False
# MQTT constants
lo_mqtt_fifo = ""
# set number of positions from GPS required before MissionPlanner launch can be decided
npos = 0
# current number of positions from GPS required before MissionPlanner launch can be decded
npos_current = 0
# set output file (full) name
trajectory_rlt_file = ""
# mission planner object
missplan = None
# fichier configuration
configfile = ""

# MQTT : the callback for when the client receives a CONNACK response from the server
def on_connect_mqtt(client, userdata, flags, rc):
    global lo_mqtt_fifo
    log.info("On connect MQTT with result code " + str(rc))
    if rc == 0:
        # Subscribing in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed
        log.info("MQTT subscribing to " + lo_mqtt_fifo)
        client.subscribe("fifo/" + lo_mqtt_fifo)


# MQTT : the callback for when a PUBLISH message is received from the server.
def on_message_mqtt_pipo(client, userdata, msg):
    log.info(msg.topic + " --> trashed")


# MQTT : the callback for when a PUBLISH message is received from the server.
def on_message_mqtt(client, userdata, msg):
    global exe_test_mode
    global npos
    global npos_current
    global trajectory_rlt_file
    global missplan
    global configfile

    log.info("*************************************************************************")
    log.info("Message MQTT topic " + msg.topic)
    payload = (msg.payload).decode("utf-8")
    #log.info("payload " + payload)
    json_data = json.loads(payload)
    #log.info(str(json_data))
    streamId = json_data["streamId"]
    log.info("streamId " + streamId)
    npos_current = npos_current + 1
    #log.info("metadata " + str(json_data["metadata"]))
    #log.info("network " + str(json_data["metadata"]["network"]))
    #log.info("lora " + str(json_data["metadata"]["network"]["lora"]))
    fcnt = json_data["metadata"]["network"]["lora"]["fcnt"]
    log.info("MQTT message n°" + str(npos_current) + "   -   fcnt " + str(fcnt))
    log.debug("npos current " + str(npos_current) + " / npos " + str(npos))

    go = False
    try:
        # ============== TEST CASE ==============
        if exe_test_mode:
            # get n positions before you can compute the velocity
            get_push_position(json_data, trajectory_rlt_file)
            if npos_current >= npos:
                # decide if it is time to launch MissionPlanner
                go = launch_MissionPlanner(trajectory_rlt_file)
                log.debug('go: ' + str(go))

        # ============== END OF TEST CASE ==============
        else:
            # ============== IN FLIGHT CASE ==============
            # get n positions before you can compute the velocity
            get_push_position(json_data, trajectory_rlt_file)
            if npos_current >= npos:
                # decide if it is time to launch MissionPlanner
                go = launch_MissionPlanner(trajectory_rlt_file)
                log.debug('go: ' + str(go))
    except KeyboardInterrupt:
        msg = '*** Keyboard Interrupt'
        log.error(msg)
    except:
        msg = '*** FATAL ERROR: something went wrong...'
        log.error(msg)

    if go:
        log.debug('DISCONNECT client MQTT')
        client.loop_stop(force=False)
        client.disconnect()

    if exe_test_mode and go:
        # ============== TEST CASE ==============
        # create dummy landing position
        land_time = Time('2019-10-24T08:51:35.000', format='isot')
        land_loc = EarthLocation(lon=-0.200 * u.deg, lat=43.700 * u.deg, height=80.0 * u.m)
        log.debug('land_t: ' + land_time.isot)
        log.debug('land_lat: ' + str(land_loc.lat.to('deg')))
        log.debug('land_lon: ' + str(land_loc.lon.to('deg')))
        log.debug('land_alt: ' + str(land_loc.height.to('m')))

        # send landing site to SODAQ card
        # TODO uncomments
        # success_send = send_land_position(serial_obj, land_time, land_loc.lat, land_loc.lon, land_loc.height)
        # log.debug('success_send=' + str(success_send))
        # ============== END OF TEST CASE ==============
    elif go:
        # ============== IN FLIGHT CASE ==============
        # create MissionPlanner Object
        log.debug('Creating MissionPlanner object')
        missplan = missionplan.MissionPlanner(configfile)
        # launches MissionPlanner
        log.debug('Now launches Mission Planner')
        (land_t, land_loc) = missplan.launch()
        log.debug('Back from Mission Planner')
        log.debug('land_t: ' + land_t.isot)
        log.debug('land_lat: ' + str(land_loc.lat.to('deg')))
        log.debug('land_lon: ' + str(land_loc.lon.to('deg')))
        log.debug('land_alt: ' + str(land_loc.height.to('m')))

        # send landing site to SODAQ
        # TODO uncomments
        # success_send = send_land_position(serial_obj, land_t, land_loc.lat, land_loc.lon, land_loc.height)
        # log.debug('success_send=' + str(success_send))
        # ============== END OF IN FLIGHT CASE ==============

    log.info("END on message MQTT")




# get MissionPlanner configuration file from argument
def launch_liveobjects(config_file, test_mode):
    """Launches the communication with Live Objects and run mission planner when needed.
    
    Parameters
    ----------
    config_file : string
        Configuration file for MissionPlanner1.1 object.
    test_mode : boolean
        If True, MissionPlanner is not run and a dummy position is sent back to SODAQ card.

    """
    global lo_mqtt_fifo
    global exe_test_mode
    global trajectory_rlt_file
    global npos
    global missplan
    global configfile
    configfile = config_file
    exe_test_mode = test_mode
    if not os.path.exists(config_file):
        msg = '*** FATAL ERROR: config file ' + config_file + ' does not exist'
        raise IOError(msg)

    # create ConfigParser Object
    config = ConfigParser(interpolation=ExtendedInterpolation())
    # read configuration file
    config.read(config_file)
    # set outut file (full) name
    trajectory_rlt_file = config['CAMPAIGN']['trajectory_rlt_file']
    log.info('Trajectory realtime position file: ' + trajectory_rlt_file)
    # set nb positions
    npos = int(config['CAMPAIGN']['nbpos'])
    log.info('Nb positions: ' + str(npos))

    # # create MissionPlanner Object
    # if not test_mode:
    #     log.debug('Creating MissionPlanner object')
    #     missplan = missionplan.MissionPlanner(config_file)

    # MQTT initialization
    log.debug('MQTT vidange')
    mqtt_port = int(config['LO']['port'])
    mqtt_keepalive = int(config['LO']['keepalive'])
    lo_mqtt_fifo = config['LO']['fifo']

    client_pipo = mqtt.Client()
    client_pipo.on_connect = on_connect_mqtt
    client_pipo.on_message = on_message_mqtt_pipo
    client_pipo.username_pw_set(config['LO']['username'], password=config['LO']['password'])
    client_pipo.connect(config['LO']['ip'], mqtt_port, mqtt_keepalive)
    client_pipo.loop_start()
    # Wait for 10 seconds
    time.sleep(10)
    client_pipo.loop_stop(force=False)
    client_pipo.disconnect()

    log.debug('MQTT initialization')
    mqtt_client = mqtt.Client()
    mqtt_client.on_connect = on_connect_mqtt
    mqtt_client.on_message = on_message_mqtt
    mqtt_client.username_pw_set(config['LO']['username'], password=config['LO']['password'])
    log.debug('MQTT connect')
    mqtt_client.connect(config['LO']['ip'], mqtt_port, mqtt_keepalive)

    # Blocking call that processes network traffic, dispatches callbacks and handles reconnecting
    mqtt_client.loop_forever()

    # initialization: sends back the incoming message to make sure everything is working fine.
    # log.debug('Initialization: waiting for first message to be received')
    # (time_sodaq, lon_sodaq, lat_sodaq, alt_sodaq) = get_position_from_sodaq(serial_obj)
    # log.debug('Message received: bouncing it back.')
    # success_send = send_land_position(serial_obj, time_sodaq, lon_sodaq, lat_sodaq, alt_sodaq)
    # log.debug('success_send=' + str(success_send))

    return


def get_push_position(json_data, out_file):
    """Get one position and push it into output file.
    
    Parameters
    ----------
    json_data : data sent by the device
        Json object
    out_file : string
        Output file where the positions will be appended.

    """
    # get latest position
    log.debug('getting position from live objects')
    (time_lo, lon_lo, lat_lo, alt_lo) = get_position_from_liveobjects(json_data)

    # puts latest position into output file
    log.debug('put last position into ' + out_file)
    put_latest_position_into_file(out_file, time_lo, lon_lo, lat_lo, alt_lo)

    return


def get_position_from_liveobjects(json_data):
    """Get the current position from live objects.
    
    Parameters
    ----------
    json_data : data sent by the device
        Json object
    
    Returns
    -------
    time_sodaq : astrop.time.Time object
        Time
    lon_sodaq : astropy.units.Quantity object
        Longitude in deg
    lat_sodaq : astropy.units.Quantity object
       Latitude in deg
    alt_sodaq : astropy.units.Quantity object
        Altitude in m
    """
    # communication with Live Object


    # example of json data
    # { "timestamp":"2019-11-26T16:20:40.778Z", "value":{"payload":"0010141d1675d3015e2df00001ced7","hour":16.0,"lati":48.8011219,"longi":2.294936,"alti":118.487,"frame":0.0,"minute":20.0}}
    # log.debug('json:' + str(json_data))
    time_lo = json_data["timestamp"]
    # withdraw of the last char Z
    time_lo = time_lo[:-1]
    lat_lo = json_data["value"]["lati"]
    lon_lo = json_data["value"]["longi"]
    alt_lo = json_data["value"]["alti"]
    log.debug('time: ' + time_lo + " lon: "+str(lon_lo) + " lat: "+str(lat_lo) + " alt: "+str(alt_lo))
    #  returns latest position
    return time_lo, lon_lo, lat_lo, alt_lo


    # example of SODAQ message:
    # b'4512098765,212387654,1200,2019-10-24T08:51:35.\n'
    # # convert time into astropy.time.Time object
    # time_sodaq = Time(sodaq_time, format='isot')
    # # convert lon,lat, alt into an astropy.units.Quantity object
    # lat_sodaq = float(sodaq_lat) / 1.0E+07 * u.deg
    # lon_sodaq = float(sodaq_lon) / 1.0E+07 * u.deg
    # alt_sodaq = float(sodaq_alt) / 1.0E+03 * u.m


def put_latest_position_into_file(filename, time_lo, lon_lo, lat_lo, alt_lo):
    """Put latest position at the end of a trajectory file.
    
    Parameters
    ----------
    filename : string
        output file name (full path)
    time_lo : string
        Time iso format
    lon_lo : int
        Longitude in deg
    lat_lo : int
       Latitude in deg
    alt_lo : int
        Altitude in m
    
    Returns
    -------
    None.
    
    """
    # double check existence of file and set write mode
    if not os.path.exists(filename):
        openmode = 'w'
        log.info('File ' + filename + ' will be created')
    else:
        openmode = 'a+'

    # write into file
    try:
        with open(filename, openmode) as f:
            # f.write(time_sodaq.isot + ' ' + str(lon_sodaq.to('deg').value) + ' ' + str(
            #     lat_sodaq.to('deg').value) + ' ' + str(alt_sodaq.to('m').value) + '\n')
            f.write(time_lo + ' ' + str(lon_lo) + ' ' + str(lat_lo) + ' ' + str(alt_lo) + '\n')
            log.debug('latest position saved in ' + filename)
    # handle all possible errors
    except(IOError):
        msg = '*** FATAL ERROR: impossible to save into ' + filename
        log.error(msg)
        raise IOError(msg)
    except(ValueError):
        msg = '*** Value Error: double check every variable.'
        log.error('time = ' + time_lo)
        log.error('lon = ' + str(lon_lo))
        log.error('lat = ' + str(lat_lo))
        log.error('alt = ' + str(alt_lo))
        raise ValueError(msg)
    except:
        msg = '*** FATAL ERROR: something wrong happened///'
        raise ValueError(msg)

    return


def tail(filename, n, offset=0):
    """Get the last n lines of a file
    
    Parameters
    ----------
    filename : file object
        File for whic we want the last n lines.
    n : integer
        number of lines
    offset : integer
        Offset.
    
    Returns
    -------
    lines : list of strings
        Last n lines of the file.
    """
    proc = subprocess.Popen(['tail', '-n', n + offset, filename], stdout=subprocess.PIPE)
    lines = proc.stdout.readlines()
    return lines[:, -offset]


def read_sodaq_traj(trajectory_rlt_file, n=None):
    """Read SODAQ trajectory file.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file.
    n : integer, optional
        If not None, read the latest n lines of trajectory_rlt_file.
        Default is None.
    
    Returns
    -------
    data : astropy.table.QTalbe
        All data in trajectory_rlt_file.
    
    """
    log.debug('Reading: ' + trajectory_rlt_file)
    # set n lies to read
    if n:
        outf = 'junk'
        # filename undefined ? replaced by trajectory_rlt_file
        proc = subprocess.Popen(['tail', '-n', n, trajectory_rlt_file, ' > ', outf], stdout=subprocess.PIPE)
        file_to_read = outf
    else:
        file_to_read = trajectory_rlt_file
    # read trajectory file and set units
    data = QTable.read(trajectory_rlt_file, format='ascii', names=['Time', 'lon', 'lat', 'alt'])
    data['lon'].unit = u.deg
    data['lat'].unit = u.deg
    data['alt'].unit = u.m
    # convert time into Time object
    time = Time(data['Time'], format='isot')
    data['Time'] = time
    return data


def launch_MissionPlanner(trajectory_rlt_file):
    """Decide to launch MissionPLanner.
    
    Parameters
    ----------
    trajectory_rlt_file : string
        Real time trajectory file.
    
    Returns
    -------
    launch : boolean
        True if descending phase. False otherwise.
    
    """
    # read trajectory file and set units
    data = read_sodaq_traj(trajectory_rlt_file)
    # compute time and altitude difference
    dt = data['Time'][-3:] - data['Time'][-4:-1]
    da = data['alt'][-3:] - data['alt'][-4:-1]
    # compute vertical velocity
    Vvert = da / dt
    log.info('Vvert = ' + str(Vvert.to('m/s')))
    # if vertical velocity is negative the fall has started => time to predict landing point.
    if np.all(Vvert > 0.0 * u.m / u.s):
        launch = False
    else:
        launch = True
    return launch


# def get_initcond(trajectory_rlt_file):
#     """Get initial conditions.
#
#     Parameters
#     ----------
#     trajectory_rlt_file : string
#         Real time trajectory file from SODAQ card measurements.
#
#     Returns
#     -------
#     last_point : astropy.coordinates.EarthLocation object
#         Last measured point.
#     last_time : astropy.time.Time object
#         Tim of last point.
#     Vvert : astropy.units.Quantity object
#         Median vertical velocity in [m/s].
#
#     """
#     # read trajectory file and set units
#     data = read_sodaq_traj(trajectory_rlt_file)
#     # compute time and altitude difference
#     dt = data['Time'][-3:] - data['Time'][-4:-1]
#     da = data['alt'][-3:] - data['alt'][-4:-1]
#     # compute vertical velocity
#     Vvert = [0.0, 0.0, np.median(da / dt).to('m/s').value] * u.m / u.s
#     # set last position
#     last_point = EarthLocation(lon=data['lon'][-1],
#                                lat=data['lat'][-1],
#                                height=data['alt'][-1])
#     # set last time
#     last_time = data['Time'][-1]
#
#     return (last_time, last_point, Vvert)


def send_land_position(serial_obj, land_time, land_lat, land_lon, land_alt):
    """Send predicted landing position to ground via LoRa network, through SODAQ card.
    
    Parameters
    ----------
    serial_obj : serial.Serial object
        Serial object with open UART port.
    land_time : astrop.time.Time object
        Time
    land_lon : astropy.units.Quantity object
        Longitude in deg
    land_lat : astropy.units.Quantity object
       Latitude in deg
    land_alt : astropy.units.Quantity object
        Altitude in m
    
    Returns
    -------
    success_send : boolean
        True is message sent successfully. False otherwise.
    
    """
    # separator
    sep = ','
    sodaq_time = land_time.isot
    # convert lon,lat, alt into an astropy.units.Quantity object
    sodaq_lat = str(int(land_lat.to('deg').value * 1.0E+07))
    sodaq_lon = str(int(land_lon.to('deg').value * 1.0E+07))
    sodaq_alt = str(int(land_alt.to('m').value * 1.0E+03))

    # format the message: must be a long string
    sodaq_msg = sep.join([sodaq_lat, sodaq_lon, sodaq_alt]) + ',' + sodaq_time + '\n'
    log.debug('sodaq_msg: ' + sodaq_msg)

    # send to SODAQ card
    success_send = send_msg_twice(serial_obj, sodaq_msg)

    return success_send


def send_msg_twice(serial_obj, sodaq_msg):
    """Send a message to SODAQ card twice to make sure it receives it.
    
    Parameters
    ----------
    serial_obj : serial.Serial object
        Serial object with open UART port.
    sodaq_msg : string
        Message to send to SODAQ card.
    
    Returns
    -------
    success_send : boolean
        True is messages were sent successfully. False otherwise.
    
    """
    try:
        import time
        # send to SODAQ card twice with a 1-second interval.
        log.debug('msg about to be sent')
        serial_obj.write(bytes(sodaq_msg, 'utf-8'))
        log.debug('msg written to port once')
        # wait for 1 sec before sending the 2nd message.
        time.sleep(1)
        serial_obj.write(bytes(sodaq_msg, 'utf-8'))
        log.debug('msg written to port twice')
        success_send = True
    except:
        success_send = False
    return success_send
