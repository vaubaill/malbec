/*
* Copyright (C) 2017 Orange
*
* This software is distributed under the terms and conditions of the 'Apache-2.0'
* license which can be found in the file 'LICENSE.txt' in this package distribution
* or at 'http://www.apache.org/licenses/LICENSE-2.0'.
*/

/* Orange LoRa Explorer Kit
*
* Version:     1.0-SNAPSHOT
* Created:     2017-02-15 by Karim BAALI
* Modified:    2017-04-21 by Halim BENDIABDALLAH
*			   2017-05-09 by Karim BAALI
*/

#include <OrangeForRN2483.h>
//#include <Wire.h> //Needed for I2C to GPS & OpenLog (already called by GPS Lib
#include <SparkFun_Ublox_Arduino_Library.h> //http://librarymanager/All#SparkFun_Ublox_GPS
#include <SparkFun_Qwiic_OpenLog_Arduino_Library.h> // Logger

/*create our objects*/
SFE_UBLOX_GPS myGPS; // for the GPS
OpenLog myLog; // for the logger


#define debugSerial SerialUSB

// The following keys are for structure purpose only. You must define YOUR OWN.
//AppEUI A0ACB2670DA9B1AE 
const int8_t appEUI[8] = { 0xA0, 0xAC, 0xB2, 0x67, 0x0D, 0xA9, 0xB1, 0xAE };
// AppKey 10468D9509D0BF050A97A70416651920
const int8_t appKey[16] = { 0x10, 0x46, 0x8D, 0x95, 0x09, 0xD0, 0xBF, 0x05, 0x0A, 0x97, 0xA7, 0x04, 0x16, 0x65, 0x19, 0x20 };

int latitude =0; //for instant GPS position LoRa transmission
int longitude=0; //for instant GPS position LoRa transmission
int altitude=0;  //for instant GPS position LoRa transmission
byte frametype = 0; //to differentiate instant GPS data & estimated landing coordinates (0=instant, 1=estimated) 

unsigned long timestampTask1 = 0; //timer for GPS query & send ti PiZero (every 5 seconds)
unsigned long timestampTask2 = 0; //timer for sending instant GPS position to ground via LoRa

int year = 0; //to store time data from the GPS (for the logger)
byte month = 0; //to store time data from the GPS (for the logger)
byte day = 0; //to store time data from the GPS (for the logger)
byte hour = 0;  //to store time data from the GPS (for the logger)
byte minute = 0; //to store time data from the GPS (for the logger)
byte second = 0; //to store time data from the GPS (for the logger)

const byte numChars = 50; // maximum size of the char array recieved on serial from the PiZero
char receivedChars[numChars];   // an array to store the received data

String Mystring; // to hold serial data recieved from the PiZero
int i_longitude; // variable to hold data from the PiZero
int i_latitude; // variable to hold data from the PiZero
int i_altitude; // variable to hold data from the PiZero
byte hour_landing; // variable to hold data from the PiZero
byte minute_landing; // variable to hold data from the PiZero

int firstcomma; // variable used for string decoding
int secondcomma;  // variable used for string decoding
int thirdcomma; // variable used for string decoding
int first_semicol; // variable used for string decoding
int second_semicol; // variable used for string decoding
int T_time; // variable used for string decoding

static byte ndx = 0; // variable used for string decoding
char endMarker = '\n'; // variable used for string decoding
char rc; // variable used for string decoding
boolean newData = false; // variable used for string decoding

typedef enum _eLedColor {
  RED = 0,
  GREEN,
  BLUE,
  WHITE,
  ORANGE 
}eLedColor;

typedef struct _sRgbColor
{
  int red;
  int green;
  int blue;
}sRgbColor;

sRgbColor listRgbColor[] = {
  {.red = LOW, .green = HIGH, .blue = HIGH},
  {.red = HIGH, .green = LOW, .blue = HIGH},
  {.red = HIGH, .green = HIGH, .blue = LOW},
  {.red = LOW, .green = LOW, .blue = LOW},
  {.red = LOW, .green = LOW, .blue = HIGH},
};

void initRgbLed()
{
  //Initialize the LEDs and turn them all off
  pinMode(LED_RED, OUTPUT) ;
  pinMode(LED_GREEN, OUTPUT) ;
  pinMode(LED_BLUE, OUTPUT) ;   
}

void initPin()
{
  //Initialize the LEDs and turn them all off
  initRgbLed();  
}

void setRgbColor(eLedColor color){
  digitalWrite(LED_RED, listRgbColor[color].red);
  digitalWrite(LED_GREEN, listRgbColor[color].green);
  digitalWrite(LED_BLUE, listRgbColor[color].blue);
}

void setup()
{ 
  initPin();     
  debugSerial.begin(57600);
  Serial.begin(9600); // starting serial to communicate with the PiZero that calculates the estimated landing time & position
  Serial3.begin(9600);// starting serial 3 to communicate with the second starterkit that doubles the coordinates sending to the ground (A4 is TX, A5 is RX)
  debugSerial.println("starting programm...waiting for 10 sec");
  delay(10000);
    Wire.begin(); 
    myGPS.begin();
    myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
    myGPS.saveConfiguration(); //Save the current settings to flash and BBR

    myLog.begin(QOL_DEFAULT_ADDRESS,Wire); //Open connection to OpenLog (no pun intended)
    debugSerial.print("Qwiic OpenLog Version: ");
    debugSerial.println(String(myLog.getVersion()+"\r\n"));
    myLog.writeString("latitude,longitude,altitudeMSL"); // writing the data format on top of log file
    delay(30);
    myLog.writeString("Tyear-month-day");
    delay(30);
    myLog.writeString("hour:minute:second.000");
    delay(30);
    myLog.writeString("\r\n");
    delay(30);
    myLog.syncFile();
    
    

    
  
  //while((!debugSerial) && (millis() < 10000));
  
  OrangeForRN2483.init();

  setRgbColor(ORANGE);

  LpwaOrangeEncoder.flush();

  bool res = 0;
  int attempt = 0;
  do 
  {
    setRgbColor(BLUE);
    debugSerial.println("Join Request") ;

    OrangeForRN2483.setDataRate(DATA_RATE_2); // Set DataRate to SF10/125Khz
    
    res = OrangeForRN2483.joinNetwork(appEUI, appKey);
    //OrangeForRN2483.enableAdr(); //we don't want adr to be on to maximize transmission
    debugSerial.println(res ? "Join Accepted." : "Join Failed! Trying again. Waiting 10 seconds.") ;
    
    if(!res)
    {
      setRgbColor(RED);
      attempt++;
      delay(10000);
    }
    
    if (attempt == 3) 
    {
      debugSerial.println("All Join Failed. Program stoped") ;
      while(true) {        
        setRgbColor(RED);
        delay(1000);
        setRgbColor(ORANGE);
        delay(1000);
        setRgbColor(WHITE);
        delay(1000);

      }
    }
  }while (res == 0);
  
  debugSerial.println("Sleeping for 5 seconds before starting sending  packets.");
  sleep(5);  
}

void sleep(unsigned short count)
{
  for (uint8_t i = count; i > 0; i--)
  {
    debugSerial.println(i) ;
    delay(1000) ;
  }
}

void debugFrame(int8_t* frame, int8_t len)
{  
    if(frame != NULL)
    {
        debugSerial.print("Payload: ");
        int i = 0;
        for(i = 0; i < len; i++)
        {
            unsigned char value = (unsigned char)frame[i];
            debugSerial.print(value, HEX); debugSerial.print(" ");
        }
        debugSerial.print("\r\n");
    }
}





void loop() {

  timestampTask1 = millis();
  timestampTask2 = millis();
    
  while(true)
  {
      //if (Serial.available())     // If anything comes in Serial (pins 0 & 1)
    //debugSerial.write(Serial.read());   // read it and send it out Serial (USB)
    while (Serial.available() > 0 && newData == false) //checking if serial data are available from the PiZero
    {
        rc = Serial.read();

        if (rc != endMarker) {
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars) {
                ndx = numChars - 1;
            }
        }
        else {
            receivedChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            newData = true;
        }
    }
       showNewData(); //processing data form the PiZero
    
      if ((millis() - timestampTask1) > 5000) 
      {
            setRgbColor(WHITE);
            GetGPSinfo();
            SendToPi();
            LogGPSinfo();
            timestampTask1 = millis();
      }
     
      if((millis() - timestampTask2) > 60000)
      {
        
        LpwaOrangeEncoder.flush();
        LpwaOrangeEncoder.addByte(frametype);
        LpwaOrangeEncoder.addByte(hour);
        LpwaOrangeEncoder.addByte(minute);
        LpwaOrangeEncoder.addInt(latitude);
        LpwaOrangeEncoder.addInt(longitude);
        LpwaOrangeEncoder.addInt(altitude);
        

        int port = 5;
        int8_t len;
        int8_t* frame = LpwaOrangeEncoder.getFramePayload(&len);
        debugFrame(frame, len);
        bool res = OrangeForRN2483.sendMessage(frame, len, port);
    
        if(res) {
            debugSerial.println("Successful transmission.");
            DownlinkMessage* downlinkMessage = OrangeForRN2483.getDownlinkMessage();
    
            int port = downlinkMessage->getPort();
            String message = downlinkMessage->getMessage();
            int8_t len = 0;
            const int8_t* msgint= downlinkMessage->getMessageByteArray(&len);
            
            if(len >= 2)
            {
              uint16_t value = ((uint16_t)(msgint[0] << 8)) | msgint[1];
              debugSerial.print("Receive value=");
              debugSerial.println(value, DEC);
            }
            else debugSerial.println("value not found");
                    
            debugSerial.print("Port :");
            debugSerial.print(port);
            if(message == NULL) 
              debugSerial.println("Response with empty payload"); 
            else {
              debugSerial.print("Frame Payload :");
              debugSerial.println(message);
              unsigned int messagelen = message.length();
              
              debugSerial.print(messagelen);
              
            }   
            
            setRgbColor(GREEN);
           
            //delay(2000);  
            setRgbColor(WHITE);   
          }
          else 
          {
              setRgbColor(RED);
              eErrorType errorType = OrangeForRN2483.getLastError();
              switch(errorType) 
              {   
                case LORA_TIMEOUT:       
                  debugSerial.println("Connection timed-out. Check your serial connection to the device! Sleeping for 20sec.");
                  delay(20000) ;
                break ;
          
                case LORA_INVALID_DATA_LEN:
                  debugSerial.println("The size of the payload is greater than allowed. Transmission failed!");
                break ;
               
                case LORA_BUSY:
                  debugSerial.println("The device is busy. Sleeping for 10 extra seconds.");
                  delay(10000) ;
                break ;
                  
                default: 
                  debugSerial.println("Other error");
                  delay(10000) ;
                break ;
          } 
       }
       InstantGroundBackup();
       timestampTask2 = millis(); 
    }   
  }
}

void GetGPSinfo ()//request the GPS receviver for instant position data
{   
    latitude = myGPS.getLatitude();
    longitude = myGPS.getLongitude();
    altitude = myGPS.getAltitudeMSL();
    year = myGPS.getYear();
    month = myGPS.getMonth();
    day = myGPS.getDay();
    hour = myGPS.getHour();
    minute = myGPS.getMinute();
    second = myGPS.getSecond();
}
void LogGPSinfo()//log the GPS instant position received
      {
        myLog.writeString(String(latitude));
        delay(30);
      myLog.writeString(",");
        delay(30);
      myLog.writeString(String(longitude));
        delay(30);
      myLog.writeString(",");
        delay(30);
      myLog.writeString(String(altitude));
        delay(30);
      myLog.writeString(",");
        delay(30);
      myLog.writeString(String(year));
        delay(30);
      myLog.writeString("-");
        delay(30);
        if (month < 10) myLog.writeString("0");
      myLog.writeString(String(month));
         delay(30);
      myLog.writeString("-");
        delay(30);
        if (day < 10) myLog.writeString("0");
        myLog.writeString(String(day));
         delay(30);
      myLog.writeString("T");
        delay(30);
        if (hour < 10) myLog.writeString("0");
      myLog.writeString(String(hour));
         delay(30);
      myLog.writeString(":");
      delay(30);
      if (minute < 10) myLog.writeString("0");
      myLog.writeString(String(minute));
         delay(30);
      myLog.writeString("-");
      delay(30);
      if (second < 10) myLog.writeString("0");
      myLog.writeString(String(second));
         delay(30);
      myLog.writeString(".000");
        delay(30);
        myLog.writeString("\r\n");
         
      myLog.syncFile();  
}

void SendToPi ()// send the instant position form the GPS receiver to the PiZero for estimate landing position calculation
{
  Serial.print(latitude);
  Serial.print(",");
  Serial.print(longitude);
  Serial.print(",");
  Serial.print(altitude);
  Serial.print(",");
  Serial.print(year);
  Serial.print("-");
  if (month < 10) Serial.print("0");
  Serial.print(month);
  Serial.print("-");
  if (day < 10) Serial.print("0");
  Serial.print(day);
  Serial.print("T");
  Serial.print(hour);
  Serial.print(":");
  if (minute < 10) Serial.print("0");
  Serial.print(minute);
  Serial.print(":");
  if (second < 10) Serial.print("0");
  Serial.print(second);
  Serial.print(".000");
  Serial.write('\n');
}
void showNewData() // function to handle data received from the PiZero & actions
{
    if (newData == true) {
        debugSerial.print("This just in ... ");
        debugSerial.println(receivedChars);
        Mystring = (String(receivedChars));
        firstcomma = Mystring.indexOf(',');
        secondcomma = Mystring.indexOf(',',firstcomma + 1);
        thirdcomma = Mystring.indexOf(',',secondcomma + 1);
        first_semicol = Mystring.indexOf(':');
        second_semicol = Mystring.indexOf(':',first_semicol + 2);
        T_time = Mystring.indexOf('T');
        debugSerial.print("firstcomma :");
        debugSerial.println(firstcomma);
        debugSerial.print("secondcomma :");
        debugSerial.println(secondcomma);
        debugSerial.print("thirdcomma :");
        debugSerial.println(thirdcomma);
        debugSerial.println("as a string");
        debugSerial.println(Mystring);
        
        
        i_latitude = (Mystring.substring(0,',').toInt());
        i_longitude = (Mystring.substring(firstcomma + 1,secondcomma).toInt());
        i_altitude = (Mystring.substring(secondcomma + 1,thirdcomma).toInt());
        hour_landing = (Mystring.substring(T_time + 1,first_semicol).toInt());
        minute_landing = (Mystring.substring(first_semicol + 1,second_semicol).toInt());
        
        debugSerial.print("longitude extract :");
        debugSerial.println(i_longitude);
        debugSerial.print("latitude extract :");
        debugSerial.println(i_latitude);
        debugSerial.print("altitude extract :");
        debugSerial.println(i_altitude);
        debugSerial.print("estimated landing time :");
        debugSerial.print(hour_landing);
        debugSerial.print(":");
        debugSerial.println(minute_landing);
        frametype = 1;

        LpwaOrangeEncoder.flush();
        LpwaOrangeEncoder.addByte(frametype);
        LpwaOrangeEncoder.addByte(hour_landing);
        LpwaOrangeEncoder.addByte(minute_landing);
        LpwaOrangeEncoder.addInt(i_latitude);
        LpwaOrangeEncoder.addInt(i_longitude);
        LpwaOrangeEncoder.addInt(i_altitude);

        int port_i = 7;
        int8_t len_i;
        int8_t* frame_i = LpwaOrangeEncoder.getFramePayload(&len_i);
        debugFrame(frame_i, len_i);

        OrangeForRN2483.sendMessage(frame_i, len_i, port_i);
        EstimateGroundBackup();
        newData = false;
        frametype = 0;
    }
}
void InstantGroundBackup ()// send the instant position form the GPS receiver to the second LoRa card as a backup to ground & Network Analysis (the second card is LoRa adr ON)
{  
  Serial3.print("0");
  Serial3.print(latitude);
  Serial3.print(",");
  Serial3.print(longitude);
  Serial3.print(",");
  Serial3.print(altitude);
  Serial3.print(",");
  Serial3.print(year);
  Serial3.print("-");
  if (month < 10) Serial3.print("0");
  Serial3.print(month);
  Serial3.print("-");
  if (day < 10) Serial3.print("0");
  Serial3.print(day);
  Serial3.print("T");
  Serial3.print(hour);
  Serial3.print(":");
  if (minute < 10) Serial3.print("0");
  Serial3.print(minute);
  Serial3.print(":");
  if (second < 10) Serial3.print("0");
  Serial3.print(second);
  Serial3.print(".000");
  Serial3.write('\n');
}
void EstimateGroundBackup ()// send the estimated position from the PiZero to the second LoRa card as a backup to ground & Network Analysis (the second card is LoRa adr ON)
{
  Serial3.print("1");
  Serial3.print(i_latitude);
  Serial3.print(",");
  Serial3.print(i_longitude);
  Serial3.print(",");
  Serial3.print(i_altitude);
  Serial3.print(",");
  Serial3.print(year);
  Serial3.print("-");
  if (month < 10) Serial3.print("0");
  Serial3.print(month);
  Serial3.print("-");
  if (day < 10) Serial3.print("0");
  Serial3.print(day);
  Serial3.print("T");
  Serial3.print(hour_landing);
  Serial3.print(":");
  if (minute < 10) Serial3.print("0");
  Serial3.print(minute_landing);
  Serial3.print(":");
  if (second < 10) Serial3.print("0");
  Serial3.print(second);
  Serial3.print(".000");
  Serial3.write('\n');
}
