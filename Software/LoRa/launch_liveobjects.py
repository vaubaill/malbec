"""Launches the scripts to communicate with Live Objects and launch MissionPlanner.
Get GPS position from Live Objects https://liveobjects.orange-business.com/
Put current position into a file.
The latests position will tell the flight phase (ASCEND, FREEFALL).
MissionPlanner is launched when needed (descent).
The result of the predicted landing point is sent to SODAQ card.
Parameters
----------
config_file : string
     Configuration file for MissionPlanner1.1
test_mode : string
    if 'True' MissionPlanner is launched.
    To be set to 'True' for real flight.
    If False, a dummy landing point is returned.
Returns
-------
None.
"""
import os
import sys
import logging
import liveobjects

from missionplanner.utils import log

"""Main program: get the current position and save into output file.
"""
# set syntax
syntax = 'python3 launch_liveobjects.py config_file test_mode'
# get MissionPlanner configuration file from argument
try:
    config_file = sys.argv[1]
    test_mode = sys.argv[2] in ['true','True','TRUE','1','Test','test','TEST']
except:
    msg = 'syntax is: '+syntax
    raise ValueError(msg)

if not os.path.exists(config_file):
    msg = '*** FATAL ERROR: config file '+config_file+' does not exist'
    raise IOError(msg)

liveobjects.launch_liveobjects(config_file, test_mode)
