import serial
import time
from astropy.time import Time
import astropy.units as u

print('opening serial port')
s = serial.Serial('/dev/ttyAMA0', 9600) # change name, if needed
#s.open()
n=5
print('waiting for '+str(n)+' sec')
time.sleep(n) # the Arduino is reset after enabling the serial connectio, therefore we have to wait some seconds

print('Listening to serial port')


#s.write("test")
try:
    while True:
        sodaq_msg = str(s.readline()) # wait for \n to retrieve response
        print('sodaq_msg = '+sodaq_msg)
        sep = ','
        [sodaq_lat,sodaq_lon,sodaq_alt,sodaq_time] = str(sodaq_msg).split("'")[1][:-5].split(sep)
        print('sodaq_lat='+sodaq_lat)
        print('sodaq_lon='+sodaq_lon)
        print('sodaq_alt='+sodaq_alt)
        print('sodaq_time='+sodaq_time)
        
        # convert time into astropy.time.Time object
        time = Time(sodaq_time,format='isot')
        # convert lon,lat, alt into an astropy.units.Quantity object
        lat = float(sodaq_lat) / 1.0E+07 * u.deg
        lon = float(sodaq_lon) / 1.0E+07 * u.deg
        alt = float(sodaq_alt) / 1.0E+03 * u.m
        print('time='+time.isot)
        print('lat='+str(lat))
        print('lon='+str(lon))
        print('alt='+str(alt))
        
except KeyboardInterrupt:
    s.close()
