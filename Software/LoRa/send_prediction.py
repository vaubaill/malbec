"""Send landing position prediction to LoRa SODAQ card.

"""
import serial
import time
from astropy.time import Time
import astropy.units as u



print('opening serial port')
serial_obj = serial.Serial('/dev/ttyAMA0', 9600) # change name, if needed
n=5
print('waiting for '+str(n)+' sec')
time.sleep(n) # the Arduino is reset after enabling the serial connectio, therefore we have to wait some seconds

print('Talking to serial port')

# define variables to send
land_time = Time('2019-11-23T10:0:00.000',format='isot')
land_lon = 3.43*u.deg
land_lat= 43.456*u.deg
land_alt= 100.0*u.m

# separator
sep = ','	
sodaq_time = land_time.isot
# convert lon,lat, alt into sodaq format
sodaq_lat = str(int(land_lat.to('deg').value * 1.0E+07))
sodaq_lon = str(int(land_lon.to('deg').value * 1.0E+07))
sodaq_alt = str(int(land_alt.to('m').value * 1.0E+03))
print('sodaq_time='+sodaq_time)
print('sodaq_lat='+sodaq_lat)
print('sodaq_lon='+sodaq_lon)
print('sodaq_alt='+sodaq_alt)


# format the message: must be a long string
sodaq_msg = sep.join([sodaq_lat,sodaq_lon,sodaq_alt,sodaq_time])
print('sodaq_msg: '+sodaq_msg)
	
# send to SODAQ card
try:
	while True:
		serial_obj.write(bytes(sodaq_msg,'utf-8'))
		time.sleep(n)
except KeyboardInterrupt:
    serial_obj.close()
