import serial
import time
from sodaq import send_land_position
import astropy.units as u
from astropy.time import Time

print('opening serial port')
# create Serial object and opens the port
serial_obj = serial.Serial('/dev/ttyAMA0', 9600) # change name, if needed

# the Arduino is reset after enabling the serial connectio, therefore we have to wait some seconds
sleep_sec=30
print('waiting for '+str(sleep_sec)+' sec')

land_time=Time('2019-10-24T08:51:35.000',format='isot')
land_lat=45.12098765*u.deg
land_lon=2.12387654*u.deg
land_alt=1200.0*u.m

try:
    # infinite loop
    while True:
        send_land_position(serial_obj,land_time,land_lat,land_lon,land_alt)
        time.sleep(sleep_sec)
except KeyboardInterrupt:
    serial_obj.close()



"""
# example of sodaq message
sodaq_msg = b'4512098765,212387654,1200,2019-10-24T08:51:35.000\n'

print('Talking to serial port')
# send to SODAQ card ad infinitum
try:
	# infinite loop
	while True:
		# send msg to SODAQ card
		response = s.write(sodaq_msg)
		time.sleep(sleep_sec)
except KeyboardInterrupt:
    s.close()
"""
