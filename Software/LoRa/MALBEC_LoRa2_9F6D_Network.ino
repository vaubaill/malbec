#include <OrangeForRN2483.h>

#define debugSerial SerialUSB

// The following keys are for structure purpose only. You must define YOUR OWN.
//70B3D57ED001BABA
//70 B3 D5 7E D0 01 BA BA 
const int8_t appEUI[8] = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x01, 0xBA, 0xBA };
//B0B197D149F90A0EE08FBFDDCA429103
//B0 B1 97 D1 49 F9 0A 0E E0 8F BF DD CA 42 91 03
const int8_t appKey[16] = { 0xB0, 0xB1, 0x97, 0xD1, 0x49, 0xF9, 0x0A, 0x0E, 0xE0, 0x8F, 0xBF, 0xDD, 0xCA, 0x42, 0x91, 0x03 };

// Variable Definition
long timestamp = 0;
uint8_t frameReceived;
int latitude =0; //for instant GPS position LoRa transmission
int longitude=0; //for instant GPS position LoRa transmission
int altitude=0;  //for instant GPS position LoRa transmission
byte frametype = 0; //to differentiate instant GPS data & estimated landing coordinates (0=instant, 1=estimated) 

unsigned long timestampTask1 = 0; //timer for GPS query & send ti PiZero (every 5 seconds)
unsigned long timestampTask2 = 0; //timer for sending instant GPS position to ground via LoRa

int year = 0; //to store time data from the GPS (for the logger)
byte month = 0; //to store time data from the GPS (for the logger)
byte day = 0; //to store time data from the GPS (for the logger)
byte hour = 0;  //to store time data from the GPS (for the logger)
byte minute = 0; //to store time data from the GPS (for the logger)
byte second = 0; //to store time data from the GPS (for the logger)

const byte numChars = 50; // maximum size of the char array recieved on serial from the PiZero
char receivedChars[numChars];   // an array to store the received data

String Mystring; // to hold serial data recieved from the PiZero
int i_longitude; // variable to hold data from the PiZero
int i_latitude; // variable to hold data from the PiZero
int i_altitude; // variable to hold data from the PiZero
byte hour_landing; // variable to hold data from the PiZero
byte minute_landing; // variable to hold data from the PiZero

int firstcomma; // variable used for string decoding
int secondcomma;  // variable used for string decoding
int thirdcomma; // variable used for string decoding
int first_semicol; // variable used for string decoding
int second_semicol; // variable used for string decoding
int T_time; // variable used for string decoding

static byte ndx = 0; // variable used for string decoding
char endMarker = '\n'; // variable used for string decoding
char rc; // variable used for string decoding
boolean newData = false; // variable used for string decoding


void initPin()
{
    //Initialize the LEDs and turn them all off
  pinMode(LED_RED, OUTPUT) ;
  pinMode(LED_GREEN, OUTPUT) ;
  pinMode(LED_BLUE, OUTPUT) ;
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);  
}

void red() 
{
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, HIGH);
  digitalWrite(LED_BLUE, HIGH);
}

void green() 
{
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_GREEN, LOW);
  digitalWrite(LED_BLUE, HIGH);
}

void blue() 
{
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_GREEN, HIGH);
  digitalWrite(LED_BLUE, LOW);
}

void white()
{
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, LOW);
  digitalWrite(LED_BLUE, LOW);
}

void orange()
{
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, LOW);
  digitalWrite(LED_BLUE, HIGH);
}

void setup()
{ 
  initPin();
     
  debugSerial.begin(57600);
  Serial.begin(9600); 

  while((!debugSerial) && (millis() < 5000));
  
  OrangeForRN2483.init();

  orange();

  LpwaOrangeEncoder.flush();

  bool res = 0;
  int attempt = 0;
  do 
  {
    blue();
    debugSerial.println("Join Request") ;

    OrangeForRN2483.setDataRate(DATA_RATE_3); // Set DataRate to SF12/125Khz
    
    
    res = OrangeForRN2483.joinNetwork(appEUI, appKey);
    debugSerial.println(res ? "Join Accepted." : "Join Failed! Trying again. Waiting 10 seconds.") ;
    OrangeForRN2483.enableAdr();
    
    if(!res)
    {
      red();
      attempt++;
      delay(10000);
    }
    
    if (attempt == 3) 
    {
      while(true) {
        red();
        delay(1000);
        orange();
        delay(1000);
        white();
        delay(1000);
        }
    }
  }while (res == 0);
  
  debugSerial.println("Sleeping for 5 seconds before starting sending packets.");
  sleep(5);  
}

void sleep(unsigned short count)
{
  for (uint8_t i = count; i > 0; i--)
  {
    debugSerial.println(i) ;
    delay(1000) ;
  }
}

void debugFrame(int8_t* frame, int8_t len)
{  
    if(frame != NULL)
    {
        debugSerial.print("Payload: ");
        int i = 0;
        for(i = 0; i < len; i++)
        {
            unsigned char value = (unsigned char)frame[i];
            debugSerial.print(value, HEX); debugSerial.print(" ");
        }
        debugSerial.print("\r\n");
    }
}


void sendRequest()
{ 
  white();
  
  
  
  int8_t len;
  int8_t* frame = LpwaOrangeEncoder.getFramePayload(&len);
  debugFrame(frame, len);
  int port = 9;
  bool res = OrangeForRN2483.sendMessage(frame, len, port);

  if(res)
  {
      debugSerial.println("Successful transmission.");
      green();
      
  //    delay(15000);
  DownlinkMessage* downlinkMessage = OrangeForRN2483.getDownlinkMessage();

        int port = downlinkMessage->getPort();
        String message = downlinkMessage->getMessage();
        int8_t len = 0;
        const int8_t* msgint= downlinkMessage->getMessageByteArray(&len);
        debugSerial.print("message brut :");
        debugSerial.println(message);
        
        if(len >= 2)
        {
          uint16_t value = ((uint16_t)(msgint[0] << 8)) | msgint[1];
          debugSerial.print("Receive value=");
          debugSerial.println(value, DEC);
        }
        else debugSerial.println("value not found");
        
        
                
        debugSerial.print("Port :");
        debugSerial.print(port);
        if(message == NULL) 
          debugSerial.println("Response with empty payload"); 
        else {
          debugSerial.print("Frame Payload :");
          debugSerial.println(message);
          unsigned int messagelen = message.length();
          
          debugSerial.print(messagelen);  
      white();        
  }
  }
  else
  {
    eErrorType errorType = OrangeForRN2483.getLastError();
    switch(errorType)
    {   
      case LORA_TIMEOUT:       
        debugSerial.println("Connection timed-out. Check your serial connection to the device! Sleeping for 20sec.");
        red();
        delay(20000) ;
      break ;
  
      case LORA_INVALID_DATA_LEN:
        debugSerial.println("The size of the payload is greater than allowed. Transmission failed!");
        red();
      break ;
       
      case LORA_BUSY:
        debugSerial.println("The device is busy. Sleeping for 10 extra seconds.");
        red();
        delay(10000) ;
      break ;
          
      default: 
        debugSerial.println("Other error");
        red();
        delay(10000) ;
      break ;
    }

      /*LpwaOrange.receive(&frameReceived, 200);
      
      debugSerial.print("received : ");
      debugSerial.println(frameReceived);
      */
  } 
}

void loop()
{
   

    while(true)
    {
      while (Serial.available() > 0 && newData == false) //checking if serial data are available from the PiZero
    {
        rc = Serial.read();

        if (rc != endMarker) {
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars) {
                ndx = numChars - 1;
            }
        }
        else {
            receivedChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            newData = true;
        }
    }
      
      showNewData();   
     }
     
   }

void showNewData() // function to handle data received from the PiZero & actions
{
    if (newData == true) {
        debugSerial.print("This just in ... ");
        debugSerial.println(receivedChars);
        Mystring = (String(receivedChars));
        firstcomma = Mystring.indexOf(',');
        secondcomma = Mystring.indexOf(',',firstcomma + 1);
        thirdcomma = Mystring.indexOf(',',secondcomma + 1);
        first_semicol = Mystring.indexOf(':');
        second_semicol = Mystring.indexOf(':',first_semicol + 2);
        T_time = Mystring.indexOf('T');
        debugSerial.print("firstcomma :");
        debugSerial.println(firstcomma);
        debugSerial.print("secondcomma :");
        debugSerial.println(secondcomma);
        debugSerial.print("thirdcomma :");
        debugSerial.println(thirdcomma);
        debugSerial.println("as a string");
        debugSerial.println(Mystring);
        
        
        i_latitude = (Mystring.substring(0,',').toInt());
        i_longitude = (Mystring.substring(firstcomma + 1,secondcomma).toInt());
        i_altitude = (Mystring.substring(secondcomma + 1,thirdcomma).toInt());
        hour_landing = (Mystring.substring(T_time + 1,first_semicol).toInt());
        minute_landing = (Mystring.substring(first_semicol + 1,second_semicol).toInt());
        frametype = String((Mystring.charAt(0))).toInt();
        
        debugSerial.print("frametype :");
        debugSerial.println(frametype);
        debugSerial.print("longitude extract :");
        debugSerial.println(i_longitude);
        debugSerial.print("latitude extract :");
        debugSerial.println(i_latitude);
        debugSerial.print("altitude extract :");
        debugSerial.println(i_altitude);
        debugSerial.print("estimated landing time :");
        debugSerial.print(hour_landing);
        debugSerial.print(":");
        debugSerial.println(minute_landing);
        
        LpwaOrangeEncoder.flush();
        LpwaOrangeEncoder.addByte(frametype);
        LpwaOrangeEncoder.addByte(hour_landing);
        LpwaOrangeEncoder.addByte(minute_landing);
        LpwaOrangeEncoder.addInt(i_latitude);
        LpwaOrangeEncoder.addInt(i_longitude);
        LpwaOrangeEncoder.addInt(i_altitude);

        int port_i = 7;
        int8_t len_i;
        int8_t* frame_i = LpwaOrangeEncoder.getFramePayload(&len_i);
        debugFrame(frame_i, len_i);

        OrangeForRN2483.sendMessage(frame_i, len_i, port_i);

        

        
        newData = false;
    }
}
    
