"""Get GPS position from SODAQ - Orange card

put current position into a file.
The latests position will tell the flight phase (ASCEND, FREEFALL)

Parameters
----------



Returns
-------

"""
import os
import sys
import time
import glob
import serial
import shutil
import logging
from configparser import ConfigParser, ExtendedInterpolation
import astropy.units as u
from astropy.time import Time
import sodaq

from missionplanner.utils import log


"""Main program: get the current position and save into output file.

"""
# get MissionPlanner configuration file from argument
try:
    config_file = sys.argv[1]
except:
    msg = 'syntax is: python3 get_position config_file'
    raise ValueError(msg)
if not os.path.exists(config_file):
    msg = '*** FATAL ERROR: config file '+config_file+' does not exist'
    raise IOError(msg)

# create ConfigParser Object
config = ConfigParser(interpolation=ExtendedInterpolation())
# read configuration file
config.read(config_file)
# set outut file (full) name
out_file = config['CAMPAIGN']['trajectory_rlt_file']
log.info('Output position file: '+out_file)

# Open serial port
log.debug('opening serial port /dev/ttyAMA0')
s = serial.Serial('/dev/ttyAMA0', 9600) # change name, if needed
log.debug('serial port open')


# enter loop
try:
    while True:
        # get latest position
        (time,lon,lat,alt) = sodaq.get_position_from_sodaq()
        
        # puts latest position into output file
        sodaq.put_latest_position_into_file(out_file,time,lon,lat,alt)
except KeyboardInterrupt:
    s.close()
except:
    msg = '*** FATAL ERROR: something went wrong...'
    log.error(msg)


