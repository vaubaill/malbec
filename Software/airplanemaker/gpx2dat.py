"""Tranform GPX data file into ascii dat file.


"""


import numpy as np
import argparse
import logging
import simplekml
from astropy.time import Time
import spiceypy as sp
from astropy.table import QTable
import astropy.units as u
from pygc import great_distance, great_circle
from zipfile import ZipFile
import xml.sax, xml.sax.handler
import gpxpy
import csv

from missionplanner.utils import log

def gpx2csv(gpx_file,csv_file):
    """Convert GPX file into trajectory file.
    
    Parameters
    ----------
    gpx_file : string
        input GPX file.
    csv_file : string
        output text file
    
    Returns
    -------
    None.
    
    """
    log.info('Read gpx file '+gpx_file)
    with open(csv_file, 'w',encoding="UTF-8") as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL,lineterminator="\n")
        with open(gpx_file, 'r') as gpxf:
            gpxo = gpxpy.parse(gpxf)
            for track in gpxo.tracks:
                for segment in track.segments:
                    for point in segment.points:
                        spamwriter.writerow([Time(point.time).isot,
                             point.latitude,
                             point.longitude,
                             point.elevation])
    log.info('csv data saved in '+csv_file)
    return

def gpx2traj(gpx_file,traj_file):
    """Convert GPX file into trajectory file.
    
    Parameters
    ----------
    gpx_file : string
        input GPX file.
    traj_file : string
        output trajectory text file
    
    Returns
    -------
    None.
    
    """
    first_point = True
    ids=0
    tlonlatalt = QTable(names=['Time','lon','lat','alt'],
                        dtype=['object','float','float','float'])
    log.info('Read gpx file '+gpx_file)
    with open(gpx_file, 'r') as gpxf:
        gpxo = gpxpy.parse(gpxf)
        for track in gpxo.tracks:
            for segment in track.segments:
                for point in segment.points:
                    if first_point:
                        idtab = 0
                    else:
                        idtab = -1
                    ids = ids + 1
                    tlonlatalt.insert_row(idtab,vals=[Time(point.time),
                                                      point.latitude,
                                                      point.longitude,
                                                      point.elevation])
    npts = len(tlonlatalt['lat'])
    log.info('There are '+str(npts)+' GPS points in '+gpx_file)
    # convert GPS data into trajectory
    log.info('Making trajectory from GPS measurements.')
    # set duration
    dur = np.roll(tlonlatalt['Time'],-1)-tlonlatalt['Time']
    dur[0] = 0.0
    dur.name = 'Dur'
    dur.unit = u.d
    dur.convert_unit_to(u.min)
    # set azimuth and distance
    azdist = great_distance(start_latitude=tlonlatalt['lat'].value,
                    start_longitude=tlonlatalt['lon'].value,
                    end_latitude=np.roll(tlonlatalt['lat'].value,-1),
                    end_longitude=np.roll(tlonlatalt['lon'].value,-1))
    azdist['azimuth'][0] = 0.0
    azdist['distance'][0] = 0.0
    # set total duration and total distance
    t0 = np.repeat(tlonlatalt['Time'][-1],npts)
    total_dur = tlonlatalt['Time'] - t0
    total_dur.name = 'totT'
    total_dur.unit = u.d
    total_dur.convert_unit_to(u.min)
    total_dist = np.cumsum(azdist['distance'])*u.m
    
    # arrange traj data
    traj = QTable()
    traj['Time'] = tlonlatalt['Time']
    traj['id'] = np.arange(npts)
    traj['Lat'] = tlonlatalt['lat']*u.deg
    traj['Lon'] = tlonlatalt['lon']*u.deg
    traj['Alt'] = tlonlatalt['alt']*u.m
    traj['az'] = azdist['azimuth']*u.deg
    traj['Dist'] = azdist['distance']*u.m
    traj['Dur'] = dur
    traj['totT'] = total_dur
    traj['totD']= total_dist  
    #set traj formats
    traj['Lon'].info.format = '14.10f'
    traj['Lat'].info.format = '14.10f'
    traj['Alt'].info.format = '7.1f'
    traj['az'].info.format = '7.3f'
    traj['Dist'].info.format = '7.1f'
    try:
        traj['Dur'].info.format = '7.3f'
        traj['totT'].info.format = '7.3f'
    except:
        msg = '*** WARNING: unable to set time format for Dur and toD columns'
        log.warning(msg)
    traj['totD'].info.format = '7.1f'   
    # save the trajectory into text file
    traj.write(traj_file,
                        format='ascii.fixed_width_two_line',delimiter=' ',
                        formats={'Time':'%23s','':''},overwrite=True)
    log.info('Trajectory file saved in '+traj_file)
    return


if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='gpx2dat arguments.')
    parser.add_argument('-i',help='input file.')
    parser.add_argument('-o',help='output file')
    args = parser.parse_args()
    
    #launch conversion
    gpx2csv(args.i,args.o)
    #gpx2traj(args.i,args.o)
    
else:
    log.debug('successfully imported')