""" Routine to create ck_files with SPICE for airplane.
    create_airplane_ ck.py
    
    
    _________
    INPUT/DEPENDENCIES:
    - SPICE toolkits: msopck (to convert text file containing orientation data to a CK)
    _________
    Output: ck-file
    - "Orientation data": .quaternions from which orientation matrices are formed by SPICE softtware
                          .matrices are used to rotate position vectors from a base reference frame
                           ("from" frame) into a second reference frame ("to" frame) -> C-Matrix
    
    Authors
    -------------
    A. Rietze - U. Oldenburg, Germany
    J. Vaubaillon - IMCCE
    The MALBEC project
    
    -------------
    Version
    1.0
    
    """
#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import os
import logging
import shutil
import spiceypy as sp
import astropy.units as u
from scipy.interpolate import interp1d
from astropy.table import QTable
from astropy.time import Time

from missionplanner import utils
from obsplanner.make_id import make_topoframe_name,make_nacelle_orientation_frame_id

from missionplanner.utils import log

home = os.getenv('HOME') + '/'

def get_airplane_spice_angles(time,azimuth_table,offset_az,offset_el):
    """Compute airplane orientation angles given SPICE conventions.
    
    Here are Boris Semenov (JPL) comment about how to do this:
    
    Given Euler_angle1 computed as LON by RECLAT and Euler_angle2 computed such that
    it's positive for a point above horizon, to point the X axis at the point interest,
    your airplane frame should be rotated from the topo frame
    first counter-clockwise (positive) by Euler_angle1 about Z,
    then clockwise (negative) by Euler_angle2 about Y.
    Assuming that you do not have the EULER_ROTATIONS_TYPE keyword in your setup,
    the MSOPCK input file order for this case could be
    
    time, -Euler_angle2, Euler_angle1, 0.0
    
    with the EULER_ROTATIONS_ORDER set to:
    
    EULER_ROTATIONS_ORDER  = 'Y', 'Z', 'X'
    
    With these inputs MSOPCK will create the rotation from the topo frame to the airplane frame
    
    Mtopo->airplane = [-Euler_angle2]y * [Euler_angle1]z * [0]x
    
    using this EUL2M call
    
    EUL2M ( -Euler_angle2, Euler_angle1, 0.0, 2, 3, 1, M )
    
    Note from reclat SPICE doc:
    LONG    Longitude of the input point.  This is the angle 
            between the prime meridian and the meridian
            containing the point.  The direction of increasing
            longitude is from the +X axis towards the +Y axis.
            LONG is output in radians.  The range of LONG is 
            [ -pi, pi].
    LAT     Latitude of the input point.  This is the angle from
            the XY plane of the ray from the origin through the
            point.
            LAT is output in radians.  The range of LAT is 
            [-pi/2, pi/2].
    
    Note: The airplane reference frame is defined as: 'X'=North, 'Y'=West, 'Z'=vertical
    
    As a consequence:
    LONG (here called relative_az) is the azimuth towards the WEST. The geographic azimuth is therefore -LONG.
    Note that the first angle is defined in the airplane1 frame: as a consequence there is no need
    to change it for the orientation of airplane1, but 180 deg must be added for the orientation of airplane2.
    Last but not least, counterclockwise rotation is considered in SPICE. So an azimuth offset of +10 deg
    results in SPICE in a rotation about Z-axis of -10 deg. Note that relative_az is already in SPICE orientation.
    
    Parameters
    ----------
    time : string
        Time stamp in isot format for which we want to compute the orientation.
    airplane_name: string
        airplane name.
    airplane_id : int
        airplane SPICE id.
    azimuth_table : astropy.table.QTable Object
        ATable of airplane azimuth heading as a function of SPICE time.
    meteor_height : astropy.units.Quantity object
        Meteor altitude above sea level, in [m] or [km].
    offset_az : astropy,coordinates.Angle object.
        Azimuth offset
    offset_az : astropy,coordinates.Angle object.
        Elevation offset
    
    Returns
    -------
    nac_spice_angle1 : astropy.coordinates.Angle object
        airplane azimuth angle in SPICE convention: positive counter clockwise.
        This is basically the opposite of the geographic azimuth.
    nac_spice_angle2 : astropy.coordinates.Angle object
        airplane elevation angle such that it is counted positive above horizon.
    
    """
    log.debug('Computing airplane angles at time '+time)
    et = sp.utc2et(time)
    # get the heading of the airplane at time 
    relative_az_func = interp1d(azimuth_table['et'],azimuth_table['az'])
    # set SPICE azimuth angle
    relative_az = relative_az_func(et)
    # define first Euler angle for camera pointing
    spice_angle1 = (-relative_az*u.deg + offset_az.to('deg'))%(360.0*u.deg)
    # set SPICE elevation angle
    spice_angle2 = - offset_el.to('deg')
    
    log.debug('spice_angle1: '+str(spice_angle1))
    log.debug('spice_angle2: '+str(spice_angle2))
    return spice_angle1,spice_angle2

# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_airplane_ck(spk_file,frame_file,azimuth_table,offset_az=0.0*u.deg,offset_el=0.0*u.deg,offset_roll=0.0*u.deg,meteor_height=100.0*u.km,out_ck_file=None,tmp_dir='/tmp/',freq=1*u.min,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',load_stdker=True,load_spker=True,load_frame=True):
    """Create ck (orientation) kernel files for an airplane.
    
    An airplane orientation points towards a fixed azimuth and elevation relative to the airplane,
    Azimuth and elevation offset are consideredm if set.
    
    The created ck file is first saved in the temporary directory due to SPICE character limitation reasons
    and is then copied into file specified by out_ck_file parameter.
    
    Note that this behaviour might cause some conflict if several instances of the program are run in parallel.
    This is why you can set the temporary directory thanks to the tmp_dir parameter.
    
    Parameters:
    -------------
    spk_file : string
        Aircraft SPICE spk full path file name.
    frame_file : string
        Aircraft SPICE frame full path file name (tf-file).
    azimuth_table : astropy.table.QTable Object
        ATable of airplane azimuth heading as a function of SPICE time.
    offset_az : astropy.units.Quantity object
        Azimuth offset angle. Default is 0.0*u.deg.
    offset_el : astropy.units.Quantity object
        Elevation offset angle. Default is 0.0*u.deg.
    offset_roll : astropy.units.Quantity object
        Roll offset angle. Default is 0.0*u.deg.
    out_ck_file: string, optional
        Full path output ck file. If not specified (default), the output ck file
        is saved in the same directory as spk_file and its name is the same as spk_file,
        except for the extension that is saved as '.ck'.
        Default is None (i.e. unspecified).
    freq: astropy.units.Quantity Object,optional
        frequency for which the kernel needs to be created. Default is 1 min.
    kernel_path : string, optional
        Path to SPICE standard kernel. Default is '/astrodata/kernels/'.
    spice_exe_path: String, optional
        SPICE executables directory full name.Default is '/astrodata/exe/'.
    tmp_dir : string
        temporary directory where files are created.
        This is useful if the path to the files are very long, since it causes the mkspk utility program to crash.
        Temporary files (e.g. trajectory in ascii format) and output files are first
        created in tmp_dir directory, and then the output is saved in spk_file location. Default is '/tmp/'.
    load_stdker : boolean, optional.
        If True, the SPICE standard kernel is loaded. Default is True.
    load_spker : boolean, optional.
        If True, the SPICE SPK kernel is loaded. Default is True.
    load_frame : boolean, optional.
        If True, the SPICE frame kernel is loaded. Default is True.
    
    Returns
    -------
    None.
    
    """
    # load SPICE standard kernel if needed
    if load_stdker:
        # load standard kernel
        std_kernel_file = kernel_path + 'standard.ker'
        utils.check_file(std_kernel_file,log)
        log.info('Load: '+std_kernel_file)
        sp.furnsh(std_kernel_file)
    
    # load airplane spk file if needed
    if load_spker:
        utils.check_file(spk_file,log)
        log.info('Load: '+spk_file)
        sp.furnsh(spk_file)
    
    # load frame file (tf-file)
    if load_frame:
        log.info('Load: '+frame_file)
        sp.furnsh(frame_file)
    
    # copy frame file to tmp_dir to avoid long string in SPICE setup file.
    shutil.copy(frame_file, tmp_dir + os.path.basename(frame_file))
    log.debug('Frame files '+frame_file+' copied under '+tmp_dir)
    
    
    # make output ck file name.
    if not out_ck_file:
        out_dir = os.path.dirname(spk_file) + '/'
        ck_file_name = os.path.basename(spk_file).replace('.spk','.ck')
        out_ck_file = tmp_dir + ck_file_name
    else:
        out_dir = os.path.dirname(out_ck_file) + '/'
        out_ck_file = tmp_dir + '/' + os.path.basename(out_ck_file)
    
    # make output temporary fake clock kernel file name
    tsc_file = out_ck_file.replace('.ck','_fake.tsc')
    # make temporary input data file name required to create the ck file.
    input_file = out_ck_file.replace('.ck','_input.dat')
    # make setup file name
    setup_file = out_ck_file.replace('.ck','.ck_setup')
    
    # logging the setup, ck, tsc and input file
    log.debug('ck_file1: ' + out_ck_file)
    log.debug('tsc_file: ' + tsc_file)
    log.debug('input_file: ' + input_file)
    log.debug('setup_file1: ' + setup_file)
    
    # erase previously created files
    for file2rm in [out_ck_file,tsc_file,input_file,setup_file]:
        if os.path.exists(file2rm):
            log.debug('removing existing file: '+file2rm)
            os.remove(file2rm)
    
    # get airplane SPICE names, id and spk coverage from spk file
    airplane_id = sp.spkobj(spk_file)[0]
    airplane_name = sp.bodc2n(airplane_id)
    cov = sp.spkcov(spk_file,airplane_id)
    start,stop = sp.wnfetd(cov,0)
    
    sm_azimuth_table = azimuth_table
    
    # log
    log.info("start: "+sp.et2utc(start,"ISOC",2,50))
    log.info("stop: "+sp.et2utc(stop,"ISOC",2,50))
    
    # make input_file input files.
    out = open(input_file,'w')
    # create input data
    input_data = QTable(names=['Time','pitch','roll','yaw'],
                        dtype=['str','float','float','float'])
    # compute orientation of cameras as a function of time
    for (time,et) in zip(sm_azimuth_table['Time'],sm_azimuth_table['et']):
        # get Euler angles
        try:
            timestr = time.isot
        except:
            timestr = time
        log.debug('Gettig Euler angle at time: '+timestr)
        (spice_angle1,spice_angle2) = get_airplane_spice_angles(timestr,
                                                                sm_azimuth_table,
                                                                offset_az,
                                                                offset_el)
        # store Euler angles
        input_data.insert_row(len(input_data),vals=[timestr,0.0,spice_angle2.to('deg').value,spice_angle1.to('deg').value])
        # write Euler angle into file
        out.write(timestr+' '+str( offset_roll.to('deg').value)+' '+str(spice_angle2.to('deg').value)+' '+str(spice_angle1.to('deg').value)+"\n")    
    # close ck input files
    out.close()
    log.info("input data saved in:  "+input_file)
    # now save the attitude in data file
    attit_file = input_file.replace('input.dat','attitude.dat')
    attit_data = QTable()
    attit_data['Time'] = input_data['Time'] 
    attit_data['az'] = (-input_data['yaw'])%(360.0)
    attit_data['el'] = -input_data['roll']
    attit_data['az'].info.format = '7.3f'
    attit_data['el'].info.format = '7.3f'
    attit_data.write(attit_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
    log.info('Attitude data saved in '+attit_file)
    
    # make the (intermediate) setup file
    with open(setup_file,'w') as out:
        out.write("\\begindata \n")
        # setup data for testing with airplane data
        # -------------------------------------------------------------------------------
        out.write("   LSK_FILE_NAME    = "+repr(kernel_path+'naif0012.tls')+"\n")
        out.write("   MAKE_FAKE_SCLK   = "+repr(tsc_file)+"\n")
        out.write("   FRAMES_FILE_NAME = "+repr(tmp_dir + os.path.basename(frame_file))+"\n")
        out.write("   INTERNAL_FILE_NAME     = " + repr('CK file for ' + str(airplane_name)) + "\n")
        out.write("   CK_TYPE                = 3 \n")
        out.write("   CK_SEGMENT_ID          = "+repr('CK file for ' + str(airplane_name))+"\n")
        out.write("   INSTRUMENT_ID          = "+str(make_nacelle_orientation_frame_id(airplane_id))+"\n")
        out.write("   REFERENCE_FRAME_NAME   = "+repr(make_topoframe_name(airplane_name))+"\n")
        out.write("   ANGULAR_RATE_PRESENT   = "+repr('NO')+"\n")
        out.write("   MAXIMUM_VALID_INTERVAL = "+str(freq.to('s').value+10.0)+"\n")
        out.write("   INPUT_TIME_TYPE        = "+repr('UTC')+"\n")
        out.write("   INPUT_DATA_TYPE        = "+repr('EULER ANGLES')+"\n")
        out.write("   EULER_ANGLE_UNITS      = "+repr('DEGREES')+"\n")
        out.write("   EULER_ROTATIONS_ORDER  = "+repr('X')+","+repr('Y')+"," +repr('Z') +"\n")
        out.write("   ANGULAR_RATE_FRAME     = "+repr('REFERENCE')+"\n") #" or 'INSTRUMENT'
        out.write("   PRODUCER_ID            = "+repr('J. Vaubaillon - IMCCE, A. Rietze - U. Oldenburg')+"\n")
        out.write(" \n")
    log.info("setup saved in:  "+setup_file)
    
    # build the command
    cmd = spice_exe_path + "msopck " + setup_file + " " + input_file + " " + out_ck_file
    log.info("cmd="+cmd)
    
    # launches the cmd
    try:
        os.system(cmd)
    except Exception:
        msg = '*** FATAL ERROR: Impossible to submit the cmd' +cmd
        log.error(msg)
        raise IOError(msg)
    
    # double check that CK file was successfully created
    utils.check_file(out_ck_file,log)
    
    # now copy temporary output files (ck, tsc, setup) into out_dir
    for file2move in [setup_file,out_ck_file,tsc_file,input_file,attit_file]:
        dst = out_dir + os.path.basename(file2move)
        if os.path.exists(dst):
            os.remove(dst)
        try:
            # remark: os.remove does not always work when trying to move file from one File System to another...
            log.debug('Moving '+file2move+' into '+dst)
            shutil.copy(file2move, dst)
        except:
            msg = '*** FATAL ERROR: impossible to move ' + file2move + ' into ' + dst
            log.error(msg)
            raise IOError(msg)
    log.info('SPICE CK kernel saved in: ' + out_dir + os.path.basename(out_ck_file))
    
    results = (out_dir+os.path.basename(out_ck_file),
               out_dir+os.path.basename(tsc_file),
               cov)
    
    return results

# =============================================================================
if __name__ == '__main__':
    log.debug('To be run from obsplanner.')
else:
    log.debug('successfully imported')
