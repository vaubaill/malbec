"""Airplanemaker

Author
------
J. Vaubaillon, - IMCCE, 2022

Version
-------
1.0



"""
import os
import shutil
import argparse
import logging
import simplekml
import numpy as np
from configparser import ConfigParser, ExtendedInterpolation
# astropy
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import EarthLocation, AltAz, SkyCoord, get_moon, get_sun
import spiceypy as sp

from pygc import great_distance, great_circle
from zipfile import ZipFile
import xml.sax, xml.sax.handler
import gpxpy


# some missionplanner tools
from missionplanner import utils
from missionplanner.utils import log
from obsplanner.make_id import make_camera_name,make_camera_id,make_nacelle_orientation_frame_id,make_nacelle_orientation_frame_name
from obsplanner.create_tf import create_tf
from obsplanner.create_ik import create_ik
from obsplanner.create_tk import create_tk
from obsplanner import obsutils

from create_airplane_ck import create_airplane_ck

from reduction.filter import Filter
from reduction.lens import Lens
from reduction.camera import Camera
from showersim.meteorshower import MeteorShower
from showersim.shower_radiant import Shower_Radiant
from showersim import shower_utils, fakeor, anchor, afm, phot, fakeobs

###################################################
# 
# DEFINITION OF THE MissionPlanner OBJECT
# 
###################################################
# create default logger object
# log = logging.getLogger(__name__)

"""PlacemarkHandler Object definition.

Usefull to read kmz file.


"""
class PlacemarkHandler(xml.sax.handler.ContentHandler):
    def __init__(self):
        self.inName = False # handle XML parser events
        self.inPlacemark = False
        self.mapping = {}
        self.buffer = ""
        self.name_tag = ""
       
    def startElement(self, name, attributes):
        if name == "Placemark": # on start Placemark tag
            self.inPlacemark = True
            self.buffer = ""
        if self.inPlacemark:
            if name == "name": # on start title tag
                self.inName = True # save name text to follow
           
    def characters(self, data):
        if self.inPlacemark: # on text within tag
            self.buffer += data # save text if in title
           
    def endElement(self, name):
        self.buffer = self.buffer.strip('\n\t')
       
        if name == "Placemark":
            self.inPlacemark = False
            self.name_tag = "" #clear current name
       
        elif name == "name" and self.inPlacemark:
            self.inName = False # on end title tag           
            self.name_tag = self.buffer.strip()
            self.mapping[self.name_tag] = {}
        elif self.inPlacemark:
            if name in self.mapping[self.name_tag]:
                self.mapping[self.name_tag][name] += self.buffer
            else:
                self.mapping[self.name_tag][name] = self.buffer
        self.buffer = ""


class Airplane(object):
    """Definition of an Airplane object.
    
    Parameters
    ----------
    
    TBF
    
    
    """
    # set default parameters
    __version__='1.0'
    log_set = False
    config = False
    npts = 0
    eloc = None
    Vloc = None
    tf_file = None
    traj = None
    
    def __init__(self, config_file=None):
        """Initialize Airplane Object.
        
        Parameters
        ----------
        config_file : string
            full name of configuration parameters file. Default is None.
        
        Returns
        -------
        None
        
        """
        # creates configparser.ConfigParser() object for the configuration file
        self.config = ConfigParser(interpolation=ExtendedInterpolation())
        # if set, read configuration file
        if config_file:
            self.set_and_read_config(config_file)
        
        # read waypoints flight data
        self.read_waypoints(self.config['FLIGHT']['wypnt_file'])
        # create airplane SPICE SPK file
        self.traj2spk()
        # create fake missionplanner configuration file for bosplanner tool
        self.make_fake_missplan_conf()
        # make Camera object
        self.make_camera()
        # cmake all SPICE kernels
        self.make_all_kernels()
        # compute FOV as a function of time
        self.compute_all_fov()
        return
    
    
    def set_and_read_config(self,config_file):
        """Set and read the configuration file.
        
        Parameters
        ----------
        config_file : string, optional
            full name of configuration parameters file.
        
        Returns
        -------
        None.
        
        """
        # set config_file.
        self.config_file = config_file
        # test if config_file file exists.
        utils.check_file(self.config_file,log)
        # read the configuration file BEFORE setting the logger since log file name is in config file.
        log.info('Read configuration file '+config_file)
        self.config.read(self.config_file)
        # set the home variable
        self.config['USER']['home'] = os.getenv('HOME')
        
        # set logger object BEFORE checking the configuration file so log messages can be output.
        logdict = {'DEBUG' : logging.DEBUG,
                   'INFO' : logging.INFO,
                   'WARNING' : logging.WARNING,
                   'ERROR' : logging.ERROR}
        if not self.log_set:
            try:
                log_level = self.config['USER']['log_level']
                self.set_log(level=logdict[log_level])
                log.info('Log level set to '+log_level)
            except:
                self.set_log(level=logging.DEBUG)
        
        # save SPICE paths into global variable
        self.kernel_path = self.config['USER']['kernel_path']+'/'
        self.spice_exe_path = self.config['USER']['spice_exe_path']+'/'
        # set some variables
        self.out_dir = self.config['CAMPAIGN']['out_dir']+'/'
        self.tmp_dir = self.config['USER']['tmp_dir']
        self.start = Time(self.config['FLIGHT']['start'],format='isot')
        self.stop = Time(self.config['FLIGHT']['stop'],format='isot')
        self.name = self.config['PLANE']['name']
        self.airplane_id = int(self.config['PLANE']['spice_id'])
        self.freq = float(self.config['CAMPAIGN']['freq'])*u.min
        self.cruise_speed = float(self.config['PLANE']['speed'])*u.km/u.hr
        self.cruise_alt = float(self.config['PLANE']['alt'])*u.m
        self.sim_id = self.config['CAMPAIGN']['name']+'_'+self.name+'_'+self.config['CAMERA']['az_off']+'_'+self.config['CAMERA']['roll_off']+'_'+self.config['CAMERA']['camera_name']+'_'+self.config['CAMERA']['lens_name']
        # set the frequency at which the orientation is to be created
        self.freq = float(self.config['CAMPAIGN']['freq'])*u.min
        self.meteor_alt = float(self.config['SHOWER']['meteor_alt'])*u.km
        # retrieve the camera orientation wrt airplane from config file
        self.offset_az = float(self.config['CAMERA']['az_off'])*u.deg
        self.offset_el = float(self.config['CAMERA']['el_off'])*u.deg
        self.offset_roll = float(self.config['CAMERA']['roll_off'])*u.deg
        # set read_res parameter: useful to read results if the simulation has already been performed.
        try:
            self.read_res = self.config['USER']['read_res'].lower() in ['true', '1', 't', 'y', 'yes']
        except:
            self.read_res = False
        # make a Radiant object
        try:
            self.radiant = Shower_Radiant(ra=float(self.config['SHOWER']['radiant_ra'])*u.deg,
                                         dec=float(self.config['SHOWER']['radiant_dec'])*u.deg,
                                         time_max=Time(self.config['SHOWER']['max_time'],scale='utc'),
                                         dra=float(self.config['SHOWER']['radiant_drift_ra'])*u.deg/u.d,
                                         ddec=float(self.config['SHOWER']['radiant_drift_dec'])*u.deg/u.d)
        except:
            log.warning('no radiant info in config file: '+self.config_file+'. Radiant will be created from shower catalogs.')
        # make a MeteorShower Object
        try:
            self.shower = MeteorShower(self.config['SHOWER']['shower_name'],
                                       float(self.config['SHOWER']['shower_V'])*u.km/u.s,
                                       float(self.config['SHOWER']['shower_r']),
                                       radiant=self.radiant,
                                       time_max=Time(self.config['SHOWER']['max_time'],scale='utc'),
                                       ZHR=float(self.config['SHOWER']['zhr_expected'])/u.hr,
                                       fwhm=float(self.config['SHOWER']['shower_fwhm'])*u.hr,
                                       leap_sec_ker=self.kernel_path+'naif0012.tls')
            self.shower.info()
        except:
            msg = os.getcwd()+' *** FATAL ERROR: no shower info in config file: '+self.config_file
            log.error(msg)
            raise ValueError(msg)
        
        # create data QTables
        self.fov_data = obsutils.make_fov_table()
        self.geom_data = obsutils.make_single_geometry_output_table()
        # set fov kml file name
        self.fov_kml_file = self.config['CAMPAIGN']['fov_kml_file']
        # set FOV data file name
        self.fov_dat_file = self.fov_kml_file.replace('.kml','.dat')
        # set geometry data file name
        self.geom_dat_file = self.fov_kml_file.replace('.kml','-geom.dat')
        
        return
    
    def set_log(self,level=logging.INFO,nosdlr=False,nohdlr=False):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG
        nosdlr : boolean, optional
            If True, no stream handler is created. Default is False.
        nohdlr  : boolean, optional
            If True, no file handler is created. Default is False.
        
        Returns
        -------
        None.
        
        
        """
        # set log level
        log.setLevel(level)
        log.propagate = True
        
        # log format
        log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
        
        # create StreamHandler
        sdlr = logging.StreamHandler()
        sdlr.setLevel(level)
        sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
        
        # create FileHandler
        strt = Time.now().isot.replace('-','').replace(':','').split('.')[0]
        self.log_file = self.config['CAMPAIGN']['log_file_root'].replace('YYYYMMDDTHHMMSS',strt)
        hdlr = logging.FileHandler(self.log_file)
        hdlr.setLevel(level)
        hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
            
        # if LOG directory does not exist make one
        if not os.path.isdir(self.config['USER']['log_dir']):
            os.makedirs(self.config['USER']['log_dir'])
        
        # set the log_set parameter
        self.log_set = True
        
        # log the log file name
        log.info('Log file: '+self.log_file)
        
        return
    
    def read_waypoints(self,wypnt_file):
        """Read file containing waypoints.
        
        Parameters
        ----------
        wypnt_file : string
        Waypoint files, in either dat, txt or kmz or gpx format.
        
        Returns
        ------
        npts : int
            Number of waypoints.
        wypnt : astropy.table.QTable object.
            Waypoints coordinates.
        
        """
        utils.check_file(wypnt_file,log)
        ext = wypnt_file.split('.')[-1]
        log.info('ext='+ext)
        cases = {
            'kmz': lambda: self.read_kmz_waypoints(wypnt_file),
            'txt': lambda: self.read_txt_waypoints(wypnt_file),
            'dat': lambda: self.read_dat_waypoints(wypnt_file),
            'csv': lambda: self.read_csv_waypoints(wypnt_file),
            'gpx': lambda: self.read_gpx_waypoints(wypnt_file),
        }
        # test file format
        try:
            cases[ext]
        except:
            msg = '*** waypoints file format unknown: '+wypnt_file
            log.error(msg)
            raise ValueError(msg)
        # launches read waypoints or GPS data
        cases[ext]()
        return
    
    def read_txt_waypoints(self,txt_file):
        """Read text waypoints file.
        
        Parameters
        ----------
        txt_file : string
            Text file containing the waypoints.
        
        Returns
        -------
        npts : int
            Number of waypoints.
        wypnt : astropy.table.QTable object.
            Waypoints coordinates.
        
        """
        log.info('Read waypoints file '+txt_file)
        self.wypnt = QTable.read(txt_file,format='ascii',delimiter=',',
                                 names=['Lat','Lon','Alt'],
                                 units=['deg','deg','m'])
        # re-arrange data
        self.npts = len(self.ypnt['Lat'])
        log.info('There are '+str(self.npts)+' waypoints in '+txt_file)
        # make trajectory
        self.make_trajectory_from_waypoints()
        return
     
    def read_kmz_waypoints(self,kmz_file):
        """Read kmz waypoints file.
        
        Parameters
        ----------
        kmz_file : string
            Googlearth kmz file containing the waypoints.
        
        Returns
        -------
        None. Variable are sved in global variable.
        
        
        """
        log.info('Read waypoints file '+kmz_file)
        kmz = ZipFile(kmz_file, 'r')
        kml = kmz.open(kmz.filelist[0].filename, 'r').read()
        kml = kmz.open('doc.kml', 'r')
        parser = xml.sax.make_parser()
        handler = PlacemarkHandler()
        parser.setContentHandler(handler)
        parser.parse(kml)
        kmz.close()
        # find the coordinates
        coo_key = None
        for map_key in handler.mapping:
            for key in map_key:
                try:
                    handler.mapping[map_key]['coordinates']
                    coo_key = map_key
                except:
                    pass
        # retrieve the coordinates
        try:
            data = [float(i) for i in handler.mapping[coo_key]['coordinates'].replace(' ',',').split(',')[:-1]]
        except:
            raise ValueError('Impossible to read coordinates in '+kmz_file)
        # re-arrange data
        self.npts = int(len(data)/3)
        log.info('There are '+str(self.npts)+' waypoints in '+kmz_file)
        data = np.reshape(data,(self.npts,3))
        # set altitude
        data[:,2] = np.repeat(self.cruise_alt.to('m').value,self.npts)
        self.wypnt = QTable(data,names=['Lon','Lat','Alt'],units=['deg','deg','m'])
        # make trajectory
        self.make_trajectory_from_waypoints()
        return
    
    def read_gpx_waypoints(self,gpx_file):
        """Read GPS measured data from gpx waypoints file.
        
        Parameters
        ----------
        gpx_file : string
            Gpx file containing the GPS measurements.
        
        Returns
        -------
        npts : int
            Number of waypoints.
        wypnt : astropy.table.QTable object.
            Waypoints coordinates.
        
        """
        first_point = True
        ids=0
        tlonlatalt = QTable(names=['Time','lon','lat','alt'],
                            dtype=['object','float','float','float'])
        log.info('Read gpx file '+gpx_file)
        with open(gpx_file, 'r') as gpxf:
            gpxo = gpxpy.parse(gpxf)
            for track in gpxo.tracks:
                for segment in track.segments:
                    for point in segment.points:
                        if first_point:
                            idtab = 0
                        else:
                            idtab = -1
                        ids = ids + 1
                        tlonlatalt.insert_row(idtab,vals=[Time(point.time),
                                                          point.latitude,
                                                          point.longitude,
                                                          point.elevation])
        self.npts = len(tlonlatalt['lat'])
        log.info('There are '+str(self.npts)+' GPS points in '+gpx_file)
        # convert GPS data into trajectory
        self.make_trajectory_from_gps(tlonlatalt)
        return
    
    def read_dat_waypoints(self,dat_file):
        """Read GPS measured data from data waypoints file.
        
        Parameters
        ----------
        dat_file : string
            Data text file containing the GPS measurements.
        
        Returns
        -------
        npts : int
            Number of waypoints.
        wypnt : astropy.table.QTable object.
            Waypoints coordinates.
        
        """
        tlonlatalt = QTable(names=['Time','lon','lat','alt'],
                            dtype=['object','float','float','float'])
        log.info('Read data file '+dat_file)            
        gps_data = QTable.read(dat_file,format='ascii.csv',delimiter=',',
                           names=['Time','lat','lon','alt'])
        for data in gps_data[::10]:
            tlonlatalt.insert_row(len(tlonlatalt),
                                  vals=[Time(data['Time'],format='iso',scale='utc'),
                                  data['lon'],data['lat'],data['alt']])
        self.npts = len(tlonlatalt['lat'])
        log.info('There are '+str(self.npts)+' GPS points in '+dat_file)
        # convert GPS data into trajectory
        self.make_trajectory_from_gps(tlonlatalt)
        return
        
    def read_csv_waypoints(self,csv_file):
        """Read GPS measured data from csv waypoints file.
        
        Parameters
        ----------
        csv_file : string
            Data text file containing the GPS measurements.
        
        Returns
        -------
        npts : int
            Number of waypoints.
        wypnt : astropy.table.QTable object.
            Waypoints coordinates.
        
        """
        tlonlatalt = QTable(names=['Time','lon','lat','alt'],
                            dtype=['object','float','float','float'])
        log.info('Read csv file '+csv_file)            
        gps_data = QTable.read(csv_file,format='ascii.csv',delimiter=',',
                           names=['Time','lat','lon','alt'])
        for data in gps_data[::10]:
            tlonlatalt.insert_row(len(tlonlatalt),
                                  vals=[Time(data['Time'],format='isot',scale='utc'),
                                  data['lon'],data['lat'],data['alt']])
        self.npts = len(tlonlatalt['lat'])
        log.info('There are '+str(self.npts)+' GPS points in '+csv_file)
        # convert GPS data into trajectory
        self.make_trajectory_from_gps(tlonlatalt)
        return
        
    def create_trajectory_table(self,create=True):
        """Create or set trajectory self.traj QTable
        
        Parameters
        ----------
        create : Boolean, optional
            If True, the QTable is created.
            Otherwise units and formats only are set.
            Default is True.
        
        Returns
        -------
        None.
        
        """
        # create trajectory QTable
        names = ['Time','id','Lon','Lat','Alt','az','Dist','Dur','totT','totD']
        types = ['object','int','float','float','float','float','float','float','float','float']
        if create:
            self.traj = QTable(names=names,dtype=types)
        self.traj['Lon'].unit = u.deg
        self.traj['Lat'].unit = u.deg
        self.traj['Alt'].unit = u.m
        self.traj['az'].unit = u.deg
        self.traj['Dist'].unit = u.km
        self.traj['Dur'].unit = u.min
        self.traj['totT'].unit = u.min
        self.traj['totD'].unit = u.km
        return
    
    def set_trajectory_formats(self):
        """Set trajectory QTable formats.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        """
        #self.traj['id'].info.format = 'i3'
        self.traj['Lon'].info.format = '14.10f'
        self.traj['Lat'].info.format = '14.10f'
        self.traj['Alt'].info.format = '7.1f'
        self.traj['az'].info.format = '7.3f'
        self.traj['Dist'].info.format = '7.1f'
        try:
            self.traj['Dur'].info.format = '7.3f'
            self.traj['totT'].info.format = '7.3f'
        except:
            msg = '*** WARNING: unable to set time format for Dur and toD columns'
            log.warning(msg)
        self.traj['totD'].info.format = '7.1f'
        return
    
    def make_trajectory_from_gps(self,tlonlatalt):
        """Make trajectory from GPS measurements.
        
        The trajectory is saved in an automacally generated file.
        
        Parameters
        ----------
        tlonlatalt : astropy.table.QTable
            Table of time, longitude, latitude, altitude.
        
        Returns
        -------
        None.
        
        """
        log.info('Making trajectory from GPS measurements.')
        # set duration
        dur = np.roll(tlonlatalt['Time'],-1)-tlonlatalt['Time']
        dur[0] = 0.0
        dur.name = 'Dur'
        dur.unit = u.d
        dur.convert_unit_to(u.min)
        # set azimuth and distance
        azdist = great_distance(start_latitude=tlonlatalt['lat'].value,
                        start_longitude=tlonlatalt['lon'].value,
                        end_latitude=np.roll(tlonlatalt['lat'].value,-1),
                        end_longitude=np.roll(tlonlatalt['lon'].value,-1))
        azdist['azimuth'][0] = 0.0
        azdist['distance'][0] = 0.0
        # set total duration and total distance
        t0 = np.repeat(tlonlatalt['Time'][-1],self.npts)
        total_dur = tlonlatalt['Time'] - t0
        total_dur.name = 'totT'
        total_dur.unit = u.d
        total_dur.convert_unit_to(u.min)
        total_dist = np.cumsum(azdist['distance'])*u.m
        
        # arrange traj data
        self.traj = QTable()
        self.traj['Time'] = tlonlatalt['Time']
        self.traj['id'] = np.arange(self.npts)
        self.traj['Lat'] = tlonlatalt['lat']*u.deg
        self.traj['Lon'] = tlonlatalt['lon']*u.deg
        self.traj['Alt'] = tlonlatalt['alt']*u.m
        self.traj['az'] = azdist['azimuth']*u.deg
        self.traj['Dist'] = azdist['distance']*u.m
        self.traj['Dur'] = dur
        self.traj['totT'] = total_dur
        self.traj['totD']= total_dist     
        # save the trajectory into text file
        self.traj_save()
        return
    
    def make_trajectory_from_waypoints(self):
        """"Make trajectory from waypoints.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('Making trajectory from expected waypoints.')
        # initializations
        self.create_trajectory_table()
        total_dist = 0.0*u.km
        total_dur = 0.0*u.min
        first_point = True
        stop_flight = False
        flight_duration = (self.stop - self.start).to('min')
        freqdist = self.cruise_speed * self.freq.to('h')
        time_epsilon = 0.01 *u.s
        time_current = self.start - time_epsilon
        log.info('flight_duration: '+str(flight_duration.to('min')))
        
        # loop over waypoints
        for (wypnt1,wypnt2,ids) in zip(self.wypnt,np.roll(self.wypnt,-1),np.arange(1,self.npts)):
            log.info('==================== Waypoint: '+str(ids))
            time_current = time_current + time_epsilon
            lat1 = wypnt1['Lat'].to('deg').value
            lon1 = wypnt1['Lon'].to('deg').value
            azdist = great_distance(start_latitude=lat1,start_longitude=lon1,
                                      end_latitude=wypnt2['Lat'], end_longitude=wypnt2['Lon'])
            azimuth = azdist['azimuth']*u.deg
            dist = azdist['distance']*u.m
            duration = dist.to('km') / self.cruise_speed.to('km/s')
            # cut the trajectory into small chunks of freqdist distance
            pntdist = int(dist.to('km') / freqdist.to('km'))
            if pntdist==0:
                alldist = np.array([dist.to('km').value])*u.km
                alltime = [time_current + duration.to('min')]
                alldur = np.array([duration.to('min').value])*u.min
                log.warning('pntdist=0')
                log.warning('alldist = '+str(alldist))
                log.warning('alltime = '+str(alltime))
                log.warning('alldur = '+str(alldur))
            else:
                allpnt = np.arange(pntdist)
                alldist = freqdist*allpnt
                lstdist = dist.to('km')
                alldist = np.append(alldist,lstdist)
                alldur = np.repeat(self.freq.to('min'),pntdist)
                lstdur = duration - pntdist*self.freq.to('min')
                alldur = np.append(alldur,lstdur)
                alltime = time_current + (np.cumsum(alldur.to('min').value)-1)*u.min
                # change last time to avoid having 2 positions/orientations at the same time for the next waypoint
                alltime[-1] = alltime[-1]-time_epsilon
                log.debug(np.cumsum(alldur.to('min').value)-1)
                log.debug(alltime)
                log.debug(allpnt)
            
            # create all points ont he surface of the Earth
            allpnts = great_circle(distance=alldist.to('m').value,
                         azimuth=azimuth.to('deg').value,
                         latitude=lat1,
                         longitude=lon1)
            log.info('azimuth = '+str(azimuth))
            log.info('duration = '+str(duration.to('min')))
            log.info('dist '+str(dist))
            log.info('freqdist='+str(freqdist))
            log.info('pntdist = '+str(pntdist))
            log.debug('alldist = '+str(alldist))
            log.debug('alltime = '+str(alltime))
            log.debug('alldur = '+str(alldur))
            log.debug('allpnts = '+str(allpnts))
            for (lon,lat,dst,tme,dur) in zip(allpnts['longitude'],
                                            allpnts['latitude'],
                                            alldist,
                                            alltime,
                                            alldur):
                log.debug('==========')
                log.debug('lon='+str(lon))
                log.debug('lat='+str(lat))
                log.debug('dst='+str(dst))
                log.debug('tme='+str(tme))
                log.debug('dur='+str(dur))
                log.debug('total_dur='+str(total_dur))
                total_dist = total_dist + dst.to('km')
                total_dur = total_dur + dur.to('min')
                time_current = tme
                if first_point:
                    idtab = 0
                    first_point = False
                else:
                    idtab = -1
                self.traj.insert_row(idtab,vals=[time_current.isot,ids,
                                  lon*u.deg,
                                  lat*u.deg,
                                  wypnt1['Alt'].to('m'),
                                 azimuth.to('deg'),
                                 dst.to('km'),
                                 dur.to('min'),
                                 total_dur.to('min'),
                                 total_dist.to('km')])
                if total_dur > (flight_duration+2.0*self.freq.to('min')):
                    stop_flight = True
                    break
            if stop_flight:
                log.warning('End of flight reached')
                break
                
        # save the trajectory
        self.traj.sort('Time')
        self.traj.remove_row(-1)
        self.npts = self.npts-1
        self.traj_save()
        return
    
    def traj_save(self,traj_file=None):
        """Save trajectory into data text file.
        
        Parameters
        ----------
        traj_file : string, optional
            Trajectory data text output file. Default is None meaning it is
            automatically derived from the configuration file.
        
        Returns
        -------
        None.
        
        """
        # set output trajectory file name
        if traj_file:
            self.traj_file = traj_file
        else:
            self.traj_file = self.config['FLIGHT']['wypnt_file'].replace('.kmz','.dat').replace('.dat','-traj.dat').replace('.csv','-traj.dat')
        # set QTable formats
        self.set_trajectory_formats()
        # save trajectory into self.traj_file
        self.traj.write(self.traj_file,
                        format='ascii.fixed_width_two_line',delimiter=' ',
                        formats={'Time':'%23s','':''},overwrite=True)
        log.info('Trajectory file saved in '+self.traj_file)
        return
    
    def compute_eloc(self):
        """Compute astropy.coordinates.EarthLocation from trajectory data.
        
        This is helpful in particular for the conversion to SPICE spk file.
        Note that the self.traj table must be filled in.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        if not self.npts:
            msg = 'WARNING: there is no data in Trajectory.'
            log.error(msg)
            raise ValueError(msg)
        # convert lon,lat into EarthLocation
        self.eloc = EarthLocation.from_geodetic(self.traj['Lon'], self.traj['Lat'], height=self.traj['Alt'])
        # add eloc to trajectory
        self.traj.add_column(self.eloc.x.to('km'),name='X')
        self.traj.add_column(self.eloc.y.to('km'),name='Y')
        self.traj.add_column(self.eloc.z.to('km'),name='Z')
        # for completion purpose, also computes the velocity Vloc
        self.V = self.compute_Vloc()
        self.Vloc_x,self.Vloc_y,self.Vloc_z = self.V
        # add Vloc to trajectory
        self.traj.add_column(self.Vloc_x.to('km/h'),name='VX')
        self.traj.add_column(self.Vloc_y.to('km/h'),name='VY')
        self.traj.add_column(self.Vloc_z.to('km/h'),name='VZ')
        self.traj.add_column(np.linalg.norm(self.V),name='V')
        # save again the trajectory data
        self.traj['VX'].info.format = '7.1f'
        self.traj['VY'].info.format = '7.1f'
        self.traj['VZ'].info.format = '7.1f'
        self.traj['V'].info.format = '7.1f'
        self.traj.write(self.traj_file,
                        format='ascii.fixed_width_two_line',delimiter=' ',
                        formats={'Time':'%23s','':''},overwrite=True)
        log.info('Trajectory file saved in '+self.traj_file)
        return
    
    def compute_Vloc(self):
        """Computes cartesian velocity from EarthLocation coordinates.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        Vloc : list of 3-astropy.units.quantity.Quantity Object.
            Cartesian Velocity from astropy.coordinates.EarthLocation object.
        
        """
        # copy time and position into local array
        posX = self.eloc.x.to('km').value
        posY = self.eloc.y.to('km').value
        posZ = self.eloc.z.to('km').value
        # compute position at previous time
        preX = (np.roll(posX,-1))
        preY = (np.roll(posY,-1))
        preZ = (np.roll(posZ,-1))
        # compute relative position
        p_relX = posX - preX
        p_relY = posY - preY
        p_relZ = posZ - preZ
        # compute cartesian velocity
        uvec = np.array([p_relX,p_relY,p_relZ]) / np.linalg.norm(np.array([p_relX,p_relY,p_relZ]))
        VlocX = uvec[0] * self.cruise_speed.to('km/h')
        VlocY = uvec[1] * self.cruise_speed.to('km/h')
        VlocZ = uvec[2] * self.cruise_speed.to('km/h')
        V = np.array([VlocX,VlocY,VlocZ])*u.km/u.hr
        return V
    
    
    def traj2spk(self,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',tmpdir='/tmp/'):
        """Convert trajectory into SICE SPK file.
        
        Parameters
        ----------
        kernel_path : string, optional
            SPICE kernel files directory full name.
            Default is '/astrodata/kernels/'
        spice_exe_path : string, optional
            SPICE kernel executables directory full name.
            Default is '/astrodata/exe/'
        tmpdir : string, optional
            Temporary directory where files are created. This is useful if the path to the files
            are very long, since it causes the mkspk utility program to crash.
            Temporary files (e.g. trajectory in ascii format) andoutput files are first
            created in tmpdir directory, and then the output is saved in spk_file.
            Default is '/tmp/'.
        
        Returns
        -------
        None.
        
        """
        # first compute EarthLocation and Vloc
        self.compute_eloc()
        # creates the ephemeris data file
        names=['EPOCH','X','Y','Z','VX','VY','VZ']
        # make spk name
        self.spk_file = self.config['CAMPAIGN']['out_dir']+'/'+self.name+'.spk'
        # get intermediate spk_dta file name
        data_file = tmpdir+os.path.basename(self.spk_file.replace('.spk','_spk.dat'))
        # intermediate setup file name
        setup_file = tmpdir+os.path.basename(self.spk_file.replace('.spk','.setup'))
        # intermediate output file
        tmpout_file = tmpdir+os.path.basename(self.spk_file)
        # double check that we are not about the erase an existing file
        for f2chk in [data_file,setup_file]:
            if f2chk==self.spk_file:
                msg = 'FATAL ERROR: '+self.spk_file+' is about to be erased: double check file name and make sure it contains spk'
                log.error(msg)
                raise ValueError(msg)
        log.debug('spk_file: '+self.spk_file)
        log.debug('data_file: '+data_file)
        log.debug('setup_file: '+setup_file)
        log.debug('tmpout_file: '+tmpout_file)
        
        # srt data with time
        self.traj.sort(['Time'])
        # write intermediate raw spk data
        with open(data_file,'w') as out:
            out.write("# "+' '.join(names)+" \n")
            for (t,x,y,z,vx,vy,vz) in zip(self.traj['Time'],
                          self.eloc.x.to('km').value,
                          self.eloc.y.to('km').value,
                          self.eloc.z.to('km').value,
                          self.Vloc_x.to('km/s').value,
                          self.Vloc_y.to('km/s').value,
                          self.Vloc_z.to('km/s').value):
                # write time
                out.write(Time(t,scale='utc').iso+"\n")
                # format data
                xstr = "{:.9f}".format(x)
                ystr = "{:.9f}".format(y)
                zstr = "{:.9f}".format(z)
                vxstr = "{:.9f}".format(vx)
                vystr = "{:.9f}".format(vy)
                vzstr = "{:.9f}".format(vz)
                # write data
                out.write(' '.join([xstr,ystr,zstr,vxstr,vystr,vzstr])+" \n")
        log.info("raw spk data saved in: "+data_file)
        utils.check_file(data_file,log)
        
        # make the intermediate setup file
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
            out.write("   OUTPUT_SPK_TYPE   = 13\n")
            out.write("   OBJECT_ID         = "+str(self.airplane_id)+"\n")
            out.write("   OBJECT_NAME       = "+repr(self.name)+"\n")
            out.write("   CENTER_ID         = 399\n")
            out.write("   REF_FRAME_NAME    = "+repr('ITRF93')+"\n")
            out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
            out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
            out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
            out.write("   LEAPSECONDS_FILE  = "+repr(kernel_path+'naif0012.tls')+"\n")
            out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
            out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
            out.write("   PCK_FILE          = "+repr(kernel_path+'pck00010.tpc')+"\n")
            out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
            out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
            out.write("   IGNORE_FIRST_LINE = 1 \n")
            out.write("   LINES_PER_RECORD  = 2 \n")
            out.write("   POLYNOM_DEGREE    = 3 \n")
            out.write("   SEGMENT_ID        = "+repr(str(self.airplane_id))+"\n")
            out.write(" \n")
        log.info("setup saved in: "+setup_file)
        utils.check_file(setup_file,log)
        
        # remove spk file if it already exists
        if os.path.exists(tmpout_file):
            os.remove(tmpout_file)
        # build the command
        cmd = spice_exe_path+"mkspk -setup "+setup_file
        log.info("cmd="+cmd)
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
            log.error(msg)
            raise IOError(msg)
        # double check that the SPK file was successfully created
        if not os.path.exists(tmpout_file):
            msg='*** FATAL ERROR: temporary SPK file: '+self.spk_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        # now copy temporary output file into spk_file
        outdir = os.path.dirname(self.spk_file)+'/'
        for file2move in [setup_file,data_file,tmpout_file]:
            dst = outdir+os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            log.debug('Moving file '+file2move+' into '+dst)
            try:
                # remark: os.remove does not always work when trying to move file from one FS to another...
                shutil.copy(file2move,dst)
            except:
                msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
                log.error(msg)
                raise IOError(msg)
        log.info("SPICE SPK kernel saved in: "+self.spk_file)
        self.cov = sp.spkcov(self.spk_file, self.airplane_id)
        return 
    
    def make_fake_missplan_conf(self):
        """Make a fake missionplanner configuration file.
        
        This is useful for integration with the obsplan package.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        None
        
        """
        MPconf = ConfigParser(interpolation=ExtendedInterpolation())
        MPconf['USER'] = {'home' : os.getenv('HOME')}
        MPconf['CAMPAIGN'] = {'nacelle_id': self.airplane_id,
                              'nacelle_name': self.name,
                              'trajectory_sim_file' : self.config['CAMPAIGN']['trajectory_sim_file']}
        MPconf_file =self.traj_file.replace('.dat','-MPconfig.in')
        with open(MPconf_file, 'w') as configfile:
           MPconf.write(configfile)
        log.info('Fake MPconfig file saved in '+MPconf_file)        
        return
    
    def make_all_kernels(self):
        """Make and load all necessary kernels.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # load SPICE standard kernel
        self.load_spice_kernels(self.kernel_path)
        # load SPK kernels and get time coverage
        self.cov = self.load_spk_kernel(self.spk_file,self.airplane_id)
        # make and load topo-like frame kernels
        self.tf_file,self.frame_id,self.frame_name = self.make_tf(kernel_path=self.kernel_path)
        # make and load instrument text frame kernels and instrument kernels
        self.tk_file,self.camera_frame_id,self.camera_frame_name = self.make_tk(kernel_path=self.kernel_path)
        # make instrument kernels
        self.ik_file,self.ik_id,self.ik_name = self.make_ik(self.spk_file,self.tf_file,self.camera.fov,kernel_path=self.kernel_path)
        # make and load ck (orientation) kernel files
        self.ck_file = self.make_airplane_ck(self.spk_file,self.tf_file)

        # make an ellipsoid of the top of atmosphere
        (self.atmosphere_name,
         self.atmosphere_id,
         self.atmosphere_text_kernel,
         self.atmosphere_spk_file) = self.make_atmosphere_kernel()
        
        log.info('All SPICE kernels loaded.')
        return
    
    def load_spice_kernels(self,kernel_path,standard_file_name='standard.ker'):
        """Function to load SPICE standard kernels.
        
        Parameters
        -------------
        kernel_path:
        
        spice_exe_path:
        
        Returns
        -------------
        None.
        
        """
        
        # load required standard spice kernels
        kernel_file = kernel_path + standard_file_name
        utils.check_file(kernel_file,log)
        sp.furnsh(kernel_file)
        log.debug('kernel_file loaded: '+kernel_file)
        return
    
    def load_spk_kernel(self,spk_file,spk_id):
        """Load SPK kernels and check the SPICE id.
        
        Make sure the time coverage overlap.
        
        Parameters
        ----------
        spk_file : String
            full path to spk file.
        spk_id : int
            SPICE spacecraft id that should be in SPK file.
        
        Returns
        -------
        time_cover : SPICE time window
            Time coverage of the spk file.
        
        """
        # chekc SPK file
        utils.check_file(spk_file,log)
        # load kernel with sp.furnsh()
        sp.furnsh(spk_file)
        log.debug('SPK kernel loaded from file: '+spk_file)

        # check nacelle id from SPK file
        if not (spk_id == sp.spkobj(spk_file)[0]):
            msg = '*** FATAL ERROR: SPICE nacelle id do not match: '+str(spk_id)+' vs '+str(sp.spkobj(spk_file)[0])
            log.error(msg)
            raise ValueError(msg)
        
        # get the time window coverage
        time_cover = sp.spkcov(spk_file, spk_id)
        log.info('time_cover start: '+sp.et2utc(time_cover[0], "ISOC", 2, 50))
        log.info('time_cover end  : '+sp.et2utc(time_cover[1], "ISOC", 2, 50))
        return time_cover    
    
    def make_tf(self,kernel_path='/astrodata/kernels/',load_stdker=True,load_spker=True):
        """Make airplane SPICE tf airplane centered local frame.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # Set output path for tf-file for nacelle
        self.tf_file = self.config['CAMPAIGN']['out_dir'] + '/' + self.name + '.tf'
        
        # launch the cmd to make the tf file
        (self.tf_file,
         self.airplane_frame_id,
         self.airplane_frame_name) = create_tf(self.spk_file,
                                              self.airplane_id,
                                              self.name,
                                              out_tf_file=self.tf_file)
        
        log.info("tf file saved in " + self.tf_file)
        utils.check_file(self.tf_file,log)
        sp.furnsh(self.tf_file)
        return self.tf_file,self.airplane_frame_id,self.airplane_frame_name
    
    def make_tk(self,kernel_path='/astrodata/kernels/',load_stdker=True,load_spker=True):
        """Make camera SPICE fixed tk frame wrt airplane frame.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # Set tk file name
        self.tk_file =self.tf_file.replace('.tf','_CAMORI_FRAME.tk')
        
        # make tk file
        (self.tk_file,
         self.camera_frame_id,
         self.camera_frame_name) = create_tk(self.spk_file,
                                             self.airplane_id,
                                             self.name,
                                             out_tk_file=self.tk_file,
                                             kernel_path=kernel_path,
                                             load_stdker=False,
                                             load_spker=False)
        
        log.info("tk-file saved in " + self.tk_file)
        utils.check_file(self.tk_file,log)
        sp.furnsh(self.tk_file)
        return self.tk_file,self.camera_frame_id,self.camera_frame_name
    
    
    def make_ik(self,spk_file,tf_file,camera_fov,kernel_path='/astrodata/kernels/'):
        """Make SPICE instrument kernel.
        
        Parameters:
        ------------
        spk_file : string
            Nacelle SPICE position-velocity (spk) kernel full path file name.
        tf_file : string
            Nacelle SPICE frame (fk) kernel full path file name.
        camera_fov : 2-array of astropy.units.Quantity object
            Camera field of view (azimuth, elevation extents).
        kernel_path : string, optional
            Path to SPICE kernel files. Default is '/astrodata/kernels/'
        
        Returns
        -------
        ik_file : string
            SPICE instrument kernel full path file name.
        ik_id : int
            SPICE instrument id.
        ik_name : string
            SPICE instrument name.
        
        """
        # create output file name in output directory
        ik_file,ik_id,ik_name = create_ik(spk_file,tf_file,camera_fov,outdir=self.out_dir,kernel_path=kernel_path)
        # load ik kernel
        sp.furnsh(ik_file)
        log.info('ik_file loaded: '+ik_file)
        return ik_file,ik_id,ik_name    
    
    def make_airplane_ck(self,spk_file,frame_file):
        """Create and load SPICE CK kernel.
        
        Parameters:
        --------------
        spk_file : string
            Airplane SPICE spk full path file name.
        frame_file : string
            Airplane SPICE frame full path file name (tf-file) of the respective spk_file.
        
        Returns
        -------
        ck_file :string
            Output airplane CK kernel file.
        
        """        
        # make output ck files
        self.ck_file = self.spk_file.replace('.spk','.ck')
        
        # make a QTable of airplane azimuth as a function of SPICE time
        try:
            self.et = [sp.utc2et(tm) for tm in self.traj['Time'][:]]
        except AttributeError:
            self.et = [sp.utc2et(tm.isot) for tm in self.traj['Time'][:]]
        self.aztime = QTable([self.traj['Time'],self.et,self.traj['az']],
                             names=['Time','et','az'])
        self.aztime['et'].info.format = '17.3f'
        self.aztime['az'].info.format = '7.3f'
        self.aztime_file = self.spk_file.replace('.spk','_aztime.dat')
        self.aztime.write(self.aztime_file,format='ascii.fixed_width_two_line',delimiter=' ',formats={'Time':'%23s','':''},overwrite=True)
        log.info('azimuth - time table saved in '+self.aztime_file)

        # make temporary directory
        sim_tmp_dir = self.tmp_dir + '/' + self.sim_id + '/'
        if not os.path.isdir(sim_tmp_dir):
            os.makedirs(sim_tmp_dir)
            log.info('Temporary directory created: '+sim_tmp_dir)
        # create ck kernel
        (self.ck_file,self.tsc_file,self.cov) = create_airplane_ck(self.spk_file,
                                        frame_file,
                                        self.aztime,
                                      offset_az=self.offset_az,
                                      offset_el=self.offset_el,
                                      offset_roll=self.offset_roll,
                                      meteor_height=self.meteor_alt,
                                      tmp_dir=sim_tmp_dir,
                                      freq=self.freq,
                                      out_ck_file=self.ck_file)
        # remove temporary directory
        try:
            shutil.rmtree(sim_tmp_dir)
            log.info('Temporary directory deleted: '+sim_tmp_dir)
        except:
            msg = '*** FATAL ERROR: unable to remove temporary directory: '+sim_tmp_dir
            log.error(msg)
            raise IOError(msg)
        
        # load ck file
        utils.check_file(self.ck_file,log)
        sp.furnsh(self.ck_file)
        log.info('ck file loaded: '+self.ck_file)
        # load tsc file
        utils.check_file(self.tsc_file,log)
        sp.furnsh(self.tsc_file)
        log.info('tsc file loaded: '+self.tsc_file)
        return self.ck_file
    
    
    def make_camera(self):
        """Make Camera object.
        
        The data needed to make the Camera objects are in the config file
        in the 'CAMERA' section.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.debug('Making Camera Object')
        # create Filter object
        flt = Filter(brand=self.config['CAMERA']['filter_brand'],name=self.config['CAMERA']['filter_name'])
        
        # create Lens object
        lens = Lens(brand=self.config['CAMERA']['lens_brand'], name=self.config['CAMERA']['lens_name'], filtre=flt)
                
        # create Camera Object
        self.camera = Camera(brand=self.config['CAMERA']['camera_brand'], name=self.config['CAMERA']['camera_name'], lens=lens)
        self.camera.fps = float(self.config['CAMERA']['fps']) / u.s
        self.camera.info()
        self.camera_id = make_camera_id(self.airplane_id)
        return
    
    def create_ik(self,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',load_stdker=True,load_spker=True,load_frame=True):
        """Create Instrument kernel file.
        
        The frame kernel is a tk (text frame kernel), which can be created
        with the create_tk routine.
        
        Parameters
        ----------
        kernel_path : string, optional
            Path to SPICE standard kernel. Default is '/astrodata/kernels/'.
        load_stdker : boolean, optional.
            If True, the SPICE standard kernel is loaded. Default is True.
        load_spker : boolean, optional.
            If True, the SPICE SPK kernel is loaded. Default is True.
        load_frame : boolean, optional.
            If True, the SPICE frame kernel is loaded. Default is True.
        
        Returns:
        ------------
        ik_file : string
            SPICE instrument kernel full path file name.
        ik_id : int
            SPICE instrument id.
        ik_name : string
            SPICE instrument name.
        
        """
        
        # load SPICE standard kernel if needed
        if load_stdker:
            # load standard kernel
            std_kernel_file = kernel_path + 'standard.ker'
            utils.check_file(std_kernel_file,log)
            sp.furnsh(std_kernel_file)
        
        # load nacelle spk file if needed
        if load_spker:
            utils.check_file(self.spk_file,log)
            sp.furnsh(self.spk_file)
        
        # load frame file (tf-file)
        if load_frame:
            utils.check_file(self.tf_file,log)
            sp.furnsh(self.tf_file)
                
        # Set ik-file
        self.ik_file = self.spk_file.replace('.spk','_camera.ik')
        
        # delete previously created ik files
        if os.path.exists(self.ik_file):
            os.remove(self.ik_file)
        
        # make camera name and id
        self.camera_name = make_camera_name(self.name)
        self.camera_id = make_camera_id(self.airplane_id)
        
        # get fov info
        [fov_az,fov_el] = self.camera.fov
        
        # Compute boresight vector, pointing in the direction
        # of the X-axis of the camera/instrument frame kernel,
        # defined in create_tk routine.
        boresight = np.array([1.0,0.0,0.0]) # X-vector
        refvector = np.array([0.0,1.0,0.0]) # Y-vector
        log.info('camera_name='+self.camera_name)
        log.info('camera_id  ='+str(self.camera_id))
        log.info('fov_az='+str(fov_az))
        log.info('fov_el='+str(fov_el))
        log.info('boresight='+str(boresight))
        log.info('refvector='+str(refvector))
        
        # ====================================================
        # now make instrument kernel for MALBEC station camera
        with open(self.ik_file, 'w') as out:
            out.write("\\begindata \n")
            out.write('    NAIF_BODY_NAME += ('+repr(self.camera_name)+')\n')
            out.write('    NAIF_BODY_CODE += ('+str(self.camera_id)+')\n\n\n')
            out.write("\\begintext \n")
            out.write("  Definition of the pointing direction and FOV of MALBEC cameras instruments. \n\n")
            out.write("   \n")
            out.write("  Author: J. Vaubaillon - IMCCE - 2022 \n")
            out.write("  MALBEC project \n\n\n")
            out.write("Definition of the " + self.camera_name + " FOV: \n \n")
            out.write("\\begindata \n")
            out.write("INS" + str(self.camera_id) + "_FOV_FRAME = " + repr(self.camera_frame_name) + " \n")
            out.write("INS" + str(self.camera_id) + "_FOV_SHAPE = " + repr("RECTANGLE") + " \n")
            out.write("INS" + str(self.camera_id) + "_BORESIGHT = ( " + str(boresight[0]) + "   " + str(
                boresight[1]) + "  " + str(boresight[2]) + " ) \n")
            # defining the corners with the ref_vector, ref_angle and cross_angle
            out.write("INS" + str(self.camera_id) + "_FOV_CLASS_SPEC = " + repr("ANGLES") + " \n")
            out.write("INS" + str(self.camera_id) + "_FOV_REF_VECTOR = ( " + str(refvector[0]) + "   " + str(
            refvector[1]) + "   " + str(refvector[2]) + " )  \n")
            out.write("INS" + str(self.camera_id) + "_FOV_REF_ANGLE =  " + str(fov_az.to('deg').value / 2) + " \n")
            out.write("INS" + str(self.camera_id) + "_FOV_CROSS_ANGLE =  " + str(fov_el.to('deg').value / 2) + " \n")
            out.write("INS" + str(self.camera_id) + "_FOV_ANGLE_UNITS =  " + repr("DEGREES") + " \n\n")
            out.write("\\begintext \n")
        utils.check_file(self.ik_file,log)
        log.info("data saved in " + self.ik_file)
        sp.furnsh(self.ik_file)
        
        return
    
    def make_atmosphere_kernel(self,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',tmp_dir='/tmp/'):
        """Make an atmosphere object kernel.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        atmosphere_kernel_file : string
            Output kernel file.
        
        """
        # create output file name
        atmosphere_text_kernel = self.config['CAMPAIGN']['out_dir'] +'/'+ 'atmosphere.ker'
        atmosphere_spk_file = atmosphere_text_kernel.replace('.ker','.spk')
        # make SPICE object name and id
        atmosphere_name = 'ATMOSPHERE_ELLIPSOID'
        atmosphere_id = 1399001
        surface_name = 'ATMOSPHERE_SURFACE'
        surface_id = 1399002
        atmosphere_frame_name = 'ATM_FRAME'
        atmosphere_frame_id = 1399003
        # get Earth radii
        earth_radii = sp.bodvrd( "EARTH", "RADII", 3)[1]*u.km
        # set atmosphere radii
        self.meteor_alt = float(self.config['SHOWER']['meteor_alt'])*u.km
        self.atm_radii = earth_radii + self.meteor_alt
        
        # check if files alrady exist
        if (os.path.exists(atmosphere_text_kernel) and os.path.exists(atmosphere_spk_file) and self.read_res):
            # load kernels
            sp.furnsh(atmosphere_text_kernel)
            sp.furnsh(atmosphere_spk_file)
            log.debug('atmosphere_text_kernel file loaded: '+atmosphere_text_kernel)
            log.debug('atmosphere_spk_file file loaded: '+atmosphere_spk_file)
            return (atmosphere_name,atmosphere_id,atmosphere_text_kernel,atmosphere_spk_file)
        
        # make temporary directory
        sim_tmp_dir = tmp_dir + '/' + self.sim_id + '/'
        if not os.path.isdir(sim_tmp_dir):
            os.makedirs(sim_tmp_dir)
            log.info('Temporary directory created: '+sim_tmp_dir)
        # loop over time
        start,stop = sp.wnfetd(self.cov,0)
        # set delta_time for atmosphere definition, in [s]
        det = 60.0
        # set start time
        et = start - det
        # creates the ephemeris data file
        names=['EPOCH','X','Y','Z','VX','VY','VZ']
        # make intermediate spk_data file name
        data_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file.replace('.spk','_spk.dat'))
        # intermediate setup file name
        setup_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file.replace('.spk','.setup'))
        # intermediate output file
        tmpout_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file)
        log.debug('spk_file: '+atmosphere_spk_file)
        log.debug('data_file: '+data_file)
        log.debug('setup_file: '+setup_file)
        log.debug('tmpout_file: '+tmpout_file)
        with open(data_file,'w') as out:
            out.write("# "+' '.join(names)+" \n")
            while et < (stop+det):
                out.write(sp.et2utc(et,'ISOC',2,50).replace('T',' ')+"\n")
                out.write('0.000000000 0.000000000 0.000000000 -0.000000000 -0.000000000 -0.000000000\n')
                et = et + self.freq.to('s').value
        log.debug('Atmosphere data saved in '+setup_file)
        # make the intermediate setup file
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
            out.write("   OUTPUT_SPK_TYPE   = 13\n")
            out.write("   OBJECT_ID         = "+str(atmosphere_id)+"\n")
            out.write("   OBJECT_NAME       = "+repr(atmosphere_name)+"\n")
            out.write("   CENTER_ID         = 399\n")
            out.write("   REF_FRAME_NAME    = "+repr('ITRF93')+"\n")
            out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
            out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
            out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
            out.write("   LEAPSECONDS_FILE  = "+repr(kernel_path+'naif0012.tls')+"\n")
            out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
            out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
            out.write("   PCK_FILE          = "+repr(kernel_path+'pck00010.tpc')+"\n")
            out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
            out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
            out.write("   IGNORE_FIRST_LINE = 1 \n")
            out.write("   LINES_PER_RECORD  = 2 \n")
            out.write("   POLYNOM_DEGREE    = 3 \n")
            out.write("   SEGMENT_ID        = "+repr(str(atmosphere_id))+"\n")
            out.write(" \n")
        log.info("setup saved in: "+setup_file)
        
        # remove spk file if it already exists
        if os.path.exists(tmpout_file):
            os.remove(tmpout_file)
        # build the command
        cmd = spice_exe_path+"mkspk -setup "+setup_file
        log.info("cmd="+cmd)
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
            log.error(msg)
            raise IOError(msg)
        # double check that the SPK file was successfully created
        if not os.path.exists(tmpout_file):
            msg='*** FATAL ERROR: temporary SPK file: '+atmosphere_spk_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        # now copy temporary output file into spk_file
        outdir = os.path.dirname(atmosphere_spk_file)+'/'
        for file2move in [setup_file,data_file,tmpout_file]:
            dst = outdir+os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            log.debug('Moving file '+file2move+' into '+dst)
            try:
                # remark: os.remove does not always work when trying to move file from one FS to another...
                shutil.copy(file2move,dst)
            except:
                msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
                log.error(msg)
                raise IOError(msg)
        log.info("SPICE SPK kernel saved in: "+atmosphere_spk_file)
        # remove temporary directory
        try:
            shutil.rmtree(sim_tmp_dir)
            log.info('Temporary directory deleted: '+sim_tmp_dir)
        except:
            msg = '*** FATAL ERROR: Impossible to remove temporary directory: '+sim_tmp_dir
            log.error(msg)
            raise IOError(msg)
        
        # make text kernel
        with open(atmosphere_text_kernel,'w') as out:
            out.write("\\begintext \n\n")
            out.write('  Definition of the top of atmosphere at meteor altitude \n')
            out.write('  Author: J. Vaubaillon - IMCCE - 2020\n')
            out.write('  MALBEC project\n\n')
            out.write("\\begindata \n\n")
            out.write('   NAIF_BODY_NAME += ('+repr(atmosphere_name)+')\n')
            out.write('   NAIF_BODY_CODE += ( '+str(atmosphere_id)+ ')\n')
            out.write('   NAIF_BODY_CENTER = 399 \n')
            out.write('   NAIF_SURFACE_NAME += ( '+repr(surface_name)+' )\n')
            out.write('   NAIF_SURFACE_CODE += ( '+str(surface_id)+' )\n')
            out.write('   NAIF_SURFACE_BODY += ( '+str(atmosphere_id)+' )\n\n')
            out.write('   OBJECT_'+str(surface_id)+'_FRAME = '+repr('ITRF93')+'\n\n')
            out.write('   BODY'+str(atmosphere_id)+'_CENTER = 399\n\n')
            out.write('   BODY'+str(atmosphere_id)+'_RADII = ( '+str(self.atm_radii[0].to('km').value)+' , '+str(self.atm_radii[1].to('km').value)+' , '+str(self.atm_radii[2].to('km').value)+' )\n')
            out.write('   BODY'+str(atmosphere_id)+'_POLE_RA        = (    0.      -0.641         0. )\n')
            out.write('   BODY'+str(atmosphere_id)+'_POLE_DEC       = (   90.      -0.557         0. )\n')
            out.write('   BODY'+str(atmosphere_id)+'_PM             = (  190.147  360.9856235     0. )\n')
            out.write('   BODY'+str(atmosphere_id)+'_LONG_AXIS      = (    0.                        )\n\n')
            out.write('   FRAME_'+atmosphere_frame_name+' = '+str(atmosphere_frame_id)+'\n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_NAME    =  '+repr(atmosphere_frame_name)+'\n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_CLASS   =  4 \n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_CLASS_ID = '+str(atmosphere_frame_id)+'\n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_CENTER  = '+str(atmosphere_id)+'\n\n')
            out.write('   OBJECT_'+str(atmosphere_id)+'_FRAME = '+repr(atmosphere_frame_name)+'\n\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_RELATIVE            =  '+repr('ITRF93')+'\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_SPEC                =  '+repr('ANGLES')+'\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_UNITS               =  '+repr('DEGREES')+'\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_AXES                =  ( 3, 2, 3 )\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_ANGLES              =  ( 0, 0, 0 )\n')
        # load kernels
        sp.furnsh(atmosphere_text_kernel)
        sp.furnsh(atmosphere_spk_file)
        log.debug('atmosphere_text_kernel file loaded: '+atmosphere_text_kernel)
        log.debug('atmosphere_spk_file file loaded: '+atmosphere_spk_file)
        return (atmosphere_name,atmosphere_id,atmosphere_text_kernel,atmosphere_spk_file)
    
            
    def compute_all_fov(self,deltaT=1.0):
        """Compute field of view as a function of time.
        
        Data and in particular the surveyed area as a function of time
        are saved in output files.
        
        Parameters
        ----------
        deltaT : float.
            Number of seconds after the start of the flight, and before the end
            of the flight when the fov will be computed. This prevents SPICE to
            search for data at undefined times. Default is 1.0 (i.e. one second).
        
        Returns
        -------
        None.
        
        """
        # make a list of times when fov are to be computed
        self.et_start,self.et_stop = sp.wnfetd(self.cov,0)
        self.et_start = self.et_start + deltaT
        self.et_stop = self.et_stop - deltaT
        ets = np.append(np.arange(self.et_start,self.et_stop,self.freq.to('s').value),self.et_stop)
        # compute fov over time
        for et in ets:
            self.compute_fov_attime(et)
        # convert FOV into kml
        self.fov2datNkml()     
        # save geometry data
        self.save_geom_data()
        return
    
    def get_pointing(self,time):
        """Get camera boresight pointing direction.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        eloc : astropy.coordinates.EarthLocation object
            Location of the aircraft in ITRF93 coordinates.
        cam_radec : astropy.coordinates.SkyCoord object
            Camera boresight Sky coordinates: right ascension and declination, in J2000 frame.
        cam_altaz : astropy.coordinates.SkyCoord object
            Camera boresight Sky coordinates: azimuth and elevation, in topo-like frame.
        
        """
        # covert time
        et = sp.utc2et(time.isot)
        # retrieve camera FOV information
        [shape,insfrm,brsgt,n,bounds] = sp.getfov(self.camera_id,4)
        
        # retrieve airplane location
        eloc = self.get_position(time)
        
        # compute frame conversion matrix
        pointing_mtx = sp.pxform(self.camera_frame_name,'J2000',et)
        # get camera boresight pointing direction
        brsgt_radec_sp = sp.mxv(pointing_mtx,brsgt)
        # convert to spherical coordinates
        (brsgt_dist,brsgt_ra_sp,brsgt_dec_sp) = sp.recrad(brsgt_radec_sp)
        # make SkyCoord objects
        cam_radec = SkyCoord(ra=brsgt_ra_sp,dec=brsgt_dec_sp,unit='rad',frame='icrs')
        # get Altazimuthal coordinates
        cam_altaz = cam_radec.transform_to(AltAz(obstime=time,location=eloc))
        return (eloc,cam_radec,cam_altaz)
    
    def get_radmoonsun(self,time,eloc=None,radec=None):
        """Get sky coordinates of the radiant, Moon and the Sun.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        eloc : astropy.coordinates.EarthLocation object, optional.
            aircraft location. Automatically computed if not set.
        radec : astropy.coordinates.SkyCoord object, optional
            Sky coordinates of the camera boresight. Automatically computed if not set.
        
        Returns
        -------
        rad_radec : astropy.coordinates.SkyCoord object
            Radiant right ascension and declination, in J2000 frame.
        rad_altaz : astropy.coordinates.SkyCoord object
            Radiant azimuth and altitude, in topocentric frame.
        moon_radec : astropy.coordinates.SkyCoord object
            Moon right ascension and declination, in J2000 frame.
        moon_altaz : astropy.coordinates.SkyCoord object
            Moon azimuth and altitude, in topocentric frame.
        sun_radec : astropy.coordinates.SkyCoord object
            Sun right ascension and declination, in J2000 frame.
        sun_altaz : astropy.coordinates.SkyCoord object
            Sun azimuth and altitude, in topocentric frame.
        
        """
        # if eloc not provided, compute it
        if ((not eloc) or (not radec)):
            (eloc,radec,altaz) = self.get_pointing(time)
        # get radiant drift
        rad_radec = self.radiant.drift(time)
        rad_altaz = rad_radec.transform_to(AltAz(obstime=time,location=eloc))
        # use astropy to get Moon RA,DEC
        moon_radec = get_moon(time,eloc)
        # use astropy to convert Moon RA,DEC into AltAz
        moon_altaz = moon_radec.transform_to(AltAz(obstime=time,location=eloc))
        # use astropy to get Sun RA,DEC
        sun_radec = get_sun(time)
        # use astropy to convert Moon RA,DEC into AltAz
        sun_altaz = sun_radec.transform_to(AltAz(obstime=time,location=eloc))
        # compute distance of center of FOV to radiant, moon and sun
        sep_moon = moon_radec.separation(radec)
        sep_sun = sun_radec.separation(radec)
        sep_radiant = rad_radec.separation(radec)
        return (rad_radec,rad_altaz,sep_radiant,
                moon_radec,moon_altaz,sep_moon,
                sun_radec,sun_altaz,sep_sun)
        
    def compute_fov_attime(self,et):
        """Compute FOV for the 2 cameras.
        
        Data appended to self.fov_data and self.grids data.
        
        Parameters
        ----------
        et : float
            SPICE time (sec after J2000).
        
        Returns
        -------
        None.
        
        """
        # set time
        time = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
        log.info('========== Computing FOV at time: '+time.isot)
        
        # compute aircraft location and camera pointing direction
        (eloc,cam_radec,cam_altaz) = self.get_pointing(time)
        # get radiant, moon and sun sky coordinates
        (rad_radec,rad_altaz,sep_radiant,
         moon_radec,moon_altaz,sep_moon,
         sun_radec,sun_altaz,sep_sun) = self.get_radmoonsun(time,eloc,cam_radec)
        # store geometry data
        self.add_geom_data(time,cam_radec,cam_altaz,
                           rad_radec,rad_altaz,sep_radiant,
                           moon_radec,moon_altaz,sep_moon,
                           sun_radec,sun_altaz,sep_sun)
        
        # retrieve camera FOV information
        [shape,insfrm,brsgt,n,bounds] = sp.getfov(self.camera_id,4)
        # make a list of bounds, and add boresight
        bounds = bounds.tolist()
        bounds.append(brsgt)
        # creates coordinates: coords_lo: fov at atm top
        (coords_lo,crn_dst,bounds_eloc) = ([],[], [])
        # loop over FOV boundary angles
        for bound in bounds:
            # compute intersection with top of atmosphere
            #log.info('getting FOV from '+self.name+' in frame ' +self.camera_frame_name)
            bound_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',self.name,self.camera_frame_name,bound)
            # compute physical distance between bound and nacelle
            crn_dst.append(sp.vnorm(vecout))
            # convert into EarthLocation
            bound_top_eloc = EarthLocation.from_geocentric(bound_top[0],bound_top[1],bound_top[2],unit='km')
            bounds_eloc.append(bound_top_eloc)
            # store FOV boundaries coordinates
            coords_lo.append((bound_top_eloc.lon.to('deg').value,bound_top_eloc.lat.to('deg').value,bound_top_eloc.height.to('m').value))
        # get radius of the Earth at boresight latitude (latest computed)
        R_earth = utils.latrad(bounds_eloc[-1].lat.to('rad').value)
        # compute theoretical area covered by the fov
        fov_area = utils.elocs42area(bounds_eloc[-2],bounds_eloc[-3],bounds_eloc[-4],bounds_eloc[-5],r=R_earth)
        # Store data in fov_data
        self.add_fov_data(time,coords_lo,crn_dst,fov_area)
        return
    
    
    def add_geom_data(self,time,cam_radec,cam_altaz,
                           rad_radec,rad_altaz,sep_radiant,
                           moon_radec,moon_altaz,sep_moon,
                           sun_radec,sun_altaz,sep_sun):
        """Add geometry data into self.geom global variable.
        
        Parameters
        ----------
        
        
        Returns
        -------
        None.
        
        
        """
        # convert time
        et = sp.str2et(time.isot)
        # add row in self.geom QTable
        self.geom_data.add_row([time.isot,et,cam_radec.ra.to('deg'),cam_radec.dec.to('deg'),
                                cam_altaz.az.to('deg'),cam_altaz.alt.to('deg'),
                           rad_radec.ra.to('deg'),rad_radec.dec.to('deg'),
                           rad_altaz.az.to('deg'),rad_altaz.alt.to('deg'),
                           sep_radiant.to('deg'),
                           moon_radec.ra.to('deg'),moon_radec.dec.to('deg'),
                           moon_altaz.az.to('deg'),moon_altaz.alt.to('deg'),
                           sep_moon.to('deg'),
                           sun_radec.ra.to('deg'),sun_radec.dec.to('deg'),
                           sun_altaz.az.to('deg'),sun_altaz.alt.to('deg'),
                           sep_sun.to('deg')])
        return
    
    def save_geom_data(self):
        """Save FOV data into data type file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # sort FOV data
        self.geom_data.sort('Time')
        # add index
        self.geom_data.add_index('Time')
        # write into FOV data output file
        self.geom_data.write(self.geom_dat_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
        log.info('Geometry Data saved in '+self.geom_dat_file)
        return
    
    def add_fov_data(self,time,coords_lo,crn_dst,fov_area):
        """Add FOV data into fov_data global variable.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for each fov data to add.
        coords_lo : array of float
            Coordinates of fov corners and boresight at atmosphere top to add.
        crn_dst : array of float
            distance between nacelle and fov corners and boresight.
        fov_area : astropy.units.Quantity object.
            Area surveyed by nacelle_name (single station).
        
        Returns
        -------
        None.
        
        """
        et = sp.str2et(time.isot)
        # add bounds into data Table: time, et, name, [lon, lat, alt, distance]*[boresight and 4 corners], area
        self.fov_data.add_row([time.isot,et,self.name,
                               coords_lo[-1][0]*u.deg,coords_lo[-1][1]*u.deg,coords_lo[-1][2]*u.m,crn_dst[-1]*u.km,
                               coords_lo[-2][0]*u.deg,coords_lo[-2][1]*u.deg,coords_lo[-2][2]*u.m,crn_dst[-2]*u.km,
                               coords_lo[-3][0]*u.deg,coords_lo[-3][1]*u.deg,coords_lo[-3][2]*u.m,crn_dst[-3]*u.km,
                               coords_lo[-4][0]*u.deg,coords_lo[-4][1]*u.deg,coords_lo[-4][2]*u.m,crn_dst[-4]*u.km,
                               coords_lo[-5][0]*u.deg,coords_lo[-5][1]*u.deg,coords_lo[-5][2]*u.m,crn_dst[-5]*u.km,
                               fov_area])
        return
    
    def save_fov_data(self):
        """Save FOV data into data type file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # sort FOV data
        self.fov_data.sort('Time')
        # add index
        self.fov_data.add_index('Time')
        # write into FOV data output file
        self.fov_data.write(self.fov_dat_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
        log.info('FOV Data saved in '+self.fov_dat_file)
        return
    
    def get_position(self,time,silent=False):
        """Get position of one aircraft at time t.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        silent : boolean, optional
            If True, no log is performed. Default is False
        
        Returns
        -------
        eloc : astropy.coordinates.EarthLocation
            Location of the nacelle at time t.
        
        """
        et = sp.utc2et(time.isot)
        if not silent:
            log.debug('Retrieving '+self.name+' position for time: '+time.isot+' et: '+str(et))
        # check that the nacelle is known to SPICE
        try:
            sp.bodn2c(self.name)
        except:
            msg = '*** FATAL ERROR: SPICE does not seem to know nacelle: '+self.name
            log.error(msg)
            raise ValueError(msg)
        # get positons of nacelles with SPICE tools ; note that you might want to convert time to SPICE time.
        (eph, lt) = sp.spkezr(self.name,et,'ITRF93','NONE','EARTH')
        # example: eloc = EarthLocation.from_geocentric(x, y, z, unit=u.km)
        eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
        if not silent:
            log.debug('Position of '+self.name+' at '+time.isot+' = '+str(eloc.to_geodetic()))
        return eloc    
    
    def fov2datNkml(self,silent=True):
        """Save FOV data in kml file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # create FOV KML object
        fov_kml = simplekml.Kml()
        self.save_fov_data()
        # loop over all fov data
        for fov in self.fov_data:
            fov_name = 'FOV'+self.name
            bst_name = 'BRS'+self.name
            time = Time(fov['Time'],format='isot',scale='utc')
            nac_eloc = self.get_position(time)# ,silent=True)
            
            # make a yellow line from nacelle to boresight at meteor height
            line_brs = fov_kml.newlinestring(name=bst_name)
            line_brs.coords = [(nac_eloc.lon.to('deg').value,nac_eloc.lat.to('deg').value,nac_eloc.height.to('m').value),
                               (fov['brsght_lon'].to('deg').value,fov['brsght_lat'].to('deg').value,fov['brsght_alt'].to('m').value)]
            line_brs.extrude = 0
            line_brs.altitudemode = simplekml.AltitudeMode.absolute
            line_brs.style.linestyle.width = 5
            line_brs.style.linestyle.color = simplekml.Color.yellow
            line_brs.timestamp.when = time.isot
            
            # make a blue line from nacelle to each corner
            for crn_num in np.arange(4):
                crn_ns = str(crn_num+1)
                line_crn = fov_kml.newlinestring(name=fov_name)
                line_crn.coords = [(nac_eloc.lon.to('deg').value,nac_eloc.lat.to('deg').value,nac_eloc.height.to('m').value),
                                   (fov['crn'+crn_ns+'_lon'].to('deg').value,fov['crn'+crn_ns+'_lat'].to('deg').value,fov['crn'+crn_ns+'_alt'].to('m').value)]            
                line_crn.extrude = 0
                line_crn.altitudemode = simplekml.AltitudeMode.absolute
                line_crn.style.linestyle.width = 5
                line_crn.style.linestyle.color = simplekml.Color.blue
                line_crn.timestamp.when = time.isot
            
            # make a polygon of the surveyed area at meteor height
            pol = fov_kml.newpolygon(name='Surveyed area '+self.name)
            coo_pol = []
            for crn_num in np.arange(4):
                crn_ns = str(crn_num+1)
                coo_pol.append((fov['crn'+crn_ns+'_lon'].to('deg').value,fov['crn'+crn_ns+'_lat'].to('deg').value,fov['crn'+crn_ns+'_alt'].to('m').value))
            # add first point to close the polygon
            coo_pol.append((fov['crn1_lon'].to('deg').value,fov['crn1_lat'].to('deg').value,fov['crn1_alt'].to('m').value))
            pol.outerboundaryis = coo_pol
            pol.extrude = 0
            pol.altitudemode = simplekml.AltitudeMode.absolute
            pol.style.linestyle.width = 5
            pol.style.linestyle.color = simplekml.Color.white
            pol.style.polystyle.color = simplekml.Color.changealphaint(0, simplekml.Color.white)
            pol.timestamp.when = time.isot
 
        fov_kml.save(self.fov_kml_file)
        log.info('FOV saved in KML file '+self.fov_kml_file)
        return

    def get_fov_data(self,et):
        """Get FOV data at specific time.
        
        Parameters
        ----------
        et : float
            Time in SPICE format [sec after J2000].
        
        Returns
        -------
        fov_data : astropy.table.QTable
            Subset of self.fov_data at specific time.
        
        """
        # select fov_data at time et
        mask = self.fov_data['et']==et
        fov_data = self.fov_data[mask]
        
        # if there is no data, retrieve data at closest time et
        if not len(fov_data):
            # compute time in fov_data the closest to et
            et_closest = self.fov_data['et'][np.argmin(np.abs(et - self.fov_data['et']))]
            mask = self.fov_data['et']==et_closest
            fov_data = self.fov_data[mask]
        
        # if it all fails...
        if not len(fov_data):
            msg = '*** Impossible to retrieve fov_data for time: '+str(et)+' = '+sp.et2utc(et,"ISOC",2,50)
            log.error(msg)
            raise ValueError(msg)
        
        return fov_data
    
    def compute_geometry(self,time):
        """Compute the geometry of the camera FOV.
        
        Parameters
        ----------
        time : astropy.time.Time objec
            Time when to check geometry.
        
        
        Returns
        -------
        eloc : astropy.coordinates.EarthLocation object
            Airplane location.
        data : astropy.table.QTable object
            data containing: time, et, radiant sky coordinates (rad,dec),
            radiant separation, moon sky coordinates and separation,
            
        
        """
        # get the location of the airplane
        (eloc,radec,altaz) = self.get_pointing(time)
        # get location of the radiant, Moon and Sun
        (radiant_skycoo,radiant_altaz,
         moon_skycoo,moon_altaz,
         sun_skycoo,sun_altaz) = self.get_radmoonsun(time,eloc=eloc)
        # convert time
        et = sp.str2et(time.isot)
        # store data
        data = np.array([time,et,
                        radec.ra.to('deg'),radec.dec.to('deg'),
                        radiant_skycoo.ra.to('deg'),radiant_skycoo.dec.to('deg'),
                        sep_radiant.to('deg'),
                        moon_skycoo.ra.to('deg'),moon_skycoo.dec.to('deg'),
                        sep_moon.to('deg'),
                        sun_skycoo.ra.to('deg'),sun_skycoo.dec.to('deg'),
                        sep_sun.to('deg'),
                        altaz.az.to('deg'),altaz.alt.to('deg'),
                        radiant_altaz.az.to('deg'),radiant_altaz.alt.to('deg'),
                        moon_altaz.az.to('deg'),moon_altaz.alt.to('deg'),
                        sun_altaz.az.to('deg'),sun_altaz.alt.to('deg'),
                        ])
        return data,eloc

        
        
        
    
if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='airplanmaker arguments.')
    parser.add_argument('-conf',default='./conf/config.in',help='configuration file. Default is: ./config.in')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.conf
    
    # create Object
    airplane = Airplane(config_file)
    
else:
    log.debug('successfully imported')

