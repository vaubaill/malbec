"""
Program getlatestearthrotation

Goal: download the latest high precision Earth rotation file
      from the NASA/NAIF public website
      
"""

import urllib
from initialize import kernel_path

filename="earth_latest_high_prec.bpc"
url='http://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/'+filename
filename=kernel_path+filename
urllib.urlretrieve(url, filename)
