airplanemaker package
===========================

Submodules
----------

airplanemaker allows the simulate the flight of an airplane carrying the cameras.

airplanemaker\.airplanemaker module
----------------------------------

.. automodule:: airplanemaker.airplanemaker
    :members:
    :undoc-members:
    :show-inheritance:


airplanemaker\.create_airplane_ck module
----------------------------------

.. automodule:: airplanemaker.create_airplane_ck
    :members:
    :undoc-members:
    :show-inheritance:


airplanemaker\.gpx2dat module
----------------------------------

.. automodule:: airplanemaker.gpx2dat
    :members:
    :undoc-members:
    :show-inheritance:
