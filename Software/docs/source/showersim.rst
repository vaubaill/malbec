showersim package
===========================

Submodules
----------

showersim allows the launch of several solutions for mmissionplanner and all possible associtated obsplanner configurations.

showersim\.meteorshower module
----------------------------------

.. automodule:: showersim.meteorshower
    :members:
    :undoc-members:
    :show-inheritance:


showersim\.shower_radiant module
----------------------------------

.. automodule:: showersim.shower_radiant
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.shower_utils module
----------------------------------

.. automodule:: showersim.shower_utils
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.phot module
----------------------------------

.. automodule:: showersim.phot
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.make_fake_shower module
----------------------------------

.. automodule:: showersim.make_fake_shower
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.fakeower module
----------------------------------

.. automodule:: showersim.fakeower
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.fakeor module
----------------------------------

.. automodule:: showersim.fakeor
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.fakeobs module
----------------------------------

.. automodule:: showersim.fakeobs
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.afm module
----------------------------------

.. automodule:: showersim.afm
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.anchor module
----------------------------------

.. automodule:: showersim.anchor
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.distrib module
----------------------------------

.. automodule:: showersim.distrib
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.instrument module
----------------------------------

.. automodule:: showersim.instrument
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.makeafmcollection module
----------------------------------

.. automodule:: showersim.makeafmcollection
    :members:
    :undoc-members:
    :show-inheritance:

showersim\.spacecraft module
----------------------------------

.. automodule:: showersim.spacecraft
    :members:
    :undoc-members:
    :show-inheritance:
