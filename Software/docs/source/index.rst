.. MALBEC documentation master file, created by
   sphinx-quickstart on Wed Apr 19 14:28:15 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MALBEC's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Contents
--------

.. toctree::

   usage
   missionplanner
   obsplanner
   campaignplanner
   stationmaker
   airplanemaker
   simulgps
   showersim
   reduction
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


