simulgps package
===========================

Submodules
----------

simulgps allows the launch of several solutions for mmissionplanner and all possible associtated obsplanner configurations.

simulgps\.simulgps module
----------------------------------

.. automodule:: simulgps.simulgps
    :members:
    :undoc-members:
    :show-inheritance:


simulgps\.simulgps_sodaq module
----------------------------------

.. automodule:: simulgps.simulgps_sodaq
    :members:
    :undoc-members:
    :show-inheritance:
