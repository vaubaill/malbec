campaignplanner package
===========================

Submodules
----------

campaignplanner allows the launch of several solutions for mmissionplanner and all possible associtated obsplanner configurations.

campaignplanner\.campaignplan module
----------------------------------

.. automodule:: campaignplanner.campaignplan
    :members:
    :undoc-members:
    :show-inheritance:


campaignplanner\.launch_campaign module
----------------------------------

.. automodule:: campaignplanner.launch_campaign
    :members:
    :undoc-members:
    :show-inheritance:
