reduction package
===========================

Submodules
----------

reduction allows the launch of several solutions for mmissionplanner and all possible associtated obsplanner configurations.

reduction\.bands module
----------------------------------

.. automodule:: reduction.bands
    :members:
    :undoc-members:
    :show-inheritance:


reduction\.camera module
----------------------------------

.. automodule:: reduction.camera
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.lens module
----------------------------------

.. automodule:: reduction.lens
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.filter module
----------------------------------

.. automodule:: reduction.filter
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.parent_types_iau module
----------------------------------

.. automodule:: reduction.parent_types_iau
    :members:
    :undoc-members:
    :show-inheritance:

reduction\. module
----------------------------------

.. automodule:: reduction.
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.expected_skybrightness module
----------------------------------

.. automodule:: reduction.expected_skybrightness
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.make_atmosphere_from_alt module
----------------------------------

.. automodule:: reduction.make_atmosphere_from_alt
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.malbec_block_processing module
----------------------------------

.. automodule:: reduction.malbec_block_processing
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.pop-mass module
----------------------------------

.. automodule:: reduction.pop-mass
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.real_showers module
----------------------------------

.. automodule:: reduction.real_showers
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.twilight module
----------------------------------

.. automodule:: reduction.twilight
    :members:
    :undoc-members:
    :show-inheritance:

reduction\.twilight_search module
----------------------------------

.. automodule:: reduction.twilight_search
    :members:
    :undoc-members:
    :show-inheritance:
