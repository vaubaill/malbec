missionplanner package
===========================

Submodules
----------

The main program, preforming the computation of the trajectory is missionplan.

missionplanner\.missionplan module
----------------------------------

.. automodule:: missionplanner.missionplan
    :members:
    :undoc-members:
    :show-inheritance:

The Atmosphere Object needed for the computation of the trajectory is specified in the following routine.

missionplanner\.atmosphere_ram module
-------------------------------------

.. automodule:: missionplanner.atmosphere_ram
    :members:
    :undoc-members:
    :show-inheritance:

In order to consider in-flight position measurements (see also the liveobject routine), a special Atmosphere object is created:

missionplanner\.inflight_atmosphere module
------------------------------------------

.. automodule:: missionplanner.inflight_atmosphere
    :members:
    :undoc-members:
    :show-inheritance:

If the nacelle is equipped with a LoRa card sending its position every minute, the prediction of the landing point can be run each time a new nacelle position is received.


missionplanner\.liveobjects module
----------------------------------

.. automodule:: missionplanner.liveobjects
    :members:
    :undoc-members:
    :show-inheritance:

The liveobject routine is launched with the followin module:

missionplanner\.launch_liveobjects module
-----------------------------------------

.. automodule:: missionplanner.launch_liveobjects
    :members:
    :undoc-members:
    :show-inheritance:


In order to launch several instanes of the trajectory simulation at once:

missionplanner\.launch_simulti module
-------------------------------------

.. automodule:: missionplanner.launch_simulti
    :members:
    :undoc-members:
    :show-inheritance:


missionplanner\.exceptions module
-------------------------------------

.. autoexception:: missionplanner.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.density module
-------------------------------------

.. automodule:: missionplanner.density
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.groundata module
-------------------------------------

.. automodule:: missionplanner.groundata
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.grounnodata module
-------------------------------------

.. automodule:: missionplanner.grounnodata
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.trajectory module
-------------------------------------

.. automodule:: missionplanner.trajectory
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.landzone module
-------------------------------------

.. automodule:: missionplanner.landzone
    :members:
    :undoc-members:
    :show-inheritance:
    

missionplanner\.utils module
-------------------------------------

.. automodule:: missionplanner.utils
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.make_malbec_kernel module
-----------------------------------------

.. automodule:: missionplanner.make_malbec_kernel
    :members:
    :undoc-members:
    :show-inheritance:


missionplanner\.traj2lora module
-------------------------------------

.. automodule:: missionplanner.traj2lora
    :members:
    :undoc-members:
    :show-inheritance:


missionplanner\.create_config module
-------------------------------------

.. automodule:: missionplanner.create_config
    :members:
    :undoc-members:
    :show-inheritance:

missionplanner\.atmosphere_comp module
--------------------------------------

.. automodule:: missionplanner.atmosphere_comp
    :members:
    :undoc-members:
    :show-inheritance:


