stationmaker package
===========================

Submodules
----------

stationmaker allows the launch of several solutions for mmissionplanner and all possible associtated obsplanner configurations.

stationmaker\.stationmaker module
----------------------------------

.. automodule:: stationmaker.stationmaker
    :members:
    :undoc-members:
    :show-inheritance:

