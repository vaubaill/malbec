obsplanner package
===========================

Submodules
----------

Obsplanner performs the planification of observations.

obsplanner\.obsplan module
----------------------------------

.. automodule:: obsplanner.obsplan
    :members:
    :undoc-members:
    :show-inheritance:


obsplanner\.launch_obsplanner module
-------------------------------------

.. automodule:: obsplanner.launch_obsplanner
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.launch_obsplanner_multi module
-------------------------------------

.. automodule:: obsplanner.launch_obsplanner_multi
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.obsutils module
-------------------------------------

.. automodule:: obsplanner.obsutils
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.create_ik module
-------------------------------------

.. automodule:: obsplanner.create_ik
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.create_tf module
-------------------------------------

.. automodule:: obsplanner.create_tf
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.create_ck module
-------------------------------------

.. automodule:: obsplanner.create_ck
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.make_id module
-------------------------------------

.. automodule:: obsplanner.make_id
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.read_grid module
-------------------------------------

.. automodule:: obsplanner.read_grid
    :members:
    :undoc-members:
    :show-inheritance:

obsplanner\.exceptions module
-------------------------------------

.. automodule:: obsplanner.exceptions
    :members:
    :undoc-members:
    :show-inheritance:
