# goal: create a tool box for malbec
import glob
import os
import logging
import numpy as np
from astropy import units as u
from astropy.io import fits
#from ccdproc import CCDData, Combiner
#from ccdproc import Combiner, ImageFileCollection,fits_ccddata_writer
#from ccdproc import wcs_project
from astropy import wcs
# astroalign as aa
import logging

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


def name2time(name):
    """Converts a file name into an astropy.time.Time Object
    
    Parameters
    ----------
    name : string
        file name
    
    Returns
    -------
    time : astropy.time.Time Object
        Time deduced from the name of the file
    
    
    """
    timestr=os.path.basename(name).split('.')[0].split('_UT')[0]
    time=Time(timestr[0:4]+'-'+timestr[4:6]+'-'+timestr[6:8]+'T'+timestr[9:11]+':'+timestr[11:13]+':'+timestr[13:15],format='isot')
    return time

def malbec_make_master_offset(offset_dir='OFFSET/',offset_file_root='offset_',master_file_name='master-offset.fits'):
    master_file     = offset_dir+master_file_name
    listimg         = ImageFileCollection(offset_dir) #,glob_include='*.fit',glob_exclude='*.fits')
    listccd         = []
    for ccd,file_name in listimg.ccds(ccd_kwargs={'unit':'adu'},return_fname=True):
        log.info('now considering file '+file_name)
        listccd.append(ccd)
    combiner        = Combiner(listccd)
    log.info('now making the median combination')
    master_offset   = combiner.median_combine()
    fits_ccddata_writer(master_offset,master_file)
    log.info( 'Result saved in '+master_file)
    return

def malbec_mkaverage(imgdir='DATA/2017',img_root_name='',img_ext_name='.fit',master_root_name='master'):

    dict_ccd_data       = {}
    dict_ccd_proj       = {}
    
    # get list of images
    list_img        = glob.glob(imgdir+img_root_name+'*'+img_ext_name)
    if not list_img:
        log.info('*** FATAL ERROR: there is no img of type '+imgdir+img_root_name+'*'+img_ext_name)
    
    # load first image for reference
    hdulist             = fits.open(list_img[0])
    ref_img_array       = np.array(hdulist[0].data)
    
    # now loops over all the files
    for fit_file in list_img:
        log.info( 'now opening file: '+fit_file)
        hdulist        = fits.open(fit_file)
        try:
            filter_name    = hdulist[0].header['FILTER']
        except:
            filter_name    = ['NONE']
        if not dict_ccd_data.has_key(filter_name):
            dict_ccd_proj[filter_name]	= []
            dict_ccd_proj[filter_name].append(CCDData(hdulist[0].data,unit=u.adu))
        else:
            proj_img_array		= aa.register(np.array(hdulist[0].data),ref_img_array)
            dict_ccd_proj[filter_name].append(CCDData(proj_img_array,unit=u.adu))

    log.info( 'now loop over the filters')
    for filter_name in dict_ccd_proj:
        combiner_proj       = Combiner(dict_ccd_proj[filter_name])     
        master_proj         = combiner_proj.median_combine()
        hdu_proj            = master_proj.to_hdu()
        master_file_proj    = imgdir+img_root_name+'-'+master_root_name+'-'+filter_name+'-proj.fits'
        hdu_proj.writeto(master_file_proj,overwrite=True)
        log.info( 'Master img saved in '+master_file_proj)
    return




def malbec_mkimgaverage(imgdir='DATA/2017',img_root_name='',img_ext_name='.fit',master_name='master',master_dir='./'):
    """Makes a simple average of images.
    
    
    Parameters
    ----------
    imgdir : str, optional
        name of directory where images are located
    img_root_name: str, optional
        root name of the image file names to take into account
    img_ext_name : str, optional
        extension name of the image files to take into account
    master_name : str, optional
        name of output master file
    master_dir : str, optional
        name of output directory
    
    """
    prog='(malbec_mkimgaverage) '
    dict_ccd_data       = {}
    
    # get list of images
    list_img        = glob.glob(imgdir+img_root_name+'*'+img_ext_name)
    if not list_img:
        log.info( '*** FATAL ERROR: there is no img of type '+imgdir+img_root_name+'*'+img_ext_name)
    
    # load first image for reference
    hdulist             = fits.open(list_img[0])
    ref_img_array       = np.array(hdulist[0].data)
    
    # now loops over all the files
    for fit_file in list_img:
        log.info( 'now opening file: '+fit_file)
        hdulist        = fits.open(fit_file)
        try:
            filter_name    = hdulist[0].header['FILTER']
        except:
            filter_name    = ''
        log.info('filter_name='+filter_name)
        if not dict_ccd_data.has_key(filter_name):
            dict_ccd_data[filter_name]  = []    # create array
            dict_ccd_data[filter_name].append(CCDData(hdulist[0].data,unit=u.adu))
        else:
            ccd_data    = hdulist[0].data
            dict_ccd_data[filter_name].append(CCDData(ccd_data,unit=u.adu))

    log.info('now loop over the filters')
    for filter_name in dict_ccd_data:
        log.info('considering filter='+filter_name)
        combiner_data       = Combiner(dict_ccd_data[filter_name])     
        master_data         = combiner_data.median_combine()
        hdu_data            = master_data.to_hdu()
        master_file_data    = master_dir+master_name
        hdu_data.writeto(master_file_data,overwrite=True)
        log.info( 'Master img saved in '+master_file_data)
    return

