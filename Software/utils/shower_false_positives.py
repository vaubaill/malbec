import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KernelDensity
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import get_sun
import time
import math

AU = 1*u.au.to(u.m)

# Average speed of Earth [km/s]
SPEED_EARTH = 29.7

def load_and_extract_columns(csv_file_path, columns_to_extract):
    # Load CSV file into a dataframe
    df = pd.read_csv(csv_file_path)

    # Dictionary to hold the extracted columns
    extracted_columns = {}

    # Loop over each column you want to extract
    for new_col_name, possible_matches in columns_to_extract.items():
        for col in df.columns:
            if col in possible_matches:
                # If a match is found, save the column to the dictionary and break
                extracted_columns[new_col_name] = df[col]
                break

    # Create a new dataframe from the extracted columns
    extracted_df = pd.DataFrame(extracted_columns)

    return extracted_df

def calculate_solar_longitude(jd):
    """Calculate solar longitude for a given Julian Date."""
    time = Time(jd, format='jd')
    sun = get_sun(time)
    sun_ecliptic = sun.geocentrictrueecliptic
    solar_longitude = sun_ecliptic.lon.deg
    return solar_longitude

def fit_kde(data, parameters, kernel):
    # Normalize the data
    scaler = StandardScaler()
    data_normalized = scaler.fit_transform(data[parameters])

    # Fitting multi-dimensional KDE
    kde = KernelDensity(kernel='gaussian', bandwidth=kernel).fit(data_normalized)

    return kde, scaler

# Modified draw_samples function to implement rejection sampling
def draw_samples_with_bounds(kde, parameters, scaler, bounds, num_samples=1):
    valid_samples = []
    while len(valid_samples) < num_samples:
        samples = kde.sample(num_samples)
        samples_unnorm = scaler.inverse_transform(samples)

        # Check if samples are within bounds
        valid = True
        for i, param in enumerate(parameters):
            if not bounds[param][0] <= samples_unnorm[0][i] <= bounds[param][1]:
                valid = False
                break
        if valid:
            valid_samples.append(samples_unnorm[0])

    return pd.DataFrame(np.array(valid_samples), columns=parameters)

# Adjusted plotting function to handle 5-dimensional KDE evaluation with proper scaling
def plot_original_vs_sampled_with_kde_adjusted(kde, original_data, sampled_data, parameters, bounds):
    fig, axes = plt.subplots(nrows=1, ncols=len(parameters), figsize=(15, 5))
    for i, param in enumerate(parameters):
        # Get histogram values for proper KDE scaling
        hist_vals, bin_edges = np.histogram(original_data[param], bins=30, density=True)
        bin_width = bin_edges[1] - bin_edges[0]

        # Plot histogram of original data
        axes[i].hist(original_data[param], bins=30, alpha=0.5, label='Original Data', density=True)

        # Plot histogram of sampled data
        axes[i].hist(sampled_data[param], bins=100, alpha=0.5, label='Sampled Data', density=True, color='red')

        # # Plot KDE curve with adjusted scaling
        # x_vals = np.linspace(bounds[param][0], bounds[param][1], 100)
        # dummy_data = np.zeros((x_vals.shape[0], len(parameters)))
        # dummy_data[:, i] = x_vals.ravel()
        # x_vals_normalized = scaler.transform(dummy_data)
        # log_dens = kde.score_samples(x_vals_normalized)
        # axes[i].plot(x_vals, np.exp(log_dens) * bin_width, '-g', label='KDE')

        # Set title and legend
        axes[i].set_title(param)
        axes[i].legend()
    plt.tight_layout()
    plt.show()

def inclination_discriminant(i1, lan1, i2, lan2):
    rad_i1 = np.deg2rad(i1);       rad_i2 = np.deg2rad(i2)
    rad_lan1 = np.deg2rad(lan1);   rad_lan2 = np.deg2rad(lan2)
    I = np.arccos( (np.cos(rad_i1) * np.cos(rad_i2)) + (np.sin(rad_i1) * np.sin(rad_i2) * np.cos(rad_lan1-rad_lan2)) )
    return I

def pi_discriminant(i1, arg_p1, lan1, i2, arg_p2, lan2):
    rad_i1 = np.deg2rad(i1);       rad_i2 = np.deg2rad(i2)
    rad_lan1 = np.deg2rad(lan1);   rad_lan2 = np.deg2rad(lan2)
    rad_arg_p1 = np.deg2rad(arg_p1);   rad_arg_p2 = np.deg2rad(arg_p2)

    I = inclination_discriminant(i1, lan1, i2, lan2)

    if np.abs(lan1 - lan2) > 180:
        asin_pi_sign = -1
    else:
        asin_pi_sign = 1

    pi = rad_arg_p1 - rad_arg_p2 + (2. * asin_pi_sign * np.arcsin(np.cos((rad_i1 + rad_i2) / 2.) * np.sin((rad_lan1 - rad_lan2) / 2.) / np.cos(I/2.)))
    return pi

def theta_discriminant(i1, arg_p1, lan1, i2, arg_p2, lan2):
    rad_i1 = np.deg2rad(i1);           rad_i2 = np.deg2rad(i2)
    rad_lan1 = np.deg2rad(lan1);       rad_lan2 = np.deg2rad(lan2)
    rad_arg_p1 = np.deg2rad(arg_p1);   rad_arg_p2 = np.deg2rad(arg_p2)

    B1 = np.arcsin( np.sin(rad_i1) * np.sin(rad_arg_p1) )
    B2 = np.arcsin( np.sin(rad_i2) * np.sin(rad_arg_p2) )

    if np.cos(rad_arg_p1) < 0:
        gamma1 = rad_lan1 + np.arctan( np.cos(rad_i1) * np.tan(rad_arg_p1) ) + np.pi
    else:
        gamma1 = rad_lan1 + np.arctan( np.cos(rad_i1) * np.tan(rad_arg_p1) )

    if np.cos(rad_arg_p2) < 0:
        gamma2 = rad_lan2 + np.arctan( np.cos(rad_i2) * np.tan(rad_arg_p2) ) + np.pi
    else:
        gamma2 = rad_lan2 + np.arctan( np.cos(rad_i2) * np.tan(rad_arg_p2) )

    theta = np.arccos( (np.sin(B1)*np.sin(B2)) + (np.cos(B1)*np.cos(B2)*np.cos(gamma1-gamma2)) )

    return theta

def d_southworks_hawkins_1963(q1, e1, i1, arg_p1, lan1, q2, e2, i2, arg_p2, lan2):
    I = inclination_discriminant(i1, lan1, i2, lan2)
    pi = pi_discriminant(i1, arg_p1, lan1, i2, arg_p2, lan2)

    d_1 = (q1 - q2) / AU
    d_2 = (e1 - e2)
    d_3 = 2. * np.sin(I/2)
    # d_3 = (2 * np.sin((rad_i2 - rad_i1) / 2))**2 + np.sin(rad_i1) * np.sin(rad_i2) * (2 * np.sin((rad_lan2 - rad_lan1) / 2))**2
    d_4 = (e1 + e2) * np.sin(pi/2.)

    d = np.sqrt(d_1**2 + d_2**2 + d_3**2 + d_4**2)

    return d

def d_criterion_drummond_1979(q1, e1, i1, arg_p1, lan1, q2, e2, i2, arg_p2, lan2):

    I = inclination_discriminant(i1, lan1, i2, lan2)
    pi = pi_discriminant(i1, arg_p1, lan1, i2, arg_p2, lan2)
    theta = theta_discriminant(i1, arg_p1, lan1, i2, arg_p2, lan2)

    d_1 = (e1 - e2) / (e1 + e2)
    d_2 = (q1 - q2) / (q1 + q2)
    d_3 = I / np.pi
    d_4 = ((e1 + e2) / 2.) * (theta / np.pi)

    d = np.sqrt(d_1**2 + d_2**2 + d_3**2 + d_4**2)

    return d

def d_jopek_1993(q1, e1, i1, arg_p1, lan1, q2, e2, i2, arg_p2, lan2):

    I = inclination_discriminant(i1, lan1, i2, lan2)
    pi = pi_discriminant(i1, arg_p1, lan1, i2, arg_p2, lan2)

    d_1 = (e1 - e2)
    d_2 = (q1 - q2) / (q1 + q2)
    d_3 = 2. * np.sin(I/2)
    d_4 = (e1 + e2) * np.sin(pi/2.)

    d = np.sqrt(d_1**2 + d_2**2 + d_3**2 + d_4**2)

    return d

def calcVgComponents(ra, dec, sol, vg):
    """ Calculates geocentric velovity (Vg) components relative to Earth velocity. All components are in
        J2000.0 and all angles are in radians. Used for calculating the Valsecchi D criteria, needed for
        Opik Vg component calculation.

    Arguments:
        ra: [double] Right ascension (rad)
        dec: [double] Declination (rad)
        sol: [double] Solar longitude (rad)
        vg: [double] Geocentric velocity (km/s)

    Return:
        [list] Geocentric velocity vector components (km/s)
    """

    # Obliquity of Earth orbit for J2000.0 (Boulet)
    earth_EPS = 0.40909280
    sin_EPS = math.sin(earth_EPS)
    cos_EPS = math.cos(earth_EPS)

    # Terrestrial longitude J2000.0
    LE = sol - math.pi
    sin_LE = math.sin(LE)
    cos_LE = math.cos(LE)

    # Calculate Vg components before rotation
    vg_x = -(vg/SPEED_EARTH)*math.cos(dec)*math.cos(ra)
    vg_y = -(vg/SPEED_EARTH)*math.cos(dec)*math.sin(ra)
    vg_z = -(vg/SPEED_EARTH)*math.sin(dec)

    output = [0, 0, 0]

    # Rotate Vg components and add them to an output vector
    output[0] =  cos_LE*vg_x + sin_LE*cos_EPS*vg_y + sin_LE*sin_EPS*vg_z
    output[1] = -sin_LE*vg_x + cos_LE*cos_EPS*vg_y + cos_LE*sin_EPS*vg_z
    output[2] =                      -sin_EPS*vg_y +        cos_EPS*vg_z

    return output

def d_valsecchi_1999(ra1, dec1, sol1, vg1, ra2, dec2, sol2, vg2, d_max=999.0):
    """ Calculate the Valsecchi D criterion between two orbits. Only parameters used are ra, dec, sol, vg,
        other are disregarded.

    Arguments:
        point1: [double pointer] container for:
            ra1: [double] right ascension, 1st orbit (radians)
            dec1: [double] declination, 1st orbit (radians)
            sol1: [double] solar longitude, 1st orbit (radians)
            vg1: [double] geocentric velocity, 1st orbit (km/s)
            q1: [double] perihelion distance of the first orbit
            e1: [double] num. eccentricity of the first orbit
            i1: [double] inclination of the first orbit (radians)
            O1: [double] longitude of ascending node of the first orbit (radians)
            w1: [double] argument of perihelion of the first orbit (radians)

        point2: [double pointer] container for:
            ra2: [double] right ascension, 2nd orbit (radians)
            dec2: [double] declination, 2nd orbit (radians)
            sol2: [double] solar longitude, 2nd orbit (radians)
            vg2: [double] geocentric velocity, 2nd orbit (km/s)
            q2: [double] perihelion distance of the second orbit
            e2: [double] num. eccentricity of the second orbit
            i2: [double] inclination of the second orbit (radians)
            O2: [double] longitude of ascending node of the second orbit (radians)
            w2: [double] argument of perihelion of the second orbit (radians)

        d_max: [double] maximum value of the criterion, if larger than that number, 999.0 will be returned
            (used for speeding up the algorithm)

    Return:
        [double] Valsecchi D criterion value
    """

    # Define weights
    w1 = 1.0
    w2 = 1.0
    w3 = 1.0

    # Calculate the Vg components relative to Earth
    vg_x1, vg_y1, vg_z1 = calcVgComponents(ra1, dec1, sol1, vg1)

    vg_x2, vg_y2, vg_z2 = calcVgComponents(ra2, dec2, sol2, vg2)

    # Reaclaulate the speeds to realtive speed to Earth
    vg1 = vg1/SPEED_EARTH
    vg2 = vg2/SPEED_EARTH

    # Primary comparison
    sqr_diff_Vg = (vg2 - vg1)**2

    if sqr_diff_Vg <= d_max:

        # Orbit 1B, choose correct quadrant
        phi1 = math.atan2(vg_x1, vg_z1)
        cos_theta1 = vg_y1/vg1

        # Orbit 2B, choose correct quadrant
        phi2 = math.atan2(vg_x2, vg_z2)
        cos_theta2 = vg_y2/vg2

        # Final comparison
        sqr_diff_cos_theta = w1 * (cos_theta2 - cos_theta1)**2

        # Secondary check against max. dissimilarity
        if (sqr_diff_cos_theta <= d_max):

            d_phi_A = 2*math.sin((phi2 - phi1)/2.0)
            d_phi_B = 2*math.sin((math.pi + phi2 - phi1)/2.0)

            d_lambda_A = 2*math.sin((sol2 - sol1)/2.0)
            d_lambda_B = 2*math.sin((math.pi + sol2 - sol1)/2.0)

            d_zeta = min(w2 * d_phi_A**2 + w3 * d_lambda_A**2,
                w2 * d_phi_B**2 + w3 * d_lambda_B**2)

            dissim = math.sqrt(sqr_diff_Vg + sqr_diff_cos_theta + d_zeta)

        else:
            return d_max

    else:
        dissim = d_max

    if (dissim > d_max):
        dissim = d_max

    return dissim

class BreakNestedLoop(Exception):
    pass

def count_associations(sampled_data, criterion, limit, shower, body):
    if shower:
        database_path = os.getcwd()+'/streamestablisheddata2022.csv'
        database_table = pd.read_csv(database_path)

        ra2 = database_table['Ra'];                i2 = database_table['inc']
        dec2 = database_table['De'];               q2 = database_table['q']
        a2 = database_table['a'];                  arg_p2 = database_table['peri']
        e2 = database_table['e'];                  lan2 = database_table['node']
        ra2_err = database_table['dRa'];           dec2_err = database_table['dDe']
        v_g2 = database_table['Vg'];
        sol2 = database_table['LoS']
    elif body: # works for tables generated from JPL HORIZONS Database
        database_path = os.getcwd()+'/jpl_horizons_all.csv'
        database_table = pd.read_csv(database_path)

        i2 = database_table['i'];        arg_p2 = database_table['w']
        q2 = database_table['q'];        lan2 = database_table['om']
        a2 = database_table['a'];        e2 = database_table['e']

        i2_err = database_table['sigma_i'];        arg_p2_err = database_table['sigma_w']
        q2_err = database_table['sigma_q'];        lan2_err = database_table['sigma_om']
        a2_err = database_table['sigma_a'];        e2_err = database_table['sigma_e']

    # Initialize count for current sample
    count = 0

    number_associations = []
    # Iterate over each sample
    for index, sample in sampled_data.iterrows():
        if criterion == 'D_v':
            ra1 = sample['RA']
            dec1 = sample['Dec']
            sol1 = sample['solar_longitude']
            v_g1 = sample['v_g']
        else:
            i1 = sample['inclination']
            q1 = sample['perihelion']
            arg_p1 = sample['argument_periapsis']
            e1 = sample['eccentricity']
            lan1 = sample['longitude_ascending_node']

        try:
            # Iterate over each data point in the database
            for i in range(len(database_table)):
                if criterion == 'D_v':
                    similarity = d_valsecchi_1999(np.deg2rad(ra1), np.deg2rad(dec1), np.deg2rad(sol1), v_g1, np.deg2rad(ra2[i]), np.deg2rad(dec2[i]), np.deg2rad(sol2[i]), v_g2[i])
                elif criterion == 'D_sh':
                    similarity = d_southworks_hawkins_1963(q1, e1, i1, arg_p1, lan1, q2[i], e2[i], i2[i], arg_p2[i], lan2[i])
                elif criterion == 'D_prime':
                    similarity = d_criterion_drummond_1979(q1, e1, i1, arg_p1, lan1, q2[i], e2[i], i2[i], arg_p2[i], lan2[i])
                elif criterion == 'D_j':
                    similarity = d_jopek_1993(q1, e1, i1, arg_p1, lan1, q2[i], e2[i], i2[i], arg_p2[i], lan2[i])

                # Check if the calculated similarity meets a certain threshold (this part may need adjustment)
                if similarity < limit:  # assuming a threshold of 0.15 as an example
                    count += 1
                    raise BreakNestedLoop   # exit nested for loop
        except BreakNestedLoop:
            pass

    return count

def characterize_false_positives(data, parameters, v_parameters, bounds, v_bounds, shower, body):
    # Define the limits and bandwidths
    # limits = [0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2]
    limits = [0.05, 0.1, 0.15, 0.2]
    bandwidths = [0.1, 0.25, 0.5, 1.0]
    criteria = ['D_v', 'D_sh', 'D_prime', 'D_j']

    # Dictionary to store results
    results = {criterion: {} for criterion in criteria}

    total_iterations = len(criteria) * len(bandwidths) * len(limits) * 100  # assuming 100 samples for each setting
    current_iteration = 0

    for criterion in criteria:
        for bandwidth in bandwidths:

            if criterion == 'D_v':
                params = v_parameters
                b = v_bounds
            else:
                params = parameters
                b = bounds

            # Fit KDE with specified bandwidth
            kde, scaler = fit_kde(data, params, bandwidth)

            for limit in limits:
                # Track time for a single iteration to estimate remaining time
                start_time = time.time()

                print(f"\nProcessing: Criterion: {criterion}, Bandwidth: {bandwidth}, Limit: {limit}")

                num_iterations = 100
                num_associations = []
                raw_associations = []
                for i in range(num_iterations):
                    sampled_data = draw_samples_with_bounds(kde, params, scaler, b, len(data))
                    sampled_data_df = pd.DataFrame(sampled_data, columns=params)
                    num_associations.append(count_associations(sampled_data_df, criterion, limit, shower, body))

                    current_iteration += 1
                    elapsed_time = time.time() - start_time
                    estimated_remaining_time = (total_iterations - current_iteration) * (elapsed_time / (i + 1))
                    print(f"\rIteration {i + 1}/{num_iterations}. Estimated Time Remaining: {estimated_remaining_time:.2f} seconds", end='')

                # calculate actual number of associations
                raw_associations.append(count_associations(data, criterion, limit, shower, body))

                # Store the mean and standard deviation
                results[criterion][(bandwidth, limit)] = (np.mean(num_associations), np.std(num_associations))
                results[criterion][('RAW', limit)] = (np.mean(raw_associations), np.std(raw_associations))

    print("\nCharacterization completed!")

    # Plotting
    plot_characterization_results1(results, len(data))
    plot_characterization_results2(results)

    return results

def plot_characterization_results1(results, N):
    fig, axes = plt.subplots(4, 1, figsize=(10, 15))

    for i, criterion in enumerate(['D_v', 'D_sh', 'D_prime', 'D_j']):
        for bandwidth in [0.1, 0.25, 0.5, 1.0]:
            # limits = [0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2]
            limits = [0.05, 0.1, 0.15, 0.2]
            means = [results[criterion][(bandwidth, limit)][0] for limit in limits]
            stds = [results[criterion][(bandwidth, limit)][1] for limit in limits]

            axes[i].errorbar(limits, np.array(means)/float(N), yerr=(3*np.array(stds))/float(N), label=f'Bandwidth: {bandwidth}', capsize=5)

        axes[i].set_title(f'Criterion: {criterion}')
        axes[i].set_xlabel('Limit')
        axes[i].set_ylabel('% of False Positives')
        axes[i].legend()

    plt.tight_layout()
    plt.show()

def plot_characterization_results2(results):
    fig, axes = plt.subplots(4, 1, figsize=(10, 15))

    for i, criterion in enumerate(['D_v', 'D_sh', 'D_prime', 'D_j']):
        for bandwidth in [0.1, 0.25, 0.5, 1.0]:
            # limits = [0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2]
            limits = [0.05, 0.1, 0.15, 0.2]
            means = [results[criterion][(bandwidth, limit)][0] for limit in limits]
            stds = [results[criterion][(bandwidth, limit)][1] for limit in limits]

            raw_matches = [results[criterion][('RAW', limit)][0] for limit in limits]

            # estimate the number of real shower by subtracting the false positive estimate from the number of matches
            estimated_real = [raw_matches[i] - means[i] for i in range(len(means))]
            axes[i].errorbar(limits, estimated_real, yerr=3*np.array(stds), label=f'Bandwidth: {bandwidth}', capsize=5)

        axes[i].set_title(f'Criterion: {criterion}')
        axes[i].set_xlabel('Limit')
        axes[i].set_ylabel('Number of actual Showers')
        axes[i].legend()

    plt.tight_layout()
    plt.show()

def main(data, parameters, criterion, limit, kernel, bounds):
    kde, scaler = fit_kde(data, parameters, kernel)

    num_associations = []
    num_iterations = 100
    start_time = time.time()

    for i in range(num_iterations):
        sampled_data = draw_samples_with_bounds(kde, parameters, scaler, bounds, len(data))
        sampled_data = pd.DataFrame(sampled_data, columns=parameters)
        num_associations.append(count_associations(sampled_data, criterion, limit, shower, body))

        # Calculate elapsed and estimated remaining time
        elapsed_time = time.time() - start_time
        estimated_remaining_time = (elapsed_time / (i + 1)) * (num_iterations - i - 1)

        # Print progress and estimated remaining time
        print(f"\rProgress: {i + 1}/{num_iterations} - Estimated remaining time: {estimated_remaining_time:.2f} seconds", end="")

    print("\nProcessing completed!")

    # Plot histogram of number of false associations
    plt.hist(num_associations, bins=np.max(num_associations), alpha=0.7, label="Number of False Associations")

    mean_value = np.mean(num_associations)
    std_value = np.std(num_associations)
    plt.axvline(mean_value, color='r', linestyle='dashed', linewidth=1, label=f"Mean: {mean_value:.2f}")
    plt.axvline(mean_value + std_value, color='g', linestyle='dotted', linewidth=1, label=f"Mean + 1 std: {mean_value + std_value:.2f}")
    plt.axvline(mean_value - std_value, color='g', linestyle='dotted', linewidth=1, label=f"Mean - 1 std: {mean_value - std_value:.2f}")

    plt.legend()
    plt.title("Histogram of False Associations")
    plt.xlabel("Number of Associations")
    plt.ylabel("Frequency")
    plt.show()

    # Save num_associations to a CSV file
    associations_df = pd.DataFrame(num_associations, columns=[['Number of Associations']])
    associations_df.to_csv('num_associations.csv', index=False)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Determine the number of random associations within a dataset with a shower or a object based on just random chance.')

    # Argument for database file
    parser.add_argument("-d", "--database", type=str, help="Database file [CSV]", required=True)

    # Argument for selecting between meteor match or small bodies
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-shower", "--meteor_match", action="store_true", help="Compare to all meteor match")
    group.add_argument("-body", "--small_bodies", action="store_true", help="Compare to all small bodies from HORIZONS")

    # Argument for criterion
    parser.add_argument("-c", "--criterion", type=str, choices=['D_sh', 'D_prime', 'D_j', 'D_v'], help="Criterion [D_sh, D_prime, D_j]")

    # Argument for criterion limit
    parser.add_argument("-l", "--limit", type=float, help="Set limit for chosen criterion. [ex. 0.105]")

    # Argument for bandwidth of kernel
    parser.add_argument("-k", "--kernel", type=float, help="Kernel bandwidth value, using gaussian kernel. [ex. 0.1]", default=0.1)

    parser.add_argument("-check", "--check_the_kde", action="store_true", help="Check that the KDE is fitting correctly by saving a plot")

    parser.add_argument("-characterize", "--characterize", action="store_true", help="Does many runs producing plots and CSVs varying the criterion, the limit, and the kernel bandwidth")

    args = parser.parse_args()

    datafile = args.database
    shower = args.meteor_match
    body = args.small_bodies
    criterion = args.criterion
    limit = args.limit
    kernel = args.kernel
    check_the_kde = args.check_the_kde
    characterize = args.characterize

    parameters = ['perihelion', 'eccentricity', 'inclination', 'argument_periapsis', 'longitude_ascending_node']

    valsecchi_parameters = ['RA', 'Dec', 'solar_longitude', 'v_g']

    columns_to_extract = {
    'perihelion': ['perihelion'],
    'eccentricity': ['eccentricity', 'e'],
    'inclination': ['inclination', 'i', 'inc'],
    'argument_periapsis': ['argument_periapsis', 'peri'],
    'longitude_ascending_node': ['longitude_ascending_node', 'node'],
    'RA': ['ra_eci_inf', 'RAgeo', 'RA_g', 'ra_j2000', 'ra_geocentric', 'RA'],
    'Dec': ['dec_eci_inf', 'DEgeo', 'Dec_g', 'dec_j2000', 'dec_geocentric', 'Dec'],
    'JD': ['JD'],
    'v_g': ['initial_velocity_geo_kms', 'Vgeo', 'V_g', 'v_g', 'vg_geocentric']
    }

    # Define the bounds for each parameter
    data = load_and_extract_columns(datafile, columns_to_extract)
    data['solar_longitude'] = data['JD'].apply(calculate_solar_longitude)

    # convert the velocities to km/s if in m/s
    v_range = np.max(data['v_g']) - np.min(data['v_g'])
    if v_range > 1000.0:
        data['v_g'] = data['v_g'] / 1000.0

    bounds = {
        'perihelion': (data['perihelion'].min(), data['perihelion'].max()),
        'eccentricity': (data['eccentricity'].min(), data['eccentricity'].max()),
        'inclination': (0.0, 180.0),
        'argument_periapsis': (0.0, 360.0),
        'longitude_ascending_node': (0.0, 360.0)
    }

    valsecchi_bounds = {
                        'RA': (0.0, 360.0),
                        'Dec': (-90.0, 90.0),
                        'solar_longitude': (0.0, 360.0),
                        'v_g': (11.2, 72.0)  # vague
    }


    if not characterize:
        if check_the_kde:
            kde, scaler = fit_kde(data, parameters, kernel)
            v_kde, v_scaler = fit_kde(data, valsecchi_parameters, kernel)

            sampled_data = draw_samples_with_bounds(kde, parameters, scaler, bounds, 5000)
            v_sampled_data = draw_samples_with_bounds(v_kde, valsecchi_parameters, v_scaler, valsecchi_bounds, 5000)

            # Plot the histograms with adjusted KDE curves
            plot_original_vs_sampled_with_kde_adjusted(kde, data, sampled_data, parameters, bounds)

            # Plot Valsecchi KDE
            plot_original_vs_sampled_with_kde_adjusted(v_kde, data, v_sampled_data, valsecchi_parameters, valsecchi_bounds)

        if criterion == 'D_v':
            parameters = valsecchi_parameters
            bounds = valsecchi_bounds
        else:
            pass

        main(data, parameters, criterion, limit, kernel, bounds)
    elif characterize:
        results = characterize_false_positives(data, parameters, valsecchi_parameters, bounds, valsecchi_bounds, shower, body)
        results_df = pd.DataFrame(results)
        results_df.to_csv('characterize_results.csv')
