import logging
import sys
import os
import shutil
from astropy.time import Time
from configparser import ConfigParser
from utils import check_file


# make default logger object
log = logging.getLogger(__name__)

def make_name(log_dir='./',root='',ext='.log',Notime=False):
    """Make a log file name from the date the program is launched.
    
    Parameters
    ----------
    log_dir : string, optional
        log file directory. Default is './'
    root : string, optional
        root of the file name (prepended to the file name). Default is empty string.
    ext : string, optional
        extension of the file name (appended to the file name). Default is '.log'
    Notime : Boolean
        If set to False (Default) a time string is appended to the name of the filename.
        If True, the filename has no time info.
    
    Returns
    -------
    filename : string
        File name based on current time, build according to: log_dir + root + time + ext if Notime is False,
        and to log_dir + root + ext if Notime is True.
    
    """
    strt = 'malbec-' + Time.now().isot.replace('-','').replace(':','').split('.')[0] 
    if Notime:
       filename = log_dir + root + ext
    else:
       filename = log_dir + root + strt + ext
    #msg =  'filename = '+filename
    return filename

def make_log(filename=None,confile=None):
    """Make a logging.Logger Object
    
    Parameters
    ----------
    filename : string, optional
        Log full path file name. If not set, an automated name will be created.
    confile : string, optional
        If set, filename is taken from the config configuration file.
    
    Returns
    -------
    log : logging.Logger Object
        Logger object
    
    """
    # set filename
    if not filename:
        filename = make_name(root='malbec')
    else:
        filename = __name__
    
    # create Logger Object
    log = logging.getLogger(filename)
    # test if config_file file exists.
    check_file(filename,log)
    
    # set filename from config object
    if confile:
        try:
            config = ConfigParser()
            config.read(confile)
            filename = config['LOG']['file']
        except:
            raise ValueError('*** Impossible to get log file name from confile '+confile)
    
    # set Logger Object
    log.setLevel(logging.DEBUG)
    log.propagate = True
    
    # log format
    log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
    
    # create StreamHandler
    sdlr = logging.StreamHandler()
    sdlr.setLevel(logging.DEBUG)
    sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    
    # create FileHandler
    hdlr = logging.FileHandler(filename)
    hdlr.setLevel(logging.DEBUG)
    hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    
    # add StreamHandler and FileHandler into logger
    log.addHandler(sdlr)
    log.addHandler(hdlr)
    
    return log

def change_alllog(name):
    """Change all the log settings
    
    Parameters
    ----------
    name : string
        name of the log root
    
    Returns
    -------
    None
        
    """
    cleaname = name.replace(' ','').replace('(','').replace(')','')
    change_logfile(cleaname)
    return


def change_logfile(log,file_name):
    """Change Fripipe default output log file name.
    
    Parameters
    ----------
    log : logging.Logger Object
        logger for which the file stream handler name should be changed.
    file_name : string
        name of the new log file.
    
    Returns
    -------
    None
        
    """
    # check file_name
    if (len(file_name)==0):
        msg = '*** FATAL ERROR: no file_name is specified.'
        log.error(msg)
        sys.exit(msg)
    
    # close fileHandler
    log.hdlr.close()
    # rename the output file
    log.info('renaming '+log.hdlr.baseFilename+' into '+os.path.abspath(file_name))
    try:
        shutil.move(log.hdlr.baseFilename,os.path.abspath(file_name))
    except:
        msg = '*** FATAL ERROR: impossible to move '+log.hdlr.baseFilename+' into '+os.path.abspath(file_name)
        log.error(msg)
        raise IOError(msg)
    # set up new file name
    log.hdlr.baseFilename = os.path.abspath(file_name)
    
    log.info('Log file changed to '+file_name)
    return
