#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 15:29:17 2024

@author: vaubaill
"""

import numpy as np
import matplotlib.pyplot as plt
# from pylab import *

a = 2.5
e = 0.77

# a = 2.5
# e = 0.96

theta = 0
while theta <= 2*np.pi:
    r = (a*(1-e**2))/(1+e*np.cos(theta))
    print("r = ",r,"theta = ",theta)
    plt.polar(theta, r,'r.')
    theta = theta + np.pi/180

plt.show()
