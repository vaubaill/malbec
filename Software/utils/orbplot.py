
from matplotlib import pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import math
from pytwobodyorbit import TwoBodyOrbit

sunmu = 1.32712440041e20

n = 200

#GEM
#orbit = TwoBodyOrbit("Gem", mu = sunmu)
#orbit.setOrbKepl(0, 1.275, 0.875, 22, 265.5, 321.5, MA = 180) #t, a, e, i, Om, om, m

#DRA
orbit = TwoBodyOrbit("Dra", mu = sunmu)
orbit.setOrbKepl(0, 3.4, 0.735, 30, 198, 171, MA = 180) #t, a, e, i, Om, om, m

#LEO
#orbit = TwoBodyOrbit("Leo", mu = sunmu)
#orbit.setOrbKepl(0, 10.4, 0.905, 167, 170, 228, MA = 180) #t, a, e, i, Om, om, m

#Encke
#orbit = TwoBodyOrbit("Encke", mu = sunmu)
#orbit.setOrbKepl(0, 2.21, 0.848, 11.78, 334.566, 186.553, MA = 180) #t, a, e, i, Om, om, m


X, Y, Z, t = orbit.points(n)
premier = True
deuxieme = False
for i in range(0, len(Z)):
    if (premier and (Z[i] < 0)):
        ind1 = i
        premier = False
        deuxieme = True
    if (deuxieme and (Z[i] >= 0)):
        ind2 = i
        deuxieme = False
x1 = -6.0
x2 = 2.0
y1 = -3.0
y2 = 2.0
z1 = -0.8
z2 = 0.5

#Terre
XT = np.linspace(x1, x2, num = n)
YT = np.linspace(y1, y2, num = n)
XT, YT = np.meshgrid(XT, YT)
ZT = np.zeros((n,n))

for i in range(0, n):
   fig = plt.figure()
   fig.patch.set_facecolor('black')
   ax = Axes3D(fig)
   ax.patch.set_facecolor('black')
   ax.autoscale(True, axis = 'both', tight= True)
   ax.xaxis.pane.fill = False
   ax.yaxis.pane.fill = False
   ax.zaxis.pane.fill = False
   ax.grid(False)
   ax.w_xaxis.line.set_color("white")
   ax.w_yaxis.line.set_color("darkgray")
   ax.w_zaxis.line.set_color("white")
   ax.set_xlim(x1, x2)
   ax.set_ylim(y1, y2)
   ax.set_zlim(z1, z2)
   ax.set_xlabel("X (ua)", fontsize = 12, color = "white")
   ax.set_ylabel("Y (ua)", fontsize = 12, color = "darkgray")
   ax.set_zlabel("Z (ua)", fontsize = 12, color = "white")
   ax.plot_surface(XT, YT, ZT, color = "yellow", alpha = 0.6)
   ax.scatter(0,0,0, color = "red")

   if i < ind1:
       ax.plot(X[0:i+1], Y[0:i+1], Z[0:i+1], c = "gold")
   elif i < ind2:
       ax.plot(X[0:ind1+1], Y[0:ind1+1], Z[0:ind1+1], c = "gold")
       ax.plot(X[ind1:i+1], Y[ind1:i+1], Z[ind1:i+1], c = "darkorange")
   else:
       ax.plot(X[0:ind1+1], Y[0:ind1+1], Z[0:ind1+1], c = "gold")
       ax.plot(X[ind1:ind2+1], Y[ind1:ind2+1], Z[ind1:ind2+1], c = "darkorange")
       ax.plot(X[ind2:i+1], Y[ind2:i+1], Z[ind2:i+1], c = "gold")

   if Z[i] > 0:
       ax.scatter(X[i], Y[i], Z[i], c = "yellow")
   else:
       ax.scatter(X[i], Y[i], Z[i], c = "darkorange")

   ax.w_xaxis.set_tick_params(which = 'both', colors = 'white', pad = -2)
   ax.w_yaxis.set_tick_params(which = 'both', colors = "darkgray")
   ax.w_zaxis.set_tick_params(which = 'both', colors = "white", pad = 2)
   ax.xaxis._axinfo['tick']['color'] = 'white'
   ax.yaxis._axinfo['tick']['color'] = 'darkgray'
   ax.zaxis._axinfo['tick']['color'] = 'white'
   fig.savefig('plots/Dra_'+str(i+1)+'.png', dpi = 200)
   plt.close(fig)
 
