"""Campaign Planner

Author
------
J. Vaubaillon, - IMCCE, 2020

Version
-------
1.0

"""
# import all necessary tools
import os
import re
import time
import copy
import shutil
import argparse
import logging
import numpy as np
from configparser import ConfigParser, ExtendedInterpolation
import multiprocessing
import spiceypy as sp
# astropy
import astropy.units as u
from astropy.time import Time,TimeDelta

# some MALBEC tools
from missionplanner import missionplan,utils
from obsplanner import obsplan
from obsplanner.exceptions import ConstraintError
from missionplanner.utils import log,log_fmt,strt,add_FileHandler

###################################################
# 
# DEFINITION OF THE CampaignPlanner OBJECT
# 
###################################################

def siteid2nacelleid(site_id):
    """Make a nacelle/spacecraft id from a launch site id.
    
    Parameters
    ----------
    site_id : int
        SPICE launch site id.
    
    Returns
    -------
    nacelle_id : int
        SPICE nacelle (spacecraft) id.
    
    
    """
    nacelle_id = - site_id*1000 - 1
    
    return nacelle_id

def make_obsplan_strid(site1,site2):
    """Make an ObsPlanner simulation id.
    
    Parameters
    ----------
    site1 : int
        Site 1 id.
    site2 : int
        Site 2 id.
        
    Returns
    -------
    obsplan_id : string
        ObsPlanner string id.
    
    """
    obsplan_strid = str(site1)+ '_' + str(site2)
    return obsplan_strid


def make_all_site_pairs(list_id):
    """Make a list of all possible pairs of sites.
    
    Parameters
    ----------
    list_id : list of int.
        List of all possible launch sites ids.
    
    Returns
    -------
    list_pairs : list of tuples.
        List of all possible launch sites id pairs.
    
    """
    list_pairs = []
    list_copy = copy.copy(list_id)
    list_copy.reverse()
    for site1 in list_id:
        for site2 in list_copy:
            if site1==site2:
                continue
            list_pairs.append((site1,site2))
    # remove half of list to avoid duplicated pairs: (1,2) is the same as (2,1)
    iend = int(len(list_pairs)/2)
    list_pairs = list_pairs[:iend]
    return list_pairs


def create_missplan_config(template,site_id,nacelle_id,launch_time,sim_dir_name,outputdir):
    """Creates a MissionPlanner configuration file.
    
    Parameters
    ----------
    template : string
        Configuration file template full name.
    site_id : int
        launch site id
    nacelle_id : float
        nacelle SPICE id
    launch_time : astropy.time.Time Object
        Time of launch.
    sim_dir_name : string
        MissionPlanner sim_dir_name.
    outputdir : string
        Output directory name.
    
    Returns
    -------
    outputfile : string
        Output configuration full file name.
    
    """
    log.debug('template: '+template)
    log.debug('site_id: '+site_id)
    log.debug('nacelle_id: '+str(nacelle_id))
    log.debug('launch_time: '+launch_time.isot)
    log.debug('sim_dir_name: '+sim_dir_name)
    log.debug('outputdir: '+outputdir)
    # read template file 
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(template)
    
    # make output file full name
    outputfile = outputdir + os.path.basename(template)
    # check if outputfile already exist and get launch time and nacelle id
    if os.path.exists(outputfile):
        log.info('Output config file already exists: checking for consistency')
        outconfig = ConfigParser(interpolation=ExtendedInterpolation())
        outconfig.read(outputfile)
        # change HOME variable
        outconfig['USER']['home'] = os.getenv('HOME')
        # retrieve trajectory SPK file name
        trajspk = outconfig['CAMPAIGN']['trajectory_sim_file'].replace('.dat','.spk')
        # check if SPK file exists
        if os.path.exists(trajspk):
            log.info('SPK file found in: '+trajspk)
            # load SPK file
            sp.furnsh(trajspk)
            # load SPICE standard kernel to get leap second
            sp.furnsh(outconfig['USER']['kernel_path']+'/standard.ker')
            # get SPICE ID
            outid = sp.spkobj(trajspk)[0]
            log.info('SPK id: '+str(outid))
            # check SPICE id
            if not (outid==nacelle_id):
                # remove spk file
                os.remove(trajspk)
                log.info('SPK file DELETED because of SPICE id inconsistency: '+str(outid)+' vs '+str(nacelle_id)+' '+trajspk)
            else:
                # get launch time
                cov = sp.spkcov(trajspk,outid)
                start,stop = sp.wnfetd(cov,0)
                # change launch time for consistency with past data so that config data reflect trajectory data.
                launch_time = Time(sp.et2utc(start,"ISOC",2,50),scale='utc')
                log.warning('Changing launch time to that set in SPK file: '+launch_time.isot)
        else:
            log.debug('SPK file '+trajspk+' does not already exist')
    
    # update output directory name
    config.set('CAMPAIGN', 'sim_dir_name',sim_dir_name)
    
    # update nacelle_name
    config.set('CAMPAIGN', 'nacelle_name','MALBEC'+site_id)
    
    # update nacelle SPICE id
    config.set('CAMPAIGN', 'nacelle_id',str(nacelle_id))
    
    # update log file name
    config.set('CAMPAIGN', 'log_file_root','${USER:log_dir}/MissPlan-YYYYMMDDTHHMMSS-'+site_id+'.log')
    
    # update trajectory file name
    config.set('CAMPAIGN', 'trajectory_sim_file','${out_dir}/trajectory-'+site_id+'.dat')
    
    # update config launch site
    config.set('INITCOND', 'initcond_site',site_id)
    
    # update config launch time
    config.set('INITCOND', 'initcond_time',launch_time.isot)
    
    # create output directory if needed
    if not os.path.isdir(outputdir):
        log.info('Making directory: '+outputdir)
        os.makedirs(outputdir)
    
    # write output config file
    with open(outputfile,'w') as outconfigfile:
        config.write(outconfigfile)
    
    log.info('missplan config file created id: '+outputfile)
    
    return outputfile

def create_obsplan_config(template,site_id1,site_id2,az_off,el_off): #,outputdir):
    """Creates a MissionPlanner configuration file.
    
    Parameters
    ----------
    template : string
        Configuration file template full name.
    site_id : int
        launch site id
    nacelle_id : float
        nacelle SPICE id
    az_off : astropy.units.Quantity object.
        Camera Azimuth offset.
    el_off : astropy.units.Quantity object.
        Camera Elevation offset.
    outputdir : string
        Output directory name.
    
    Returns
    -------
    outputfile : string
        Output obsplanner configuration full file name.
    
    """
    log.debug('template: '+template)
    log.debug('site_id1: '+str(site_id1))
    log.debug('site_id2: '+str(site_id2))
    log.debug('az_off: '+str(az_off))
    log.debug('el_off: '+str(el_off))
    # read template file 
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(template)
    
    # change HOME variable
    config['USER']['home'] = os.getenv('HOME')
    
    # update reading output True/False
    config.set('USER', 'read_res','True')
    
    # update nacelleX_dir and nacelleX_name
    for (key,site_id,onetwo) in zip(['nacelle1_name','nacelle2_name'],[site_id1,site_id2],['1','2']):
        key_name = 'nacelle'+onetwo+'_name'
        key_dir = 'nacelle'+onetwo+'_dir'
        # set nacelle name
        nacelle_name = config['CAMPAIGN'][key_name] # ex: NACELLE5
        try:
            nacelle_id = re.findall(r'\d+',nacelle_name)[0] # ex: 5 in NACELLE5
            nacelle_name = nacelle_name.replace(nacelle_id,str(site_id)) # ex: NACELLE27
        except:
            pass
        config.set('CAMPAIGN',key_name,nacelle_name)
        # retrieve nacelle_dir
        nacelle_dir = config['CAMPAIGN'][key_dir]
        # retrieve local directory
        missplan_updir = config['CAMPAIGN']['missplan_updir']+'/'
        nacelle_dir_root = nacelle_dir.replace(missplan_updir,'')#.replace('/','') # ex: MissionPlanSim-Ceiling3
        try:
            nacelle_dir_id = re.findall(r'\d+',nacelle_dir_root)[0] # ex: 3
            nacelle_dir_root = nacelle_dir_root.replace(nacelle_dir_id,str(site_id))
        except:
            pass
        nacelle_dir = '${missplan_updir}/'+nacelle_dir_root+'/'
        config.set('CAMPAIGN',key_dir,nacelle_dir)
        
    # update camera info
    config.set('CAMERA', 'az_off',str(int(az_off.to('deg').value)))
    config.set('CAMERA', 'el_off',str(int(el_off.to('deg').value)))
    
    # create output directory if needed
    outputdir = config['CAMPAIGN']['out_dir']
    if not os.path.isdir(outputdir):
        log.info('Making directory: '+outputdir)
        os.makedirs(outputdir)
    
    # make output config file full name
    outputfile = outputdir + os.path.basename(template)
    # check if outputfile already exist
    if os.path.exists(outputfile):
        log.info('removing already existing config file '+outputfile)
        os.remove(outputfile)
    
    # write output config file
    with open(outputfile,'w') as outconfigfile:
        config.write(outconfigfile)
    
    log.info('obsplan config file created id: '+outputfile)
    
    return outputfile










class CampaignPlanner (object):
    """CampaignPlanner Object definition
    
    Parameters
    ----------
    config : string
        full name of configuration parameters file. Default is None.
    
    
    """
    
    # initializes some variables with default values, such as e.g None
    __version__='1.0'
    
    log_set = False
    check_list = False
    
    def __init__(self,config_file):
        """Initialize CampaignPlanner Object.
        
        Parameters
        ----------
        config : string
            full name of configuration parameters file. Default is None.
        
        Returns
        -------
        None
        
        """
        # creates configparser.ConfigParser() object for the configuration file
        self.config = ConfigParser(interpolation=ExtendedInterpolation())
        
        # if set, read configuration file
        if config_file:
            # set and read config
            self.set_and_read_config(config_file)
        else:
            msg = '*** FATAL ERROR: configuration file is mandatory.'
            raise ValueError(msg)
        
        return
    
    def set_and_read_config(self,config_file):
        """Set and read the configuration file.
        
        Parameters
        ----------
        config_file : string, optional
            full name of configuration parameters file.
        
        Returns
        -------
        None.
        
        """
        # set config_file.
        self.config_file = config_file
        # test if config_file file exists.
        utils.check_file(self.config_file,log)
        # read the configuration file BEFORE setting the logger since log file name is in config file.
        self.config.read(self.config_file)
        # set the home variable
        try:
            self.config['USER']['home'] = os.getenv('HOME')
        except:
            msg = 'FATAL ERROR: impossible to find home in configuration file '+config_file
            log.error(msg)
            raise ValueError(msg)
        
        # set logger object BEFORE checking the configuration file so log messages can be output.
        logdict = {'DEBUG' : logging.DEBUG,
                   'INFO' : logging.INFO,
                   'WARNING' : logging.WARNING,
                   'ERROR' : logging.ERROR}
        if not self.log_set:
            try:
                self.set_log(level=logdict[self.config['USER']['log_level']])
            except:
                msg = '*** FATAL ERROR: impossible to set logger level'
                raise ValueError(msg)
        
        # check the configuration file
        self.check_config()
        
        # prepare for the MissionPlan simulations
        self.preps_MP()
        
        # prepare for the ObsPlan simulations
        self.preps_OP()
        
        return
    
    def set_log(self,level=logging.INFO):
        """Set Logger object.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        self.log_level = level
        # set log level
        log.setLevel(self.log_level)
        
        try:
            self.log_file = self.config['COMMON']['log_file_root'].replace('YYYYMMDDTHHMMSS',strt)
        except:
            msg = 'FATAL ERROR: impossible to find log_file_root in configuration file '+config_file
            log.error(msg)
            raise ValueError(msg)
        
        # if LOG directory does not exist make one
        if not os.path.isdir(self.config['USER']['log_dir']):
            os.makedirs(self.config['USER']['log_dir'])
        

        # make FileHandler
        add_FileHandler(self.log_file)
        
        # set the log_set parameter
        self.log_set = True
        
        # log the log file name
        log.info('Log file: '+self.log_file)
        return
    
    def check_config(self):
        """Check and reads the configuration file.
        
        Make sure all necessary parameters are present.
        Raise ValueError if a parameters is missing.
        
        Parameters
        ---------
        None.
        
        Returns
        -------
        None
        
        """
        sections = {'USER' : ['home','proj_dir','soft_dir','conf_dir','data_dir','log_dir','kernel_path','spice_exe_path'],
                    'CAMPAIGN' : ['name','camp_dir_name','sites','nacelle_id_start','max_deltaT'],
                    'COMMON' : ['out_dir','log_file_root','sites_file','sites_type'],
                    'MISSIONPLANNER' : ['missplan_dir','missplan_confdir','missplan_conf_file'],
                    'OBSPLANNER' : ['obsplan_dir','obsplan_confdir','obsplan_conf_file']
                    }
        
        # check the presence of necessary keys in all section of configuration file
        for seckey in sections:
            log.debug('now checking presence of section : '+seckey+' in '+self.config_file)
            for key in sections[seckey]:
                log.debug('now checking presence of : '+key+' in section '+seckey+' in config file '+self.config_file)
                try:
                    tmp = self.config[seckey][key]
                    log.debug('key: '+tmp)
                except:
                    msg = '*** FATAL ERROR: '+key+' is missing from '+seckey+' section of configuration file '+self.config_file
                    log.error(msg)
                    raise ValueError(msg)
        
        # check the presence of necessary paths names in USER section of configuration file
        user_dirs = ['home','proj_dir','soft_dir','conf_dir','data_dir','log_dir','kernel_path','spice_exe_path']
        for key in user_dirs:
            utils.check_dir(self.config['USER'][key],log)
        
        # make directories if necessary
        list_dir2make = [self.config['COMMON']['out_dir'],]
        for dir2make in list_dir2make:
            log.debug('now checking existence of directory: '+dir2make)
            if not os.path.isdir(dir2make):
                msg = 'Warning: directory '+dir2make+' does not exists -> making one now.'
                log.warning(msg)
                os.makedirs(dir2make)
        
        # copy configuration file into output directory
        src = self.config_file
        dst = self.config['COMMON']['out_dir']+'/'+os.path.basename(src)
        shutil.copyfile(src,dst)
        log.info('File :'+src+' has been copied to '+dst)
        
        # save SPICE paths into global variable
        self.kernel_path = self.config['USER']['kernel_path']
        self.spice_exe_path = self.config['USER']['spice_exe_path']
        
        # check existence of necessary files
        list_filetocheck = [self.config['COMMON']['sites_file'],
                            self.config['MISSIONPLANNER']['missplan_conf_file'],
                            self.config['OBSPLANNER']['obsplan_conf_file']]
        for filetocheck in list_filetocheck:
            # check existence of file
            utils.check_file(filetocheck,log)
        
        # copy sites file into output directory
        shutil.copyfile(self.config['COMMON']['sites_file'],self.config['COMMON']['out_dir']+'/'+os.path.basename(self.config['COMMON']['sites_file']))
        log.info(self.config['COMMON']['sites_file']+' has been copied to '+self.config['COMMON']['out_dir']+'/')
        # copy missionplanner template config file into output directory
        dst = self.config['COMMON']['out_dir']+'/missplan-template-'+os.path.basename(self.config['MISSIONPLANNER']['missplan_conf_file'])
        shutil.copyfile(self.config['MISSIONPLANNER']['missplan_conf_file'],dst)
        log.info(self.config['MISSIONPLANNER']['missplan_conf_file']+' has been copied to '+dst)
        # copy obsplanner template config file into output directory
        dst = self.config['COMMON']['out_dir']+'/obsplan-template-'+os.path.basename(self.config['OBSPLANNER']['obsplan_conf_file'])
        shutil.copyfile(self.config['OBSPLANNER']['obsplan_conf_file'],dst)
        log.info(self.config['OBSPLANNER']['obsplan_conf_file']+' has been copied to '+dst)
        
        # set self.go_MP and check if self.config['COMMON']['sites_type'] is set or not
        self.go_MP = True
        try:
            if self.config['COMMON']['sites_type'].upper() in ['FIXED','MOMET']:
                self.go_MP = False
        except:
            pass
        
        # creates configparser.ConfigParser() objects
        self.sitesconf = ConfigParser(interpolation=ExtendedInterpolation())
        self.missplanconf = ConfigParser(interpolation=ExtendedInterpolation())
        self.obsplanconf = ConfigParser(interpolation=ExtendedInterpolation())
        # read the different configuration files
        self.sitesconf.read(self.config['COMMON']['sites_file'])
        self.missplanconf.read(self.config['MISSIONPLANNER']['missplan_conf_file'])
        self.obsplanconf.read(self.config['OBSPLANNER']['obsplan_conf_file'])
        # set HOME for all configuration files
        self.missplanconf['USER']['home'] = os.getenv('HOME')
        self.obsplanconf['USER']['home'] = os.getenv('HOME')
        
        # check sites files
        self.check_sites()
        
        return
    
    def check_sites(self):
        """Check each useful parameter of the sites configuration file.
        
        Sets the self.landingsites and self.nsites parameters.
        Raises ValueError if any error is encountered.
        Note that the existence and opening of the file have already been performed.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # list of key to input into landing site dictionnary
        listkey = ['id','lon','lat','alt','type','name','tel']
        self.nsites = 0
        keepgoing = True
        while keepgoing:
            param = 'SITE'+str(self.nsites+1)
            log.debug('checking site: '+param)
            # check that the SITE number is present in the file
            try:
                site = self.sitesconf[param]
                # test each of the key needed for each site
                for key in listkey:
                    try:
                        tmp = site[key]
                        log.debug('Site '+param+' has required key '+key+' : '+tmp)
                    except:
                        msg = '*** FATAL ERROR: site '+param+' is lacking key: '+key
                        log.error(msg)
                        raise ValueError(msg)
            except:
                log.warning('*** Impossible to find '+param+' in '+self.config['COMMON']['sites_file'])
                break
            self.nsites = self.nsites+1
        log.info('There are '+str(self.nsites)+' possible landing sites recorded in '+self.config['COMMON']['sites_file'])
        return
    
    def preps_MP(self):
        """Prepare for the MissionPlan simulations.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        # retrieve the sites ids
        self.list_sites_id = self.config['CAMPAIGN']['sites'].split(',')
        log.debug(self.list_sites_id)
        self.num_site = len(self.list_sites_id)
        log.info('There are '+str(self.num_site)+' sites to consider')
        
        # make list of nacelle SPICE ids
        self.list_sites_id_int = [ int(tmp) for tmp in self.list_sites_id ]
        self.list_nacelle_id = [ siteid2nacelleid(sid) for sid in self.list_sites_id_int ]
        log.info('list_nacelle_id: '+str(self.list_nacelle_id))
        
        # make list of MissionPlanner output directory
        self.list_missplan_simdirname = list(map(lambda a : self.config['CAMPAIGN']['camp_dir_name']+'/'+self.missplanconf['CAMPAIGN']['sim_dir_name'].replace('/','')+a+'/' ,self.list_sites_id))
        log.debug('list_missplan_simdirname: '+str(self.list_missplan_simdirname))
        self.list_missplan_outdir = list(map(lambda a : self.config['USER']['data_dir']+self.config['CAMPAIGN']['name']+'/'+a,self.list_missplan_simdirname))
        log.debug('list_missplan_outdir: '+str(self.list_missplan_outdir))
        
        # make list of launch time
        self.launch_time_ref = Time(self.missplanconf['INITCOND']['initcond_time'],format='isot',scale='utc')
        self.dt = TimeDelta((np.random.rand(self.num_site) * float(self.config['CAMPAIGN']['max_deltaT']) - float(self.config['CAMPAIGN']['max_deltaT'])/2)*u.s)
        self.list_launch_time = self.launch_time_ref + self.dt
        log.debug('launch_time_ref: '+str(self.launch_time_ref))
        log.debug('dt: '+str(self.dt.to('s')))
        log.debug('list_launch_time: '+str(self.list_launch_time))
        
        # make a missionplan configuration file for each site id
        self.list_missplan_conf = []
        for (site_id,nacelle_id,launch_time,simdirname,outputdir) in zip(self.list_sites_id,self.list_nacelle_id,self.list_launch_time,self.list_missplan_simdirname,self.list_missplan_outdir):
            self.list_missplan_conf.append(create_missplan_config(self.config['MISSIONPLANNER']['missplan_conf_file'],site_id,nacelle_id,launch_time,simdirname,outputdir))
        log.info('list_missplan_conf: '+str(self.list_missplan_conf))
        
        # check which MissionPlanner simulations have already been run
        self.list_MPisrun = []
        for missplan_conf in self.list_missplan_conf:
            tmp_conf = ConfigParser(interpolation=ExtendedInterpolation())
            tmp_conf.read(missplan_conf)
            tmp_conf['USER']['home'] = os.getenv('HOME')
            tmp_traj = tmp_conf['CAMPAIGN']['trajectory_sim_file'].replace('.dat','.spk')
            log.debug('Trajectory SPK file for '+missplan_conf+'  : '+tmp_traj)
            self.list_MPisrun.append(True if os.path.exists(tmp_traj) else False)
        log.info('list_MPisrun:'+str(self.list_MPisrun))
        
        return
    
    def preps_OP(self):
        """Prepare for the ObsPlan simulations.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # make a list of all possible site pairs
        self.OP_site_pairs = make_all_site_pairs(self.list_sites_id)
        # make a list of all possible azimuth
        AZmin = float(self.config['OBSPLANNER']['az_off_min'])*u.deg
        AZmax = float(self.config['OBSPLANNER']['az_off_max'])*u.deg
        AZstep = float(self.config['OBSPLANNER']['az_off_step'])*u.deg
        n_az = int((AZmax.to('deg').value - AZmin.to('deg').value) / AZstep.to('deg').value  ) + 1
        list_az = np.linspace(AZmin.to('deg').value,AZmax.to('deg').value,n_az,endpoint=True)*u.deg
        # make list of all possible elevation offsets
        list_el = None
        try:
            ELmin = float(self.config['OBSPLANNER']['el_off_min'])*u.deg
            ELmax = float(self.config['OBSPLANNER']['el_off_max'])*u.deg
            ELstep = float(self.config['OBSPLANNER']['el_off_step'])*u.deg
            n_el = int((ELmax.to('deg').value - ELmin.to('deg').value) / ELstep.to('deg').value  ) + 1
            list_el = np.linspace(ELmin.to('deg').value,ELmax.to('deg').value,n_el,endpoint=True)*u.deg
        except:
            log.warning('No elevation info in config file.')
        
        # make a list of all possible obsplanner configuration full path file
        self.list_obsplan_conf = []
        for (site1_id,site2_id) in self.OP_site_pairs:
            # for fixed stations, i.e. in MoMet configuration
            if self.config['COMMON']['sites_type'].upper()=='MOMET':
                log.debug('Configuration for MOMET type')
                for az in list_az:
                    for el in list_el:
                        if az.value==0.0:
                            el = 0.0*u.deg
                        else:
                            el = ELmax
                        log.debug('Create config file for az='+str(az)+' el='+str(el))
                        conf_file = create_obsplan_config(self.config['OBSPLANNER']['obsplan_conf_file'],site1_id,site2_id,az,el)
                        if not conf_file in self.list_obsplan_conf:
                            self.list_obsplan_conf.append(conf_file)
            else:
                for az in list_az:
                    self.list_obsplan_conf.append(create_obsplan_config(self.config['OBSPLANNER']['obsplan_conf_file'],site1_id,site2_id,az,0.0*u.deg))
        log.info('list_obsplan_conf: '+str(self.list_obsplan_conf))
        
        # make a list of all obsplanner output directories
        self.list_obsplan_outdir = list(map(lambda a : os.path.dirname(a)+'/' ,self.list_obsplan_conf))
        
        # check which ObsPlanner simulations have already been run
        self.list_OPisrun = []
        for obsplan_conf in self.list_obsplan_conf:
            tmp_conf = ConfigParser(interpolation=ExtendedInterpolation())
            tmp_conf.read(obsplan_conf)
            tmp_conf['USER']['home'] = os.getenv('HOME')
            tmp_decid = tmp_conf['CAMPAIGN']['decision_file']
            log.info('decision file '+tmp_decid+' exists? '+str(os.path.exists(tmp_decid)))
            self.list_OPisrun.append(True if os.path.exists(tmp_decid) else False)
        log.info('list_OPisrun:'+str(self.list_OPisrun))
        
        return
        
    
    
    def launch(self):
        """Launches the Campaign Planner opertions.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        
        # launch all missionplanner
        if self.go_MP:
            self.launch_MP()
        
        # launch all obsplanner
        self.launch_OP()
        
        # read all obsplanner results
        # TODO: develop this method
        #self.conclude()
        
        log.info('Done')
        return
    
    def launch_MP(self):
        """Launches the MissionPlanner component of CampaignPlanner.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # get total number of processors
        num_cores = multiprocessing.cpu_count()
        # launch all MissionPlanner simulation in parallel
        with multiprocessing.Pool(num_cores) as pool:
            pool.map(self.launch_missionplanner,zip(self.list_missplan_conf,self.list_MPisrun))
        
        return
    
    def launch_OP(self):
        """Launches the ObsPlanner component of CampaignPlanner.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        
        """
        # get total number of processors
        num_cores = multiprocessing.cpu_count()
        with multiprocessing.Pool(num_cores) as pool:
            pool.map(self.launch_obsplanner,zip(self.list_obsplan_conf,self.list_OPisrun))        
        """
        
        for confisrun in zip(self.list_obsplan_conf,self.list_OPisrun):
             self.launch_obsplanner(confisrun)
        
        return
    
    def launch_missionplanner(self,conf_isrun):
        """Launches a MissionPlanner simulation.
        
        Parameters
        ----------
        conf_isrun : tuple of string and boolean
            conf: MissionPlanner configuration file
            isrun: True if the simulation has alrady been run.
            False otherwise.
        
        Returns
        -------
        None.        
        
        """
        (missplan_conf,isrun) = conf_isrun
        if not isrun:
            log.info('=============================================================')
            log.info('Launch MissionPlanner for config file: '+missplan_conf)
            log.info('=============================================================')
            # creates a MissionPlanner object
            missplan = missionplan.MissionPlanner(missplan_conf)
            # remove stream handler to avoid output conflict between processes.
            missplan.set_log(level=self.log_level,nosdlr=True,nohdlr=True)
            # launches MissionPlanner simulation
            missplan.launch()
        return
    
    def launch_obsplanner(self,conf_isrun):
        """Launches a ObsPlanner simulation.
        
        Parameters
        ----------
        conf_isrun : tuple of string and boolean
            conf: ObsPlanner configuration file
            isrun: True if the simulation has alrady been run.
            False otherwise.
        
        Returns
        -------
        None.
        
        
        """
        # creates a ObsPlanner object
        obsplanner = obsplan.ObservationPlanner()
        (obsplan_conf,isrun) = conf_isrun
        if not isrun:
            log.info('=============================================================')
            log.info('Launch ObsPlanner for config file: '+obsplan_conf)
            log.info('=============================================================')
            # Load ObsPlanner config file
            obsplanner.load(obsplan_conf,nolog=True)
            # set ObservationPlanner logging level
            obsplanner.set_log(level=self.log_level,nosdlr=True,nohdlr=True)
            # remove stream handler to avoid output conflict between processes.
            # launches ObsPlanner simulation
            try:
                obsplanner.launch()
            except ConstraintError:
                msg = 'Geometry constraints does not allow observations for configuration file: '+obsplan_conf
                log.warning(msg)
            except:
                msg = 'Something wrong happened when running obsplaner with configuration file: '+obsplan_conf
                log.error(msg)
                raise ValueError(msg)
            finally:
                # reset the obsplanner object
                obsplanner.reset()
        return
    
    
    
    def data_save(self):
        """Save the data .
        
        Parameters
        ----------
        TBF
        
        Returns
        -------
        None.
        """
        # TBF
        log.info('All data saved')
        return
    


if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='obsplanner arguments.')
    parser.add_argument('-conf',default='./conf/config.in',help='configuration file. Default is: ./conf/config.in')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.conf
    
    # set time
    launch_time = time.time()
    
    # create Object
    camplan = CampaignPlanner(config_file)
    
    # launch simulation
    camplan.launch()
    
    # print execution time
    print('The CampaignPlanner object took '+str(time.time()-launch_time)+' s to run.')
else:
    log.debug('successfully imported')
