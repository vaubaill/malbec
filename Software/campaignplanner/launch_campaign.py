#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Allows the easy launch of campaignplanner.

Created on Fri May 17 15:11:09 2019

@author: dzilkova, J. Vaubaillon, IMCCE

MALBEC project, IMCCE, KYU-ACE, ScotopicLab, CNES

Parameters
----------
None.

Returns
-------
None.

"""

# launche MissionPlanner for a chosen configuration, defined by config files.

import time
from campaignplanner import campaignplan


# set time
launch_time = time.time()


# set configuration file
#config_file = './conf/config.in'
#config_file = './conf/config_GEM2016.in'
#config_file = './conf/config_ceiling.in'
#config_file = './conf/config.in_SANDRAIOS'
config_file = './conf/config_GEM2023_momet.in'

# create Object
camplan = campaignplan.CampaignPlanner(config_file)
# launch simulation
camplan.launch()

# print execution time
print('The CampaignPlanner object took '+str(time.time()-launch_time)+' s to run.')
