"""
launch_obsplanner.py

Launch the Observation Planner tool for a chosen configuration, defined by a config file.

"""

from obsplanner.obsplan import ObservationPlanner

# set configuration file
#config_file = './conf/config.in'
#config_file = './conf/campaign_ceiling.in'
#config_file = './conf/config_GEM2016.in'
config_file = './conf/config_stationmaker.in'
#config_file = '/home/vaubaill/malbec//DATA//20161213/ObsPlan//NACELLE27_NACELLE28_-80_0_alpha7s_24mm-1.5/config_GEM2016.in'
#config_file = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/ObsPlan/CEILING-NACELLE2_NACELLE5_-80_0_acA1920-155um_12mm-1.2/campaign_ceiling.in'
#config_file = './conf/config_tests.in'

# create ObservationPlanner Object
obsplanner = ObservationPlanner(config_file)

# launch the tool
obsplanner.launch()


# do something with results (???)
