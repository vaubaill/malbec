# ObsPlanner

J. Vaubaillon, D. Zilkova, A. Rietze, IMCCE, 2019-2020

# Scope

The ObsPlanner package allows the optimization of a double-station meteor shower observation, by computing the ideal pointing directions of the cameras. Such cameras are defined by the user (sensr, lens, filter). The cameras might be mounted at fixed location, or on-board mobile platforms, such as stratospheric balloon (MALBEC project), airplane or even satellites. For this, 2 SPICE spk kernels created by e.g. [missionplanner](../missionplanner) are needed.

By default, the orientation angle is computed so that the 2 spacecraft camera point towards eachother. However, a tilt angle might be applied, and the optimum angle is computed considering such angle.
Constraints if the pointing direction of the cameras w.r.t the targeted meteor shower radiant, the Moon or even the Sun can be defined. It helps decide if a given observation is feasible given the constraints. If no constraint is desired, the user might simply set such angular constraints to negative values.

The program also computes the area (in km^2) monitored by each camera and by both cameras.
If the [AFM Fortran package](https://gitlab.com/vaubaill/AFM) is installed, the program can even simulate the observation of meteors by simulating the entry of meteoroids given the conditions of observation and the targeted meteor shower.
As an output, the expected number of detected meteors is computed for each camera and for both cameras.
Fake observations are created and can be injected into a meteor reduction software to asses the trajectory and orbit accuracy.

# HOWO to
The user runs ObsPlanner thanks to a configuration file.

## configuration file
A configuration file looks like this:

    [USER]
    # users HOME directory. if 'auto', this is set automatically thanks to the linux $HOME environment variable.
    home = auto
    # Project directory: MALBEC project directory, where all packages are installed.
    proj_dir = ${home}/malbec/
    # MALBEC Software directory
    soft_dir = ${proj_dir}/Software/
    # MALBEC Software configuration directory: this is where configuration files common to all MALBEC packages are stored, e.g. sites.in, fixed_stations.in etc.
    conf_dir = ${soft_dir}/conf/
    # Data sub-directory: data output main directory
    data_dir = ${proj_dir}/DATA/
    # simulation sub-directory name: data output sub-directory
    sim_dir_name = ObsPlan/
    # log sub-directory : logs output directory
    log_dir = ${CAMPAIGN:out_dir}
    # log level : log level.
    log_level = INFO
    # IAU established meteor shower file
    iau_shower_file = ${conf_dir}/streamestablisheddata_jv.csv
    # IMO daytime shower file
    day_shower_file = ${conf_dir}/List_daytime_showers.csv
    # IMO nighttime shower file
    night_shower_file = ${conf_dir}/List_nighttime_showers.csv
    # SPICE kernel path
    kernel_path = /astrodata/kernels/
    # SPICE utilities path
    spice_exe_path = /astrodata/exe/
    # temporary directory used to create ck kernels: MUST be short because SPICE has a restriceted number of character when issuing a command.
    tmp_dir = /tmp/
    # read output file if already exist: True/False : this allows to quickly re-do a simulation if needed.
    read_res = False
    # (re)-make sites kernel files: True/False : this allows to quickly re-do a simulation if needed.
    make_site_kernels = False

    [CAMPAIGN]
    # name of the flight campaign: this MUST be in format yyyymmdd
    name = 20190523
    # data output directory full path
    out_dir = ${USER:data_dir}/${name}/${USER:sim_dir_name}/${nacelle1_name}_${nacelle2_name}_${CAMERA:az_off}_${CAM
    ERA:roll_off}_${CAMERA:camera_name}_${CAMERA:lens_name}/
    # logging file name root
    log_file_root = ${USER:log_dir}ObsPlan-YYYYMMDDTHHMMSS.log
    # path to directory with all the MissionPlanner directories including trajectory SPK and config files for the 2 
    nacelles.
    missplan_updir = ${USER:data_dir}/${name}/CampaignPlanner/
    # nacelle1 directory containing all MissionPlanner results
    nacelle1_dir = ${missplan_updir}/MissionPlanSim2/
    # name of nacelle1
    nacelle1_name = NACELLE2
    # nacelle2 directory containing all MissionPlanner results
    nacelle2_dir = ${missplan_updir}/MissionPlanSim5/
    # name of nacelle2
    nacelle2_name = NACELLE5
    # Launch sites configuration file
    sites_file = ${USER:conf_dir}/sites.in
    # setting altitude range for observation phase
    # minimum altitude for observation [in km]
    min_obs_alt = 5
    # maximum altitude for observation [in km]
    max_obs_alt = 45
    # minimum physical distance between the nacelles [in km]
    min_obs_dist = 40
    # maximum physical distance between the nacelles [in km]
    #max_obs_dist = 120
    max_obs_dist = 200
    # minimum angular distance between the camera boresight and the radiant [in deg]
    min_bsgt_rad = 30
    # minimum angular distance between the camera boresight and the Moon [in deg]
    min_bsgt_moon = 10
    # minimum angular distance between the camera boresight and the Sun [in deg]
    min_bsgt_sun = 30
    # frequency of data storage in SPICE ck (orientation) file, in [min]
    freq = 10
    # output decision data file containing the decision data (default is ${out_dir}/decision.dat)
    decision_file = ${out_dir}/decision.dat
    # output data file containing all the geometry info (default is ${out_dir}/geometry.dat)
    geom_file = ${out_dir}/geometry.dat
    # output FOV data file (default is ${out_dir}/fov.dat)
    fov_dat_file = ${out_dir}/fov.dat
    # output FOV kml file (default is ${out_dir}/fov.kml)
    fov_kml_file = ${out_dir}/fov.kml
    # output data file of surveyed area as a function of time (default is ${out_dir}/area.dat)
    area_file = ${out_dir}/area.dat
    # output data file of fake meteors (fakeors) anchors in both fov as a function of time (default is ${out_dir}/ancinfov.dat)
    ancinfov_file = ${out_dir}/ancinfov.dat
    # output expected number of detected meteors extension file name (default is ${out_dir}/numet.dat)
    numet_file = ${out_dir}/numet.dat
    # output FOV resolution, in [deg]
    fov_step = 2.0

    [FAKEOR]
    # number of fakeor clones
    fakeors_num = 10
    # Anchor output DATA directory
    anchor_out_dir = ${CAMPAIGN:out_dir}/Anchor/
    # anchors time and location data file
    anchors_file = ${anchor_out_dir}/anchors.dat
    # Anchor SPICE id start
    anchor_id = 39900001
    # AFM executable
    afm_exe = ${USER:HOME}/AFM/AFM_poirier/exe/AFM.x
    # AFM DATA collection directory
    afm_datacol_dir = /data/processed/Fakeor/AFM_simul/AFM_poirier
    # AFM output DATA directory
    afm_out_dir = ${CAMPAIGN:out_dir}/AFM/
    # AFM data creation mode: from collection (collect) or from direct integration (integ)
    afm_data_mode = collect
    # Fakeor output DATA directory
    fakeor_out_dir = ${CAMPAIGN:out_dir}/Fakeor/
    # Fakeor summary output file
    fakeor_file = ${fakeor_out_dir}/Fakeors.dat
    # Fakeor SPICE id start
    fakeor_id = -900001
    # Fakeor minimum mass to simulate in [kg]
    fakeor_min_mass = 1.0E-08
    # Fake observations file extension (default is obs.dat)

    [SHOWER]
    # shower IAU name
    shower_name = Arietids
    # shower IAU code
    shower_code = ARI
    # shower IAU number
    shower_num = 171
    # year of observation of shower (ex: 2020)
    shower_year = 2019
    # expected time of maximum of shower, in isot format: YYYY-MM-DDTHH:MM:SS.SSS
    max_time = 2019-05-23T09:10:00
    # fwhm value for shower in [hour]
    shower_fwhm = 2.0
    # expected value for ZHR for shower (optional, if not in IMO list), in [hr-1]
    zhr_expected = 100
    # shower entry velocity in [m/s]
    shower_V = 41.1
    # shower population index
    shower_r = 2.2
    # radiant right ascension (optional, but mandatory when not provided from IAU catalogue), in [deg]
    radiant_ra = 42.0
    # radiant declination (optional, but mandatory when not provided from IAU catalogue), in [deg]
    radiant_dec = +25.0
    # radiant drift right ascension (optional, but mandatory when not provided from IAU catalogue), in [deg/day]
    radiant_drift_ra = 1.0
    # radiant drift declination (optional, but mandatory when not provided from IAU catalogue), in [deg/day]
    radiant_drift_dec = 0.4
    # zhr range for reading IMO nighttime shower list for zhr value 'Var'
    zhr_night_range = 5-10
    # average altitude of meteors, in [km]
    meteor_alt = 110

    [CAMERA]
    # name of camera used for flight campaign
    camera_name = acA1920-155um
    # brand of camera used fo flight campaign
    camera_brand = Basler
    # name of lens used for flight campaign
    lens_name = 6mm-1.2
    # brand of lens used for flight campaign
    lens_brand = Pentax
    # filter type/name used for flight campaign
    filter_name = 66670
    # brand of filter used for flight campaign
    filter_brand = EdmundOptics
    # offset azimuth of camera for second pointing direction in deg
    az_off = -80
    # offset elevation of camera for second pointing direction in deg
    el_off = 0
    # offset roll
    roll_off = 0
    # camera frame rate [s-1]
    fps = 25.0
    # expected limiting magnitude
    lim_mag = 4.0

# how to launch the simulation

The default syntax is: `python obsplan.py [-conf conf/config.in]`.
However, one might as well modify the launch_obsplanner.py script and run:`python launch_obsplanner.py`.
Last but not least, in order to run several simulations at once, the user might modify the launch_obsplanner_multi.py script and launch: `python launch_obsplanner_multi.py`.


