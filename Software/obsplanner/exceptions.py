"""Exception modules for the ObsPlanner sub-project.

Authors:
Jeremie Vaubaillon (IMCCE, France)
The MALBEC project, 2020

"""

class ObsPlanError(Exception):
    """Base class for exceptions in this module."""
    pass

class ConstraintError(ObsPlanError):
    """Exception raised when obsplanner geometry does not allow
    observations given the constraints defined in the config file.

    Attributes:
        message: string
            Explanation of the error
    """

    def __init__(self,message):
        self.message = message
