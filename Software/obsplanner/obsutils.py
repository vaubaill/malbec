#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 08:05:28 2021

@author: vaubaill
"""
#import logging
import numpy as np
import matplotlib.pyplot as plt
from astropy.table import QTable
import astropy.units as u
import spiceypy as sp
from missionplanner.utils import log


def make_histimeplot(ets,et_ref,pltfile,rng=None,nbins=20):
    """Make plot of time histogram.
    
    Parameters
    ----------
    ets : array of float
        Times in SPICE format [sec after J2000].
    et_ref : float
        Reference time in SPICE format.
    pltfile : string
        Output file name.
    rng : 2-array of float, optional
        Range. Default is None, meaning that range
        will be computed from min/max value of ets.
    nbins : int, optional
        Number of bins. Default is 20.
    """
    
    if rng:
        rng_min = rng[0]
        rng_max = rng[1]
    else:
        rng_min = ((np.min(ets) - et_ref) * u.s).to('min').value
        rng_max = ((np.max(ets) - et_ref) * u.s).to('min').value
    fig=plt.figure()
    # compute relative time
    rel_t = (ets - et_ref) * u.s
    # make histogram of relative time
    n_rel_t, bins_rel_t, patches = plt.hist(rel_t.to('min').value, nbins, range=(rng_min,rng_max), facecolor='g', alpha=0.75)
    plt.title('Fakeor time wrt shower max.',color='indianred')
    plt.xlabel('Time [min]')
    plt.ylabel('Number of Fakeors []')
    #plt.gcf().autofmt_xdate()
    plt.savefig(pltfile,dpi=300)
    plt.close(fig)
    return


def make_grid_data_qtable():
    """Make grid data QTable.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    grid_data : astropy.table.QTable object.
        Empty grid data QTable.        
    """
    names = ['Time','et','nac_name',
             'ctrn_lon','ctrn_lat','ctrn_alt',
             'crn1_lon','crn1_lat','crn1_alt',
             'crn2_lon','crn2_lat','crn2_alt',
             'crn3_lon','crn3_lat','crn3_alt',
             'crn4_lon','crn4_lat','crn4_alt',
             'az','el','dist','area','Lgrnd']
    dtpyes = ['object','float','object',
             'float','float','float',
             'float','float','float',
             'float','float','float',
             'float','float','float',
             'float','float','float',
             'float','float','float','float','bool']
    grid_data = QTable(names=names,dtype=dtpyes)
    set_grid_units(grid_data)
    # set format
    grid_data['Time'].info.format = '%23s'
    grid_data['et'].info.format = '13.3f'
    grid_data['ctrn_lon'].info.format = '11.7f'
    grid_data['ctrn_lat'].info.format = '11.7f'
    grid_data['ctrn_alt'].info.format = '9.2f'
    grid_data['crn1_lon'].info.format = '11.7f'
    grid_data['crn2_lon'].info.format = '11.7f'
    grid_data['crn3_lon'].info.format = '11.7f'
    grid_data['crn4_lon'].info.format = '11.7f'
    grid_data['crn1_lat'].info.format = '11.7f'
    grid_data['crn2_lat'].info.format = '11.7f'
    grid_data['crn3_lat'].info.format = '11.7f'
    grid_data['crn4_lat'].info.format = '11.7f'
    grid_data['crn1_alt'].info.format = '9.2f'
    grid_data['crn2_alt'].info.format = '9.2f'
    grid_data['crn3_alt'].info.format = '9.2f'
    grid_data['crn4_alt'].info.format = '9.2f'
    grid_data['az'].info.format = '7.3f'
    grid_data['el'].info.format = '7.3f'
    grid_data['dist'].info.format = '8.1f'
    grid_data['area'].info.format = '12.1f'
    return grid_data

def set_grid_units(grid_data):
    """Set fov_data units.
    
    Parameters
    ----------
    grid_data : astropy.table.QTable object.
        grid data QTable.        
    
    Returns
    -------
    None.
    
    """
    # set units
    grid_data['ctrn_lon'].unit = u.deg
    grid_data['ctrn_lat'].unit = u.deg
    grid_data['ctrn_alt'].unit = u.m
    grid_data['crn1_lon'].unit = u.deg
    grid_data['crn2_lon'].unit = u.deg
    grid_data['crn3_lon'].unit = u.deg
    grid_data['crn4_lon'].unit = u.deg
    grid_data['crn1_lat'].unit = u.deg
    grid_data['crn2_lat'].unit = u.deg
    grid_data['crn3_lat'].unit = u.deg
    grid_data['crn4_lat'].unit = u.deg
    grid_data['crn1_alt'].unit = u.m
    grid_data['crn2_alt'].unit = u.m
    grid_data['crn3_alt'].unit = u.m
    grid_data['crn4_alt'].unit = u.m
    grid_data['az'].unit = u.deg
    grid_data['el'].unit = u.deg
    grid_data['dist'].unit = u.km
    grid_data['area'].unit = u.km**2
    return


def get_grid_data(time,nacelle_name,grid_data):
    """Get grid data at a specific time and nacelle.
    
    Parameters
    ----------
    time : astropy.time.Time object
        Time.
    nacelle_name : string
        Nacelle name
    grid_data : astropy.table.QTable
        Grid datafor all times and nacelles.
    
    Returns
    -------
    sub_sub_grid_data : astropy.table.QTable
        Subset of grid_data at specific time and nacelle name.
    
    """
    # compute et
    et = sp.str2et(time.isot)
    # select fov_data at time et
    mask = grid_data['et']==et
    sub_grid_data = grid_data[mask]
    
    # if there is no data, retrieve data at closest time et
    if not len(sub_grid_data):
        # compute time in grid_data the closest to et
        et_closest = grid_data['et'][np.argmin(np.abs(et - grid_data['et']))]
        mask = grid_data['et']==et_closest
        sub_grid_data = grid_data[mask]
    
    # if it all fails...
    if not len(sub_grid_data):
        msg = '*** Impossible to retrieve grid_data for time: '+time.isot
        log.error(msg)
        raise ValueError(msg)
    
    # now select nacelle_name
    mask = sub_grid_data['nac_name']==nacelle_name
    sub_sub_grid_data = sub_grid_data[mask]
    if not len(sub_sub_grid_data):
        msg = '*** Impossible to retrieve grid_data at time: '+time.isot+' for nacelle: '+nacelle_name
        log.error(msg)
        raise ValueError(msg)
    
    return sub_sub_grid_data


def make_reduced_area_data_qtable():
    """Make reduced are data QTable.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    grid_data : astropy.table.QTable object.
        grid data QTable.        
    """
    names = ['airmasses','dm_atm','dm_dist','dm_appvel','dm_tot','f_area_loss','red_area']
    dtpyes = ['float','float','float','float','float','float','float']
    redarea_data = QTable(names=names,dtype=dtpyes)
    # set units
    redarea_data['dm_atm'].unit = u.mag
    redarea_data['dm_dist'].unit = u.mag
    redarea_data['dm_appvel'].unit = u.mag
    redarea_data['dm_tot'].unit = u.mag
    redarea_data['red_area'].unit = u.km**2
    # set format
    redarea_data['airmasses'].info.format = '8.3f'
    redarea_data['dm_atm'].info.format = '5.3f'
    redarea_data['dm_tot'].info.format = '5.3f'
    redarea_data['dm_dist'].info.format = '5.3f'
    redarea_data['dm_atm'].info.format = '5.3f'
    redarea_data['dm_appvel'].info.format = '5.3f'
    redarea_data['red_area'].info.format = '12.1f'     
    redarea_data['f_area_loss'].info.format = '5.3f'
    return redarea_data



def make_geometry_output_table():
    """Make output QTable.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    geom_data : astropy.table.QTable object.
        Empty QTable with coloumn names and types.
    
    """
    geom_data = QTable(names=['Time','et','Dphy','Az12',
                            'Cam1Ra','Cam1Dec',
                            'Cam2Ra','Cam2Dec',
                            'RadRa','RadDec','SepRad1','SepRad2',
                            'MooRa','MooDec','SepMoo1','SepMoo2',
                            'SunRa','SunDec','SepSun1','SepSun2',
                            'Cam1Az','Cam1Alt','Cam2Az','Cam2Alt',
                            'RadAz1','RadAlt1','RadAz2','RadAlt2',
                            'MooAz1','MooAlt1','MooAz2','MooAlt2',
                            'SunAz1','SunAlt1','SunAz2','SunAlt2'],
                    dtype=['object','float','float','float',
                            'float','float',
                            'float','float',
                            'float','float','float','float',
                            'float','float','float','float',
                            'float','float','float','float',
                            'float','float','float','float',
                            'float','float','float','float',
                            'float','float','float','float',
                            'float','float','float','float'])
    return geom_data

def set_geometry_output_table_unit_and_format(geom_data):
    """Set output QTable units and format.
    
    Parameters
    ----------
    geom_data : astropy.table.QTable object.
        Empty QTable without units and formats.
    
    Returns
    -------
    geom_data : astropy.table.QTable object.
        QTable with units and formats.
    
    """
    # set units
    geom_data['Dphy'].unit = 'km'
    geom_data['Az12'].unit = 'deg'
    geom_data['Cam1Ra'].unit = 'deg'
    geom_data['Cam2Ra'].unit = 'deg'
    geom_data['Cam1Dec'].unit = 'deg'
    geom_data['Cam2Dec'].unit = 'deg'
    geom_data['RadRa'].unit = 'deg'
    geom_data['RadDec'].unit = 'deg'
    geom_data['SepRad1'].unit = 'deg'
    geom_data['SepRad2'].unit = 'deg'
    geom_data['MooRa'].unit = 'deg'
    geom_data['MooDec'].unit = 'deg'
    geom_data['SepMoo1'].unit = 'deg'
    geom_data['SepMoo2'].unit = 'deg'
    geom_data['SunRa'].unit = 'deg'
    geom_data['SunDec'].unit = 'deg'
    geom_data['SepSun1'].unit = 'deg'
    geom_data['SepSun2'].unit = 'deg'
    geom_data['Cam1Az'].unit = 'deg'
    geom_data['Cam2Az'].unit = 'deg'
    geom_data['Cam1Alt'].unit = 'deg'
    geom_data['Cam2Alt'].unit = 'deg'
    geom_data['RadAz1'].unit = 'deg'
    geom_data['RadAz2'].unit = 'deg'
    geom_data['RadAlt1'].unit = 'deg'
    geom_data['RadAlt2'].unit = 'deg'
    geom_data['MooAz1'].unit = 'deg'
    geom_data['MooAz2'].unit = 'deg'
    geom_data['MooAlt1'].unit = 'deg'
    geom_data['MooAlt2'].unit = 'deg'
    geom_data['SunAz1'].unit = 'deg'
    geom_data['SunAz2'].unit = 'deg'
    geom_data['SunAlt1'].unit = 'deg'
    geom_data['SunAlt2'].unit = 'deg'
    
    # set format
    geom_data['Time'].info.format = '%23s'
    geom_data['et'].info.format = '13.3f'
    geom_data['Dphy'].info.format = '7.3f'
    geom_data['Az12'].info.format = '7.3f'
    geom_data['Cam1Ra'].info.format = '7.3f'
    geom_data['Cam2Ra'].info.format = '7.3f'
    geom_data['Cam1Dec'].info.format = '7.3f'
    geom_data['Cam2Dec'].info.format = '7.3f'
    geom_data['RadRa'].info.format = '7.3f'
    geom_data['RadDec'].info.format = '7.3f'
    geom_data['SepRad1'].info.format = '7.3f'
    geom_data['SepRad2'].info.format = '7.3f'
    geom_data['MooRa'].info.format = '7.3f'
    geom_data['MooDec'].info.format = '7.3f'
    geom_data['SepMoo1'].info.format = '7.3f'
    geom_data['SepMoo2'].info.format = '7.3f'
    geom_data['SunRa'].info.format = '7.3f'
    geom_data['SunDec'].info.format = '7.3f'
    geom_data['SepSun1'].info.format = '7.3f'
    geom_data['SepSun2'].info.format = '7.3f'
    geom_data['Cam1Az'].info.format = '7.3f'
    geom_data['Cam2Az'].info.format = '7.3f'
    geom_data['Cam1Alt'].info.format = '7.3f'
    geom_data['Cam2Alt'].info.format = '7.3f'
    geom_data['RadAz1'].info.format = '7.3f'
    geom_data['RadAz2'].info.format = '7.3f'
    geom_data['RadAlt1'].info.format = '7.3f'
    geom_data['RadAlt2'].info.format = '7.3f'
    geom_data['MooAz1'].info.format = '7.3f'
    geom_data['MooAz2'].info.format = '7.3f'
    geom_data['MooAlt1'].info.format = '7.3f'
    geom_data['MooAlt2'].info.format = '7.3f'
    geom_data['SunAz1'].info.format = '7.3f'
    geom_data['SunAz2'].info.format = '7.3f'
    geom_data['SunAlt1'].info.format = '7.3f'
    geom_data['SunAlt2'].info.format = '7.3f'
    return 

def select_geom_data(time,geom_data_all):
    """Retrieve geometrical data for specified time.
    
    Parameters
    ----------
    time : astropy.time.Time object.
        Time.
    geom_data : astropy.table.QTable
        All geometruc data QTable.
    
    Returns
    -------
    geom_data : astropy.table.QTable
        selection of geom_data for time time.
    
    """
    # retrieve geometrical data
    mask = geom_data_all['Time']==time
    geom_data = geom_data_all[mask]
    if not len(geom_data):
        #msg = '*** WARNING: nothing in geom_data for time '+time.isot
        #log.warning(msg)    
        # try with et
        et = sp.str2et(time.isot)
        mask = geom_data_all['et']==et
        geom_data = geom_data_all[mask]
        if not len(geom_data):
            #msg = '*** WARNING: nothing in geom_data for et '+str(et)
            #log.warning(msg) 
            # get time closest to et
            et_closest =  geom_data_all['et'][np.argmin(np.abs(et - geom_data_all['et']))]
            mask = geom_data_all['et']==et_closest
            geom_data = geom_data_all[mask]
    
    # double check that there is data to output
    if not len(geom_data):
        msg = '*** error: nothing in geom_data for time: '+time.isot+' et='+str(et)
        log.error(msg)
        raise ValueError(msg)
    
    return geom_data

def get_geom_data(time,nacelle_name,nacelle_num,geom_data):
    """Retrieve geometrical data for specified time.
    
    Parameters
    ----------
    time : astropy.time.Time object.
        Time.
    nacelle_name : string
        Nacelle name
    nacelle_num : int
        Nacelle number
    geom_data : astropy.table.QTable object
        Geometry QTable.
    
    Returns
    -------
    sub_sub_geom_data : astropy.table.QTable
        selection of geom_data for time and nacelle_name.
    
    """
    # retrieve geometrical data
    sub_geom_data = select_geom_data(time,geom_data)
    
    # convert into string
    num = str(nacelle_num)
    # create subset of geom_data
    sub_sub_geom_data = QTable()
    sub_sub_geom_data['Time'] = sub_geom_data['Time']
    sub_sub_geom_data['et'] = sub_geom_data['et']
    sub_sub_geom_data['Dphy'] = sub_geom_data['Dphy']
    sub_sub_geom_data['Az12'] = sub_geom_data['Az12']
    sub_sub_geom_data['CamRa'] = sub_geom_data['Cam'+num+'Ra']
    sub_sub_geom_data['CamDec'] = sub_geom_data['Cam'+num+'Dec']
    sub_sub_geom_data['RadRa'] = sub_geom_data['RadRa']
    sub_sub_geom_data['RadDec'] = sub_geom_data['RadDec']
    sub_sub_geom_data['SepRad'] = sub_geom_data['SepRad'+num]
    sub_sub_geom_data['MooRa'] = sub_geom_data['MooRa']
    sub_sub_geom_data['MooDec'] = sub_geom_data['MooDec']
    sub_sub_geom_data['SepMoo'] = sub_geom_data['SepMoo'+num]
    sub_sub_geom_data['SunRa'] = sub_geom_data['SunRa']
    sub_sub_geom_data['SunDec'] = sub_geom_data['SunDec']
    sub_sub_geom_data['SepSun'] = sub_geom_data['SepSun'+num]
    sub_sub_geom_data['CamRa'] = sub_geom_data['Cam'+num+'Ra']
    sub_sub_geom_data['CamAz'] = sub_geom_data['Cam'+num+'Az']
    sub_sub_geom_data['RadAz'] = sub_geom_data['RadAz'+num]
    sub_sub_geom_data['RadAlt'] = sub_geom_data['RadAlt'+num]
    sub_sub_geom_data['MooAz'] = sub_geom_data['MooAz'+num]
    sub_sub_geom_data['MooAlt'] = sub_geom_data['MooAlt'+num]
    sub_sub_geom_data['SunAz'] = sub_geom_data['SunAz'+num]
    sub_sub_geom_data['SunAlt'] = sub_geom_data['SunAlt'+num]
    
    return sub_sub_geom_data





def make_single_geometry_output_table():
    """Make output QTable of geometry data for a single spacecraft.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    sgl_geom_data : astropy.table.QTable object.
        Empty QTable with coloumn names, types, format and units set.
    """
    sgl_geom_data = QTable(names=['Time','et',
                            'CamRa','CamDec',
                            'CamAz','CamAlt',
                            'RadRa','RadDec',
                            'RadAz','RadAlt','SepRad',
                            'MooRa','MooDec',
                            'MooAz','MooAlt','SepMoo',
                            'SunRa','SunDec',
                            'SunAz','SunAlt','SepSun'],
                    dtype=['object','float',
                            'float','float',
                            'float','float',
                            'float','float','float',
                            'float','float',
                            'float','float','float',
                            'float','float',
                            'float','float','float',
                            'float','float'])
    set_single_geometry_output_table_unit_and_format(sgl_geom_data)
    return sgl_geom_data

def set_single_geometry_output_table_unit_and_format(sgl_geom_data):
    """Set output QTable units and format for a single spacecraft.
    
    Parameters
    ----------
    sgl_geom_data : astropy.table.QTable object.
        Empty QTable without units and formats.
    
    Returns
    -------
    sgl_geom_data : astropy.table.QTable object.
        QTable with units and formats.
    
    """
    # set units

    sgl_geom_data['CamRa'].unit = 'deg'
    sgl_geom_data['CamDec'].unit = 'deg'
    sgl_geom_data['RadRa'].unit = 'deg'
    sgl_geom_data['RadDec'].unit = 'deg'
    sgl_geom_data['SepRad'].unit = 'deg'
    sgl_geom_data['MooRa'].unit = 'deg'
    sgl_geom_data['MooDec'].unit = 'deg'
    sgl_geom_data['SepMoo'].unit = 'deg'
    sgl_geom_data['SunRa'].unit = 'deg'
    sgl_geom_data['SunDec'].unit = 'deg'
    sgl_geom_data['SepSun'].unit = 'deg'
    sgl_geom_data['CamAz'].unit = 'deg'
    sgl_geom_data['CamAlt'].unit = 'deg'
    sgl_geom_data['RadAz'].unit = 'deg'
    sgl_geom_data['RadAlt'].unit = 'deg'
    sgl_geom_data['MooAz'].unit = 'deg'
    sgl_geom_data['MooAlt'].unit = 'deg'
    sgl_geom_data['SunAz'].unit = 'deg'
    sgl_geom_data['SunAlt'].unit = 'deg'
    
    # set format
    sgl_geom_data['Time'].info.format = '%23s'
    sgl_geom_data['et'].info.format = '13.3f'
    sgl_geom_data['CamRa'].info.format = '7.3f'
    sgl_geom_data['CamDec'].info.format = '7.3f'
    sgl_geom_data['RadRa'].info.format = '7.3f'
    sgl_geom_data['RadDec'].info.format = '7.3f'
    sgl_geom_data['SepRad'].info.format = '7.3f'
    sgl_geom_data['MooRa'].info.format = '7.3f'
    sgl_geom_data['MooDec'].info.format = '7.3f'
    sgl_geom_data['SepMoo'].info.format = '7.3f'
    sgl_geom_data['SunRa'].info.format = '7.3f'
    sgl_geom_data['SunDec'].info.format = '7.3f'
    sgl_geom_data['SepSun'].info.format = '7.3f'
    sgl_geom_data['CamAz'].info.format = '7.3f'
    sgl_geom_data['CamAlt'].info.format = '7.3f'
    sgl_geom_data['RadAz'].info.format = '7.3f'
    sgl_geom_data['RadAlt'].info.format = '7.3f'
    sgl_geom_data['MooAz'].info.format = '7.3f'
    sgl_geom_data['MooAlt'].info.format = '7.3f'
    sgl_geom_data['SunAz'].info.format = '7.3f'
    sgl_geom_data['SunAlt'].info.format = '7.3f'
    return 










def make_fov_table():
    """Make a QTable for storing the field of view.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    fov_data : astropy.table.QTable object
        Empty table for FOV data
    """
    
    fov_data = QTable(names=['Time','et','nacelle_name',
                                  'brsght_lon','brsght_lat','brsght_alt','brsght_dst',
                                  'crn1_lon','crn1_lat','crn1_alt','crn1_dst',
                                  'crn2_lon','crn2_lat','crn2_alt','crn2_dst',
                                  'crn3_lon','crn3_lat','crn3_alt','crn3_dst',
                                  'crn4_lon','crn4_lat','crn4_alt','crn4_dst',
                                  'area_single'],
                      dtype=['object','float','str','float','float','float','float','float','float',
                                   'float','float','float','float','float','float','float',
                                   'float','float','float','float','float','float','float',
                                   'float'])
    # set units
    fov_data = set_fov_units(fov_data)
    # set format
    fov_data['brsght_lon'].info.format = '11.7f'
    fov_data['brsght_lat'].info.format = '11.7f'
    fov_data['brsght_alt'].info.format = '9.2f'
    fov_data['brsght_dst'].info.format = '9.1f'
    fov_data['crn1_lon'].info.format = '11.7f'
    fov_data['crn1_lat'].info.format = '11.7f'
    fov_data['crn1_alt'].info.format = '9.2f'
    fov_data['crn1_dst'].info.format = '9.1f'
    fov_data['crn2_lon'].info.format = '11.7f'
    fov_data['crn2_lat'].info.format = '11.7f'
    fov_data['crn2_alt'].info.format = '9.2f'
    fov_data['crn2_dst'].info.format = '9.1f'
    fov_data['crn3_lon'].info.format = '11.7f'
    fov_data['crn3_lat'].info.format = '11.7f'
    fov_data['crn3_alt'].info.format = '9.2f'
    fov_data['crn3_dst'].info.format = '9.1f'
    fov_data['crn4_lon'].info.format = '11.7f'
    fov_data['crn4_lat'].info.format = '11.7f'
    fov_data['crn4_alt'].info.format = '9.2f'
    fov_data['crn4_dst'].info.format = '9.1f'
    fov_data['area_single'].info.format = '13.1f'
    return fov_data

def set_fov_units(fov_data):
    """Set fov_data units.
    
    Parameters
    ----------
    fov_data :astropy.table.QTable object
        FOV data to be set.
    
    Returns
    -------
    fov_data :astropy.table.QTable object
        FOV data with set units.
    
    """
    # set units
    fov_data['brsght_lon'].unit = u.deg
    fov_data['brsght_lat'].unit = u.deg
    fov_data['brsght_alt'].unit = u.m
    fov_data['brsght_dst'].unit = u.km
    fov_data['crn1_lon'].unit = u.deg
    fov_data['crn1_lat'].unit = u.deg
    fov_data['crn1_alt'].unit = u.m
    fov_data['crn1_dst'].unit = u.km
    fov_data['crn2_lon'].unit = u.deg
    fov_data['crn2_lat'].unit = u.deg
    fov_data['crn2_alt'].unit = u.m
    fov_data['crn2_dst'].unit = u.km
    fov_data['crn3_lon'].unit = u.deg
    fov_data['crn3_lat'].unit = u.deg
    fov_data['crn3_alt'].unit = u.m
    fov_data['crn3_dst'].unit = u.km
    fov_data['crn4_lon'].unit = u.deg
    fov_data['crn4_lat'].unit = u.deg
    fov_data['crn4_alt'].unit = u.m
    fov_data['crn4_dst'].unit = u.km
    fov_data['area_single'].unit = u.km**2
    return fov_data

def create_numet_tables():
    """Create numet_data and numet_tot QTables and set units and formats.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    numet_data : astropy.table.QTable
        Table of expected number of detected meteors as a function of time.
    numet_data : astropy.table.QTable
        Table of total expected number of detected meteors.
    
    """
    # create total expected number of meteors Qtable
    numet_tot =  QTable(names=['nacelle','Numet_sgl','Numet_dbl'],dtype=['object','float','float'])
    numet_tot['Numet_sgl'].info.format = '6.2f'
    numet_tot['Numet_dbl'].info.format = '6.2f'
    
    # create expected number of meteors as function of time Qtable
    numet_data = QTable(names=['Time','et','nacelle','ZHR','HR','f_radel','f_LM','f_time','f_area_sgl','f_area_dbl','numet_sgl','numet_dbl'],
                        dtype=['object','float','object','float','float','float','float','float','float','float','float','float'])
    
    # set units
    numet_data['ZHR'].unit = 1./u.hr
    numet_data['HR'].unit = 1./u.hr
    
    return numet_data,numet_tot

def set_numet_format(numet_data):
    """Set numet_data format.
    
    Parameters
    ----------
    numet_data : astropy.table.QTable
        Table of expected number of detected meteors without format.
    
    Returns
    -------
    None.
        
    """
    #set format
    numet_data['Time'].info.format = '%23s'
    numet_data['et'].info.format = '13.3f'
    numet_data['ZHR'].info.format = '7.1f'
    numet_data['HR'].info.format = '6.2f'
    numet_data['f_radel'].info.format = '5.3f'
    numet_data['f_LM'].info.format = '5.3f'
    numet_data['f_time'].info.format = '5.3f'
    numet_data['f_area_sgl'].info.format = '5.3f'
    numet_data['f_area_dbl'].info.format = '5.3f'
    numet_data['numet_sgl'].info.format = '7.3f'
    numet_data['numet_dbl'].info.format = '7.3f'    
    
    return numet_data

def compute_12third(qty,unit=None):
    """Get the 1/3 and 2/3 of a Quantity Object.
    
    Parameters
    ----------
    qty : astropy.units.Quantity object.
        Physical parameter for which the 1/3 and 2/3 is required.
    unit : astropy.units.Unit object., optional.
        qty unit. Default is None.
    
    """
    qty_min = np.min(qty)
    qty_max = np.max(qty)
    
    if unit is not None:
        qty_min = np.min(qty.to(unit).value)
        qty_max = np.max(qty.to(unit).value)
            
    (qty_1,qty_2) = (qty_min+(qty_max-qty_min)/3.0,
                     qty_min+(qty_max-qty_min)/3.0*2.0)
    return qty_1,qty_2