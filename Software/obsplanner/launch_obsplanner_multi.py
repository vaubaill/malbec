"""Launch the Observation Planner tool for different configuration files

"""

from obsplanner.obsplan import ObservationPlanner

# TODO: write a create_config script that automatically makes several configuration files from given initial conditions:
# see the missionplanner.create_config script for an example of what can be done.
# note that you'll need to create a template.


# set template configuration file
#templat_config_file = 'conf/conf_gen/template.in'
# make several different configuration files:
# TBD

# loop over newly created configuration files and launch obsplanner

# do something with results (???)


# launche 2 instances

listconf = ['./conf/config-2-5_45.in',
            './conf/config-2-5_-45.in',
            './conf/config-5-2_45.in',
            './conf/config-5-2_-45.in']
obsplanner = ObservationPlanner()
for config_file in listconf:
    obsplanner.load(config_file)
    obsplanner.launch()
    obsplanner.reset()

"""
obsplanner1 = ObservationPlanner('./conf/config-2-5_45.in')
obsplanner1.launch()
del obsplanner1
obsplanner2 = ObservationPlanner('./conf/config-2-5_-45.in')
obsplanner2.launch()
del obsplanner2
"""

"""
obsplanner3 = ObservationPlanner('./conf/config-5-2_45.in')
obsplanner3.launch()
del obsplanner3
obsplanner4 = ObservationPlanner('./conf/config-5-2_-45.in')
obsplanner4.launch()
del obsplanner4
"""
