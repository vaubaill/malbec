"""
Goal:
-make instrument spice kernels for MALBEC nacelles

DEPENDENCIES:
-spice librairie
-spice pinpoint utility program installed


Author:
J. Vaubaillon, IMCCE, 2020, MALBEC project
A. Rietze, Univ. Oldenburg, Germany
"""
# --------------------------------------------------------------------------------
# import all necessary tools
# --------------------------------------------------------------------------------
import os
import spiceypy as sp
import numpy as np

# import Software.reduction.camera as camera
from missionplanner import utils
from missionplanner.utils import log
from obsplanner.make_id import make_camera_name,make_camera_id
from obsplanner.make_id import make_nacelle_orientation_frame_name


# set home directory
home = os.getenv('HOME') + '/'

def make_ik_filename(nacelle_name,path='./'):
    """Make ik file full name.
    
    Parameters
    ----------
    nacelle_name : string
        Nacelle name.
    path : string, optional
        Directory where output file is located.
    
    Returns
    -------
    ik_full_path : string
        SPICE instrument kernel full path file name.
    """
    return path + '/' + nacelle_name + '.ik'

# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_ik(spk_file,tf_file,camera_fov,nacelle_name='',outdir=None,outputikfile=None,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',load_stdker=True,load_spker=True,load_frame=True):
    """Create Instrument kernel file.
    
    The frame kernel is a tk (text frame kernel), which can be created
    with the create_tk routine.
    
    Parameters
    ----------
    spk_file : string
        Nacelle SPK full path file name.
    tf_file: string
        Full path nacelle SPICE frame kernel file.
    nacelle_name : string
        Nacelle SPICE name.
    camera_fov : 2-array of astropy.units.Quantity object
        Camera field of view (azimuth, elevation extents).
    nacelle_name : string, optional
        Nacelle SPICE name. This is required if the association between
        the SPICE nacelle name and its SPICE id has not been performed.
        Default is '', implying the association is already performed.
    outdir : string, optional
        Output directory name. Default is None meaning that outputikfile will be created
        in the same directory as spk_file.
    outputikfile : string, optional
        Full path output file. If not specified (default), the output ik file
        is saved in the same directory as spk_file and its name is: nacelle_name + '.ik'.
        Default is None (i.e. unspecified).
    kernel_path : string, optional
        Path to SPICE standard kernel. Default is '/astrodata/kernels/'.
    load_stdker : boolean, optional.
        If True, the SPICE standard kernel is loaded. Default is True.
    load_spker : boolean, optional.
        If True, the SPICE SPK kernel is loaded. Default is True.
    load_frame : boolean, optional.
        If True, the SPICE frame kernel is loaded. Default is True.
    
    Returns
    -------
    ik_file : string
        SPICE instrument kernel full path file name.
    ik_id : int
        SPICE instrument id.
    ik_name : string
        SPICE instrument name.
    
    """
    
    # load SPICE standard kernel if needed
    if load_stdker:
        # load standard kernel
        std_kernel_file = kernel_path + 'standard.ker'
        utils.check_file(std_kernel_file,log)
        sp.furnsh(std_kernel_file)
    
    # load nacelle spk file if needed
    if load_spker:
        utils.check_file(spk_file,log)
        sp.furnsh(spk_file)
    
    # load frame file (tf-file)
    if load_frame:
        utils.check_file(tf_file,log)
        sp.furnsh(tf_file)
    
    # get nacelle SPICE IDs
    nacelle_id = sp.spkobj(spk_file)[0]
    # if needed set nacelle SPICE names
    if not nacelle_name:
        nacelle_name = sp.bodc2n(nacelle_id)
    
    # get nacelle frame name and id.
    nacelle_orientation_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
    #nacelle_orientation_frame_id = make_nacelle_orientation_frame_id(nacelle_id)
    
    # Set output path for ik-file
    if not outputikfile:
        outputikfile = make_ik_filename(nacelle_name,path=os.path.dirname(spk_file))
        if outdir:
            outputikfile = outdir + os.path.basename(outputikfile)
    
    # delete previously created ik files
    if os.path.exists(outputikfile):
        os.remove(outputikfile)
    
    # make camera name and id
    camera_name = make_camera_name(nacelle_name)
    camera_id = make_camera_id(nacelle_id)
    
    # get fov info
    [fov_az,fov_el] = camera_fov
    
    # Compute boresight vector, pointing in the direction
    # of the X-axis of the camera/instrument frame kernel,
    # defined in create_tk routine.
    boresight = np.array([1.0,0.0,0.0]) # X-vector
    refvector = np.array([0.0,1.0,0.0]) # Y-vector
    log.info('camera_name='+camera_name)
    log.info('camera_id  ='+str(camera_id))
    log.info('fov_az='+str(fov_az))
    log.info('fov_el='+str(fov_el))
    log.info('boresight='+str(boresight))
    log.info('refvector='+str(refvector))
    
    
    # ====================================================
    # now make instrument kernel for MALBEC station camera
    with open(outputikfile, 'w') as out:
        out.write("\\begindata \n")
        out.write('    NAIF_BODY_NAME += ('+repr(camera_name)+')\n')
        out.write('    NAIF_BODY_CODE += ('+str(camera_id)+')\n\n\n')
        out.write("\\begintext \n")
        out.write("  Definition of the pointing direction and FOV of MALBEC cameras instruments. \n\n")
        out.write("   \n")
        out.write("  Author: J. Vaubaillon - IMCCE - 2020, A. Rietze, Univ. Oldenburg \n")
        out.write("  MALBEC project \n\n\n")
        out.write("Definition of the" + camera_name + " FOV: \n \n")
        out.write("\\begindata \n")
        out.write("INS" + str(camera_id) + "_FOV_FRAME = " + repr(nacelle_orientation_frame_name) + " \n")
        out.write("INS" + str(camera_id) + "_FOV_SHAPE = " + repr("RECTANGLE") + " \n")
        out.write("INS" + str(camera_id) + "_BORESIGHT = ( " + str(boresight[0]) + "   " + str(
            boresight[1]) + "  " + str(boresight[2]) + " ) \n")
        # defining the corners with the ref_vector, ref_angle and cross_angle
        out.write("INS" + str(camera_id) + "_FOV_CLASS_SPEC = " + repr("ANGLES") + " \n")
        out.write("INS" + str(camera_id) + "_FOV_REF_VECTOR = ( " + str(refvector[0]) + "   " + str(
        refvector[1]) + "   " + str(refvector[2]) + " )  \n")
        out.write("INS" + str(camera_id) + "_FOV_REF_ANGLE =  " + str(fov_az.to('deg').value / 2) + " \n")
        out.write("INS" + str(camera_id) + "_FOV_CROSS_ANGLE =  " + str(fov_el.to('deg').value / 2) + " \n")
        out.write("INS" + str(camera_id) + "_FOV_ANGLE_UNITS =  " + repr("DEGREES") + " \n\n")
        #out.write("INS" + str(camera_id) + "_MODE_NAME      = " + repr('NOMINAL') + " \n")
        #out.write("INS" + str(camera_id) + "_TRIGGER_OFFSET = " + repr('0:01:00.0') + " \n")
        #out.write("INS" + str(camera_id) + "_CYCLE_DURATION = " + repr('0:01:00.0') + " \n")
        out.write("\\begintext \n")
    log.info("data saved in " + outputikfile)
    
    return outputikfile,camera_id,camera_name


# -----------------------------------------------------------------------------
# Define some variables
# -----------------------------------------------------------------------------


def main():
    # --------------------------------------------------------------------------------
    # Define default input values
    # --------------------------------------------------------------------------------
    # set kernel path and SPICE utilities path -> better in configuration file
    # SPICE default kernel path
    kernel_path = '/astrodata/kernels/'
    # SPICE utilities path
    spice_exe_path = '/astrodata/exe/'

    # create output directory/ set output path
    # has to be adjusted according to chosen nacelle and site
    out_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/ik_kernels/'

    # set input file (config file) with information about camera/lens name:
    config_file = home + '/malbec/Software/obsplanner/conf/config.in'

    # path to spk files of both nacelles
    spk_file = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/trajectory.spk'

    # path to tf-files (frame files) of nacelle1 and nacelle2
    tf_file = '/astrodata/kernels/nacelle1.tf'

    # setting output file for ik-kernel
    outputikfile = out_dir + '/nacelle1.ik'

    # call subroutine create_ik
    create_ik(kernel_path, spice_exe_path, out_dir, config_file, spk_file1, spk_file2, tf_file1,
              tf_file2, outputikfile)

# =============================================================================
if __name__ == '__main__':
    #main()
    log.debug('fix main before running.')
else:
    log.debug('successfully imported')
