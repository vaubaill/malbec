""" Routine to create ck_files with SPICE
    create_ck.py
    
    
    _________
    INPUT/DEPENDENCIES:
    - SPICE toolkits: msopck (to convert text file containing orientation data to a CK)
    _________
    Output: ck-file
    - "Orientation data": .quaternions from which orientation matrices are formed by SPICE softtware
                          .matrices are used to rotate position vectors from a base reference frame
                           ("from" frame) into a second reference frame ("to" frame) -> C-Matrix
    
    Authors
    -------------
    A. Rietze - U. Oldenburg, Germany
    J. Vaubaillon - IMCCE
    The MALBEC project
    
    -------------
    Version
    1.0
    
    """
#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import os
import shutil
import numpy as np
import spiceypy as sp
import astropy.units as u

from missionplanner import utils
from missionplanner.utils import log
from obsplanner.make_id import make_topoframe_name,make_camera_name,make_camera_id,make_nacelle_orientation_frame_id

home = os.getenv('HOME') + '/'

def get_spice_angles(time,nacelle_name1,nacelle_id1,nacelle_topoframe_name1,nacelle_name2,nacelle_id2,nacelle_topoframe_name2,meteor_height,offset_az,offset_el,fix_el=False):
    """Compute nacelle orientation angles given SPICE conventions.
    
    Here are Boris Semenov (JPL) comment about how to do this:
    
    Given Euler_angle1 computed as LON by RECLAT and Euler_angle2 computed such that
    it's positive for a point above horizon, to point the X axis at the point interest,
    your nacelle frame should be rotated from the topo frame
    first counter-clockwise (positive) by Euler_angle1 about Z,
    then clockwise (negative) by Euler_angle2 about Y.
    Assuming that you do not have the EULER_ROTATIONS_TYPE keyword in your setup,
    the MSOPCK input file order for this case could be
    
    time, -Euler_angle2, Euler_angle1, 0.0
    
    with the EULER_ROTATIONS_ORDER set to:
    
    EULER_ROTATIONS_ORDER  = 'Y', 'Z', 'X'
    
    With these inputs MSOPCK will create the rotation from the topo frame to the nacelle frame
    
    Mtopo->nacelle = [-Euler_angle2]y * [Euler_angle1]z * [0]x
    
    using this EUL2M call
    
    EUL2M ( -Euler_angle2, Euler_angle1, 0.0, 2, 3, 1, M )
    
    Parameters
    ----------
    time : astropy.time.Time object
        Time for which we want to compute the orientation.
    nacelle_name1 : string
        Nacelle 1 name.
    nacelle_id1 :  int
        Nacelle 1 SPICE id.
    nacelle_topoframe_name1 : string
        Nacelle 1 frame name.
    nacelle_name2 : string
        Nacelle 2 name.
    nacelle_id2 :  int
        Nacelle 2 SPICE id.
    nacelle_topoframe_name2 : string
        Nacelle 2 frame name.
    meteor_height : astropy.units.Quantity object
        Meteor altitude above sea level, in [m] or [km].
    offset_az : astropy,coordinates.Angle object.
        Azimuth offset
    offset_az : astropy,coordinates.Angle object.
        Elevation offset
    fix_el : boolean, optional
        If True, elevation is set to offset_el. Default is False.
    
    Returns
    -------
    nac1_spice_angle1 : astropy.coordinates.Angle object
        Nacelle 1 azimuth angle in SPICE convention: positive counter clockwise.
        This is basically the opposite of the geographic azimuth.
    nac1_spice_angle2 : astropy.coordinates.Angle object
        Nacelle 1 azimuth angle in SPICE convention: positive counter clockwise.
        This is basically the opposite of the geographic azimuth.
    nac2_spice_angle1 : astropy.coordinates.Angle object
        Nacelle 1 elevation angle such that it is counted positive above horizon.
    nac2_spice_angle2 : astropy.coordinates.Angle object
        Nacelle 2 elevation angle such that it is counted positive above horizon.
    
    """
    # we need to compute the 3 Euler angle defining the pointing direction of the camera, in the nacelle frame.
    # See note from Boris Semenov (JPL) in doc above.
    # compute cartesian position of nacelle2 wrt nacelle1, in nacelle1 frame, in [km]
    (eph,lt) = sp.spkezp(nacelle_id2,time,nacelle_topoframe_name1,'NONE',nacelle_id1)
    # convert cartesian coordinates into relative distance,azimuth,elevation, in [km,rad,rad]
    (distance,relative_az,relative_el) = sp.reclat(eph)
    """
    note from reclat SPICE doc:
    LONG    Longitude of the input point.  This is the angle 
            between the prime meridian and the meridian
            containing the point.  The direction of increasing
            longitude is from the +X axis towards the +Y axis.
            LONG is output in radians.  The range of LONG is 
            [ -pi, pi].
    LAT     Latitude of the input point.  This is the angle from
            the XY plane of the ray from the origin through the
            point.
            LAT is output in radians.  The range of LAT is 
            [-pi/2, pi/2].
    
    Note: The nacelle1 reference frame is defined as: 'X'=North, 'Y'=West, 'Z'=vertical
    
    As a consequence:
    LONG (here called relative_az) is the azimuth towards the WEST. The geographic azimuth is therefore -LONG.
    Note that the first angle is defined in the nacelle1 frame: as a consequence there is no need
    to change it for the orientation of nacelle1, but 180 deg must be added for the orientation of nacelle2.
    Last but not least, counterclockwise rotation is considered in SPICE. So an azimuth offset of +10 deg
    results in SPICE in a rotation about Z-axis of -10 deg. Note that relative_az is already in SPICE orientation.
    """
    # define first Euler angle for camera pointing
    nac1_spice_angle1 = (+(relative_az*u.rad).to('deg')               + offset_az.to('deg'))%(360.0*u.deg)
    nac2_spice_angle1 = (+(relative_az*u.rad).to('deg') + 180.0*u.deg - offset_az.to('deg'))%(360.0*u.deg)
    
    log.debug('nac1_spice_angle1: '+str(nac1_spice_angle1))
    log.debug('nac2_spice_angle1: '+str(nac2_spice_angle1))
    
    # computes the elevation angle
    # get position of nacelle in ITRF93 frame
    (eph1,lt) = sp.spkezr(nacelle_name1,time,'ITRF93','NONE','EARTH')
    (eph2,lt) = sp.spkezr(nacelle_name2,time,'ITRF93','NONE','EARTH')
    # get subpnt at Earth ellipsoid and nacelle-to-sub_point vector obs2sub*
    (sub1,subt,obs2sub1) = sp.subpnt('NADIR/ELLIPSOID','EARTH',time,'ITRF93','NONE',nacelle_name1)
    (sub1,subt,obs2sub2) = sp.subpnt('NADIR/ELLIPSOID','EARTH',time,'ITRF93','NONE',nacelle_name2)
    # nacelle altitude above Earth ellipsoid
    alt1 = sp.vnorm(obs2sub1) *u.km
    alt2 = sp.vnorm(obs2sub2) *u.km
    # compute average altitude of the 2 nacelles
    alt = (alt1+alt2)/2.0
    # computes difference of altitude with meteors
    diffalt = meteor_height - alt
    # half-distance between the 2 nacelles
    half_dist = distance*u.km / 2.0
    # compute elevation angle taking into account the azimuth offset
    el1 = - np.arctan2(diffalt.to('km').value,half_dist.to('km').value/np.cos(offset_az.to('rad').value))*u.rad
    # set second Euler angle as positive above horizon.
    nac1_spice_angle2 = el1.to('deg') - offset_el.to('deg')
    nac2_spice_angle2 = el1.to('deg') - offset_el.to('deg')
    # change elevation if requested
    # if fix_el:
    #     nac1_spice_angle2 = offset_el
    #     nac2_spice_angle2 = offset_el
    #     log.warning('By request, Elevation is fixed to: '+str(offset_el))
    log.debug('nac1_spice_angle2: '+str(nac1_spice_angle2))
    log.debug('nac2_spice_angle2: '+str(nac2_spice_angle2))
    return nac1_spice_angle1,nac1_spice_angle2,nac2_spice_angle1,nac2_spice_angle2



# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_ck(spk_file1,frame_file1,spk_file2,frame_file2,offset_az=0.0*u.deg,offset_el=0.0*u.deg,fix_el=False,offset_roll=0.0*u.deg,meteor_height=100.0*u.km,out_ck_file1=None,out_ck_file2=None,tmp_dir='/tmp/',freq=1*u.min,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',load_stdker=True,load_spker=True,load_frame=True):
    """Create ck (orientation) kernel files for the 2 nacelles.
    
    A nacelle points towards the azimuth of the other nacelle,
    and its elevation is such that it points towards a portion
    of atmosphere where meteor occurs, between the 2 nacelles.
    Azimuth and elevation offset are consideredm if set.
    
    The created ck file is first saved in the temporary directory due to SPICE character limitation reasons
    and is then copied into file specified by out_ck_file1 and out_ck_file2 parameters.
    
    Note that this behaviour might cause some conflict if several instances of the program are run in parallel.
    This is why you can set the temporary directory thanks to the tmp_dir parameter.
    
    Parameters:
    -------------
    spk_file1 : string
        Nacelle1 SPICE spk full path file name.
    frame_file1 : string
        Nacelle1 SPICE frame full path file name (tf-file).
    spk_file2 : string
        Nacelle2 SPICE spk full path file name.
    frame_file2 : string
        Nacelle2 SPICE frame full path file name (tf-file).
    offset_az : astropy.units.Quantity object
        Azimuth offset angle. Default is 0.0*u.deg.
    offset_el : astropy.units.Quantity object
        Elevation offset angle. Default is 0.0*u.deg.
    fix_el : boolean, optional
        If True, elevation is set to offset_el. Default is False.
    offset_roll : astropy.units.Quantity object
        Roll offset angle. Default is 0.0*u.deg.
    out_ck_file1: string, optional
        Full path output ck file1. If not specified (default), the output ck file1
        is saved in the same directory as spk_file1 and its name is the same as spk_file1,
        except for the extension that is saved as '.ck'.
        Default is None (i.e. unspecified).
    out_ck_file2: string, optional
        Full path output file2. If not specified (default), the output ck file2
        is saved in the same directory as spk_file2 and its name is the same as spk_file2,
        except for the extension that is saved as '.ck'.
        Default is None (i.e. unspecified).
    freq: astropy.units.Quantity Object,optional
        frequency for which the kernel needs to be created. Default is 1 min.
    kernel_path : string, optional
        Path to SPICE standard kernel. Default is '/astrodata/kernels/'.
    spice_exe_path: String, optional
        SPICE executables directory full name.Default is '/astrodata/exe/'.
    tmp_dir : string
        temporary directory where files are created.
        This is useful if the path to the files are very long, since it causes the mkspk utility program to crash.
        Temporary files (e.g. trajectory in ascii format) and output files are first
        created in tmp_dir directory, and then the output is saved in spk_file location. Default is '/tmp/'.
    load_stdker : boolean, optional.
        If True, the SPICE standard kernel is loaded. Default is True.
    load_spker : boolean, optional.
        If True, the SPICE SPK kernel is loaded. Default is True.
    load_frame : boolean, optional.
        If True, the SPICE frame kernel is loaded. Default is True.
    
    Returns
    -------
    None.
    
    """
    # load SPICE standard kernel if needed
    if load_stdker:
        # load standard kernel
        std_kernel_file = kernel_path + 'standard.ker'
        utils.check_file(std_kernel_file,log)
        log.info('Load: '+std_kernel_file)
        sp.furnsh(std_kernel_file)
    
    # load nacelle spk file if needed
    if load_spker:
        utils.check_file(spk_file1,log)
        utils.check_file(spk_file2,log)
        log.info('Load: '+spk_file1)
        log.info('Load: '+spk_file2)
        sp.furnsh(spk_file1)
        sp.furnsh(spk_file2)
    
    # load frame file (tf-file)
    if load_frame:
        log.info('Load: '+frame_file1)
        log.info('Load: '+frame_file2)
        sp.furnsh(frame_file1)
        sp.furnsh(frame_file2)
    
    # copy frame file to tmp_dir to avoid long string in SPICE setup file.
    shutil.copy(frame_file1, tmp_dir + os.path.basename(frame_file1))
    shutil.copy(frame_file2, tmp_dir + os.path.basename(frame_file2))
    log.debug('Frame files '+frame_file1+' and '+frame_file2+' copied under '+tmp_dir)
    
    
    # make output ck file name.
    if not out_ck_file1:
        out_dir1 = os.path.dirname(spk_file1) + '/'
        ck_file_name1 = os.path.basename(spk_file1).replace('.spk','.ck')
        out_ck_file1 = tmp_dir + ck_file_name1
    else:
        out_dir1 = os.path.dirname(out_ck_file1) + '/'
        out_ck_file1 = tmp_dir + '/' + os.path.basename(out_ck_file1)
    if not out_ck_file2:
        out_dir2 = os.path.dirname(spk_file2) + '/'
        ck_file_name2 = os.path.basename(spk_file2).replace('.spk','.ck')
        out_ck_file2 = tmp_dir + ck_file_name2
    else:
        out_dir2 = os.path.dirname(out_ck_file2) + '/'
        out_ck_file2 = tmp_dir + '/' + os.path.basename(out_ck_file2)
    
    # make output temporary fake clock kernel file name
    tsc_file1 = out_ck_file1.replace('.ck','_fake.tsc')
    tsc_file2 = out_ck_file2.replace('.ck','_fake.tsc')
    # make temporary input data file name required to create the ck file.
    input_file1 = out_ck_file1.replace('.ck','_input.dat')
    input_file2 = out_ck_file2.replace('.ck','_input.dat')
    # make setup file name
    setup_file1 = out_ck_file1.replace('.ck','.ck_setup')
    setup_file2 = out_ck_file2.replace('.ck','.ck_setup')
    
    # logging the setup, ck, tsc and input file
    log.debug('ck_file1: ' + out_ck_file1)
    log.debug('ck_file2: ' + out_ck_file2)
    log.debug('tsc_file1: ' + tsc_file1)
    log.debug('tsc_file2: ' + tsc_file2)
    log.debug('input_file1: ' + input_file1)
    log.debug('input_file2: ' + input_file2)
    log.debug('setup_file1: ' + setup_file1)
    log.debug('setup_file2: ' + setup_file2)
    
    # erase previously created files
    for file2rm in [out_ck_file1,tsc_file1,input_file1,setup_file1,
                    out_ck_file2,tsc_file2,input_file2,setup_file2]:
        if os.path.exists(file2rm):
            log.debug('removing existing file: '+file2rm)
            os.remove(file2rm)
    
    # get nacelle SPICE names, id and spk coverage from spk file
    nacelle_id1 = sp.spkobj(spk_file1)[0]
    nacelle_id2 = sp.spkobj(spk_file2)[0]
    nacelle_name1 = sp.bodc2n(nacelle_id1)
    nacelle_name2 = sp.bodc2n(nacelle_id2)
    camera_name1 = make_camera_name(nacelle_name1)
    camera_name2 = make_camera_name(nacelle_name2)
    camera_id1 = make_camera_id(nacelle_id1)
    camera_id2 = make_camera_id(nacelle_id2)
    nacelle_topoframe_name1 = make_topoframe_name(nacelle_name1)
    nacelle_topoframe_name2 = make_topoframe_name(nacelle_name2)
    cov1 = sp.spkcov(spk_file1,nacelle_id1)
    cov2 = sp.spkcov(spk_file2,nacelle_id2)
    start1,stop1 = sp.wnfetd(cov1,0)
    start2,stop2 = sp.wnfetd(cov2,0)
    
    # compute intersection time coverage
    cov = sp.wnintd(cov1,cov2)
    start,stop = sp.wnfetd(cov,0)
    
    # log
    log.info("start1: "+sp.et2utc(start1,"ISOC",2,50))
    log.info("start2: "+sp.et2utc(start2,"ISOC",2,50))
    log.info("stop1: "+sp.et2utc(stop1,"ISOC",2,50))
    log.info("stop2: "+sp.et2utc(stop2,"ISOC",2,50))
    log.info("start: "+sp.et2utc(start,"ISOC",2,50))
    log.info("stop: "+sp.et2utc(stop,"ISOC",2,50))
    
    # make input_file input files.
    # first open the files
    out1 = open(input_file1,'w')
    out2 = open(input_file2,'w')
    # set start time
    time = start
    # compute orientation of cameras as a function of time
    while time < stop:
        log.debug('write ck for time: '+sp.et2utc(time,"ISOC",2,50))
        # get Euler angles
        (nac1_spice_angle1,nac1_spice_angle2,
         nac2_spice_angle1,nac2_spice_angle2) = get_spice_angles(time,
                                    nacelle_name1,nacelle_id1,nacelle_topoframe_name1,
                                    nacelle_name2,nacelle_id2,nacelle_topoframe_name2,
                                    meteor_height,offset_az,offset_el,fix_el=fix_el)
        # write Euler angle into file
        out1.write(sp.et2utc(time,"ISOC",2,50)+' '+str( offset_roll.to('deg').value)+' '+str(nac1_spice_angle2.to('deg').value)+' '+str(nac1_spice_angle1.to('deg').value)+"\n")
        out2.write(sp.et2utc(time,"ISOC",2,50)+' '+str(-offset_roll.to('deg').value)+' '+str(nac2_spice_angle2.to('deg').value)+' '+str(nac2_spice_angle1.to('deg').value)+"\n")
        # increment time
        time = time + freq.to('s').value
    # now get euler angles at stop time
    log.debug('start: '+str(start)+' '+sp.et2utc(start,"ISOC",2,50))
    log.debug('stop: '+str(stop)+' '+sp.et2utc(stop,"ISOC",2,50))
    log.debug('time: '+str(time)+' '+sp.et2utc(time,"ISOC",2,50))
    log.debug('time-stop: '+str(time-stop))
    if ((time-stop)<0.01):
        time = stop
        log.debug('write ck for time: '+sp.et2utc(time,"ISOC",2,50))
        (nac1_spice_angle1,nac1_spice_angle2,
         nac2_spice_angle1,nac2_spice_angle2) = get_spice_angles(time,
                                        nacelle_name1,nacelle_id1,nacelle_topoframe_name1,
                                        nacelle_name2,nacelle_id2,nacelle_topoframe_name2,
                                        meteor_height,offset_az,offset_el)
        out1.write(sp.et2utc(time,"ISOC",2,50)+' '+str( offset_roll.to('deg').value)+' '+str(nac1_spice_angle2.to('deg').value)+' '+str(nac1_spice_angle1.to('deg').value)+"\n")
        out2.write(sp.et2utc(time,"ISOC",2,50)+' '+str(-offset_roll.to('deg').value)+' '+str(nac2_spice_angle2.to('deg').value)+' '+str(nac2_spice_angle1.to('deg').value)+"\n")
    # close ck input files
    out1.close()
    out2.close()
    log.info("input data saved in:  "+input_file1)
    log.info("input data saved in:  "+input_file2)
    
    # make the (intermediate) setup file
    for (tsc_file,frame_file,nacelle_name,nacelle_id,camera_name,camera_id,nacelle_topoframe_name,
        input_file,setup_file,out_ck_file,out_dir) in zip([tsc_file1,tsc_file2],[frame_file1,frame_file2],
                          [nacelle_name1,nacelle_name2],[nacelle_id1,nacelle_id2],
                          [camera_name1,camera_name2],[camera_id1,camera_id2],
                          [nacelle_topoframe_name1,nacelle_topoframe_name2],[input_file1,input_file2],
                          [setup_file1,setup_file2],[out_ck_file1,out_ck_file2],[out_dir1,out_dir2]):
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            # setup data for testing with nacelle data
            # -------------------------------------------------------------------------------
            out.write("   LSK_FILE_NAME    = "+repr(kernel_path+'naif0012.tls')+"\n")
            out.write("   MAKE_FAKE_SCLK   = "+repr(tsc_file)+"\n")
            out.write("   FRAMES_FILE_NAME = "+repr(tmp_dir + os.path.basename(frame_file))+"\n")
            out.write("   INTERNAL_FILE_NAME     = " + repr('CK file for ' + str(nacelle_name)) + "\n")
            out.write("   CK_TYPE                = 3 \n")
            out.write("   CK_SEGMENT_ID          = "+repr('CK file for ' + str(nacelle_name))+"\n")
            out.write("   INSTRUMENT_ID          = "+str(make_nacelle_orientation_frame_id(nacelle_id))+"\n")
            out.write("   REFERENCE_FRAME_NAME   = "+repr(nacelle_topoframe_name)+"\n")
            out.write("   ANGULAR_RATE_PRESENT   = "+repr('NO')+"\n")
            # note: MAXIMUM_VALID_INTERVAL is augmented by a few seconds to make sure the coverage is ok.
            out.write("   MAXIMUM_VALID_INTERVAL = "+str(freq.to('s').value+10.0)+"\n")
            out.write("   INPUT_TIME_TYPE        = "+repr('UTC')+"\n")
            out.write("   INPUT_DATA_TYPE        = "+repr('EULER ANGLES')+"\n")
            out.write("   EULER_ANGLE_UNITS      = "+repr('DEGREES')+"\n")
#            out.write("   EULER_ROTATIONS_ORDER  = "+repr('Y')+"," +repr('Z')+"," +repr('X')+"\n")
            out.write("   EULER_ROTATIONS_ORDER  = "+repr('X')+","+repr('Y')+"," +repr('Z') +"\n")
            out.write("   ANGULAR_RATE_FRAME     = "+repr('REFERENCE')+"\n") #" or 'INSTRUMENT'
            out.write("   PRODUCER_ID            = "+repr('J. Vaubaillon - IMCCE, A. Rietze - U. Oldenburg')+"\n")
            out.write(" \n")
        log.info("setup saved in:  "+setup_file)
        
        # build the command
        cmd = spice_exe_path + "msopck " + setup_file + " " + input_file + " " + out_ck_file
        log.info("cmd="+cmd)
        
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg = '*** FATAL ERROR: Impossible to submit the cmd' +cmd
            log.error(msg)
            raise IOError(msg)
        
        # double check that CK file was successfully created
        if not os.path.exists(out_ck_file):
            msg = '*** FATAL ERROR: CK file: '+out_ck_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        
        # now copy temporary output files (ck, tsc, setup) into out_dir
        for file2move in [setup_file,out_ck_file,tsc_file,input_file]:
            dst = out_dir + os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            try:
                # remark: os.remove does not always work when trying to move file from one File System to another...
                log.debug('Moving '+file2move+' into '+dst)
                shutil.copy(file2move, dst)
            except:
                msg = '*** FATAL ERROR: impossible to move ' + file2move + ' into ' + dst
                log.error(msg)
                raise IOError(msg)
        log.info('SPICE CK kernel saved in: ' + out_dir + os.path.basename(out_ck_file))
    
    results = (out_dir1+os.path.basename(out_ck_file1),
               out_dir2+os.path.basename(out_ck_file2),
               out_dir1+os.path.basename(tsc_file1),
               out_dir2+os.path.basename(tsc_file2),
               cov)
    
    return results

# =============================================================================
if __name__ == '__main__':
    log.debug('To be run from obsplanner.')
else:
    log.debug('successfully imported')
