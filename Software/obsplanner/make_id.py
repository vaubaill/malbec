"""Utility to make SPICE names and ids.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project.

"""


def make_camera_name(nacelle_name):
    """Make camera instrument name from nacelle name.
    
    Parameters
    ----------
    nacelle_name : string
        Nacelle name.
    
    Returns
    -------
    camera_name : string
        Camera name.
    
    """
    return nacelle_name + '_CAMERA'

def make_camera_id(nacelle_id):
    """Make camera instrument SPICE id from nacelle id.
    
    Parameters
    ----------
    nacelle_id : int
        Nacelle SPICE id.
    
    Returns
    -------
    camera_name : int
        Camera SPICE id.
    
    """
    return nacelle_id * 1000 - 5

def make_topoframe_name(nacelle_name):
    """Make nacelle topographic-like frame name from nacelle name.
    
    Parameters
    ----------
    nacelle_name : string
        Nacelle name.
    
    Returns
    -------
    frame_name : string
        Frane name.
    
    """
    return nacelle_name + '_TOPOLIKE'

def make_topoframe_id(nacelle_id):
    """Make nacelle topographic-like frame id from nacelle id.
    
    Parameters
    ----------
    nacelle_id : int
        Nacelle SPICE id.
    
    Returns
    -------
    frame_id : int
        Frame SPICE id.
    
    """
    frame_id = nacelle_id * 1000
    return frame_id

def make_nacelle_orientation_frame_name(nacelle_name):
    """Make nacelle orientation frame name from nacelle name.
    
    Parameters
    ----------
    nacelle_name : string
        Nacelle name.
    
    Returns
    -------
    frame_name : string
        Frane name.
    
    """
    return nacelle_name + '_NACORI'

def make_nacelle_orientation_frame_id(nacelle_id):
    """Make nacelle orientation frame id from nacelle id.
    
    Parameters
    ----------
    nacelle_id : int
        Nacelle SPICE id.
    
    Returns
    -------
    frame_id : int
        Frame SPICE id.
    
    """
    return nacelle_id * 1000 - 3

def make_clock_id(nacelle_id,from_instr=False):
    """Make a spacecraft clock id from a spacecraft id.
    
    Parameters
    ----------
    nacelle_id : int
        SPICE spacecraft id.
    
    Returns
    -------
    clock_id : int
        SPICE spacecraft clock id.
    
    """
    if from_instr:
        clock_id = nacelle_id
    else:
        clock_id = int(nacelle_id/1000)
    return clock_id
