#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 13:56:11 2021

@author: vaubaill
"""

# read grid data and make some plots

import os
from configparser import ConfigParser, ExtendedInterpolation
import glob
import matplotlib.pyplot as plt
import numpy as np
from astropy.table import QTable
from astropy.time import Time
from astropy.coordinates import EarthLocation
from obsplanner import obsutils
import astropy.units as u
import spiceypy as sp

#pth = '/home/vaubaill/malbec/DATA/20190523/ObsPlan/NACELLE2_NACELLE5_-80_0_test_6mm-1.2/'
#grid_file = pth + '/MALBEC_NACELLE2_grid.dat'
#pth = '/home/vaubaill/malbec/DATA/20190523/ObsPlan/NACELLE2_NACELLE5_-75_0_test_6mm-1.2/'
#grid_file = pth + '/MALBEC_NACELLE2_grid.dat'
#pth = '/home/vaubaill/malbec/DATA/20161213/ObsPlan/NACELLE27_NACELLE28_-85_0_alpha7s_24mm-1.5_sav1deg/'
#grid_file = pth + '/MALBEC_NACELLE27_grid.dat'
#pth = '/home/vaubaill/malbec/DATA/20190523/ObsPlan/CEILING-NACELLE1_NACELLE18_-80_0_acA1920-155um_6mm-1.2/'
#grid_file = pth + '/MALBEC_NACELLE1_grid.dat'
#pth = '/home/vaubaill/malbec/DATA/20190523/ObsPlan/NACELLE5_NACELLE2_40_0_acA1920-155um_12mm-1.2/'
#grid_file = pth + '/MALBEC_NACELLE5_grid.dat'
pth = '/home/vaubaill/malbec/DATA/20190523/ObsPlan/NACELLE2_NACELLE5_-70_0_acA1920-155um_12mm-1.2/'
grid_file = pth + '/MALBEC_NACELLE2_grid.dat'
nacelle_num = '2'

if not os.path.isdir(pth):
    msg = '*** WRONG PATH'
    raise IOError(msg)

# get time of shower max
listconf = glob.glob(pth+'/config*.in')
print('listconf ',listconf)
if not len(listconf):
    config_file = glob.glob(pth+'/campaign*.in')[0]
else:
    config_file = listconf[0]
print('config_file : ',config_file)
conf = ConfigParser(interpolation=ExtendedInterpolation())
conf.read(config_file)
time_max = Time(conf['SHOWER']['max_time'],scale='utc')
print('time_max : ',time_max.isot)

# read spk file
home = os.getenv('HOME') + '/'
pattern = conf['CAMPAIGN']['nacelle'+nacelle_num+'_dir'] + '/trajectory*.spk'
pattern = pattern.replace('auto',home)
spk_file = glob.glob(pattern)[0]
print('pattern: ',pattern)
print('spk_file: ',spk_file)
sp.furnsh(conf['USER']['kernel_path'].replace('auto',home)+'/standard.ker')
sp.furnsh(spk_file)
nac_id = sp.spkobj(spk_file)[0]
nac_name = conf['CAMPAIGN']['nacelle'+nacelle_num+'_name']
crit_angle = float(conf['CAMPAIGN']['fov_step'])*u.deg

print('Readind data file '+grid_file)
#grid_data = obsutils.make_grid_data_qtable()
grid_data = QTable().read(grid_file,format='ascii.fixed_width_two_line')
obsutils.set_grid_units(grid_data)
grid_data['red_area'].unit = u.km**2
grid_data['airmasses'].unit = u.dimensionless_unscaled
grid_data['dm_atm'].unit = u.mag
grid_data['dm_dist'].unit = u.mag
grid_data['dm_appvel'].unit = u.mag
grid_data['dm_tot'].unit = u.mag
times = Time(grid_data['Time'],scale='utc')

print('Compute unique time')
utimes= np.unique(times)
print('utimes: ',utimes)
print('len(utimes): ',len(utimes))

t_rel = time_max - utimes
print('t_rel: ',t_rel)

# getting min and max
t_rel_min = (np.min(t_rel)).to('min')
t_rel_max = (np.max(t_rel)).to('min')
el_min = np.min(grid_data['el'])
el_max = np.max(grid_data['el'])
print('t_rel_min: ',t_rel_min)
print('t_rel_max: ',t_rel_max)
print('el_min: ',el_min)
print('el_max: ',el_max)


data2plt = {}
alt2plt = {}

for time in utimes:
    print('Getting data for time: ',time.isot)
    mask = grid_data['Time']==time.isot
    subdata = grid_data[mask]
    print('There are ',len(subdata),' data for this time')
    mask = (subdata['airmasses']<35.0)
    data = subdata[mask]
    print('There are ',len(data),' data for airmass<35')
    # get location of the nacelle
    et = sp.utc2et(time.isot)
    (eph, lt) = sp.spkez(nac_id,et,'ITRF93','NONE',399)
    eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
    nac_alt = "%.0f" % eloc.height.to('km').value
    print('Nacelle altitude: ',nac_alt)
    print('data[az]',data['az'])
    print('data[el]',data['el'])
    az_max = np.max(data['az'])
    az_min = np.min(data['az'])    
    az_med = (az_max+az_min)/2.0
    az_ref = data['az'][np.argmin(np.abs(data['az'] - az_med))]
    print('az_ref: ',az_ref)
    submask = np.abs(data['az']-az_ref)<crit_angle*1.1
    subdata = data[submask]
    data2plt[nac_alt] = subdata
    el = subdata['el']
    print('el: ',el)
    print('There are ',len(el),' for this azimuth')
    

# make plot
qt2plot = ['area','red_area','airmasses','dm_atm','dm_dist','dm_appvel','dm_tot']
qtdisp = ['Area','Reduced area','Air mass','Magnitude loss due to airmass','Magnitude loss due to distance','Magnitude loss due to apparent velocity','Total magnitude loss']
qtunits = ['km^2','km^2','','mag','mag','mag','mag']
for (qtname,qtdis,unit) in zip(qt2plot,qtdisp,qtunits):
    fig=plt.figure()
    print('Ploting '+qtname+' in '+unit+' vs el')
    qtsymb = ['b+','r+','g+','c+','m+','y+','k+','bx','rx','gx','cx','mx','yx','kx']
    for (key,symb) in zip(data2plt,qtsymb):
        subdata = data2plt[key]
        plt.plot(subdata['el'].to('deg').value,
                subdata[qtname].to(unit).value,symb,label='alt = '+key+'km') # ,s=1,marker='P'
    plt.title(qtdis+' versus elevation',color='indianred')
    plt.xlabel('Elevation (deg)')
    plt.ylabel(qtdis+' ('+unit+')')
    plt.legend(loc='upper right')
    outfile = pth+'/'+qtname+'-El.png'
    plt.savefig(outfile,dpi=300)
    plt.close(fig)
    print('plot saved in '+outfile)

    
    
    
    
    
    
    
    