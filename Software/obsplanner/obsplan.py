"""MALBEC multi-station Observations Planning tool.

Determine the launch sites and timing of observations.

Authors:
Athleen Rietze (IMCCE, France - Univ. Oldenburg, Germany)
Jeremie Vaubaillon (IMCCE, France)
The MALBEC project, 2020


"""

import os
import glob
import logging
import shutil
import time as tm
import simplekml
import argparse
import multiprocessing
from itertools import repeat
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
from configparser import ConfigParser, ExtendedInterpolation
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable, vstack,hstack
from astropy.coordinates import EarthLocation, get_moon, get_sun, AltAz, SkyCoord,spherical_to_cartesian
import spiceypy as sp
from pygc import great_circle

# MALBEC specific import
from missionplanner import  utils
from missionplanner.utils import log,log_fmt,add_FileHandler
from obsplanner.make_id import make_clock_id,make_camera_name, make_camera_id,make_topoframe_name, make_topoframe_id,make_nacelle_orientation_frame_id,make_nacelle_orientation_frame_name
from obsplanner.create_ck import create_ck
from obsplanner.create_ik import create_ik
from obsplanner.create_tf import create_tf
from obsplanner.create_tk import create_tk
from obsplanner.exceptions import ConstraintError
from obsplanner import obsutils
#import obsplanner.read_shower_files
from showersim import shower_utils, fakeor, anchor, afm, phot, fakeobs
from showersim.meteorshower import MeteorShower
from showersim.shower_radiant import Shower_Radiant
from reduction.filter import Filter
from reduction.lens import Lens
from reduction.camera import Camera

# set home directory
home = os.getenv('HOME') + '/'

###################################################
# 
# DEFINITION OF THE ObservationPlanner OBJECT
# 
###################################################
class ObservationPlanner (object):
    """ObservationPlanner Object definition
    
    Parameters
    ----------
    
    
    """
    # version of the program
    __version__='1.0'
    # logger set is False until set
    log_set = False
    # kml : None until set
    kml = None
    # grids : None until set
    grids = None
    # set anchors_infov and anchors_all
    anchors_bothfov = None
    anchors_all = None
    fov_kml = None
    fov_data = None
    geom_data = None
    
    def __init__(self,config_file=''):
        """Initialize ObservationPlanner Object.
        
        Parameters
        ----------
        config_file : string
            full name of configuration parameters file. Default is None.
        
        Returns
        -------
        None
        
        """
        # if needed load all necessary tools
        if config_file:
            self.load(config_file)
        return
    
    def load(self,config_file,nolog=False):
        """Load ObservationPlanner configuration file.
        
        Parameters
        ----------
        config : string
            full name of configuration parameters file. Default is None.
        nolog : boolean
            If True, log is not set. Default is False.
        
        Returns
        -------
        None
        
        """
        # set config_file.
        self.config_file = config_file
        # creates configparser.ConfigParser() object for the configuration file
        self.config = ConfigParser(interpolation=ExtendedInterpolation())
        # read the configuration file.
        if not os.path.exists(self.config_file):
            raise IOError('Config File'+self.config_file+' does not exist') 
        self.config.read(self.config_file)
        # set the home variable
        self.config['USER']['home'] = os.getenv('HOME')
        
        # set logger object BEFORE checking the configuration file so log messages can be output.
        self.logdict = {'DEBUG' : logging.DEBUG,
                   'INFO' : logging.INFO,
                   'WARNING' : logging.WARNING,
                   'ERROR' : logging.ERROR}
        if not self.log_set:
            try:
                if not os.path.isdir(self.config['CAMPAIGN']['out_dir']):
                    if 'LOG' in self.config['USER']['log_dir']:
                        log.info('Making output directiry: '+self.config['CAMPAIGN']['out_dir'])
                    else:
                        print('Making output directiry: '+self.config['CAMPAIGN']['out_dir'])
                    os.makedirs(self.config['CAMPAIGN']['out_dir'])
            except:
                msg = '*** FATAL ERROR: impossible to make output LOG directory. Check configuration file: '+self.config_file
                raise ValueError(msg)
            try:
                self.set_log(level=self.logdict[self.config['USER']['log_level']])
            except:
                self.set_log(level=logging.DEBUG)
        
        # check configuration file
        self.check_config()
        
        # make Camera object
        self.make_camera()
        
        # make and load all SPICE kernels
        self.make_all_kernels()
        
        # load meteor shower information
        #self.get_shower_data()
        
        return
    
    def set_log(self,level=logging.DEBUG,nosdlr=False,nohdlr=False):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG
        nosdlr : boolean, optional
            If True, no stream handler is created. Default is False.
        nohdlr  : boolean, optional
            If True, no file handler is created. Default is False.
        
        Returns
        -------
        None.
        
        """
        # set log level
        log.setLevel(level)
        log.propagate = True
        
        # loop over log to retrieve StreamHandler and FileHandler
        # Get the FileHandler from the logger
        self.log_level = level
        # set log level
        log.setLevel(self.log_level)
        
        try:
            self.log_file = self.config['CAMPAIGN']['log_file_root'].replace('YYYYMMDDTHHMMSS',log_fmt)
        except:
            msg = 'FATAL ERROR: impossible to find log_file_root in configuration file '+config_file
            log.error(msg)
            raise ValueError(msg)
        
        # if LOG directory does not exist make one
        if not os.path.isdir(self.config['USER']['log_dir']):
            os.makedirs(self.config['USER']['log_dir'])
        
        # make FileHandler
        add_FileHandler(self.log_file)
        # log the log file name
        log.info('Log file: '+self.log_file)
        
        # set the log_set parameter
        self.log_set = True
        
        # log the log file name
        log.info('Log file: '+self.log_file)
        
        return
    
    def reset(self):
        """Reset all variables and unload all SPICE kernels.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        log.debug('Reseting...')
        # reset variables
        self.config = None
        self.out_dir = None
        self.geo_data = None
        self.kml = None
        self.shower,self.radiant,self.camera = None,None,None
        self.nacelle_id1,self.nacelle_name1 = None,None
        self.nacelle_id2,self.nacelle_name2 = None,None
        self.start,self.stop,self.cov = None,None,None
        # unload SPICE kernels
        log.debug('unloading SPICE kernels...')
        sp.unload(self.sites_fk)
        sp.unload(self.MPspk1)
        sp.unload(self.MPspk2)
        sp.unload(self.tf_file1)
        sp.unload(self.tf_file2)
        sp.unload(self.tk_file1)
        sp.unload(self.tk_file2)
        sp.unload(self.ik_file1)
        sp.unload(self.ik_file2)
        sp.unload(self.ck_file1)
        sp.unload(self.ck_file2)
        sp.unload(self.atmosphere_text_kernel)
        sp.unload(self.atmosphere_spk_file)
        log.debug('Reset Done.')
        # remove log handlers
        # Get the FileHandler from the logger
        for handler in log.handlers:
            if isinstance(handler, logging.FileHandler):
                log.removeHandler(handler)
            # if isinstance(handler, logging.StreamHandler):
            #     log.removeHandler(handler)
        self.log_file = None
        self.log_set = False
        return
    
    def check_config(self):
        """Check the configuration file.
        
        Make sure all necessary parameters are present.
        Raise ValueError if a parameters is missing.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        log.debug('Checking config file '+self.config_file)
        # - - - - - - - - - - USER section - - - - - - - - - - - - - - - - - -
        # check the presence of necessary paths names in USER section of config file
        listkeys = ['home', 'proj_dir', 'soft_dir', 'conf_dir', 'data_dir', 'log_dir', 'iau_shower_file',
                    'day_shower_file', 'night_shower_file', 'kernel_path', 'spice_exe_path','tmp_dir']
        for key in listkeys:
            log.debug('now checking presence of : ' + key + ' in ' + self.config_file)
            try:
                tmp = self.config['USER'][key]
            except:
                msg = '*** FATAL ERROR: ' + key + ' is missing from USER section of configuration file ' + self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # check existence of directories in USER section of config file
        listkey_dirtocheck = ['home', 'proj_dir', 'soft_dir', 'conf_dir', 'data_dir', 'log_dir', 'iau_shower_file',
                              'day_shower_file', 'night_shower_file', 'kernel_path', 'spice_exe_path']
        for key in listkey_dirtocheck:
            log.debug('now checking existence of : ' + self.config['USER'][key])
            if not os.path.exists(self.config['USER'][key]):
                msg = '*** FATAL ERROR: directory ' + self.config['USER'][key] + ' does not exists'
                log.error(msg)
                raise IOError(msg)
        
        # set IAU meteor shower file
        self.iau_shower_file = self.config['USER']['iau_shower_file']
        # check that the IAU meteor shower file exist
        utils.check_file(self.config_file,log)
        
        # set IMO daytime shower file
        self.day_shower_file = self.config['USER']['day_shower_file']
        # check that the IMO daytime shower file exist
        utils.check_file(self.config_file, log)
        
        # set IMO nighttime shower file
        self.night_shower_file = self.config['USER']['night_shower_file']
        
        # save SPICE paths into global variable
        self.kernel_path = self.config['USER']['kernel_path']+'/'
        self.spice_exe_path = self.config['USER']['spice_exe_path']+'/'
        
        # set temporary directory
        self.tmp_dir = self.config['USER']['tmp_dir']
        
        # set read_res parameter: useful to read results if the simulation has already been performed.
        try:
            self.read_res = self.config['USER']['read_res'].lower() in ['true', '1', 't', 'y', 'yes']
        except:
            self.read_res = False
        
        # set make_site_kernels parameter: useful to (re)-do the sites kernels files.
        try:
            self.mksitker = self.config['USER']['make_site_kernels'].lower() in ['true', '1', 't', 'y', 'yes']
        except:
            self.mksitker = False
        
        # - - - - - - - - - - CAMPAIGN section - - - - - - - - - - - - - - - - - -
        # check the presence of necessary keys in CAMPAIGN config file
        listkeys = ['name', 'out_dir', 'log_file_root', 'missplan_updir', 'nacelle1_name',
                    'nacelle2_name', 'sites_file','min_obs_alt', 'max_obs_alt','freq']
        for key in listkeys:
            log.debug('now checking presence of : ' + key + ' in ' + self.config_file)
            try:
                tmp = self.config['CAMPAIGN'][key]
            except:
                msg = '*** FATAL ERROR: ' + key + ' is missing from CAMPAIGN section of configuration file ' + self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # set optional keywords
        listoptkeys = ['geom_file','decision_file','fov_dat_file','fov_kml_file','area_file','ancinfov_file','numet_file','fov_step']
        listoptvalue = [self.config['CAMPAIGN']['out_dir']+'/geometry.dat',
                        self.config['CAMPAIGN']['out_dir']+'/decision.dat',
                        self.config['CAMPAIGN']['out_dir']+'/fov.dat',
                        self.config['CAMPAIGN']['out_dir']+'/fov.kml',
                        self.config['CAMPAIGN']['out_dir']+'/area.dat',
                        self.config['CAMPAIGN']['out_dir']+'/ancinfov.dat',
                        self.config['CAMPAIGN']['out_dir']+'/decision.dat',
                        self.config['CAMPAIGN']['out_dir']+'/numet.dat',
                        1.0*u.deg]
        for (key,val) in zip(listoptkeys,listoptvalue):
            try:
                tmp = self.config['CAMPAIGN'][key]
            except:
                self.config['CAMPAIGN'][key] = val
                log.debug(self.config['CAMPAIGN'][key]+' is set by default to '+val)
        
        # check existence of directories and files in CAMPAIGN section of config file
        listkey_tocheck = ['missplan_updir','sites_file']
        for key in listkey_tocheck:
            log.debug('now checking existence of : ' + self.config['CAMPAIGN'][key])
            if not os.path.exists(self.config['CAMPAIGN'][key]):
                msg = '*** FATAL ERROR: directory or file ' + self.config['CAMPAIGN'][key] + ' does not exists'
                log.error(msg)
                raise IOError(msg)
        
        # store some parameters
        self.sites_file = self.config['CAMPAIGN']['sites_file']
        self.out_dir = self.config['CAMPAIGN']['out_dir']+'/'
        self.decision_file = self.config['CAMPAIGN']['decision_file']
        self.fov_step = float(self.config['CAMPAIGN']['fov_step'])*u.deg
        # make a simulation identifier (string). This is used in particular for the temporary directory when making the ck files.
        self.sim_id = self.config['CAMPAIGN']['nacelle1_name']+'_'+self.config['CAMPAIGN']['nacelle2_name']+'_'+self.config['CAMERA']['az_off']+'_'+self.config['CAMERA']['roll_off']+'_'+self.config['CAMERA']['camera_name']+'_'+self.config['CAMERA']['lens_name']
        # make directories if necessary
        listkey_dirtocheck = ['out_dir']
        for key in listkey_dirtocheck:
            log.debug('now checking existence of directory: ' + self.config['CAMPAIGN'][key])
            if not os.path.isdir(self.config['CAMPAIGN'][key]):
                msg = 'Warning: directory ' + self.config['CAMPAIGN'][key] + ' does not exists -> making one now.'
                log.warning(msg)
                os.makedirs(self.config['CAMPAIGN'][key])
        
        # - - - - - - - - - - FAKEOR section - - - - - - - - - - - - - - - - - -
        # check the presence of necessary keys in FAKEOR section of config file
        listkeys = ['fakeors_num','anchors_file','afm_exe','afm_out_dir','afm_datacol_dir','afm_data_mode',
                    'fakeor_out_dir','fakeor_file','anchor_out_dir','fakeor_min_mass']
        for key in listkeys:
            log.debug('now checking presence of : ' + key + ' in ' + self.config_file)
            try:
                tmp = self.config['FAKEOR'][key]
            except:
                msg = '*** FATAL ERROR: ' + key + ' is missing from FAKEOR section of configuration file ' + self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # check existence of directories and files in FAKEOR section of config file
        listkey_tocheck = ['afm_exe','afm_datacol_dir']
        for key in listkey_tocheck:
            log.debug('now checking existence of : ' + self.config['FAKEOR'][key])
            if not os.path.exists(self.config['FAKEOR'][key]):
                msg = '*** FATAL ERROR: directory or file ' + self.config['FAKEOR'][key] + ' does not exists'
                log.error(msg)
                raise IOError(msg)
        
        # store some parameters
        self.nclone = int(self.config['FAKEOR']['fakeors_num'])
        self.fkr_coldir = self.config['FAKEOR']['afm_datacol_dir'] + '/'
        self.fkr_outdir = self.config['FAKEOR']['fakeor_out_dir'] + '/'
        self.anchor_out_dir = self.config['FAKEOR']['anchor_out_dir'] + '/'
        self.anchor_file = self.config['FAKEOR']['anchors_file']
        self.afm_out_dir = self.config['FAKEOR']['afm_out_dir'] + '/'
        self.afm_exe = self.config['FAKEOR']['afm_exe']
        self.afm_data_mode = self.config['FAKEOR']['afm_data_mode']
        self.fkr_min_mass = float(self.config['FAKEOR']['fakeor_min_mass'])*u.kg
        
        # make fakeor output directory
        for dir2make in [self.fkr_outdir,self.afm_out_dir,self.anchor_out_dir]:
            if not os.path.isdir(dir2make):
                log.info('Making output directory: '+dir2make)
                os.makedirs(dir2make)
        
        # - - - - - - - - - - SHOWER section - - - - - - - - - - - - - - - - - -
        # check the presence of necessary keys in SHOWER config file
        listkeys = ['shower_name','shower_code','shower_num','shower_year','max_time','shower_fwhm','zhr_expected','radiant_drift_ra',
                    'radiant_drift_dec','zhr_night_range']
        for key in listkeys:
            log.debug('now checking presence of : ' + key + ' in ' + self.config_file)
            try:
                tmp = self.config['SHOWER'][key]
            except:
                msg = '*** FATAL ERROR: ' + key + ' is missing from SHOWER section of configuration file ' + self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # - - - - - - - - - - CAMERA section - - - - - - - - - - - - - - - - - -
        # check the presence of necessary keys in CAMERA config file
        listkeys = ['camera_name', 'camera_brand', 'lens_name', 'lens_brand', 'filter_name', 'filter_brand','fps','lim_mag',
                    'az_off','el_off','roll_off']
        for key in listkeys:
            log.debug('now checking presence of : ' + key + ' in ' + self.config_file)
            try:
                tmp = self.config['CAMERA'][key]
            except:
                msg = '*** FATAL ERROR: ' + key + ' is missing from CAMERA section of configuration file ' + self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # copy configuration file into output directory
        src = self.config_file
        dst = self.out_dir + os.path.basename(src)
        try:
            shutil.copyfile(src,dst)
            log.info('File :'+src+' has been copied to '+dst)
        except shutil.SameFileError:
            msg = 'Warning: trying to move config file into SAME location '+dst
            log.warning(msg)
        except Exception as error:
            msg = '*** FATAL ERROR: impossible to move '+src+' into '+dst
            log.error(msg)
            raise error
        
        # read the MissionPlanner configuration file
        self.MPconf1,self.MPconfile1,self.MPdat1,self.MPspk1,self.nacelle_id1,self.nacelle_name1 = self.find_read_MPconf(self.config['CAMPAIGN']['nacelle1_dir'])
        self.MPconf2,self.MPconfile2,self.MPdat2,self.MPspk2,self.nacelle_id2,self.nacelle_name2 = self.find_read_MPconf(self.config['CAMPAIGN']['nacelle2_dir'])
        
        log.info('Nacelle1: '+self.MPspk1+' id: '+str(self.nacelle_id1)+' name: '+self.nacelle_name1)
        log.info('Nacelle2: '+self.MPspk2+' id: '+str(self.nacelle_id2)+' name: '+self.nacelle_name2)
        self.list_nacelle_names = [self.nacelle_name1,self.nacelle_name2]
        self.list_nacelle_num = [1,2]
        self.list_nacelle_numstr = ['1','2']
        
        # check the presence of necessary keys in SHOWER config file
        listkeys = ['shower_name','shower_code','shower_num','shower_year','max_time','shower_fwhm','meteor_alt','zhr_night_range']
        for key in listkeys:
            log.debug('now checking presence of : ' + key + ' in ' + self.config_file)
            try:
                tmp = self.config['SHOWER'][key]
            except:
                msg = '*** FATAL ERROR: ' + key + ' is missing from SHOWER section of configuration file ' + self.config_file
                log.error(msg)
                raise ValueError(msg)
        
        # store meteor altitude variable
        self.meteor_alt = float(self.config['SHOWER']['meteor_alt'])*u.km
        self.lim_mag = float(self.config['CAMERA']['lim_mag'])*u.mag
        
        # check for OPTIONAL keys in SHOWER section of config file
        # first check max_time format
        try:
            tmp = Time(self.config['SHOWER']['max_time'],scale='utc')
            log.debug('Shower max time: '+tmp.isot)
        except:
            msg = '***FATAL ERROR: max_time is NOT is isot format: YYYY-MM-DDTHH:MM:SS: '+self.config['SHOWER']['max_time']
            log.error(msg)
            raise ValueError(msg)
        # try to make a Radiant object
        try:
            self.radiant = Shower_Radiant(ra=float(self.config['SHOWER']['radiant_ra'])*u.deg,
                                         dec=float(self.config['SHOWER']['radiant_dec'])*u.deg,
                                         time_max=Time(self.config['SHOWER']['max_time'],scale='utc'),
                                         dra=float(self.config['SHOWER']['radiant_drift_ra'])*u.deg/u.d,
                                         ddec=float(self.config['SHOWER']['radiant_drift_dec'])*u.deg/u.d)
        except:
            log.warning('no radiant info in config file: '+self.config_file+'. Radiant will be created from shower catalogs.')
        # make a MeteorShower Object
        try:
            self.shower = MeteorShower(self.config['SHOWER']['shower_name'],
                                       float(self.config['SHOWER']['shower_V'])*u.km/u.s,
                                       float(self.config['SHOWER']['shower_r']),
                                       radiant=self.radiant,
                                       time_max=Time(self.config['SHOWER']['max_time'],scale='utc'),
                                       ZHR=float(self.config['SHOWER']['zhr_expected'])/u.hr,
                                       fwhm=float(self.config['SHOWER']['shower_fwhm'])*u.hr,
                                       leap_sec_ker=self.kernel_path+'naif0012.tls')
            self.shower.info()
        except:
            msg = os.getcwd()+' *** FATAL ERROR: no shower info in config file: '+self.config_file
            log.error(msg)
            raise ValueError(msg)
            #log.warning('no shower info in config file: '+self.config_file+'. Shower will be created from shower catalogs.')
            # retrieve Meteor shower info from catalogs
            #self.get_shower_data()
                
        log.info('Configuration check ok.')
        return
    
    def nacelle_name2number(self,nacelle_name):
        """Convert acelle number into nacelle obsplan number.
        
        Parameters
        ----------
        nacelle_name : string
            Nacelle name.
        
        Returns
        -------
        num : integer
            Nacelle number.
        """
        num = None
        if self.config['CAMPAIGN']['nacelle1_name'] in nacelle_name:
            num = 1
        if self.config['CAMPAIGN']['nacelle2_name'] in nacelle_name:
            num = 2
        if not num:
            msg = '*** FATAL ERROR: no nacelle obsplan-number for nacelle '+nacelle_name
            log.error(msg)
            raise ValueError(msg)
        return num
    
    def make_camera(self):
        """Make Camera object.
        
        The data needed to make the Camera objects are in the config file
        in the 'CAMERA' section.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.debug('Making Camera Object')
        # create Filter object
        flt = Filter(brand=self.config['CAMERA']['filter_brand'],name=self.config['CAMERA']['filter_name'])
        
        # create Lens object
        lens = Lens(brand=self.config['CAMERA']['lens_brand'], name=self.config['CAMERA']['lens_name'], filtre=flt)
        lens.set_log_level(self.logdict[self.config['USER']['log_level']])
        
        # create Camera Object
        self.camera = Camera(brand=self.config['CAMERA']['camera_brand'], name=self.config['CAMERA']['camera_name'], lens=lens)
        self.camera.set_log_level(self.logdict[self.config['USER']['log_level']])
        self.camera.fps = float(self.config['CAMERA']['fps']) / u.s
        self.camera.info()
        return
    
    def find_read_MPconf(self,MPdir):
        """Find and read the MissionPlanner configuration file.
        
        Parameters
        ----------
        MPdir : string
            MissionPlanner output directory containing the config and spk file.
        
        Returns
        -------
        MPconf  : ConfigParser object
            MissionPlanner configuration.
        MPconfig_file : string
            MissionPlanner configuration file.
        MPdat : string
            Nacelle Trajectory file in ascii '.dat' format.
        MPspk : string
            Nacelle Trajectory file in SPICE spk format.
        nacelle_id : int
            Nacelle SPICE spacecraft id.
        nacelle_name : string
            Nacelle name
        
        """
        log.info('Searching for MissionPlanner configuration file in '+MPdir+'/*.in')
        MPconf_found = False
        # loop over all '*.in' files in missplan_dir1 to find the MissionPlanner configuration file
        for infile in glob.glob(MPdir+'/*.in'):
            log.debug('Now reading config file '+infile)
            # open and read the config file
            MPconfig = ConfigParser(interpolation=ExtendedInterpolation())
            MPconfig.read(infile)
            # check the presence of necessary keys
            try:
                MPconfig['USER']['home'] = os.getenv('HOME')
                MPconf_found = True
                MPconfig_file = infile
            except:
                msg = 'File '+infile+' is not a MissionPlanner configuration file'
                log.warning(msg)
            if MPconf_found:
                break
        # check that the MissionPlanner configuration file was found.
        if not MPconf_found:
            msg = '*** FATAL ERROR: impossible to find MissionPlanner configuration file in '+MPdir
            log.error(msg)
            raise IOError(msg)
        # read info in MPconfig file
        try:
            nacelle_id = int(MPconfig['CAMPAIGN']['nacelle_id'])
            nacelle_name = MPconfig['CAMPAIGN']['nacelle_name']
            MPdat = MPconfig['CAMPAIGN']['trajectory_sim_file']
        except:
            msg = 'MissionPlanner configuration file looks corrupted'
            log.error(msg)
            raise ValueError(msg)
        # make spk file name
        MPspk = MPdat.replace('.dat','.spk')
        # check existence of SPK file
        if not os.path.exists(MPspk):
            msg = '*** FATAL ERROR: spk file '+MPspk+' does not exist'
            log.error(msg)
            raise IOError(msg)
        
        return MPconfig,MPconfig_file,MPdat,MPspk,nacelle_id,nacelle_name
    
    def make_all_kernels(self):
        """Make and load all necessary kernels.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # load SPICE standard kernel
        self.load_spice_kernels(self.kernel_path)
        # load SPK kernels and get time coverage
        self.cov1 = self.load_spk_kernel(self.MPspk1,self.nacelle_id1)
        self.cov2 = self.load_spk_kernel(self.MPspk2,self.nacelle_id2)
        # load MALBEC launch sites frame kernel
        self.sites_fk = self.make_sites_frame_kernel(make=self.mksitker)
        # make and load topo-like frame kernels
        self.tf_file1,self.frame_id1,self.frame_name1 = self.make_tf(self.MPspk1,self.nacelle_id1,self.nacelle_name1)
        self.tf_file2,self.frame_id2,self.frame_name2 = self.make_tf(self.MPspk2,self.nacelle_id2,self.nacelle_name2)
        # make and load instrument text frame kernels and instrument kernels
        self.tk_file1,self.nacori_id1,self.nacori_name1 = self.make_tk(self.MPspk1,self.nacelle_id1,self.nacelle_name1,kernel_path=self.kernel_path)
        self.tk_file2,self.nacori_id2,self.nacori_name2 = self.make_tk(self.MPspk2,self.nacelle_id2,self.nacelle_name2,kernel_path=self.kernel_path)
        # make instrument kernels
        self.ik_file1,self.ik_id1,self.ik_name1 = self.make_ik(self.MPspk1,self.tf_file1,self.camera.fov,kernel_path=self.kernel_path)
        self.ik_file2,self.ik_id2,self.ik_name2 = self.make_ik(self.MPspk2,self.tf_file1,self.camera.fov,kernel_path=self.kernel_path)
        # make and load ck (orientation) kernel files
        self.ck_file1,self.ck_file2,self.cov = self.make_nacelle_cks(self.MPspk1,self.tf_file1,self.MPspk2,self.tf_file2)
        # restrict the time coverage to effective observation run
        self.cov = self.get_effective_coverage()
        # make an ellipsoid of the top of atmosphere
        (self.atmosphere_name,self.atmosphere_id,self.atmosphere_text_kernel,self.atmosphere_spk_file) = self.make_atmosphere_kernel()
        
        log.info('All SPICE kernels loaded.')
        return
    
    def get_effective_coverage(self):
        """Compute effective observation coverage.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # check if fixed station or not
        try:
            fixed_station = self.config['CAMPAIGN']['fixed_station'].lower() in ['true', '1', 't', 'y', 'yes']
            log.debug('Fixed stations detected: '+str(fixed_station))
            # extract coverage and shrinks it a bit to make sure SPICE finds all ephemerids
            start,stop = sp.wnfetd(self.cov,0)
            start = int(start+60)
            stop = int(stop-60)
            outcov = sp.utils.support_types.SPICEDOUBLE_CELL(2)
            sp.wninsd(start,stop,outcov)
            return outcov
        except:
            log.debug('Mobile stations detected.')
            
        # set min observation altitudes from configuration file
        min_obs_alt = float(self.config['CAMPAIGN']['min_obs_alt']) * u.km
        
        # load ascii trajectory
        log.debug('Reading file: '+self.MPdat1+' and '+self.MPdat2)
        data1 = QTable.read(self.MPdat1,format='ascii.fixed_width_two_line')
        data2 = QTable.read(self.MPdat2,format='ascii.fixed_width_two_line')
        # set unit
        data1['Altitude'].unit = 'm'
        data2['Altitude'].unit = 'm'
        data1['Vz'].unit = 'm/s'
        data2['Vz'].unit = 'm/s'
        
        # check if the flight is ceiling or not
        for (MPconfig,MPconfile) in zip([self.MPconf1,self.MPconf2],[self.MPconfile1,self.MPconfile2]):
            # get payload file from configuration file
            payload_file = MPconfig['CAMPAIGN']['payload_file']
            payload_conf = ConfigParser(interpolation=ExtendedInterpolation())
            payload_conf.read(payload_file)
            try:
                # get ceiling vertical speed
                Vvert_ceil = -float(payload_conf['CEILING']['Vdesc'])*u.m/u.s
                payload_ceiling = True
                log.info('Ceiling payload detected in '+payload_file)
            except:
                payload_ceiling = False
                log.info('Ceiling payload NOT detected in '+payload_file+' provided in '+MPconfile)
        
        # get index of last data to consider for observation
        if payload_ceiling:
            try:
                idx1 = np.where(data1['Vz'].to('m/s').value < Vvert_ceil.to('m/s').value)[0][0]
                idx2 = np.where(data2['Vz'].to('m/s').value < Vvert_ceil.to('m/s').value)[0][0]
                log.debug('idx1 ceiling alt: '+str(idx1))
                log.debug('idx2 ceiling alt: '+str(idx2))
            except:
                msg = '*** FATAL ERROR: Vertical velocity never reaches '+str(Vvert_ceil.to('m/s'))
                log.error(msg)
                raise ValueError(msg)
        # no ceiling payload
        else:
            idx1 = np.argmax(data1['Altitude'])
            idx2 = np.argmax(data2['Altitude'])
        
        # get time of maximum
        timax1 = data1['Time'][idx1]
        timax2 = data1['Time'][idx2]
        # convert Time to SPICE et time.
        etmax1 = sp.str2et(timax1)
        etmax2 = sp.str2et(timax2)
        # set coverage max time
        etmax = np.min([etmax1,etmax2])
        
        # get time of minimum altitude
        try:
            idx1 = np.where(data1['Altitude'].to('km').value > min_obs_alt.to('km').value)[0][0]
            idx2 = np.where(data2['Altitude'].to('km').value > min_obs_alt.to('km').value)[0][0]
        except:
            msg = '*** FATAL ERROR: Altitude never reaches '+str(min_obs_alt.to('km'))
            log.error(msg)
            raise ValueError(msg)
        # get time of minimum altitude
        timin1 = data1['Time'][idx1]
        timin2 = data1['Time'][idx2]
        # convert Time to SPICE et time.
        etmin1 = sp.str2et(timin1)
        etmin2 = sp.str2et(timin2)
        # set coverage max time
        etmin = np.max([etmin1,etmin2])
        
        log.info("Effective time start: "+sp.et2utc(etmin,"ISOC",2,50))
        log.info("Effective time stop: "+sp.et2utc(etmax,"ISOC",2,50))
        
        # make SPICE effective coverage time window
        outcov = sp.utils.support_types.SPICEDOUBLE_CELL(2)
        sp.wninsd(etmin,etmax,outcov)
        
        return outcov
    
    def make_atmosphere_kernel(self):
        """Make an atmosphere object kernel.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        atmosphere_kernel_file : string
            Output kernel file.
        
        """
        # create output file name
        atmosphere_text_kernel = self.out_dir +'/'+ 'atmosphere.ker'
        atmosphere_spk_file = atmosphere_text_kernel.replace('.ker','.spk')
        # make SPICE object name and id
        atmosphere_name = 'ATMOSPHERE_ELLIPSOID'
        atmosphere_id = 1399001
        surface_name = 'ATMOSPHERE_SURFACE'
        surface_id = 1399002
        atmosphere_frame_name = 'ATM_FRAME'
        atmosphere_frame_id = 1399003
        # get Earth radii
        earth_radii = sp.bodvrd( "EARTH", "RADII", 3)[1]*u.km
        # set atmosphere radii
        self.atm_radii = earth_radii + self.meteor_alt
        
        # check if files alrady exist
        if (os.path.exists(atmosphere_text_kernel) and os.path.exists(atmosphere_spk_file) and self.read_res):
            # load kernels
            sp.furnsh(atmosphere_text_kernel)
            sp.furnsh(atmosphere_spk_file)
            log.debug('atmosphere_text_kernel file loaded: '+atmosphere_text_kernel)
            log.debug('atmosphere_spk_file file loaded: '+atmosphere_spk_file)
            return (atmosphere_name,atmosphere_id,atmosphere_text_kernel,atmosphere_spk_file)
        
        # make temporary directory
        sim_tmp_dir = self.tmp_dir + '/' + self.sim_id + '/'
        if not os.path.isdir(sim_tmp_dir):
            os.makedirs(sim_tmp_dir)
            log.info('Temporary directory created: '+sim_tmp_dir)
        # loop over time
        start,stop = sp.wnfetd(self.cov,0)
        # set start time
        et = start - 10.0
        # creates the ephemeris data file
        names=['EPOCH','X','Y','Z','VX','VY','VZ']
        # make intermediate spk_data file name
        data_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file.replace('.spk','_spk.dat'))
        # intermediate setup file name
        setup_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file.replace('.spk','.setup'))
        # intermediate output file
        tmpout_file = sim_tmp_dir+os.path.basename(atmosphere_spk_file)
        log.debug('spk_file: '+atmosphere_spk_file)
        log.debug('data_file: '+data_file)
        log.debug('setup_file: '+setup_file)
        log.debug('tmpout_file: '+tmpout_file)
        with open(data_file,'w') as out:
            out.write("# "+' '.join(names)+" \n")
            while et < stop:
                out.write(sp.et2utc(et,'ISOC',2,50).replace('T',' ')+"\n")
                out.write('0.000000000 0.000000000 0.000000000 -0.000000000 -0.000000000 -0.000000000\n')
                et = et + self.freq.to('s').value
            et = stop + 10.0
            out.write(sp.et2utc(et,'ISOC',2,50).replace('T',' ')+"\n")
            out.write('0000.000000000 0.000000000 0.000000000 -0.000000000 -0.000000000 -0.000000000\n')
        log.debug('Atmosphere data saved in '+data_file)
        # make the intermediate setup file
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
            out.write("   OUTPUT_SPK_TYPE   = 13\n")
            out.write("   OBJECT_ID         = "+str(atmosphere_id)+"\n")
            out.write("   OBJECT_NAME       = "+repr(atmosphere_name)+"\n")
            out.write("   CENTER_ID         = 399\n")
            out.write("   REF_FRAME_NAME    = "+repr('ITRF93')+"\n")
            out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
            out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
            out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
            out.write("   LEAPSECONDS_FILE  = "+repr(self.kernel_path+'naif0012.tls')+"\n")
            out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
            out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
            out.write("   PCK_FILE          = "+repr(self.kernel_path+'pck00010.tpc')+"\n")
            out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
            out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
            out.write("   IGNORE_FIRST_LINE = 1 \n")
            out.write("   LINES_PER_RECORD  = 2 \n")
            out.write("   POLYNOM_DEGREE    = 3 \n")
            out.write("   SEGMENT_ID        = "+repr(str(atmosphere_id))+"\n")
            out.write(" \n")
        log.info("setup saved in: "+setup_file)
        
        # remove spk file if it already exists
        if os.path.exists(tmpout_file):
            os.remove(tmpout_file)
        # build the command
        cmd = self.spice_exe_path+"mkspk -setup "+setup_file
        log.info("cmd="+cmd)
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
            log.error(msg)
            raise IOError(msg)
        # double check that the SPK file was successfully created
        if not os.path.exists(tmpout_file):
            msg='*** FATAL ERROR: temporary SPK file: '+atmosphere_spk_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        # now copy temporary output file into spk_file
        outdir = os.path.dirname(atmosphere_spk_file)+'/'
        for file2move in [setup_file,data_file,tmpout_file]:
            dst = outdir+os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            log.debug('Moving file '+file2move+' into '+dst)
            try:
                # remark: os.remove does not always work when trying to move file from one FS to another...
                shutil.copy(file2move,dst)
            except:
                msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
                log.error(msg)
                raise IOError(msg)
        log.info("SPICE SPK kernel saved in: "+atmosphere_spk_file)
        # remove temporary directory
        try:
            shutil.rmtree(sim_tmp_dir)
            log.info('Temporary directory deleted: '+sim_tmp_dir)
        except:
            msg = '*** FATAL ERROR: Impossible to remove temporary directory: '+sim_tmp_dir
            log.error(msg)
            raise IOError(msg)
        
        # make text kernel
        with open(atmosphere_text_kernel,'w') as out:
            out.write("\\begintext \n\n")
            out.write('  Definition of the top of atmosphere at meteor altitude \n')
            out.write('  Author: J. Vaubaillon - IMCCE - 2020\n')
            out.write('  MALBEC project\n\n')
            out.write("\\begindata \n\n")
            out.write('   NAIF_BODY_NAME += ('+repr(atmosphere_name)+')\n')
            out.write('   NAIF_BODY_CODE += ( '+str(atmosphere_id)+ ')\n')
            out.write('   NAIF_BODY_CENTER = 399 \n')
            out.write('   NAIF_SURFACE_NAME += ( '+repr(surface_name)+' )\n')
            out.write('   NAIF_SURFACE_CODE += ( '+str(surface_id)+' )\n')
            out.write('   NAIF_SURFACE_BODY += ( '+str(atmosphere_id)+' )\n\n')
            out.write('   OBJECT_'+str(surface_id)+'_FRAME = '+repr('ITRF93')+'\n\n')
            out.write('   BODY'+str(atmosphere_id)+'_CENTER = 399\n\n')
            out.write('   BODY'+str(atmosphere_id)+'_RADII = ( '+str(self.atm_radii[0].to('km').value)+' , '+str(self.atm_radii[1].to('km').value)+' , '+str(self.atm_radii[2].to('km').value)+' )\n')
            out.write('   BODY'+str(atmosphere_id)+'_POLE_RA        = (    0.      -0.641         0. )\n')
            out.write('   BODY'+str(atmosphere_id)+'_POLE_DEC       = (   90.      -0.557         0. )\n')
            out.write('   BODY'+str(atmosphere_id)+'_PM             = (  190.147  360.9856235     0. )\n')
            out.write('   BODY'+str(atmosphere_id)+'_LONG_AXIS      = (    0.                        )\n\n')
            out.write('   FRAME_'+atmosphere_frame_name+' = '+str(atmosphere_frame_id)+'\n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_NAME    =  '+repr(atmosphere_frame_name)+'\n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_CLASS   =  4 \n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_CLASS_ID = '+str(atmosphere_frame_id)+'\n')
            out.write('   FRAME_'+str(atmosphere_frame_id)+'_CENTER  = '+str(atmosphere_id)+'\n\n')
            out.write('   OBJECT_'+str(atmosphere_id)+'_FRAME = '+repr(atmosphere_frame_name)+'\n\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_RELATIVE            =  '+repr('ITRF93')+'\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_SPEC                =  '+repr('ANGLES')+'\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_UNITS               =  '+repr('DEGREES')+'\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_AXES                =  ( 3, 2, 3 )\n')
            out.write('   TKFRAME_'+str(atmosphere_frame_id)+'_ANGLES              =  ( 0, 0, 0 )\n')
        # load kernels
        sp.furnsh(atmosphere_text_kernel)
        sp.furnsh(atmosphere_spk_file)
        log.debug('atmosphere_text_kernel file loaded: '+atmosphere_text_kernel)
        log.debug('atmosphere_spk_file file loaded: '+atmosphere_spk_file)
        return (atmosphere_name,atmosphere_id,atmosphere_text_kernel,atmosphere_spk_file)
    
    def launch(self):
        """Launch ObsPLanner for a given set of initial conditions.
        
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        # set time to compute the computation time
        run_time = tm.time()
        
        # launch geometry computation
        decide = self.launch_geometry()
        
        # if impossible to observe, write a warning log and raise ValueError
        if not decide:
            msg = '*** CONFIGURATION DOES NOT ALLOW OBSERVATION given constraints.'
            log.warning(msg)
            raise ConstraintError(msg)
        
        # launch computation of FOV atmosphere area as a function of time
        self.compute_all_fov()
        
        # compute expected number of double station meteors
        self.compute_numet()
        
        # create Fakeors and fake observations
        self.create_fakeors()
        
        log.info('The computation took '+str(tm.time()-run_time)+' sec to run.')
        return
    
    def launch_geometry(self):
        """Launch the computation of the observation geometry.
        
        Parameters
        ----------
        
        
        Returns
        -------
        decide : boolean
            True if the geometry configuration allows the observation run
            given the constraints provided in the configuration file.
            False otherwise.
        
        """
        # create geometry output data file
        self.geom_file = self.config['CAMPAIGN']['geom_file']
        # get time coverage
        self.start,self.stop = sp.wnfetd(self.cov,0)
        # create output QTable
        self.geom_data = obsutils.make_geometry_output_table()
        # if data were already created simply load them
        if (os.path.exists(self.geom_file) and self.read_res):
            log.info('Loading data from '+self.geom_file)
            self.geom_data = QTable.read(self.geom_file,format='ascii.fixed_width_two_line')
            obsutils.set_geometry_output_table_unit_and_format(self.geom_data)
        else:
            # set output table unit and format
            obsutils.set_geometry_output_table_unit_and_format(self.geom_data)
            # loop time over coverage
            lastloop = False
            et = self.start + 0.1
            while et <= self.stop:
                log.debug('start: '+sp.et2utc(self.start,"ISOC",2,50))
                log.debug('stop : '+sp.et2utc(self.stop,"ISOC",2,50))
                log.debug('et   : '+sp.et2utc(et,"ISOC",2,50))
                # convert time
                time = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
                # check geometry
                (datarow,eloc1,eloc2) = self.compute_geometry(self.nacelle_name1,self.nacelle_name2,time)
                # put data into QTable
                self.geom_data.add_row(datarow)
                # time increment
                et = et + self.freq.to('s').value
                if lastloop:
                    break
                if et > self.stop:
                    et = self.stop
                    lastloop = True
            # save data
            self.geom_data.sort('Time')
            self.geom_data.write(self.geom_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
            log.info('Data saved in '+self.geom_file)
        
        # plot results
        if os.path.exists(self.decision_file) and self.read_res:
            # skip if decide file already exist, to save cpu time
            log.debug('Skip plot making')
        else:
            self.make_plots()
        
        # take a decision: is the tested geometry configuration feasible or not?
        decide = self.decide()
        
        return decide
    
    def get_radiant_from_shower(self,shower_name,shower_file):
        """Get radiat Ra,Dec from IAU shower file.
        
        Parameters
        ----------
        shower_name : string
            Name of meteor shower
        shower_file : string, optional
            Name of IAU established meteor shower data file.
            Default is 'conf/streamestablisheddata_jv.csv'
        year : string, optional
            Year label of the shower. Default is 'annual'.
        
        
        Returns
        -------
        radiant_skycoo : astropy.coordinate.SkyCoord object
            radiant sky coordinates.
        
        """
        # get shower info
        data = self.get_shower_data(self,shower_name,shower_file=shower_file,activity='annual')
        # select shower radiant
        if len(data['Ra'])>1:
            radiant_skycoo = SkyCoord(np.median(data['Ra']),np.median(data['De']),frame='icrs')
        else:
            radiant_skycoo = SkyCoord(data['Ra'],data['De'],frame='icrs')
        return radiant_skycoo
    
    def load_spice_kernels(self,kernel_path,standard_file_name='standard.ker'):
        """Function to load SPICE standard kernels.
        
        Parameters
        -------------
        kernel_path:
        
        spice_exe_path:
        
        Returns
        -------------
        None.
        
        """
        
        # load required standard spice kernels
        kernel_file = kernel_path + standard_file_name
        utils.check_file(kernel_file,log)
        sp.furnsh(kernel_file)
        log.debug('kernel_file loaded: '+kernel_file)
        return
    
    def load_spk_kernel(self,spk_file,spk_id):
        """Load SPK kernels and check the SPICE id.
        
        Make sure the time coverage overlap.
        
        Parameters
        ----------
        spk_file : String
            full path to spk file.
        spk_id : int
            SPICE spacecraft id that should be in SPK file.
        
        Returns
        -------
        nacelle_cover : SPICE time window
            Time coverage of the nacelle spk file.
        
        """
        # load SPK file
        self.load_nacelle_kernel(spk_file)
        # check nacelle id from SPK file
        if not (spk_id == sp.spkobj(spk_file)[0]):
            msg = '*** FATAL ERROR: SPICE nacelle id do not match: '+str(spk_id)+' vs '+str(sp.spkobj(spk_file)[0])
            log.error(msg)
            raise ValueError(msg)
        
        # get the time window coverage
        nacelle_cover = sp.spkcov(spk_file, spk_id)
        log.info('nacelle_cover start: '+sp.et2utc(nacelle_cover[0], "ISOC", 2, 50))
        log.info('nacelle_cover end  : '+sp.et2utc(nacelle_cover[1], "ISOC", 2, 50))
        return nacelle_cover
        
    def load_nacelle_kernel(self,nacelle_kernel_file):
        """Load nacelle kernel file.
        
        Parameters
        ----------
        nacelle_kernel_file : string
            nacelle kernel file.
        
        Returns
        -------
        None.
        
        """
        # first make sure the file exists
        utils.check_file(nacelle_kernel_file,log)
        # load kernel with sp.furnsh()
        sp.furnsh(nacelle_kernel_file)
        log.debug('nacelle_kernel_file loaded '+nacelle_kernel_file)
        return
    
    def get_nacelle_position(self,nacelle_name,time,silent=False):
        """Get position of one nacelle at time t.
        
        Parameters
        ----------
        nacelle_name : string
            name of nacelle whose position is to be determined
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        silent : boolean, optional
            If True, no log is performed. Default is False
        
        Returns
        -------
        eloc : astropy.coordinates.EarthLocation
            Location of the nacelle at time t.
        
        """
        et = sp.utc2et(time.isot)
        if not silent:
            log.debug('Retrieving '+nacelle_name+' position for time: '+time.isot+' et: '+str(et))
        # check that the nacelle is known to SPICE
        try:
            sp.bodn2c(nacelle_name)
        except:
            msg = '*** FATAL ERROR: SPICE does not seem to know nacelle: '+nacelle_name
            log.error(msg)
            raise ValueError(msg)
        # get positons of nacelles with SPICE tools ; note that you might want to convert time to SPICE time.
        (eph, lt) = sp.spkezr(nacelle_name,et,'ITRF93','NONE','EARTH')
        # convert x,y,z into an astropy.coordinates.EarthLocation (global) object:
        # example: eloc = EarthLocation.from_geocentric(x, y, z, unit=u.km)
        eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
        if not silent:
            log.debug('Position of '+nacelle_name+' at '+time.isot+' = '+str(eloc.to_geodetic()))
        return eloc
    
    def get_nacelle_pointing(self,nacelle_name,time,append_posradec=False):
        """Get local pointing direction of a nacelle camera at time t.
        
        Parameters
        ----------
        nacelle_name : string
            name of nacelle whose pointing direction is to be determined.
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        append_posradec : boolean
            If True, returns also the RA,DEC sky coordinates position
            of the nacelle pointing direction.
            Default is False.
        
        Returns
        -------
        eloc : astropy.coordinates.EarthLocation Object, optional.
            Location of the nacelle at time time.
            Is not returned if append_posradec=False.
        radec : astropy.coordinates.SkyCoord object, optional.
            Pointing direction of the nacelle in ICRS at J2000.
            Is not returned if append_posradec=False.
        altaz : astropy.coordinates.AltAz object
            Local orientation of the nacelle, in nacelle frame.
        
        """
        # get necessary parameters
        (et,nacelle_id,
            instrument_name,
            instrument_id,
            instrument_frame_name,
            instrument_frame_id,
            clock_id,
            encoded_clock) = self.get_orientation_input(nacelle_name,time)
        nacelle_topoframe_name = make_topoframe_name(nacelle_name)
        log.debug('Getting orientation of '+instrument_name+' id: '+str(instrument_id)+' at '+time.isot+' from frame: '+instrument_frame_name+' into frame: '+nacelle_topoframe_name)
        # get transformation between instrument frame and topo-like frame
        pointing_mtx = sp.pxform(instrument_frame_name,nacelle_topoframe_name,et)
        # get instrument boresight vector
        (fov_shape,fov_frame,brst_vec,n_crn,crn_vec) = sp.getfov(instrument_id,4)
        # get instrument orientation vector
        pointing_vec = sp.mxv(pointing_mtx,brst_vec)
        # translates orientation vector into angle
        rlonlat = sp.reclat(pointing_vec)
        # compute geographic azimuth/elevation
        az = (-rlonlat[1]*sp.dpr() %360) * u.deg
        el =   rlonlat[2]*sp.dpr() * u.deg
        # make astropy.coordinates.AltAz object
        altaz = AltAz(az=az,alt=el,obstime=time)
        if append_posradec:
            # retrieve nacelle position
            eloc = self.get_nacelle_position(nacelle_name,time)
            # create AltAz frame
            altaz_frame = AltAz(obstime=time,location=eloc)
            # compute RA,DEC
            radec = SkyCoord(altaz.az,altaz.alt,frame=altaz_frame).transform_to('icrs')
            # FOR TEST PURPOSE: REMOVE
            pointing_mtx = sp.pxform(instrument_frame_name,'J2000',et)
            # compute orientation of boresight vecor
            pointing_vec = sp.mxv(pointing_mtx,brst_vec)
            rradec = sp.recrad(pointing_vec)
            radec_spice = SkyCoord(rradec[1]*sp.dpr(),rradec[2]*sp.dpr(),unit=u.deg,frame='icrs')
            log.debug('radec_spice : '+radec_spice.to_string('hmsdms'))
            log.debug('sep radec radec_spice: '+str(radec.separation(radec_spice)))
            output_data = (eloc,radec_spice,altaz)
        else:
            output_data = (altaz)
        return output_data
    
    def get_orientation_input(self,nacelle_name,time):
        """Get parameters needed to compute orientation.
        
        Parameters
        ----------
        nacelle_name : string
            name of nacelle whose pointing direction is to be determined.
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        et : float
            SPICE time.
        nacelle_id : int
            SPICE nacelle id.
        instrument_name : string
            SPICE instrument name.
        instrument_id : int
            SPICE instrument id.
        instrument_frame_name : string
            SPICE instrument frame name.
        instrument_frame_id : int
            SPICE instrument frame id.
        clock_id : int
            SPICE nacelle clock id.
        encoded_clock : int
            SPICE nacelle encoded clock time.
        """
        # convert astropy.time.Time into SPICE time
        et = sp.str2et(time.isot)
        # get nacelle id
        nacelle_id = sp.bodn2c(nacelle_name)
        # get camera name from nacelle name
        instrument_name = make_camera_name(nacelle_name)
        instrument_id = sp.bodn2c(instrument_name)
        instrument_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
        instrument_frame_id = make_nacelle_orientation_frame_id(nacelle_id)
        # convert SPICE time into instrument clock
        clock_id = make_clock_id(nacelle_id,from_instr=True)
        # log
        log.debug('nacelle_name: '+nacelle_name)
        log.debug('nacelle_id: '+str(nacelle_id))
        log.debug('instrument_name: '+instrument_name)
        log.debug('instrument_id: '+str(instrument_id))
        log.debug('instrument_frame_name: '+instrument_frame_name)
        log.debug('instrument_frame_id: '+str(instrument_frame_id))
        log.debug('clock_id: '+str(clock_id))
        unencoded_clockstring = sp.sce2s(clock_id,et)
        log.debug('unencoded_clockstring: '+str(unencoded_clockstring))
        encoded_clock = sp.scencd(clock_id,unencoded_clockstring)
        log.debug('encoded_clock: '+str(encoded_clock))
        # check that nacelle and instrument are not the same
        if (instrument_name == nacelle_name):
            msg = '*** FATAL ERROR: instrument and nacelle have same name: '+instrument_name
            log.error(msg)
            raise ValueError(msg)
        if (instrument_id == nacelle_id):
            msg = '*** FATAL ERROR: instrument and nacelle have same id: '+str(instrument_id)
            log.error(msg)
            raise ValueError(msg)
        return (et,nacelle_id,instrument_name,instrument_id,instrument_frame_name,instrument_frame_id,clock_id,encoded_clock)
    
    def make_tf(self,nacelle_spk,nacelle_id,nacelle_name,kernel_path='/astrodata/kernels/'):
        """Make and load nacelle topo-like frame kernels (tf-files).
        
        Parameters
        ----------
        nacelle_spk : string
            Nacelle SPICE SPK file full name.
        nacelle_id : int
            Nacelle SPICE id
        nacelle_name : string
            Nacelle SPICE name.
        kernel_path: string, optional
            path to SPICE standard kernels; must include input standard kernel
                        'standard.ker'. Default is '/astrodata/kernels/'.
        
        Returns
        -------
        tf_file : string
            Nacelle SPICE FK (frame kernel) file full name.
        frame_id : int
            Nacelle frame SPICE id.
        frame_name : string
            Nacelle SPICE frame name.
        
        """
        # make tf file name
        tf_file = self.out_dir + nacelle_name + '.tf'
        # check if tf_file already exist
        if (os.path.exists(tf_file) and self.read_res):
            frame_id = make_topoframe_id(nacelle_id)
            frame_name = make_topoframe_name(nacelle_name)
        else:
            # call create_tf. Note: kernels already loaded at this point.
            tf_file,frame_id,frame_name = create_tf(nacelle_spk,nacelle_id,nacelle_name,out_tf_file=tf_file,load_stdker=False,load_spker=False)
        # load the frame kernel
        sp.furnsh(tf_file)
        log.debug('tf_file loaded: '+tf_file)
        return tf_file,frame_id,frame_name
    
    def make_tk(self,nacelle_spk,nacelle_id,nacelle_name,kernel_path='/astrodata/kernels/'):
        """Make instrument (camera) text frame kernel.
        
        Parameters
        ----------
        nacelle_spk : string
            Nacelle SPICE SPK file full name.
        nacelle_id : int
            Nacelle SPICE id
        nacelle_name : string
            Nacelle SPICE name.
        kernel_path: string, optional
            path to SPICE standard kernels; must include input standard kernel
                        'standard.ker'. Default is '/astrodata/kernels/'.
        
        Returns
        -------
        tk_file : string
            output tk file full name.
        nacelle_orientation_frame_id : int
            Nacelle orientation frame SPICE id
        nacelle_orientation_frame_name : string
            Nacelle orientation frame SPICE name.
        
        """
        # retrieve spk directory
        nacelle_dir = os.path.dirname(nacelle_spk)
        # make tk file name
        tk_file = nacelle_dir + '/' + nacelle_name + '.tk'
        # check if tk_file already exist
        if (os.path.exists(tk_file) and self.read_res):
            nacelle_orientation_frame_id = make_nacelle_orientation_frame_id(nacelle_id)
            nacelle_orientation_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
        else:
            # launch tk command
            (tk_file,nacelle_orientation_frame_id,nacelle_orientation_frame_name) = create_tk(nacelle_spk,nacelle_id,nacelle_name,
                    out_tk_file=tk_file,kernel_path=kernel_path)
        # load the frame kernel
        sp.furnsh(tk_file)
        log.debug('tk_file loaded: '+tk_file)
        return tk_file,nacelle_orientation_frame_id,nacelle_orientation_frame_name
    
    def check_spice_ids(self,config_file_MP1,config_file_MP2):
        """
        Function to double check if the SPICE ID for nacelle 1 and nacelle 2 as noted in the config
        file are matching the values in their corresponding spk files.
        If the values are not the same, raise ValueError.
        Also checking, that nacelle 1 SPICE ID and nacelle 2 SPICE ID are not the same.
        If Values are the same, raise ValueError.
        
        Parameters:
        -------------
        config_file_MP1 : String.
            Full path to missionplaner configuration file for nacelle 1.
        config_file_MP2 : String.
            Full path to missionplaner configuration file for nacelle 2.
        
        Returns:
        -------------
        None.
        
        """
        # checking that nacelle1_id_spk and nacelle2_id_spk are not the same
        if self.nacelle_id1 == self.nacelle_id2:
            msg = '*** FATAL Error: nacelle 1 SPICE ID and nacelle 2 SPICE ID are the same.'
            log.error(msg)
            raise ValueError
        # set up MALBEC-specific variables (nacelle SPICE IDs) from config_file
        missionconf1 = ConfigParser()
        missionconf2 = ConfigParser()
        log.info(config_file_MP1)
        log.info(config_file_MP2)
        missionconf1.read(config_file_MP1)
        missionconf2.read(config_file_MP2)
        # get SPICE id of nacelle 1 as noted in the config_file from missionplan
        nacelle1_id_conf = missionconf1['CAMPAIGN']['nacelle_id']
        nacelle2_id_conf = missionconf2['CAMPAIGN']['nacelle_id']
        # check if nacelle1_id_conf and nacelle1_id_spk are the same
        for (nac_id_MP,nac_id_OP,id_str) in zip([nacelle1_id_conf,nacelle2_id_conf],[self.nacelle_id1,self.nacelle_id2],self.list_nacelle_numstr):
            if nac_id_MP == nac_id_OP:
                msg = 'nacelle'+id_str+'_id noted in the config_file matches the ' \
                      'nacelle ID in the corresponding spk file.'
                log.info(msg)
            else:
                msg = '*** FATAL ERROR: SPICE ID of nacelle'+id_str+' is not the same in the config file' \
                      'and the corresponding spk file'
                log.error(msg)
                raise ValueError(msg)
        return
    
    def make_sites_frame_kernel(self,out_dir=None,make=False):
        """Make and load a SPICE frame kernel for all possible landing sites.
        
        Sites configuration file is under the self.sites_file parameter and
        defined in the configuration file.
        
        Parameters
        ----------
        kernel_path : string
            Path to SPICE kernel files.
        spice_exe_path : string
            Path to SPICE executable files.
        out_dir : string, optional
            Output file directory. If not set (default) the output directory
            is the directory containing the sites_file defined in the configuration
            file at under the 'CAMPAIGN' section ('sites_file').
        make : boolean
            If True, make the kernels, otherwise just load them.
            Default is False.
        
        Returns
        -------
        None.
        
        """
        # check existence of sites file
        utils.check_file(self.sites_file,log)
        # sets input standard pck kernel
        inputpckfile = self.kernel_path + '/pck00010.tpc'
        # set output directory
        if not out_dir:
            out_dir = os.path.dirname(self.sites_file) + '/'
        # sets output files
        outputdefnsfile = out_dir + '/' + os.path.basename(self.sites_file).replace('.in','.defns')
        outputspkfile = out_dir + '/' + os.path.basename(self.sites_file).replace('.in','.bsp')
        outputfkfile = out_dir + '/' + os.path.basename(self.sites_file).replace('.in','.fk')
                
        if make:
            # delete previously created sites kernel files
            if os.path.exists(outputdefnsfile):
                os.remove(outputdefnsfile)
            if os.path.exists(outputspkfile):
                os.remove(outputspkfile)
            if os.path.exists(outputfkfile):
                os.remove(outputfkfile)
            
            # set up MALBEC-specific variables
            # check sites
            log.info('Reading: '+self.sites_file)
            sitesconf = ConfigParser()
            sitesconf.read(self.sites_file)
            # list of keys to input into landing site dictionary
            list_keys = ['id', 'lon', 'lat', 'alt', 'type', 'name', 'tel']
            
            # determine the number of possible landing sites
            nsites = 0
            for nsites in range(100):
                param = 'SITE' + str(nsites + 1)
                log.debug('checking site: ' + param)
                try:
                    tmp = sitesconf[param]
                    log.debug(str(tmp))
                    for key in list_keys:
                        testkey = sitesconf[param][key]
                        log.debug('checking '+param+'['+key+'] = '+testkey)
                except:
                    log.warning('*** Impossible to find ' + param + ' in ' + 'sites_file')
                    break
            log.info('There are ' + str(nsites) + ' possible landing sites recorded in the sites_file')
            # check number of sites found
            if not nsites:
                msg = '*** FATAL ERROR: there is no possible landing site in ' + self.sites_file
                log.error(msg)
                raise ValueError(msg)
            
            # now starts printing the outputdefnsfile file
            log.debug('opening definition file: '+outputdefnsfile)
            out = open(outputdefnsfile, 'w')
            out.write("\\begindata \n")
            out.write("SITES = ( \n")
            # writes the name of all MALBEC stations as SITE1, SITE2,...
            for nsite in range(nsites):
                param = 'SITE' + str(nsite + 1)
                out.write("        " + repr(param) + ", \n")
            out.write(") \n")
            
            # loop to write the outputdefnsfile for all MALBEC stations
            for nsite in range(nsites):
                param = 'SITE' + str(nsite + 1)
                log.debug('checking site: ' + param)
                try:
                    for key in list_keys:
                        testkey = sitesconf[param][key]
                        log.debug('checking '+param+'['+key+'] = '+testkey)
                except:
                    log.warning('*** Impossible to find ' + param + ' in ' + self.sites_file)
                    break
                
                # now prints the outputdefnsfile file
                out.write("\n")
                out.write("      " + str(param) + "_CENTER = 399 \n")
                out.write("      " + str(param) + "_FRAME = " + repr('ITRF93') + "\n")
                out.write("      " + str(param) + "_IDCODE = " + str(399200 + float(sitesconf[param]['id'])) + "\n")
                out.write("      " + str(param) + "_LATLON = (" + str(sitesconf[param]['lat']) + ',' + str(
                    sitesconf[param]['lon']) + ',' + str(float(sitesconf[param]['alt']) / 1000.0) + ")\n")
                out.write("      " + str(param) + "_UP = " + repr('Z') + "\n")
                out.write("      " + str(param) + "_NORTH = " + repr('X') + "\n")
                out.write(" \n")
                """
                local_dict = {}
                # to check the information in sites.in
                for key in list_keys:
                    try:
                        local_dict[key] = sitesconf[param][key]
                        landingsites[param] = local_dict
                        log.debug(key + ' : ' + sitesconf[param][key])
                    except:
                        msg = '*** FATAL ERROR: ' + key + ' was not found in ' + param
                        log.error(msg)
                        raise ValueError(msg)
                """
            # closes the outputdefnsfile
            out.close()
            log.debug('outputdefnsfile file closed: '+outputdefnsfile)
            # now executes the pinpoint program to create the spk kernel file
            cmd = self.spice_exe_path + "pinpoint -def " + outputdefnsfile + " -spk " + outputspkfile + " -pck " + inputpckfile + " -fk " + outputfkfile + " -batch"
            log.info("cmd=" + cmd)
            os.system(cmd)
            log.info("data saved in " + outputspkfile + " and " + outputfkfile)
        
        # check existence of output files
        utils.check_file(outputspkfile,log)
        utils.check_file(outputfkfile,log)
        
        # load sites kernels
        log.info('Loading site kernel: '+outputspkfile)
        sp.furnsh(outputspkfile)
        log.info('Loading site frame kernel: '+outputfkfile)
        sp.furnsh(outputfkfile)
        return outputfkfile
    
    def get_nacelle_id(self,spk_file):
        """Get the nacelle SPICE ID from spk file.
        
        Parameters:
        --------------
        spk_file1 : string
            Nacelle SPICE spk full path file name.
        
        Returns
        -------
        nacelle_id : int
            SPICE spacecrfat id.
        """
        nacelle_id = sp.spkobj(spk_file)[0]
        return nacelle_id
    
    def make_nacelle_cks(self,spk_file1,frame_file1,spk_file2,frame_file2):
        """Create and load SPICE CK kernel.
        
        Parameters:
        --------------
        spk_file1 : string
            Nacelle1 SPICE spk full path file name.
        frame_file1 : string
            Nacelle1 SPICE frame full path file name (tf-file) of the respective spk_file.
        spk_file2 : string
            Nacelle2 SPICE spk full path file name.
        frame_file2 : string
            Nacelle2 SPICE frame full path file name (tf-file) of the respective spk_file.
        
        Returns
        -------
        ck_file1 :string
            Output Nacelle1 CK kernel file.
        ck_file2 :string
            Output Nacelle2 CK kernel file.
        cov : SPICE time window
            Intersection of the 2 nacelle time windows
        
        """
        # set the frequency at which the orientation is to be created
        self.freq = float(self.config['CAMPAIGN']['freq'])*u.min
        # retrieve offset angles
        self.offset_az = float(self.config['CAMERA']['az_off']) * u.deg
        self.offset_el = float(self.config['CAMERA']['el_off']) * u.deg
        self.offset_roll = float(self.config['CAMERA']['roll_off']) * u.deg
        # make output ck files
        ck_file1 = self.out_dir + self.nacelle_name1 + '.ck'
        ck_file2 = self.out_dir + self.nacelle_name2 + '.ck'
        
        # if output file exist, simply load them and set coverage
        if (os.path.exists(ck_file1) and os.path.exists(ck_file2) and self.read_res):
            # retrieve nacelle ids from spk files
            nacelle_id1 = sp.spkobj(spk_file1)[0]
            nacelle_id2 = sp.spkobj(spk_file2)[0]
            # fetch spk coverage for each spk file
            cov1 = sp.spkcov(spk_file1,nacelle_id1)
            cov2 = sp.spkcov(spk_file2,nacelle_id2)
            start1,stop1 = sp.wnfetd(cov1,0)
            start2,stop2 = sp.wnfetd(cov2,0)
            # compute intersection time coverage
            cov = sp.wnintd(cov1,cov2)
            # make fake spacecraft clock file names
            tsc_file1 = ck_file1.replace('.ck','_fake.tsc')
            tsc_file2 = ck_file2.replace('.ck','_fake.tsc')
        else:
            # make temporary directory
            sim_tmp_dir = self.tmp_dir + '/' + self.sim_id + '/'
            if not os.path.isdir(sim_tmp_dir):
                os.makedirs(sim_tmp_dir)
                log.info('Temporary directory created: '+sim_tmp_dir)
            # check if MOMET type of observations
            Lfix_el = False
            try:
                if self.config['CAMPAIGN']['sites_type'].upper() in ['MOMET']:
                    Lfix_el = True
                    log.warning('MOMET-type of observation detected=> fixed elevation')
            except:
                log.info('No MoMet type of observation')
            # create ck kernel
            log.info('Now create ck kernels')
            (ck_file1,ck_file2,
             tsc_file1,tsc_file2,cov) = create_ck(spk_file1,frame_file1,
                                          spk_file2,frame_file2,
                                          offset_az=self.offset_az,
                                          offset_el=self.offset_el,
                                          fix_el=Lfix_el,
                                          offset_roll=self.offset_roll,
                                          meteor_height=self.meteor_alt,
                                          tmp_dir=sim_tmp_dir,freq=self.freq,
                                          out_ck_file1=ck_file1,out_ck_file2=ck_file2,
                                          spice_exe_path=self.spice_exe_path,
                                          kernel_path=self.kernel_path)#,load_stdker=False,load_spker=False,load_frame=False)
            # remove temporary directory
            try:
                shutil.rmtree(sim_tmp_dir)
                log.info('Temporary directory deleted: '+sim_tmp_dir)
            except:
                msg = '*** FATAL ERROR: unable to remove temporary directory: '+sim_tmp_dir
                log.error(msg)
                raise IOError(msg)
        # end of test if ck files already exists.
        
        # slightly shrink cov to make sure nacelle ephemeris can be retrieved.
        start,stop = sp.wnfetd(cov,0)
        start = int(start+1)
        stop = int(stop-1)
        outcov = sp.utils.support_types.SPICEDOUBLE_CELL(2)
        sp.wninsd(start,stop,outcov)
        log.info('coverage: start: '+sp.et2utc(start,'ISOC',2,50))
        log.info('coverage: stop: '+sp.et2utc(stop,'ISOC',2,50))
        
        # load ck file
        sp.furnsh(ck_file1)
        sp.furnsh(ck_file2)
        log.info('ck file loaded: '+ck_file1)
        log.info('ck file loaded: '+ck_file2)
        # load tsc file
        sp.furnsh(tsc_file1)
        sp.furnsh(tsc_file2)
        log.info('tsc file loaded: '+tsc_file1)
        log.info('tsc file loaded: '+tsc_file2)
        return ck_file1,ck_file2,outcov
    
    def make_ik(self,spk_file,tf_file,camera_fov,kernel_path='/astrodata/kernels/'):
        """Make SPICE instrument kernel.
        
        Parameters:
        ------------
        spk_file : string
            Nacelle SPICE position-velocity (spk) kernel full path file name.
        tf_file : string
            Nacelle SPICE frame (fk) kernel full path file name.
        camera_fov : 2-array of astropy.units.Quantity object
            Camera field of view (azimuth, elevation extents).
        kernel_path : string, optional
            Path to SPICE kernel files. Default is '/astrodata/kernels/'
        
        Returns
        -------
        ik_file : string
            SPICE instrument kernel full path file name.
        ik_id : int
            SPICE instrument id.
        ik_name : string
            SPICE instrument name.
        
        """
        # create output file name in output directory
        ik_file,ik_id,ik_name = create_ik(spk_file,tf_file,camera_fov,outdir=self.out_dir,kernel_path=kernel_path)
        # load ik kernel
        sp.furnsh(ik_file)
        log.info('ik_file loaded: '+ik_file)
        return ik_file,ik_id,ik_name
    
    def get_radiant_position(self,t):
        """Get drifted radiant position in the sky for givent date.
        
        Parameters
        ----------
        radiant_skycoo : astropy.coordinates.SkyCoordinate object
            Sky coordinates of the meteor shower radiant.
        eloc : astropy.coordinates.EarthLocation object
            Location of observer.
        t : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        radec : astropy.coordinates.SkyCoord Object
            Coordinates of the radiant corrected for drift.
        """
        radec = self.radiant.drift(t)
        return radec
    
    def get_Moon_position(self,t,eloc):
        """Get Moon position at time t, from location eloc.
        
        Parameters
        ----------
        eloc : astropy.coordinates.EarthLocation object
            Location of observer.
        t : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        moon_skycoo : astropy.coordinates.SkyCoordinate object
            Sky coordinates of the Moon.
        altaz : astropy.coordinates.AltAz Object
            Altazimuthal coordinates of the Moon.
        """
        # use astropy to get Moon RA,DEC
        moon_skycoo = get_moon(t,eloc)
        # use astropy to convert Moon RA,DEC into AltAz
        altaz = moon_skycoo.transform_to(AltAz(obstime=t,location=eloc))
        return (moon_skycoo,altaz)
    
    def get_Moon_position_spice(self,time,nacelle_name,nacelle_eloc):
        """Get Moon position at time t, from location eloc.
        
        Parameters
        ----------
        nacelle_name : string
            SPICE nancelle name.
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        moon_skycoo : astropy.coordinates.SkyCoordinate object
            Sky coordinates of the Moon.
        altaz : astropy.coordinates.AltAz Object
            Altazimuthal coordinates of the Moon.
        """
        # convert time
        et = sp.str2et(time.isot)
        # get topo-like frame name
        topo_frame = make_topoframe_name(nacelle_name)
        # get apparent position of Moon wrt nacelle in topo-like frame
        (moon_eph,lt) = sp.spkezr('MOON',et,topo_frame,'LT+S',nacelle_name)
        # convert ephemeris vector into angle(in topo-like frame)
        raltaz = sp.recrad(moon_eph[:3])
        az = (360*u.deg-(raltaz[1]*u.rad).to('deg'))%(360*u.deg)
        el = (raltaz[2]*u.rad).to('deg')
        altaz = AltAz(az=az,alt=el,obstime=time,location=nacelle_eloc)
        
        # get apparent position of Moon wrt nacelle in J2000 frame
        (moon_eph,lt) = sp.spkezr('MOON',et,'J2000','LT+S',nacelle_name)
        # convert ephemeris vector into angle(in topo-like frame)
        rradec = sp.recrad(moon_eph[:3])
        ra = (rradec[1]*u.rad).to('deg')
        dec = (rradec[2]*u.rad).to('deg')
        moon_skycoo = SkyCoord(ra=ra,dec=dec,frame='icrs')
        return (moon_skycoo,altaz)
    
    def get_Sun_position_spice(self,time,nacelle_name,nacelle_eloc):
        """Get Sun position at time time, for nacelle nacelle_name from location eloc.
        
        Parameters
        ----------
        nacelle_name : string
            SPICE nancelle name.
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        moon_skycoo : astropy.coordinates.SkyCoordinate object
            Sky coordinates of the Moon.
        altaz : astropy.coordinates.AltAz Object
            Altazimuthal coordinates of the Moon.
        """
        # convert time
        et = sp.str2et(time.isot)
        # get topo-like frame name
        topo_frame = make_topoframe_name(nacelle_name)
        # get apparent position of Moon wrt nacelle in topo-like frame
        (sun_eph,lt) = sp.spkezr('SUN',et,topo_frame,'LT+S',nacelle_name)
        # convert ephemeris vector into angle(in topo-like frame)
        raltaz = sp.recrad(sun_eph[:3])
        az = (360*u.deg-(raltaz[1]*u.rad).to('deg'))%(360*u.deg)
        el = (raltaz[2]*u.rad).to('deg')
        altaz = AltAz(az=az,alt=el,obstime=time,location=nacelle_eloc)
        
        # get apparent position of Moon wrt nacelle in J2000 frame
        (sun_eph,lt) = sp.spkezr('SUN',et,'J2000','LT+S',nacelle_name)
        # convert ephemeris vector into angle(in topo-like frame)
        rradec = sp.recrad(sun_eph[:3])
        ra = (rradec[1]*u.rad).to('deg')
        dec = (rradec[2]*u.rad).to('deg')
        sun_skycoo = SkyCoord(ra=ra,dec=dec,frame='icrs')
        return (sun_skycoo,altaz)
    
    def get_Sun_position(self,t,eloc):
        """Get Sun position at time t, from location eloc.
        
        Parameters
        ----------
        eloc : astropy.coordinates.EarthLocation object
            Location of observer.
        t : astropy.time.Time object
            Time for which the ephemeris is needed.
        
        Returns
        -------
        sun_skycoo : astropy.coordinates.SkyCoordinate object
            Sky coordinates of the Sun.
        altaz : astropy.coordinates.AltAz Object
            Altazimuthal coordinates of the Sun.
        
        """
        # use astropy to get Sun RA,DEC
        sun_skycoo = get_sun(t)
        # use astropy to convert Sun RA,DEC into AltAz
        altaz = sun_skycoo.transform_to(AltAz(obstime=t,location=eloc))
        return (sun_skycoo,altaz)
    
    def compute_geometry(self,nacelle1_name,nacelle2_name,time):
        """Compute the geometry of the double-station flight.
        
        Compute location of meteor shower radiant, Moon, Sun, camera pointing direction,
        in ICRS and Altazimuthal coordinates.
        
        Parameters
        ----------
        nacelle1_name : string
            Nacelle1 SPICE name.
        nacelle2_name : string
            Nacelle2 SPICE name.
        time : astropy.time.Time objec
            Time when to check geometry.
        
        Returns
        -------
        dist_phys : astropy.units.Quantity Object
            physical distance between the 2 nacelles.
        dist_radiant : 2-array of astropy.units.Quantity Object
            Angular distance between the pointing directions of the 2 nacelles and the radiant.
        dist_Moon : 2-array of astropy.units.Quantity Object
            Angular distance between the pointing directions of the 2 nacelles and the Moon.
        dist_Sun : 2-array of astropy.units.Quantity Object
            Angular distance between the pointing directions of the 2 nacelles and the Sun.
        """
        # get the location of the 2 nacelles
        (eloc1,radec1,altaz1) = self.get_nacelle_pointing(nacelle1_name,time,append_posradec=True)
        (eloc2,radec2,altaz2) = self.get_nacelle_pointing(nacelle2_name,time,append_posradec=True)
        # compute physical distance between the nacelles
        phys_dist,azimuth12 = utils.distaz(eloc1,eloc2)
        # TODO: get physical conditions around nacelle: Temperature, Pressure
        #(P1,T1) = self.get_nacelle_conditions(nacelle1_name,time)
        #(P2,T2) = self.get_nacelle_conditions(nacelle2_name,time)
        
        # get location of the radiant, Moon and Sun
        radiant_skycoo = self.get_radiant_position(time)
        radiant_altaz1 = radiant_skycoo.transform_to(AltAz(obstime=time,location=eloc1))
        radiant_altaz2 = radiant_skycoo.transform_to(AltAz(obstime=time,location=eloc2))
        (moon_skycoo1,moon_altaz1) = self.get_Moon_position_spice(time,nacelle1_name,eloc1)
        (moon_skycoo2,moon_altaz2) = self.get_Moon_position_spice(time,nacelle2_name,eloc2)
        (sun_skycoo1,sun_altaz1) = self.get_Sun_position_spice(time,nacelle1_name,eloc1)
        (sun_skycoo2,sun_altaz2) = self.get_Sun_position_spice(time,nacelle2_name,eloc2)
        # compute distance of center of FOV to radiant, moon and sun
        sep_moon1 = moon_skycoo1.separation(radec1)
        sep_moon2 = moon_skycoo2.separation(radec2)
        sep_sun1 = sun_skycoo1.separation(radec1)
        sep_sun2 = sun_skycoo2.separation(radec2)
        sep_radiant1 = radiant_skycoo.separation(radec1)
        sep_radiant2 = radiant_skycoo.separation(radec2)
        # convert time
        et = sp.str2et(time.isot)
        # store data
        data = np.array([time,et,# eloc1,eloc2,
                        phys_dist.to('m'),azimuth12.to('deg'),
                        radec1.ra.to('deg'),radec1.dec.to('deg'),
                        radec2.ra.to('deg'),radec2.dec.to('deg'),
                        radiant_skycoo.ra.to('deg'),radiant_skycoo.dec.to('deg'),
                        sep_radiant1.to('deg'),sep_radiant2.to('deg'),
                        moon_skycoo1.ra.to('deg'),moon_skycoo1.dec.to('deg'),
                        sep_moon1.to('deg'),sep_moon2.to('deg'),
                        sun_skycoo1.ra.to('deg'),sun_skycoo1.dec.to('deg'),
                        sep_sun1.to('deg'),sep_sun2.to('deg'),
                        altaz1.az.to('deg'),altaz1.alt.to('deg'),
                        altaz2.az.to('deg'),altaz2.alt.to('deg'),
                        radiant_altaz1.az.to('deg'),radiant_altaz1.alt.to('deg'),
                        radiant_altaz2.az.to('deg'),radiant_altaz2.alt.to('deg'),
                        moon_altaz1.az.to('deg'),moon_altaz1.alt.to('deg'),
                        moon_altaz2.az.to('deg'),moon_altaz2.alt.to('deg'),
                        sun_altaz1.az.to('deg'),sun_altaz1.alt.to('deg'),
                        sun_altaz2.az.to('deg'),sun_altaz2.alt.to('deg'),
                        ])
        return data,eloc1,eloc2
    
    def compute_geometry_relative_time(self):
        """Compute relative time and set self.time_rel parameter.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        """
        times = Time(self.geom_data['Time'],scale='utc')
        t0 = Time(sp.et2utc(self.start,"ISOC",2,50),scale='utc')
        time_rel = times - t0
        self.geom_data['Time_rel'] = time_rel #.to('min')
        self.geom_data['Time_rel'].unit = 'min'
        self.geom_data['Time_rel'].info.format = '7.3f'
        return
    
    def make_plots(self):
        """Plots results
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # compute relative time to start
        self.compute_geometry_relative_time()
        # single quantity versus relative time
        qt2plot = ['Dphy','Az12']
        qtdisp = ['Physical distance','Azimuth nacelle 1 to 2']
        qtunits = ['km','deg','deg','deg','deg','deg','deg','deg']
        for (qtname,qtdis,unit) in zip(qt2plot,qtdisp,qtunits):
            fig=plt.figure()
            log.info('Ploting '+qtname+' in '+unit+' vs Time')
            plt.plot(self.geom_data['Time_rel'].to('min').value,
                        self.geom_data[qtname].to(unit).value,'b-',label=qtname) # ,s=1,marker='P'
            plt.title(qtdis+' versus Time',color='indianred')
            plt.xlabel('Time (min)')
            plt.ylabel(qtname+' ('+unit+')')
            plt.legend(loc='lower right')
            outfile = self.out_dir+'/'+qtname+'-Time.png'
            plt.savefig(outfile,dpi=300)
            plt.close(fig)
            log.info('plot saved in '+outfile)
        # plot quantity by pair
        qt2plot1 = ['SepRad1','SepMoo1','SepSun1']
        qt2plot2 = ['SepRad2','SepMoo2','SepSun2']
        qtdisp = ['Separation from the radiant','Separation from the Moon','Separation from the Sun']
        qtunits = ['deg','deg','deg']
        for (qtname1,qtname2,qtdis,unit) in zip(qt2plot1,qt2plot2,qtdisp,qtunits):
            fig=plt.figure()
            log.info('Ploting '+qtname1+' and '+qtname2+' in '+unit+' versus Time')
            plt.plot(self.geom_data['Time_rel'].to('min').value,self.geom_data[qtname1].to(unit).value,'b-',
                     self.geom_data['Time_rel'].to('min').value,self.geom_data[qtname2].to(unit).value,'r-') #,label=[qtname1,qtname2])
            plt.title(qtdis+' 1 and '+qtdis+' 2 versus Time',color='indianred')
            plt.xlabel('Time [min]')
            plt.ylabel(qtname1+' and '+qtname2+' ('+unit+')')
            plt.legend(loc='lower right')
            outfile = self.out_dir+'/'+qtname1+'-'+qtname2+'-Time.png'
            plt.savefig(outfile,dpi=300)
            plt.close(fig)
            log.info('plot saved in '+outfile)
        # plot all Azimuths
        unit = 'deg'
        fig=plt.figure()
        log.info('Ploting all azimuth vs Time')
        plt.plot(self.geom_data['Time_rel'].to('min').value,self.geom_data['Cam1Az'].to(unit).value,'b^',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['Cam2Az'].to(unit).value,'bv',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['RadAz1'].to(unit).value,'rx',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['RadAz2'].to(unit).value,'r+',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['MooAz1'].to(unit).value,'mo',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['MooAz2'].to(unit).value,'m*',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SunAz1'].to(unit).value,'yo',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SunAz2'].to(unit).value,'yX')#,
                 #labels=['Cam1','Cam2','Radiant1','Radiant2','Moon1','Moon2','Sun1','Sun2'])
        plt.title('All Azimuths versus Time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Azimuth (deg)')
        plt.legend(['Cam1','Cam2','Radiant1','Radiant2','Moon1','Moon2','Sun1','Sun2'],loc='lower right')
        outfile = self.out_dir+'AllAz-Time.png'
        plt.savefig(outfile,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+outfile)
        # plot all Elevations
        unit = 'deg'
        fig=plt.figure()
        log.info('Ploting all elevations vs Time')
        plt.plot(self.geom_data['Time_rel'].to('min').value,self.geom_data['Cam1Alt'].to(unit).value,'b^',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['Cam2Alt'].to(unit).value,'bv',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['RadAlt1'].to(unit).value,'rx',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['RadAlt2'].to(unit).value,'r+',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['MooAlt1'].to(unit).value,'mo',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['MooAlt2'].to(unit).value,'m*',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SunAlt1'].to(unit).value,'yo',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SunAlt2'].to(unit).value,'yX')#,
                 #labels=['Cam1','Cam2','Radiant1','Radiant2','Moon1','Moon2','Sun1','Sun2'])
        plt.title('All elevations versus Time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Elevqtion (deg)')
        plt.legend(['Cam1','Cam2','Radiant1','Radiant2','Moon1','Moon2','Sun1','Sun2'],loc='lower right')
        outfile = self.out_dir+'AllAlt-Time.png'
        plt.savefig(outfile,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+outfile)
        # plot all Separations
        unit = 'deg'
        fig=plt.figure()
        log.info('Ploting all Separations vs Time')
        plt.plot(self.geom_data['Time_rel'].to('min').value,self.geom_data['SepRad1'].to(unit).value,'rx',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SepRad2'].to(unit).value,'r+',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SepMoo1'].to(unit).value,'mo',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SepMoo2'].to(unit).value,'m*',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SepSun1'].to(unit).value,'yo',
                 self.geom_data['Time_rel'].to('min').value,self.geom_data['SepSun2'].to(unit).value,'yX',
                 np.array([np.min(self.geom_data['Time_rel'].to('min').value),
                           np.max(self.geom_data['Time_rel'].to('min').value)]), np.repeat(float(self.config['CAMPAIGN']['min_bsgt_rad']),2),'r--',
                 np.array([np.min(self.geom_data['Time_rel'].to('min').value),
                           np.max(self.geom_data['Time_rel'].to('min').value)]), np.repeat(float(self.config['CAMPAIGN']['min_bsgt_moon']),2),'m--',
                 np.array([np.min(self.geom_data['Time_rel'].to('min').value),
                           np.max(self.geom_data['Time_rel'].to('min').value)]), np.repeat(float(self.config['CAMPAIGN']['min_bsgt_sun']),2),'y--')#,
                 #labels=['Cam1','Cam2','Radiant1','Radiant2','Moon1','Moon2','Sun1','Sun2'])
        plt.title('All separations versus Time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Angular separation (deg)')
        plt.legend(['Radiant1','Radiant2','Moon1','Moon2','Sun1','Sun2'],loc='upper right')
        outfile = self.out_dir+'AllSep-Time.png'
        plt.savefig(outfile,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+outfile)
        
        return
    
    def decide(self):
        """Decide whether or not the observation conditions are ok.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        # retrieve min/max physical distance between the nacelles
        min_obs_dist = float(self.config['CAMPAIGN']['min_obs_dist']) * u.km
        max_obs_dist = float(self.config['CAMPAIGN']['max_obs_dist']) * u.km
        # retrieve min angular distance to the radiant, Moon, Sun
        min_bsgt_rad = float(self.config['CAMPAIGN']['min_bsgt_rad']) * u.deg
        min_bsgt_moo = float(self.config['CAMPAIGN']['min_bsgt_moon']) * u.deg
        min_bsgt_sun = float(self.config['CAMPAIGN']['min_bsgt_sun']) * u.deg
        # check with data
        crit_phy_min = True if np.all(self.geom_data['Dphy'].to('km') > min_obs_dist.to('km')) else False
        crit_phy_max = True if np.all(self.geom_data['Dphy'].to('km') < max_obs_dist.to('km')) else False
        crit_rad1 = True if np.all(self.geom_data['SepRad1'].to('deg') > min_bsgt_rad.to('deg')) else False
        crit_rad2 = True if np.all(self.geom_data['SepRad2'].to('deg') > min_bsgt_rad.to('deg')) else False
        crit_moo1 = True if np.all(self.geom_data['SepMoo1'].to('deg') > min_bsgt_moo.to('deg')) else False
        crit_moo2 = True if np.all(self.geom_data['SepMoo2'].to('deg') > min_bsgt_moo.to('deg')) else False
        crit_sun1 = True if np.all(self.geom_data['SepSun1'].to('deg') > min_bsgt_sun.to('deg')) else False
        crit_sun2 = True if np.all(self.geom_data['SepSun2'].to('deg') > min_bsgt_sun.to('deg')) else False
        # decide
        all_crit = np.array([crit_phy_min,crit_phy_max,crit_rad1,crit_rad2,
                    crit_moo1,crit_moo2,crit_sun1,crit_sun2])
        all_critstr = ['crit_phy_min','crit_phy_max','crit_rad1','crit_rad2',
                       'crit_moo1','crit_moo2','crit_sun1','crit_sun2']
        decide = True if np.all(all_crit) else False
        # log result
        for (param,value) in zip(all_crit,all_critstr):
            log.info(value+' : '+str(param))
        log.info('all_crit: '+str(all_crit))
        log.info('decide: '+str(decide))
        # save results
        all_critstr.append('decide')
        decision_tab = QTable(np.append(all_crit,[decide]),names=all_critstr,dtype=[bool,bool,bool,bool,bool,bool,bool,bool,bool])
        decision_tab.write(self.decision_file,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('Decision saved in '+self.decision_file)
        return decide
    

    def make_filenames(self):
        """Make output file names and create fov_data QTable.
        
        Global data are created.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        None.
        
        """
        # set effective surveyed area file names
        self.area_file = self.config['CAMPAIGN']['area_file']
        
        # create effective surveyed area output tables
        self.area = QTable(names=['Time','et','camera','single_area','reduced_single_area','double_area','reduced_double_area'],dtype=['object','float','object','float','float','float','float'])
        
        # set fov data file name
        self.fov_dat_file = self.config['CAMPAIGN']['fov_dat_file']
        # make boresight and 4 corners output data QTable
        self.fov_data = obsutils.make_fov_table()        
        
        # number of meteors file name
        self.numet_file = self.config['CAMPAIGN']['numet_file']
        bsn,ext = os.path.splitext(os.path.basename(self.numet_file))
        self.numet_tot_file = self.numet_file.replace(bsn,bsn+'_tot')
        # make grids file names
        self.grid_data_file = {}
        # create output file name for nacelle1 and 2
        bsn,ext = os.path.splitext(os.path.basename(self.fov_dat_file))
        for nacelle_name in self.list_nacelle_names:
            self.grid_data_file[nacelle_name] = self.fov_dat_file.replace(bsn,nacelle_name+'_grid')
        
        # set anchors in fov data file name
        self.ancinfov_file = self.config['CAMPAIGN']['ancinfov_file']
        
        return
    
    def compute_all_fov(self):
        """Compute field of view as a function of time.
        
        Data and in particular the surveyed area as a function of time
        are saved in output files.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # make FOV, area, grids file names, as well as FOV QTable.
        self.make_filenames()
        
        # if data exist read them
        if self.check_fovareagrids():
            self.load_fovNarea()
            self.load_grids()
        else:
            # otherwise launches the computation for each time step
            # make a list of times when fov are to be computed
            ets = np.append(np.arange(self.start,self.stop,self.freq.to('s').value),self.stop-0.009)
            # compute fov over time
            for et in ets:
                self.compute_fov_attime(et)
            """This method generates a 'TypeError: can't pickle _thread.RLock objects' error...
            # get total number of processors
            num_cores = multiprocessing.cpu_count()
            # create Pool to parallelize the computation of the anchors in FOV for given camera
            with multiprocessing.Pool(num_cores) as pool:
                pool.map(self.compute_fov_attime,ets)
            """      
            # save FOV, area and grids data in dat and kml format
            self.save_fovNarea_data()    
            self.save_grid_data()
            self.make_kmls()     
        
        # make a few plots
        self.plot_fov()
        
        return
    
    def check_fovareagrids(self):
        """Check if FOV, area and grids data are already created.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        check : boolean.
            True if FOV, single and double area and grids data files exist.
        
        """
        check = True
        if not self.read_res:
            check = False
        if not os.path.exists(self.fov_dat_file):
            check = False
        if not os.path.exists(self.area_file):
            check = False
        for nacelle_name in self.list_nacelle_names:
            if not os.path.exists(self.grid_data_file[nacelle_name]):
                check = False
        return check
    
    def add_fov_data(self,time,nacelle_name,coords_lo,crn_dst,fov_area):
        """Add FOV data into fov_data global variable.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for each fov data to add.
        nacelle_name : string
            Nacelle name.
        coords_lo : array of float
            Coordinates of fov corners and boresight at atmosphere top to add.
        crn_dst : array of float
            distance between nacelle and fov corners and boresight.
        fov_area : astropy.units.Quantity object.
            Area surveyed by nacelle_name (single station).
        
        Returns
        -------
        None.
        
        """
        et = sp.str2et(time.isot)
        # add bounds into data Table: time, et, name, [lon, lat, alt, distance]*[boresight and 4 corners], area
        self.fov_data.add_row([time.isot,et,nacelle_name,
                               coords_lo[-1][0]*u.deg,coords_lo[-1][1]*u.deg,coords_lo[-1][2]*u.m,crn_dst[-1]*u.km,
                               coords_lo[-2][0]*u.deg,coords_lo[-2][1]*u.deg,coords_lo[-2][2]*u.m,crn_dst[-2]*u.km,
                               coords_lo[-3][0]*u.deg,coords_lo[-3][1]*u.deg,coords_lo[-3][2]*u.m,crn_dst[-3]*u.km,
                               coords_lo[-4][0]*u.deg,coords_lo[-4][1]*u.deg,coords_lo[-4][2]*u.m,crn_dst[-4]*u.km,
                               coords_lo[-5][0]*u.deg,coords_lo[-5][1]*u.deg,coords_lo[-5][2]*u.m,crn_dst[-5]*u.km,
                               fov_area])
        return
    
    def save_fovNarea_data(self):
        """Save FOV data into data type file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # sort FOV data
        self.fov_data.sort('Time')
        # add index
        self.fov_data.add_index('Time')
        # write into FOV data output file
        self.fov_data.write(self.fov_dat_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
        log.info('FOV Data saved in '+self.fov_dat_file)
        
        # save common surveyed area data
        self.area['single_area'].unit = 'km2'
        self.area['double_area'].unit = 'km2'
        self.area['reduced_single_area'].unit = 'km2'
        self.area['reduced_double_area'].unit = 'km2'
        self.area['single_area'].info.format = '10.3f'
        self.area['double_area'].info.format = '10.3f'
        self.area['reduced_single_area'].info.format = '10.3f'
        self.area['reduced_double_area'].info.format = '10.3f'
        self.area['et'].info.format = '13.3f'
        self.area.sort('Time')
        self.area.add_index('Time')
        self.area.write(self.area_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
        log.info('Area Data saved in '+self.area_file)
        return
    
    def load_fovNarea(self):
        """Load FOV and area data.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.info('Reading FOV file: '+self.fov_dat_file)
        self.fov_data = QTable.read(self.fov_dat_file,format='ascii.fixed_width_two_line')
        self.fov_data = obsutils.set_fov_units(self.fov_data)
        
        log.info('Reading area file: '+self.area_file)
        self.area = QTable.read(self.area_file,format='ascii.fixed_width_two_line')
        self.area['single_area'].unit = 'km2'
        self.area['double_area'].unit = 'km2'
        self.area['reduced_single_area'].unit = 'km2'
        self.area['reduced_double_area'].unit = 'km2'
        
        return
    
    
    def plot_fov(self):
        """"Make plots of differenet FOV parameters as a function of time.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        area = {}
        syms = ['rx','bo']
        # loop over nacelles
        for nacelle_name in self.list_nacelle_names:
            mask = self.area['camera']==nacelle_name
            area[nacelle_name] = self.area[mask]
        
        # convert time since 
        # Time(area[nacelle_name]['Time'],format='isot',scale='utc')
        # does not seem to work
        time = []
        for t in area[nacelle_name]['Time']:
            time.append(str(t))
        time = Time(time,format='isot',scale='utc')
        rel_t = (time - self.shower.time_max).to('min')
        
        # plot double-station area
        area_plt_file = self.area_file.replace('.dat','_double.png')
        fig=plt.figure()
        log.debug('Ploting surveyed area versus time')
        for (nacelle_name,sym) in zip(self.list_nacelle_names,syms):
            plt.plot(rel_t.to('min').value,area[nacelle_name]['double_area'].to('km2').value,sym,label=nacelle_name)
        #plt.ylim([0.0,np.max(self.area['double_area']).to('km2').value])
        plt.title('Double-station surveyed area versus time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Area (km2)')
        plt.legend(loc='upper right')
        #plt.gcf().autofmt_xdate()
        plt.savefig(area_plt_file,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+area_plt_file)                
        
        # plot double-station reduced area
        area_plt_file = self.area_file.replace('.dat','_double_reduced.png')
        fig=plt.figure()
        log.info('Ploting reduced area vs Time')
        for (nacelle_name,sym) in zip(self.list_nacelle_names,syms):
            #rel_t = (Time(area[nacelle_name]['Time'],scale='utc') - self.shower.time_max).to('min')
            plt.plot(rel_t.to('min').value,area[nacelle_name]['reduced_double_area'].to('km2').value,sym,label=nacelle_name)
        #plt.ylim([0.0,np.max(self.area['reduced_double_area']).to('km2').value*1.1])
        plt.title('Double-station reduced area versus time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Reduced area (km^2)')
        plt.legend(loc='upper right')
        #plt.gcf().autofmt_xdate()
        plt.savefig(area_plt_file,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+area_plt_file)                
        
        # plot single station area
        area_plt_file = self.area_file.replace('.dat','_single.png')
        fig=plt.figure()
        log.info('Ploting surveyed area vs Time')
        for (nacelle_name,sym) in zip(self.list_nacelle_names,syms):
            #rel_t = (Time(area[nacelle_name]['Time'],scale='utc') - self.shower.time_max).to('min')
            plt.plot(rel_t.to('min').value,area[nacelle_name]['single_area'].to('km2').value,sym,label=nacelle_name)
        #plt.ylim([0.0,np.max(self.area['single_area']).to('km2').value])
        plt.title('Single-station surveyed area versus time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Area (km^2)')
        plt.legend(loc='upper right')
        #plt.gcf().autofmt_xdate()
        plt.savefig(area_plt_file,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+area_plt_file)                
        
        # plot single stationreduced area
        area_plt_file = self.area_file.replace('.dat','_single_reduced.png')
        fig=plt.figure()
        log.info('Ploting surveyed area vs Time')
        for (nacelle_name,sym) in zip(self.list_nacelle_names,syms):
            #rel_t = (Time(area[nacelle_name]['Time'],scale='utc') - self.shower.time_max).to('min')
            plt.plot(rel_t.to('min').value,area[nacelle_name]['reduced_single_area'].to('km2').value,sym,label=nacelle_name)
        #plt.ylim([0.0,np.max(self.area['reduced_single_area']).to('km2').value])
        plt.title('Single-station reduced area versus Time',color='indianred')
        plt.xlabel('Time (min)')
        plt.ylabel('Area (km^2)')
        plt.legend(loc='upper right')
        #plt.gcf().autofmt_xdate()
        plt.savefig(area_plt_file,dpi=300)
        plt.close(fig)
        log.info('plot saved in '+area_plt_file)                
        
        return
    
    def compute_fov_attime(self,et):
        """Compute FOV for the 2 cameras.
        
        Data appended to self.fov_data and self.grids data.
        
        Parameters
        ----------
        et : float
            SPICE time (sec after J2000).
        
        Returns
        -------
        None.
        
        """
        # set time
        time = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
        log.info('========== Computing FOV at time: '+time.isot)
                        
        # loop over the stations
        for nacelle_name in [self.nacelle_name1,self.nacelle_name2]:
            # retrieve nacelle SPICE names and ids
            nacelle_id = sp.bodn2c(nacelle_name)
            camera_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
            camera_id = make_camera_id(nacelle_id)
            # retrieve camera FOV information
            [shape,insfrm,brsgt,n,bounds] = sp.getfov(camera_id,4)
            # make a list of bounds, and add boresight
            bounds = bounds.tolist()
            bounds.append(brsgt)
            # creates coordinates: coords_lo: fov at atm top
            (coords_lo,crn_dst,bounds_eloc) = ([],[], [])
            # loop over FOV boundary angles
            for bound in bounds:
                # compute intersection with top of atmosphere
                bound_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',nacelle_name,camera_frame_name,bound)
                # compute physical distance between bound and nacelle
                crn_dst.append(sp.vnorm(vecout))
                # convert into EarthLocation
                bound_top_eloc = EarthLocation.from_geocentric(bound_top[0],bound_top[1],bound_top[2],unit='km')
                bounds_eloc.append(bound_top_eloc)
                # store FOV boundaries coordinates
                coords_lo.append((bound_top_eloc.lon.to('deg').value,bound_top_eloc.lat.to('deg').value,bound_top_eloc.height.to('m').value))
            # get radius of the Earth at boresight latitude (latest computed)
            R_earth = utils.latrad(bounds_eloc[-1].lat.to('rad').value)
            # compute theoretical area covered by the fov
            fov_area = utils.elocs42area(bounds_eloc[-2],bounds_eloc[-3],bounds_eloc[-4],bounds_eloc[-5],r=R_earth)
            # Store data in fov_data
            self.add_fov_data(time,nacelle_name,coords_lo,crn_dst,fov_area)
            # make fov grid
            grid_data,redarea_data,area_sgl_total,area_sgl_reduced_total = self.make_grid(nacelle_name,et,step_grid=self.fov_step)
            # compute double-station area
            dblfov_data,area_dbl_total,red_area_dbl_total = self.compute_double_area(grid_data,redarea_data)
            # store area data in QTable: self.area
            self.area.add_row([time.isot,et,nacelle_name,area_sgl_total,area_sgl_reduced_total,area_dbl_total,red_area_dbl_total])
            # store grid_data and red_area_data into self.grids
            self.add_grids_data(grid_data,redarea_data,dblfov_data)
        # end of loop over nacelle
        return
    
    def make_grid_table(self,time,nacelle_name,eloc_cntr,elocs_crn,az,el,dist,area,Lgrnd):
        """Make a QTable grid data.
        
        Parameters
        ----------
        time :astropy.time.Time ovject
            Time of the record.
        nacelle_name : string
            Nacelle name
        eloc_cntr : astropy.coordinates.EarthLocation object.
            Location of center of grid cell.
        elocs_crn : 4-astropy.coordinates.EarthLocation object.
            Location of 4 corners of grid cell.
        az : astropy.units.Quantity object
            Azimuth of center of cell as seen from nacelle.
        el : astropy.units.Quantity object
            Elevation of center of cell as seen from nacelle.            
        dist : astropy.units.Quantity object
            Physical distance between nacelle and center of cell.
        area : astropy.units.Quantity object
            Cell physical surface area.
        Lgrnd : boolean
            True if ground in the FOV for this cell.
        
        Returns
        -------
        grid_data : astropy.table.QTable object.
            QTable with all parameters.
        """
        
        et = sp.str2et(time.isot)
        grid_data = obsutils.make_grid_data_qtable()
        grid_data.add_row([time.isot,et,nacelle_name,
             eloc_cntr.lon.to('deg')   ,eloc_cntr.lat.to('deg')   ,eloc_cntr.height.to('m'),
             elocs_crn[0].lon.to('deg'),elocs_crn[0].lat.to('deg'),elocs_crn[0].height.to('m'),
             elocs_crn[1].lon.to('deg'),elocs_crn[1].lat.to('deg'),elocs_crn[1].height.to('m'),
             elocs_crn[2].lon.to('deg'),elocs_crn[2].lat.to('deg'),elocs_crn[2].height.to('m'),
             elocs_crn[3].lon.to('deg'),elocs_crn[3].lat.to('deg'),elocs_crn[3].height.to('m'),
             az.to('deg'),el.to('deg'),
             dist.to('km'),area.to('km2'),Lgrnd])
        return grid_data
    
    def save_grid_data(self):
        """Save grid data in dat file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # sort grid data
        self.grids.sort(['Time','nac_name','ctrn_lon','ctrn_lat'])
        # loop over nacelles
        for nac in [self.nacelle_name1,self.nacelle_name2]:
            log.info('Saving grid data in '+self.grid_data_file[nac])
            mask = self.grids['nac_name']==nac
            if not len(mask):
                msg = '*** FATAL ERROR: there is no grid data for nacelle '+nac
                log.error(msg)
                raise ValueError(msg)
            self.grids[mask].write(self.grid_data_file[nac],format='ascii.fixed_width_two_line',overwrite=True)
        return    
    
    def load_grids(self):
        """Load grids data into self.grids variable.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        # initializations
        self.grids = None
        # loop over nacelles
        for nac in [self.nacelle_name1,self.nacelle_name2]:
            grids_file = self.grid_data_file[nac]
            log.info('Readgin grids data file: '+grids_file)
            grids = QTable.read(grids_file,format='ascii.fixed_width_two_line')
            if self.grids is None:
                self.grids = grids
            else:
                self.grids = vstack((self.grids,grids))
        # set units
        obsutils.set_grid_units(self.grids)
        return
        
    def make_azel_mesh(self,nacelle_name,step_grid=1.0*u.deg):
        """Make azimuth/elevation mesh for given nacelle.
        
        Parameters
        ----------
        nacelle_name : string
            Nacelle name.
        step_grid : astropy.units.Quantity object, optional.
            Grid angular step. Default is 1.0*u.deg.
        
        Returns
        -------
        az_all : 2-array of float.
            Azimuth angles.
        el_all : 2-array of float.
            Elevation angles.
        vec_x : 2-array of float.
            First cartesian coordinate.
        vec_y : 2-array of float.
            Second cartesian coordinate.
        vec_z : 2-array of float.
            Third cartesian coordinate.
        
        """
        # get fov features
        camera_id = make_camera_id(sp.bodn2c(nacelle_name))
        # retrieve fov from nacelle name
        (fov_shape,fov_frame,brst_vec,n_crn,crn_vec) = sp.getfov(camera_id,4)
        # compute half-azel extent
        (r,az_half,el_half) = sp.reclat(crn_vec[0])
        az_half = az_half * u.rad
        el_half = el_half * u.rad
        # compute start/end azimuth/elevation for each cell center
        az_beg = -az_half + step_grid.to('rad')/2.0
        az_end =  az_half - step_grid.to('rad')/2.0
        el_beg = -el_half + step_grid.to('rad')/2.0
        el_end =  el_half - step_grid.to('rad')/2.0
        # compute number of points for azimuth/elevation
        az_npnt = int(np.abs(az_end-az_beg).to('deg')/step_grid.to('deg')) + 1
        el_npnt = int(np.abs(el_end-el_beg).to('deg')/step_grid.to('deg')) + 1
        # make all azimuth and elevations
        az_all = np.linspace(az_beg.to('deg'),az_end.to('deg'),num=az_npnt)
        el_all = np.linspace(el_beg.to('deg'),el_end.to('deg'),num=el_npnt)        
        # make grid vectors
        azz,ell = np.meshgrid(az_all,el_all)
        # make cartesian vectors
        (vecs_x,vecs_y,vecs_z) = spherical_to_cartesian(np.repeat(1.0,len(azz.flatten())),
                                            ell.to('rad').flatten(),
                                            azz.to('rad').flatten())
        shp = (azz + ell).shape
        vecs_x = np.reshape(vecs_x,shp)
        vecs_y = np.reshape(vecs_y,shp)
        vecs_z = np.reshape(vecs_z,shp)
        
        return (azz,ell,vecs_x,vecs_y,vecs_z)
    
    def make_grid(self,nacelle_name,et,step_grid=1.0*u.deg):
        """Make a grid of the EarthLocations in fov at top of atmosphere.
        
        Parameters
        ----------
        nacelle_name : string
            Nacelle name.
        et : float
            Time in SPICE format (sec after J2000).
        step_grid : astropy.units.Quantity object, optional.
            Grid angular step. Default is 1.0*u.deg.
        
        Returns
        -------
        grid_data : astropy.table.QTable object.
            QTable containing grid data.
        redarea_data : astropy.table.QTable object.
            QTable containing reduced area data.
        area_total : astropy.units.Quantity object.
            Total surveyed area, without taking into account corrections.
        area_reduced_total : astropy.units.Quantity object
            Total reduced area.
        """
        # convert et to Time object
        time = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
        log.debug('Making grid for '+nacelle_name+' at '+time.isot)
        # retrieve nacelle position
        nac_eloc = self.get_nacelle_position(nacelle_name,time,silent=True)
        # get camera frame name
        camera_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
        # get nacelle topographic-like frame
        topo_frame = make_topoframe_name(nacelle_name)
        # make grid azimuth/elevation and cartessian vectors
        (azz,ell,vecs_x,vecs_y,vecs_z) = self.make_azel_mesh(nacelle_name,step_grid=step_grid)
        
        # create grid QTable at time 
        grid_data = None
        redarea_data = obsutils.make_reduced_area_data_qtable()
        # loop over all FOV azimuth/elevation vectors
        for (az,el,x,y,z) in zip(azz.flatten(),ell.flatten(),vecs_x.flatten(),vecs_y.flatten(),vecs_z.flatten()):
            vec = np.array([x,y,z])
            # get frame rotation between 'ITRF93' and nacelle local topographic-like frame ('TOPOLIKE')
            pointing_mtx = sp.pxform('ITRF93',topo_frame,et)
            # set Lgnd to True by default
            Lgnd = True
            # compute intersection with Earth surface
            try:
                loc_gnd,lt,vecgnd = sp.sincpt('ELLIPSOID','EARTH',et,'ITRF93','NONE',nacelle_name,camera_frame_name,vec)
                #loc_gnd,lt,vecgnd = sp.sincpt('ELLIPSOID','EARTH',et,topo_frame,'NONE',nacelle_name,camera_frame_name,vec)
                log.warning('Earth surface in FOV for instrment az,el= '+f"{az.to('deg'):0.01f}"+' '+f"{el.to('deg'):0.01f}")
                # rotate nac2loc to get it in topolike frame and later compute azimuth/elevation
                vecgnd_topo = sp.mxv(pointing_mtx,vecgnd)
                # compute distance, azimuth, elevation of cell wrt nacelle
                (gnd_dist,vecgnd_az,vecgnd_el) = sp.recrad(vecgnd_topo)
                vecgnd_az = ((-vecgnd_az)%(2.0*np.pi))*u.rad
                vecgnd_el = vecgnd_el*u.rad
                log.warning('                         topo-like: az,el= '+f"{vecgnd_az.to('deg'):0.01f}"+' '+f"{vecgnd_el.to('deg'):0.01f}")                
            except sp.utils.support_types.SpiceyError as e:
                notfoundstr = 'Spice returns not found for function: sincpt'
                if notfoundstr in str(e):
                    Lgnd = False
            except Exception as e:
                log.error(str(e))
                raise e
            # compute intersection of cell boresight with top of atmosphere
            loc_top,lt,nac2loc = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',nacelle_name,camera_frame_name,vec)
            # rotate nac2loc to get it in topolike frame and later compute azimuth/elevation
            nac2loc_topo = sp.mxv(pointing_mtx,nac2loc)
            # compute distance, azimuth, elevation of cell wrt nacelle
            (loc_dist,loc_az,loc_el) = sp.recrad(nac2loc_topo)
            loc_dist = loc_dist *u.km
            loc_az = ((-loc_az)%(2.0*np.pi))*u.rad
            loc_el = loc_el*u.rad            
            
            # convert location at top of atmosphere into EarthLocation object
            eloc_top = EarthLocation.from_geocentric(loc_top[0],loc_top[1],loc_top[2],unit='km')
            # create vectors for the 4 corners of the cell
            crn_vec = self.get4corners(az,el,step_grid.to('rad')/2.0,step_grid.to('rad')/2.0)
            elocs_crn = []
            # get location of 4 corners with top of atmosphere
            for crn in crn_vec:
                crn_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',nacelle_name,camera_frame_name,np.array(crn))
                eloc_crn = EarthLocation.from_geocentric(crn_top[0],crn_top[1],crn_top[2],unit='km')
                elocs_crn.append(eloc_crn)
            # get radius of the Earth at center of corner latitude
            R_earth = utils.latrad(eloc_top.lat.to('rad').value)
            # compute theoretical area covered by the fov
            area = utils.elocs42area(elocs_crn[0],elocs_crn[1],elocs_crn[2],elocs_crn[3],r=R_earth)
            # store data for given nacelle into a QTable
            grid_vec = self.make_grid_table(time,nacelle_name,eloc_top ,elocs_crn,loc_az,loc_el,loc_dist,area,Lgnd)
            # vstack grid created for vector vec and grid_data
            if grid_data is None:
                grid_data = grid_vec
            else:
                grid_data = vstack((grid_data,grid_vec))
        # compute reduced area. data are stored in self.grid_data
        redarea_data,area_total,area_reduced_total = self.compute_reduced_area(time,nacelle_name,nac_eloc,grid_data)
        return grid_data,redarea_data,area_total,area_reduced_total
    
    def compute_reduced_area(self,time,nacelle_name,nac_eloc,grid_data):
        """Compute reduced area.
        
        Parameters
        ----------
        time : astropy.timeTime object
            Time.
        nacelle_name : string
            Nacelle name.
        nac_eloc : astropy.coordinates.Earthlocation
            Nacelle location.
        grid_data : astropy.table.QTable
            Table of all grid parameters.
        
        Returns
        -------
        redarea_data : astropy.table.QTable
            Table of all grid parameters, updated with reduced area parameters.
        area_total : astropy.units.Quantity object.
            Total surveyed area, without taking into account corrections.
        area_reduced_total : astropy.units.Quantity object.
            Total reduced area.
        """        
        # create elocs for all grid cell centers
        elocs = EarthLocation.from_geodetic(grid_data['ctrn_lon'].to('deg'),
                                            grid_data['ctrn_lat'].to('deg'),
                                            height=grid_data['ctrn_alt'].to('m'))                        
        # compute air masses from nacelle to all elocs
        airmasses = shower_utils.compute_airmass_one2many(nac_eloc,elocs,time)
        # compute magnitude loss due to atmopshere absorption
        dm_atm = shower_utils.Roth1994(airmasses)
        
        # compute magnitude loss due to physical distance between camera and meteor
        dm_dist = -5.0*np.log10(100.0*u.km/grid_data['dist'].to('km'))*u.mag # dist is in [km]
        
        # compute magnitude loss due to meteor apparent velocity
        # make locs SkyCoord
        loc_altaz = SkyCoord(ra=grid_data['az'].to('deg'),dec=grid_data['el'].to('deg'))
        # retrieve nacelle obsplan number: 1 or 2
        nacelle_num = self.nacelle_name2number(nacelle_name)
        # retrieve geometric data at time time
        geom_data = obsutils.get_geom_data(time,nacelle_name,nacelle_num,self.geom_data)
        # make radiant AltAz skycoord
        rad_altaz = SkyCoord(ra=geom_data['RadAz'],dec=geom_data['RadAlt'],unit=u.deg)
        # compute angular separation between radiant and anchor, in [rad]
        locrad_sep = rad_altaz.separation(loc_altaz)
        # compute apparent angular velocity
        omega_met = (self.shower.Ventry.to('m.s-1') / 
                    grid_data['dist'].to('m') * 
                    np.sin(locrad_sep.to('rad').value) * 
                    np.sin(grid_data['el'].to('rad').value) * 
                    u.rad)
        # compute mag loss due to apparent angular velocity
        dm_appvel = phot.mag_loss_appvel(omega_met,self.camera.psf_angle,self.camera.fps)
        
        # compute total magnitude loss
        dm_total = dm_atm  + dm_appvel  + dm_dist
                
        # compute area loss factor due to total magnitude loss
        f_area_loss = self.shower.population_index**(-dm_total.value)
        # compute reduced Area surveyed by the camera
        red_area = grid_data['area'] * f_area_loss
        # compute total area and reduced area
        area_total = np.sum( grid_data['area'])
        reduced_area_total = np.sum(red_area)
        
        # put al results into a QTable
        redarea_data = obsutils.make_reduced_area_data_qtable()
        # 'airmasses','dm_atm','dm_dist','dm_appvel','dm_tot','f_area_loss','red_area'
        redarea_data = QTable([airmasses,dm_atm,dm_dist,dm_appvel,dm_total,f_area_loss,red_area],
                              names=['airmasses','dm_atm','dm_dist','dm_appvel','dm_tot','f_area_loss','red_area'])
        # set format
        redarea_data['airmasses'].info.format = '5.3f'
        redarea_data['dm_atm'].info.format = '5.3f'
        redarea_data['dm_tot'].info.format = '5.3f'
        redarea_data['dm_dist'].info.format = '5.3f'
        redarea_data['dm_atm'].info.format = '5.3f'
        redarea_data['dm_appvel'].info.format = '5.3f'
        redarea_data['red_area'].info.format = '12.1f'     
        redarea_data['f_area_loss'].info.format = '5.3f'
        
        return redarea_data,area_total,reduced_area_total    
    
    def get4corners(self,vec_az,vec_el,az_step,el_step):
        """Get the four corners around a pointing vector.
        
        Parameters
        ----------
        vec_az : astropy.units.Quantity object
            Azimuth of pointing vector.
        vec_el : astropy.units.Quantity object
            Elevation of pointing vector.
        az_step : astropy.units.Quantity object
            Azimuth step.
        el_step : astropy.units.Quantity object
            Elevation step.
        
        Returns
        -------
        crn_vec : 4x3 array of float
            vectors of the 4 corners around vec.
        
        """
        # 4 corners azimuth/elevation, in [rad]
        crn_el = [vec_el-el_step,vec_el+el_step,vec_el+el_step,vec_el-el_step]
        crn_az = [vec_az-az_step,vec_az-az_step,vec_az+az_step,vec_az+az_step]
        # 4 corners cartesian coordiantes
        (x,y,z) = spherical_to_cartesian(np.repeat(1.0,4),crn_el,crn_az)
        crn_vec = np.array([x,y,z]).transpose()
        #log.debug('crn_vec: '+str(crn_vec))
        return crn_vec
    
    def get_fov_data(self,et):
        """Get FOV data at specific time.
        
        Parameters
        ----------
        et : float
            Time in SPICE format [sec after J2000].
        
        Returns
        -------
        fov_data : astropy.table.QTable
            Subset of self.fov_data at specific time.
        
        """
        # select fov_data at time et
        mask = self.fov_data['et']==et
        fov_data = self.fov_data[mask]
        
        # if there is no data, retrieve data at closest time et
        if not len(fov_data):
            # compute time in fov_data the closest to et
            et_closest = self.fov_data['et'][np.argmin(np.abs(et - self.fov_data['et']))]
            mask = self.fov_data['et']==et_closest
            fov_data = self.fov_data[mask]
        
        # if it all fails...
        if not len(fov_data):
            msg = '*** Impossible to retrieve fov_data for time: '+str(et)+' = '+sp.et2utc(et,"ISOC",2,50)
            log.error(msg)
            raise ValueError(msg)
        
        return fov_data
    
    
    def add_grids_data(self,grid_data,redarea_data,dblfov_data):
        """Add data to grids global variable.
        
        Parameters
        ----------
        grid_data : astropy.table.QTable object.
            QTable containing grid data.
        redarea_data : astropy.table.QTable object.
            QTable containing reduced area data.
        dblfov_data : astropy.table.QTable object.
            QTable containing True where cell center is in both FOV.
        
        Returns
        -------
        None.        
        
        """
        grids = hstack((grid_data,redarea_data,dblfov_data))
        if self.grids is None:
            log.debug('Creating grids data')
            self.grids = grids
        else:
            log.debug('Appending grids')
            self.grids = vstack((self.grids,grids))        
        dim = len(self.grids['Time'])
        log.debug('grids has '+str(dim)+' lines of data')
        if not dim:
            msg = '*** ERROR: no grid_data nor reduced area data to stack'
            log.error(msg)
            raise ValueError(msg)
        return
    
    def compute_double_area(self,grid_data,redarea_data):
        """Compute double-station surveyed area.
        
        Parameters
        ----------
        grid_data : astropy.table.QTable object.
            QTable containing grid data.
        redarea_data : astropy.table.QTable object.
            QTable containing reduced area data.
        
        Returns
        -------
        dblfov_data : astropy.table.QTable object.
            QTable of booleans: True where cell center is in other camera FOV.
        """
        # retrieve time
        time = Time(grid_data['Time'][0],format='isot',scale='utc')
        # compute et
        et = sp.str2et(time.isot)
        # get primary nacelle name
        nac_primary = grid_data['nac_name'][0]
        # get secondary nacelle name
        if nac_primary==self.nacelle_name1:
            nac_secondary = self.nacelle_name2
        else:
            nac_secondary = self.nacelle_name1
        # retrieve secondary camera name
        cam_secondary = make_camera_name(nac_secondary)
        # get secondary nacelle location in ITRF93
        eloc_secondary = self.get_nacelle_position(nac_secondary,time,silent=True)
        # retrieve cell center and make EarthLocation
        elocs = EarthLocation.from_geodetic(grid_data['ctrn_lon'].to('deg'),grid_data['ctrn_lat'].to('deg'),height=grid_data['ctrn_alt'].to('m'))
        # make ray vector from secondary nacelle to cell center
        raydir = np.array([(elocs.x - eloc_secondary.x).to('km').value,
                           (elocs.y - eloc_secondary.y).to('km').value,
                           (elocs.z - eloc_secondary.z).to('km').value]).transpose()
        # make output table
        dblfov_data = QTable(names=['dbl_fov'],dtype=['bool'])
        # get total number of processors
        num_cores = multiprocessing.cpu_count()
        # create Pool to parallelize the computation of the anchors in FOV for given camera
        with multiprocessing.Pool(num_cores) as pool:
            dblfov_data['dbl_fov'] = pool.starmap(sp.fovray,zip(repeat(cam_secondary),raydir,repeat('ITRF93'),repeat('NONE'),repeat(nac_secondary),repeat(et)))
        
        # compute total double_station area and reduced area
        mask = dblfov_data['dbl_fov']==True
        sub_grid_data = grid_data[mask]
        sub_redarea_data = redarea_data[mask]
        dim = len(sub_grid_data)
        if not dim:
            area_dbl_total =0.0*u.km**2
            red_area_dbl_total =0.0*u.km**2
        else:
            area_dbl_total = np.sum(sub_grid_data['area'])
            red_area_dbl_total = np.sum(sub_redarea_data['red_area'])
        
        return dblfov_data,area_dbl_total,red_area_dbl_total
    
    
    def make_kmls(self):
        """Make FOV kmls.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # save FOV into one kml file
        self.fov2kml()
        # save grid data into several kml files
        self.grid2kml()
        return
    
    def fov2kml(self):
        """Save FOV data in kml file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # create FOV KML object
        fov_kml = simplekml.Kml()
        # set fov kml file name
        self.fov_kml_file = self.config['CAMPAIGN']['fov_kml_file']
        # loop over all fov data
        for fov in self.fov_data:
            nac = fov['nacelle_name']
            num = str(self.nacelle_name2number(nac))
            fov_name = 'FOV'+num
            bst_name = 'BRS'+num
            time = Time(fov['Time'],format='isot',scale='utc')
            nac_eloc = self.get_nacelle_position(nac,time,silent=True)
            
            # make a yellow line from nacelle to boresight at meteor height
            line_brs = fov_kml.newlinestring(name=bst_name)
            line_brs.coords = [(nac_eloc.lon.to('deg').value,nac_eloc.lat.to('deg').value,nac_eloc.height.to('m').value),
                               (fov['brsght_lon'].to('deg').value,fov['brsght_lat'].to('deg').value,fov['brsght_alt'].to('m').value)]
            line_brs.extrude = 0
            line_brs.altitudemode = simplekml.AltitudeMode.absolute
            line_brs.style.linestyle.width = 5
            line_brs.style.linestyle.color = simplekml.Color.yellow
            line_brs.timestamp.when = time.isot
            
            # make a blue line from nacelle to each corner
            for crn_num in np.arange(4):
                crn_ns = str(crn_num+1)
                line_crn = fov_kml.newlinestring(name=fov_name)
                line_crn.coords = [(nac_eloc.lon.to('deg').value,nac_eloc.lat.to('deg').value,nac_eloc.height.to('m').value),
                                   (fov['crn'+crn_ns+'_lon'].to('deg').value,fov['crn'+crn_ns+'_lat'].to('deg').value,fov['crn'+crn_ns+'_alt'].to('m').value)]            
                line_crn.extrude = 0
                line_crn.altitudemode = simplekml.AltitudeMode.absolute
                line_crn.style.linestyle.width = 5
                line_crn.style.linestyle.color = simplekml.Color.blue
                line_crn.timestamp.when = time.isot
            
            # make a polygon of the surveyed area at meteor height
            pol = fov_kml.newpolygon(name='Surveyed area'+num)
            coo_pol = []
            for crn_num in np.arange(4):
                crn_ns = str(crn_num+1)
                coo_pol.append((fov['crn'+crn_ns+'_lon'].to('deg').value,fov['crn'+crn_ns+'_lat'].to('deg').value,fov['crn'+crn_ns+'_alt'].to('m').value))
            # add first point to close the polygon
            coo_pol.append((fov['crn1_lon'].to('deg').value,fov['crn1_lat'].to('deg').value,fov['crn1_alt'].to('m').value))
            pol.outerboundaryis = coo_pol
            pol.extrude = 0
            pol.altitudemode = simplekml.AltitudeMode.absolute
            pol.style.linestyle.width = 5
            pol.style.linestyle.color = simplekml.Color.white
            pol.style.polystyle.color = simplekml.Color.changealphaint(0, simplekml.Color.white)
            pol.timestamp.when = time.isot
 
        fov_kml.save(self.fov_kml_file)
        log.info('FOV saved in KML file '+self.fov_kml_file)
        return
    
    def grid2kml(self):
        """Save grids data into KML format.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        log.debug('Converting grid into kml format')
        # make each nacelle grid kml file name
        bsn,ext = os.path.splitext(os.path.basename(self.fov_kml_file))
        (self.single_double_kml_file,self.area_kml_file,
         self.red_kml_file,self.dm_atm_kml_file,self.airmass_kml_file,
         self.dm_dist_kml_file,self.dm_appvel_kml_file,
         self.dm_tot_kml_file,self.f_area_loss_kml_file
         ) = ({},{},{},{},{},{},{},{},{})        
        for nacelle_name in self.list_nacelle_names:
            self.single_double_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_grid')
            self.area_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_area')
            self.red_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_red')
            self.airmass_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_airmass')
            self.dm_atm_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_dm_atm')
            self.dm_dist_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_dm_dist')
            self.dm_appvel_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_dm_appvel')
            self.dm_tot_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_dm_tot')
            self.f_area_loss_kml_file[nacelle_name] = self.fov_kml_file.replace(bsn,nacelle_name+'_f_area_loss')
        # set color for single and double station cell
        color_double = simplekml.Color.yellow
        color_single = simplekml.Color.blue
        color_ground = simplekml.Color.black
        # set color for quartiles
        color_first = simplekml.Color.red
        color_second = simplekml.Color.blue
        color_third = simplekml.Color.green
        # set alphaint
        alphaint_atm = 100
        alphaint_gnd = 255
        
        # loop over the nacelles
        for (nac_name,num) in zip(self.list_nacelle_names,self.list_nacelle_numstr):
            # create grid KML object
            single_double_kml = simplekml.Kml()
            area_kml = simplekml.Kml()
            red_kml = simplekml.Kml()
            airmass_kml = simplekml.Kml()
            dm_atm_kml = simplekml.Kml()
            dm_dist_kml = simplekml.Kml()
            dm_appvel_kml = simplekml.Kml()
            dm_tot_kml = simplekml.Kml()
            f_area_loss_kml = simplekml.Kml()
            # select grid data for given nacelle
            mask = self.grids['nac_name']==nac_name
            grid_data = self.grids[mask]
            # retrieve min/max of area and readuced area
            (area_1,area_2) = obsutils.compute_12third(grid_data['area'],unit=u.km**2)
            (red_1,red_2) = obsutils.compute_12third(grid_data['red_area'],unit=u.km**2)
            (airmass_1,airmass_2) = obsutils.compute_12third(grid_data['airmasses'])
            (dm_atm_1,dm_atm_2) = obsutils.compute_12third(grid_data['dm_atm'],unit=u.mag)
            (dm_dist_1,dm_dist_2) = obsutils.compute_12third(grid_data['dm_dist'],unit=u.mag)
            (dm_appvel_1,dm_appvel_2) = obsutils.compute_12third(grid_data['dm_appvel'],unit=u.mag)
            (dm_tot_1,dm_tot_2) = obsutils.compute_12third(grid_data['dm_tot'],unit=u.mag)
            (f_area_loss_1,f_area_loss_2) = obsutils.compute_12third(grid_data['f_area_loss'])
            # loop over grid data
            for grid in grid_data:
                time = Time(grid['Time'],format='isot',scale='utc')
                # make a new polygon
                pol_grd = single_double_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_ara = area_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_red = red_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_airmass = airmass_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_atm = dm_atm_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_dm_dist = dm_dist_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_dm_appvel = dm_appvel_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_dm_tot = dm_tot_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_f_area_loss = f_area_loss_kml.newpolygon(name='Cell'+num) # TO CHANGE LATER: add cell id
                pol_coo = []
                for crn_num in np.arange(4):
                    crn_ns = str(crn_num+1)
                    pol_coo.append((grid['crn'+crn_ns+'_lon'].to('deg').value,grid['crn'+crn_ns+'_lat'].to('deg').value,grid['crn'+crn_ns+'_alt'].to('m').value))
                # add first corner to close the polygon
                pol_coo.append((grid['crn1_lon'].to('deg').value,grid['crn1_lat'].to('deg').value,grid['crn1_alt'].to('m').value))
                for pol in [pol_grd,pol_ara,pol_red,pol_atm,pol_airmass,pol_dm_dist,
                            pol_dm_appvel,pol_dm_tot,pol_f_area_loss]:
                    pol.timestamp.when = time.isot
                    pol.outerboundaryis = pol_coo
                    pol.extrude = 0
                    pol.altitudemode = simplekml.AltitudeMode.absolute
                    pol.style.linestyle.width = 2
                # set alphaint
                alphaint = alphaint_atm
                # set color for single/double fov grid
                if grid['dbl_fov']:
                    color = color_double
                else:
                    color = color_single
                if grid['Lgrnd']:
                    color = color_ground
                    alphaint = alphaint_gnd
                pol_grd.style.linestyle.color = color
                pol_grd.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for area
                diff = grid['area'].to('km2').value - np.array([area_1,area_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_ara.style.linestyle.color = color
                pol_ara.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for reduced area
                diff = grid['red_area'].to('km2').value - np.array([red_1,red_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_red.style.linestyle.color = color
                pol_red.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for dm_atm
                diff = grid['dm_atm'].to('mag').value - np.array([dm_atm_1,dm_atm_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_atm.style.linestyle.color = color
                pol_atm.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for airmass
                diff = grid['airmasses'].value - np.array([airmass_1,airmass_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_airmass.style.linestyle.color = color
                pol_airmass.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for dm_dist
                diff = grid['dm_dist'].to('mag').value - np.array([dm_dist_1,dm_dist_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_dm_dist.style.linestyle.color = color
                pol_dm_dist.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for dm_appvel
                diff = grid['dm_appvel'].to('mag').value - np.array([dm_appvel_1,dm_appvel_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_dm_appvel.style.linestyle.color = color
                pol_dm_appvel.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for dm_tot
                diff = grid['dm_tot'].to('mag').value - np.array([dm_tot_1,dm_tot_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_dm_tot.style.linestyle.color = color
                pol_dm_tot.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                # set color for f_area_loss
                diff = grid['f_area_loss'] - np.array([f_area_loss_1,f_area_loss_2])
                color = color_second
                if np.all(diff<0):
                    color = color_third
                if np.all(diff>0):
                    color = color_first
                if grid['Lgrnd']:
                    color = color_ground
                pol_f_area_loss.style.linestyle.color = color
                pol_f_area_loss.style.polystyle.color = simplekml.Color.changealphaint(alphaint, color)
                
            # save in nacelle grid file
            single_double_kml.save(self.single_double_kml_file[nac_name])
            log.info('Grid saved in kml file '+self.single_double_kml_file[nac_name])
            area_kml.save(self.area_kml_file[nac_name])
            log.info('Area saved in kml file '+self.area_kml_file[nac_name])
            red_kml.save(self.red_kml_file[nac_name])
            log.info('Reduced area saved in kml file '+self.red_kml_file[nac_name])
            dm_atm_kml.save(self.dm_atm_kml_file[nac_name])
            log.info('Atmospheric extinction factor saved in kml file '+self.dm_atm_kml_file[nac_name])
            
            airmass_kml.save(self.airmass_kml_file[nac_name])
            log.info('Airmass saved in kml file '+self.airmass_kml_file[nac_name])
            dm_dist_kml.save(self.dm_atm_kml_file[nac_name])
            log.info('Distance extinction saved in kml file '+self.dm_dist_kml_file[nac_name])
            dm_appvel_kml.save(self.dm_appvel_kml_file[nac_name])
            log.info('Apparent velocity extinction saved in kml file '+self.dm_appvel_kml_file[nac_name])
            dm_tot_kml.save(self.dm_tot_kml_file[nac_name])
            log.info('Total extinction saved in kml file '+self.dm_tot_kml_file[nac_name])
            f_area_loss_kml.save(self.f_area_loss_kml_file[nac_name])
            log.info('Area loss factor saved in kml file '+self.f_area_loss_kml_file[nac_name])
        return
    
    
    
    def make_new_fov_kml_lines(self):
        """Make new lines in FOV kml data.
        
        Parameters
        ----------
        None.
        
        Parameters
        ----------
        None.
        """
        (fov_line1,fov_line2) = (self.fov_kml.newlinestring(name='FOV1'),
                                 self.fov_kml.newlinestring(name='FOV2'))
        return (fov_line1,fov_line2)
    
    def make_new_bst_kml_lines(self):
        """Make new lines in FOV kml data.
        
        Parameters
        ----------
        None.
        
        Parameters
        ----------
        None.
        """
        (bst_line1,bst_line2) = (self.fov_kml.newlinestring(name='BSGT1'),
                                 self.fov_kml.newlinestring(name='BSGT2'))
        return (bst_line1,bst_line2)   
    
    def add_fov_to_kml(self,time,ls,coords_ls,lb,coords_lb,coords_lo):
        """Add FOV data into kml.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for each kml component to add.
        ls : simplekml.Kml object.
            Line from nacelle to each fov corner at atmosphere top, in kml format.
        coords_ls : array of float
            Coordinates of line to add.
        lb : simplekml.Kml object.
            Line from nacelle to boresight at atmosphere top, in kml format.
        coords_lo : array of float
            Coordinates of lines connecting corners of fov at atmosphere top to add.
        
        Returns
        -------
        None.
        
        """
        # remove boresight from coords_lo and coords_ls
        coords_lo = coords_lo[:-1]
        coords_ls = coords_ls[:-2]
        # add 1st point to fov to close the 4-corners figure of FOV at meteor height
        coords_lo.append(coords_lo[0])
        # connect corners of fov at top of atmosphere with a white line.
        lo = self.fov_kml.newlinestring(name='meteor_height')
        lo.coords = coords_lo
        lo.extrude = 0
        lo.altitudemode = simplekml.AltitudeMode.absolute
        lo.style.linestyle.width = 5
        lo.style.linestyle.color = simplekml.Color.white
        lo.timestamp.when = time.isot
        # connect nacelle to each corner of fov with a blue line
        ls.coords = coords_ls
        ls.extrude = 0
        ls.altitudemode = simplekml.AltitudeMode.absolute
        ls.style.linestyle.width = 5
        ls.style.linestyle.color = simplekml.Color.blue
        ls.timestamp.when = time.isot
        # connect nacelle to boresight vector with a blue yellow
        lb.coords = coords_lb
        lb.extrude = 0
        lb.altitudemode = simplekml.AltitudeMode.absolute
        lb.style.linestyle.width = 5
        lb.style.linestyle.color = simplekml.Color.yellow
        lb.timestamp.when = time.isot        
        return
    
    def coords_kml_append(self,eloc,bound_top_eloc,coords_ls,coords_lo=None):
        """Append location to coordinates arrays.
        
        Parameters
        ----------
        eloc : astrop.coordinates.EarthLocation object.
            Location of nacelle.
        bound_top_eloc : astrop.coordinates.EarthLocation object.
            Location of boresight vector at atmosphere top.
        coords_ls : array of float.
            coordinates of corners at top of atmosphere to connect to each other.
            If set, bound_top_eloc is appended
        coords_lo : array of float, optional.
            coordinates of corners at top of atmosphere to connect to nacelle.
            If set, bound_top_eloc is appended
        """
        # add coordinates into kml coordinates: from nacelle to each bound
        coords_ls.append((          eloc.lon.to('deg').value,          eloc.lat.to('deg').value,          eloc.height.to('m').value))
        coords_ls.append((bound_top_eloc.lon.to('deg').value,bound_top_eloc.lat.to('deg').value,bound_top_eloc.height.to('m').value))
        if (coords_lo is not None):
            coords_lo.append((bound_top_eloc.lon.to('deg').value,bound_top_eloc.lat.to('deg').value,bound_top_eloc.height.to('m').value))
        return    
    
    
    def create_grid_kml(self):
        """Create grid kml objects.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.                
        """
        # KML object for nacelle1 and 2
        self.grid_kml = {self.nacelle_name1 : simplekml.Kml(),
                         self.nacelle_name2 : simplekml.Kml()}
        # make grid output kml file names
        bsn,ext = os.path.splitext(os.path.basename(self.fov_kml_file))
        self.grid_kml_file = {self.nacelle_name1 : self.fov_kml_file.replace(bsn,self.nacelle_name1+'_grid'),
                              self.nacelle_name2 : self.fov_kml_file.replace(bsn,self.nacelle_name2+'_grid')}
        return
    
    def add_grid_kml_polygon(self,time,name,elocs,grid_kml,color='blue'):
        """Add polygon to grid kml object.
        
        Parameters
        ----------
        time : astropy.time.Time object.
            Time of each corner to appear.
        name : string
            Name of polygon.
        elocs : astropy.coordinates.EarthLocation object.
            Location of 4 corners of polygon.
        grid_kml : simplekml.Kml object
            Kml object to be appended with polygon.
        color : string, optional
            color of the polygon. Choice is 'blue', 'red', 'green', 'black'.
            Default is 'blue'.
        """
        # define color dict
        colors = {'blue': simplekml.Color.blue,
                  'red' : simplekml.Color.red,
                  'green' : simplekml.Color.green,
                  'black' : simplekml.Color.black}
        # make kmlpolygon
        pol = grid_kml.newpolygon(name=name)
        # put polygon coordinates
        pol.outerboundaryis = [
                (elocs[0].lon.to('deg').value,elocs[0].lat.to('deg').value,elocs[0].height.to('m').value),
                (elocs[1].lon.to('deg').value,elocs[1].lat.to('deg').value,elocs[1].height.to('m').value),
                (elocs[2].lon.to('deg').value,elocs[2].lat.to('deg').value,elocs[2].height.to('m').value),
                (elocs[3].lon.to('deg').value,elocs[3].lat.to('deg').value,elocs[3].height.to('m').value)]
        pol.style.linestyle.width = 2
        pol.style.linestyle.color = colors[color]
        pol.style.polystyle.color = simplekml.Color.changealphaint(100, colors[color])
        pol.altitudemode = simplekml.AltitudeMode.absolute
        pol.extrude = 0
        pol.timestamp.when = time.isot
        return
    
    def save_grid_kml(self):
        """Save grid data in kml file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        for nac in [self.nacelle_name1,self.nacelle_name2]:
            log.info('Saving grid kml in '+self.grid_kml_file[nac])
            self.grid_kml[nac].save(self.grid_kml_file[nac])
        return    
    
    
    
    
    
    
    
    def get_fov_center_maxrad(self,et,verbose=False):
        """Compute center of FOV and FOV max physical distance of all corners.
        
        Parameters
        ----------
        et : float
            Time in SPICE format [sec after J2000].
        verbose : boolean
            If True, log is performed. Default is False.
        
        Returns
        -------
        fov_center_eloc : astropy.coordinates.EarthLocation Object.
            Location of the middle point of center of the FOV of both cameras.
        max_phy_dist : astropy.unitsQuantity object
            Maximum physical distance between center of FOV and all FOV corners.
        
        """
        # get FOV data at time et
        fov_data = self.get_fov_data(et)
        # get middle point of fov in atmosphere (NOT boresight: at low elevation angle boresight is not at center of area)
        crn11_eloc = EarthLocation.from_geodetic(lon=fov_data[0]['crn1_lon'],lat=fov_data[0]['crn1_lat'],height=fov_data[0]['crn1_alt'])
        crn13_eloc = EarthLocation.from_geodetic(lon=fov_data[0]['crn1_lon'],lat=fov_data[0]['crn1_lat'],height=fov_data[0]['crn1_alt'])
        crn21_eloc = EarthLocation.from_geodetic(lon=fov_data[1]['crn1_lon'],lat=fov_data[1]['crn1_lat'],height=fov_data[1]['crn1_alt'])
        crn23_eloc = EarthLocation.from_geodetic(lon=fov_data[1]['crn1_lon'],lat=fov_data[1]['crn1_lat'],height=fov_data[1]['crn1_alt'])
        mid1_eloc = utils.elocs2mid(crn11_eloc,crn13_eloc)
        mid2_eloc = utils.elocs2mid(crn21_eloc,crn23_eloc)
        fov_center_eloc = utils.elocs2mid(mid1_eloc,mid2_eloc)
        
        """
        # OLD METHOD: based on boresight
        # compute middle of center of fov for both cameras
        brs1_eloc = EarthLocation.from_geodetic(fov_data[0]['brsght_lon'],fov_data[0]['brsght_lat'],height=fov_data[0]['brsght_alt'])
        brs2_eloc = EarthLocation.from_geodetic(fov_data[1]['brsght_lon'],fov_data[1]['brsght_lat'],height=fov_data[1]['brsght_alt'])
        phyD,az = utils.distaz(brs1_eloc,brs2_eloc)
        fov_center_gtcirc = great_circle(distance=phyD.to('km').value/2, azimuth=az.to('deg').value, latitude=brs1_eloc.lat.to('deg').value, longitude=brs1_eloc.lon.to('deg').value)
        fov_center_eloc = EarthLocation.from_geodetic(fov_center_gtcirc['longitude']*u.deg,
                                                      fov_center_gtcirc['latitude']*u.deg,
                                                      height=self.meteor_alt)
        """
        # compute distance between center of fov and each of the 4 corners of the 2 camera FOV
        max_phyD = -1.0*u.m
        for drow in fov_data:
            for cid in range(4):
                eloc = EarthLocation.from_geodetic(drow['crn'+str(cid+1)+'_lon'],
                                                   drow['crn'+str(cid+1)+'_lat'],
                                            height=drow['crn'+str(cid+1)+'_alt'])
                phyD,az = utils.distaz(fov_center_eloc,eloc)
                if phyD > max_phyD:
                    max_phyD = phyD
        
        if verbose:
            log.debug('phyD: '+str(phyD.to('km')))
            log.debug('az: '+str(az))
            log.debug('fov_center_eloc: '+str(fov_center_eloc.to_geodetic()))
            log.debug('max_phyD: '+str(max_phyD.to('km')))
        
        return (fov_center_eloc,max_phyD)
    
    def compute_numet(self):
        """Compute all expected number of meteors for the 2 nacelles.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        # create output data
        numet_data,numet_tot = obsutils.create_numet_tables()        
        # compute ZHR as a function of time
        times = Time(self.geom_data['Time'],scale='utc')
        ZHR = self.shower.get_ZHR(times)
        
        # loop over nacelle names
        for (nacelle_name,num) in zip(self.list_nacelle_names,self.list_nacelle_numstr):
            # select data
            mask = self.area['camera']==nacelle_name
            area = self.area[mask]
            # compute HR given radiant elevation (=camera position dependent) and camera limiting magnitude
            # note that the meteor altitude correction factor is computed with the shower_utils.HR2numet routine
            # so it is NOT included it here or you'll take twice this effect into account.
            (HR,dmag_alt,f_radel,f_LM,LM_eff) = shower_utils.ZHR2HR(ZHR,self.shower.population_index,self.geom_data['RadAlt'+num],self.lim_mag)
            
            # compute expected number of meteors in both FOV, as seen from nacelle nacelle_name
            # note: do NOT change or set the h_meteor keyword, since the difference of altitude
            # is already taken into account in the computation of the Ared parameter
            (numet_sgl,f_area_sgl,f_time) = shower_utils.HR2numet(HR,area['reduced_single_area'],self.freq.to('hour'))
            # compute for double-station
            (numet_dbl,f_area_dbl,f_time) = shower_utils.HR2numet(HR,area['reduced_double_area'],self.freq.to('hour'))
            
            """
            log.debug('ZHR = '+str(ZHR))
            log.debug('HR = '+str(HR))
            log.debug('geom_data[RadAlt+num] = '+str(self.geom_data['RadAlt'+num]))
            log.debug('f_radel = '+str(f_radel))
            log.debug('r = '+str(self.shower.population_index))
            log.debug('self.lim_mag = '+str(self.lim_mag))
            log.debug('dmag_alt = '+str(dmag_alt))
            log.debug('LM_eff = '+str(LM_eff))
            log.debug('f_LM = '+str(f_LM))
            log.debug('freq [hr] = '+str(self.freq.to('hour')))
            log.debug('f_time = '+str(f_time))
            log.debug('f_area_sgl = '+str(f_area_sgl))
            log.debug('numet_sgl = '+str(numet_sgl))
            log.debug('f_area_dbl = '+str(f_area_dbl))
            log.debug('numet_dbl = '+str(numet_dbl))
            """
            
            # put data into QTable
            dim = len(times)
            tmp_tab = QTable([times,self.geom_data['et'],np.repeat(nacelle_name,dim),
                              ZHR,HR,
                              f_radel,np.repeat(f_LM,dim),np.repeat(f_time,dim),
                              f_area_sgl,f_area_dbl,numet_sgl,numet_dbl],names=numet_data.colnames)
            if len(numet_data):
                numet_data = vstack((numet_data,tmp_tab))
            else:
                numet_data = tmp_tab
            # compute total expected number of meteors
            num_tot_sgl = np.sum(tmp_tab['numet_sgl'])
            num_tot_dbl = np.sum(tmp_tab['numet_dbl'])
            numet_tot.add_row([nacelle_name,num_tot_sgl,num_tot_dbl])
        
        # save numet_data
        numet_data.sort('et')
        obsutils.set_numet_format(numet_data)
        numet_data.write(self.numet_file,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('Expected number of observed meteors saved in '+self.numet_file)
            
        # save total expected number of meteors in output file
        numet_tot.write(self.numet_tot_file,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('Total expected number of meteors saved in '+self.numet_tot_file)
        
        return
    



        
    def make_anchor_eloc(self,nclone,fov_center_eloc,fov_center_radius):
        """Make anchor EarthLocation.
        
        Parameters
        ----------
        nclone : integer
            Number of anchors clones to make.
        fov_center_eloc : astropy.coordinates.EarthLocation Object.
            Location of the middle point of center of the FOV of both cameras.
        fov_center_radius : astropy.unitsQuantity object
            Maximum physical distance between center of FOV and all FOV corners.
        
        Returns
        -------
        anchors_eloc : astropy.coordinates.EarthLocation object.
            Location of anchors in ITRF93.
        
        """
        log.debug('Make '+str(nclone)+' anchor elocs')
        fov_center_dist = np.sqrt(np.random.random(nclone)) * fov_center_radius
        fov_center_azmt = np.random.random(nclone) * 360.0*u.deg
        fov_center_gtcirc = great_circle(distance=fov_center_dist.to('m').value/2,
                                         azimuth=fov_center_azmt.to('deg').value,
                                         latitude=fov_center_eloc.lat.to('deg').value,
                                         longitude=fov_center_eloc.lon.to('deg').value)
        # make anchor eloc
        anchors_eloc = EarthLocation.from_geodetic(fov_center_gtcirc['longitude']*u.deg,
                                                   fov_center_gtcirc['latitude']*u.deg,
                                                   height=np.repeat(self.meteor_alt,nclone))
        
        # reorganise if there is only 1 element, to avoid returning an array of EarthLocation
        if nclone==1:
            anchors_eloc = anchors_eloc[0]
        return anchors_eloc
    
    def eloc_infov(self,nacelle_name,et,obj_eloc):
        """Check if a location is in FOV of a given nacelle camera.
        
        Parameters
        ----------
        nacelle_name : string
            MALBEC nacelle name.
        et : float
            SPICE time [sec after J2000].
        obj_eloc : astropy.coordinates.EarthLocation Object.
            Location of object to check in FOV.
        
        Returns
        -------
        in_fov : boolean
            True if in nacelle camera FOV. False otherwise.
        dist : astropy.units.Quantity object
            Physical distance between object and nacelle.
        
        """
        # retrieve camera name
        camera_name = make_camera_name(nacelle_name)
        # retrieve location of nacelle in ITRF93
        (eph, lt) = sp.spkezr(nacelle_name,et,'ITRF93','NONE','EARTH')
        # convert nacelle ephemeris into EarthLocation
        nac_eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
        # construct a vector from object to nacelle
        raydir = np.array([(obj_eloc.x - nac_eloc.x).to('km').value,
                           (obj_eloc.y - nac_eloc.y).to('km').value,
                           (obj_eloc.z - nac_eloc.z).to('km').value])
        # distance between nacelle and object
        dist = sp.vnorm(raydir) * u.km
        # check if object is in FOV
        infov = sp.fovray(camera_name, raydir, 'ITRF93', 'NONE', nacelle_name, et)
        return (infov,dist)
    
    
    def make_anchors_all_table(self):
        """Creates the global anchors_all QTable and set units and formats.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # make QTable
        self.anchors_all = QTable(names=['Time','et','lon','lat','alt','color'],
                                dtype=['object','float','float','float','float','object'])
        self.anchors_all['lon'].unit = u.deg
        self.anchors_all['lat'].unit = u.deg
        self.anchors_all['alt'].unit = u.m
        self.anchors_all['Time'].info.format = '%23s'
        self.anchors_all['et'].info.format  = '13.3f'
        self.anchors_all['lon'].info.format = '9.5f'
        self.anchors_all['lat'].info.format = '9.5f'
        self.anchors_all['alt'].info.format = '8.2f'
        self.anchors_all['color'].info.format = '%2s'
        return
        
    def store_anchors(self,et,anchors_eloc):
        """Store anchors EarthLocations into anchors_all global variable.
        
        Parameters
        ----------
        et : float
            SPICE time [sec after J2000].
        anchors_eloc : astropy.coordinates.EarthLocation Object.
            Location of anchors to store in self.anchors_all.
        
        Returns
        -------
        anchors_infov : astropy.table.QTable object.
            QTable of added anchors.
        
        """
        nclone = len(anchors_eloc)
        ets = np.repeat(et,nclone)
        # create local anchors QTable
        anchors_infov = QTable()
        anchors_infov['Time'] = Time(sp.et2utc(ets,"ISOC",2,50),scale='utc')
        anchors_infov['et'] = ets
        anchors_infov['lon'] = anchors_eloc.lon.to('deg')
        anchors_infov['lat'] = anchors_eloc.lat.to('deg')
        anchors_infov['alt'] = anchors_eloc.height.to('m')
        anchors_infov['color'] = np.repeat('b',nclone)
        # format anchors_infov
        anchors_infov['color'].info.format = '%2s'
        anchors_infov['Time'].info.format = '%23s'
        anchors_infov['et'].info.format  = '13.3f'
        anchors_infov['lon'].info.format = '9.5f'
        anchors_infov['lat'].info.format = '9.5f'
        anchors_infov['alt'].info.format = '8.2f'
        
        if self.anchors_all:
            self.anchors_all = vstack([self.anchors_all,anchors_infov])
        else:
            self.anchors_all = anchors_infov
        return anchors_infov
            
    def make_fakeor_times(self,nclone,et_start,et_stop):
        """Create fakeor times.
        
        Parameters
        ----------
        nclone : int
            Number of fakeor times to create.
        et_start : float
            Observation start time in SPICE format [sec after J2000].
        et_stop : float
            Observation stop time in SPICE format [sec after J2000].
        
        Returns
        -------
        data_time : astropy.table.QTable object
            Fakeor timing within observation run.
        
        """
        log.info('Making '+str(nclone)+' fakeor timings')
        num_times = 0
        names = ['Time','et']
        dtypes = ['object','float']
        data_time = QTable(names=names,dtype=dtypes)
        # setting min/max time of observability given radiant elevation
        loc_start_obs = np.where(self.geom_data['RadAlt1']>0.0*u.deg)[0]
        if len(loc_start_obs):
            et_strt_obs = self.geom_data['et'][loc_start_obs[0]]
            et_stop_obs = self.geom_data['et'][loc_start_obs[-1]]
        else:
            msg = '*** FATAL ERROR: the radiant NEVER rises given the observation constraints'
            log.error(msg)
            raise ValueError(msg)
        log.debug('et_strt_obs: '+str(et_strt_obs))
        log.debug('et_stop_obs: '+str(et_stop_obs))
        # set et_real_start
        et_real_strt = np.max([et_start,et_strt_obs])
        et_real_stop = np.min([et_stop,et_stop_obs])
        fwhm = self.shower.fwhm.to('s').value
        while num_times < nclone:
            # create random time within shower
            random_et = np.random.normal(loc=self.shower.et_max,scale=fwhm,size=nclone)
            # select time within observation run
            et_loc_start = random_et[np.where(random_et>et_real_strt)]
            n_loc_start = len(et_loc_start)
            if not n_loc_start:
                msg = '*** Warning: no time after start: please double check meteor max date and launch date.'
                log.warning(msg)
                #raise ValueError(msg)
                continue
            et_select = et_loc_start[np.where(et_loc_start<et_real_stop)]
            n_select = len(et_select)
            if not n_select:
                msg = '*** Warning: no time before stop: please double check meteor max date and launch date.'
                log.warning(msg)
                #raise ValueError(msg)
                continue
            times = sp.et2utc(et_select,"ISOC",2,50)
            subdata = QTable([times,et_select],names=names,dtype=dtypes)
            data_time = vstack([data_time,subdata])
            num_times = len(data_time['et'])
        
        # truncate output table to nclone data
        data_time = data_time[:nclone]
        
        # plot a histogram of fakeor/anchor times
        fkr_time_plt_file = self.anchor_file.replace('.dat','-time.png')
        rng_min = ((et_start - self.shower.et_max)*u.s).to('min').value
        rng_max = ((et_stop  - self.shower.et_max)*u.s).to('min').value
        nbins = np.min([int(nclone/2),10])
        obsutils.make_histimeplot(data_time['et'],self.shower.et_max,fkr_time_plt_file,rng=[rng_min,rng_max],nbins=nbins)
        log.info('plot saved in '+fkr_time_plt_file)
        return data_time
    
    def make_anchor_data(self,data_anchor_times):
        """Make anchor data.
        
        Parameters
        ----------
        data_anchor_times : astropy.table.QTable object
            Anchor timing within observation run.
            Columns are 'Time' and 'et', for
            astropy.time.Time in isot format
            and SPICE float time.
        
        Returns
        -------
        data_anchor : astropy.table.QTable object
            Anchor timing ('Time','et'), location ('lon','lat','alt'),
            azimuth ('Az'), zenith angle ('zen_angle') and 
            distance from camera1 and 2 ('dist1','dist2').
        
        """
        log.info('Making '+str(len(data_anchor_times['et']))+' fakeor locations (please wait...)')
        # set output data
        data_anchor = QTable(names=['lon','lat','alt','Az','zen_angle','dist1','dist2'])
        data_anchor['lon'].info.format = '9.5f'
        data_anchor['lat'].info.format = '9.5f'
        data_anchor['alt'].info.format = '8.2f'
        data_anchor['Az'].info.format = '9.5f'
        data_anchor['zen_angle'].info.format = '6.2f'
        data_anchor['dist1'].info.format = '9.2f'
        data_anchor['dist2'].info.format = '9.2f'
        
        # create interpolation function to get radiant sky coordinates as a function of et time.
        radiant_ra_func = interpolate.interp1d(self.geom_data['et'],self.geom_data['RadRa'])
        radiant_dec_func = interpolate.interp1d(self.geom_data['et'],self.geom_data['RadDec'])
        # interpolate radiant sky coordinates
        radiant_ra = radiant_ra_func(data_anchor_times['et'])
        radiant_dec = radiant_dec_func(data_anchor_times['et'])
        # make radiant SkyCoordinate
        radiant_skycoo = SkyCoord(ra=radiant_ra,dec=radiant_dec,unit='deg',frame='icrs')
        
        # loop over fakeor time
        for (et,rad_skycoo) in zip(data_anchor_times['et'],radiant_skycoo):
            # retrieve center of fov and max physical distance with all corners of fov
            (fov_center_eloc,fov_center_radius) = self.get_fov_center_maxrad(et,verbose=True)
            keepgoing = True
            # loop until fakeor in FOV of both nacelles
            while keepgoing:
                # make anchors locations
                anc_eloc = self.make_anchor_eloc(1,fov_center_eloc,fov_center_radius)
                # check if achor is in fov of camera1
                (infov1,dist1) = self.eloc_infov(self.nacelle_name1,et,anc_eloc)
                # check if anchor is in fov of camera2
                (infov2,dist2) = self.eloc_infov(self.nacelle_name2,et,anc_eloc)
                # if in both fov exit loop
                if (infov1 and infov2):
                    keepgoing = False
                    #log.debug('Anchor eloc: '+str(anc_eloc))
            # convert et into Time object
            time = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
            # compute radiant azimuth at time et, from fakeor location
            radiant_altaz = rad_skycoo.transform_to(AltAz(obstime=time,location=anc_eloc))
            # compute fakeor azimuth = radiant azimuth + 180 deg
            fakeor_az = (radiant_altaz.az.to('deg') + 180*u.deg)%(360.0*u.deg)
            # compute fakeor entry zenith angle = 90-radiant elevation
            fakeor_zenagl =  90.0*u.deg - radiant_altaz.alt.to('deg')
            # store anchor location into output data
            data_anchor.add_row([anc_eloc.lon.to('deg'),anc_eloc.lat.to('deg'),anc_eloc.height.to('m'),fakeor_az.to('deg'),fakeor_zenagl.to('deg'),dist1.to('m').value,dist2.to('m').value])
        # set data_anchor units
        data_anchor['lon'].unit = u.deg
        data_anchor['lat'].unit = u.deg
        data_anchor['alt'].unit = u.m
        data_anchor['Az'].unit = u.deg
        data_anchor['zen_angle'].unit = u.deg
        data_anchor['dist1'].unit = u.m
        data_anchor['dist2'].unit = u.m
        # add time in data_anchor
        data_anchor['Time'] = data_anchor_times['Time']
        data_anchor['et'] = data_anchor_times['et']
        data_anchor['et'].info.format = '13.3f'
        # rearrange data_anchor
        data_anchor.sort('et')
        data_anchor = data_anchor['Time','et','lon','lat','alt','Az','zen_angle','dist1','dist2']
        return data_anchor
    
    def create_anchor_data(self):
        """Create Anchor data for the observation run.
        
        Data are written in anchor_file file defined 
        in the 'FAKEOR' section of the configuration file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        anchor_data : astropy.table.QTable
            Anchor data: Time,et,lon,lat,alt,Az,zen_angle,dist1,dist2,Mass
        
        """
        # check if anchor file exists
        go_make_anchor = True
        if (os.path.exists(self.anchor_file) and self.read_res):
            log.info('Read anchor file: '+self.anchor_file)
            anchor_data = QTable.read(self.anchor_file,format='ascii.fixed_width_two_line')
            anchor_data['lon'].unit = u.deg
            anchor_data['lat'].unit = u.deg
            anchor_data['alt'].unit = u.m
            anchor_data['Az'].unit = u.deg
            anchor_data['zen_angle'].unit = u.deg
            anchor_data['dist1'].unit = u.m
            anchor_data['dist2'].unit = u.m
            anchor_data['Mass'].unit = u.kg
            # now check if the number of anchors is the expected one
            anc_num = len(anchor_data['lon'])
            if (anc_num==self.nclone):
                # no need to make Anchors
                go_make_anchor = False
            else:
                msg = '*** The Anchor file '+self.anchor_file+' has '+str(anc_num)+' data instead of '+str(self.nclone)+' => re-doing Anchors.'
                log.warning(msg)
        if go_make_anchor:
            # make fakeors times within start/stop observation run, with time margins to make sure all quantities are defined.
            fakeor_times = self.make_fakeor_times(self.nclone,self.start+0.01,self.stop-0.01)
            # create a population of magnitude or mass given meter shower properties
            self.shower.create_masspop(N=self.nclone,min_mass=self.fkr_min_mass)
            # make fakeors locations
            anchor_data = self.make_anchor_data(fakeor_times)
            # put absolute magnitude into data_anchor
            anchor_data['Mass'] = self.shower.pop_mass
            anchor_data['Mass'].info.format = '7.2E'
            # save anchors into dat file
            anchor_data.write(self.anchor_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
            log.info('Anchor data saved in '+self.anchor_file)
        return anchor_data
    
    def make_fkr_summary_data(self):
        """Make Fakeor summary QTable.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        fkr_summary_data = QTable(names=['Time','et','id','cam','min_mag','nobs'],dtype=['object','float','int','str','float','int'])
        fkr_summary_data['Time'].info.format = '%23s'
        fkr_summary_data['et'].info.format = '13.3f'
        fkr_summary_data['min_mag'].info.format = '6.2f'
        fkr_summary_data['nobs'].info.format = '4d'
        return fkr_summary_data
    
    def create_fakeors(self):
        """Create Fakeors.
        
        Parameters
        ----------
        
        Returns
        -------
        
        
        """
        # create Anchor data for the observation duration
        anchor_data = self.create_anchor_data()
        
        # make fakeor summary output data
        fkr_summary_data = self.make_fkr_summary_data()
        
        # loop over all anchor objects
        for (fkr_id,anc_data) in zip(range(self.nclone),anchor_data):
            fmt = '{:0'+str(len(str(self.nclone)))+'d}'
            fkr_id_str = fmt.format(fkr_id)
            # make Fakeor Time object
            time = Time(anc_data['Time'],scale='utc')
            # set Fakeor name root
            fkr_name_root = utils.time2name(time,extn='')
            # make Anchor object
            anc_name = 'ANC' + fkr_id_str
            anc = anchor.Anchor(name=anc_name)
            anc_eloc = EarthLocation.from_geodetic(anc_data['lon'].to('deg'),anc_data['lat'].to('deg'),height=anc_data['alt'].to('m'))
            anc.from_eloc(anc_eloc,time,anc_data['Az'].to('deg'))
            anc_file = self.anchor_out_dir + anc_name + '.dat'
            anc.save(anc_file)
            anc_spice_topoframe_file = self.anchor_out_dir + anc_name + '.fk'
            anc_spice_id = int(self.config['FAKEOR']['anchor_id']) + fkr_id
            anc.make_topolike_frame(spice_id=anc_spice_id,spice_topoframe_file=anc_spice_topoframe_file)
            # make AFM object
            log.info('create AFM object')
            afmo = afm.AFM()
            # if AFM data are taken from a collection of already performed simulations
            if self.afm_data_mode=='collect':
                # retrieve already performed simulations.
                afm_intfile = afmo.get_intfile_fromcollection(self.shower.Ventry,anc_data['zen_angle'],anc_data['Mass'],coldir=self.fkr_coldir)
                # if integration not found in collection, make it
                if not afm_intfile:
                    afm_intfile = afm.make_afm_collection([self.shower.Ventry],[anc_data['zen_angle']],[anc_data['Mass']],out_dir=self.fkr_coldir)[0]
            # if AFM data are created for each fakeor
            else:
                afmconf = self.afm_out_dir + fkr_name_root + '.in'
                afm_dummy_radius = 0.01*u.m
                afmo.write_config(afmconf,time,self.shower.Ventry,anc_data['zen_angle'],self.camera.fps,afm_dummy_radius,self.afm_out_dir,mass=anc_data['Mass'])
                afm_intfile = afmo.launch(config_file=afmconf,sub=True)
            log.debug('afm_intfile : '+str(afm_intfile))
            if not os.path.exists(afm_intfile):
                msg = '*** FATAL ERROR: afm int file '+afm_intfile+' was NOT created'
                log.error(msg)
                raise IOError(msg)
            # read AFM data
            afmo.read_int(afm_intfile)
            # check number of data
            if (len(afmo.data)<2):
                msg = '*** There is at most one data point in file '+afm_intfile+' -> Skipped.'
                log.warning(msg)
                continue
            # make Fakeor object
            log.info('create fakeor object')
            fkr_name = 'FKR' + fkr_id_str
            fkr = fakeor.Fakeor(name=fkr_name)
            fkr.set_log_level(self.logdict[self.config['USER']['log_level']])
            # put AFM and Anchor objects into Fakeor
            fkr.from_afm_anchor(afmo,anc)
            # set Fakeor SPK, dat and kml file names, and SPICE id
            fkr_spk_file = self.fkr_outdir + fkr_name + '.spk'
            fkr_dat_file = fkr_spk_file.replace('.spk','.dat')
            fkr_kml_file = fkr_spk_file.replace('.spk','.kml')
            fkr_spice_id = int(self.config['FAKEOR']['fakeor_id']) - fkr_id
            # convert Fakeor into SPK, save in dat and kml formats
            fkr.to_spk(fkr_name,fkr_spice_id,fkr_spk_file)
            fkr.save(fkr_dat_file)
            # create fake observations from each nacelle
            for nacelle_name in [self.nacelle_name1,self.nacelle_name2]:
                # set fakeobs output file name
                obs_file = self.fkr_outdir + fkr_name + '_'+nacelle_name+'-obs.dat'
                # make FakeObs object
                fkobs = self.make_fakeobs(fkr,nacelle_name,obs_file)
                # make a fake observation summary
                (isobs,fkr_time,fkr_et_start,fkr_et_stop,min_mag,nobs) = fkobs.make_summary()
                # check if the Fakeor is observable
                if isobs:
                    # put data into fake observations summary table
                    fkr_summary_data.add_row([fkr_time.isot,fkr_et_start,fkr_spice_id,nacelle_name,min_mag,nobs])
                    # save Fakeor into kml
                    fkr.to_kml(fkr_kml_file,et_start=fkr_et_start,et_stop=fkr_et_stop)
            log.debug('Fake observations done.')
            
            # unload fakeor spk
            log.debug('unloading Fakeor and Anchor SPICE kernels')
            sp.unload(fkr_spk_file)
            sp.unload(anc.bsp_file)
            sp.unload(anc.spice_topoframe_file)
            log.debug('kernels: '+' '.join([anc.spice_topoframe_file,anc.bsp_file,fkr_spk_file])+' unloaded')
        log.info('All Fakeors done')
        
        # save Fakeor summary
        nb_fkrsum = len(fkr_summary_data)
        log.info('There are '+str(nb_fkrsum)+' fakeor summary data to write')
        if nb_fkrsum:
            self.fkr_sum_file = self.config['FAKEOR']['fakeor_file']
            log.info('saving fakeor summary into '+self.fkr_sum_file)
            fkr_summary_data.write(self.fkr_sum_file,format='ascii.fixed_width_two_line',overwrite=True)
            log.info('Fakeor summary data saved in '+self.fkr_sum_file)
            # plot fakeor summary data: time histogram and mag vs time, single and double station observation
            # plot a histogram of observed fakeor times
            fkr_time_plt_file = self.fkr_sum_file.replace('.dat','-time.png')
            rng_min = ((np.min(fkr_summary_data['et']) - self.shower.et_max) * u.s).to('min').value
            rng_max = ((np.max(fkr_summary_data['et']) - self.shower.et_max) * u.s).to('min').value
            nbins = np.min([int(self.nclone/2),10])
            obsutils.make_histimeplot(fkr_summary_data['et'],self.shower.et_max,fkr_time_plt_file,rng=[rng_min,rng_max],nbins=nbins)
        else:
            log.warning('There is no fakeor observable given the constrains!')
        return
    
    def make_fakeobs(self,fkr,nacelle_name,fakeobs_file):
        """Make fake observation from a given camera.
        
        Parameters
        ----------
        fkr : showersim.fakeor.Fakeor Object.
            Fakeor to be observed.
        nacelle_name : string
            Nacelle (SPICE spacecraft) name.
        fakeobs_file : string
            Output fake observation file name.
        load_spk : boolean, optional
            If True, the nacelle kernels are loaded.
            Default is True.
        
        Returns
        -------
        fkobs : showersim.fakeobs.FakeObs object
            Fake Observation
        
        """
        log.debug('Making FakeObservation for '+fkr.name+' as seen from '+nacelle_name)
        # make FakeOBs Object
        fkobs = fakeobs.FakeObs(name=fkr.name)
        fkobs.set_log_level(self.logdict[self.config['USER']['log_level']])
        # fill in FakeObs data
        fkobs.from_FNCF(fkr,nacelle_name,self.cov,self.lim_mag,self.camera.fps)
        # save FakeOBs data
        if (fkobs.nobs):
            fkobs.save(fakeobs_file)
            log.debug(str(fkobs.nobs)+' fake observations saved in '+fakeobs_file)
        else:
            log.debug('There is no observable data for fakeor: '+fkr.name)
        return fkobs
        
    
    
if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='obsplanner arguments.')
    parser.add_argument('-conf',default='./conf/config.in',help='configuration file. Default is: ./conf/config.in')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.conf
    
    # create Object
    op = ObservationPlanner(config_file)

    # launch simulation
    op.launch()
else:
    log.debug('successfully imported')
