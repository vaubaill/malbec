"""
create_tf.py
Goal:
- make frame spice kernel (= a text file) for MALBEC nacelle

Authors:
A. Rietze, Univ. Oldenburg, Germany
J. Vaubaillon, IMCCE, 2020, MALBEC project
"""

# --------------------------------------------------------------------------------
# import all necessary tools
# --------------------------------------------------------------------------------
import os
import spiceypy as sp

from missionplanner import utils
from missionplanner.utils import log


# set home directory
home = os.getenv('HOME') + '/'


from obsplanner.make_id import make_topoframe_name,make_topoframe_id

# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_tf(nacelle_spk,nacelle_id,nacelle_name,out_tf_file=None,kernel_path='/astrodata/kernels/',load_stdker=True,load_spker=True):
    """Create a SPICE frame kernel tf-file for the MALBEC nacelle.
    
    For that the spk file of a specific nacelle at specific launch site is loaded.
    The SPICE ID of the nacelle is retrieved from this spk file and checked with nacelle_id.
    The corresponding SPICE name is also double checked.
    
    Unless the full path output file is specified via the out_tf_file parameter, the output tf file
    is saved in the same directory as nacelle_spk and its name is: nacelle_name + '.tf'
    
    Parameters
    ----------
    nacelle_spk : string
        Nacelle SPK full path file name.
    nacelle_id : int
        Nacelle SPICE id.
    nacelle_name : string
        Nacelle name.
    out_tf_file: string, optional
        Full path output file. If not specified (default), the output tf file
        is saved in the same directory as nacelle_spk and its name is: nacelle_name + '.tf'.
        Default is None (i.e. unspecified).
    kernel_path : string, optional
        Path to SPICE standard kernel. Default is '/astrodata/kernels/'.
    load_stdker : boolean, optional.
        If True, the SPICE standard kernel is loaded. Default is True.
    load_spker : boolean, optional.
        If True, the SPICE SPK kernel is loaded. Default is True.
    
    Returns:
    ------------
    out_tf_dir : string
        Full path to output tf-file
    nacelle_frame_id : int
        Nacelle frame SPICE id.
    nacelle_frame_name : string
        Nacelle frame name.
    
    """
    
    # load SPICE standard kernel if needed
    if load_stdker:
        # load standard kernel
        std_kernel_file = kernel_path + 'standard.ker'
        utils.check_file(std_kernel_file,log)
        sp.furnsh(std_kernel_file)
    
    # load nacelle spk file if needed
    if load_spker:
        utils.check_file(nacelle_spk,log)
        sp.furnsh(nacelle_spk)
    
    # retrieve the path to nacelle_spk
    nacelle_dir = os.path.dirname(nacelle_spk)+'/'
    
    # get nacelle SPICE id from spk file
    if not (nacelle_id == sp.spkobj(nacelle_spk)[0]):
        msg = '***FATAL ERROR: SPICE nacelle ids are different: '+str(nacelle_id)+' vs '+str(sp.spkobj(nacelle_spk)[0])
        log.error(msg)
        raise ValueError(msg)
    
    # set SPICE Frame name
    nacelle_frame_name = make_topoframe_name(nacelle_name)
    
    # set nacelle frame SPICE id
    nacelle_frame_id = make_topoframe_id(nacelle_id)
    
    # Set output path for tf-file for nacelle
    if not out_tf_file:
        out_tf_file = nacelle_dir + '/' + nacelle_name + '.tf'
    
    # checking if output ik-file already exists
    # delete previously created malbec/nacelle kernel files
    if os.path.exists(out_tf_file):
        os.remove(out_tf_file)
    
    # ====================================================
    # now make frame kernel for MALBEC station camera on nacelle
    with open(out_tf_file, 'w') as out:
        out.write("KPL/FK\n\n")
        out.write("MALBEC Frame Kernel\n")
        out.write("===============================================================================\n")
        out.write("\n")
        out.write("     This frame kernel contains the orientation of the MALBEC nacelle of\n")
        out.write("     " + repr(nacelle_name) + ".\n")
        out.write("\n")
        out.write("Version and Date\n")
        out.write("-------------------------------------------------------------------------------\n")
        out.write("\n")
        out.write("     Version 1.0 -- May 11, 2020 - Jeremie Vaubaillon, Athleen Rietze\n")
        out.write("\n")
        out.write(" MALBEC Frame\n")
        out.write("-------------------------------------------------------------------------------\n")
        out.write("\n")
        out.write("The following MALBEC frame is defined in this kernel file:\n")
        out.write("\n")
        out.write("         Name                  Relative to           Type       NAIF ID\n")
        out.write("     ======================  ===================  ============   =======\n")
        out.write("     Nacelle Frame:\n")
        out.write("     -----------------\n")
        out.write("   " + repr(nacelle_frame_name) +"       rel.to ITRF93         CK         " + str(nacelle_frame_id)+"\n")
        out.write("\n")
        out.write("MALBEC_NACELLE NAIF ID Codes -- Definition Section\n")
        out.write("========================================================================\n")
        out.write("\n")
        out.write("   This section contains name to NAIF ID mappings for the MALBEC\n")
        out.write("   Nacelle1. Once the contents of this file is loaded into the\n")
        out.write("   KERNEL POOL, these mappings become available within SPICE, \n")
        out.write("   making it possible to use names instead of ID code in the high\n")
        out.write("   level SPICE routine calls.\n\n")
        out.write("  Spacecraft and its structures: \n")
        out.write("   ------------------------------\n\n")
        out.write("      " + repr(nacelle_name) + "         " + str(nacelle_id) + "\n\n")
        out.write("   Spacecraft Frames:\n")
        out.write("   --------------------\n\n")
        out.write("      " + repr(nacelle_frame_name) + "    " + str(nacelle_frame_id) + "\n\n")
        out.write("   The mappings summarized in this table are implemented by the keywords\n")
        out.write("   below.\n\n")
        out.write("\\begindata\n\n")
        out.write("    NAIF_BODY_NAME += (" + repr(nacelle_name) + ")\n")
        out.write("    NAIF_BODY_CODE += (" + str(nacelle_id) + ")\n\n")
        out.write("\\begintext\n\n")
        out.write("\\begindata\n\n")
        out.write("  FRAME_" + str(nacelle_frame_name) + " = " + str(nacelle_frame_id) + "\n")
        #out.write("  FRAME_" + str(nacelle_frame_id) + "_ID             = " + str(nacelle_frame_id) + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_NAME           = " + repr(nacelle_frame_name) + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_CLASS          = 5" + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_CLASS_ID       = " + str(nacelle_frame_id) + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_CENTER         = " + str(nacelle_id) + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_RELATIVE       = " + repr('J2000') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_DEF_STYLE      = " + repr('PARAMETERIZED') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_FAMILY         = " + repr('TWO-VECTOR') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_AXIS       = " + repr('-Z') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_VECTOR_DEF = " + repr('TARGET_NEAR_POINT') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_OBSERVER   = " + str(nacelle_id) + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_TARGET     = " + repr('EARTH') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_ABCORR     = " + repr('NONE') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_AXIS       = " + repr('X') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_VECTOR_DEF = " + repr('CONSTANT') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_FRAME      = " + repr('ITRF93') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_SPEC       = " + repr('RECTANGULAR') + "\n")
        out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_VECTOR     = (" + str(0) + ", " + str(0) + ", " + str(1) + ")\n\n")
        out.write("\\begintext\n")
    log.info("data saved in " + out_tf_file)
    return out_tf_file,nacelle_frame_id,nacelle_frame_name


# -----------------------------------------------------------------------------
# Define some variables
# -----------------------------------------------------------------------------
def main():
    # Define some input values
    # SPICE default kernel path
    kernel_path = '/astrodata/kernels/'

    # set path to nacelle & launch site specific input (nacelle_spk) directory
    # in order to retrieve the necessary SPICE IDs
    nacelle_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'
    nacelle_spk = nacelle_dir + 'trajectory.spk'
    nacelle_id = -1399001
    nacelle_name = 'MALBEC_NACELLE1'
    out_tf_file = nacelle_dir + 'nacelle1.tf'
    
    # Call create_tf function to create tf-file for nacelle
    tf_file = create_tf(nacelle_spk,nacelle_id,nacelle_name,out_tf_file=out_tf_file,load_stdker=True,load_spker=True,kernel_path=kernel_path)
    
    # check the whole thing went well
    if not os.path.exists(out_tf_file):
        msg = '*** FATAL ERROR: output tf file not created: '+out_tf_file
        raise IOError(msg)
    
# =============================================================================
if __name__ == '__main__':
    main()
