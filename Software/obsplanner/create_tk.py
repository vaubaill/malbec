"""
create_tf.py
Goal:
- make fixed frame spice kernel (= a text file) for MALBEC camera

Authors:
A. Rietze, Univ. Oldenburg, Germany
J. Vaubaillon, IMCCE, 2020, MALBEC project
"""

# --------------------------------------------------------------------------------
# import all necessary tools
# --------------------------------------------------------------------------------
import os
import spiceypy as sp
import astropy.units as u

from missionplanner import utils
from missionplanner.utils import log
from obsplanner.make_id import make_topoframe_name
from obsplanner.make_id import make_nacelle_orientation_frame_name,make_nacelle_orientation_frame_id

# set home directory
home = os.getenv('HOME') + '/'

# log sub-directory
log_dir = home + 'LOG/'

# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_tk(nacelle_spk,nacelle_id,nacelle_name,offset_az=0.0*u.deg,offset_el=0.0*u.deg,out_tk_file=None,kernel_path='/astrodata/kernels/',load_stdker=True,load_spker=True):
    """Create a SPICE camera text frame kernel tk-file for the MALBEC camera.
    
    Unless the full path output file is specified via the out_tk_file parameter, the output tf file
    is saved in the same directory as nacelle_spk and its name is: nacelle_name + '.tf'
    
    Parameters
    ----------
    nacelle_spk : string
        Nacelle SPK full path file name.
    nacelle_id : int
        Nacelle SPICE id.
    nacelle_name : string
        Nacelle name.
    offset_az : astropy.units.Quantity object
        Azimuth offset angle. Default is 0.0*u.deg.
    offset_el : astropy.units.Quantity object
        Elevation offset angle. Default is 0.0*u.deg.
    out_tk_file: string, optional
        Full path output file. If not specified (default), the output tf file
        is saved in the same directory as nacelle_spk and its name is: nacelle_name + '.tf'.
        Default is None (i.e. unspecified).
    kernel_path : string, optional
        Path to SPICE standard kernel. Default is '/astrodata/kernels/'.
    load_stdker : boolean, optional.
        If True, the SPICE standard kernel is loaded. Default is True.
    load_spker : boolean, optional.
        If True, the SPICE SPK kernel is loaded. Default is True.
    
    Returns:
    ------------
    out_tf_dir : string
        Full path to output tf-file
    nacelle_frame_id : int
        Nacelle frame SPICE id.
    nacelle_frame_name : string
        Nacelle frame name.
    
    """
    
    # load SPICE standard kernel if needed
    if load_stdker:
        # load standard kernel
        std_kernel_file = kernel_path + 'standard.ker'
        utils.check_file(std_kernel_file,log)
        sp.furnsh(std_kernel_file)
    
    # load nacelle spk file if needed
    if load_spker:
        utils.check_file(nacelle_spk,log)
        sp.furnsh(nacelle_spk)
    
    # retrieve the path to nacelle_spk
    nacelle_dir = os.path.dirname(nacelle_spk)+'/'
    
    # get nacelle SPICE id from spk file
    if not (nacelle_id == sp.spkobj(nacelle_spk)[0]):
        msg = '***FATAL ERROR: SPICE nacelle ids are different: '+str(nacelle_id)+' vs '+str(sp.spkobj(nacelle_spk)[0])
        log.error(msg)
        raise ValueError(msg)
    
    """
    # extract nacelle number from nacelle_name. Note: nacelle_number is a string.
    nacelle_number = re.findall(r'\d+',nacelle_name)[0]
    
    # using the nacelle_number to double check the corresponding SPICE name
    # in case of example1: nacelle_name = MALBEC_NACELLE1
    if not (nacelle_name == 'MALBEC_NACELLE' + nacelle_number):
        msg = '*** FATAL ERROR: conflict of nacelle name: '+nacelle_name+' vs '+'MALBEC_NACELLE' + nacelle_number
        log.error(msg)
        raise ValueError(msg)
    """
    
    # set nacelle frame name and id
    nacelle_orientation_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
    nacelle_orientation_frame_id = make_nacelle_orientation_frame_id(nacelle_id)
    #frame_class_id = make_camera_id(nacelle_id)
    frame_class_id = make_nacelle_orientation_frame_id(nacelle_id)
    
    # for documentation
    nacelle_topo_frame_name = make_topoframe_name(nacelle_name)
    
    # Set output path for tf-file for nacelle
    if not out_tk_file:
        out_tk_file = nacelle_dir + '/' + nacelle_name + '.tk'
    
    # checking if output ik-file already exists
    # delete previously created malbec/nacelle kernel files
    if os.path.exists(out_tk_file):
        os.remove(out_tk_file)
    
    # ====================================================
    # now make orientation text frame kernel for MALBEC nacelle
    with open(out_tk_file, 'w') as out:
        out.write("KPL/IK\n\n")
        out.write("MALBEC nacelle orientation Frame Kernel\n")
        out.write("===============================================================================\n")
        out.write("\n")
        out.write("     This text frame kernel contains the orientation of the MALBEC nacelle \n")
        out.write("     " + repr(nacelle_name) + ".\n")
        out.write("\n")
        out.write("Version and Date\n")
        out.write("-------------------------------------------------------------------------------\n")
        out.write("\n")
        out.write("     Version 1.0 -- May 11, 2020 - Jeremie Vaubaillon, Athleen Rietze\n")
        out.write("\n")
        out.write(" MALBEC Frame\n")
        out.write("-------------------------------------------------------------------------------\n")
        out.write("\n")
        out.write("The following MALBEC text frame is defined or used in this kernel file:\n")
        out.write("\n")
        out.write("         Name                  Relative to           Type       NAIF ID\n")
        out.write("     ======================  ===================  ============   =======\n")
        out.write("     Nacelle Frame:\n")
        out.write("     -----------------\n")
        out.write("   " + repr(nacelle_orientation_frame_name) +"       rel.to "+repr(nacelle_topo_frame_name)+"         CK         " + str(nacelle_orientation_frame_id)+"\n")
        out.write("\n")
        out.write(nacelle_name+" NAIF ID Codes -- Definition Section\n")
        out.write("========================================================================\n")
        out.write("\n")
        out.write("   This section contains name to NAIF ID mappings for the MALBEC\n")
        out.write("   "+nacelle_name+". Once the contents of this file is loaded into the\n")
        out.write("   KERNEL POOL, these mappings become available within SPICE, \n")
        out.write("   making it possible to use names instead of ID code in the high\n")
        out.write("   level SPICE routine calls.\n\n")
        out.write("  Spacecraft and its structures: \n")
        out.write("   ------------------------------\n\n")
        out.write("      " + repr(nacelle_name) + "         " + str(nacelle_id) + "\n\n")
        out.write("   Science Instruments:\n")
        out.write("   --------------------\n\n")
        out.write("   The mappings summarized in this table are implemented by the keywords\n")
        out.write("   below.\n\n")
        out.write("\\begindata\n\n")
        out.write("    NAIF_BODY_NAME += (" + repr(nacelle_name) + ")\n")
        out.write("    NAIF_BODY_CODE += (" + str(nacelle_id) + ")\n\n")
        out.write("\\begintext\n\n")
        out.write("\\begindata\n\n")
        out.write("  FRAME_" + str(nacelle_orientation_frame_name) + "  = " + str(nacelle_orientation_frame_id) + "\n")
        out.write("  FRAME_" + str(nacelle_orientation_frame_id) + "_NAME           = " + repr(nacelle_orientation_frame_name) + "\n")
        out.write("  FRAME_" + str(nacelle_orientation_frame_id) + "_CLASS          = 3" + "\n")
        out.write("  FRAME_" + str(nacelle_orientation_frame_id) + "_CLASS_ID       = " + str(frame_class_id) + "\n")
        out.write("  FRAME_" + str(nacelle_orientation_frame_id) + "_CENTER         = " + str(nacelle_id) + "\n")
        out.write("  CK_" + str(nacelle_orientation_frame_id) + "_SCLK              = " + str(nacelle_id) + "\n")
        out.write("  CK_" + str(nacelle_orientation_frame_id) + "_SPK               = " + str(nacelle_id) + "\n")
        out.write("\\begintext\n\n")
        
    log.info("data saved in " + out_tk_file)
    return out_tk_file,nacelle_orientation_frame_id,nacelle_orientation_frame_name


# =============================================================================
if not __name__ == '__main__':
    log.debug('successfully loaded.')
