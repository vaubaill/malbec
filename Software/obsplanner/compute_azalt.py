"""Compute azimuth-elevation from one nacelle to the other.

Parameters
----------
nacelle1_name : string
    SPICE nacelle1 (spacecraft) name
nacelle2_name : string
    SPICE nacelle2 (spacecraft) name
time : astropy.time.Time Object
    Time
    
Returns
-------
altaz1 : astropy.coordinates.AltAz Object
    Azimuth and elevation of nacelle2 as seen from nacelle1.
altaz2 : astropy.coordinates.AltAz Object
    Azimuth and elevation of nacelle1 as seen from nacelle2.
spice_altaz1 : 2-array of astropy.units.Quantoty objects.
    Altitude and elevation of nacelle2 as seen from nacelle1.
spice_altaz2 : astropy.coordinates.AltAz Object
    Altitude and elevation of nacelle1 as seen from nacelle2.
radec1 : astropy.coordinates.SkyCoord Object
    Sky position of nacelle2 as seen from nacelle1.
radec2 : astropy.coordinates.SkyCoord Object
    Sky position of nacelle1 as seen from nacelle2.
    

"""


def compute_altaz(nacelle1_name,nacelle2_name,time):
    """
    
    
    """
    nacelle_id1 = sp.bodn2c(nacelle1_name)
    nacelle_id2 = sp.bodn2c(nacelle2_name)
    et = sp.str2et(time.isot)
    # compute cartesian position of nacelle2 wrt nacelle1, in nacelle1 frame, in [km]
    (eph,lt) = sp.spkezp(nacelle_id2,time,nacelle_frame_name1,'NONE',nacelle_id1)
    # convert cartesian coordinates into relative distance,azimuth,elevation, in [km,rad,rad]
    (spice_r,spice_az,spice_el) = sp.reclat(eph)
    
    
    
    return (altaz1,altaz2,radec1,radec2)
























