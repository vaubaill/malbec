from astropy.coordinates import get_sun,SkyCoord,AltAz
from astropy.time import Time
from astropy import units as u

def get_sun_az(eloc,ti,tf,step=10.0*u.min):
    """Get Sun azimuth as a function of time.
    
    Parameters
    ----------
    eloc : astropy.coordinates.EarthLocation object
        Location of observer
    ti : astropy.time.Time object
        Start time.
    tf : astropy.time.Time object
        Stop time.
    
    Returns
    -------
    t : astropy.time.Time
        Times for each computed step.
    as : astropy.coordinates.Angle object
        Azimuth
    el : astropy.coordinates.Angle object
        Elevation
    
    """
    local_frame     = AltAz(obstime=jd,location=eloc)
    sun_altaz       = get_sun(jd).transform_to(local_frame)
    
    return (t,alt,az)

