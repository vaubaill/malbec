from astropy.coordinates import SkyCoord, Angle, Latitude, Longitude, EarthLocation, AltAz,get_sun
from astropy.time import Time as Time
import astropy.units as u



# location of the launching site
site = EarthLocation.from_geodetic(Longitude('00:28:08.40',unit='deg'),
                            Latitude('43:37:07.74',unit='deg'),
                            0.0*u.km)
# Time in UT: change if needed
time =  Time('2018-06-21T12:00:00')# ,format='')


# get location f the Sun in the Sky: [ra,dec]
radec_sun = get_sun(time)
# define Altazimutal frame
altazframe = AltAz(obstime=time, location=site)
# transform Sun sky coordinate into local coordinates
altaz_Sun = radec_sun.transform_to(altazframe)

# print
print 'AltAz Sun= ',altaz_Sun.alt,altaz_Sun.az,'=',altaz_Sun.az.hour,'=',altaz_Sun.az.hms
print 'AltAz Sun= ',altaz_Sun.alt.to('deg').value,altaz_Sun.az.to('deg').value
