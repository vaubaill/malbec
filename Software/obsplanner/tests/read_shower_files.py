"""
    read_shower_files.py
    Test routine to read nighttime and daytime shower files from IMO in order to retrieve
    ZHR value, solar longitude of maximum of shower, date of shower maximum.
    Read IAU file "streamestablisheddata_jv.csv" to get radiant drift (RA, Dec).

    Input parameters:
    -----------------
    shower name (code for shower: e.g. 007 PER for Perseids)

    Step by step:
    -------------
    - read shower name
    - search in both IMO files for date of max., zhr
    - if shower not found in IMO files: raise Value Error

    Output:
    ------------
    - zhr value for shower
    - maximum of shower
    - solar longitude of maximum
    - radiant drift
        -> dra = radiant daily motion in right asscention
        -> dde = radiant daily motion in declination

    Authors:
    ------------
    A. Rietze - U. Oldenburg, Germany
    J. Vaubaillon - IMCCE

    ------------
    Version
    1.0

"""

#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import logging
import os
from astropy.table import QTable
from Software.missionplanner import utils
import astropy.units as u
from datetime import date
from time import strptime

# set home directory
home = os.getenv('HOME') + '/'


# --------------------------------------------------------------------------------
# create default logger object
# --------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)S) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


def get_shower_data(shower_name, shower_year, iau_shower_file, day_shower_file, night_shower_file):
    """
    Function to get useful data for the chosen meteor shower.

    Parameters:
    -----------
    shower_name:            string, Name of meteor shower (code)
                            e.g. 007 PER for Perseids

    shower_year:            year in which the shower shall be observed (e.g.: 2020)

    iau_shower_file:        string, optional
                            Name of meteor shower data file (IAU, IMO nighttime showers, IMO daytime showers)
                            Default for IAU: 'conf/streamestablisheddata_jv.csv'

    night_shower_file:      string, optional
                            Name of IMO nighttime meteor shower data file
                            Default for IMO nighttime: 'conf/List_nighttime_showers.csv'

    day_shower_file:        string, optional
                            Name of IMO daytime meteor shower data file
                            Default for IMO daytime: ''conf/List_daytime_showers.csv'
    Returns:
    ----------
    zhr:

    max_time:

    sol_long_max: solar longitude of maximum of shower

    rad_drift_ra:

    rad_drift_dec:

    fwhm: TBD

    """

    # read shower data in IMO daytime shower file
    data_day_show = read_imo_day_shower_file(day_shower_file)
    # make mask to select data
    mask = data_day_show['shower_code'] == shower_name

    # to check the information in those shower files
    # checking first daytime shower file
    # if not found, checking in nighttime shower file
    # then checking IAU shower file
    # @TODO: adding check to try to read all shower files one after the other, raising value error of not found

    # select data for the specific shower name
    day_show = data_day_show[mask.nonzero()]
    # retrieve zhr value, max_time_month, max_time_day, solar longitude from max. of shower from
    # IMO daytime shower file for given shower name
    zhr = day_show['rate_zhr']
    max_time_month = day_show['max_date_month'][0]
    max_time_day = day_show['max_date_day']
    max_solar_long = day_show['max_solar_longitude']

    # creating one date for time of maximum of shower from shower_year, max_time_month, max_time_day
    # by calling function to calculate this date
    max_time = calc_max_date(shower_year, max_time_month, max_time_day)
    # calculating correct zhr
    zhr_corrected = check_zhr_value(zhr)

    # read shower data in IMO nighttime shower file
    data_night_show = read_imo_night_shower_file(night_shower_file)
    # make mask to select data (Attention: here capital letter)
    mask = data_night_show['Shower_code'] == shower_name
    # select data for the specific shower name
    night_show = data_day_show[mask.nonzero()]
    # retrieve zhr value, max_time_month, max_time_day, solar longitude from max. of shower from
    # IMO daytime shower file for given shower name
    zhr = night_show['rate_zhr']
    max_time_month = night_show['maximum_date_month']
    max_time_day = night_show['maximum_date_day']
    max_solar_long = night_show['maximum_solar_longitude']

    # read all shower data in IAU file
    data_all_show = read_iau_shower_file(iau_shower_file)
    # make mask to select data
    mask = data_all_show['shower_name'] == shower_name
    # select data from IAU file for specific shower name
    shw_data = data_all_show[mask.nonzero()]
    # retrieve rad_drift_ra and rad_drift_dec from IAU file
    rad_drift_ra = shw_data['dRa']
    rad_drift_dec = shw_data['dDe']

    return zhr_corrected, max_time, max_solar_long, rad_drift_ra, rad_drift_dec

def read_iau_shower_file(shower_file):
    """Reads the IAU established meteor shower file, modified to be readable by JV.

    Parameters
    ----------
    shower_file : string
        IAU established meteor shower file, modified to be readable with csv.

    Returns
    -------
    data_shower : astropy.table.QTable
        Data of IAU established meteor showers.
    """
    # check existence of file
    utils.check_file(shower_file, log)
    # Read IAU meteor shower data: IAU meteor shower file modified by JV to make it easily readeable by astropy.table.Table
    shw_data = QTable.read(shower_file, format='ascii.csv', delimiter=';')

    # sanitize the missing data
    shw_data['q'].fill_value = -9.99
    shw_data['inc'].fill_value = -9.99
    shw_data['peri'].fill_value = -9.99
    shw_data['node'].fill_value = -9.99

    # set units of useful data
    shw_data['LaSun'].unit = u.deg
    shw_data['Ra'].unit = u.deg
    shw_data['De'].unit = u.deg
    shw_data['dRa'].unit = u.deg / u.d
    shw_data['dDe'].unit = u.deg / u.d
    shw_data['Vg'].unit = u.km / u.s
    shw_data['q'].unit = u.au
    shw_data['inc'].unit = u.deg
    shw_data['peri'].unit = u.deg
    shw_data['node'].unit = u.deg

    return shw_data

def read_imo_day_shower_file(shower_file):
    """
    Reads the IMO daytime shower file.

    Parameters
    ----------
    shower_file : string
        IMO daytime shower file, modified bz AR

    Returns
    -------
    data_shower : astropy.table.QTable
        Data of IMO daytime meteor showers.
    """
    # check existence of file
    utils.check_file(shower_file, log)
    # Read IMO daytime meteor shower data: IMO daytime meteor shower file modified by AR to make
    # it easily readeable by astropy.table.Table
    shw_data = QTable.read(shower_file, format='ascii.csv', delimiter=';')

    # set units of useful data
    shw_data['max_solar_longitude'].unit = u.deg
    shw_data['ra_radiant'].unit = u.deg
    shw_data['dec_radiant'].unit = u.deg

    return shw_data


def read_imo_night_shower_file(shower_file):
    """
    Reads the IMO nighttime shower file.

    Parameters
    ----------
    shower_file : string
        IMO nighttime shower file, modified by AR

    Returns
    -------
    data_shower : astropy.table.QTable
        Data of IMO nighttime meteor showers.
    """
    # check existence of file
    utils.check_file(shower_file, log)
    # Read IMO nighttime meteor shower data: IMO daytime meteor shower file modified by AR to make it easily readable by astropy.table.Table
    shw_data = QTable.read(shower_file, format='ascii.csv', delimiter=';')

    # set units of useful data
    shw_data['maximum_solar+longitude'].unit = u.deg
    shw_data['ra_radiant'].unit = u.deg
    shw_data['dec_radiant'].unit = u.deg

    return shw_data


def calc_max_date(shower_year, max_month, max_day):
    """
    Function to calculate the complete date of the maximum of the observed shower.
    Therefore the abbreviated name of the month which is found in the shower files, has
    to be converted into a number from 1-12.

    Parameters:
    ------------
    shower_year:    year of the observed shower. Format: YYYY (e.g.: 2020)

    max_month:      Month of maximum of observed shower as abbreviated name of month
                    (e.g.: 'Jan', 'Feb',...)

    max_day:        Day of maximum of observed shower

    Returns:
    ------------
    max_date:       complete date of maximum of observed shower. Format: YYYY-MM-DD
                    (e.g.: 2020-02-19)

    """

    # in order to retrieve the month as a number from 1-12, the abbreviated name of the month found
    # in the shower files must be converted
    log.debug(max_month)
    month_number = strptime(max_month, '%b').tm_mon

    # creating the date of the maximum of shower
    max_date = date(shower_year, month_number, max_day)

    return max_date

def check_zhr_value(zhr):
    """
    Function to check the zhr value retrieved from either IMO daytime or nighttime shower list.
    If it is either a level (from daytime shower list): set it to the corresponding value
        Sets rate of ZHR (level) to certain values:
        H: 100
        M: 30
        L: 10

    If it is a labelled as 'Var' from nighttime shower list:
        Sets value of ZHR when 'Var' to certain value: (in obsplan: found in config file)
        5-10

    Parameters:
    ------------
    zhr:    zhr value retrieved from either IMO daytime or nighttime shower list

    Returns:
    ----------
    new_zhr: zhr value set to a specific value when is was set to a Var or level before

    """

    # check if zhr value is 'Var'/ H / M / L and set it to corresponding Value
    if zhr == 'Var':
        new_zhr = 10
    elif zhr == 'H':
        new_zhr = 100
    elif zhr == 'M':
        new_zhr = 30
    elif zhr == 'L':
        new_zhr = 10
    else:
        new_zhr = zhr

    return new_zhr
# -----------------------------------------------------------------------------
# Define some variables
# -----------------------------------------------------------------------------

def main():
    # --------------------------------------------------------------------------------
    # Define default input values, can be overwritten by command line parameters
    # maybe better: configuration file?
    # --------------------------------------------------------------------------------
    # setting a year in which the shower is (going to be) observed
    year_of_shower = 2020

    # set a nighttime shower name (code) to test
    shower_name_night = '007 PER'

    # set path IMO list for nighttime showers
    shower_file_night = home + 'malbec/Software/obsplanner/conf/List_nighttime_showers.csv'

    # set a daytime shower name (code)
    shower_name_day = '171 ARI'

    # set path IMO list for daytime showers
    shower_file_day = home + 'malbec/Software/obsplanner/conf/List_daytime_showers.csv'

    shower_file_iau = 'conf / streamestablisheddata_jv.csv'

    zhr, max_time, sol_long_max, rad_drift_ra, rad_drift_dec = get_shower_data(shower_name_day,
                                                                               year_of_shower,
                                                                               shower_file_iau,
                                                                               shower_file_day,
                                                                               shower_file_night)
    print(zhr, max_time, sol_long_max, rad_drift_ra, rad_drift_dec)
# =============================================================================
if __name__ == '__main__':
    main()