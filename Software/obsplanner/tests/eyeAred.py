"""Compute naked eye reduced area


"""

import os
import logging
import numpy as np
import spiceypy as sp
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable, vstack,hstack
from astropy.coordinates import EarthLocation, AltAz, SkyCoord,spherical_to_cartesian,cartesian_to_spherical
from missionplanner import  utils
from showersim import shower_utils

# --------------------------------------------------------------------------------
# create default logger object
# --------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d  (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.INFO)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

def make_azel_mesh(eye_name,step_grid=1.0*u.deg):
    """Make azimuth/elevation mesh for given nacelle.
    
    Parameters
    ----------
    eye_name : string
        Human eye SPICE name.
    step_grid : astropy.units.Quantity object, optional.
        Grid angular step. Default is 1.0*u.deg.
    
    Returns
    -------
    az_all : 2-array of float.
        Azimuth angles.
    el_all : 2-array of float.
        Elevation angles.
    vec_x : 2-array of float.
        First cartesian coordinate.
    vec_y : 2-array of float.
        Second cartesian coordinate.
    vec_z : 2-array of float.
        Third cartesian coordinate.
    
    """
    # get fov features
    eye_id = sp.bodn2c(eye_name)
    log.debug('eye_id: '+str(eye_id))
    # retrieve eye fov info
    (fov_shape,fov_frame,brst_vec,n_crn,crn_vec) = sp.getfov(eye_id,4)
    # get fov half angle
    eye_half_angle = sp.vsep(crn_vec[0],brst_vec)*u.rad
    log.debug('eye_half_angle: '+str(eye_half_angle.to('deg')))
    # get azimuthelevation of boresight vector
    (eye_r,eye_el,eye_az) = cartesian_to_spherical(brst_vec[0],brst_vec[1],brst_vec[2])
    # compute start/end azimuth/elevation for each cell center
    az_beg = -eye_az + step_grid.to('rad')/2.0
    az_end =  eye_az - step_grid.to('rad')/2.0
    el_beg = -eye_el + step_grid.to('rad')/2.0
    el_end =  eye_el - step_grid.to('rad')/2.0
    # compute number of points for azimuth/elevation
    az_npnt = int(np.abs(az_end-az_beg).to('deg')/step_grid.to('deg')) + 1
    el_npnt = int(np.abs(el_end-el_beg).to('deg')/step_grid.to('deg')) + 1
    # make all azimuth and elevations
    az_all = np.linspace(az_beg.to('deg'),az_end.to('deg'),num=az_npnt)
    el_all = np.linspace(el_beg.to('deg'),el_end.to('deg'),num=el_npnt)        
    # make grid vectors
    azz,ell = np.meshgrid(az_all,el_all)
    # make cartesian vectors
    (vecs_x,vecs_y,vecs_z) = spherical_to_cartesian(np.repeat(1.0,len(azz.flatten())),
                                        ell.to('rad').flatten(),
                                        azz.to('rad').flatten())
    shp = (azz + ell).shape
    vecs_x = np.reshape(vecs_x,shp)
    vecs_y = np.reshape(vecs_y,shp)
    vecs_z = np.reshape(vecs_z,shp)
    
    return (azz,ell,vecs_x,vecs_y,vecs_z)

def get4corners(vec_az,vec_el,az_step,el_step):
    """Get the four corners around a pointing vector.
    
    Parameters
    ----------
    vec_az : astropy.units.Quantity object
        Azimuth of pointing vector.
    vec_el : astropy.units.Quantity object
        Elevation of pointing vector.
    az_step : astropy.units.Quantity object
        Azimuth step.
    el_step : astropy.units.Quantity object
        Elevation step.
    
    Returns
    -------
    crn_vec : 4x3 array of float
        vectors of the 4 corners around vec.
    
    """
    # 4 corners azimuth/elevation, in [rad]
    crn_el = [vec_el-el_step,vec_el+el_step,vec_el+el_step,vec_el-el_step]
    crn_az = [vec_az-az_step,vec_az-az_step,vec_az+az_step,vec_az+az_step]
    # 4 corners cartesian coordiantes
    (x,y,z) = spherical_to_cartesian(np.repeat(1.0,4),crn_el,crn_az)
    crn_vec = np.array([x,y,z]).transpose()
    #log.debug('crn_vec: '+str(crn_vec))
    return crn_vec

def make_grid_table(time,nacelle_name,eloc_cntr,elocs_crn,az,el,dist,area,Lgrnd):
    """Make a QTable grid data.
    
    Parameters
    ----------
    time :astropy.time.Time ovject
        Time of the record.
    nacelle_name : string
        Nacelle name
    eloc_cntr : astropy.coordinates.EarthLocation object.
        Location of center of grid cell.
    elocs_crn : 4-astropy.coordinates.EarthLocation object.
        Location of 4 corners of grid cell.
    az : astropy.units.Quantity object
        Azimuth of center of cell as seen from nacelle.
    el : astropy.units.Quantity object
        Elevation of center of cell as seen from nacelle.            
    dist : astropy.units.Quantity object
        Physical distance between nacelle and center of cell.
    area : astropy.units.Quantity object
        Cell physical surface area.
    Lgrnd : boolean
        True if ground in the FOV for this cell.
    
    Returns
    -------
    grid_data : astropy.table.QTable object.
        QTable with all parameters.
    """
    
    et = sp.str2et(time.isot)
    names = ['Time','et','nac_name',
             'ctrn_lon','ctrn_lat','ctrn_alt',
             'crn1_lon','crn1_lat','crn1_alt',
             'crn2_lon','crn2_lat','crn2_alt',
             'crn3_lon','crn3_lat','crn3_alt',
             'crn4_lon','crn4_lat','crn4_alt',
             'az','el','dist','area','Lgrnd']
    dtpyes = ['object','float','object',
             'float','float','float',
             'float','float','float',
             'float','float','float',
             'float','float','float',
             'float','float','float',
             'float','float','float','float','bool']
    grid_data = QTable(names=names,dtype=dtpyes)
    grid_data['ctrn_lon'].unit = u.deg
    grid_data['ctrn_lat'].unit = u.deg
    grid_data['ctrn_alt'].unit = u.m
    grid_data['crn1_lon'].unit = u.deg
    grid_data['crn2_lon'].unit = u.deg
    grid_data['crn3_lon'].unit = u.deg
    grid_data['crn4_lon'].unit = u.deg
    grid_data['crn1_lat'].unit = u.deg
    grid_data['crn2_lat'].unit = u.deg
    grid_data['crn3_lat'].unit = u.deg
    grid_data['crn4_lat'].unit = u.deg
    grid_data['crn1_alt'].unit = u.m
    grid_data['crn2_alt'].unit = u.m
    grid_data['crn3_alt'].unit = u.m
    grid_data['crn4_alt'].unit = u.m
    grid_data['az'].unit = u.deg
    grid_data['el'].unit = u.deg
    grid_data['dist'].unit = u.km
    grid_data['area'].unit = u.km**2
    # set format
    grid_data['Time'].info.format = '%23s'
    grid_data['et'].info.format = '13.3f'
    grid_data['ctrn_lon'].info.format = '11.7f'
    grid_data['ctrn_lat'].info.format = '11.7f'
    grid_data['ctrn_alt'].info.format = '9.2f'
    grid_data['crn1_lon'].info.format = '11.7f'
    grid_data['crn2_lon'].info.format = '11.7f'
    grid_data['crn3_lon'].info.format = '11.7f'
    grid_data['crn4_lon'].info.format = '11.7f'
    grid_data['crn1_lat'].info.format = '11.7f'
    grid_data['crn2_lat'].info.format = '11.7f'
    grid_data['crn3_lat'].info.format = '11.7f'
    grid_data['crn4_lat'].info.format = '11.7f'
    grid_data['crn1_alt'].info.format = '9.2f'
    grid_data['crn2_alt'].info.format = '9.2f'
    grid_data['crn3_alt'].info.format = '9.2f'
    grid_data['crn4_alt'].info.format = '9.2f'
    grid_data['az'].info.format = '7.3f'
    grid_data['el'].info.format = '7.3f'
    grid_data['dist'].info.format = '8.1f'
    grid_data['area'].info.format = '12.1f'
    grid_data.add_row([time.isot,et,nacelle_name,
         eloc_cntr.lon.to('deg')   ,eloc_cntr.lat.to('deg')   ,eloc_cntr.height.to('m'),
         elocs_crn[0].lon.to('deg'),elocs_crn[0].lat.to('deg'),elocs_crn[0].height.to('m'),
         elocs_crn[1].lon.to('deg'),elocs_crn[1].lat.to('deg'),elocs_crn[1].height.to('m'),
         elocs_crn[2].lon.to('deg'),elocs_crn[2].lat.to('deg'),elocs_crn[2].height.to('m'),
         elocs_crn[3].lon.to('deg'),elocs_crn[3].lat.to('deg'),elocs_crn[3].height.to('m'),
         az.to('deg'),el.to('deg'),
         dist.to('km'),area.to('km2'),Lgrnd])
    return grid_data


def compute_reduced_area(time,nacelle_name,nac_eloc,grid_data):
    """Compute reduced area.
    
    Parameters
    ----------
    time : astropy.timeTime object
        Time.
    nacelle_name : string
        Nacelle name.
    nac_eloc : astropy.coordinates.Earthlocation
        Nacelle location.
    grid_data : astropy.table.QTable
        Table of all grid parameters.
    
    Returns
    -------
    redarea_data : astropy.table.QTable
        Table of all grid parameters, updated with reduced area parameters.
    area_total : astropy.units.Quantity object.
        Total surveyed area, without taking into account corrections.
    area_reduced_total : astropy.units.Quantity object.
        Total reduced area.
    """        
    # create elocs for all grid cell centers
    elocs = EarthLocation.from_geodetic(grid_data['ctrn_lon'].to('deg'),
                                        grid_data['ctrn_lat'].to('deg'),
                                        height=grid_data['ctrn_alt'].to('m'))                        
    # compute air masses from nacelle to all elocs
    airmasses = shower_utils.compute_airmass_one2many(nac_eloc,elocs,time)
    # compute magnitude loss due to atmopshere absorption
    dm_atm = shower_utils.Roth1994(airmasses)
    
    # compute magnitude loss due to physical distance between camera and meteor
    dm_dist = -5.0*np.log10(100.0*u.km/grid_data['dist'].to('km'))*u.mag # dist is in [km]
    
    # compute magnitude loss due to meteor apparent velocity
    # make locs SkyCoord
    loc_altaz = SkyCoord(ra=grid_data['az'].to('deg'),dec=grid_data['el'].to('deg'))
    # retrieve nacelle obsplan number: 1 or 2
    nacelle_num = 0
    # retrieve geometric data at time time
    geom_data = obsutils.get_geom_data(time,nacelle_name,nacelle_num,self.geom_data)
    # make radiant AltAz skycoord
    rad_altaz = SkyCoord(ra=geom_data['RadAz'],dec=geom_data['RadAlt'],unit=u.deg)
    # compute angular separation between radiant and anchor, in [rad]
    locrad_sep = rad_altaz.separation(loc_altaz)
    # compute apparent angular velocity
    omega_met = (self.shower.Ventry.to('m.s-1') / 
                grid_data['dist'].to('m') * 
                np.sin(locrad_sep.to('rad').value) * 
                np.sin(grid_data['el'].to('rad').value) * 
                u.rad)
    # compute mag loss due to apparent angular velocity
    dm_appvel = phot.mag_loss_appvel(omega_met,self.camera.psf_angle,self.camera.fps)
    
    # compute total magnitude loss
    dm_total = dm_atm  + dm_appvel  + dm_dist
            
    # compute area loss factor due to total magnitude loss
    f_area_loss = self.shower.population_index**(-dm_total.value)
    # compute reduced Area surveyed by the camera
    red_area = grid_data['area'] * f_area_loss
    # compute total area and reduced area
    area_total = np.sum( grid_data['area'])
    reduced_area_total = np.sum(red_area)
    
    # put al results into a QTable
    redarea_data = obsutils.make_reduced_area_data_qtable()
    # 'airmasses','dm_atm','dm_dist','dm_appvel','dm_tot','f_area_loss','red_area'
    redarea_data = QTable([airmasses,dm_atm,dm_dist,dm_appvel,dm_total,f_area_loss,red_area],
                          names=['airmasses','dm_atm','dm_dist','dm_appvel','dm_tot','f_area_loss','red_area'])
    # set format
    redarea_data['airmasses'].info.format = '5.3f'
    redarea_data['dm_atm'].info.format = '5.3f'
    redarea_data['dm_tot'].info.format = '5.3f'
    redarea_data['dm_dist'].info.format = '5.3f'
    redarea_data['dm_atm'].info.format = '5.3f'
    redarea_data['dm_appvel'].info.format = '5.3f'
    redarea_data['red_area'].info.format = '12.1f'     
    redarea_data['f_area_loss'].info.format = '5.3f'
    
    """
    log.debug('airmasses = '+str(redarea_data['airmasses']))
    log.debug('dm_atm = '+str(redarea_data['dm_atm']))
    log.debug('locs_dist = '+str(grid_data['dist'].to('km')))
    log.debug('dm_dist = '+str(redarea_data['dm_dist']))
    log.debug('loc_altaz = '+str(loc_altaz))
    log.debug('rad_altaz = '+str(rad_altaz))
    log.debug('locrad_sep = '+str(locrad_sep))        
    log.debug('Ventry/locs_dist = '+str(self.shower.Ventry.to('m.s-1') / grid_data['dist'].to('m')))
    log.debug('locrad_sep = '+str(locrad_sep.to('deg')))
    log.debug('grid_data[el] = '+str(grid_data['el'].to('deg')))
    log.debug('psf_angle = '+str(self.camera.psf_angle.to('deg')))
    log.debug('omega_met = '+str(omega_met.to('deg.s-1')))
    log.debug('dm_appvel = '+str(redarea_data['dm_appvel']))
    log.debug('dm_tot (loss) = '+str(redarea_data['dm_tot']))        
    log.debug('areas = '+str(grid_data['area']))
    log.debug('areas_total = '+str(np.sum(grid_data['area'])))
    log.debug('population_index = '+str(self.shower.population_index))
    log.debug('f_area_loss = '+str(redarea_data['f_area_loss']))
    log.debug('reduced area = '+str(redarea_data['red_area']))
    log.debug('reduced_area_total = '+str(reduced_area_total))
    R_eq = np.sqrt(reduced_area_total)
    log.debug('R_eq = '+str(R_eq))
    """
    return redarea_data,area_total,reduced_area_total    



# set variables
home = os.getenv('HOME')

# SPICE path
spice_path = '/astrodata/kernels/'
# load standard SPICE kernels
sp.furnsh(spice_path+'standard.ker')

# load an Atmosphere kernel
sp.furnsh(home+'/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20161213/ObsPlan/NACELLE27_NACELLE28_-85_0_alpha7s_24mm-1.5/atmosphere.ker')

# load MALBEC site
sp.furnsh(home+'/PROJECTS/PODET/PODET-MET/MALBEC/malbec/Software/conf/sites.bsp')
sp.furnsh(home+'/PROJECTS/PODET/PODET-MET/MALBEC/malbec/Software/conf/sites.fk')
topo_frame = 'SITE1_TOPO'

# load nake eye instrument kernel
sp.furnsh(home+'/PROJECTS/PODET/PODET-MET/MALBEC/malbec/Software/conf/eye.ik')
# set eye SPICE name
eye_name = 'HUMAN_EYE'
# set step_grid
step_grid = 1.0*u.deg
# make azimuth/elevation mesh
(azz,ell,vecs_x,vecs_y,vecs_z) = make_azel_mesh(eye_name,step_grid=step_grid)

# set time
time = Time('2016-12-13T23:18:30.433',scale='utc')
et = sp.str2et(time.isot)

# set grid_data to None
grid_data = None

# loop over all FOV azimuth/elevation vectors
for (az,el,x,y,z) in zip(azz.flatten(),ell.flatten(),vecs_x.flatten(),vecs_y.flatten(),vecs_z.flatten()):
    vec = np.array([x,y,z])
    # get frame rotation between 'ITRF93' and nacelle local topographic-like frame ('TOPOLIKE')
    pointing_mtx = sp.pxform('ITRF93',topo_frame,et)
    # set Lgnd to True by default
    Lgnd = True
    # compute intersection with Earth surface
    try:
        loc_gnd,lt,vecgnd = sp.sincpt('ELLIPSOID','EARTH',et,'ITRF93','NONE',eye_name,topo_frame,vec)
        #loc_gnd,lt,vecgnd = sp.sincpt('ELLIPSOID','EARTH',et,topo_frame,'NONE',nacelle_name,camera_frame_name,vec)
        log.warning('Earth surface in FOV for instrment az,el= '+f"{az.to('deg'):0.01f}"+' '+f"{el.to('deg'):0.01f}")
        # rotate nac2loc to get it in topolike frame and later compute azimuth/elevation
        vecgnd_topo = sp.mxv(pointing_mtx,vecgnd)
        # compute distance, azimuth, elevation of cell wrt nacelle
        (gnd_dist,vecgnd_az,vecgnd_el) = sp.recrad(vecgnd_topo)
        vecgnd_az = ((-vecgnd_az)%(2.0*np.pi))*u.rad
        vecgnd_el = vecgnd_el*u.rad
        log.warning('                         topo-like: az,el= '+f"{vecgnd_az.to('deg'):0.01f}"+' '+f"{vecgnd_el.to('deg'):0.01f}")                
    except sp.utils.support_types.SpiceyError as e:
        notfoundstr = 'Spice returns not found for function: sincpt'
        if notfoundstr in str(e):
            Lgnd = False
    except Exception as e:
        log.error(str(e))
        raise e
    # compute intersection of cell boresight with top of atmosphere
    loc_top,lt,nac2loc = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',eye_name,topo_frame,vec)
    # rotate nac2loc to get it in topolike frame and later compute azimuth/elevation
    nac2loc_topo = sp.mxv(pointing_mtx,nac2loc)
    # compute distance, azimuth, elevation of cell wrt nacelle
    (loc_dist,loc_az,loc_el) = sp.recrad(nac2loc_topo)
    loc_dist = loc_dist *u.km
    loc_az = ((-loc_az)%(2.0*np.pi))*u.rad
    loc_el = loc_el*u.rad            
    
    # convert location at top of atmosphere into EarthLocation object
    eloc_top = EarthLocation.from_geocentric(loc_top[0],loc_top[1],loc_top[2],unit='km')
    # create vectors for the 4 corners of the cell
    crn_vec = get4corners(az,el,step_grid.to('rad')/2.0,step_grid.to('rad')/2.0)
    elocs_crn = []
    # get location of 4 corners with top of atmosphere
    for crn in crn_vec:
        crn_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',eye_name,topo_frame,np.array(crn))
        eloc_crn = EarthLocation.from_geocentric(crn_top[0],crn_top[1],crn_top[2],unit='km')
        elocs_crn.append(eloc_crn)
    # get radius of the Earth at center of corner latitude
    R_earth = utils.latrad(eloc_top.lat.to('rad').value)
    # compute theoretical area covered by the fov
    area = utils.elocs42area(elocs_crn[0],elocs_crn[1],elocs_crn[2],elocs_crn[3],r=R_earth)
    # store data for given nacelle into a QTable
    grid_vec = make_grid_table(time,eye_name,eloc_top ,elocs_crn,loc_az,loc_el,loc_dist,area,Lgnd)
    # vstack grid created for vector vec and grid_data
    if grid_data is None:
        grid_data = grid_vec
    else:
        grid_data = vstack((grid_data,grid_vec))
# compute reduced area. data are stored in self.grid_data
redarea_data,area_total,area_reduced_total = compute_reduced_area(time,eye_name,nac_eloc,grid_data)


# create a mesh of angles