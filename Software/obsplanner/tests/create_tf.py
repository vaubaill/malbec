"""
create_tf.py
Goal:
- make frame spice kernel (= a text file) for MALBEC nacelle

NOTE:
    - creation of tf-file works only for 9 nacelles due to the way the
       nacelle name is created (with only considering one last digit)

Authors:
A. Rietze, Univ. Oldenburg, Germany
J. Vaubaillon, IMCCE, 2020, MALBEC project
"""

# --------------------------------------------------------------------------------
# import all necessary tools
# --------------------------------------------------------------------------------
import os
import logging
import spiceypy as sp

# set home directory
home = os.getenv('HOME') + '/'

# --------------------------------------------------------------------------------
# create default logger object
# --------------------------------------------------------------------------------
# log sub-directory
log_dir = home + 'LOG/'
# logging file name root
log_file_root = log_dir + 'create_ik-YYYYMMDDTHHMMSS.log'

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_tf(kernel_path, nacelle_dir):
    """
    Function to create a tf-file for the MALBEC nacelle.
    For that the spk file of a specific nacelle at specific launch site is loaded.
    The SPICE ID of the nacelle is retrieved from this spk file.
    The corresponding SPICE name is created in this routine to make the link between
    the ID and name in the tf-file.
    The created tf-file is saved in out_tf_file.
    out_tf_file: path to output directory where the tf-file is going to be saved, including
                the name for the output tf-file
                example: path_to_output_directory + 'nacelle1.tf'
                        OR: out_tf_file = kernel_path + '/' + nacelle_name + '.tf'
    NOTE:
    - creation of tf-file works only for 9 nacelles due to the way the
       nacelle name is created (with only considering one last digit)

    Parameters:
    -----------
    kernel_path:    path to SPICE standard kernels; must include input standard kernel
                    'standard.ker'.
                    example: kernel_path = '/astrodata/kernels/'

    nacelle_dir:    path to input directory with nacelle & launch site specific input
                    (spk-file) directory in order to retrieve the necessary SPICE ID
                    example:
                    nacelle_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'

    Returns:
    ------------
    out_tf_file:    String. Full path to output tf-file which is created in this function.
                    example: path_to_output_directory + 'nacelle1.tf'
                        OR: out_tf_file = kernel_path + '/' + nacelle_name + '.tf'
    """

    # set input standard kernel path
    input_kernel_file = kernel_path + 'standard.ker'
    # load standard kernel
    sp.furnsh(input_kernel_file)

    # load nacelle spk file
    spk_file = nacelle_dir + 'trajectory.spk'
    sp.furnsh(spk_file)

    # get nacelle SPICE id from spk file
    nacelle_id = sp.spkobj(spk_file)[0]

    # getting the last digit (= nacelle_number) of the nacelle SPICE ID to create the corresponding
    # nacelle SPICE name
    # NOTE: works only for the first 9 nacelles!
    # example1: nacelle_id = -1399001 -> the nacelle_number is then: 1
    nacelle_number = str(nacelle_id)[-1:]

    # using the nacelle_number to create corresponding SPICE name
    # in case of example1: nacelle_name = MALBEC_NACELLE1
    nacelle_name = 'MALBEC_NACELLE' + nacelle_number

    # set SPICE Frame name
    nacelle_frame = nacelle_name + '_FRAME'

    # set SPICE Frame ID
    nacelle_frame_id = str(nacelle_id).replace(str(-1399), str(5399))

    # Set output path for tf-file for nacelle
    out_tf_file = kernel_path + '/' + nacelle_name + '.tf'

    # checking if output ik-file already exists
    # delete previously created malbec/nacelle kernel files
    if os.path.exists(out_tf_file):
        os.remove(out_tf_file)

    # ====================================================
    # now make frame kernel for MALBEC station camera on nacelle
    out = open(out_tf_file, 'w')
    out.write("KPL/FK\n\n")
    out.write("MALBEC Frame Kernel\n")
    out.write("===============================================================================\n")
    out.write("\n")
    out.write("     This frame kernel contains the orientation of the MALBEC camera of\n")
    out.write("     " + repr(nacelle_name) + ".\n")
    out.write("\n")
    out.write("Version and Date\n")
    out.write("-------------------------------------------------------------------------------\n")
    out.write("\n")
    out.write("     Version 1.0 -- May 11, 2020 - Jeremie Vaubaillon, Athleen Rietze\n")
    out.write("\n")
    out.write(" MALBEC Frame\n")
    out.write("-------------------------------------------------------------------------------\n")
    out.write("\n")
    out.write("The following MALBEC frame is defined in this kernel file:\n")
    out.write("\n")
    out.write("         Name                  Relative to           Type       NAIF ID\n")
    out.write("     ======================  ===================  ============   =======\n")
    out.write("     Camera Frame:\n")
    out.write("     -----------------\n")
    out.write("   " + repr(nacelle_frame) +"       rel.to J2000         CK         " + str(nacelle_frame_id)+"\n")
    out.write("\n")
    out.write("MALBEC_NACELLE1 NAIF ID Codes -- Definition Section\n")
    out.write("========================================================================\n")
    out.write("\n")
    out.write("   This section contains name to NAIF ID mappings for the MALBEC\n")
    out.write("   Nacelle1. Once the contents of this file is loaded into the\n")
    out.write("   KERNEL POOL, these mappings become available within SPICE, \n")
    out.write("   making it possible to use names instead of ID code in the high\n")
    out.write("   level SPICE routine calls.\n\n")
    out.write("  Spacecraft and its structures: \n")
    out.write("   ------------------------------\n\n")
    out.write("      " + repr(nacelle_name) + "         " + str(nacelle_id) + "\n\n")
    out.write("   Science Instruments:\n")
    out.write("   --------------------\n\n")
    out.write("      " + repr(nacelle_frame) + "    " + str(nacelle_frame_id) + "\n\n")
    out.write("   The mappings summarized in this table are implemented by the keywords\n")
    out.write("   below.\n\n")
    out.write("\\begindata\n\n")
    out.write("    NAIF_BODY_NAME += (" + repr(nacelle_name) + ")\n")
    out.write("    NAIF_BODY_CODE += (" + str(nacelle_id) + ")\n\n")
    out.write("    NAIF_BODY_NAME += (" + repr(nacelle_frame) + ")\n")
    out.write("    NAIF_BODY_CODE += (" + str(nacelle_frame_id) + ")\n\n")
    out.write("\\begintext\n\n")
    out.write("\\begindata\n\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_ID             = " + str(nacelle_frame_id) + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_NAME           = " + repr(nacelle_frame) + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_CLASS          = 5" + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_CLASS_ID       = " + str(nacelle_id) + '001' + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_CENTER         = " + str(nacelle_id) + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_RELATIVE       = " + repr('J2000') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_DEF_STYLE      = " + repr('PARAMETERIZED') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_FAMILY         = " + repr('TWO-VECTOR') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_AXIS       = " + repr('Z') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_VECTOR_DEF = " + repr('OBSERVER_TARGET_POSITION') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_OBSERVER   = " + repr('EARTH') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_TARGET     = " + str(nacelle_id) + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_PRI_ABCORR     = " + repr('NONE') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_AXIS       = " + repr('X') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_VECTOR_DEF = " + repr('CONSTANT') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_FRAME      = " + repr('ITRF93') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_SPEC       = " + repr('RECTANGULAR') + "\n")
    out.write("  FRAME_" + str(nacelle_frame_id) + "_SEC_VECTOR     = (" + str(0) + ", " + str(0) + ", " + str(1) + ")\n\n")
    out.write("\\begintext\n")
    out.close()
    log.info("data saved in " + out_tf_file)
    return out_tf_file


# -----------------------------------------------------------------------------
# Define some variables
# -----------------------------------------------------------------------------
def main():
    # Define some input values
    # SPICE default kernel path
    kernel_path = '/astrodata/kernels/'

    # set path to nacelle & launch site specific input (spk-file) directory
    # in order to retrieve the necessary SPICE IDs
    nacelle_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'

    # Call create_tf function to create tf-file for nacelle
    tf_file = create_tf(kernel_path, nacelle_dir)


# =============================================================================
if __name__ == '__main__':
    main()