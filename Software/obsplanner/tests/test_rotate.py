import os
import logging
import numpy as np
import spiceypy as sp
import astropy.units as u

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

"""
# 2-5_-45 S
brsgt_vec = [-0.58711141  0.14836409  0.79579413] # ok
brsgt_vec = [-0.14836409 -0.58711141  0.79579413] # pas ok
# 2-5_45 N
brsgt_vec = [ 0.14836409  0.58711141  0.79579413] # pas ok
brsgt_vec = [ 0.58711141 -0.14836409  0.79579413] # ok
# 5-2_-45 N
brsgt_vec = [ 0.58929796 -0.14284906  0.79518681] # ok
brsgt_vec = [ 0.14284906  0.58929796  0.79518681] # pas ok
# 5-2_45 S
brsgt_vec = [-0.14284906 -0.58929796  0.79518681] # pas ok
brsgt_vec = [-0.58929796  0.14284906  0.79518681] # ok
"""

fov1 = 25.2126195882407 * u.deg
fov2 = 14.0362434679264 * u.deg

angles1 = [ fov1,
            fov1,
           -fov1,
           -fov1]
angles2 = [ fov2,
           -fov2,
           -fov2,
            fov2]
brsgt_vec_l = [[-0.58711141,  0.14836409,  0.79579413],
             [-0.14836409, -0.58711141,  0.79579413],
             [ 0.14836409,  0.58711141,  0.79579413],
             [ 0.58711141, -0.14836409,  0.79579413],
             [ 0.58929796, -0.14284906,  0.79518681],
             [ 0.14284906,  0.58929796,  0.79518681],
             [-0.14284906, -0.58929796,  0.79518681],
             [-0.58929796,  0.14284906,  0.79518681]]
ok_l = ['ok',
        'pas ok',
        'pas ok',
        'ok',
        'ok',
        'pas ok',
        'pas ok',
        'ok']

for (brsgt_vec,ok) in zip(brsgt_vec_l,ok_l):
    log.debug('brsgt_vec: '+str(brsgt_vec))
    log.debug('angle1: '+str(fov1))
    corner_tmp = sp.rotvec(brsgt_vec,fov1.to('rad').value,3)
    log.debug('corner_tmp: '+str(corner_tmp))
    sep_tmp = sp.vsep(brsgt_vec,corner_tmp) * u.rad
    log.debug('* sep_tmp: '+str(sep_tmp.to('deg')))
    log.debug('angle2: '+str(fov2))
    corner_vec = sp.rotvec(corner_tmp,fov2.to('rad').value,2)
    log.debug('corner_vec: '+str(corner_vec))
    sep_vec = sp.vsep(brsgt_vec,corner_vec) * u.rad
    log.debug('** sep_vec: '+str(sep_vec.to('deg')))
    log.debug('----------------'+ok)
log.debug('====================')


for (brsgt_vec,ok) in zip(brsgt_vec_l,ok_l):
    log.debug('brsgt_vec: '+str(brsgt_vec))
    log.debug('angle1: '+str(fov1))
    corner_tmp = sp.vrotv(brsgt_vec,np.array([0,0,1]),fov1.to('rad').value)
    log.debug('corner_tmp: '+str(corner_tmp))
    sep_tmp = sp.vsep(brsgt_vec,corner_tmp) * u.rad
    log.debug('* sep_tmp: '+str(sep_tmp.to('deg')))
    log.debug('angle2: '+str(fov2))
    corner_vec = sp.vrotv(corner_tmp,np.array([0,1,0]),fov2.to('rad').value)
    log.debug('corner_vec: '+str(corner_vec))
    sep_vec = sp.vsep(brsgt_vec,corner_vec) * u.rad
    log.debug('** sep_vec: '+str(sep_vec.to('deg')))
    log.debug('----------------'+ok)











