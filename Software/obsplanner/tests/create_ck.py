""" Routine to create ck_files with SPICE
    create_ck.py


    _________
    INPUT/DEPENDENCIES:
    - SPICE toolkits: msopck (to convert text file containing orientation data to a CK)
    - base reference frame:
        [predefined local frame
        vertical, horizontal and West direction (similar to cabernet.fk)]

    ___________
    To change before running:
    - kernel_path
    - spice_exe_path
    - output_path
    - input_file_name
    To change name of nacelle:
    - frame_file_path
    - output_path
    - change malbec_nac1_fake.tsc/malbec_nac2_fake.tsc (2x: in MAKE_FAKE_SCLK & when checking if it already exists)
    - INTERNAL_FILE_NAME
    - CK_SEGMENT_ID
    - INSTRUMENT_ID
    - FRAMES_FILE_NAME
    _________
    Output: ck-file
    - "Orientation data": .quaternions from which orientation matrices are formed by SPICE softtware
                          .matrices are used to rotate position vectors from a base reference frame
                           ("from" frame) into a second reference frame ("to" frame) -> C-Matrix
    - optionally: angular velocity vectors of "to" frame with respect to "from" frame (expressed
                  relative to "from" frame)

    Authors
    -------------
    J. Vaubaillon - IMCCE
    A. Rietze - U. Oldenburg, Germany

    -------------
    Version
    1.0

    """
#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import os
import logging
import shutil
import spiceypy as sp

home = os.getenv('HOME') + '/'

#--------------------------------------------------------------------------------
# setup logging
#--------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_ck(kernel_path, spice_exe_path, nacelle_dir, frame_file):
    """
    Function to create ck kernel for a specific nacelle depending on launch site (see nacelle _dir).
    The created ck is first saved in the temporary directory due to character limitation reasons
    and is then copied from there into a sub directory called "ck_kernels" in the nacelle_dir.

    Parameters:
    -------------
    kernel_path:    String. Path to SPICE standard kernels; must include input standard kernel
                    'standard.ker'.
                    example: kernel_path = '/astrodata/kernels/'

    spice_exe_path: String. SPICE kernel executables directory full name.
                    Example: spice_exe_path = '/astrodata/exe/'

    nacelle_dir:    path to input directory with nacelle & launch site specific input
                    (spk-file) directory in order to retrieve the necessary SPICE ID
                    example:
                    nacelle_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'

    frame_file:     path to input directory (including name of file) with the associated frame file
                    (tf-file) of the nacelle for the respective spk-file.
                    This file is needed in order to make the link between the SPICE ID and
                    corresponding SPICE name which enables us to retrieve the correct SPICE name
                    of the analyzed nacelle.

    :return:
    """

    # load required standard kernels
    kernel_file = kernel_path+'standard.ker'
    sp.furnsh(kernel_file)

    # set temporary directory where files are created
    # This is useful if the path to the files are very long, since it causes the mkspk utility program to crash.
    # Temporary files (e.g. trajectory in ascii format) andoutput files are first
    # created in tmpdir directory, and then the output is saved in spk_file. Default is '/tmp/'.
    tmp_dir = '/tmp/'

    # create output directory/ set output path
    # has to be adjusted according to chosen nacelle and site
    # checking if output directory already exists
    out_dir = nacelle_dir + 'ck_kernels/'
    try:
        os.makedirs(out_dir)
    except FileExistsError:
        log.info('Directory ' + out_dir + ' already exists.')

    # load frame file (tf-file)
    sp.furnsh(frame_file)

    # load nacelle spk file
    spk_file = nacelle_dir + 'trajectory.spk'
    sp.furnsh(spk_file)
    # get nacelle SPICE names, id and spk coverage from spk file
    nacelle_id = sp.spkobj(spk_file)[0]
    nacelle_name = sp.bodc2n(nacelle_id)
    cov = sp.spkcov(spk_file,nacelle_id)
    start,stop = sp.wnfetd(cov,0)
    log.info("start: "+sp.et2utc(start,"ISOC",2,50))
    log.info("stop: "+sp.et2utc(stop,"ISOC",2,50))
    # output spacecraft kernel file name
    ck_file = os.path.basename(frame_file).replace('.tf','.ck')
    output_ck_file = tmp_dir + ck_file

    # output fake clock kernel file name
    tsc_file = output_ck_file.replace('.ck','_fake.tsc')

    # input data file name, must exist
    input_file = out_dir + 'Input_create_ck.dat'

    # making input file
    with open(input_file,'w') as out:
        while start < stop:
            out.write(sp.et2utc(start,"ISOC",2,50)+' 0.0 0.0 0.0'+"\n")
            start = start + 1
        out.write(sp.et2utc(stop,"ISOC",2,50)+' 0.0 0.0 0.0')
    log.info("input data saved in:  "+input_file)

    # setup file name, must exist
    setup_file = output_ck_file.replace('.ck','.setup')

    # double check that no CK, setup or TSC (fake sclk) file is going to be erased
    # remove fake sclk/ck files if they already exist
    if os.path.exists(output_ck_file):
        log.debug('removing existing ck file '+output_ck_file)
        os.remove(output_ck_file)
    if os.path.exists(tsc_file):
        log.debug('removing existing tsc file '+tsc_file)
        os.remove(tsc_file)
    if os.path.exists(setup_file):
        log.debug('removing existing setup file '+setup_file)
        os.remove(setup_file)


    # logging the setup, ck and tsc file
    log.debug('ck_file: ' + frame_file)
    log.debug('tsc_file: ' + tsc_file)
    log.debug('setup_file: ' + setup_file)

    # make the intermediate setup file
    with open(setup_file,'w') as out:
        out.write("\\begindata \n")
        # setup data for testing with nacelle data
        # -------------------------------------------------------------------------------
        out.write("   LSK_FILE_NAME   = "+repr(kernel_path+'naif0012.tls')+"\n")
        out.write("   MAKE_FAKE_SCLK  = "+repr(tsc_file)+"\n")
        out.write("   FRAMES_FILE_NAME= "+repr(frame_file)+"\n")
        out.write("   INTERNAL_FILE_NAME     = " + repr('CK file for ' + str(nacelle_name)) + "\n")
        out.write("   CK_TYPE                = 3 \n")
        out.write("   CK_SEGMENT_ID          = "+repr('CK file for' + str(nacelle_name))+"\n")
        out.write("   INSTRUMENT_ID          = "+str(nacelle_id)+"\n")
        out.write("   REFERENCE_FRAME_NAME   = "+repr('ITRF93')+"\n")
        out.write("   ANGULAR_RATE_PRESENT   = "+repr('NO')+"\n")
        out.write("   MAXIMUM_VALID_INTERVAL = 60\n")
        # out.write("   TIME_CORRECTION        = time bias, seconds
        out.write("   INPUT_TIME_TYPE        = "+repr('UTC')+"\n")
        out.write("   INPUT_DATA_TYPE        ="+repr('EULER ANGLES')+"\n")
        out.write("   EULER_ANGLE_UNITS      = "+repr('DEGREES')+"\n")
        out.write("   EULER_ROTATIONS_ORDER  = "+repr('X')+"," +repr('Y')+"," +repr('Z')+"\n")
        out.write("   ANGULAR_RATE_FRAME     = "+repr('REFERENCE')+"\n") #" or 'INSTRUMENT'
        out.write("   PRODUCER_ID            ="+repr('J. Vaubaillon - IMCCE, A. Rietze - U. Oldenburg')+"\n")
        out.write(" \n")
    log.info("setup saved in:  "+setup_file)

    # remove ck file if it already exists
    if os.path.exists(output_ck_file):
        os.remove(output_ck_file)

    # build the command
    cmd = spice_exe_path + "msopck " + setup_file + " " + input_file + " " + output_ck_file
    log.info("cmd="+cmd)

    # launches the cmd
    try:
        os.system(cmd)
    except Exception:
        msg = '*** FATAL ERROR: Impossible to submit the cmd' +cmd
        log.error(msg)
        raise IOError(msg)
    # double check that CK file was successfully created
    if not os.path.exists(output_ck_file):
        msg = '*** FATAL ERROR: CK file: '+output_ck_file+' was not created.'
        log.error(msg)
        raise IOError(msg)

    # now copy temporary output files (ck, tsc, setup) into out_dir
    for file2move in [setup_file,output_ck_file,tsc_file]:
        dst = out_dir + os.path.basename(file2move)
        if os.path.exists(dst):
            os.remove(dst)
        try:
            # remark: os.remove does not always work when trying to move file from one FS to another...
            shutil.copy(file2move, dst)
        except:
            msg = '*** FATAL ERROR: impossible to move ' + file2move + ' into ' + dst
            log.error(msg)
            raise IOError(msg)
    log.info("SPICE CK kernel saved in: " + output_ck_file)

    return

# -----------------------------------------------------------------------------
# Define some variables
# -----------------------------------------------------------------------------

def main():
    # --------------------------------------------------------------------------------
    # Define default input values, can be overwritten by command line parameters
    # maybe better: configuration file?
    # --------------------------------------------------------------------------------
    # set kernel path and SPICE utilities path -> better in configuration file
    # SPICE default kernel path
    kernel_path = '/astrodata/kernels/'
    # SPICE utilities path
    spice_exe_path = '/astrodata/exe/'

    # set path to nacelle & launch site specific input (spk-file) & output (ck-kernel) directory
    nacelle_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'

    # setting path to frame file (tf-file)
    frame_file = kernel_path + 'nacelle1.tf' # path to old frame_file
    # frame_file = kernel_path + 'MALBEC_NACELLE1.tf' # path to new frame file -> to be tested

    # call subroutine create_ck
    create_ck(kernel_path, spice_exe_path, nacelle_dir, frame_file)

# =============================================================================
if __name__ == '__main__':
    main()