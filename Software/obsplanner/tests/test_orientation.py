"""Test MALBEC camera orientation

"""
import logging
import spiceypy as sp
import astropy.units as u
from astropy.coordinates import SkyCoord # EarthLocation, get_moon, get_sun, AltAz, Angle, Latitude, Longitude

#--------------------------------------------------------------------------------
# setup logging
#--------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# load kernels
sp.furnsh('/astrodata/kernels/standard.ker')
datapath = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampainPlanner/MissionPlanSim1/'
# load spacecraft kernel
spk_file = datapath+'trajectory-1.spk'
sp.furnsh(spk_file)
# load spacecraft clock
sp.furnsh(datapath+'trajectory-1_fake.tsc')
# load spacecraft orientation
sp.furnsh(datapath+'trajectory-1.ck')
# load spacecrfat frame
sp.furnsh(datapath+'MALBEC_NACELLE1.tf')
# load camera frame kernel
sp.furnsh(datapath+'MALBEC_NACELLE1.tk')
# load instrument kernel
sp.furnsh(datapath+'MALBEC_NACELLE1.ik')
datapath = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampainPlanner/MissionPlanSim2/'
# load spacecraft kernel
spk_file2 = datapath+'trajectory-2.spk'
sp.furnsh(spk_file2)
# load spacecraft clock
sp.furnsh(datapath+'trajectory-2_fake.tsc')
# load spacecraft orientation
sp.furnsh(datapath+'trajectory-2.ck')
# load spacecrfat frame
sp.furnsh(datapath+'MALBEC_NACELLE2.tf')
# load camera frame kernel
sp.furnsh(datapath+'MALBEC_NACELLE2.tk')
# load instrument kernel
sp.furnsh(datapath+'MALBEC_NACELLE2.ik')

# MANUAL SETTINGS
# set nacelle name and id
nacelle_id = sp.spkobj(spk_file)[0]
nacelle_name = sp.bodc2n(nacelle_id)
nacelle_id2 = sp.spkobj(spk_file2)[0]
nacelle_name2 = sp.bodc2n(nacelle_id2)

# set time
time_utc = '2019-05-23T08:00:10.810'

# START OF PROGRAM
# get spice time
et = sp.str2et(time_utc)
# retrieve spacecraft cock id
clock_id = int(nacelle_id/1000)
# retrieve spacecraft cock time
unencoded_clockstring = sp.sce2s(clock_id,et)
# encode spacecraft clock
encoded_clock = sp.scencd(clock_id,unencoded_clockstring)
log.debug('encoded_clock: '+str(encoded_clock))
log.debug('Getting orientation of '+nacelle_name+' '+str(nacelle_id)+' at '+time_utc+' in frame J2000')
nacelle_pointing_mtx,t = sp.ckgp(nacelle_id, encoded_clock, 0.0,'J2000')
log.debug('nacelle_pointing_mtx: '+str(nacelle_pointing_mtx))
w = [1,0,0]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('r  : '+str(rlonlat[0]))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))
skycoo = SkyCoord(ra=rlonlat[1]*u.rad,dec=rlonlat[2]*u.rad,unit=u.rad)
log.debug('skycoo: '+skycoo.to_string('hmsdms'))
Eul = sp.m2eul(nacelle_pointing_mtx,3,2,1)
log.debug('Euler angles:'+str((Eul[0]*sp.dpr(),Eul[1]*sp.dpr(),Eul[2]*sp.dpr())))
w = [0,0,1]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('r  : '+str(rlonlat[0]))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))
skycoo = SkyCoord(ra=rlonlat[1]*u.rad,dec=rlonlat[2]*u.rad,unit=u.rad)
log.debug('skycoo: '+skycoo.to_string('hmsdms'))
log.debug('=======')
# get orientation in TOPO-LIKE frame
topo_frame_name = nacelle_name + '_TOPOLIKE'
log.debug('Getting orientation of '+nacelle_name+' '+str(nacelle_id)+' at '+time_utc+' in frame '+topo_frame_name)
nacelle_pointing_mtx,t = sp.ckgp(nacelle_id, encoded_clock,0.0,topo_frame_name)
log.debug('nacelle_pointing: '+str(nacelle_pointing_mtx))
Eul = sp.m2eul(nacelle_pointing_mtx,3,2,1)
log.debug('Euler angles:'+str((Eul[0]*sp.dpr(),Eul[1]*sp.dpr(),Eul[2]*sp.dpr())))
w = [1,0,0]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))
w = [0,0,1]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))


# GET position of the other nacelle in TOPO-LIKE frame
topo_frame_name = nacelle_name + '_TOPOLIKE'
log.debug('Getting position of '+nacelle_name2+' '+str(nacelle_id2)+' at '+time_utc+' in frame '+topo_frame_name)
eph,lt = sp.spkezr(nacelle_name2, et, topo_frame_name, 'NONE', nacelle_name)
log.debug('eph: '+str(eph))
# translates into angle
rlonlat = sp.reclat(eph[:3])
# get angles
log.debug('converting into reclat : ')
log.debug('r : '+str(rlonlat[1]))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))

# test transformation bewteen MALBEC_TOPOLIKE frame and ITRF93
log.debug('Getting transformation between ITRF93 and '+topo_frame_name)
mtx = sp.pxform(topo_frame_name,'ITRF93',et)
Eul = sp.m2eul(mtx,3,2,1)
log.debug('Euler angles:'+str((Eul[0]*sp.dpr(),Eul[1]*sp.dpr(),Eul[2]*sp.dpr())))
log.debug('mtx: '+str(mtx))
w = [1,0,0]
# rotates the unit vector
v = sp.mxv(mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('r  : '+str(rlonlat[0]))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))

w = [0,0,1]
# rotates the unit vector
v = sp.mxv(mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('r  : '+str(rlonlat[0]))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))











