""" Routine to create ck_files with SPICE
    create_ck.py


    _________
    INPUT/DEPENDENCIES:
    - SPICE toolkits: msopck (to convert text file containing orientation data to a CK)
    - base reference frame:
        [predefined local frame
        vertical, horizontal and West direction (similar to cabernet.fk)]

    ___________
    To change before running:
    - kernel_path
    - spice_exe_path
    - output_path
    - input_file_name
    To change name of nacelle:
    - frame_file
    - output_path
    - change malbec_nac1_fake.tsc/malbec_nac2_fake.tsc (2x: in MAKE_FAKE_SCLK & when checking if it already exists)
    - INTERNAL_FILE_NAME
    - CK_SEGMENT_ID
    - INSTRUMENT_ID
    - FRAMES_FILE_NAME
    _________
    Output: ck-file
    - "Orientation data": .quaternions from which orientation matrices are formed by SPICE softtware
                          .matrices are used to rotate position vectors from a base reference frame
                           ("from" frame) into a second reference frame ("to" frame) -> C-Matrix
    - optionally: angular velocity vectors of "to" frame with respect to "from" frame (expressed
                  relative to "from" frame)

    Authors
    -------------
    J. Vaubaillon - IMCCE
    A. Rietze - U. Oldenburg, Germany

    -------------
    Version
    1.0

    """
#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import os
import logging

home = os.getenv('HOME') + '/'

#--------------------------------------------------------------------------------
# setup logging
#--------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


#--------------------------------------------------------------------------------
# Define default input values, can be overwritten by command line parameters
# maybe better: configuration file?
#--------------------------------------------------------------------------------
# set kernel path and SPICE utilities path -> better in configuration file
# SPICE default kernel path
kernel_path = '/astrodata/kernels/'
# SPICE utilities path
spice_exe_path = '/astrodata/exe/'

# create output directory/ set output path
# has to be adjusted according to chosen nacelle and site
nacelle_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'
out_dir = nacelle_dir + 'ck_kernels/'
try:
    os.makedirs(out_dir)
except FileExistsError:
    log.debug('Directory '+out_dir+ ' already exists.')

# providing necessary file names for msopck
# nacelle frame kernel file name
frame_file = nacelle_dir + 'nacelle1.tf'

# nacelle spk file
spk_file1 = nacelle_dir + 'trajectory.spk'
sp.furnsh(spk_file)
# get nacelle SPICE names, id and spk coverage
nacelle_id = sp.spkobj(spk_file)[0]
nacelle_name = sp.bodc2n(nacelle_id)
cov = sp.spkcov(spk_file,nacelle_id)
start,stop = sp.wnfetd(cov,0)

# attitude file
attitude_file = nacelle_dir + 'attitude1.dat'

# output spacecraft kernel file name
ck_file = os.path.basename(frame_file).replace('.tf','.ck')

# output clock kernel file name
tsc_file = ck_file.replace('.tf','.tsc')

# input data file name, must exist
input_file_name = out_dir + 'Input_create_ck.dat'

# setup file name, must exist
setup_file = ck_file.replace('.ck','.setup')

# double check that no CK or TSC file is going to be erased
if os.path.exists(ck_file):
    log.debug('removing existing ck file '+ck_file)
    os.remove(ck_file)
if os.path.exists(tsc_file):
    log.debug('removing existing tsc file '+tsc_file)
    os.remove(tsc_file)

# logging
log.debug('setup_file: '+setup_file)
log.debug('ck_file: '+ck_file)
log.debug('tsc_file: '+tsc_file)

# make the attitude file 
with open(attitude_file,'w') as out:
    out.write(sp.et2utc(start,"ISOC",2,50)+' 0.0 0.0 0.0')
    out.write(sp.et2utc(stop,"ISOC",2,50)+' 0.0 0.0 0.0')
log.info("attitude data saved in:  "+attitude_file)


# make the intermediate setup file
with open(setup_file,'w') as out:
    out.write("\\begindata \n")
    # setup data for testing with nacelle data
    # -------------------------------------------------------------------------------
    out.write("   LSK_FILE_NAME   = "+repr(kernel_path+'naif0012.tls')+"\n")
    out.write("   MAKE_FAKE_SCLK  = "+repr(tsc_file)+"\n")
    out.write("   FRAMES_FILE_NAME= "+repr(frame_file)+"\n")
    #out.write("   INTERNAL_FILE_NAME     = "+repr(attitude_file)+"\n")
    out.write("   CK_TYPE                = 3 \n")
    out.write("   CK_SEGMENT_ID          = "+repr('CK file for MALBEC Nacelle1')+"\n")
    out.write("   INSTRUMENT_ID          = "+str(-1399001)+"\n")
    out.write("   REFERENCE_FRAME_NAME   = "+repr('ITRF93')+"\n")
    out.write("   ANGULAR_RATE_PRESENT   = "+repr('NO')+"\n")
    out.write("   MAXIMUM_VALID_INTERVAL = 60\n")
    # out.write("   TIME_CORRECTION        = time bias, seconds
    out.write("   INPUT_TIME_TYPE        = "+repr('UTC')+"\n")
    out.write("   INPUT_DATA_TYPE        ="+repr('EULER ANGLES')+"\n")
    out.write("   EULER_ANGLE_UNITS      = "+repr('DEGREES')+"\n")
    out.write("   EULER_ROTATIONS_ORDER  = "+repr('X')+"," +repr('Y')+"," +repr('Z')+"\n")
    out.write("   ANGULAR_RATE_FRAME     = "+repr('REFERENCE')+"\n") #" or 'INSTRUMENT'
    out.write("   PRODUCER_ID            ="+repr('J. Vaubaillon - IMCCE, A. Rietze - U. Oldenburg')+"\n")
    out.write(" \n")
log.info("setup saved in:  "+setup_file)

# remove ck file if it already exists
if os.path.exists(output_ck_file):
    os.remove(output_ck_file)

# build the command
cmd = spice_exe_path + "msopck " + setup_file + " " + attitude_file + " " + ck_file
log.info("cmd="+cmd)

# launches the cmd
try:
    os.system(cmd)
except Exception:
    msg = '*** FATAL ERROR: Impossible to submit the cmd' +cmd
    log.error(msg)
    raise IOError(msg)
# double check that CK file was successfully created
if not os.path.exists(output_ck_file):
    msg = '*** FATAL ERROR: CK file: '+output_ck_file+' was not created.'
    log.error(msg)
    raise IOError(msg)

