import simplekml
from astropy.time import Time
from astropy.coordinates import EarthLocation
import astropy.units as u


colors = {'blue': simplekml.Color.blue,
                  'red' : simplekml.Color.red,
                  'green' : simplekml.Color.green,
                  'black' : simplekml.Color.black}


time = Time('2019-05-23T08:14:06.230',scale='utc')

elocs = []
elocs.append(EarthLocation.from_geocentric(4459.59387844, -96.36718588, 4696.01547469,unit=u.km))
elocs.append(EarthLocation.from_geocentric(4481.82781485, -82.33406635, 4675.20672346,unit=u.km))
elocs.append(EarthLocation.from_geocentric(4489.49041713, -89.42890583, 4667.76778128,unit=u.km))
elocs.append(EarthLocation.from_geocentric(4468.40817354, -103.77931692, 4687.52721978,unit=u.km))
elocs.append(EarthLocation.from_geocentric(4459.59387844, -96.36718588, 4696.01547469,unit=u.km))


print('elocs[0]: '+str(elocs[0].to_geodetic()))
print('elocs[1]: '+str(elocs[1].to_geodetic()))
print('elocs[2]: '+str(elocs[2].to_geodetic()))
print('elocs[3]: '+str(elocs[3].to_geodetic()))


grid_kml = simplekml.Kml()
pol = grid_kml.newpolygon(name='test')
pol.outerboundaryis = [
        (elocs[0].lon.to('deg').value,elocs[0].lat.to('deg').value,elocs[0].height.to('m').value),
        (elocs[1].lon.to('deg').value,elocs[1].lat.to('deg').value,elocs[1].height.to('m').value),
        (elocs[2].lon.to('deg').value,elocs[2].lat.to('deg').value,elocs[2].height.to('m').value),
        (elocs[3].lon.to('deg').value,elocs[3].lat.to('deg').value,elocs[3].height.to('m').value)]
pol.style.linestyle.width = 2
pol.style.linestyle.color = colors['blue']
pol.style.polystyle.color = simplekml.Color.changealphaint(100, colors['red'])
pol.altitudemode = simplekml.AltitudeMode.absolute
pol.extrude = 0
pol.timestamp.when = time.isot
grid_kml.save('test_polygon.kml')

