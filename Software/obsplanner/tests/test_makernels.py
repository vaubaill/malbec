#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 13:29:25 2021

@author: vaubaill
"""
import os
import spiceypy as sp

# load default and site kernels
sp.furnsh('/astrodata/kernels/standard.ker')
sp.furnsh('/home/vaubaill/malbec//Software//conf//sites.bsp')
sp.furnsh('/home/vaubaill/malbec//Software//conf//sites.fk')


# 20190523
campaign = '20161214'
nacelle_num = '2'
simid='NACELLE2_NACELLE18_-80_0_acA1920-155um_6mm-1.2'
#simid='NACELLE2_NACELLE5_-80_0_alpha7s_24mm-1.5'

MPpth = '/home/vaubaill/malbec//DATA/'+campaign+'/CampaignPlanner/'
spk_file= MPpth+'/MissionPlanSim'+nacelle_num+'/trajectory-'+nacelle_num+'.spk'
tk_file= MPpth+'/MissionPlanSim'+nacelle_num+'/MALBEC_NACELLE'+nacelle_num+'.tk'
sp.furnsh(spk_file)
sp.furnsh(tk_file)
print('Loading: '+spk_file)
print('Loading: '+tk_file)

nacelle_id = sp.spkobj(spk_file)[0]
cov = sp.spkcov(spk_file,nacelle_id)
start,stop = sp.wnfetd(cov,0)
print('coverage: start: '+sp.et2utc(start,'ISOC',2,50))
print('coverage: stop: '+sp.et2utc(stop,'ISOC',2,50))
et = (stop + start) /2.0

OPpth='/home/vaubaill/malbec//DATA/'+campaign+'/ObsPlan/'+simid+'/'
ckfile = OPpth+'/MALBEC_NACELLE'+nacelle_num+'.ck'
tffile = OPpth+'/MALBEC_NACELLE'+nacelle_num+'.tf'
tscfile = OPpth+'/MALBEC_NACELLE'+nacelle_num+'_fake.tsc'

print('Loading: '+tffile)
print('Loading: '+tscfile)
print('Loading: '+ckfile)
print('Loading: '+OPpth+'/atmosphere.ker')
print('Loading: '+OPpth+'/atmosphere.spk')
sp.furnsh(tffile)
sp.furnsh(tscfile)
sp.furnsh(ckfile)
#sp.furnsh(OPpth+'/atmosphere.ker')
#sp.furnsh(OPpth+'/atmosphere.spk')

ckid = sp.ckobj(ckfile)[0]
print('ckid: ',ckid)
# get coverage of ck kernel
cov = sp.ckcov(ckfile,ckid,False,'SEGMENT',1.,'TDB')
ck_start,ck_stop = sp.wnfetd(cov,0)
print('coverage: ck_start: '+sp.et2utc(ck_start,'ISOC',2,50))
print('coverage: ck_stop: '+sp.et2utc(ck_stop,'ISOC',2,50))

# note: ck is expressed in MALBEC_NACELLE'+nacelle_num+'_TOPOLIKE' frame

instrument_frame_name = 'MALBEC_NACELLE'+nacelle_num+'_NACORI'
#nacelle_topoframe_name = 'ITRF93'
#nacelle_topoframe_name = 'SITE'+nacelle_num+'_TOPO'
nacelle_topoframe_name=  'MALBEC_NACELLE'+nacelle_num+'_TOPOLIKE' # ok
#nacelle_topoframe_name = 'MALBEC_NACELLE'+nacelle_num+'_NACORI'

print('getting orientation at et: '+sp.et2utc(et,'ISOC',2,50),' from '+instrument_frame_name+' to '+nacelle_topoframe_name)


pointing_mtx = sp.pxform(instrument_frame_name,nacelle_topoframe_name,et)
print(pointing_mtx)