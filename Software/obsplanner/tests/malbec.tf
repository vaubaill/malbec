KPL/FK

MALBEC Frame Kernel
===============================================================================

   This frame kernel contains the orientation of the MALBEC camera.

Version and Date
-------------------------------------------------------------------------------

   Version 1.0 -- March 30, 2020 - Jeremie Vaubaillon, Athleen Rietze

References
-------------------------------------------------------------------------------

   1. https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/frames.html
   2. https://naif.jpl.nasa.gov/pub/naif/self_training/individual_docs/B14_dynamic_frames.pdf
   3. apollo15.0001.tf

MALBEC Frame
-------------------------------------------------------------------------------

   The following MALBEC frame is defined in this kernel file:

           Name                  Relative to           Type       NAIF ID
      ======================  ===================  ============   =======

   Camera frame:
   -----------------
      MALBEC_ZENITH          rel.to J2000         CK             5399001


MALBEC Frame Hierarchy
-------------------------------------------------------------------------------

   The diagram below shows MALBEC frames hierarchy:


                               "J2000" INERTIAL
        +-----------------------+
        |                       |                  
        | <--pck                | <--ck    
        |                       |          
        V                       |          
    "IAU_EARTH"     	        |         
    -----------         	    |          
                                |
                                |
                                V
                         "MALBEC_ZENITH"                       

MALBEC NAIF ID Codes -- Definition Section
========================================================================

   This section contains name to NAIF ID mappings for the MALBEC. Once the
   contents of this file is loaded into the KERNEL POOL, these mappings
   become available within SPICE, making it possible to use names
   instead of ID code in the high level SPICE routine calls.

   Spacecraft and its structures: 
   ------------------------------

      MALBEC                   -1399001

   Science Instruments:
   --------------------

      MALBEC_ZENITH            5399001

   The mappings summarized in this table are implemented by the keywords 
   below.

   \begindata

      NAIF_BODY_NAME += ( 'MALBEC'               )
      NAIF_BODY_CODE += ( -1399001               )

      NAIF_BODY_NAME += ( 'MALBEC_ZENITH'           )
      NAIF_BODY_CODE += (  5399001                  )

\begintext

Definition of the nadir frame
-----------------------------
  All vectors are geometric: no aberration corrections are used.

  The +Z axis points from the center of the Earth to the spacecrat.

  The component of the inertially referenced spacecraft velocity vector 
  orthogonal to +Z is aligned with the +X axis.
  
 The +Y axis is the cross product of the +Z axis and the +X axis.

Associated Kernels
------------------

  In order to use this kernel, the following kernels must
  be loaded:

      - An SPK file giving the position of MALBEC
        spacecraft with respect to the Earth

  For most geometry computations, the following additional kernels
  will be needed:

      - A planetary ephemeris SPK, for example de421.bsp

      - A leapseconds kernel

Restrictions
------------

Frame specification data
------------------------

\begindata

  FRAME_MALBEC_ZENITH          =  5399001
  FRAME_5399001_NAME           = 'MALBEC_ZENITH'
  FRAME_5399001_CLASS          = 5
  FRAME_5399001_CLASS_ID       = -1399001001
  FRAME_5399001_CENTER         = -1399001
  FRAME_5399001_RELATIVE       = 'J2000'
  FRAME_5399001_DEF_STYLE      = 'PARAMETERIZED'
  FRAME_5399001_FAMILY         = 'TWO-VECTOR'
  FRAME_5399001_PRI_AXIS       = 'Z'
  FRAME_5399001_PRI_VECTOR_DEF = 'OBSERVER_TARGET_POSITION'
  FRAME_5399001_PRI_OBSERVER   = 'EARTH'
  FRAME_5399001_PRI_TARGET     = -1399001
  FRAME_5399001_PRI_ABCORR     = 'NONE'
  FRAME_5399001_SEC_AXIS       = 'X'
  FRAME_5399001_SEC_VECTOR_DEF = 'OBSERVER_TARGET_VELOCITY'
  FRAME_5399001_SEC_OBSERVER   = 'EARTH'
  FRAME_5399001_SEC_TARGET     = -1399001
  FRAME_5399001_SEC_ABCORR     = 'NONE'
  FRAME_5399001_SEC_FRAME      = 'J2000'

\begintext
    
       
