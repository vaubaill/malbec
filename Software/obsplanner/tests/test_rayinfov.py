import logging
import simplekml
import numpy as np
import spiceypy as sp
import astropy.units as u
from astropy.time import Time as Time
from astropy.coordinates import EarthLocation, get_moon, get_sun, AltAz, SkyCoord, Angle, Latitude, Longitude, ICRS

from obsplanner.create_ik import make_camera_name

#--------------------------------------------------------------------------------
# setup logging
#--------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)
# load kernels
sp.furnsh('/astrodata/kernels/standard.ker')

misplan_path1 = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampaignPlanner/MissionPlanSim2/'
misplan_path2 = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampaignPlanner/MissionPlanSim5/'
obsplan_path = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/ObsPlan/NACELLE2_NACELLE5_0/'
# load spacecraft kernel
spk_file1 = misplan_path1+'trajectory-2.spk'
sp.furnsh(spk_file1)
# load spacecrfat frame
sp.furnsh(misplan_path1+'MALBEC_NACELLE2.tf')
# load camera frame kernel
sp.furnsh(misplan_path1+'MALBEC_NACELLE2.tk')
# load instrument kernel
sp.furnsh(misplan_path1+'MALBEC_NACELLE2.ik')
# load spacecraft clock
sp.furnsh(obsplan_path+'MALBEC_NACELLE2_fake.tsc')
# load spacecraft orientation
sp.furnsh(obsplan_path+'MALBEC_NACELLE2.ck')
# load spacecraft kernel
spk_file2 = misplan_path2+'trajectory-5.spk'
sp.furnsh(spk_file2)
# load spacecrfat frame
sp.furnsh(misplan_path2+'MALBEC_NACELLE5.tf')
# load camera frame kernel
sp.furnsh(misplan_path2+'MALBEC_NACELLE5.tk')
# load instrument kernel
sp.furnsh(misplan_path2+'MALBEC_NACELLE5.ik')
# load spacecraft clock
sp.furnsh(obsplan_path+'MALBEC_NACELLE5_fake.tsc')
# load spacecraft orientation
sp.furnsh(obsplan_path+'MALBEC_NACELLE5.ck')

# load atmosphere
sp.furnsh(obsplan_path+'atmosphere.ker')
sp.furnsh(obsplan_path+'atmosphere.spk')

#================================================
# set time manually
time_utc = '2019-05-23T08:30:10.810'
et = sp.str2et(time_utc)

for ra in np.arange(360):
    for dec in np.arange(180):
        raydir = sp.radrec( 1.0, ra*sp.rpd(), (-90+dec)*sp.rpd())
        infov = sp.fovray('MALBEC_NACELLE2_CAMERA', raydir, 'ITRF93', 'NONE', 'MALBEC_NACELLE2', et)
        if infov:
            log.debug('ra,dec: '+str(ra)+' '+str((-90+dec)))
            log.debug('raydir: '+str(raydir))
            log.debug('infov: '+str(infov))
        
