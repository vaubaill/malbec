\begintext 
  Definition of the pointing direction and FOV of MALBEC cameras instruments. 

  Given the definition of the SPICE topographic frame,
  the spice azimuth is defined from the North towards the West. 
  Now the MALBEC azimuth is defined from the North towards the East. 
  Therefore the pointing azimuth is set to negative to take this effect into account. 

  The FOV is rectangular and has 40x27 deg FOV,
   
  Author: J. Vaubaillon - IMCCE - 2020 
  MALBEC project 


Definition of the MALBEC FOV: 
 
\begindata 
INS-399202002_NAME = 'Guzet_MALBEC_CAMERA' 
INS-399202002_FOV_FRAME = 'Guzet_TOPO' 
INS-399202002_FOV_SHAPE = 'RECTANGLE' 
INS-399202002_BORESIGHT = ( -1.46236881864,   4.38224884065 ,  8.86889917278 ) 
INS-399202002_FOV_BOUNDARY = (  0.12464019  6.56093068  7.54577056 
                               -2.87299456  5.5882504   7.77929044 
                               -2.87299456  1.44744361  9.46841113 
                                0.12464019  2.42012389  9.701931   ) 
INS-399202002_FOV_CLASS_SPEC = 'ANGLES' 
INS-399202002_FOV_REF_VECTOR = ( 4.38224884065,   1.46236881864 ,  8.86889917278 )  
INS-399202002_FOV_REF_ANGLE =  13.5 
INS-399202002_FOV_CROSS_ANGLE =  20.0 
INS-399202002_FOV_REF_ANGLE =  20.0 
INS-399202002_FOV_CROSS_ANGLE =  13.5 
INS-399202002_FOV_ANGLE_UNITS =  'DEGREES' 
