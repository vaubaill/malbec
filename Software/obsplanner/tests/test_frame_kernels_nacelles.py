"""
    file: test_frame_kernels_nacelles.py
    test routine to check if it is possible to always define the secondary axis when the secondary
    axis points towards the other nacelle



    Authors
    --------
    J. Vaubaillon - IMCCE
    A. Rietze - U. Oldenburg, Germany
"""

#--------------------------------------------------------------------------------
# import all necessary tools
#--------------------------------------------------------------------------------
import spiceypy as sp
import logging
import os
from astropy.time import Time
import astropy.units as u
from astropy.coordinates import EarthLocation, Longitude, Latitude, Angle, SkyCoord, AltAz

# set home directory
home = os.getenv('HOME')+'/'
#--------------------------------------------------------------------------------
# Define default input values, can be overwritten by command line parameters
# maybe better: configuration file?
#--------------------------------------------------------------------------------
# set kernel path and SPICE utilities path -> better in configuration file
# SPICE default kernel path
kernel_path = '/astrodata/kernels/'
# SPICE utilities path
spice_exe_path = '/astrodata/exe/'


#--------------------------------------------------------------------------------
# create default logger object
#--------------------------------------------------------------------------------
# log sub-directory
log_dir = home + 'LOG/'
# logging file name root
log_file_root = log_dir+'Test_FK_Nac-YYYYMMDDTHHMMSS.log'

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


#--------------------------------------------------------------------------------
# start test routine
#--------------------------------------------------------------------------------
# load required pck kernel & leapsecondskernel in standard kernels
kernel_file = kernel_path+'standard.ker'
sp.furnsh(kernel_file)

# load MALBEC stations kernel and frame kernels
malbec_site_kernel = kernel_path + 'malbec_site.bsp'
# malbec_site_kernel = kernel_path + 'malbec_site_modified.bsp'
sp.furnsh(malbec_site_kernel)
malbec_site_frame_kernel = kernel_path + 'malbec_site.fk'
# malbec_site_frame_kernel = kernel_path + 'malbec_site_modified.fk'
sp.furnsh(malbec_site_frame_kernel)

# set up paths
data_dir1 = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/'
data_dir2 = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac2_sit2/'

# load nacelle spk kernels
spk_file1 = data_dir1 + 'trajectory.spk'
spk_file2 = data_dir2 + 'trajectory.spk'
sp.furnsh(spk_file1)
sp.furnsh(spk_file2)

# load nacelle fk kernels
tf_file1 = data_dir1 + 'nacelle1.tf'
tf_file2 = data_dir2 + 'nacelle2.tf'
sp.furnsh(tf_file1)
sp.furnsh(tf_file2)

# get nacelle SPICE IDs
nacelle1_id = sp.spkobj(spk_file1)[0]
nacelle2_id = sp.spkobj(spk_file2)[0]
# set nacelle SPICE names
nacelle1_name = sp.bodc2n(nacelle1_id)
nacelle2_name = sp.bodc2n(nacelle2_id)

log.info('nacelle1 name: '+nacelle1_name+' id: '+str(nacelle1_id))
log.info('nacelle2 name: '+nacelle2_name+' id: '+str(nacelle2_id))

# get time window coverage for each nacelle
cov1 = sp.spkcov(spk_file1,nacelle1_id)
cov2 = sp.spkcov(spk_file2,nacelle2_id)

# test/make sure that there is indeed a window coverage for both nacelles
start1,stop1 = sp.wnfetd(cov1,0)
start2,stop2 = sp.wnfetd(cov2,0)
log.info('cov1: START UTC: '+sp.et2utc(start1,"ISOC",2,50)+' STOP:'+sp.et2utc(stop1,"ISOC",2,50))
log.info('cov2: START UTC: '+sp.et2utc(start2,"ISOC",2,50)+' STOP:'+sp.et2utc(stop1,"ISOC",2,50))

# compute the time window intersection in order to guarantee that the 2 nacelle
# always exist when we query the frame kernel
cov = sp.wnintd(cov1,cov2)

# get time window boundaries
start,stop = sp.wnfetd(cov,0)
log.info('cov: STAR UTC: '+sp.et2utc(start,"ISOC",2,50)+'STOP:'+sp.et2utc(stop,"ISOC",2,50))

# test: get ephemeris of nacelle2 in nacelle1 reference frame -> if everything goes well,
# one coordinate should be = 0
# first set the time
et = start

# position of nacelle 1 wrt nacelle 2 in ITRF93 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE1',et,'ITRF93','NONE','MALBEC_NACELLE2')
log.info('ITRF93 et='+sp.et2utc(et,"ISOC",2,50)+' nac1 wrt NAC2'+str(eph2_1))

# position of nacelle 1 wrt site 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE1',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' nac1 wrt SITE1'+str(eph2_1))

# position of nacelle 2 wrt site 2 in site 2 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'SITE2_TOPO','NONE','SITE2')
log.info('TOPO2 et='+sp.et2utc(et,"ISOC",2,50)+' nac2 wrt SITE2'+str(eph2_1))

# tests
(eph2_1,lt) = sp.spkgps(399202,et,'SITE1_TOPO',399201)
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE2 GPS wrt SITE1'+str(eph2_1))

# get SITE1 in ITRF and convert into Earthlocation
(eph2_1,lt) = sp.spkezr('SITE1',et,'ITRF93','NONE','EARTH')
eloc = EarthLocation.from_geocentric(eph2_1[0],eph2_1[1],eph2_1[2],unit=u.km)
print(eloc.lon,eloc.lat,eloc.height)
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE1',et,'ITRF93','NONE','EARTH')
eloc = EarthLocation.from_geocentric(eph2_1[0],eph2_1[1],eph2_1[2],unit=u.km)
print(eloc.lon,eloc.lat,eloc.height)
(eph2_1,lt) = sp.spkezr('SITE2',et,'ITRF93','NONE','EARTH')
eloc = EarthLocation.from_geocentric(eph2_1[0],eph2_1[1],eph2_1[2],unit=u.km)
print(eloc.lon,eloc.lat,eloc.height)
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'ITRF93','NONE','EARTH')
eloc = EarthLocation.from_geocentric(eph2_1[0],eph2_1[1],eph2_1[2],unit=u.km)
print(eloc.lon,eloc.lat,eloc.height)

# position of nacelle 2 wrt Site 1 in Site1 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' nac2 wrt SITE1'+str(eph2_1))

# poition of site 2 wrt site 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('SITE2',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE2 wrt SITE1'+str(eph2_1))

# position of site 1 wrt site 2 in site 1 frame
(eph2_1,lt) = sp.spkezr('SITE1',et,'SITE1_TOPO','NONE','SITE2')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE1 wrt SITE2'+str(eph2_1))

# position of nacelle 2 wrt nacelle 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'SITE1_TOPO','NONE','MALBEC_NACELLE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' NAC2 wrt NAC1'+str(eph2_1))

# position of nacelle 2 wrt nacelle 1 in site 2 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'SITE2_TOPO','NONE','MALBEC_NACELLE1')
log.info('TOPO2 et='+sp.et2utc(et,"ISOC",2,50)+' NAC2 wrt NAC1'+str(eph2_1))

# position of nacelle 2 wrt nacelle 1 in nacelle 1 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'MALBEC_NACELLE1_FRAME','NONE','MALBEC_NACELLE1')
log.info('NAC1 et='+sp.et2utc(et,"ISOC",2,50)+' NAC2 wrt NAC1'+str(eph2_1))

# position of nacelle 1 wrt nacelle 2 in nacelle 2 frame
(eph2_1,lt) = sp.spkezr('MALBEC_NACELLE1',et,'MALBEC_NACELLE2_FRAME','NONE','MALBEC_NACELLE2')
log.info('NAC2 et='+sp.et2utc(et,"ISOC",2,50)+' NAC1 wrt NAC2'+str(eph2_1))


# log result
log.info('START UTC: '+sp.et2utc(start,"ISOC",2,50)+'STOP: '+sp.et2utc(stop,"ISOC",2,50))
log.info('eph2_1'+str(eph2_1))

#--------------------------------------------------------------------------------
# checking calculated altitude difference between site1 and site2 with
# variating site2 from very close to site1 and then increasing the distance between
# original site1 and modified site2 piece by piece
# site2 = very close to site1
# site 3 = slightly farther away
# site 4 = even more distance towards site 1
# etc
#--------------------------------------------------------------------------------
log.info('NOW CHECKING ALTITUDE Difference between site1 and modified site 2')

# poition of site 2 wrt site 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('SITE2',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE2 wrt SITE1'+str(eph2_1))

# poition of site 3 wrt site 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('SITE3',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE3 wrt SITE1'+str(eph2_1))

# poition of site 4 wrt site 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('SITE4',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE4 wrt SITE1'+str(eph2_1))

# poition of site 5 wrt site 1 in site 1 frame
(eph2_1,lt) = sp.spkezr('SITE5',et,'SITE1_TOPO','NONE','SITE1')
log.info('TOPO1 et='+sp.et2utc(et,"ISOC",2,50)+' SITE5 wrt SITE1'+str(eph2_1))
