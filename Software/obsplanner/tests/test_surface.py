import spiceypy as sp
import numpy as np



sp.furnsh('/astrodata/kernels/standard.ker')
sp.furnsh('/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampaignPlanner/MissionPlanSim1/trajectory-1.spk')
sp.furnsh('/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampaignPlanner/MissionPlanSim1/MALBEC_NACELLE1.tf')

et = 611871690.7561078


pnt = sp.subpnt('Intercept/ellipsoid','EARTH',611870779.995,'ITRF93','NONE','MALBEC_NACELLE1')
print('---- 1 -----')
print(pnt)

sp.furnsh('/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/ObsPlan/atmosphere.spk')
sp.furnsh('/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/ObsPlan/atmosphere.ker')
#pnt = sp.subpnt('Intercept/ellipsoid','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE','MALBEC_NACELLE1')

pnt = sp.subpnt('Intercept/ellipsoid','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE','MALBEC_NACELLE1')
print('---- 2 -----')
print(pnt)



eph = sp.spkezp(1399001,et,'ITRF93','NONE',399)
print('---- 3 -----')
print(eph)


eph  = sp.spkezp(-1001,et,'ATM_FRAME','NONE',1399001)
print('---- 4 -----')
print(eph)
eph  = sp.spkezp(-1001,et,'ITRF93','NONE',1399001)
print('---- 4 -----')
print(eph)

vec = [0,0,-1]
brsgt_top,lt,vecout = sp.sincpt('ELLIPSOID','EARTH',et,'ITRF93','NONE','MALBEC_NACELLE1','ITRF93',vec)
print('---- 5 -----')
print(brsgt_top)
brsgt_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE','MALBEC_NACELLE1','ITRF93',vec)
print('---- 5 -----')
print(brsgt_top)
vec = [0,0,1]
brsgt_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE','MALBEC_NACELLE1','MALBEC_NACELLE1_TOPOLIKE',vec)
print('---- 5 -----')
print(brsgt_top)

