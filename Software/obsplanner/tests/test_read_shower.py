"""Test the readout of the meteor shower file

"""
from obsplanner.obsplan import read_shower_file
from obsplanner import obsplan

from obsplanner import read_shower_files


"""
# set configuration file
config_file = '../conf/config.in'
# create ObservationPlanner Object
ObsPplan = obsplan.ObservationPlanner(config_file,'dumb','dumber')
"""


shower_file = '../conf/streamestablisheddata_jv.csv'
#data = ObsPplan.get_shower_data('Perseids',shower_file)
data = read_shower_files.get_shower_data('Perseids','2020',shower_file)
