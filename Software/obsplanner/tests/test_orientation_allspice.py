"""Test MALBEC camera orientation

"""
import logging
import simplekml
import spiceypy as sp
from spiceypy.utils.support_types import SpiceyError
import astropy.units as u
from astropy.time import Time as Time
from astropy.coordinates import EarthLocation, get_moon, get_sun, AltAz, SkyCoord, Angle, Latitude, Longitude, ICRS

import obsplanner.make_id as make_id

#--------------------------------------------------------------------------------
# setup logging
#--------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

# load kernels
sp.furnsh('/astrodata/kernels/standard.ker')

misplan_path1 = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampaignPlanner/MissionPlanSim2/'
misplan_path2 = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CampaignPlanner/MissionPlanSim5/'
obsplan_path = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/ObsPlan/NACELLE2_NACELLE5_10/'
# load spacecraft kernel
spk_file1 = misplan_path1+'trajectory-2.spk'
sp.furnsh(spk_file1)
# load TOPO-LIKE frame
sp.furnsh(obsplan_path+'MALBEC_NACELLE2.tf')
# load spacecraft orientation
sp.furnsh(obsplan_path+'MALBEC_NACELLE2.ck')
# load nacelle orientation frame kernel
sp.furnsh(misplan_path1+'MALBEC_NACELLE2.tk')
# load instrument kernel
sp.furnsh(misplan_path1+'MALBEC_NACELLE2.ik')
# load camera clock
sp.furnsh(obsplan_path+'MALBEC_NACELLE2_fake.tsc')

# load spacecraft kernel
spk_file2 = misplan_path2+'trajectory-5.spk'
sp.furnsh(spk_file2)
# load spacecraft frame
sp.furnsh(obsplan_path+'MALBEC_NACELLE5.tf')
# load camera frame kernel
sp.furnsh(misplan_path2+'MALBEC_NACELLE5.tk')
# load instrument kernel
sp.furnsh(misplan_path2+'MALBEC_NACELLE5.ik')
# load spacecraft clock
sp.furnsh(obsplan_path+'MALBEC_NACELLE5_fake.tsc')
# load spacecraft orientation
sp.furnsh(obsplan_path+'MALBEC_NACELLE5.ck')

# load atmosphere
sp.furnsh(obsplan_path+'atmosphere.ker')
sp.furnsh(obsplan_path+'atmosphere.spk')

#================================================
# set time manually
time_utc = '2019-05-23T08:30:10.810'
# time at top:
time_utc = '2019-05-23T09:29:49.730'

# START OF PROGRAM
kml = simplekml.Kml()
# get spice time
et = sp.str2et(time_utc)
# retrieve nacelle name and id
nacelle_id1 = sp.spkobj(spk_file1)[0]
nacelle_name1 = sp.bodc2n(nacelle_id1)
nacelle_id2 = sp.spkobj(spk_file2)[0]
nacelle_name2 = sp.bodc2n(nacelle_id2)
nacelle_cam_name1 = make_id.make_camera_name(nacelle_name1)
nacelle_cam_name2 = make_id.make_camera_name(nacelle_name2)
nacelle_cam_id1 = sp.bodn2c(nacelle_cam_name1)
nacelle_cam_id2 = sp.bodn2c(nacelle_cam_name2)

# retrieve spacecraft cock id
clock_id1 = make_id.make_clock_id(nacelle_cam_id1)
clock_id2 = make_id.make_clock_id(nacelle_cam_id2)
# retrieve spacecraft cock time
unencoded_clockstring1 = sp.sce2s(clock_id1,et)
unencoded_clockstring2 = sp.sce2s(clock_id2,et)
# encode spacecraft clock
encoded_clock1 = sp.scencd(clock_id1,unencoded_clockstring1)
encoded_clock2 = sp.scencd(clock_id2,unencoded_clockstring2)
log.debug('encoded_clock1: '+str(encoded_clock1))
log.debug('encoded_clock2: '+str(encoded_clock2))
# set topo-like frames
topo_frame_name1 = make_id.make_topoframe_name(nacelle_name1)
topo_frame_name2 = make_id.make_topoframe_name(nacelle_name2)
nacori_frame_name1 = make_id.make_nacelle_orientation_frame_name(nacelle_name1)
nacori_frame_name2 = make_id.make_nacelle_orientation_frame_name(nacelle_name2)
#================================================

# get nacelle 1 position in itrf93
(eph, lt) = sp.spkezr(nacelle_name1,et,'ITRF93','NONE','EARTH')
# convert x,y,z into an astropy.coordinates.EarthLocation (global) object:
eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)

# retrieve camera FOV information
[shape,insfrm,brsgt,n,bounds] = sp.getfov(nacelle_cam_id1,4)
# make a list of bounds, and add boresight
bounds = bounds.tolist()
bounds.append(brsgt)

# test: pxform: get transformation between 2 frames
for (frame1,frame2) in zip([nacori_frame_name1,nacori_frame_name1,nacori_frame_name1],
                           [topo_frame_name1,'ITRF93','J2000']):
    log.debug('Getting orientation of frame: '+frame1+' at '+time_utc+' wrt frame '+frame2)
    frm_mtx = sp.pxform(frame1,frame2,et)
    log.debug('frm_mtx: '+str(frm_mtx))
log.debug('============================')


instr_name = nacelle_cam_name1
instr_id = nacelle_cam_id1
encoded_clock = encoded_clock1
# TEST TEST TEST TEST TEST TEST TEST
# TEST TEST TEST TEST TEST TEST TEST
# TEST TEST TEST TEST TEST TEST TEST
instr_name = nacelle_name1
instr_id = nacelle_id1
# TEST TEST TEST TEST TEST TEST TEST
# TEST TEST TEST TEST TEST TEST TEST
# TEST TEST TEST TEST TEST TEST TEST

boresight_vec = [1.0,0.0,0.0]

for frame in [nacori_frame_name1,topo_frame_name1,'ITRF93','ATM_FRAME','J2000']:
    log.debug('Getting orientation of '+instr_name+' '+str(instr_id)+' at '+time_utc+' in frame '+frame)
    # loop over FOV bounds. last is boresight
    for bound in bounds:
        # get orientation in different frames
        #log.debug('Getting orientation of '+instr_name+' id: '+str(instr_id)+' at '+time_utc+' / encoded clock: '+str(encoded_clock)+' in frame '+frame)
        log.debug('Getting orientation of bound '+str(bound)+' at '+time_utc+' in frame '+frame)
        try:
            #nacelle_pointing_mtx,t = sp.ckgp(instr_id,encoded_clock,0.0,frame)
            nacelle_pointing_mtx = sp.pxform(frame1,frame2,et)
            # rotate FOV boundary from nacelle orientation
            bound_vec = sp.mxv(nacelle_pointing_mtx,bound)
            # translates into az/el angle
            rlonlat = sp.reclat(bound_vec)
            log.debug('reclat_lon: '+str(rlonlat[1]*sp.dpr()))
            log.debug('reclat_lat: '+str(rlonlat[2]*sp.dpr()))
            # convert rlonlat into geographic azimuth and elevation
            az = (-rlonlat[1]*sp.dpr() %360) * u.deg
            el = rlonlat[2]*sp.dpr() * u.deg
            log.debug('az: '+str(az))
            log.debug('el: '+str(el))
            # translates into ra/dec angle
            rng_ra_dec = sp.recrad(bound_vec)
            log.debug('bound : '+str(bound))
            log.debug('bound_vec : '+str(bound_vec))
            log.debug('recrad_lon: '+str(rng_ra_dec[1]*sp.dpr()))
            log.debug('recrad_lat: '+str(rng_ra_dec[2]*sp.dpr()))
            # put result into SkyCoord Object
            skycoo = SkyCoord(ra=rng_ra_dec[1],dec=rng_ra_dec[2],
                      frame=ICRS,
                      unit=u.rad,
                      obstime=Time(time_utc,scale='utc'))
            log.debug('ra angle: '+str(rng_ra_dec[1]*sp.dpr()))
            log.debug('dec angle: '+str(rng_ra_dec[2]*sp.dpr()))
            log.debug(str(skycoo.ra))
            log.debug(str(skycoo.dec))
            log.debug(skycoo.to_string('hmsdms'))
        except SpiceyError as exc:
            print( 'Exception message is: {:s}'.format(exc.value ))


#  - - - - - - - - - - boresight vector at atmosphere - - - -  -- -- - 
coords_lb = []
for bound in bounds:
    # get intersection with atmosphere
    log.debug('Getting intersection of bound: '+str(bound)+' with '+'ATMOSPHERE_ELLIPSOID'+' at '+time_utc+' et: '+str(et)+' in frame '+'ATM_FRAME')
    log.debug('bound: '+str(bound))
    # compute intersection between boresight and top of atmosphere ellipsoid.
    log.debug('Getting intersection with atmosphere of '+instr_name+' '+str(instr_id)+' of bound at '+time_utc+' in frame '+topo_frame_name1)
    brsgt_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',instr_name,nacori_frame_name1,bound)
    brsgt_top_eloc = EarthLocation.from_geocentric(brsgt_top[0],brsgt_top[1],brsgt_top[2],unit='km')
    log.debug('brsgt_top_eloc:: '+str(brsgt_top_eloc.to_geodetic()))
    
    coords_lb.append((          eloc.lon.to('deg').value,          eloc.lat.to('deg').value,          eloc.height.to('m').value))
    coords_lb.append((brsgt_top_eloc.lon.to('deg').value,brsgt_top_eloc.lat.to('deg').value,brsgt_top_eloc.height.to('m').value))
    # create kml
    lb1 = kml.newlinestring(name='FOV1')
    lb1.coords = coords_lb
    lb1.extrude = 0
    lb1.altitudemode = simplekml.AltitudeMode.absolute
    lb1.style.linestyle.width = 5
    lb1.style.linestyle.color = simplekml.Color.blue
    lb1.timestamp.when = time_utc
print('- - - - - - - -- - -')


kml_file = 'test_fov.kml'
kml.save(kml_file)
print('KML saved in '+kml_file)





# - - - - - - J2000 - - - - - - - - - - - 
# geting pointing direction in RADEC
log.debug('Getting orientation of '+nacelle_name1+' '+str(nacelle_id1)+' at '+time_utc+' in frame J2000')
nacelle_pointing_mtx,t = sp.ckgp(nacelle_id1, encoded_clock1,0.0,'J2000')
log.debug('Getting orientation of '+nacelle_cam_name1+' '+str(nacelle_cam_id1)+' at '+time_utc+' in frame J2000')
nacelle_pointing_mtx,t = sp.ckgp(nacelle_cam_id1, encoded_clock1,0.0,'J2000')
log.debug('nacelle_pointing: '+str(nacelle_pointing_mtx))
w = [1,0,0]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rng_ra_dec = sp.recrad(v)
# put result into SkyCoord Object
skycoo = SkyCoord(ra=rng_ra_dec[1],dec=rng_ra_dec[2],
                  frame=ICRS,
                  unit=u.rad,
                  obstime=Time(time_utc,scale='utc'))
# get angles
log.debug('w : '+str(w))
log.debug('v : '+str(v))
log.debug('ra: '+str(rng_ra_dec[1]*sp.dpr()))
log.debug('dec: '+str(rng_ra_dec[2]*sp.dpr()))
print(skycoo)
print(skycoo.to_string('hmsdms'))

theo_skycoo = SkyCoord('22h53m35.0500s +30d45m45.6000s', frame="icrs") # , unit=(u.hourangle, u.deg)
print(theo_skycoo)
print(theo_skycoo.to_string('hmsdms'))



# get nacelle 2 position in itrf93
(eph, lt) = sp.spkezr(nacelle_name2,et,'ITRF93','NONE','EARTH')
# convert x,y,z into an astropy.coordinates.EarthLocation (global) object:
eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
# get orientation in TOPO-LIKE frame
log.debug('Getting orientation of '+nacelle_cam_name2+' '+str(nacelle_cam_id2)+' at '+time_utc+' in frame '+topo_frame_name2)
nacelle_pointing_mtx,t = sp.ckgp(nacelle_cam_id2, encoded_clock2,0.0,topo_frame_name2)
log.debug('nacelle_pointing: '+str(nacelle_pointing_mtx))
w = [1,0,0]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rlonlat = sp.reclat(v)
# get angles
log.debug('w : '+str(w))
log.debug('v : '+str(v))
log.debug('lon: '+str(rlonlat[1]*sp.dpr()))
log.debug('lat: '+str(rlonlat[2]*sp.dpr()))
Eul = sp.m2eul(nacelle_pointing_mtx,2,3,1)
log.debug('Euler angles:'+str((Eul[0]*sp.dpr(),Eul[1]*sp.dpr(),Eul[2]*sp.dpr())))

# compute boresight vector intersection with atmosphere
brsgt_vec = sp.mxv(nacelle_pointing_mtx,w)  # = v
log.debug('brsgt_vec: '+str(brsgt_vec))
# compute intersection between boresight and top of atmosphere ellipsoid.
log.debug('Getting intersection with atmosphere of '+nacelle_name2+' '+str(nacelle_id2)+' boresight at '+time_utc+' in frame '+topo_frame_name2)
brsgt_top,lt,vecout = sp.sincpt('ELLIPSOID','ATMOSPHERE_ELLIPSOID',et,'ATM_FRAME','NONE',nacelle_name2,topo_frame_name2,brsgt_vec)
brsgt_top_eloc = EarthLocation.from_geocentric(brsgt_top[0],brsgt_top[1],brsgt_top[2],unit='km')
log.debug('brsgt_top_eloc:: '+str(brsgt_top_eloc.to_geodetic()))
coords_lb = []
coords_lb.append((          eloc.lon.to('deg').value,          eloc.lat.to('deg').value,          eloc.height.to('m').value))
coords_lb.append((brsgt_top_eloc.lon.to('deg').value,brsgt_top_eloc.lat.to('deg').value,brsgt_top_eloc.height.to('m').value))
# create kml
lb2 = kml.newlinestring(name='BSGT2')
lb2.coords = coords_lb
lb2.extrude = 0
lb2.altitudemode = simplekml.AltitudeMode.absolute
lb2.style.linestyle.width = 5
lb2.style.linestyle.color = simplekml.Color.yellow
lb2.timestamp.when = time_utc




# - - - - - - J2000 - - - - - - - - - - - 
# geting pointing direction in RADEC
log.debug('Getting orientation of '+nacelle_cam_name2+' '+str(nacelle_cam_id2)+' at '+time_utc+' in frame J2000')
nacelle_pointing_mtx,t = sp.ckgp(nacelle_cam_id2, encoded_clock2,0.0,'J2000')
log.debug('nacelle_pointing: '+str(nacelle_pointing_mtx))
w = [-1,0,0]
# rotates the unit vector
v = sp.mxv(nacelle_pointing_mtx,w)
# translates into angle
rng_ra_dec = sp.recrad(v)
# put result into SkyCoord Object
skycoo = SkyCoord(ra=rng_ra_dec[1],dec=rng_ra_dec[2],
                  frame=ICRS,
                  unit=u.rad,
                  obstime=Time(time_utc,scale='utc'))
# get angles
log.debug('w : '+str(w))
log.debug('v : '+str(v))
log.debug('ra: '+str(rng_ra_dec[1]*sp.dpr()))
log.debug('dec: '+str(rng_ra_dec[2]*sp.dpr()))
print(skycoo)
print(skycoo.to_string('hmsdms'))

theo_skycoo = SkyCoord('02h48m03.0300s +51d37m05.9000s', frame="icrs") # , unit=(u.hourangle, u.deg)
print(theo_skycoo)
print(theo_skycoo.to_string('hmsdms'))



kml_file = 'test_fov.kml'
kml.save(kml_file)
print('KML saved in '+kml_file)








