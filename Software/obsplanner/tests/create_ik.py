"""
Goal:
-make instrument spice kernels for MALBEC nacelles

DEPENDENCIES:
-spice librairie
-spice pinpoint utility program installed


Author:
J. Vaubaillon, IMCCE, 2020, MALBEC project
A. Rietze, Univ. Oldenburg, Germany
"""
# --------------------------------------------------------------------------------
# import all necessary tools
# --------------------------------------------------------------------------------
import os
import spiceypy as sp
import numpy as np
import astropy.units as u
import logging
from configparser import ConfigParser, ExtendedInterpolation
from astropy.coordinates import EarthLocation, Longitude, Latitude, Angle, SkyCoord, AltAz
# import Software.reduction.camera as camera
from reduction.camera import Camera
from reduction.filter import Filter
from reduction.lens import Lens
from missionplanner import utils
import math

# set home directory
home = os.getenv('HOME') + '/'


# --------------------------------------------------------------------------------
# create default logger object
# --------------------------------------------------------------------------------
# log sub-directory
log_dir = home + 'LOG/'
# logging file name root
log_file_root = log_dir + 'create_ik-YYYYMMDDTHHMMSS.log'

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

def make_camera_name(nacelle_name):
    """Make camera instrument name from nacelle name.
    
    Parameters
    ----------
    nacelle_name : string
        Nacelle name.
    
    Returns
    -------
    camera_name : string
        Camera name.
    
    """
    return nacelle_name + '_CAMERA'



# -----------------------------------------------------------------------------
# Define subroutines
# -----------------------------------------------------------------------------
def create_ik(kernel_path, spice_exe_path, out_dir, config_file, spk_file1, spk_file2, tf_file1,
              tf_file2, outputikfile):
    """

    :param kernel_path:
    :param spice_exe_path:
    :param out_dir:
    :param config_file:
    :param spk_file1:
    :param spk_file2:
    :param tf_file1:
    :param tf_file2:
    :param outputikfile:
    :return:
    """


    # check if output directory already exists
    # if it does not exist yet, create output directory
    if not os.path.exists(out_dir):
        try:
            os.makedirs(out_dir)
        except FileExistsError:
            log.info('Directory '+out_dir+ ' already exists.')

    # check if config_file with information about camera/lens exists
    if not os.path.exists(config_file):
        msg = '*** FATAL ERROR: site file '+config_file+' does not exist'
        log.error(msg)
        raise IOError(msg)

    # set input standard kernel path
    input_kernel_file = kernel_path + 'standard.ker'
    # load standard kernel
    sp.furnsh(input_kernel_file)

    # load nacelle spk kernels
    sp.furnsh(spk_file1)
    sp.furnsh(spk_file2)

    # load nacelle fk kernels
    sp.furnsh(tf_file1)
    sp.furnsh(tf_file2)

    # get nacelle SPICE IDs
    nacelle1_id = sp.spkobj(spk_file1)[0]
    nacelle2_id = sp.spkobj(spk_file2)[0]
    # set nacelle SPICE names
    nacelle1_name = sp.bodc2n(nacelle1_id)
    nacelle2_name = sp.bodc2n(nacelle2_id)

    nacelle1_frame = 'MALBEC_NACELLE1_FRAME'
    nacelle2_frame = 'MALBEC_NACELLE2_FRAME'
    nac1_frame_id = 5399001
    nac2_frame_id = 5399002


    # --------------------------------------------------------------------------------
    # Checking Time window coverage of nacelles to calculate position of nacelles
    # --------------------------------------------------------------------------------
    # get time window coverage for each nacelle
    cov1 = sp.spkcov(spk_file1,nacelle1_id)
    cov2 = sp.spkcov(spk_file2,nacelle2_id)

    # test/make sure that there is indeed a window coverage for both nacelles
    start1,stop1 = sp.wnfetd(cov1,0)
    start2,stop2 = sp.wnfetd(cov2,0)
    log.info('cov1: START UTC: '+sp.et2utc(start1,"ISOC",2,50)+' STOP:'+sp.et2utc(stop1,"ISOC",2,50))
    log.info('cov2: START UTC: '+sp.et2utc(start2,"ISOC",2,50)+' STOP:'+sp.et2utc(stop1,"ISOC",2,50))

    # compute the time window intersection in order to guarantee that the 2 nacelle
    # always exist when we query the frame kernel
    cov = sp.wnintd(cov1,cov2)

    # get time window boundaries
    start,stop = sp.wnfetd(cov,0)
    log.info('cov: STAR UTC: '+sp.et2utc(start,"ISOC",2,50)+'STOP:'+sp.et2utc(stop,"ISOC",2,50))

    # test: get ephemeris of nacelle2 in nacelle1 reference frame -> if everything goes well,
    # one coordinate should be = 0
    # first set the time
    et = start

    # checking if output ik-file already exists
    # delete previously created malbec/nacelle kernel files
    if os.path.exists(outputikfile):
        os.remove(outputikfile)

    # set up MALBEC-specific variables (camera/lens name, brand, nacelle names) from config_file
    # check sites
    infoconf = ConfigParser()
    log.info(config_file)
    infoconf.read(config_file)

    # --------------------------------------------------------------------------------
    # calculating FOV of used camera, lens and filter in camera.py
    # --------------------------------------------------------------------------------
    camera_name = infoconf['CAMERA']['camera_name']
    camera_brand = infoconf['CAMERA']['camera_brand']
    camera_lens = infoconf['CAMERA']['lens_name']
    lens_brand = infoconf['CAMERA']['lens_brand']
    filter_brand = infoconf['CAMERA']['filter_brand']
    filter_name = infoconf['CAMERA']['filter_name']

    # create Filter object
    flt = Filter(brand=filter_brand,name=filter_name)

    # create Lens object
    lens = Lens(brand=lens_brand, name=camera_lens, filtre=flt)

    # create Camera Object
    cam = Camera(brand=camera_brand, name=camera_name, lens=lens)

    # computation of fov and fovdiag
    fov = cam.fov
    fovdiag = cam.fovdiag

    # --------------------------------------------------------------------------------
    # determining Azimuth & elevation from position (lat, lon, alt) of nacelle 1 and nacelle 2
    # --------------------------------------------------------------------------------
    # position of nacelle 1 wrt Earth in ITRF93 frame
    (eph1_e,lt) = sp.spkezr('MALBEC_NACELLE1',et,'ITRF93','NONE','EARTH')
    log.info('NAC2 et='+sp.et2utc(et,"ISOC",2,50)+' NAC1 wrt NAC2'+str(eph1_e))
    # get position of nac1 in ITRF and convert into Earthlocation
    eloc1 = EarthLocation.from_geocentric(eph1_e[0],eph1_e[1],eph1_e[2],unit=u.km)

    # position of nacelle 2 wrt Earth in ITRF93 frame
    (eph2_e,lt) = sp.spkezr('MALBEC_NACELLE2',et,'ITRF93','NONE','EARTH')
    log.info('NAC1 et='+sp.et2utc(et,"ISOC",2,50)+' NAC2 wrt NAC1'+str(eph2_e))
    # get position of nac2 in ITRF and convert into Earthlocation
    eloc2 = EarthLocation.from_geocentric(eph2_e[0],eph2_e[1],eph2_e[2],unit=u.km)

    # ---------
    # Method 1: using SPICE and pygc to calculate azimuth and elevation
    # Step1: Computing the azimuth using pygc (see utils.distaz)
    # Step2: using cartesian coordinates of nacelle's position to get elevation by computing the angle given the difference of Z
    # ---------
    # computing distance and azimuth of nacelle 1 wrt nacelle 2
    (distance,azimuth) = utils.distaz(eloc1, eloc2)
    
    # compute position of nacelle 1 wrt nacelle 2 in ITRF93 frame
    (eph1_2,lt) = sp.spkezr('MALBEC_NACELLE1',et,'ITRF93','NONE','MALBEC_NACELLE2')
    (eph2_1,lt) = sp.spkezr('MALBEC_NACELLE2',et,'ITRF93','NONE','MALBEC_NACELLE1')
    # computing difference in z (difference in z = eph1_2[2])
    delta_z = eph1_2[2]*u.km
    
    # for testing if difference in z is the same from both nacelles
    delta_z1 = eph2_1[2]*u.km
    log.info(delta_z1)
    log.info(delta_z)
    
    # computing the elevation of nacelle1 seen from nacelle2
    elevation = math.asin(delta_z / distance.to('km'))
    elevation = elevation*u.deg
    
    # ---------
    # Method 2: using only SPICE
    # ---------
    # transforming cartesian coordinates (of nacelles position) into spherical coordinates
    # [r, colat, lon] = sp.cspice_recsph(eph2_1)

    # ====================================================
    # now make instrument kernel for MALBEC station camera
    out = open(outputikfile, 'w')
    out.write("\\begintext \n")
    out.write("  Definition of the pointing direction and FOV of MALBEC cameras instruments. \n\n")
    # out.write("  Given the definition of the SPICE topographic frame,\n")
    # out.write("  ... \n")
    out.write("   \n")
    out.write("  Author: J. Vaubaillon - IMCCE, A. Rietze, Univ. Oldenburg - 2020 \n")
    out.write("  MALBEC project \n\n\n")
    out.write("Definition of the" + nacelle1_name + " FOV: \n \n")
    out.write("\\begindata \n")
    out.write("INS_" + str(nac1_frame_id) + "_NAME = " + repr(make_camera_name(nacelle_name)) + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_FRAME = " + repr(nacelle1_frame) + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_SHAPE = " + repr("RECTANGLE") + " \n")
    log.info(azimuth)
    log.info(elevation)
    log.debug("name=" + str(nac1_frame_id) + " id=" + str(nacelle1_id) + " az=" + str(
        azimuth.to('deg').value) + " alt=" + str(elevation.to('deg').value))
    boresight = sp.radrec(10.0, (- azimuth.to('rad').value), elevation.to('rad').value)
    out.write("INS_" + str(nac1_frame_id) + "_BORESIGHT = ( " + str(boresight[0]) + ",   " + str(
        boresight[1]) + " ,  " + str(boresight[2]) + " ) \n")
    
    # defining the 4 corners of the FOV
    out.write("INS_" + str(nac1_frame_id) + "_FOV_BOUNDARY = ( ")
    bndcrnr = sp.rotvec(boresight, fov[0].to('rad').value / 2, 3)  # rotate around z
    bndcrnr = sp.rotvec(bndcrnr, fov[1].to('rad').value / 2, 1)  # rotate around x
    out.write(str(bndcrnr)[1:-1] + " \n")
    bndcrnr = sp.rotvec(boresight, 0.0 - fov[0].to('rad').value / 2, 3)  # rotate around z
    bndcrnr = sp.rotvec(bndcrnr, fov[1].to('rad').value / 2, 1)  # rotate around x
    out.write("                               " + str(bndcrnr)[1:-1] + " \n")
    bndcrnr = sp.rotvec(boresight, 0.0 - fov[0].to('rad').value / 2, 3)  # rotate around z
    bndcrnr = sp.rotvec(bndcrnr, 0.0 - fov[1].to('rad').value / 2, 1)  # rotate around x
    out.write("                               " + str(bndcrnr)[1:-1] + " \n")
    bndcrnr = sp.rotvec(boresight, fov[0].to('rad').value / 2, 3)  # rotate around z
    bndcrnr = sp.rotvec(bndcrnr, 0.0 - fov[1].to('rad').value / 2, 1)  # rotate around x
    out.write("                               " + str(bndcrnr)[1:-1] + " ) \n")
    
    # """
    # defining the corners with the ref_vector, ref_angle and cross_angle
    refvector = sp.rotvec(boresight, sp.pi() / 2, 3)  # rotation of boresight of pi/2 around z-axis
    out.write("INS_" + str(nac1_frame_id) + "_FOV_CLASS_SPEC = " + repr("ANGLES") + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_REF_VECTOR = ( " + str(refvector[0]) + ",   " + str(
    refvector[1]) + " ,  " + str(refvector[2]) + " )  \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_REF_ANGLE =  " + str(fov[1].to('deg').value / 2) + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_CROSS_ANGLE =  " + str(fov[0].to('deg').value / 2) + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_REF_ANGLE =  " + str(fov[0].to('deg').value / 2) + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_CROSS_ANGLE =  " + str(fov[1].to('deg').value / 2) + " \n")
    out.write("INS_" + str(nac1_frame_id) + "_FOV_ANGLE_UNITS =  " + repr("DEGREES") + " \n\n")
    # """
    out.write("\\begintext \n")
    out.close()
    log.info("data saved in " + outputikfile)
    
    return


# -----------------------------------------------------------------------------
# Define some variables
# -----------------------------------------------------------------------------


def main():
    # --------------------------------------------------------------------------------
    # Define default input values
    # --------------------------------------------------------------------------------
    # set kernel path and SPICE utilities path -> better in configuration file
    # SPICE default kernel path
    kernel_path = '/astrodata/kernels/'
    # SPICE utilities path
    spice_exe_path = '/astrodata/exe/'
    
    # create output directory/ set output path
    # has to be adjusted according to chosen nacelle and site
    out_dir = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/ik_kernels/'
    
    # set input file (config file) with information about camera/lens name:
    config_file = home + '/malbec/Software/obsplanner/conf/config.in'
    
    # path to spk files of both nacelles
    spk_file1 = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac1_sit1/trajectory.spk'
    spk_file2 = home + 'malbec/DATA/20190523/Trajectory-testpredict/nac2_sit2/trajectory.spk'
    
    # path to tf-files (frame files) of nacelle1 and nacelle2
    tf_file1 = '/astrodata/kernels/nacelle1.tf'
    tf_file2 = '/astrodata/kernels/nacelle2.tf'
    
    # setting output file for ik-kernel
    outputikfile = out_dir + '/nacelle1.ik'
    
    # call subroutine create_ik
    create_ik(kernel_path, spice_exe_path, out_dir, config_file, spk_file1, spk_file2, tf_file1,
              tf_file2, outputikfile)

# =============================================================================
if __name__ == '__main__':
    main()
