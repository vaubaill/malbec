from copy import copy,deepcopy
import numpy as np
from scipy.optimize import minimize
import spiceypy as sp
import astropy.units as u
from friputils import phot
from astropy.coordinates import SkyCoord, AltAz
from fripipe.initialize import path_and_file as pthf
from fripipe.definitions.radiant import Radiant
from fripipe.definitions.trajectory import Trajectory
from fripipe.definitions.observation import Observation

def gradient_dist(param, allobs, ksiR, XM):
    """Compute gradiant and distance
    
    Parameters
    ----------
    param : array of floats
        [YM,ZM,etaR,zetaR] (see Ceplecha 1987)
    
    Returns
    -------
    grad : float
        the distance gradient
    
    """
    YM = param[0] # [m]
    ZM = param[1] # [m]
    etaR = param[2] # [-] 
    zetaR = param[3] # [-]
    nobs = len(allobs)
    npts = []
    nallpnts = 0
    for o in range(nobs):
        npts.append(len(allobs[o].skycoo.ra))
        nallpnts = (npts)				# total number of observed points
        all_d_dYM = np.zeros(nallpnts,dtype=np.float64)
        all_d_dZM = np.zeros(nallpnts,dtype=np.float64)
        all_d_detaR = np.zeros(nallpnts,dtype=np.float64)
        all_d_dzetaR = np.zeros(nallpnts,dtype=np.float64)
    grad = np.zeros(len(param),dtype=np.float64)
    
    # generalized loop
    k=0
    for o in range(nobs):
        for i in np.arange (0,npts[o]):
            D_num = ((XM-allobs[o].station.eci.x.to('m').value)*(allobs[o].zeta[i] *  etaR - allobs[o].eta[i]  * zetaR)+
            (YM-allobs[o].station.eci.y.to('m').value)*(allobs[o].ksi[i]  * zetaR - allobs[o].zeta[i] *  ksiR)+
            (ZM-allobs[o].station.eci.z.to('m').value)*(allobs[o].eta[i]  *  ksiR - allobs[o].ksi[i]  *  etaR))
            sign = D_num/np.abs(D_num)
            D_num = np.abs(D_num)
            D_den = np.sqrt(( etaR*allobs[o].zeta[i] - allobs[o].eta[i] * zetaR)**2+
                            (zetaR*allobs[o].ksi[i]  - allobs[o].zeta[i]*  ksiR)**2+
                            ( ksiR*allobs[o].eta[i]  - allobs[o].ksi[i] *  etaR)**2)
            sign = 1
            all_d_dYM[k] = sign*(allobs[o].ksi[i]*zetaR - allobs[o].zeta[i]*ksiR)/D_den
            all_d_dZM[k] = sign*(allobs[o].eta[i]* ksiR - allobs[o].ksi[i] *etaR)/D_den
            term1 = (allobs[o].ksi[i]*(     allobs[o].station.eci.z.to('m').value-ZM)  - allobs[o].zeta[i]*(allobs[o].station.eci.x.to('m').value-XM))/D_den
            term2 = (allobs[o].ksi[i]*(etaR*allobs[o].ksi[i]-allobs[o].eta[i]*ksiR)    - allobs[o].zeta[i]*(allobs[o].eta[i]*zetaR - etaR*allobs[o].zeta[i]))*D_num/(D_den**3)
            all_d_detaR[k]   = sign*(term1-term2)
            term1 = (allobs[o].eta[i]*(      allobs[o].station.eci.x.to('m').value-XM) - allobs[o].ksi[i] *(allobs[o].station.eci.y.to('m').value - YM))/D_den
            term2 = (allobs[o].eta[i]*(zetaR*allobs[o].eta[i]                          - allobs[o].zeta[i]*etaR)+allobs[o].ksi[i]*(allobs[o].ksi[i]*zetaR-ksiR*allobs[o].zeta[i]))*D_num/(D_den**3)
            all_d_dzetaR[k]  = sign*(term1-term2)
            k = k+1
        grad = [np.sum(all_d_dYM),
                np.sum(all_d_dZM),
                np.sum(all_d_detaR),
                np.sum(all_d_dzetaR)]
    return grad


# computes distance to a line
def dist_2_lines(param,allobs,ksiR,XM):
    """Computes the distance between line (see Ceplecha 1987).
    
    Parameters
    ----------
    param : array of floats
        [YM,ZM,etaR,zetaR]
    allobs : array of fripipe.definitions.observation.Observation Objects
        Observations of the event
    ksiR : float
        Ksi_r, see Ceplecha 1987
    XM : float
        1st cartesian coordinate f first point of trajectory [m]
    
    Returns
    -------
    dist_2_lines : float
        distance to line defined by param.
    
    """
    prog = "(dist_2_lines) "
    YM = param[0] # [m]
    ZM = param[1] # [m]
    etaR = param[2] # unit vector y [-]
    zetaR = param[3] # unit vector z [-]
    nobs = len(allobs)
    npts = []
    D = []
    for o in range(nobs):
        npts.append(len(allobs[o].skycoo.ra))
    k=0
    for o in range(nobs):
        for i in np.arange (0,npts[o]):
            D_num = np.abs((XM                     - allobs[o].station.eci.x.to('m').value)*
                (etaR*allobs[o].zeta[i] - allobs[o].eta[i]                      *zetaR)+
                    (YM                  - allobs[o].station.eci.y.to('m').value)*
                (allobs[o].ksi[i]*zetaR - allobs[o].zeta[i]       *ksiR)+
                    (ZM                  - allobs[o].station.eci.z.to('m').value)*
                (ksiR*allobs[o].eta[i]  - allobs[o].ksi[i]	   *etaR))
            D_den = np.sqrt(( etaR*allobs[o].zeta[i] - allobs[o].eta[i]  * zetaR)**2+
                    (zetaR*allobs[o].ksi[i]  - allobs[o].zeta[i] *  ksiR)**2+
                ( ksiR*allobs[o].eta[i]  - allobs[o].ksi[i]  *  etaR)**2)
            D.append(D_num/D_den)
    return np.sqrt(np.sum(np.asarray(D)**2)) 


class Borovicka_n (object):
    """Borovicka_n trajectory solver object.
    
    This object is used to compute the trajectory of a meteoroid
    from a multi-observation, by using the method described in
    Borovicka (1990, Bull. Astron. Inst. Czechosl., 41, 391-396).
    
    """
    ksi = 0
    XM = 0
    ksiR = 0
    eta = 0
    zeta = 0
    npts = 0
    D = np.zeros(1)
    param = [0,0,0,0]
    #
    def __init__(self,all_obs):
        """Initializes the Borovicka_n object
        
        Parameters
        ----------
        all_obs : array of fripipe.Trajectory.definitions.Observation object
            all Observation objects organized in an array
        
        Returns
        -------
        None
            ingest all_obs and launches the initialize method
        
        """
        self.allobs =all_obs
        self.nobs =len(self.allobs) # number of observations object
        self.npts =np.zeros(self.nobs,dtype=np.int16) # number of data in each observation object
        # verification
        if (self.nobs <2):
            msg = "*** FATAL ERROR: no trajectory can be computed with less than 2 stations"
            pthf.fri_log.error(msg)
            # replace with no-enough-stations error
            raise ValueError(prog+msg)
        self.initialize()
        return
    
    def initialize(self): 
        """Initializes the Borovicka_n object
        
        Parameters
        ----------
        None
        
        Returns
        -------
        None
            A first guess for the trajectory solution is computed and saved
            in the self.param variable.
        
        """
        #initialisation
        for o in range(self.nobs):
            obs = self.allobs[o]
            # transform [ra,dec] into unit vecor [xi,eta,zeta]
            obs.to_KsiEtaZeta()
            self.npts[o] = len(obs.skycoo.ra)
        # total number of observed points
        self.nallpnts = np.sum(self.npts)
        if (self.nallpnts<=0):
            msg = "*** FATAL ERROR: there is no observation to treat. "+str(self.npts)
            pthf.fri_log.error(msg)
            raise ValueError(prog+msg)
        # now we compute a first estimate of the parameters
        # Plane equation computed with first station
        plane = self.plane_equation(self.allobs[0].station.eci,
            [self.allobs[0].ksi[0],
             self.allobs[0].eta[0],
             self.allobs[0].zeta[0]],
            [self.allobs[0].ksi[self.npts[0]-1],
             self.allobs[0].eta[self.npts[0]-1],
             self.allobs[0].zeta[self.npts[0]-1]])
        a = plane[0]
        b = plane[1]
        c = plane[2]
        d = plane[3]
        # compute Beginning and end of the trajectory for each station and then
        # choose the one providing the longest trajectory
        traj_len= np.zeros(self.nobs)
        mu_beg	= np.zeros(self.nobs)
        mu_end	= np.zeros(self.nobs)
        X_beg	= np.zeros(self.nobs)
        Y_beg	= np.zeros(self.nobs)
        Z_beg	= np.zeros(self.nobs)
        X_end	= np.zeros(self.nobs)
        Y_end	= np.zeros(self.nobs)
        Z_end	= np.zeros(self.nobs)
        for o in range(1,self.nobs):
            mu_beg[o]=-(a*self.allobs[o].station.eci.x.to('m').value+ # [m]
                        b*self.allobs[o].station.eci.y.to('m').value+
                        c*self.allobs[o].station.eci.z.to('m').value+
                        d) / (a*self.allobs[o].ksi[0] +
                              b*self.allobs[o].eta[0]+
                              c*self.allobs[o].zeta[0])
            mu_end[o]=-(a*self.allobs[o].station.eci.x.to('m').value+ # [m]
                        b*self.allobs[o].station.eci.y.to('m').value+
                        c*self.allobs[o].station.eci.z.to('m').value+
                        d) / (a*self.allobs[o].ksi[self.npts[o]  -1]+
                              b*self.allobs[o].eta[self.npts[o] -1]+
                              c*self.allobs[o].zeta[self.npts[o]-1])      
            X_beg[o]= self.allobs[o].station.eci.x.to('m').value+self.allobs[o].ksi[0]              *mu_beg[o]  # [m]
            Y_beg[o]= self.allobs[o].station.eci.y.to('m').value+self.allobs[o].eta[0]          *mu_beg[o]  # [m]
            Z_beg[o]= self.allobs[o].station.eci.z.to('m').value+self.allobs[o].zeta[0]         *mu_beg[o]  # [m]
            X_end[o]= self.allobs[o].station.eci.x.to('m').value+self.allobs[o].ksi[self.npts[o]-1]  *mu_end[o] # [m]
            Y_end[o]= self.allobs[o].station.eci.y.to('m').value+self.allobs[o].eta[self.npts[o]-1] *mu_end[o]  # [m]
            Z_end[o]= self.allobs[o].station.eci.z.to('m').value+self.allobs[o].zeta[self.npts[o]-1]*mu_end[o]  # [m]
            traj_len[o] = np.sqrt( (X_beg[o]-X_end[o])**2 + 
                                   (Y_beg[o]-Y_end[o])**2 + 
                                   (Z_beg[o]-Z_end[o])**2) # [m]
        # now find the longest traj_len
        i_max = np.argmax(traj_len)
        X_beg = X_beg[i_max] # [m]
        Y_beg = Y_beg[i_max] # [m]
        Z_beg = Z_beg[i_max] # [m]
        X_end = X_end[i_max] # [m]
        Y_end = Y_end[i_max] # [m]
        Z_end = Z_end[i_max] # [m]
        #First approximation of the parameters
        self.XM = X_beg # [m]
        Y0M = Y_beg # [m]
        Z0M = Z_beg # [m]
        self.ksiR = (X_beg-X_end)/np.abs(X_beg-X_end)	# chosen
        etaR = (Y_beg-Y_end)/np.abs(X_beg-X_end)	# will be adjusted later
        zetaR = (Z_beg-Z_end)/np.abs(X_beg-X_end)	# will be adjusted later
        # normalize [self.ksiR,etaR,zetaR]
        [self.ksiR,etaR,zetaR] = [self.ksiR,etaR,zetaR]/np.linalg.norm(
                                 [self.ksiR,etaR,zetaR])
        #Find the parameters of the trajectory (straight line)
        self.param = [Y0M,Z0M,etaR,zetaR] # initial guess of parameters [m,m,none,none]
        pthf.fri_log.info("(Borovicka_n) first guess="+str(self.param))
        return
    
    # shift all the observation array by one
    def shift_obs(self):
        """Shift the array of Observation objects and reestimae first guess.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        None
             Shift the array of Observation objects and relaunch the initialize
             method
        
        """
        new_allobs = np.roll(self.allobs,1)
        # roll again if there is only one observation point
        # note: better than a while loop in case there are only 1-point observations...
        # in this case the program will abort when computing the orbit
        while (new_allobs[0].dim < 2):
            new_allobs = np.roll(new_allobs,1)
        self.allobs = new_allobs
        self.initialize() # so start all over again
        return
    
    def sort_obs_with_time(self,all_obs):
        """Sort all the observation object with time.
        
        In addition to sorting the Observation object with time, the initialize
        method is called again to get a first guess of the trajectory
        parameters.
        
        Parameters
        ----------
        all_obs : array of fripipe.Trajectory.definitions.Observation object
        
        Returns
        -------
        sorted_all_obs : array of fripipe.Trajectory.definitions.Observation object
            all_obs sorted as a function of time.
        
        """
        sorted_all_obs = []
        all_1stime = []
        for o in range(self.nobs):
            all_1stime.append((all_obs[o].time[0]-all_obs[0].time[0]).to('s').value)	# extract first time for all observations
        sorted_arg = np.argsort(all_1stime)		# sort first time
        for o in range(self.nobs):
            sorted_all_obs.append(all_obs[sorted_arg[o]])	# re-organize all_obs
        for o in range(self.nobs):
            pthf.fri_log.info('Observation ranked '+str(o)+self.allobs[o].station.name)
        self.initialize() # start all over again	
        return sorted_all_obs
    
    def sort_obs_with_elevation(self):
        """Sort all the observation object with elevation
        
        In addition to sorting the Observation object with elevation,
        the initialize method is called again to get a first guess
        of the trajectory parameters.
        
        Parameters
        ----------
        None
            
        
        Returns
        -------
        None
            
        
        """
        sorted_all_obs = []
        all_max_elev = []
        for o in range(self.nobs):
            all_max_elev.append(np.max(self.allobs[o].elevation.value))	# extract max elevation for all observations
        sorted_arg = np.argsort(all_max_elev)[::-1]		# sort Q from heighest to lowest
        for o in range(self.nobs):
            sorted_all_obs.append(self.allobs[sorted_arg[o]])		# re-organize all_obs
        self.allobs	= sorted_all_obs
        for o in range(self.nobs):
            pthf.fri_log.info('Observation ranked '+str(o)+' '+self.allobs[o].station.name+' max(elevation)='+str(all_max_elev[sorted_arg[o]]))
        self.initialize() # start all over again	
        return
    
    def sort_obs_sum_res(self):
        """Reorganize all observations as a function of sum of residuals.
        
        In addition to sorting the Observation object with sum of residuals,
        the initialize method is called again to get a first guess
        of the trajectory parameters.
        
        Parameters
        ----------
        None
            
        Returns
        -------
        None
            
        
        """
        all_sum_sep = []
        sorted_all_obs = []
        for o in range(self.nobs):
            all_sum_sep.append(self.allobs[o].omc.sum_sep.to('deg').value)
        sorted_arg	= np.argsort(all_sum_sep)[::] # sort from lowest to heighest
        for o in range(self.nobs): # re-organize all_traj
            sorted_all_obs.append(self.allobs[sorted_arg[o]])
        self.allobs	= sorted_all_obs
        self.initialize() # start all over again
        return
    
    def sort_obs_Q(self):
        """Reorganize all observations as a function of angle of convergence Q.
        
        In addition to sorting the Observation object with Q,
        the initialize method is called again to get a first guess
        of the trajectory parameters.
        
        Parameters
        ----------
        None
            
        Returns
        -------
        None
            
        
        
        """
        sorted_all_obs = []
        allQ		= []
        for o in range(self.nobs):
            allQ.append(self.allQ[o].to('deg').value)	
        sorted_arg	= np.argsort(allQ)[::-1]				# sort Q from heighest to lowest
        for o in range(self.nobs):					# re-organize all_traj
            sorted_all_obs.append(self.allobs[sorted_arg[o]])
        self.allobs	= sorted_all_obs
        for o in range(self.nobs):
            pthf.fri_log.info('Observation ranked '+str(o)+self.allobs[o].station.name)
        self.initialize()						# start all over again	
        return
    
    # computes (xi,eta,zeta) from (ra,dec) 
    def ad2xi_eta_zeta (self,ra,dec):
        """Computes (xi,eta,zeta) from (ra,dec).
        
        Parameters
        ----------
        ra : float or array of float
            right ascencion, in degrees
        dec : float or array of float
            declination, in degrees
        
        Returns
        -------
        xi : float or array of float
            normalized X-coordinate
        eta : float or array of float
            normalized Y-coordinate
        zeta : float or array of float
            normalized Z-coordinate
        
        """
        ra = ra*sp.rpd()				# convert to [rad]
        dec = dec*sp.rpd()  			# convert to [rad]
        xi = np.cos(dec)*np.cos(ra)		# unit vector x [-]  
        eta = np.cos(dec)*np.sin(ra)	# unit vector y [-] 
        zeta = np.sin(dec)				# unit vector z [-] 
        return xi,eta,zeta
    
    def conv_angle(self,plane1,plane2):
        """Computes the convergence angle between 2 planes.
        
        Parameters
        ----------
        plane1 : array of float
            equation coefficient of plane 1
        plane2 : array of float
            equation coefficient of plane 2
        
        Returns
        -------
        Q : float
            convergence angle between plane 1 and plane 2.
        
        """
        num = np.abs(plane1[0]*plane2[0]+
                     plane1[1]*plane2[1]+
                     plane1[2]*plane2[2])
        denum = np.sqrt( (plane1[0]*plane1[0]+
                          plane1[1]*plane1[1]+
                          plane1[2]*plane1[2])*
                         (plane2[0]*plane2[0]+
                          plane2[1]*plane2[1]+
                          plane2[2]*plane2[2]))
        cosQ = num/denum
        Q = np.arccos(cosQ) * u.rad
        pthf.fri_log.info("Convergence angle="+str(Q.to('deg')))
        return Q
    
    def plane_equation(self,station_eci,vect1,vect2):
        """Compute equation of a plane.
        
        Parameters
        ----------
        station_eci : fripipe.Trajectory.ECI object
            Coordinate of the station in ECI coordinates.
        vect1 : array of float
            3D-unit vector in the plane
        vect2 : array of float
            3D-unit vector in the plane
        
        Returns
        -------
        coeff : array of float
            the 4 coefficients of the equation of the plane.
        
        """
        a = vect1[1]*vect2[2]-vect2[1]*vect1[2]
        b = vect1[2]*vect2[0]-vect2[2]*vect1[0]
        c = vect1[0]*vect2[1]-vect2[0]*vect1[1]
        n = np.sqrt(a**2+b**2+c**2) 
        a = a/n
        b = b/n
        c = c/n
        d = -a*station_eci.x.to('m').value-b*station_eci.y.to('m').value-c*station_eci.z.to('m').value
        coeff = [a,b,c,d]
        return coeff
    
    def project_plane(self,station_eci,eqconst1,eqconst2,unit_met):
        """Project a unit vector onto a plane, from a given station.
        
        Parameters
        ----------
        station_eci : fripipe.Trajectory.ECI object
            Coordinate of the station in ECI coordinates.
        eqconst1 : array of float
            the 4 coefficients of the equation of plane 1.
        eqconst1 : array of float
            the 4 coefficients of the equation of plane 2.
        unit_met : array of float
            unit vector of observation of each point along the trajectory
            of the meteor
        
        Returns
        -------
        xyz : array of float
            3D-vector of position of projected observations unto the
            intersection plane from Ceplecha 1987, eqn 17, 18.
        """
        sz_xi = unit_met[2].size
        xyz = []
        an = eqconst1[2]*unit_met[1,:] - eqconst1[1]*unit_met[2,:] # a_n=eta_n *c_A-zeta_n*b_A
        bn = eqconst1[0]*unit_met[2,:] - eqconst1[2]*unit_met[0,:] # b_n=zeta_n*a_A-xsi_n *c_A
        cn = eqconst1[1]*unit_met[0,:] - eqconst1[0]*unit_met[1,:] # c_n=xsi_n *b_A-eta_c *a_A
        dn =-an*station_eci.x.to('m').value-bn*station_eci.y.to('m').value-cn*station_eci.z.to('m').value  # negative value because of SVD decomposition. see below
        eqconstn= np.asarray([an,bn,cn])
        eqplane1= eqconst1[0:3] # [a,b,c] of the plane eqn 1, useful for SVD decomposition
        d1 = eqconst1[3]        # d of the plane eqn 1, useful for SVD decomposition
        eqplane2= eqconst2[0:3] # [a,b,c] of the plane eqn 2, useful for SVD decomposition
        d2 = eqconst2[3]        # d of the plane eqn 2, useful for SVD decomposition
        # now projects each observation onto the plane
        for i in np.arange(0,sz_xi):
            Aeq =[eqplane1,eqplane2,eqconstn[:,i]] # all [a,b,c]-plane_equation in linear-vector
            Beq =[d1,d2,dn[i]] # all d-plane_equation in linear-vector
            U, W, V = np.linalg.svd(Aeq) # SVD decomposition
            N = W.size
            WP = np.zeros((N,N))
            for K in np.arange (0, N):
             if (np.abs(W[K]) >= 1.0e-5):
              WP[K, K] = 1.0/W[K]
            xyz.append( -( (np.inner(np.inner(np.transpose(V),WP),np.inner(np.transpose(U),Beq)))) )
        return np.asarray(np.transpose(xyz))
    
    def to_radiant(self,minimethod='Nelder-Mead') :
        """Compute the radiant by optimization.
        
        Optimisation of the trajectory computation using the Gradient method
         as described in Borovicka (1989)
        
        Parameters
        ----------
        None
            All the observation objects are already in store when the
            Borovicka_n object was created
        
        Returns
        -------
         radiant : fripipe.Trajectory.Radiant object
            apparent radiant (to be corrected for the Earth attraction
            and rotation)
        """
        #Find the parameters of the trajectory (straight line)
        #Gradient method ; unknowns : self.param=[YM, ZM, etaR, zetaR]
        pthf.fri_log.info(" Trajectory Optimisation")
        pthf.fri_log.info('initial param='+str(self.param))
        res = minimize(dist_2_lines, self.param, args=(self.allobs, self.ksiR, self.XM), jac = gradient_dist,method=minimethod)
        self.param = res.x # param=fitted parameters
        pthf.fri_log.info('fitted param ='+str(res.x))
        pthf.fri_log.info('res.success ='+str(res.success))
        pthf.fri_log.info('res.message ='+str(res.message))
        # XM,YM,ZM=XYZ_BEG of first point of meteor trajectory
        self.ini_point = [self.XM ,self.param[0],self.param[1]]
        # mean [xi,eta,zeta] (radiant unit vector; see eqn 29 Ceplecha 1987)
        XiEtaZeta_mean = np.array([self.ksiR,self.param[2],
                                   self.param[3]])/sp.vnorm([self.ksiR,
                                                              self.param[2],
                                                              self.param[3]])
        # creates a radiant object
        app_radiant	= Radiant()
        app_radiant.from_unit_vector(XiEtaZeta_mean[0],XiEtaZeta_mean[1],XiEtaZeta_mean[2])
        app_radiant.to_altaz(self.allobs[0].time[0],self.allobs[0].station)
        # compute anti-radiant rather than radiant if radiant under the horizon
        if (app_radiant.altaz.alt.to('deg').value < 0.0):
            app_radiant.invert()
            app_radiant.to_altaz(self.allobs[0].time[0],self.allobs[0].station)	# recompute the azimuth-elevation
        app_radiant.printout()
        return app_radiant
    
    def to_traj(self) :
        """compute all the possible trajectories from the observations and radiant
        
        Parameters
        ----------
        None
        
        Returns
        -------
        None
            
        Note
        ----
        The to_radiant method must be invoked prior to this one.
        
        """
        # Determination of the points on the trajectory
        # projection on the line previously computed
        all_unit_met = {}
        all_plane_s = {}
        all_traj = [] # all trajectories
        all_obs_c = [] # all computed observations
        all_sum_sep = [] # all sums of separations
        all_Q = [] # all convergence angles
        # compute unit vector and plane eqn for each observation set
        for o in range(self.nobs):
            # [xi,eta,zeta]_n : observation unit vectors
            unit_met = np.zeros((3,self.npts[o]))
            for i in np.arange (0,self.npts[o]):
                unit_met[:,i]=[self.allobs[o].ksi[i],
                               self.allobs[o].eta[i],
                               self.allobs[o].zeta[i]]
            # XYZ_BEG - XYZ_s
            XYZ_BEG_2_S=[self.ini_point[0]-self.allobs[o].station.eci.x.to('m').value,
                         self.ini_point[1]-self.allobs[o].station.eci.y.to('m').value,
                         self.ini_point[2]-self.allobs[o].station.eci.z.to('m').value]
            # plane defined by the station position, XYZ_BEG and meteor unit vector:
            # see eqn 30-31 Ceplecha 1987
            plane_s =-np.asarray(self.plane_equation(self.allobs[o].station.eci,XYZ_BEG_2_S,self.app_radiant.uvec))
            all_unit_met[o] = unit_met
            all_plane_s[o] = plane_s
        # now build the trajectories. Need 2 planes so start at 2nd obs and creates first traj separately
        for o in range(self.nobs): # loop over the observations. Note: there HAS to be a more pythonic way to do this...
            if (o==0):
                iend = self.nobs-1
            else:
                iend = 0
            pthf.fri_log.info(" Building trajectory from object "+self.allobs[o].station.name+" and "+self.allobs[iend].station.name)
            pthf.fri_log.debug('obs['+str(o)+'] from station '+self.allobs[o].station.name+' has '+str(self.allobs[o].dim)+' observations')
            pthf.fri_log.debug('obs['+str(iend)+'] from station '+self.allobs[iend].station.name+' has '+str(self.allobs[iend].dim)+' observations')
            Q = self.conv_angle(all_plane_s[o],all_plane_s[iend])	# convergence angle
            xyz = self.project_plane(self.allobs[o].station.eci,	# project unit vector unto plane
                                    all_plane_s[o],
                                    all_plane_s[iend],
                                    all_unit_met[o])
            traj = Trajectory(name=self.allobs[o].station.name)	# create Trajectory object
            traj.from_eci(np.transpose(np.asarray(xyz[0]))*u.m,			# fills in Trajectory object with ECI data
                          np.transpose(np.asarray(xyz[1]))*u.m,
                          np.transpose(np.asarray(xyz[2]))*u.m,
                          self.allobs[o].time)
            (rg,az,el) = traj.to_rgazel(self.allobs[o].station)		# compute range, azimuth, elevation
            appskycoo = traj.to_appskycoo(self.allobs[o].station)			# compute right ascension and declination
            I = []
            self.npts[o] = len(self.allobs[o].skycoo.ra)			# number of point in trajectory
            # compute absolute brightness
            for (mag,rng) in zip(self.allobs[o].Mag,rg):
                I.append(phot.MagtoI(mag.value,rng.value))
            # puts brightness into trajectory object
            traj.putI(I*u.W)
            # create computed observation object
            obs_c = Observation()
            # fills observation object with trajectory data
            obs_c.from_direct_input(self.allobs[o].station,
                                    self.allobs[o].time,
                                    appskycoo.ra,appskycoo.dec,az,el,rg,traj.I)
            # computes O-C
            self.allobs[o].compute_OMC(obs_c)
            self.allobs[o].omc.print_sum_sep()				# print sum of O-C
            # now stores the results
            all_obs_c.append(obs_c)					# appends the trajectory to all computed observations
            all_traj.append(traj)						# appends the trajectory to all trajectories
            all_sum_sep.append(self.allobs[o].omc.sum_sep.to('deg').value)# appends sum of all separations
            all_Q.append(Q)						# appends convergence angle
        pthf.fri_log.info('sum(all_sum_sep)='+str(np.sum(all_sum_sep)))		# sum of all sums of separation
        pthf.fri_log.info(str(len(all_traj))+' trajectories have been computed from all observation sets and the fitted apparent radiant')
        return all_traj,all_obs_c,all_Q# ,sum_all_sep
    
    def to_optimum_radiant_and_traj(self,minimethod='Nelder-Mead'):
        """get the optimum radiant and set of all trajectories that minimilizes the O-C
        
        Parameters
        ----------
        minimethod: string, optional
            name of method of minimization for the radiant
        
        Returns
        -------
        app_radiant: fripipe.Trajectory.Radiant object
            Radiant of the meteor
        alltraj: array of fripipe.Trajectory.Trajectory objects
            one Trajectory object per observation
        allobs: array of fripipe.Observation objects
            reorganized array of Observations
        allobs_c: array of fripipe.Observation objects
            all computed Observation objects
        allQ: array of convergence angles
        
        """
        # initialization
        min_omc_sep	= 999999999999.0*u.deg				# minimum sum of all separation O-C
        all_trajobs	= []
        # sort observations with elevation (might not be essential)
        self.sort_obs_with_elevation()
        pthf.fri_log.info('***** first observation station: '+self.allobs[0].station.name)
        self.app_radiant = self.to_radiant()	# compute radiant
        self.alltraj,self.allobs_c,self.allQ	= self.to_traj()	# compute trajectories and computed_observations
        return self.app_radiant,self.alltraj,self.allobs,self.allobs_c,self.allQ
