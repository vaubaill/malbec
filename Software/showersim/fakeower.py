
import os
import numpy as np
from scipy import interpolate
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable, vstack
from astropy.coordinates import EarthLocation, AltAz, SkyCoord
import spiceypy as sp
from pygc import great_circle

# MALBEC specific import
from missionplanner import  utils
from missionplanner.utils import log
from obsplanner.make_id import make_camera_name
from obsplanner import obsutils
#import obsplanner.read_shower_files
from showersim import shower_utils, fakeor, anchor, afm, fakeobs

# set home directory
home = os.getenv('HOME') + '/'

###################################################
# 
# DEFINITION OF THE ObservationPlanner OBJECT
# 
###################################################
class FakeOwer (object):
    """FakeOwer fake shower Object definition
    
    Parameters
    ----------
    TBF
    
    
    """
    # version of the program
    __version__='1.0'
    
    def __init__(self,metshw,eloc_center,max_dist,time_start,time_stop,num_fkr):
        """Initializes a FakeOwer fake meteor shower.
        
        Parameters
        ----------
        metshw : MeteorShower Object.
            Meteor shower to simulate.
        eloc_center : astropy.coordinates.EarthLocation Object.
            Location o fthe center of the shower ot simulate.
        max_dist : astropy.units.Quantity Object
            Maximum distance from eloc_center to simulate Fakeors.
        time_start : astropy.time.Time Object
            Start time of the simulation.
        time_stop : astropy.time.Time Object
            Stop time of the simulation.
        num_fkr : int
            Number of Fakeors to simulate.
        
        Returns
        -------
        None.
        
        """
        # ingest data
        self.metshw = metshw
        self.eloc_center = eloc_center
        self.max_dist = max_dist
        self.time_start = time_start
        self.time_stop = time_stop
        self.et_start = sp.str2et(self.time_start.isot)
        self.et_stop = sp.str2et(self.time_stop.isot)
        self.num_fkr = num_fkr
        
        return
    
    def compute_numet(self):
        """Compute all expected number of meteors for the 2 nacelles.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        # create output data
        numet_data,numet_tot = obsutils.create_numet_tables()        
        # compute ZHR as a function of time
        times = Time(self.geom_data['Time'],scale='utc')
        ZHR = self.shower.get_ZHR(times)
        
        # loop over nacelle names
        for (nacelle_name,num) in zip(self.list_nacelle_names,self.list_nacelle_numstr):
            # select data
            mask = self.area['camera']==nacelle_name
            area = self.area[mask]
            # compute HR given radiant elevation (=camera position dependent) and camera limiting magnitude
            # note that the meteor altitude correction factor is computed with the shower_utils.HR2numet routine
            # so it is NOT included it here or you'll take twice this effect into account.
            (HR,dmag_alt,f_radel,f_LM,LM_eff) = shower_utils.ZHR2HR(ZHR,self.shower.population_index,self.geom_data['RadAlt'+num],self.lim_mag)
            
            # compute expected number of meteors in both FOV, as seen from nacelle nacelle_name
            # note: do NOT change or set the h_meteor keyword, since the difference of altitude
            # is already taken into account in the computation of the Ared parameter
            (numet_sgl,f_area_sgl,f_time) = shower_utils.HR2numet(HR,area['reduced_single_area'],self.freq.to('hour'))
            # compute for double-station
            (numet_dbl,f_area_dbl,f_time) = shower_utils.HR2numet(HR,area['reduced_double_area'],self.freq.to('hour'))
                        
            # put data into QTable
            dim = len(times)
            tmp_tab = QTable([times,self.geom_data['et'],np.repeat(nacelle_name,dim),
                              ZHR,HR,
                              f_radel,np.repeat(f_LM,dim),np.repeat(f_time,dim),
                              f_area_sgl,f_area_dbl,numet_sgl,numet_dbl],names=numet_data.colnames)
            if len(numet_data):
                numet_data = vstack((numet_data,tmp_tab))
            else:
                numet_data = tmp_tab
            # compute total expected number of meteors
            num_tot_sgl = np.sum(tmp_tab['numet_sgl'])
            num_tot_dbl = np.sum(tmp_tab['numet_dbl'])
            numet_tot.add_row([nacelle_name,num_tot_sgl,num_tot_dbl])
        
        # save numet_data
        numet_data.sort('et')
        obsutils.set_numet_format(numet_data)
        numet_data.write(self.numet_file,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('Expected number of observed meteors saved in '+self.numet_file)
            
        # save total expected number of meteors in output file
        numet_tot.write(self.numet_tot_file,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('Total expected number of meteors saved in '+self.numet_tot_file)
        
        return
    



        
    def make_anchor_eloc(self,nclone,fov_center_eloc,fov_center_radius):
        """Make anchor EarthLocation.
        
        Parameters
        ----------
        nclone : integer
            Number of anchors clones to make.
        fov_center_eloc : astropy.coordinates.EarthLocation Object.
            Location of the middle point of center of the FOV of both cameras.
        fov_center_radius : astropy.unitsQuantity object
            Maximum physical distance between center of FOV and all FOV corners.
        
        Returns
        -------
        anchors_eloc : astropy.coordinates.EarthLocation object.
            Location of anchors in ITRF93.
        
        """
        log.debug('Make '+str(nclone)+' anchor elocs')
        fov_center_dist = np.sqrt(np.random.random(nclone)) * fov_center_radius
        fov_center_azmt = np.random.random(nclone) * 360.0*u.deg
        fov_center_gtcirc = great_circle(distance=fov_center_dist.to('m').value/2,
                                         azimuth=fov_center_azmt.to('deg').value,
                                         latitude=fov_center_eloc.lat.to('deg').value,
                                         longitude=fov_center_eloc.lon.to('deg').value)
        # make anchor eloc
        anchors_eloc = EarthLocation.from_geodetic(fov_center_gtcirc['longitude']*u.deg,
                                                   fov_center_gtcirc['latitude']*u.deg,
                                                   height=np.repeat(self.meteor_alt,nclone))
        
        # reorganise if there is only 1 element, to avoid returning an array of EarthLocation
        if nclone==1:
            anchors_eloc = anchors_eloc[0]
        return anchors_eloc
    
    def eloc_infov(self,nacelle_name,et,obj_eloc):
        """Check if a location is in FOV of a given nacelle camera.
        
        Parameters
        ----------
        nacelle_name : string
            MALBEC nacelle name.
        et : float
            SPICE time [sec after J2000].
        obj_eloc : astropy.coordinates.EarthLocation Object.
            Location of object to check in FOV.
        
        Returns
        -------
        in_fov : boolean
            True if in nacelle camera FOV. False otherwise.
        dist : astropy.units.Quantity object
            Physical distance between object and nacelle.
        
        """
        # retrieve camera name
        camera_name = make_camera_name(nacelle_name)
        # retrieve location of nacelle in ITRF93
        (eph, lt) = sp.spkezr(nacelle_name,et,'ITRF93','NONE','EARTH')
        # convert nacelle ephemeris into EarthLocation
        nac_eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
        # construct a vector from object to nacelle
        raydir = np.array([(obj_eloc.x - nac_eloc.x).to('km').value,
                           (obj_eloc.y - nac_eloc.y).to('km').value,
                           (obj_eloc.z - nac_eloc.z).to('km').value])
        # distance between nacelle and object
        dist = sp.vnorm(raydir) * u.km
        # check if object is in FOV
        infov = sp.fovray(camera_name, raydir, 'ITRF93', 'NONE', nacelle_name, et)
        return (infov,dist)
    
    
    def make_anchors_all_table(self):
        """Creates the global anchors_all QTable and set units and formats.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # make QTable
        self.anchors_all = QTable(names=['Time','et','lon','lat','alt','color'],
                                dtype=['object','float','float','float','float','object'])
        self.anchors_all['lon'].unit = u.deg
        self.anchors_all['lat'].unit = u.deg
        self.anchors_all['alt'].unit = u.m
        self.anchors_all['Time'].info.format = '%23s'
        self.anchors_all['et'].info.format  = '13.3f'
        self.anchors_all['lon'].info.format = '9.5f'
        self.anchors_all['lat'].info.format = '9.5f'
        self.anchors_all['alt'].info.format = '8.2f'
        self.anchors_all['color'].info.format = '%2s'
        return
        
    def store_anchors(self,et,anchors_eloc):
        """Store anchors EarthLocations into anchors_all global variable.
        
        Parameters
        ----------
        et : float
            SPICE time [sec after J2000].
        anchors_eloc : astropy.coordinates.EarthLocation Object.
            Location of anchors to store in self.anchors_all.
        
        Returns
        -------
        anchors_infov : astropy.table.QTable object.
            QTable of added anchors.
        
        """
        nclone = len(anchors_eloc)
        ets = np.repeat(et,nclone)
        # create local anchors QTable
        anchors_infov = QTable()
        anchors_infov['Time'] = Time(sp.et2utc(ets,"ISOC",2,50),scale='utc')
        anchors_infov['et'] = ets
        anchors_infov['lon'] = anchors_eloc.lon.to('deg')
        anchors_infov['lat'] = anchors_eloc.lat.to('deg')
        anchors_infov['alt'] = anchors_eloc.height.to('m')
        anchors_infov['color'] = np.repeat('b',nclone)
        # format anchors_infov
        anchors_infov['color'].info.format = '%2s'
        anchors_infov['Time'].info.format = '%23s'
        anchors_infov['et'].info.format  = '13.3f'
        anchors_infov['lon'].info.format = '9.5f'
        anchors_infov['lat'].info.format = '9.5f'
        anchors_infov['alt'].info.format = '8.2f'
        
        if self.anchors_all:
            self.anchors_all = vstack([self.anchors_all,anchors_infov])
        else:
            self.anchors_all = anchors_infov
        return anchors_infov
            
    def make_fakeor_times(self,nclone,et_start,et_stop):
        """Create fakeor times.
        
        Parameters
        ----------
        nclone : int
            Number of fakeor times to create.
        et_start : float
            Observation start time in SPICE format [sec after J2000].
        et_stop : float
            Observation stop time in SPICE format [sec after J2000].
        
        Returns
        -------
        data_time : astropy.table.QTable object
            Fakeor timing within observation run.
        
        """
        log.info('Making '+str(nclone)+' fakeor timings')
        num_times = 0
        names = ['Time','et']
        dtypes = ['object','float']
        data_time = QTable(names=names,dtype=dtypes)
        # setting min/max time of observability given radiant elevation
        loc_start_obs = np.where(self.geom_data['RadAlt1']>0.0*u.deg)[0]
        if len(loc_start_obs):
            et_strt_obs = self.geom_data['et'][loc_start_obs[0]]
            et_stop_obs = self.geom_data['et'][loc_start_obs[-1]]
        else:
            msg = '*** FATAL ERROR: the radiant NEVER rises given the observation constraints'
            log.error(msg)
            raise ValueError(msg)
        log.debug('et_strt_obs: '+str(et_strt_obs))
        log.debug('et_stop_obs: '+str(et_stop_obs))
        # set et_real_start
        et_real_strt = np.max([et_start,et_strt_obs])
        et_real_stop = np.min([et_stop,et_stop_obs])
        fwhm = self.shower.fwhm.to('s').value
        while num_times < nclone:
            # create random time within shower
            random_et = np.random.normal(loc=self.shower.et_max,scale=fwhm,size=nclone)
            # select time within observation run
            et_loc_start = random_et[np.where(random_et>et_real_strt)]
            n_loc_start = len(et_loc_start)
            if not n_loc_start:
                msg = '*** Warning: no time after start: please double check meteor max date and launch date.'
                log.warning(msg)
                #raise ValueError(msg)
                continue
            et_select = et_loc_start[np.where(et_loc_start<et_real_stop)]
            n_select = len(et_select)
            if not n_select:
                msg = '*** Warning: no time before stop: please double check meteor max date and launch date.'
                log.warning(msg)
                #raise ValueError(msg)
                continue
            times = sp.et2utc(et_select,"ISOC",2,50)
            subdata = QTable([times,et_select],names=names,dtype=dtypes)
            data_time = vstack([data_time,subdata])
            num_times = len(data_time['et'])
        
        # truncate output table to nclone data
        data_time = data_time[:nclone]
        
        # plot a histogram of fakeor/anchor times
        fkr_time_plt_file = self.anchor_file.replace('.dat','-time.png')
        rng_min = ((et_start - self.shower.et_max)*u.s).to('min').value
        rng_max = ((et_stop  - self.shower.et_max)*u.s).to('min').value
        nbins = np.min([int(nclone/2),10])
        obsutils.make_histimeplot(data_time['et'],self.shower.et_max,fkr_time_plt_file,rng=[rng_min,rng_max],nbins=nbins)
        log.info('plot saved in '+fkr_time_plt_file)
        return data_time
    
    def make_anchor_data(self,data_anchor_times):
        """Make anchor data.
        
        Parameters
        ----------
        data_anchor_times : astropy.table.QTable object
            Anchor timing within observation run.
            Columns are 'Time' and 'et', for
            astropy.time.Time in isot format
            and SPICE float time.
        
        Returns
        -------
        data_anchor : astropy.table.QTable object
            Anchor timing ('Time','et'), location ('lon','lat','alt'),
            azimuth ('Az'), zenith angle ('zen_angle') and 
            distance from camera1 and 2 ('dist1','dist2').
        
        """
        log.info('Making '+str(len(data_anchor_times['et']))+' fakeor locations (please wait...)')
        # set output data
        data_anchor = QTable(names=['lon','lat','alt','Az','zen_angle','dist1','dist2'])
        data_anchor['lon'].info.format = '9.5f'
        data_anchor['lat'].info.format = '9.5f'
        data_anchor['alt'].info.format = '8.2f'
        data_anchor['Az'].info.format = '9.5f'
        data_anchor['zen_angle'].info.format = '6.2f'
        data_anchor['dist1'].info.format = '9.2f'
        data_anchor['dist2'].info.format = '9.2f'
        
        # create interpolation function to get radiant sky coordinates as a function of et time.
        radiant_ra_func = interpolate.interp1d(self.geom_data['et'],self.geom_data['RadRa'])
        radiant_dec_func = interpolate.interp1d(self.geom_data['et'],self.geom_data['RadDec'])
        # interpolate radiant sky coordinates
        radiant_ra = radiant_ra_func(data_anchor_times['et'])
        radiant_dec = radiant_dec_func(data_anchor_times['et'])
        # make radiant SkyCoordinate
        radiant_skycoo = SkyCoord(ra=radiant_ra,dec=radiant_dec,unit='deg',frame='icrs')
        
        # loop over fakeor time
        for (et,rad_skycoo) in zip(data_anchor_times['et'],radiant_skycoo):
            # retrieve center of fov and max physical distance with all corners of fov
            (fov_center_eloc,fov_center_radius) = self.get_fov_center_maxrad(et,verbose=True)
            keepgoing = True
            # loop until fakeor in FOV of both nacelles
            while keepgoing:
                # make anchors locations
                anc_eloc = self.make_anchor_eloc(1,fov_center_eloc,fov_center_radius)
                # check if achor is in fov of camera1
                (infov1,dist1) = self.eloc_infov(self.nacelle_name1,et,anc_eloc)
                # check if anchor is in fov of camera2
                (infov2,dist2) = self.eloc_infov(self.nacelle_name2,et,anc_eloc)
                # if in both fov exit loop
                if (infov1 and infov2):
                    keepgoing = False
                    #log.debug('Anchor eloc: '+str(anc_eloc))
            # convert et into Time object
            time = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
            # compute radiant azimuth at time et, from fakeor location
            radiant_altaz = rad_skycoo.transform_to(AltAz(obstime=time,location=anc_eloc))
            # compute fakeor azimuth = radiant azimuth + 180 deg
            fakeor_az = (radiant_altaz.az.to('deg') + 180*u.deg)%(360.0*u.deg)
            # compute fakeor entry zenith angle = 90-radiant elevation
            fakeor_zenagl =  90.0*u.deg - radiant_altaz.alt.to('deg')
            # store anchor location into output data
            data_anchor.add_row([anc_eloc.lon.to('deg'),anc_eloc.lat.to('deg'),anc_eloc.height.to('m'),fakeor_az.to('deg'),fakeor_zenagl.to('deg'),dist1.to('m').value,dist2.to('m').value])
        # set data_anchor units
        data_anchor['lon'].unit = u.deg
        data_anchor['lat'].unit = u.deg
        data_anchor['alt'].unit = u.m
        data_anchor['Az'].unit = u.deg
        data_anchor['zen_angle'].unit = u.deg
        data_anchor['dist1'].unit = u.m
        data_anchor['dist2'].unit = u.m
        # add time in data_anchor
        data_anchor['Time'] = data_anchor_times['Time']
        data_anchor['et'] = data_anchor_times['et']
        data_anchor['et'].info.format = '13.3f'
        # rearrange data_anchor
        data_anchor.sort('et')
        data_anchor = data_anchor['Time','et','lon','lat','alt','Az','zen_angle','dist1','dist2']
        return data_anchor
    
    def create_anchor_data(self):
        """Create Anchor data for the observation run.
        
        Data are written in anchor_file file defined 
        in the 'FAKEOR' section of the configuration file.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        anchor_data : astropy.table.QTable
            Anchor data: Time,et,lon,lat,alt,Az,zen_angle,dist1,dist2,Mass
        
        """
        # check if anchor file exists
        go_make_anchor = True
        if (os.path.exists(self.anchor_file) and self.read_res):
            log.info('Read anchor file: '+self.anchor_file)
            anchor_data = QTable.read(self.anchor_file,format='ascii.fixed_width_two_line')
            anchor_data['lon'].unit = u.deg
            anchor_data['lat'].unit = u.deg
            anchor_data['alt'].unit = u.m
            anchor_data['Az'].unit = u.deg
            anchor_data['zen_angle'].unit = u.deg
            anchor_data['dist1'].unit = u.m
            anchor_data['dist2'].unit = u.m
            anchor_data['Mass'].unit = u.kg
            # now check if the number of anchors is the expected one
            anc_num = len(anchor_data['lon'])
            if (anc_num==self.nclone):
                # no need to make Anchors
                go_make_anchor = False
            else:
                msg = '*** The Anchor file '+self.anchor_file+' has '+str(anc_num)+' data instead of '+str(self.nclone)+' => re-doing Anchors.'
                log.warning(msg)
        if go_make_anchor:
            # make fakeors times within start/stop observation run, with time margins to make sure all quantities are defined.
            fakeor_times = self.make_fakeor_times(self.nclone,self.start+0.01,self.stop-0.01)
            # create a population of magnitude or mass given meter shower properties
            self.shower.create_masspop(N=self.nclone,min_mass=self.fkr_min_mass)
            # make fakeors locations
            anchor_data = self.make_anchor_data(fakeor_times)
            # put absolute magnitude into data_anchor
            anchor_data['Mass'] = self.shower.pop_mass
            anchor_data['Mass'].info.format = '7.2E'
            # save anchors into dat file
            anchor_data.write(self.anchor_file,format='ascii.fixed_width_two_line',formats={'Time':'%23s'},overwrite=True)
            log.info('Anchor data saved in '+self.anchor_file)
        return anchor_data
    
    def make_fkr_summary_data(self):
        """Make Fakeor summary QTable.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        fkr_summary_data = QTable(names=['Time','et','id','cam','min_mag','nobs'],dtype=['object','float','int','str','float','int'])
        fkr_summary_data['Time'].info.format = '%23s'
        fkr_summary_data['et'].info.format = '13.3f'
        fkr_summary_data['min_mag'].info.format = '6.2f'
        fkr_summary_data['nobs'].info.format = '4d'
        return fkr_summary_data
    
    def create_fakeors(self):
        """Create Fakeors.
        
        Parameters
        ----------
        
        Returns
        -------
        
        
        """
        # create Anchor data for the observation duration
        anchor_data = self.create_anchor_data()
        
        # make fakeor summary output data
        fkr_summary_data = self.make_fkr_summary_data()
        
        # loop over all anchor objects
        for (fkr_id,anc_data) in zip(range(self.nclone),anchor_data):
            fmt = '{:0'+str(len(str(self.nclone)))+'d}'
            fkr_id_str = fmt.format(fkr_id)
            # make Fakeor Time object
            time = Time(anc_data['Time'],scale='utc')
            # set Fakeor name root
            fkr_name_root = utils.time2name(time,extn='')
            # make Anchor object
            anc_name = 'ANC' + fkr_id_str
            anc = anchor.Anchor(name=anc_name)
            anc_eloc = EarthLocation.from_geodetic(anc_data['lon'].to('deg'),anc_data['lat'].to('deg'),height=anc_data['alt'].to('m'))
            anc.from_eloc(anc_eloc,time,anc_data['Az'].to('deg'))
            anc_file = self.anchor_out_dir + anc_name + '.dat'
            anc.save(anc_file)
            anc_spice_topoframe_file = self.anchor_out_dir + anc_name + '.fk'
            anc_spice_id = int(self.config['FAKEOR']['anchor_id']) + fkr_id
            anc.make_topolike_frame(spice_id=anc_spice_id,spice_topoframe_file=anc_spice_topoframe_file)
            # make AFM object
            log.info('create AFM object')
            afmo = afm.AFM()
            # if AFM data are taken from a collection of already performed simulations
            if self.afm_data_mode=='collect':
                # retrieve already performed simulations.
                afm_intfile = afmo.get_intfile_fromcollection(self.shower.Ventry,anc_data['zen_angle'],anc_data['Mass'],coldir=self.fkr_coldir)
                # if integration not found in collection, make it
                if not afm_intfile:
                    afm_intfile = afm.make_afm_collection([self.shower.Ventry],[anc_data['zen_angle']],[anc_data['Mass']],out_dir=self.fkr_coldir)[0]
            # if AFM data are created for each fakeor
            else:
                afmconf = self.afm_out_dir + fkr_name_root + '.in'
                afm_dummy_radius = 0.01*u.m
                afmo.write_config(afmconf,time,self.shower.Ventry,anc_data['zen_angle'],self.camera.fps,afm_dummy_radius,self.afm_out_dir,mass=anc_data['Mass'])
                afm_intfile = afmo.launch(config_file=afmconf,sub=True)
            log.debug('afm_intfile : '+str(afm_intfile))
            if not os.path.exists(afm_intfile):
                msg = '*** FATAL ERROR: afm int file '+afm_intfile+' was NOT created'
                log.error(msg)
                raise IOError(msg)
            # read AFM data
            afmo.read_int(afm_intfile)
            # check number of data
            if (len(afmo.data)<2):
                msg = '*** There is at most one data point in file '+afm_intfile+' -> Skipped.'
                log.warning(msg)
                continue
            # make Fakeor object
            log.info('create fakeor object')
            fkr_name = 'FKR' + fkr_id_str
            fkr = fakeor.Fakeor(name=fkr_name)
            fkr.set_log_level(self.logdict[self.config['USER']['log_level']])
            # put AFM and Anchor objects into Fakeor
            fkr.from_afm_anchor(afmo,anc)
            # set Fakeor SPK, dat and kml file names, and SPICE id
            fkr_spk_file = self.fkr_outdir + fkr_name + '.spk'
            fkr_dat_file = fkr_spk_file.replace('.spk','.dat')
            fkr_kml_file = fkr_spk_file.replace('.spk','.kml')
            fkr_spice_id = int(self.config['FAKEOR']['fakeor_id']) - fkr_id
            # convert Fakeor into SPK, save in dat and kml formats
            fkr.to_spk(fkr_name,fkr_spice_id,fkr_spk_file)
            fkr.save(fkr_dat_file)
            # create fake observations from each nacelle
            for nacelle_name in [self.nacelle_name1,self.nacelle_name2]:
                # set fakeobs output file name
                obs_file = self.fkr_outdir + fkr_name + '_'+nacelle_name+'-obs.dat'
                # make FakeObs object
                fkobs = self.make_fakeobs(fkr,nacelle_name,obs_file)
                # make a fake observation summary
                (isobs,fkr_time,fkr_et_start,fkr_et_stop,min_mag,nobs) = fkobs.make_summary()
                # check if the Fakeor is observable
                if isobs:
                    # put data into fake observations summary table
                    fkr_summary_data.add_row([fkr_time.isot,fkr_et_start,fkr_spice_id,nacelle_name,min_mag,nobs])
                    # save Fakeor into kml
                    fkr.to_kml(fkr_kml_file,et_start=fkr_et_start,et_stop=fkr_et_stop)
            log.debug('Fake observations done.')
            
            # unload fakeor spk
            log.debug('unloading Fakeor and Anchor SPICE kernels')
            sp.unload(fkr_spk_file)
            sp.unload(anc.bsp_file)
            sp.unload(anc.spice_topoframe_file)
            log.debug('kernels: '+' '.join([anc.spice_topoframe_file,anc.bsp_file,fkr_spk_file])+' unloaded')
        log.info('All Fakeors done')
        
        # save Fakeor summary
        nb_fkrsum = len(fkr_summary_data)
        log.info('There are '+str(nb_fkrsum)+' fakeor summary data to write')
        if nb_fkrsum:
            self.fkr_sum_file = self.config['FAKEOR']['fakeor_file']
            log.info('saving fakeor summary into '+self.fkr_sum_file)
            fkr_summary_data.write(self.fkr_sum_file,format='ascii.fixed_width_two_line',overwrite=True)
            log.info('Fakeor summary data saved in '+self.fkr_sum_file)
            # plot fakeor summary data: time histogram and mag vs time, single and double station observation
            # plot a histogram of observed fakeor times
            fkr_time_plt_file = self.fkr_sum_file.replace('.dat','-time.png')
            rng_min = ((np.min(fkr_summary_data['et']) - self.shower.et_max) * u.s).to('min').value
            rng_max = ((np.max(fkr_summary_data['et']) - self.shower.et_max) * u.s).to('min').value
            nbins = np.min([int(self.nclone/2),10])
            obsutils.make_histimeplot(fkr_summary_data['et'],self.shower.et_max,fkr_time_plt_file,rng=[rng_min,rng_max],nbins=nbins)
        else:
            log.warning('There is no fakeor observable given the constrains!')
        return
    
    def make_fakeobs(self,fkr,nacelle_name,fakeobs_file):
        """Make fake observation from a given camera.
        
        Parameters
        ----------
        fkr : showersim.fakeor.Fakeor Object.
            Fakeor to be observed.
        nacelle_name : string
            Nacelle (SPICE spacecraft) name.
        fakeobs_file : string
            Output fake observation file name.
        load_spk : boolean, optional
            If True, the nacelle kernels are loaded.
            Default is True.
        
        Returns
        -------
        fkobs : showersim.fakeobs.FakeObs object
            Fake Observation
        
        """
        log.debug('Making FakeObservation for '+fkr.name+' as seen from '+nacelle_name)
        # make FakeOBs Object
        fkobs = fakeobs.FakeObs(name=fkr.name)
        fkobs.set_log_level(self.logdict[self.config['USER']['log_level']])
        # fill in FakeObs data
        fkobs.from_FNCF(fkr,nacelle_name,self.cov,self.lim_mag,self.camera.fps)
        # save FakeOBs data
        if (fkobs.nobs):
            fkobs.save(fakeobs_file)
            log.debug(str(fkobs.nobs)+' fake observations saved in '+fakeobs_file)
        else:
            log.debug('There is no observable data for fakeor: '+fkr.name)
        return fkobs
        
    
    
if __name__ == '__main__':
    
    fs = FakeShower()
else:
    log.debug('successfully imported')