"""Test afm collection

"""


import os
import sys
import math
import logging
import subprocess
import numpy as np
from astropy.table import QTable
import astropy.units as u
from astropy.time import Time as Time

from showersim import afm

# set home directory
home = os.getenv('HOME') + '/'

# --------------------------------------------------------------------------------
# create default logger object
# --------------------------------------------------------------------------------
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.propagate = True
log_fmt = '%(levelname)s %(filename)s %(lineno)d  (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.DEBUG)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)


all_Velocity = [25.53,25.94,25.93]*u.km/u.s
all_zenith_angle_range = [36.9,37.8] *u.deg
all_Mass_range = [1.0E-02,1.0E-03,1.5E-04]*u.kg


"""
out_dir = './tests/'
afm.make_afm_collection(all_Velocity,all_zenith_angle_range,all_Mass_range,fps=100/u.s,out_dir=out_dir)
"""

out_dir = '/data/processed/Fakeor/AFM_simul/AFM_poirier/'
collection = afm.make_afm_collection(all_Velocity,all_zenith_angle_range,all_Mass_range,fps=100/u.s,out_dir=out_dir)
log.info('collection : '+str(collection))
