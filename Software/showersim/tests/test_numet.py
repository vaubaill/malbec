import numpy as np
import astropy.units as u
from astropy.time import Time, TimeDelta
from astropy.table import QTable,Table, Column
from astropy.coordinates import EarthLocation, AltAz, SkyCoord, Angle, Latitude, Longitude

from showersim import distrib,shower_utils,shower_radiant,meteorshower

# define a meteor shower
shower = meteorshower.MeteorShower('Arietids',41.1,2.5,
                                        ra=42.0*u.deg,dec=25.0*u.deg,
                                        dra=1.0*u.deg/u.d,ddec=0.4*u.deg/u.d,
                                        min_mag=-4.0,N=1000,
                                        ZHR=100.0/u.hr,
                                        fwhm=1.0*u.hr,
                                        time_max=Time('2019-05-23T09:10:00',scale='utc'))

lim_mag = 6.0*u.mag

# set observation time start and stop
obstime_start = Time('2019-05-23T08:00:00',scale='utc')
obstime_stop  = Time('2019-05-23T10:00:00',scale='utc')
# set observation location
eloc = EarthLocation.from_geodetic(lon=-1.0,lat=25.0,height=0.0*u.m)



t = obstime_start
dt = TimeDelta(1.0*u.min)

while (t<=obstime_stop):
    # compute ZHR at time t
    ZHR = shower.get_ZHR(t)
    # compute radiant elevation
    shower.radiant.coo.transform_to(AltAz(obstime=t,location=eloc))
    # get naked eye HR at time t
    HR = shower.get_eyeHR(eloc,t,lim_mag)
    print(ZHR,HR,shower.radiant.altaz.alt.to('deg'))
    t = t + dt
    
