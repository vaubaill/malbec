"""Test population generation

"""
import numpy as np
import powerlaw
import matplotlib.pyplot as plt
from distrib import pareto_gen,powerlaw2_gen,powerlaw_gen,powerlaw3_gen

n=1000
ymin=2
ymax=6
index=-2.10

pop1=powerlaw_gen(index,ymin,ymax,n)
pop2=powerlaw2_gen(index,ymin,n)
pop3=pareto_gen(index,ymin,n)
pop4=powerlaw3_gen(index,ymin,n) # - 6

for pop in [pop1,pop2,pop3,pop4]:
    print('----------')
    print(len(pop))
    print(np.min(pop))
    print(np.max(pop))
    fit = powerlaw.Fit(pop)
    print(fit.power_law.alpha)
    print(fit.power_law.sigma)
    print(fit.power_law.xmin)
    print(fit.power_law.xmax)
    fig=plt.figure()
    powerlaw.plot_pdf(pop, color='b')
    plt.show()
    """
    fig = fit.plot_ccdf(linewidth=3)
    fit.power_law.plot_ccdf(ax=fig, color='r', linestyle='--')
    fig.show()
    """
