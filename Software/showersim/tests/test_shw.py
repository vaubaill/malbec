# test meteorshower script

import numpy as np
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation, AltAz, SkyCoord, Angle, Latitude, Longitude

import shower_radiant
import meteorshower

name = 'Testids'
Ven = 55*u.km/u.s
tmax = Time('2020-06-30T03:00:00.000',scale='utc')
fwhm = 24*u.hr
zhr = 50 / u.hr
r = 2.7
ra = 10*u.deg
dec = 50*u.deg
dra=1.0*u.deg/u.d
ddec=0.5*u.deg/u.d

# observation conditions
eloc = EarthLocation.from_geodetic(-0.2*u.deg, 49.0*u.deg, 100*u.m)
time_max = Time('2020-06-30T02:30:00.000',scale='utc')
time = Time('2020-06-29T01:30:00.000',scale='utc')


# create a Shower_Radiant object
radiant = shower_radiant.Shower_Radiant(ra=ra,dec=dec,dra=dra,ddec=ddec,time_max=time_max)
radiant.info()

# create a MeteorShower object
shw = meteorshower.MeteorShower(name,Ven,r,radiant=radiant,min_mag=-4.0,N=1000,ZHR=zhr,fwhm=fwhm,time_max=time_max)
shw.info()

# get zhr at specific time
zhr=shw.get_ZHR(time)

# get entry angle
shw.get_entry_angle(eloc,time)

# make artificial population
shw.create_pop()


print('all good!')
