"""Test afm package

"""


import os
import sys
import math
import logging
import subprocess
import numpy as np
from astropy.table import QTable
import astropy.units as u
from astropy.time import Time as Time

from showersim import afm


afm_exe='/home/vaubaill/PROJECTS/PODET/PODET-MET/ADMIRE/AFM/outil26_AFM/exe/AFM.x'

# create AFM object
print('create AFM object')
afmo = afm.AFM(afm_exe=afm_exe,afm_dataout='./user/out/')
print('ok')

# write configuration file
afmconf = '/Users/vaubaill/Mirror/PROJECTS/PODET/PODET-MET/MALBEC/malbec/Software/showersim/tests/config_afm.in'
outpath = '/tmp/'
time = Time('2020-01-01T01:01:01.010',scale='utc')
velocity = 65.5*u.km/u.s
entry_angle = 35.5*u.deg
fps = 100/u.s
rad=1.0E-03*u.m
print('write config file: '+afmconf)
afmo.write_config(time,velocity,entry_angle,fps,radius=rad,config_file=afmconf,outpath=outpath)
print('ok')

# launch AFM
print('launch ')
outintfile = afmo.launch(config_file=afmconf,sub=True)
print('ok')

# read output data
print('read output data: '+outintfile)
afmo.read_int(outintfile)
print('ok')

print('All ok')
