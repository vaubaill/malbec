from showersim import shower_utils
from astropy.coordinates import EarthLocation
from astropy.time import Time
import astropy.units as u
import numpy as np
import nrlmsise00

"""
# test 2 points
eloci=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=0.0*u.m)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=10.0*u.m)
t=Time.now()

(d10,rho10) = shower_utils.density_distance(eloci,elocf,t)
print(d10)
print(rho10)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=20.0*u.m)
(d20,rho20) = shower_utils.density_distance(eloci,elocf,t)
print(d20)
print(rho20)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=100.0*u.m)
(d100,rho100) = shower_utils.density_distance(eloci,elocf,t)
print(d100)
print(rho100)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=1000.0*u.m)
(d1000,rho1000) = shower_utils.density_distance(eloci,elocf,t)
print(d1000)
print(rho1000)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=10000.0*u.m)
(d10000,rho10000) = shower_utils.density_distance(eloci,elocf,t)
print(d10000)
print(rho10000)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=100000.0*u.m)
(d100000,rho100000) = shower_utils.density_distance(eloci,elocf,t)
print(d100000)
print(rho100000)


print(d20/d10)
print(d100/d10)
print(d1000/d100)
print(d10000/d1000)
print(d100000/d10000)

print('========')
elocf=EarthLocation.from_geodetic(0.0*u.deg,52.0*u.deg,height=100000.0*u.m)
(d200000,rho200000) = shower_utils.density_distance(eloci,elocf,t)
print(d200000)
print(rho200000)
print(d200000/d100000)
"""

"""
eloci=EarthLocation.from_geodetic(0.0*u.deg,55.0*u.deg,height=10.0*u.km)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=100.0*u.km)
t=Time.now()


airmass = shower_utils.compute_airmass(eloci,elocf,t)
dmag = shower_utils.Roth1994(airmass)
print('airmass: ',airmass)
print('dmag: ',dmag)
"""

"""
d_g,rho_g = shower_utils.unit_air_mass(eloci.lon,eloci.lat,t)
print('d_g=',d_g)
print('rho_g=',rho_g)
d,rho = shower_utils.density_distance(eloci,elocf,t)
print('d=',d)
print('rho=',rho)
print('d_g/d=',d_g/d)
print('rho_g/rho=',rho_g/rho)
print('d/d_g=',d/d_g)
print('rho/rho_g=',rho/rho_g)
"""


"""
# test 2 points
eloci=EarthLocation.from_geodetic(0.0*u.deg,48.0*u.deg,height=0.0*u.m)
elocf=EarthLocation.from_geodetic(1.0*u.deg,50.0*u.deg,height=100.0*u.m)

eloci=EarthLocation.from_geocentric(1*u.km,2*u.km,3*u.km)
elocf=EarthLocation.from_geocentric(2*u.km,3*u.km,4*u.km)
t=Time.now()
eloco = shower_utils.compute_point_along_sight(eloci,elocf)
eloco.x
eloco.y
eloco.z
eloco.lon
eloco.lat
eloco.height.to('m')
"""

# test n2n points
"""
n=4
npts=10
eloci=EarthLocation.from_geocentric(1*u.km,2*u.km,3*u.km)
elocf=EarthLocation.from_geocentric([2,3,4,5]*u.km,[4,5,6,7]*u.km,[13,14,15,16]*u.km)
eloco = shower_utils.compute_one2many_point_along_sight(eloci,elocf,npoints=npts)
eloco.x
eloco.y
eloco.z
eloco.z.reshape(n,npts)

eloci=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=0.0*u.km)
elocf=EarthLocation.from_geodetic([0,0,0,0]*u.deg,[51,52,53,54]*u.deg,height=[10,10,100,150]*u.km)
eloco = shower_utils.compute_one2many_point_along_sight(eloci,elocf,npoints=npts)
eloco.height.reshape(n,npts)
t=Time(np.repeat(Time.now(),n*npts))
data = nrlmsise00.msise_flat(t.to_datetime(),eloco.height.to('km').value,eloco.lat.to('deg').value,eloco.lon.to('deg').value,150, 150, 4)
ndata=data.reshape(n,npts,11)
"""


n=4
npts=1000
eloci=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=0.0*u.km)
elocf=EarthLocation.from_geodetic([0,0,0,0]*u.deg,[50,50,50,50]*u.deg,height=[1,10,100,150]*u.km)
elocf2=EarthLocation.from_geodetic([0,0,0,0]*u.deg,[51,52,53,54]*u.deg,height=[100,100,100,100]*u.km)
# for test
eloco,npoints,dist = shower_utils.compute_point_along_sight_one2many(eloci,elocf,npoints=npts)
print('eloco: ')
print(eloco.to_geodetic())
t=Time.now()
(d,rho) = shower_utils.density_distance_one2many(eloci,elocf,t,npoints=npts)
#print('t: ',t)
print('d: ',d)
print('rho: ',rho)
print('=============================')


airmass = shower_utils.compute_airmass_one2many(eloci,elocf2,t)
print('airmass: ',airmass)

print('=============================')


"""
for eloctmp in elocf:
    (d,rho) = shower_utils.density_distance(eloci,eloctmp,t)
    print('d_tmp: ',d)
    print('rho_tmp: ',rho)

print('=   ==   ==   ==   ==   =')
"""

for eloctmp in elocf2:
    airmass = shower_utils.compute_airmass(eloci,eloctmp,t)
    print('airmass=',airmass)


print('= = = = = = = = = = = = = = = ')



"""
(d,rho) = shower_utils.compute_airmass_one2many(eloci,elocf,t,npoints=npts)
print('d_g: ',d_g)
print('rho_g: ',rho_g)

print('+++++++++++++++++++++++++++++++++++++')
"""

"""
# test
d_g,rho_g = shower_utils.density_distance_from_ground(eloci.lon,eloci.lat,t)
print('d_g: ',d_g)
print('rho_g: ',rho_g)

print('+ + + + + + + + + + + + + + + + + + + + + + ')

airmass = shower_utils.compute_airmass_one2many(eloci,elocf,t)
print('airmass: ',airmass)
"""

print('ok')




"""
wavelength=600.0*u.nm
n = shower_utils.air_refractive_index(wavelength)
print('n: ',n)
sigma_r = shower_utils.RayleightScatteringCoeff(wavelength)
print('sigma_r: ',sigma_r)
"""

"""
wavelength=600.0*u.nm
eloci=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=0.0*u.m)
elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=10.0*u.m)
#elocf=EarthLocation.from_geodetic(0.0*u.deg,50.0*u.deg,height=100.0*u.km)
t=Time.now()
time=t

#(d,rho) = shower_utils.density_distance(eloci,elocf,t)
"""

"""
dm = shower_utils.KoschnyZender2000(eloci,elocf,time,wavelength=wavelength)
print('dm: ',dm)
"""
