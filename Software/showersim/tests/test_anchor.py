"""Test anchor object

"""
import os
import logging
import shutil
import time
import numpy as np
import astropy.units as u
import spiceypy as sp
from astropy.time import Time
from astropy.table import QTable,Table
from astropy.coordinates import EarthLocation, AltAz, SkyCoord, Angle, Latitude, Longitude


from showersim import anchor

# create an eloc
lon = -0.7*u.deg
lat = 43.3 *u.deg
alt = 100000.0 *u.m
eloc = EarthLocation.from_geodetic(lon,lat,height=alt)
time = Time('2020-01-01T01:01:01.000',scale='utc')
azimuth = 120*u.deg
# create an Anchor object
anc = anchor.Anchor()
anc.from_eloc(eloc,time,azimuth)


anc.save('test_anc.dat')

spice_toopoframe_file='Anchor.fk'

pinpoint = '/home/vaubaill/MISC-UTIL/NAIF/toolkit/exe/pinpoint'

anc.make_topolike_frame(pinpoint=pinpoint)

anc.load('test_anc.dat')

print('Anchor ok')
