"""Fake meteor observation.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project.


"""
import logging
import numpy as np
import spiceypy as sp
from scipy import interpolate
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable

from missionplanner import utils
from showersim import phot, afm
from obsplanner.make_id import make_camera_name,make_topoframe_name
#from reduction.camera import Camera
from missionplanner.utils import log


afmo=afm.AFM()


class FakeObs(object):
    """Fake meteor observation FakeObs object.
    
    """
    # number of obervations
    @property
    def nobs(self):
        return self.__nobs
    @nobs.setter
    def nobs(self,nobs):
        self.__nobs = nobs
    
    # Spacecraft name
    @property
    def spcft_name(self):
        return self.__spcft_name
    @spcft_name.setter
    def spcft_name(self,spcft_name):
        self.__spcft_name = spcft_name
    
    # Spacecraft frame name
    @property
    def spcft_frame_name(self):
        return self.__spcft_frame_name
    @spcft_frame_name.setter
    def spcft_frame_name(self,spcft_frame_name):
        self.__spcft_frame_name = spcft_frame_name
    
    #def __init__(self,fkr_spk,fkr_name,cam_id,load_spk=True,name='FakeObs',outdir='./'):
    def __init__(self,name='FakeObs'):
        """Initializes a FakeObs object.
        
        Parameters
        ----------
        name : str, optional
            name of the Fakeor objects. Default is 'FakeObs'.
        
        Returns
        -------
        None.
        
        """
        # set name
        self.name = name
        # set default variables
        self.nobs=0
        return
    
    def set_log_level(self,level=logging.DEBUG):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG
        
        Returns
        -------
        None.
        
        """
        # set log level
        log.setLevel(level)
        return 
    
    
    
    
    def set_spacecraft(self,spcft_name,spcft_frame_name,spcft_kernel=None,spcft_frame_kernel=None):
        """Set spacecraft on which instruments are mounted.
        
        Note that a spacecraft might simply be a topographic station.
        
        Parameters
        ----------
        spcft_name : string
            Spacecraft SPICE name.
        spcft_frame_name : string
            Spacecraft SPICE frame name.
        spcft_kernel : string, optional
            SPICE kernel file to load, if needed.
            
        Returns
        -------
        None.
        
        """
        # set variable names
        self.spcft_name = spcft_name
        self.spcft_frame_name = spcft_frame_name
        # load spacecraft kernel if needed
        if spcft_kernel:
            self.spcft_kernel = spcft_kernel
            log.info('Loading spacecraft kernel '+self.spcft_kernel)
            utils.check_file(self.spcft_kernel,log)
            sp.funsh(self.spcft_kernel)
            # retrieve spacecrfat id and coverage
            self.spcft_id = sp.spkobj(self.spcft_kernel)[0]
            self.cov = sp.spkcov(self.spcft_kernel,self.spcft_id)
            self.start,self.stop = sp.wnfetd(self.cov,0)
            log.info('spcft_id'+str(self.spcft_id))
            log.info('coverage: start: '+sp.et2utc(self.start,'ISOC',2,50))
            log.info('coverage: stop: '+sp.et2utc(self.stop,'ISOC',2,50))
        # load spacecraft frame kernel, if needed
        if spcft_frame_kernel:
            self.spcft_frame_kernel = spcft_frame_kernel
            log.info('Loading spacecrfat frame kernel: '+self.spcft_frame_kernel)
            utils.check_file(self.spcft_frame_kernel,log)
            sp.funsh(self.spcft_frame_kernel)
        return
    
    def add_instrument(self,instrument):
        """Add an instrument to the spacecraft (or simply a topographic station).
        
        Parameters
        ----------
        instrument : showersim.instrument.Instrument Object.
            Instrument.
        
        Returns
        -------
        None
        
        
        """
        # set variables
        self.instrument = instrument
        
        
        return
    
    def make_data(self):
        """Make Fake observation data QTable.
        
        Parameters
        ----------
        None.
        
        Returns.
        --------
        None.
        
        
        """
        self.data = None
        self.data = QTable(names=['Time','et','rel_time','Dist','RA','DEC','Az','El','appI','appMag'],
                           dtype=['object','float','float','float','float','float','float','float','float','float'])
        # set units
        self.data['rel_time'].unit = u.s
        self.data['Dist'].unit = u.m
        self.data['RA'].unit = u.deg
        self.data['DEC'].unit = u.deg
        self.data['Az'].unit = u.deg
        self.data['El'].unit = u.deg
        self.data['appI'].unit = u.W
        self.data['appMag'].unit = u.mag
        # set format
        self.data['Time'].info.format = '%23s'
        self.data['et'].info.format = '13.3f'
        self.data['rel_time'].info.format = '8.4f'
        self.data['Dist'].info.format = '13.3f'
        self.data['RA'].info.format = '7.3f'
        self.data['DEC'].info.format = '7.3f'
        self.data['Az'].info.format = '7.3f'
        self.data['El'].info.format = '7.3f'
        self.data['appI'].info.format = '12.1f'
        self.data['appMag'].info.format = '7.3f'
        return
    
    def from_FNCF(self,fkr,nacelle_name,coverage,lim_mag,fps,camera_name=None,camera_topoframe_name=None):
        """Fill in FakeObs from a Fakeor, nacelle, coverage, fps.
        
        Parameters
        ----------
        fkr : showersim.fakeor.Fakeor Object.
            Fakeor
        nacelle_name : string
            SPICE nacelle name.
        coverage : SPICE time window.
            Effective observation time window.
        lim_mag : astropy.units.Quantity object.
            Camera limiting magnitude, in [mag]
        fps : astropy.units.Quantity object.
            Camera frame rate, in [s-1].
        
        Returns
        -------
        None.
        
        """
        # make camera name if needed
        if not camera_name:
            camera_name = make_camera_name(nacelle_name)
        if not camera_topoframe_name:
            camera_topoframe_name = make_topoframe_name(nacelle_name)
        #camera_orientation_frame_name = make_nacelle_orientation_frame_name(nacelle_name)
        # retrieve Fakeor start/stop time
        fkr_cov = sp.spkcov(fkr.spk_file,fkr.spice_id)
        # narrow down the time window
        cov = sp.wnintd(coverage,fkr_cov)
        fob_start,fob_stop = sp.wnfetd(cov,0)
        #log.debug('fob_start : '+str(fob_start))
        #log.debug('fob_stop : '+str(fob_stop))
        # create a Fakeor relative_time-magnitude interpolator
        relt_I_f = interpolate.interp1d(fkr.rel_time,fkr.I)
        # make output data QTable
        self.make_data()
        # set time to start
        et = fob_start + (1.0/fps.to('s-1').value)/2.0
        # loop over time
        while et<=fob_stop-(1.0/fps.to('s-1').value)/2.0:
            infov = sp.fovtrg(camera_name,fkr.spice_name, 'POINT',' ','LT+S', nacelle_name,et)
            if infov:
                # compute ephemeris in nacelle topographic-like frame
                (eph,lt) = sp.spkezr(fkr.spice_name,et,camera_topoframe_name,'LT+S',nacelle_name)
                # compute ephemeris in ICRS frame
                (eph2,lt) = sp.spkezr(fkr.spice_name,et,'J2000','LT+S',nacelle_name)
                # compute distance from camera to Fakeor
                distazalt = sp.recrad(eph[:3])
                # compute range, azimuth, elevation
                dist = distazalt[0]*u.km
                az = (-distazalt[1]+2.0*np.pi)%(2.0*np.pi)*u.rad
                alt = distazalt[2]*u.rad
                # compute RA,DEC
                rradec = sp.recrad(eph2[:3])
                ra = rradec[1]*u.rad
                dec = rradec[2]*u.rad
                # compute Fakeor relative time
                fkr_rel_time = (et - fkr.et[0])*u.s
                # compute absolute brightness at time et
                fkr_I = relt_I_f(fkr_rel_time.to('s').value)
                # compute apparent brightness
                appI = phot.ItoIapp(fkr_I,dist.to('m').value) *u.W
                # compute apparent magnitude
                appMag = phot.ItoMag(fkr_I,dist.to('m').value)*u.mag
                # check if magnitude is observable
                time_obs = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
                if appMag<lim_mag:
                    #time_obs = Time(sp.et2utc(et,"ISOC",2,50),scale='utc') # ligne sortie de la boucle pour etre prise en compte aussi dans le else
                    self.data.add_row([time_obs.isot,et,fkr_rel_time.to('s'),dist.to('m'),ra.to('deg'),dec.to('deg'),az.to('deg'),alt.to('deg'),appI.to('W'),appMag])
                else:
                    #time_obs = Time(sp.et2utc(et,"ISOC",2,50),scale='utc') #TODO: a verifier
                    log.debug('Meteor of mag '+str(appMag)+' not bright enough at time '+time_obs.isot)
            time_obs = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
            log.debug('Fakeor Not in FOV of camera '+camera_name+ ' at time '+time_obs.isot)
            # increase time
            et = et + 1.0/fps.to('s-1').value
        # compute total number of observations
        self.nobs = len(self.data['Time'])
        log.debug('nobs = '+str(self.nobs))
        if (self.nobs):
            # set start/stop time
            self.cov = sp.utils.support_types.SPICEDOUBLE_CELL(2)
            sp.wninsd(self.data['et'][0],self.data['et'][-1],self.cov)
        else:
            self.cov = None
        return
    
    def from_FkSpIns(self,fkr,spcft_name,instr_name,coverage,lim_mag,fps):
        """Fill in FakeObs from a Fakeor, Spacecraft, Instrument, coverage, limiting magnitude, fps.
        
        Parameters
        ----------
        fkr : showersim.fakeor.Fakeor Object.
            Fakeor
        spcft_name : string
            SPICE spacecraft (or station) name.
        instr_name  : string
            SPICE instrument name.
        coverage : SPICE time window.
            Effective observation time window.
        lim_mag : astropy.units.Quantity object.
            Camera limiting magnitude, in [mag]
        fps : astropy.units.Quantity object.
            Camera frame rate, in [s-1].
        
        Returns
        -------
        None.
        
        """
        # set instrument frames
        spcft_topoframe_name = make_topoframe_name(spcft_name)
        # retrieve Fakeor start/stop time
        fkr_cov = sp.spkcov(fkr.spk_file,fkr.spice_id)
        # narrow down the time window
        cov = sp.wnintd(coverage,fkr_cov)
        fob_start,fob_stop = sp.wnfetd(cov,0)
        log.debug('fob_start : '+str(fob_start))
        log.debug('fob_stop : '+str(fob_stop))
        # create a Fakeor relative_time-magnitude interpolator
        relt_I_f = interpolate.interp1d(fkr.rel_time,fkr.I)
        # make output data QTable
        self.make_data()
        # set time to start
        et = fob_start + (1.0/fps.to('s-1').value)/2.0
        # loop over time
        while et<=fob_stop-(1.0/fps.to('s-1').value)/2.0:
            time_obs = Time(sp.et2utc(et,"ISOC",2,50),scale='utc')
            # check if fakeor is in fov
            infov = sp.fovtrg(instr_name,fkr.spice_name, 'POINT',' ','LT+S', spcft_name,et)
            if infov:
                # compute ephemeris in spacecraft topographic-like frame
                (eph,lt) = sp.spkezr(fkr.spice_name,et,spcft_topoframe_name,'LT+S',spcft_name)
                # compute spacecraft ephemeris in ICRS frame
                (eph2,lt) = sp.spkezr(fkr.spice_name,et,'J2000','LT+S',spcft_name)
                # compute distance from camera to Fakeor
                distazalt = sp.recrad(eph[:3])
                # compute range, azimuth, elevation
                dist = distazalt[0]*u.km
                az = (-distazalt[1]+2.0*np.pi)%(2.0*np.pi)*u.rad
                alt = distazalt[2]*u.rad
                # compute RA,DEC
                rradec = sp.recrad(eph2[:3])
                ra = rradec[1]*u.rad
                dec = rradec[2]*u.rad
                # compute Fakeor relative time
                fkr_rel_time = (et - fkr.et[0])*u.s
                # compute absolute brightness at time et
                fkr_I = relt_I_f(fkr_rel_time.to('s').value)
                # compute apparent brightness
                appI = phot.ItoIapp(fkr_I,dist.to('m').value) *u.W
                # compute apparent magnitude
                appMag = phot.ItoMag(fkr_I,dist.to('m').value)*u.mag
                # check if magnitude is observable
                if appMag<lim_mag:
                    self.data.add_row([time_obs.isot,et,fkr_rel_time.to('s'),dist.to('m'),ra.to('deg'),dec.to('deg'),az.to('deg'),alt.to('deg'),appI.to('W'),appMag])
                else:
                    log.warning('Fakeor '+self.name+' of mag '+str(appMag)+ ' at time '+time_obs.isot)
            else:
                log.warning('Fakeor '+self.name+' not in FOV of camera '+instr_name+ ' at time '+time_obs.isot)
            # increase time
            et = et + 1.0/fps.to('s-1').value
        # compute total number of observations
        self.nobs = len(self.data['Time'])
        log.debug('nobs = '+str(self.nobs))
        if (self.nobs):
            # set start/stop time
            self.cov = sp.utils.support_types.SPICEDOUBLE_CELL(2)
            sp.wninsd(self.data['et'][0],self.data['et'][-1],self.cov)
        else:
            self.cov = None
        return
    
    def make_summary(self):
        """Make a summary of fake observations.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        isobs : boolean
            True if Fakeor observable by spacecraft camera.
        fkr_time : astropy.time.Time object
            Start time of observation.
        fkr_et_start : float
            Observation start SPICE time.
        fkr_et_stop : float
            Observation stop SPICE time.
        min_mag : astropy.units.Quantity object.
            Mininumum observed magnitude.
        nobs : int
            Total number of observations.
                
        """
        # return Fake Observation summary data
        if (self.nobs):
            return (True,Time(sp.et2utc(self.data['et'][0],"ISOC",2,50),scale='utc'),self.data['et'][0],self.data['et'][-1],np.min(self.data['appMag']),self.nobs)
        else:
            log.debug('There is no observable data')
            return (False,None,None,None,None,0)
    
    
    def save(self,outfile):
        """Save FakeObs data into file.
        
        Parameters
        ----------
        outfile : string
            Output file name
        
        Returns
        -------
        None.
        
        
        """
        if (self.nobs):
            log.debug('Saving '+str(self.nobs)+' fake observations into '+outfile)
            self.data.write(outfile,format='ascii.fixed_width_two_line',overwrite=True)
            log.debug(str(self.nobs)+' fake observations saved in '+outfile)
        else:
            msg = 'No observable data to be saved in '+outfile
            log.warning(msg)
        return
    
    
    def read_int_fakeobs(self,intfile):
        """Read Fakeobs data.
        
        Parameters
        ----------
        intfile : string
            Fakeobs output file.
        
        Returns
        -------
        None.
        
        """
        # checks that the Fakeobs file does exist and has enough data   # NO! Because sometime the meteor is not detected by the cameras, so there is no fakeobs
        #if not os.path.isfile(intfile):
         #   msg = "*** FATAL ERROR: file:"+intfile+" does not exist"
          #  log.error(msg)
           # raise IOError(msg)
        #file_stat = os.stat(intfile)
        #if (file_stat.st_size < 1000):
         #   msg = "*** FATAL ERROR: file "+intfile+"does not seem to contain enough data"
          #  log.error(msg)
           # raise IOError(msg)
        # reads the Fakeobs file
        log.info("reading file: "+intfile)
        self.data=QTable.read(intfile,format='ascii',
                            names=('Time','et','rel_time','Dist','RA','DEC','Az','El','appI','appMag'))
        # putting the unit to get quantity objects
        #self.data['Time'].unit='s'
        self.data['et'].unit='s'
        self.data['rel_time'].unit='s'
        self.data['Dist'].unit='m'
        self.data['RA'].unit='deg'
        self.data['DEC'].unit='deg'
        self.data['Az'].unit='deg'
        self.data['El'].unit='deg'
        self.data['appI'].unit='W'
        self.data['appMag'].unit='mag'
        return
    