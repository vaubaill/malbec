"""Definition of several useful tool for meteor shower definitions.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project


"""
import numpy as np
from astropy.coordinates import EarthLocation
import astropy.units as u
import showersim.distrib as dst
import nrlmsise00

from missionplanner.utils import log

def mag_idx2mass_idx(mag_idx):
    """Convert magnitude index into mass index.
    
    Parameters
    ----------
    mag_idx : float
        Mgnitude distribution index.
    
    Returns
    -------
    mass_idx : float
        Mass distribution index (differential).
    """
    return 1.0+2.3*np.log10(mag_idx)

def mass_idx2cmass_idx(mass_idx):
    """Convert mass index into cumulative mass index.
    
    Parameters
    ----------
    mass_idx : float
        Mass distribution index.
    
    Returns
    -------
    cmass_idx : float
        Cumulative mass distribution index.
    """
    return mass_idx+1.0

def cmass_idx2sz_idx(cmass_idx):
    """Convert cumulative mass index into differential size index.
    
    Parameters
    ----------
    cmass_idx : float
        Cumulative mass distribution index.
    
    Returns
    -------
    sz_idx : float
        Differential size index.
    """
    return 3.0*cmass_idx + 1.0
    
def mass_idx2sz_idx(mass_idx):
    """Convert (differential) mass index into (differential) size index.
    
    Parameters
    ----------
    mass_idx : float
        Mass distribution index (differential).
    
    Returns
    -------
    sz_idx : float
        Differential size index.
    """
    return 3.0*mass_idx - 2.0

def make_mag_pop(r,LimMag,N,distrib='powerlaw3_gen'):
    """Create a population of meteor magnitudes
    
    Parameters
    ----------
    r : float
        Population index
    LimMag : astropy.units.Quantity Object
        Limiting magnitude [mag]
    N : int
        Total number of meteors to generate.
    distrib : string, optional
        Name of distribution to use. Default is 'powerlaw3_gen', using the powerlaw package.
    
    Returns
    -------
    popmag : astropy.units.Quantity Object of size N.
        Population of meteor magnitudes.
    
    """
    # set initial population limit to 2 in order to ensure the population is created correctly
    ymin = 2 * u.mag
    # create first population
    pop = dst.powerlaw3_gen(r,ymin.value,N) * u.mag
    # scale the population to the specified Limiting Magnitude
    popmag = LimMag - pop + ymin
    return popmag

def make_mass_pop(r,mass_min,mass_max,N,distrib='powerlaw3_gen'):
    """Create a population of meteor masses.
    
    Parameters
    ----------
    r : float
        Population index
    mass_min : astropy.units.Quantity Object
        Minimum mass [kg].
    mass_max : astropy.units.Quantity Object
        Maximum mass [kg].
    N : int
        Total number of meteors to generate.
    distrib : string, optional
        Name of distribution to use. Default is 'powerlaw3_gen', using the powerlaw package.
    
    Returns
    -------
    popmass : astropy.units.Quantity Object of size N.
        Population of meteor masses.
    
    """
    # convert magnitude population index into differential mass index
    mass_idx = mag_idx2mass_idx(r)
    log.debug('mass_idx: '+str(mass_idx))
    # create first population
    popmass = dst.powerlaw4_gen(mass_idx,mass_min.value,mass_max.value,N) * u.kg
    return popmass

def get_radius_FOV_naked_eye():
    """Retrieve naked eye FOV radius.
    
    Parameters
    ----------
    None.
    
    Returns
    -------
    radius_FOV_naked_eye : astropy.coordinates.Angle object.
        Radius of naked eye FOV.
    
    """
    radius_FOV_naked_eye = 52.5*u.deg
    log.debug('radius_FOV_naked_eye: '+str(radius_FOV_naked_eye.to('deg')))
    return radius_FOV_naked_eye

def get_eye_radius(h_meteor=100*u.km):
    """Compute naked eye atmosphere radius.
    
    Parameters
    -----------
    h_meteor : astropy.units.Quantity Object, optional.
        altitude of beginning of meteors above ground in [km]. Default is 100 km.
    
    Returns
    -------
    r_atm_naked_eye : astropy.units.Quantity Object.
        radius of naked eye atmospheric area.
    
    """
    radius_FOV_naked_eye = get_radius_FOV_naked_eye()
    r_atm_naked_eye = h_meteor*np.tan(radius_FOV_naked_eye.to('rad').value)
    return r_atm_naked_eye

def r2A(r):
    """Compute the reduced Area, from eq. 3 of Koschak & Rendtel (1990),` 44-58, WGN
    
    Parameters
    ----------
    r : float
        Population (magnitude differential) index.
        
    Returns
    -------
    Ared : astropy.units.Quantity Object
        Atmosphere area in km^2.
    
    
    """
    
    Ared = 178700.0 * r**(-1.82) * u.km**2
    
    return Ared

def get_eye_area(h_meteor=100*u.km):
    """Compute naked eye atmosphere area.
    
    Parameters
    -----------
    h_meteor : astropy.units.Quantity Object, optional.
        altitude of beginning of meteors above ground in [km]. Default is 100 km.
    
    """
    ## radius of circle of atmosphere visible by naked eye [km]
    #r_atm_naked_eye = get_eye_radius(h_meteor=h_meteor)
    #log.debug('r_atm_naked_eye: '+str(r_atm_naked_eye.to('km')))
    ## surface of atmosphere visible by nake eye [km2]
    #A_atm_naked_eye = np.pi*r_atm_naked_eye**2.0
    A_atm_naked_eye = 37200.0*u.km**2 * (h_meteor.to('km')/(100*u.km))**2
    #log.debug('A_atm_naked_eye: '+str(A_atm_naked_eye.to('km2')))
    return A_atm_naked_eye

def zhr2FluxD(zhr,r,h_meteor=100*u.km):
    """Convert ZHR into flux density.
    
    From eq. 13 of Koschak & Rendtel 1990, 44-58, WGN.
    
    Parameters
    -----------
    ZHR : astropy.units.Quantity Object.
        Zenithal Hourly Rate, in [hr-1]
    r : float
        Meteor shower population index.
    h_meteor : astropy.units.Quantity Object, optional.
        altitude of beginning of meteors above ground in [km]. Default is 100 km.
    radius_FOV_naked_eye : astropy.units.Quantity Object.
        radius of field of view for naked eye. Default is 50 deg.
    
    Returns
    -------
    Df : astropy.units.Quantity Object.
        meteoroid Flux density, in [km-2.hr-1]
    
    """
    # surface of atmosphere visible by nake eye [km2]
    #A_atm_naked_eye = get_eye_area(h_meteor=h_meteor)
    A_red = r2A(r)
    # correction factor
    c = (13.1*r - 16.5)*(r - 1.3)**0.748
    # Flux density
    Df = zhr * c / A_red
    
    # log
    log.debug('h_meteor: '+str(h_meteor.to('km')))
    log.info('Df: '+str(Df))
    
    return Df.to('km-2.hr-1')

def zhr2spaceD(zhr,r,V):
    """Convert ZHR into space density.
    
    From: eq. 13 in Koschak & Rendtel 1990, WGN 18:44.
    Also used in: Arlt et al. 1999, WGN, 27:286, Brown et al. 2000, MNRAS 319: 419, 
    
    Parameters
    -----------
    ZHR : astropy.units.Quantity Object.
        Zenithal Hourly Rate, in [hr-1]
    r : float
        Meteor shower population index.
    V : astropy.units.Quantity Object.
        Meteoroid atmosphere entry velocity in [m.s-1].
    
    Returns
    -------
    D : astropy.units.Quantity Object.
        meteoroid 3D-density, in [km-3]
    """
    # compute Flux density in [km-2.hr-1]
    Df = zhr2FluxD(zhr,r)
    D = Df / V.to('km.hr-1')
    
    log.info('D: '+str(D))
    return D.to('km-3')



def flux2zhr(flux, r=2.0):
    """Convert flux to ZHR with Koschak & Rendtel 1990b, eqn 41.
    
    Parameters
    ----------
    flux : astropy.units.Quantity Object.
        Flux in [km^-2 h^-2]
    r : float
        population index.

    Returns
    -------
    ZHR : astropy.units.Quantity Object.
        Zenithal Hourly Rate in [h^-1].
    
    """
    # compute the correction factor with Koschak & Rendtel 1990b, eqn 33.
    c_r = (13.1*r - 16.45)
    # compute ZHR
    zhr = (flux.to('km h-1') * 37200.0*u.km**2) / ( c_r * (r - 1.3)**0.748 )
    return zhr


def HR2ZHR(HR,duration,r,radiant_el,LM,LM_naked_eye=6.5*u.mag):
    """Compute ZHR from HR.

    Parameters
    ----------
    HR : float
        Number of observed meteors.
    duration : astropy.time.DeltaTime object
        Duration of the observation.
    r : float
        Meteor shower population index [no unit].
    radiant_el : astropy.coordinates.Angle object
        Radiant elevation above the horizon during the observation duration.
    LM : astropy.units.Quantity object
        Limiting magnitude [mag].
    LM_naked_eye : astropy.units.Quantity object, optional
        Naked eye limiting magnitude. The default is 6.5*u.mag.

    Returns
    -------
    ZHR : astropy.units.Quantity object.
        Wenithal Hourly Rate, in [hr-1].

    """
    # limiting magnitude factor
    LMfct = r**((LM_naked_eye-LM).value)
    # radiant height factor
    radfct = np.sin(radiant_el.to('rad'))
    # compute ZHR
    ZHR = HR * LMfct / (radfct * duration.to('hr'))
    return ZHR

def ZHR2HR(ZHR,r,radiant_el,LM,LM_naked_eye=6.5*u.mag,h_meteor=100*u.km,cam_dist=100.0*u.km):
    """Compute local HR from ZHR.
    
    Parameters
    ----------
    ZHR : astropy.units.Quantity Object.
        Zenithal Hourly Rate, in [hr-1]
    r : float
        Meteor shower population index.
    radiant_el : astropy.coordinates.Angle object
        Radiant elevation at time of observation.
    LM : astropy.units.Quantity object, optional
        Observation Limiting magnitude.
    LM_naked_eye : astropy.units.Quantity object, optional
        Naked eye Limiting magnitude. Default is 6.0*u.mag.
    h_meteor : astropy.units.Quantity Object, optional.
        altitude of beginning of meteors above ground in [km]. Default is 100 km.
    cam_dist : astropy.units.Quantity Object, optional.
        physical distance between camera position and bore sight vector
        projected to meteor altitude in [km].
        Default is 100.0 km, meaning the camera is on the ground.
    
    Returns
    -------
    HR : astropy.units.Quantity Object.
        Hourly Rate, in [hr-1]
    mag_gain : float
        Magnitude gain, as a function of camera altitude.
    f_radel : float
        radiant elevation factor.
    f_LM : float
        Limiting magnitude factor, taking into account the magnitude gain due to camera altitude.
    
    """
    log.debug('cam_dist : '+str(cam_dist))
    # radiant elevation factor
    f_radel = np.where(radiant_el.value<0.0, 0.0, np.sin(radiant_el.to('rad').value))
    
    # camera distance brightness factor
    f_alt_I = (cam_dist.to('km') / h_meteor.to('km'))**2.0
    dmag_alt = -2.5*np.log(f_alt_I) * u.mag
    
    # effective limiting magnitude
    LM_eff = LM + dmag_alt
    # limiting magnitude factor
    f_LM = 1.0 / r**((LM_naked_eye - LM_eff).value)
    
    # Hourly rate
    HR = ZHR * f_radel * f_LM
    """
    log.debug('ZHR : '+str(ZHR))
    log.debug('r : '+str(r))
    log.debug('radiant_el : '+str(radiant_el))
    log.debug('LM : '+str(LM))
    log.debug('LM_naked_eye : '+str(LM_naked_eye))
    log.debug('f_radel : '+str(f_radel))
    log.debug('f_LM : '+str(f_LM))
    log.debug('dmag_alt : '+str(dmag_alt))
    """
    return (HR,dmag_alt,f_radel,f_LM,LM_eff)

def HR2numet(HR,obs_area,dt,h_meteor=100*u.km):
    """Convert Hourly rate to number of meteors.
    
    Parameters
    ----------
    HR : astropy.units.Quantity Object.
        Hourly Rate, corrected for radiant elevation,
        population index and limiting magnitude, in [hr-1].
    obs_area : astropy.units.Quantity object.
        Observed area.
    dt : astropy.time.TimeDelta object.
        Duration of the observation.
    h_meteor : astropy.units.Quantity Object, optional.
        altitude of beginning of meteors above ground in [km]. Default is 100 km.
    
    Returns
    -------
    numet : astropy.units.Quantity object
        Expected number of meteors [hr-1].
    f_area : float
        Observed atmosphere area factor, []
    """
    # compute naked eye atmosphere area
    A_atm_naked_eye = get_eye_area(h_meteor=h_meteor)
    # compute area factor
    f_area = obs_area / A_atm_naked_eye
    log.debug('f_area :'+str(f_area))
    # take into account observation duration
    f_time = dt.to('hr')
    log.debug('f_time: '+str(f_time))
    
    # compute expected number of meteors
    numet = HR * f_time * f_area
    return (numet,f_area,f_time)

def simple_density(eloci,elocf,time):
    """Compute air number and mass density from 2 points integrated over 2 points.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Location of observed point.
    time : astropy.time.Time object
        Time.
    
    Returns
    -------
    number_density : astropy.units.Quantity object
        integrated atmosphere molecules number.
    mass_density : astropy.units.Quantity object
        integrated atmosphere density.
    
    """
    # create locations along line of sight between the 2 points
    eloco,npoints,dist_i2f = compute_point_along_sight(eloci,elocf)
    # get all atmosphere data for all eloci
    data = nrlmsise00.msise_flat(time.to_datetime(),
                                 eloco.height.to('km').value,
                                 eloco.lat.to('deg').value,
                                 eloco.lon.to('deg').value,
                                 150, 150, 4)
    # select data
    He = np.sum(data[:,0])/u.cm**3
    O  = np.sum(data[:,1])/u.cm**3
    N2 = np.sum(data[:,2])/u.cm**3
    O2 = np.sum(data[:,3])/u.cm**3
    Ar = np.sum(data[:,4])/u.cm**3
    H  = np.sum(data[:,6])/u.cm**3
    N  = np.sum(data[:,7])/u.cm**3
    aO = np.sum(data[:,8])/u.cm**3
    # compute total number of molecules/atoms along line of sight:
    # integral of rho over space
    number_density = (He+O+N2+O2+Ar+H+N+aO).to('m-3')
    # compute total mass encountered by light along line of sight
    mass_density = np.sum(data[:,5]) *u.g/u.cm**3
    return (number_density,mass_density.to('kg.m-3'),npoints,dist_i2f)

def air_density(eloc,time):
    """Compute air number and mass density at one location.
    
    Parameters
    ----------
    eloc :astropy.coordinates.EarthLocation object
        Location of observer.
    time : astropy.time.Time object
        Time.
    
    Returns
    -------
    number_density : astropy.units.Quantity object
        integrated atmosphere molecules number.
    mass_density : astropy.units.Quantity object
        integrated atmosphere density.
    
    """
    # get all atmosphere data at eloc
    data = nrlmsise00.msise_flat(time.to_datetime(),
                                 eloc.height.to('km').value,
                                 eloc.lat.to('deg').value,
                                 eloc.lon.to('deg').value,
                                 150, 150, 4)
    # select data
    He = data[0]/u.cm**3
    O  = data[1]/u.cm**3
    N2 = data[2]/u.cm**3
    O2 = data[3]/u.cm**3
    Ar = data[4]/u.cm**3
    H  = data[6]/u.cm**3
    N  = data[7]/u.cm**3
    aO = data[8]/u.cm**3
    # compute total number of molecules/atoms along line of sight:
    # integral of rho over space
    number_density = (He+O+N2+O2+Ar+H+N+aO).to('m-3')
    # compute total mass encountered by light along line of sight
    mass_density = data[5] *u.g/u.cm**3
    return (number_density,mass_density.to('kg.m-3'))


def compute_point_along_sight(eloci,elocf,npoints=None):
    """Compute all locations between 2 points, with a 1-m distance between each point.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Location of observed point.
    npoints : integer, optional
        Number of points along line of sight.
    
    Returns
    -------
    eloco : astropy.coordinates.EarthLocation object
        Location of all points between eloc1 and eloc2.
    npoints : int
        Number of points
    dist : astropy.units.Quantity object
        Physical Distance between eloci and elocf.
    
    """
    # create eloci,elocf vector, unit=[km]
    i2f = np.array([(elocf.x - eloci.x).to('km').value,
                    (elocf.y - eloci.y).to('km').value,
                    (elocf.z - eloci.z).to('km').value])
    # compute difference of height/altitude between the 2 points
    dist_i2f = np.linalg.norm(i2f)*u.km
    #log.debug('dist_i2f: '+str(dist_i2f))
    if not npoints:
        # set npoints
        if dist_i2f < 10.0*u.km:
            # compute number of points needed: as many as the distance in m.
            npoints = np.max([1000,int(dist_i2f.to('m').value)])
        else:
            npoints = np.max([1000,int(dist_i2f.to('km').value)])
    #log.debug('npoints: '+str(npoints))
    # make intermediate vector, unit=[km]
    interm = i2f[:, np.newaxis] * np.linspace(0,1,npoints)
    # create nclones point between eloci and elocf
    pntinsight = np.array([eloci.x.to('km').value + interm[0],
                           eloci.y.to('km').value + interm[1],
                           eloci.z.to('km').value + interm[2]])
    # create eloco
    eloco = EarthLocation.from_geocentric(pntinsight[0],pntinsight[1],pntinsight[2],unit=u.km)
    # double check that the height of ending points is correct: must be the same as elocf.height
    #log.debug('height: '+str(eloco.height))
    return eloco,npoints,dist_i2f.to('m')

def compute_point_along_sight_one2many(eloci,elocf,npoints=1000):
    """Compute all locations between 2 points.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Locations of observed points.
    npoints : integer, optional.
        Number of points to create along the line of sight.
        Default is 1000.
    
    Returns
    -------
    elocos : astropy.coordinates.EarthLocation object
        Location of all points between eloc1 and eloc2.
    
    """
    # create eloci,elocf vector, unit=[km]
    i2f = np.array([(elocf.x - eloci.x).to('km').value,
                    (elocf.y - eloci.y).to('km').value,
                    (elocf.z - eloci.z).to('km').value])
    # compute difference of height/altitude between the 2 points
    dist_i2f = np.linalg.norm(i2f,axis=0)*u.km
    #log.debug('dist_i2f: '+str(dist_i2f))
    
    frac = np.linspace(0,1,npoints)
    # create nclones point along the sight
    pntinsight = np.array([(eloci.x.to('km') + (elocf.x - eloci.x).to('km')[:, np.newaxis]*frac).flatten(),
                           (eloci.y.to('km') + (elocf.y - eloci.y).to('km')[:, np.newaxis]*frac).flatten(),
                           (eloci.z.to('km') + (elocf.z - eloci.z).to('km')[:, np.newaxis]*frac).flatten()])
    # create eloco
    eloco = EarthLocation.from_geocentric(pntinsight[0],pntinsight[1],pntinsight[2],unit=u.km)
    return (eloco,npoints,dist_i2f)

def density_distance_from_ground(lon,lat,time,npoints=None):
    """Compute air number and mass density from ground.
    
    Parameters
    ----------
    lon : astropy.units.Quantity object
        Longitude.
    lat : astropy.units.Quantity object
        Latitude.
    time : astropy.time.Time object
        Time.
    npoints : integer, optional
        Number of points to along line of sight.
        Default is None, meaning that it will be computed automatically.
    
    Returns
    -------
    number_density : astropy.units.Quantity object
        integrated atmosphere molecules number.
    mass_density : astropy.units.Quantity object
        integrated atmosphere density.
    
    """
    # create eloci and elocf
    eloci = EarthLocation.from_geodetic(lon=lon,lat=lat,height=0.0*u.m)
    elocf = EarthLocation.from_geodetic(lon=lon,lat=lat,height=100.0*u.km)
    # compute total air density along sight
    (number_density,mass_density) = density_distance(eloci,elocf,time,npoints=npoints)
    return (number_density,mass_density)

def density_distance(eloci,elocf,time,npoints=None):
    """Compute air number and mass density from 2 points, times distance.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Location of observed point.
    time : astropy.time.Time object
        Time.
    npoints : integer, optional
        Number of points along line of sight.
        Default is None, meaning that it will be set automatically.
    
    Returns
    -------
    number_density : astropy.units.Quantity object
        integrated atmosphere molecules number.
    mass_density : astropy.units.Quantity object
        integrated atmosphere density.
    
    """
    # create locations along line of sight between the 2 points
    eloco,npoints,dist_i2f = compute_point_along_sight(eloci,elocf,npoints=npoints)
    # compute distance between each point
    dist = dist_i2f / npoints
    # get all atmosphere data for all eloco
    data = nrlmsise00.msise_flat(time.to_datetime(),
                                 eloco.height.to('km').value,
                                 eloco.lat.to('deg').value,
                                 eloco.lon.to('deg').value,
                                 150, 150, 4)
    # select data
    He = np.sum(data[:,0])/u.cm**3
    O  = np.sum(data[:,1])/u.cm**3
    N2 = np.sum(data[:,2])/u.cm**3
    O2 = np.sum(data[:,3])/u.cm**3
    Ar = np.sum(data[:,4])/u.cm**3
    H  = np.sum(data[:,6])/u.cm**3
    N  = np.sum(data[:,7])/u.cm**3
    aO = np.sum(data[:,8])/u.cm**3
    # compute total number of molecules/atoms along line of sight as the integral of density over space
    number_density = (He+O+N2+O2+Ar+H+N+aO).to('m-3') * dist.to('m').value
    # compute total mass encountered by light along line of sight as the integral of rho over space
    mass_density = np.sum(data[:,5]) *u.g/u.cm**3 * dist.to('m').value
    #log.debug('number_density: '+str(number_density.to('m-3')))
    #log.debug('mass_density: '+str(mass_density.to('kg.m-3')))
    return (number_density.to('m-3'),mass_density.to('kg.m-3'))

def density_distance_one2many(eloci,elocfs,time,npoints=1000):
    """Compute air number and mass density from 2 points.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Location of observed points.
    time : astropy.time.Time object
        Time when densities are to be computed.
    npoints : integer, optional.
        Number of points to create along the line of sight.
        Default is 1000.
    
    Returns
    -------
    number_density : astropy.units.Quantity object
        integrated atmosphere molecules number.
    mass_density : astropy.units.Quantity object
        integrated atmosphere density.
    
    """
    # get dimension of elocf
    dim = len(elocfs)
    # create locations along line of sight between the 2 points
    (eloco,npoints,dist_i2f) = compute_point_along_sight_one2many(eloci,elocfs,npoints=npoints)
    dist = dist_i2f / npoints
    """
    log.debug('time: '+str(time))
    log.debug('eloci: '+str(eloci.to_geodetic()))
    log.debug('elocfs: '+str(elocfs.to_geodetic()))
    log.debug('len(elocfs): '+str(len(elocfs)))
    log.debug('eloco: '+str(eloco.to_geodetic()))
    log.debug('len(eloco): '+str(len(eloco)))
    log.debug('npoints: '+str(npoints))
    log.debug('dist_i2f: '+str(dist_i2f))
    log.debug('dist: '+str(dist))
    """
    # get all atmosphere data for all eloci
    data = nrlmsise00.msise_flat(time.to_datetime(),
                                 eloco.height.to('km').value,
                                 eloco.lat.to('deg').value,
                                 eloco.lon.to('deg').value,
                                 150, 150, 4)
    ndata = data.reshape(dim,npoints,11)
    # select data
    He = np.sum(ndata[:,:,0],axis=1)/u.cm**3
    O  = np.sum(ndata[:,:,1],axis=1)/u.cm**3
    N2 = np.sum(ndata[:,:,2],axis=1)/u.cm**3
    O2 = np.sum(ndata[:,:,3],axis=1)/u.cm**3
    Ar = np.sum(ndata[:,:,4],axis=1)/u.cm**3
    H  = np.sum(ndata[:,:,6],axis=1)/u.cm**3
    N  = np.sum(ndata[:,:,7],axis=1)/u.cm**3
    aO = np.sum(ndata[:,:,8],axis=1)/u.cm**3
    # sum up data
    number_density = (He+O+N2+O2+Ar+H+N+aO) * dist.to('m').value
    mass_density = np.sum(ndata[:,:,5],axis=1) * dist.to('m').value *u.g/u.cm**3
    #log.debug('number_density: '+str(number_density.to('m-3')))
    #log.debug('mass_density: '+str(mass_density.to('kg.m-3')))
    return (number_density.to('m-3'),mass_density.to('kg.m-3'))

def compute_(eloci,elocf,time):
    """Compute air mass between 2 points.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Location of observed point.
    time : astropy.time.Time object
        Time.
    
    Returns
    -------
    airmass : float
        air mass.
    
    """
    # compute air density from ground to meteor height
    d_g,rho_g = density_distance_from_ground(eloci.lon,eloci.lat,time)
    # compute air density between the 2 points
    d,rho = density_distance(eloci,elocf,time)
    # compute air mass
    airmass = rho / rho_g
    return airmass

def compute_airmass_one2many(eloci,elocfs,time,npoints=1000):
    """Compute air mass between 2 points.
    
    Parameters
    ----------
    eloci :astropy.coordinates.EarthLocation object
        Location of observer.
    elocf :astropy.coordinates.EarthLocation object
        Location of observed point.
    time : astropy.time.Time object
        Time.
    npoints : integer, optional.
        Number of points to create along the line of sight.
        Default is 1000.
    
    Returns
    -------
    airmass : float
        air mass.
    
    """
    # compute air density from ground to meteor height at eloci longitude, latitude
    d_g,rho_g = density_distance_from_ground(eloci.lon,eloci.lat,time,npoints=npoints)
    # compute air density between the eloci and all elocfs
    #d,rho = compute_air_density_one2many(eloci,elocfs,time,npoints=npoints)
    d,rho = density_distance_one2many(eloci,elocfs,time,npoints=npoints)
    # compute air mass
    airmass = rho / rho_g
    
    try:
        loc = np.where(airmass > 35.0)
        dim = len(loc)
        if dim:
            airmass[loc] = np.repeat(35.0,dim)
    except:
        if airmass > 35.0:
            airmass = 35.0
    
    """
    log.debug('d_g: '+str(d_g))
    log.debug('d: '+str(d))
    log.debug('rho_g: '+str(rho_g))
    log.debug('rho: '+str(rho))
    log.debug('rho / rho_g: '+str(rho / rho_g))
    log.debug('d / d_g: '+str(d / d_g))
    log.debug('airmass: '+str(airmass))
    """
    return airmass


def Roth1994(airmass):
    """Compute magnitude loss due to airmass.
    
    Parameters
    ----------
    airmass : float
        air mass.
    
    Returns
    -------
    dm : astropy.units.Quantity object.
        Magnitude loss, in [mag].
    
    See also
    --------
    Roth 1994, Appendix B, Compendium of Practical Astronomy, Volume 3, Stars and Stellar Systems, Springer-Verlag, Berlin, 321 pp
    Gural & Jenniskens, Earth, Moon and Planets 82-83: 221-247, 2000
    
    """
    dim = len(airmass)
    if dim<2:
        if airmass<35.0:
            dmag = (-0.003*airmass**2 + 0.228*airmass-0.225)
        else:
            dmag = 35.0
    else:
        dmag = 0.0 * airmass.value
        mask = airmass<35.0
        dmag[mask] = (-0.003*airmass[mask]**2 + 0.228*airmass[mask]-0.225)
        mask = airmass>=35.0
        dmag[mask] = (airmass[mask]-airmass[mask]+35.0)
    log.debug('dmag = '+str(dmag))
    return dmag*u.mag

def KoschnyZender2000(eloci,elocf,time,wavelength=600.0*u.nm):
    """Compute magnitude loss due to airmass with Koschny & Zender (2000) method.
    
    Parameters
    ----------
    eloci : astropy.coordinates.EarthLocation object
        Location of observer.
    elocf : astropy.coordinates.EarthLocation object
        Location of observed point.
    time : astropy.time.Time object
        Time.
    wavelength : astropy.units.Quantity Object.
        Wavelength. Default is 600.0*u.nm.
    
    Returns
    -------
    dm : astropy.units.Quantity object.
        Magnitude loss, in [mag].
    
    See also
    --------
    Koschny, Zender, Earth, Moon and Planets 82-83: 209-220, 2000.
    
    """
    # compute light intensity extinction coefficient
    light_coef = light_extinction_coeff(eloci,elocf,time,wavelength=wavelength)
    # copmute magnitude loss
    dmag = 2.5*np.log10(light_coef) * u.mag
    return dmag

def light_extinction_coeff(eloci,elocf,time,wavelength=600.0*u.nm):
    """Compute magnitude loss due to airmass with Koschny & Zender (2000) method.
    
    Parameters
    ----------
    eloci : astropy.coordinates.EarthLocation object
        Location of observer.
    elocf : astropy.coordinates.EarthLocation object
        Location of observed point.
    time : astropy.time.Time object
        Time.
    wavelength : astropy.units.Quantity Object.
        Wavelength. Default is 600.0*u.nm.
    
    Returns
    -------
    dm : astropy.units.Quantity object.
        Magnitude loss, in [mag].
    
    See also
    --------
    Koschny, Zender, Earth, Moon and Planets 82-83: 209-220, 2000.
    
    """
    # compute air refractive index at wavelength
    n = air_refractive_index(wavelength)
    # compute air density at starting point
    (d0,rho0) = air_density(eloci,time)
    # compute Rayleigh coefficient as a function of air density
    sigma_r0 = RayleightScatteringCoeff(wavelength,N0=d0,rho0=rho0)
    # compute integral of air density over line of sight
    (d,rho) = total_density(eloci,elocf,time)
    # compute Rayleigh coefficient as a function of air density
    sigma_r = RayleightScatteringCoeff(wavelength,N0=d,rho0=rho0)
    """
    log.debug('d0: '+str(d0))
    log.debug('rho0: '+str(rho0))
    log.debug('d: '+str(d))
    log.debug('rho: '+str(rho))
    log.debug('sigma_r0: '+str(sigma_r0))
    log.debug('sigma_r: '+str(sigma_r))
    """
    
    light_coef = sigma_r / sigma_r0
    
    # compute light intensity extinction coefficient
    #light_coef = np.exp(-light_coef)
    #light_coef = sigma_r * d.to('m-3').value
    log.debug('light_coef: '+str(light_coef))
    return light_coef

def air_refractive_index(wavelength):
    """Compute refractive index of light using Elden's formula.
    
    Parameters
    ----------
    wavelength : astropy.units.Quantity Object.
        Wavelength.
    
    Returns
    -------
    n : float
        Refractive index of air.
    
    See also
    --------
    https://ui.adsabs.harvard.edu/abs/1966Metro...2...71E/abstract
    
    """
    #n = (64.328 + 29.4981/(146.0-wavelength.to('um').value**(-2)) + 255.4/(41.0-wavelength.to('um').value**(-2))) * 1.0E-06 + 1
    #print('n: ',n)
    n = (8342.13 + 2406030.0/(130.0-wavelength.to('um').value**(-2)) + 15997.0/(38.9-wavelength.to('um').value**(-2))) * 1.0E-08 + 1
    log.debug('n: '+str(n))
    return n

def RayleightScatteringCoeff(wavelength,N0=2.547E+25*u.m**(-3),rho0=1.16*u.kg*u.m**(-3)):
    """Compute the Rayleight scattering coefficient.
    
    Parameters
    ----------
    wavelength : astropy.units.Quantity object
        Wavelength.
    N0 : astropy.units.Quantity. object, optional
        air number. Default is value at ground: 2.547E+25*u.m**(-3).
    rho0 : astropy.units.Quantity. object, optional
        air density. Default is value at ground: 1.16*u.kg*u.m**(-3).
    
    Returns
    -------
    sigma_r : float
        Rayleight scattering coefficient.
    
    
    See also
    --------
    Koschny, Zender, Earth, Moon and Planets 82-83: 209-220, 2000.
    
    
    """
    # compute air refractive index
    n = air_refractive_index(wavelength)
    # compute Rayleight scattering coefficient
    sigma_r = (n-1.0) * 32.0*np.pi**3.0 / (3.0*N0*wavelength.to('m')**4*rho0)
    log.debug('(n-1.0)**2: '+str((n-1.0)**2))
    log.debug('32.0*np.pi**3/3: '+str(32.0*np.pi**3/3))
    log.debug('wavelength**4: '+str(wavelength.to('m')**4))
    log.debug('N0: '+str(N0))
    log.debug('N0*wavelength**4: '+str(N0*wavelength.to('m')**4))
    log.debug('sigma_r: '+str(sigma_r))
    log.debug('sigma_r/N0: '+str(sigma_r/N0))
    return (sigma_r).value

