import numpy as np
import astropy.units as u


def compute_tau(v,m,model="Weryk2013"):
    """Compute the luminous efficiency.
    
    The ethod used is that described in ReVelle & Ceplecha (2001),
    taken from McAuliffe & Christou 2007.
    
    Parameters
    ----------
    v : float
        velocity in [km/s]
    m : float
        mass in [kg]
    
    Returns
    -------
    tau : float
        Luminous efficiency in percentage.
    
    """
    tau = np.where(v < 24.372,np.exp(-2.338 + np.log(v) + 
                     1.15 * np.tanh(0.38 * np.log(m))),
                     np.exp( -0.334 -
                    10.307  * np.log(v) +
                     9.781  * np.power(np.log(v),2) -
                     3.0414 * np.power(np.log(v),3) +
                     0.3213 * np.power(np.log(v),4) +
                     1.15   * np.tanh(0.38 * np.log(m))))
    """
    if (v < 24.372):
      tau = np.exp(-2.338 + np.log(v) + 
                     1.15 * np.tanh(0.38 * np.log(m)))
    else:
      tau = np.exp( -0.334 -
                    10.307  * np.log(v) +
                     9.781  * np.power(np.log(v),2) -
                     3.0414 * np.power(np.log(v),3) +
                     0.3213 * np.power(np.log(v),4) +
                     1.15   * np.tanh(0.38 * np.log(m)))
    """
    return tau


def compute_I(tau,v,dmdt): 
    """Computes the brightness of a meteor.
    
    The formula used is: I=-tau/2 * v^2 * dm/dt
    
    Parameters
    ----------
    tau : float
        Luminous efficiency in percentage.
    v : float
        velocity in [km/s]
    dmdt : float
        mass loss in [kg/s]
    
    Returns
    -------
    I : float
        brightness in [Watts]=[J/s]
    
    """
    I = np.where(np.abs(dmdt)<1.0E-30,0.0,-0.5 * tau * np.power(v,2.0) * dmdt)
    return I

def ItoH(I):
    """Computes the absolute magnitude from Opik (1958).
    
    This formula is taken from McAuliffe & Christou 2007:
    H=24.3-2.5*log(I_cgs), with I_cgs=I_SI * 10^7
    
    Parameters
    ----------
    I : float
        brightness in [Watts]=[J/s]
    
    Returns
    -------
    H : float
        absolute Magnitude [no unit]
    
    """
    H = np.where(I<1.0E-30,99.9,24.3-2.5*np.log10(I*1.0E+07))
    return H


def HtoI(H):
    """Computes the instrinsic brightness absolute magnitude.
    
    The formulae is taken from Opik (1958), found in McAuliffe & Christou 2007:
    H=24.3-2.5*log(I_cgs), with I_cgs=I_SI * 10^7
    
    Parameters
    ----------
    H : float
        absolute Magnitude [no unit]
    
    Returns
    -------
    I : float
        brightness in [Watts]=[J/s]
    
    """
    I=np.power(10.0,(24.3-H)/2.5) * 1.0E-07
    return I

def ItoMag(I,r):
    """Computes the magnitude from brightness and range.
    
    This computes the instrinsic brightness absolute magnitude
    from the observed brightness and the range from observer.
    
    Parameters
    ----------
    I : float
        brightness in [Watts]=[J/s]
    r : float
        range in [m]
    
    Returns
    -------
    Mag : float
        apparent Magnitude [no unit]
    
    """
    # apparent brightness [W=J/s]
    I_app=ItoIapp(I,r)
    # apparent Magnitude [mag]
    Mag=ItoH(I_app)
    return Mag


def ItoIapp(I,r):
    """Get apparent brigthness from Intrinsic brightness and range.
    
    Parameters
    ----------
    I : float
        instrinsic brightness in [Watts=J/s]
    r : float
        range in [m]
    
    Returns
    -------
    I_app : float
        apparent brightness in [Watts=J/s]
    
    """
    # apparent brightness [W=J/s]
    I_app=I * (100000.0/r)**2
    return I_app

def MagtoI(Mag,r):
    """Computes Intrinsic brightness from apparent magnitude range from observer.
    
    Parameters
    ----------
    Mag : float
        apparent Magnitude [no unit]
    r : float
        range in [m]
    
    Returns
    -------
    I : float
        instrinsic brightness in [Watts=J/s]
    """
    I_app=1.0E-07 * np.power(10.0,(24.3-Mag)/2.5)   # apparent brightness [W=J/s]
    I=I_app * np.power(r/100000.0,2)                # intrinsic brightness [W=J/s]
    return I

def MagtoF0(MagInst,MagCat):
    """Compute the FluxMag0 of matched stars.
    
    This is used for absolute photometry ; it is also used
    to compute the extinction in a linear approach (Bouguer law).
    The formula used is: Flux0 = 10^((MagCat-MagInst)/2.5)
    
    Parameters
    ----------
    MagInst : float
        instrumental magnitude of the star in [ADU]
    MagCat :  float
        catalog magnitude of the star
    
    Returns
    -------
    Flux0 : float
        Flux of the star fictiously at "zero" magnitude
    
    """
    Flux0 = np.power(10,(MagCat-MagInst)/2.5)
    return Flux0

def dmdt(radius,velocity):
    """Computes dm/dt with the formula given by Borovicka 2007.
    Just to check if values returned by the AFM program are correct.
        
    Parameters
    ----------
    radius: astropy.units.Quantity Object
        radius of the meteoroids [m]
    velocity: astropy.units.Quantity Object
        velocity of the meteoroid [m/s]
        
    Returns
    -------
    dmdt: astropy.units.Quantity Object
        time derivative of mass [kg/s]
        
    """
    dmdt=[]
    for (R,V) in zip(radius.to('m'), velocity.to('m/s')):
        dmdt_step = -(np.pi*R.value*R.value*1E-6*V.value*V.value*V.value)/(4*1E6)
        dmdt.append(dmdt_step)
        
    return dmdt*u.kg/u.s

def absolute_mag_McAuliffe(radius,velocity,mass,dmdt):
    """Computes the absolute magnitude with the fomula given by McAuliffe & Christou 2007.
        
    Parameters
    ----------
    R: astropy.units.Quantity Object
        radius of the meteoroids [m]
    V: astropy.units.Quantity Object
        velocity of the meteoroid [km/s]
    M: astropy.units.Quantity Object
        mass of the meteoroid [kg]
        
    Returns
    -------
    H: astropy.units.Quantity Object
        apparent magnitude of the meteor [mag]
            
    """
    H=[]
    for (R,V,M,dmdt) in zip(radius.to('m'), velocity.to('km/s'), mass.to('kg'), dmdt.to('kg/s')):
        tau = compute_tau(V.value,M.value)
        #log.debug('tau: '+str(tau))
        I = -0.5*tau*V.value*V.value*dmdt*u.W
        #I = compute_I(tau,V.value,dmdt)*u.W
        #log.debug('I: '+str(I.value))
        mag = ItoH(I.value)
        H.append(mag)
            
    return H*u.mag

def absolute_mag_Boro(radius,velocity,mass,dmdt):
    """Computes the absolute magnitude with the formula given by Borovicka et al. 2007.
        
    Parameters
    ----------
    R: astropy.units.Quantity Object
        radius of the meteoroids [m]
    V: astropy.units.Quantity Object
        velocity of the meteoroid [km/s]
    M: astropy.units.Quantity Object
        mass of the meteoroid [kg]
        
    Returns
    -------
    H: astropy.units.Quantity Object
        apparent magnitude of the meteor [mag]
        
    """
    H=[]
    for (R,V,M,dmdt) in zip(radius.to('m'), velocity.to('km/s'), mass.to('kg'), dmdt.to('kg/s')):
        tau = compute_tau(V.value,M.value)
        #log.debug('tau: '+str(tau))
        I = -0.5*tau*V.value*V.value*dmdt*u.W
        #I = compute_I(tau,V.value,dmdt)*u.W
        #log.debug('I: '+str(I.value))
        mag = -2.5*np.log10(I.value)
        H.append(mag)
            
    return H*u.mag

def absolute_mag_CampbellBrown(radius,velocity,mass,dmdt):
    """Computes the absolute magnitude with the formula given by Campbell-Brown & Koschny 2004.
        
    Parameters
    ----------
    R: astropy.units.Quantity Object
        radius of the meteoroids [m]
    V: astropy.units.Quantity Object
        velocity of the meteoroid [km/s]
    M: astropy.units.Quantity Object
        mass of the meteoroid [kg]
        
    Returns
    -------
    H: astropy.units.Quantity Object
        apparent magnitude of the meteor [mag]
        
    """
    H=[]
    for (R,V,M,dmdt) in zip(radius.to('m'), velocity.to('km/s'), mass.to('kg'), dmdt.to('kg/s')):
        tau = compute_tau(V.value,M.value)
        #log.debug('tau: '+str(tau))
        I = -0.5*tau*V.value*V.value*dmdt*u.W
        #I = compute_I(tau,V.value,dmdt)*u.W
        #log.debug('I: '+str(I.value))
        mag = 6.8-1.086*np.log(I.value)
        H.append(mag)
            
    return H*u.mag

def mag_loss_distance(dist_km):
    """Compute the magnitude loss (or gain), due to distance between
    observer and meteor.
    
    Parameters
    ----------
    dist_km : float or astropy.units.Quantity object.
        range in [km]
    
    Returns
    -------
    dm : astropy.units.Quantity object
        Magnitude loss, in [mag].
    
    """
    try:
        d = dist_km.to('km').value
    except:
        d = dist_km
    # reference distance in km
    h = 100.0 # [km]
    mag_loss = 2.5 * np.log10((d/h)**2)
    return mag_loss*u.mag

def mag_loss_appvel(omega_met,pix_angle,fps):
    """Compute the magnitude loss (or gain), due to apparent velocity.
    
    Parameters
    ----------
    omega_met : astropy.units.Quantity object.
        apparent meteor velocity in [deg/s]
    pix_angle : astropy.units.Quantity object.
        subtended angular size of the sky imaged by the pixel, in [deg]
    fps : astropy.units.Quantity object.
        frame rate, in [s-1].
    
    Returns
    -------
    dm : astropy.units.Quantity object
        Magnitude loss, in [mag].
    
    """
    # reference omega in [deg/s]
    omega_ref = pix_angle * fps.to('s-1')
    mag_loss = np.where(omega_met.to('deg.s-1') < omega_ref,
                        0.0*u.mag,
                        2.5 * np.log10((omega_met.to('deg.s-1')/omega_ref.to('deg.s-1')))*u.mag)
    """
    log.debug('omega_met='+str(omega_met.to('deg.s-1')))
    log.debug('pix_angle='+str(pix_angle))
    log.debug('fps='+str(fps))
    log.debug('omega_ref='+str(omega_ref))
    log.debug('mag_loss='+str(mag_loss))
    """
    return mag_loss


def mass2radius(mass,density=2500.0*u.kg/u.m**3):
    """Convert mass to sphere radius.
    
    Parameters
    ----------
    mass : float
        Mass in [kg]
    density : float, optional
        density in [kg.m-3]. Default is 2.5 g.cm-3.
    
    Returns
    ------
    radius : float
        Equivalent sphere radius.
    
    """
    radius = (3.0 * mass / (4.0 * np.pi * density))**(1.0/3.0)
    return radius

def mass2side(mass,density=2500.0*u.kg/u.m**3):
    """Convert mass to cubic side.
    
    Parameters
    ----------
    mass : float
        Mass in [kg]
    density : float, optional
        density in [kg.m-3]. Default is 2.5 g.cm-3.
    
    Returns
    ------
    side : float
        Equivalent cubic side.
    
    """
    side = ( mass / density )**(1.0/3.0)
    return side


def mass2cylinder_length(mass,diameter=1.0*u.cm,density=2500.0*u.kg/u.m**3):
    """Convert mass to cylinder parameters.
    
    Parameters
    ----------
    mass : float
        Mass in [kg]
    diameter : float, optional
        cylinder diameter, in [m]. Default is 1 cm.
    density : float, optional
        density in [kg.m-3]. Default is 2.5 g.cm-3.
    
    Returns
    ------
    side : float
        Equivalent cylinder length.
    
    """
    #mass = length * density * np.pi * diameter**2.0 / 4.0 
    length = 4.0 * mass.to('kg') / (density.to('kg.m-3') * diameter.to('m')**2.0)
    return length

def mag2mass_Hughes1995(mag,velocity):
    """Converion magnitude to mass.
    
    From Hughes (1995), "The Perseid meteor shower", Earth, Moon & Planets, 68:31-70,
    The formulae has been converted in to international system by J. Vaubaillon, IMCCE.
    
    Parameters
    ----------
    mag : float
        Absolute magnitude in [mag].
    velocity : float
        Velocity, in [m.s-1]
    
    Returns
    -------
    mass : float
        Mass in [kg]
    """
    mass = 10.0**(14.7-(4.0*np.log10(velocity.to('m.s-1').value))-(0.4*mag.value))*u.kg
    return mass

def mass2mag_Hughes1995(mass,velocity):
    """Converion mass to magnitude.
    
    From Hughes (1995), "The Perseid meteor shower", Earth, Moon & Planets, 68:31-70,
    The formulae has been converted into international system by J. Vaubaillon, IMCCE.
    
    Parameters
    ----------
    mass : float
        Mass in [kg]
    velocity : float
        Velocity, in [m.s-1]
    
    Returns
    -------
    mag : float
        Absolute magnitude in [mag].
    """
    mag = (np.log10(mass.to('kg').value) - 14.7 + (4.0*np.log10(velocity.to('m.s-1').value))) / 0.4 *u.mag
    return mag

def mag2mass_Jacchia1967(mag,velocity):
    """Converion magnitude to mass.
    
    From Jacchia (1967), "Selected results from Precisionreduced super-Schmidt meteors",
    Smithsonian Contributions to Astrophysics, Vol. 11, p.1.
    The original formula in cgs is (for a zenith angle=0):
        mag = 55.34-8.75*log10(v_cgs)-2.25*log10(mass_cgs)
        or
        mass_cgs = 10.0**(24.5955+2.88*np.log10(velocity.to('cm.s-1').value)-mag.value/2.25)
    
    Parameters
    ----------
    mag : float
        Absolute magnitude in [mag].
    velocity : float
        Velocity, in [m.s-1]
    
    Returns
    -------
    mass : float
        Mass in [kg]
    """
    mass_Jacchia1967_cgs = 10.0**(24.5955+2.88*np.log10(velocity.to('cm.s-1').value)-mag.value/2.25)*u.g
    return mass_Jacchia1967_cgs.to('kg')

def mass2mag_Jacchia1967(mass,velocity):
    """Converion mass to magnitude.
    
    From Jacchia (1967), "Selected results from Precisionreduced super-Schmidt meteors",
    Smithsonian Contributions to Astrophysics, Vol. 11, p.1.
    The original formula in cgs is (for a zenith angle=0):
        mag = 55.34-8.75*log10(v_cgs)-2.25*log10(mass_cgs)
        or
        mass_cgs = 10.0**(24.5955+2.88*np.log10(velocity.to('cm.s-1').value)-mag.value/2.25)
    
    Parameters
    ----------
    mass : float
        Mass in [kg]
    velocity : float
        Velocity, in [m.s-1]
    
    Returns
    -------
    mag : float
        Absolute magnitude in [mag].
    """
    mag_Jacchia1967 = 55.34-8.75*np.log10(velocity.to('cm.s-1').value)-2.25*np.log10(mass.to('g').value)
    return mag_Jacchia1967*u.mag


def mag2mass_JacchiaWhipple(mag,velocity):
    """Convert magnitude to mass, from Jacchia & Whipple.
    
    From P. Jenniskens book, page 46:
    log(M[g]) = 6.31 - 0.4*mag - 3.92*np.log10(velocity [km/s]) - 0.41*np.log10(np.sin(h))
    with h: zenith distance of the meteor.
    Note: here we consider h=0 deg.
    
    Parameters
    ----------
    mag : float
        Absolute magnitude in [mag].
    velocity : float
        Velocity, in [m.s-1]
    
    Returns
    -------
    mass : float
        Mass in [kg]    
    
    """
    mass_g = 10.0**(6.31 - 0.4*mag.value - 3.92*np.log10(velocity.to('km.s-1').value)) *u.g
    return mass_g.to('kg')

def mass2mag_JacchiaWhipple(mass,velocity):
    """Convert mass to magnitude, from Jacchia & Whipple.
    
    From P. Jenniskens book, page 46:
    log(M[g]) = 6.31 - 0.4*mag - 3.92*np.log10(velocity [km/s]) - 0.41*np.log10(np.sin(h))
    with h: zenith distance of the meteor.
    Note: here we consider h=0 deg.
    
    Parameters
    ----------
    mass : float
        Mass in [kg]   
    velocity : float
        Velocity, in [m.s-1]
    
    Returns
    ------- 
    mag : float
        Absolute magnitude in [mag].
    
    """
    mag = 2.5* (6.31 - np.log10(mass.to('g').value) - 3.92*np.log10(velocity.to('km.s-1').value) )
    return mag*u.mag

