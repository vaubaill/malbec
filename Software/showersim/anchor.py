"""Definition of an Anchor object.
This is useful to create Fakeor objects.

Author: J. Vaubaillon
IMCCE, 2020

"""
import os
import astropy.units as u
import spiceypy as sp
from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import EarthLocation

from missionplanner.utils import log

class Anchor(object):
    """An Anchor object serves to fix a highest point of a Fakeor.
    
    Parameters
    ----------
    name : str
        name of the Anchor.
    eloc : astropy.coordinates.EarthLocation Object
        location of the Anchor
    time : astropy.time.Time object
        time of the anchor.
    azimuth : astropy.coordinates.Angle object
        azimuth of the fakeor.
    
    """
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,name):
        self.__name = name
    
    @property
    def time(self):
        return self.__time
    @time.setter
    def time(self,time):
        self.__time = time
    
    @property
    def azimuth(self):
        return self.__azimuth
    @azimuth.setter
    def azimuth(self,azimuth):
        self.__azimuth = azimuth
    
    @property
    def eloc(self):
        return self.__eloc
    @eloc.setter
    def eloc(self,eloc):
        self.__eloc = eloc
    
    @property
    def data(self):
        return self.__data
    @data.setter
    def data(self,data):
        self.__data = data
    
    @property
    def spice_id(self):
        return self.__spice_id
    @spice_id.setter
    def spice_id(self,spice_id):
        self.__spice_id = spice_id
    
    @property
    def spice_name(self):
        return self.__spice_name
    @spice_name.setter
    def spice_name(self,spice_name):
        self.__spice_name = spice_name
    
    
    @property
    def spice_topoframe_file(self):
        return self.__spice_topoframe_file
    @spice_topoframe_file.setter
    def spice_topoframe_file(self,spice_topoframe_file):
        self.__spice_topoframe_file = spice_topoframe_file
    
    @property
    def spice_topoframe_name(self):
        return self.spice_topoframe_name
    @spice_topoframe_name.setter
    def spice_topoframe_name(self,spice_topoframe_name):
        self.__spice_topoframe_name = spice_topoframe_name
    
    name = None
    eloc = None
    time = None
    azimuth = None
    data = None
    spice_id = None
    spice_name = None
    spice_topoframe_file = None
    spice_topoframe_name = None
    
    def __init__(self,name='Anchor',logger=False):
        """Instances an Anchor object.
        
        Parameters
        ----------
        name : string, optional
            Anchor name. Default is 'Anchor'.
        
        Returns
        -------
        None.
        
        """
        self.name = name
        return
    
    def from_eloc(self,eloc,time,azimuth):
        """Creates an Anchor Object from Station Objects.
        
        Fakeor parameters are set.
        
        Parameters
        ----------
        eloc : astropy.coordinates.EarthLocation Object.
            Location of Anchor in ITRF93 frame.
        time : astropy.time.Time Object
            Time of the Fakeor.
        azimuth : astropy.Coordinates.Angle Object, optional
            Azimuth of the fakeor.
        
        Returns
        -------
        None.
        
        """
        # set Fakeor object variables
        self.azimuth = azimuth
        self.eloc = eloc
        self.time = time
        # make fakeor QTable
        self.make_data_table()
        # fillin QTable
        self.table_fillin()
        return
    
    def make_data_table(self):
        """Make data QTable.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        self.data = QTable(names=['Name','Time','Azimuth','Lon','Lat','Alt'],dtype=['str','object','float','float','float','float'])
        self.data['Lon'].unit = 'deg'
        self.data['Lat'].unit = 'deg'
        self.data['Alt'].unit = 'm'
        self.data['Azimuth'].unit = 'deg'
        return
    
    def table_fillin(self):
        """Fill in QTable.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        self.data.add_row([self.name,self.time.isot,self.azimuth,self.eloc.lon,self.eloc.lat,self.eloc.height])
        return
    
    def save(self,fileout):
        """Save Anchor coordinates into file.
        
        Parameters
        ----------
        fileout : string
            Output file name.
        
        Returns.
        --------
        None.
        
        """
        # make output QTable
        self.make_data_table()
        # fill in QTable
        self.table_fillin()
        # write QTable
        self.data.write(fileout,format='ascii.fixed_width_two_line',overwrite=True)
        log.info("Anchor data saved in "+fileout)
        return
    
    def load(self,filein):
        """Load Anchor coordinates from file.
        
        Fakeor Objects variables are set.
        
        Parameters
        ----------
        filein : string
            Full path input file name.
        
        Returns
        -------
        None.
        
        """
        if not os.path.exists(filein):
            msg = '*** FATAL ERROR: file '+filein+' does not exist'
            log.error(msg)
            raise IOError(msg)
        #self.make_data_table()
        log.debug('Reading Anchor file: '+filein)
        self.data = QTable.read(filein,format='ascii.fixed_width_two_line')
        self.eloc = EarthLocation.from_geodetic(self.data['Lon'][0]*u.deg,self.data['Lat'][0]*u.deg,height=self.data['Alt'][0]*u.m)
        self.azimuth = self.data['Azimuth'][0]*u.deg
        self.name = self.data['Name'][0]
        self.make_topoframe_name()
        self.time = Time(self.data['Time'][0],scale='utc')
        log.info("Anchor data loaded from "+filein)
        return
    
    def make_topoframe_name(self):
        """Make TOPO-like frame name.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        self.spice_topoframe_name = self.name.upper() + '_TOPO'
        return
    
    def make_topolike_frame(self,spice_id=39900001,spice_topoframe_file='./anchor_topo.fk',spice_exe_path='/astrodata/exe/',kernel_path='/astrodata/kernels/'):
        """Make a topocentric-like SPICE frame.
        
        Parameters
        ----------
        spice_id : long integer
            SPICE Anchor id.
            Default is 39900001.
        spice_topoframe_file : string, optional
            Output SPICE frame file name.
            Default is './anchor_topo.fk'.
        spice_exe_path : string, optional
            SPICE path where pinpoint utility programs is installed.
            Default is '/astrodata/exe/'.
        kernel_path : string, optional
            SPICE kernel directory containing all necessary kernels.
            Default is './'.
        
        Returns
        -------
        None.
        
        """
        # set SPICE id
        self.spice_id = spice_id
        # set output fk frame file name.
        self.spice_topoframe_file = spice_topoframe_file
        self.make_topoframe_name()
        # check that pinpoint program exists
        if not os.path.exists(spice_exe_path + '/pinpoint'):
            msg = '*** FATAL ERROR: '+spice_exe_path + '/pinpoint program does not exist'
            log.error(msg)
            raise IOError(msg)
        # sets input standard pck kernel
        inputpckfile = kernel_path + '/pck00010.tpc'
        # set output directory
        out_dir = os.path.dirname(self.spice_topoframe_file) + '/'
        log.debug('self.spice_topoframe_file: '+self.spice_topoframe_file)
        log.debug('out_dir: '+out_dir)
        # sets output files
        outputdefnsfile = out_dir + '/' + os.path.basename(self.spice_topoframe_file).replace('.fk','.defns')
        outputspkfile = out_dir + '/' + os.path.basename(self.spice_topoframe_file).replace('.fk','.bsp')
        
        # delete previously created sites kernel files
        if os.path.exists(outputdefnsfile):
            os.remove(outputdefnsfile)
        if os.path.exists(outputspkfile):
            os.remove(outputspkfile)
        if os.path.exists(self.spice_topoframe_file):
            os.remove(self.spice_topoframe_file)
        
        # now starts printing the outputdefnsfile file
        with open(outputdefnsfile, 'w') as out:
            out.write("\\begindata \n")
            out.write("SITES = ( \n")
            out.write("        " + repr(self.name.upper()) + ", \n")
            out.write(") \n")
            # now prints the outputdefnsfile file
            out.write("\n")
            out.write("      " + self.name.upper() + "_CENTER = 399 \n")
            out.write("      " + self.name.upper() + "_FRAME = " + repr('ITRF93') + "\n")
            out.write("      " + self.name.upper() + "_IDCODE = " + str(self.spice_id) + "\n")
            out.write("      " + self.name.upper() + "_LATLON = (" + str(self.eloc.lat.to('deg').value) + ',' + str(self.eloc.lon.to('deg').value) + ',' + str(self.eloc.height.to('km').value) + ")\n")
            out.write("      " + self.name.upper() + "_UP = " + repr('Z') + "\n")
            out.write("      " + self.name.upper() + "_NORTH = " + repr('X') + "\n")
            out.write(" \n")
        # now executes the pinpoint program to create the spk kernel file
        cmd = spice_exe_path + '/pinpoint -def ' + outputdefnsfile + " -spk " + outputspkfile + " -pck " + inputpckfile + " -fk " + self.spice_topoframe_file + " -batch"
        log.info("cmd=" + cmd)
        os.system(cmd)
        log.info("data saved in " + outputspkfile + " and " + self.spice_topoframe_file)
        
        # load topo-like frame kernels
        log.info('Loading anchor kernel: '+outputspkfile)
        sp.furnsh(outputspkfile)
        log.info('Loading anchor topo-like frame kernel: '+self.spice_topoframe_file)
        sp.furnsh(self.spice_topoframe_file)
        
        # store kernel file names
        self.bsp_file = outputspkfile
        # retrieve Anchor spice id and name
        self.spice_id = sp.spkobj(self.bsp_file)[0]
        self.spice_name = sp.bodc2n(self.spice_id)
        
        return
