"""Define several distributions.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project

"""
import powerlaw
import numpy as np
import matplotlib.pyplot as plt

def powerlaw_gen(gamma,k_min,k_max,n):
    """Generate a power law distributed population.
    
    Parameters
    ----------
    k_min :  float
        minimum output number.
    k_max :  float
        maximum output number.
    n : int
        number of particles to generate.
    gamma : float
        power law distribution index.
    
    Returns
    -------
    data : numpy [n]-size array of float
        population of number generated with power law distribution.
    
    """
    y = np.random.random(n)
    return ((k_max**(-gamma+1) - k_min**(-gamma+1))*y  + k_min**(-gamma+1.0))**(1.0/(-gamma + 1.0))


def pareto_gen(shape,minimum,n):
    """Generate a population with Pareto function.
    
    Parameters
    ----------
    shape : float
        Shape of the distribution. Should be greater than zero.
    minimum : float
        Minimum output number.
    n: int
        number of particles to generate.
    
    Returns
    -------
    data : numpy [n]-size array of float
        population of number generated with Pareto distribution.
    
    """
    idx = shape if (shape<0) else -shape
    return (np.random.pareto(-idx - 1,size=n) + 1) * minimum


def powerlaw2_gen(shape,minimum,n):
    """Generate a population with Pareto function.
    
    Parameters
    ----------
    shape : float
        Shape of the distribution. Should be greater than zero.
    minimum : float
        Minimum output number.
    n: int
        number of particles to generate.
    
    Returns
    -------
    data : numpy [n]-size array of float
        population of number generated with Powr law distribution.
    
    """
    return (minimum**(-shape + 1) * np.random.random(n))**(1 / (-shape + 1))

def powerlaw3_gen(shape,minimum,n):
    """Generate a population with powerlaw.Power_Law package.
    
    Parameters
    ----------
    shape : float
        Shape of the distribution. Should be greater than zero.
    minimum : float
        Minimum output number.
    n: int
        number of particles to generate.
    
    Returns
    -------
    data : numpy [n]-size array of float
        population of number generated with Powr law distribution.
    
    """
    theoretical_distribution = powerlaw.Power_Law(xmin=minimum,parameters=[shape])
    return  theoretical_distribution.generate_random(n)

def powerlaw4_gen(shape,minimum,maximum,n):
    """Generate a population with powerlaw.Power_Law package.
    
    Parameters
    ----------
    shape : float
        Shape of the distribution. Should be greater than zero.
    minimum : float
        Minimum output number.
    n: int
        number of particles to generate.
    
    Returns
    -------
    data : numpy [n]-size array of float
        population of number generated with Powr law distribution.
    
    """
    theoretical_distribution = powerlaw.Power_Law(xmin=minimum,xmax=maximum,parameters=[shape])
    return  theoretical_distribution.generate_random(n)

def fit_pop(data,plot=False):
    """Fit a population.
    
    Parameters
    ----------
    data : array of numpy float
        population data to analyse.
    plot : boolean, optional
        If True a plot of data PDF is drawn.
    
    Returns
    -------
    fit : power_law.Fit Object
        Fit of population.
    
    """
    fit = powerlaw.Fit(data)
    print('----------')
    print('N:'+str(len(data)),' min: '+str(np.min(data))+' max: '+str(np.max(data)))
    fit = powerlaw.Fit(data)
    print('alpha: '+str(fit.power_law.alpha)+' sigma: '+str(fit.power_law.sigma))
    fig=plt.figure()
    powerlaw.plot_pdf(data, color='b')
    plt.show()
    return fit

