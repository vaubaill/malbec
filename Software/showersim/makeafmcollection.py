"""Make a collection of AFM simulations.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project.

"""
import os
import astropy.units as u
from astropy.time import Time as Time

from showersim import afm

# set home directory
home = os.getenv('HOME') + '/'

def make_afm_collection(all_Velocity,all_zenith_angle_range,all_Mass_range,time_init=Time('2000-01-01T00:00:00.000',scale='utc'),deltat_t=1.0*u.min,fps=100/u.s,out_dir='./'):
    """Make a collection of AFM simulations
    
    Data are saved in outdir.
    
    Parameters
    ----------
    Velocity_range : astropy.units.Quantity object
        Velocity range.
    zenith_angles : astropy.units.Quantity object
        Zenith angle range.
    M_range : 2-array of astropy.units.Quantity object
        Mass range.
    fps : astropy.units.Quantity object
        Frame per second. Default is 100/u.s.
    out_dir : string, optional
        Output directory name. Default is './'.
    
    Returns
    -------
    None.
    
    """
    afm_time = time_init 
    # loop over velocity
    for Vel in all_Velocity:
        # loop over zenith angle
        for zen_ang in all_zenith_angle_range:
            # loop over mass
            for Mass in all_Mass_range:
                # simulation code
                afm_code = + str(round(Vel.to('m/s').value,2)) + '_kms_E' + 'E' + '{:3.1e}'.format(Mass.to('kg').value).split('e')[1] + '_kg_' + str(round(zen_ang.to('deg').value,2)) + '_deg'
                # create full output directory name
                full_out_dir = out_dir + '/' + afm_code
                # create AFM object
                afmo = afm.AFM()
                # create AFM configuration file name
                afmconf = full_out_dir + afm_code + '.cf'
                # increse time
                afm_time = afm_time + deltat_t
                # write AFM configuration file
                afm_dummy_radius = 0.01*u.m
                afmo.write_config(afmconf,afm_time,Vel,zen_ang,fps,afm_dummy_radius,full_out_dir,mass=Mass)
                # launch AFM
                #afm_intfile = afmo.launch(config_file=afmconf,sub=True)
    
    
    
    return
