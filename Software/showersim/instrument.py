"""Instrument Object.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project.


"""

import numpy as np
import spiceypy as sp
#import astropy.units as u
#from astropy.time import Time
#from astropy.table import QTable,Table

from missionplanner import utils
from obsplanner.make_id import make_nacelle_orientation_frame_id,make_nacelle_orientation_frame_name

from missionplanner.utils import log
from reduction.camera import Camera


class Instrument(Camera):
    """Instrument object.
    
    """
    
    # name
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,name):
        self.__name = name
    
    # SPICE id
    @property
    def id(self):
        return self.__id
    @id.setter
    def id(self,id):
        self.__id = id
    
    # frame name
    @property
    def frame_name(self):
        return self.__frame_name
    @frame_name.setter
    def frame_name(self,frame_name):
        self.__frame_name = frame_name
    
    # SPICE kernel
    @property
    def kernel(self):
        return self.__kernel
    @kernel.setter
    def kernel(self,kernel):
        self.__kernel = kernel
    
    # SPICE frame kernel
    @property
    def frame_kernel(self):
        return self.__frame_kernel
    @frame_kernel.setter
    def frame_kernel(self,frame_kernel):
        self.__frame_kernel = frame_kernel
    
    # number of obervations
    @property
    def nobs(self):
        return self.__nobs
    @nobs.setter
    def nobs(self,nobs):
        self.__nobs = nobs
    
    
    def __init__(self,name,spice_id,camera):
        """Initializes a Instrument object.
        
        Parameters
        ----------
        name : str, optional
            name of the Fakeor objects. Default is 'FakeObs'.
        instr_id : int, optional
            SPICE id.
        
        Returns
        -------
        None.
        
        """
        # inherit Camera object properties
        super().__init__()
        # set name
        self.name = name
        # set SPICE id if needed
        self.spice_id = spice_id
        # set frame name
        self.frame_name = make_nacelle_orientation_frame_name(self.name)
        self.frame_id = make_nacelle_orientation_frame_id(self.spice_id)
        # set camera
        self.camera = camera
        return
    
    def make_ik(self,fov_frame_name,ikfile):
        """Make all necessary kernels.
        
        Parameters
        ----------
        kernel : string
            SPICE kernel file
        fov_frame_name : string
            SPICE field of view frame name.
        ikfile : string
            Output SPICE instrument kernel file
        
        Returns
        -------
        None.
        
        """
        # set ikfile
        self.ikfile = ikfile
        
        # get fov info
        [fov_az,fov_el] = self.camera.fov
        
        # Compute boresight vector, pointing in the direction
        # of the X-axis of the camera/instrument frame kernel,
        # defined in create_tk routine.
        boresight = np.array([1.0,0.0,0.0]) # X-vector
        refvector = np.array([0.0,1.0,0.0]) # Y-vector
        log.info('instrument name='+self.name)
        log.info('instrument id  ='+str(self.id))
        log.info('fov_az='+str(fov_az))
        log.info('fov_el='+str(fov_el))
        log.info('boresight='+str(boresight))
        log.info('refvector='+str(refvector))
        
        # ====================================================
        # now make instrument kernel for MALBEC station camera
        with open(self.ikfile, 'w') as out:
            out.write("\\begindata \n")
            out.write('    NAIF_BODY_NAME += ('+repr(self.name)+')\n')
            out.write('    NAIF_BODY_CODE += ('+str(self.spice_id)+')\n\n\n')
            out.write("\\begintext \n")
            out.write("  Definition of the pointing direction and FOV of MALBEC cameras instruments. \n\n")
            out.write("   \n")
            out.write("  Author: J. Vaubaillon - IMCCE - 2020, A. Rietze, Univ. Oldenburg \n")
            out.write("  MALBEC project \n\n\n")
            out.write("Definition of the" + self.camera.name + " FOV: \n \n")
            out.write("\\begindata \n")
            out.write("INS" + str(self.spice_id) + "_FOV_FRAME = " + repr(fov_frame_name) + " \n")
            out.write("INS" + str(self.spice_id) + "_FOV_SHAPE = " + repr("RECTANGLE") + " \n")
            out.write("INS" + str(self.spice_id) + "_BORESIGHT = ( " + str(boresight[0]) + "   " + str(
                boresight[1]) + "  " + str(boresight[2]) + " ) \n")
            # defining the corners with the ref_vector, ref_angle and cross_angle
            out.write("INS" + str(self.spice_id) + "_FOV_CLASS_SPEC = " + repr("ANGLES") + " \n")
            out.write("INS" + str(self.spice_.id) + "_FOV_REF_VECTOR = ( " + str(refvector[0]) + "   " + str(
            refvector[1]) + "   " + str(refvector[2]) + " )  \n")
            out.write("INS" + str(self.spice_id) + "_FOV_REF_ANGLE =  " + str(fov_az.to('deg').value / 2) + " \n")
            out.write("INS" + str(self.spice_id) + "_FOV_CROSS_ANGLE =  " + str(fov_el.to('deg').value / 2) + " \n")
            out.write("INS" + str(self.spice_id) + "_FOV_ANGLE_UNITS =  " + repr("DEGREES") + " \n\n")
            out.write("\\begintext \n")
        log.info("data saved in " + self.ikfile)
        return
    
    
    def load(self,ikfile,frame_name,frame_kernel=None,camera=None):
        """Initializes a FakeObs object.
        
        Parameters
        ----------
        name : str, optional
            name of the Fakeor objects. Default is 'FakeObs'.
        
        Returns
        -------
        None.
        
        """
        # set name
        self.ikfile = ikfile
        utils.check_file(self.ikfile, log)
        log.info('Loading Instrument kernel: '+self.ikfile)
        sp.furnsh(self.ikfile)
        # check SPICE id
        check_id = sp.spkobj(self.ikfile)[0]
        if not self.spice_id==check_id:
                msg = '*** FATAL ERROR: instrument id '+str(self.spice_id)+' different from kernel '+self.ikfile+' id: '+str(check_id)
                raise ValueError(msg)
        self.cov = sp.spkcov(self.ikfile,self.spice_id)
        self.start,self.stop = sp.wnfetd(self.cov,0)
        log.info('id'+str(self.spice_id))
        log.info('coverage: start: '+sp.et2utc(self.start,'ISOC',2,50))
        log.info('coverage: stop: '+sp.et2utc(self.stop,'ISOC',2,50))
        self.frame_name = frame_name
        # set and load frame kernel, if needed
        if frame_kernel:
            self.frame_kernel = frame_kernel
            log.info('Loading Instrument frame kernel: '+self.frame_kernel)
            utils.check_file(self.frame_kernel, log)
            sp.furnsh(self.frame_kernel)
        return
    

        
