"""Launches the AFM program.

The AFM program (IMCCE, J. Vaubaillon, R. DeCosta) simulates the disintegration
and fragmentation of a meteoroid in a planetary atmosphere.
This module handles the writing of the AFM configuration file, and the launch
of the program.

Author : J. Vaubaillon
28/07/2020

Copyright : IMCCE, Observatoire de Paris

Free licence

"""

import os
import shutil
import subprocess
from astropy.table import QTable
import astropy.units as u
from astropy.time import Time as Time

from missionplanner.utils import log

# set home directory
home = os.getenv('HOME') + '/'


def time2name(time):
    """Converts an Astropy Time object into a string name.
    
    Output is in the format: 'YYYYMMDDTHHMMSS_UT'
    This is useful to get e.g. the path of a meteor/fakeor event.
    
    Parameters
    ----------
    time: astropy Time object
        Time of the quantity
    
    Returns
    -------
    date_name: str
        time converted into a string, using the 'YYYYMMDDTHHMMSS_UT' format.
    
    See Also
    --------
    The time2dir function.
    
    """
    d=time.iso
    date_name=d[0:4]+d[5:7]+d[8:10]+'T'+d[11:13]+d[14:16]+d[17:19]+"_UT"
    return date_name


def time2dir(time):
    """Converts an Astropy Time object into a directory name in 'YYYYMMDDTHHMMSS_UT/' format.
    
    Output is in the format: 'YYYYMMDDTHHMMSS_UT/'
    This is useful to get e.g. the path of a meteor/fakeor event.
    
    Parameters
    ----------
    time: astropy Time object
        Time of the quantity
    
    Returns
    -------
    dir_name: str
        name of default directory where the data are to be found.
    
    See Also
    --------
    The time2name function.
    
    """
    return time2name(time)+"/"


def VZMtoname(V,Z,M):
    """Convert a set of Velocity, zenith angle and mass to a name.
    
    Parameters
    ----------
    V : astropy.units.Quantity object
        Velocity.
    Z : astropy.units.Quantity object
        Zenight angle.
    M : astropy.units.Quantity object
        Mass.
    
    Returns
    -------
    name : string
        Name based on parameters.
    
    """
    name = str(round(V.to('km/s').value,1)) + '_kms_E' + '{:3.1e}'.format(M.to('kg').value).split('e')[1] + '_kg_' + str(int(Z.to('deg').value)) + '_deg'
    return name


def VZRtoname(V,Z,R):
    """Convert a set of Velocity, zenith angle and radius to a name.
    
    Parameters
    ----------
    V : astropy.units.Quantity object
        Velocity.
    Z : astropy.units.Quantity object
        Zenight angle.
    R : astropy.units.Quantity object
        Radius.
    
    Returns
    -------
    name : string
        Name based on parameters.
    
    """
    #name = str(round(V.to('km/s').value,1)) + '_kms_E' + '{:3.1e}'.format(R.to('m').value).split('e')[1] + '_m_' + str(int(Z.to('deg').value)) + '_deg'
    name = str(round(V.to('km/s').value,1)) + 'kms_' + '{:3.1e}'.format(R.to('m').value) + 'm_' + str(int(Z.to('deg').value)) + 'deg'
    return name


def make_afm_collection(all_Velocity,all_zenith_angle_range,all_Mass_range,time=Time('2020-04-01T00:00:00.000',scale='utc'),fps=100/u.s,out_dir='/data/processed/Fakeor/AFM_simul/AFM_poirier/'):
    """Make a collection of AFM simulations
    
    Data are saved in outdir.
    
    Parameters
    ----------
    Velocity_range : astropy.units.Quantity object
        Velocity range.
    zenith_angles : astropy.units.Quantity object
        Zenith angle range.
    M_range : 2-array of astropy.units.Quantity object
        Mass range.
    time : astropy.time.Time Object.
        Time. Default is '2020-04-01T00:00:00.000'.
    fps : astropy.units.Quantity object
        Frame per second. Default is 100/u.s.
    out_dir : string, optional
        Collection output directory name.
        Default is '/data/processed/Fakeor/AFM_simul/AFM_poirier/'.
    
    Returns
    -------
    intfile_collection : list of string
        List of AFM result files.
    
    """
    intfile_collection = []
    # loop over velocity
    for Vel in all_Velocity:
        # loop over zenith angle
        for zen_ang in all_zenith_angle_range:
            # loop over mass
            for Mass in all_Mass_range:
                log.debug('Vel '+str(Vel))
                log.debug('zen_ang '+str(zen_ang))
                log.debug('Mass '+str(Mass))
                # simulation code
                afm_code = VZMtoname(Vel,zen_ang,Mass)
                # create full output directory name
                full_out_dir = out_dir + '/' + afm_code + '/'
                afm_intfile = full_out_dir + '/tables/STEPS_int.dat0'
                if os.path.exists(afm_intfile):
                    msg = '*** AFM simulation already performed in: '+afm_intfile+' kipped...'
                    log.info(msg)
                    intfile_collection.append(afm_intfile.replace('//','/').replace('//','/').replace('//','/').replace('//','/'))
                    continue
                else:
                    os.makedirs(full_out_dir)
                # create AFM object
                afmo = AFM()
                # create AFM configuration file name
                afmconf = full_out_dir + afm_code + '.cf'
                log.info('AFM configuration file: '+afmconf)
                # set dummy radius, since it's the mass that will be considered.
                afm_dummy_radius = 0.01*u.m
                # write AFM configuration file
                afmo.write_config(afmconf,time,Vel,zen_ang,fps,afm_dummy_radius,full_out_dir,mass=Mass)
                # launch AFM
                afm_intfile = afmo.launch(config_file=afmconf,sub=True).replace('//','/').replace('//','/').replace('//','/').replace('//','/')
                log.info('Results saved in '+afm_intfile)
                # now move results one directory up to ignore the timing data
                src = os.path.dirname(afm_intfile)
                dst = src.replace(afmo.eventname,'')
                log.debug('src: '+src)
                log.debug('dst: '+dst)
                log.debug('afm.eventname: '+afmo.eventname)
                log.debug('afm.eventdir: '+afmo.eventdir)
                # remove already existing dst
                if os.path.isdir(dst):
                    log.debug('dst already exist: removing: '+dst)
                    shutil.rmtree(dst)
                try:
                    shutil.move(src,dst)
                except:
                    msg = '*** FATAL ERROR: impossible to move '+src+' into '+dst
                    log.error(msg)
                    raise IOError(msg)
                # now remove useless directory
                src = '/'.join(src.split('/')[:-1])
                log.debug('Removing directory: '+src)
                shutil.rmtree(src)
                
                afm_intfile = afm_intfile.replace(afmo.eventname,'')
                log.debug('afm_intfile: '+afm_intfile)
                intfile_collection.append(afm_intfile)
    log.info('intfile_collection: '+str(intfile_collection))
    log.info('Done.')
    return intfile_collection





class AFM(object):
    
    def __init__(self,afm_exe=home+'/PROJECTS/PODET/PODET-MET/ADMIRE/AFM/AFM_poirier/exe/AFM.x',tmpdir=home+'/tmp/'):
        """Initializes an AFM Object.
        
        Parameters
        ----------
        afm_exe : string, optional
            The full AFM executable.
            Default is 'exe/'
        tmpdir : string, optional
            Sub directory where the AFM output data directories are temporarily stored.
            Default is '/tmp/'
        
        Returns
        -------
        None.
        
        """
        self.afm_exe = afm_exe
        self.tmpdir = tmpdir
        return
    
    def write_config(self,config_file,time,velocity,entry_angle,fps,radius,outpath,altitude=120.0*u.km,mass=None,mass_stop=1.0E-15*u.kg,frag=False,VZR=False,template_file=home+'/PROJECTS/PODET/PODET-MET/ADMIRE/AFM/AFM_poirier/user/in/userfile/templateBoro.in'):
        """Creates a configuration file for the AFM Fortran program.
        
        Parameters
        ----------
        config_file : str
            name of the AFM configuration file.
        time : astropy.time Time object
            time of the event considered. This will define the 'eventdir' subdirectory
            where the data will be saved.
        velocity : astropy.units.Quantity Object
            atmosphere entry velocity modulus
        entry_angle : astropy.units.Quantity Object
            atmosphere entry angle
        fps : astropy.units.Quantity Object
            Data output frequency.
        radius : astropy.units.Quantity Object
            meteoroid radius.
        outpath : str
            name of the output directory.
        altitude : astropy.units.Quantity Object, optional
            meteoroid altitude. Default is 120.0 km.
        mass : astropy.units.Quantity Object, optional
            meteoroid mass. This is useful if you wish to specify mass instead of radius.
            Default is None, meaning that radius is provided rather than mass.
        mass_stop, optional
            meteoroid mass below which the computation stops.
            Default is 1.0E-09 kg.
        frag: boolean, optional
            to choose if fragmentation is possible. Default is False.
        VZR: boolean, optional
           to choose the name of the file where AFM results will be saved.
           False: self.eventname = time2dir(time)
           True: self.eventname = VZRtoname(velocity,entry_angle,radius) + '/'
           Default is False.
        template_file : str, optional
            name of the AFM template configuration file.
        
        Returns
        -------
        None
        
        """
        # store outpath
        self.outpath = outpath+'/'
        if (not os.path.isdir(self.outpath)):
            msg = '*** FATAL ERROR: directory: '+outpath+' does not exist'
            log.error(msg)
            raise IOError(msg)
        # store config_file
        #np.where(meteorite,self.eventname = VZRtoname(velocity,entry_angle,radius),self.eventname = time2dir(time))
        if VZR:
            self.eventname = VZRtoname(velocity,entry_angle,radius) + '/'
        else:
            self.eventname = time2dir(time)
        #self.eventname = time2dir(time)
        self.eventdir = self.outpath+'/'+self.eventname
        # read template file
        with open(template_file,'r') as fin:
            lines = fin.readlines()
        # write output file
        with open(config_file,'w') as out:
            for line in lines:
                line = line[0:-1]
                if ('defoutdir') in line:
                    line = '#defoutdir     : '+self.outpath+'          # default output directory        -OK \n'
                    line = line + 'defoutdir      : '+self.tmpdir+self.eventname+'/'+'          # default output directory        -OK'
                if ('outfps') in line:
                    line = 'outfps         : '+'{:.1f}'.format(fps.to('/s').value)+'                     # data output frequency      [/s] -OK-'
                if 'vm0' in line:
                    line = 'vm0            : '+'{:+.5e}'.format(velocity.to('km/s').value)+' # initial value of speed                 [km/s] -OK-'
                if 'hm0' in line:
                    line = 'hm0            : '+'{:+.5e}'.format(altitude.to('km').value)+' # initial value of altitude                 [km] -OK-'
                if 'Om0' in line:
                    line = 'Om0            : '+'{:+.5e}'.format(entry_angle.to('deg').value)+' # initial value for zenithal angle        [deg] -OK-'
                if 'mstop' in line:
                    line = 'mstop          : '+'{:+.5e}'.format(mass_stop.to('kg').value)+'  # mstop : mass cut off limit              [kg] -OK-'
                if 'ifra' in line:
                    if frag:
                        line = 'ifra           : 1 # fragmentation  (0/1)                                     -OK-'
                    else:
                        line = 'ifra           : 0 # fragmentation  (0/1)                                     -OK-'
                # mass versus radius
                if mass is not None:
                    if 'mm0' in line:
                        line = 'mm0            : '+'{:+.5e}'.format(mass.to('kg').value)+' # initial value of mass                    [kg] -OK-\n'
                    if 'rm0' in line:
                        line = '#rm0            : +1.00000E-02  # initial value of radius                 [m] -OK-'
                else:
                    if 'mm0' in line:
                        line = '#mm0            : +1.00000E-02 # initial value of mass                    [kg] -OK-'
                    if 'rm0' in line:
                        line = 'rm0            : '+'{:+.5e}'.format(radius.to('m').value)+' # initial value of radius                    [m] -OK-\n'
                log.debug(line)
                out.write(line+'\n')
        log.info('Configuration file saved in '+config_file)
        log.info(' done')
        return
    
    def launch(self,config_file='./conf/config_afm.in',sub=True):
        """Launches an instance of AFM Fortran program
        
        Parameters
        ----------
        config_file : string, optional
            name of the AFM program configuration file.
            Default is './conf/config_afm.in'.
        sub : boolean, optional
            if True then the AFM program is launched. Set to False for debug or
            development purpose. Default is True.
        
        
        Returns
        -------
        outintfile : string
            full path name of the file containing the data produced by AFM program.
        
        """
        if not os.path.isdir(self.tmpdir):
            msg = '*** FATAL ERROR: temporary directory: '+self.tmpdir+' not found'
            log.error(msg)
            raise IOError(msg)
        if not os.path.isfile(self.afm_exe):
            msg = '*** FATAL ERROR: AFM executable: '+self.afm_exe+' not found'
            log.error(msg)
            raise IOError(msg)
        if not os.path.isfile(config_file) :
            msg = '*** FATAL ERROR: file: '+config_file+'does not exist'
            log.error(msg)
            raise IOError(msg)
        # copy config file into temporary directory
        self.config_file = config_file
        self.tmp_config_file = self.tmpdir + os.path.basename(self.config_file)
        src = self.config_file
        dst = self.tmp_config_file
        try:
            log.debug('copying '+src+' to '+dst)
            shutil.copyfile(src, dst)
        except:
            msg = '*** FATAL ERROR: impossible to copy '+src+' into '+dst
            log.error(msg)
            raise IOError(msg)
        # now changes directory because Data files in AFM are relative-path defined
        cwd = os.getcwd()       # get current directory
        log.debug('Relocating to AFM exe directory: '+os.path.dirname(self.afm_exe))
        os.chdir(os.path.dirname(self.afm_exe))  # move to AFM directory
        cmd = os.path.basename(self.afm_exe)+ " -i "+self.tmp_config_file
        if (sub):
            log.info("=== Now submitting the command: "+cmd)
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            log.info('out='+str(out).replace('\\n','\n'))
            log.info('err='+str(err))
            log.info("=== AFM simulation done.")
        else:
            log.info("=== Submission command not performed: sub="+str(sub))
        log.debug('Relocating to current directory: '+cwd)
        os.chdir(cwd)           # go back to initial directory
        # now copy data from temporary directory to specified output directory
        src = self.tmpdir+self.eventname
        dst = self.eventdir
        # first remove already existing directory if needed
        if os.path.isdir(dst):
            log.debug('Removing data in directory: '+dst)
            shutil.rmtree(dst)
        try:
            log.debug('moving results from '+src+' into '+dst)
            shutil.move(src,dst)
        except:
            msg = '*** FATAL ERROR: impossible to move '+src+' into '+dst
            log.error(msg)
            raise IOError(msg)
        # copy config file into output directory
        src = self.tmp_config_file
        dst = self.eventdir + os.path.basename(self.config_file)
        try:
            log.debug('copying '+src+' to '+dst)
            shutil.copyfile(src, dst)
        except:
            msg = '*** FATAL ERROR: impossible to copy '+src+' into '+dst
            log.error(msg)
            raise IOError(msg)
        # make results file name
        outintfile=self.eventdir+'/tables/STEPS_int.dat0'
        log.info("results are in file: "+outintfile)
        log.info(' done')
        
        return outintfile
    
    
    def get_intfile_fromcollection(self,V,Z,M,coldir='/data/processed/Fakeor/AFM_simul/AFM_poirier/'):
        """Get AFM.intfile from a pre-computed collection of files.
        
        Parameters
        ----------
        V : astropy.units.Quantity object
            Velocity.
        Z : astropy.units.Quantity object
            Zenight angle.
        M : astropy.units.Quantity object
            Mass.
        
        Returns
        -------
        outintfile : string
            full path name of the file containing the data produced by AFM program.
            None if it does not exist.
        
        """
        outintfile = None
        # create AFM code from V,Z,M
        afm_code = VZMtoname(V,Z,M)
        # create full output directory name
        full_intfile = coldir + '/' + afm_code + '/tables/STEPS_int.dat0'
        if os.path.exists(full_intfile):
            log.info('*** AFM simulation data found: '+full_intfile)
            outintfile = full_intfile
        else:
            log.info('*** AFM simulation data NOT found: '+full_intfile)
        return outintfile
    
    def read_int(self,intfile):
        """Read AFM results.
        
        Parameters
        ----------
        intfile : string
            AFM integration output file
        
        Returns
        -------
        None.
        
        """
        # checks that the AFM file does exists and has enough data
        if not os.path.isfile(intfile):
            msg = "*** FATAL ERROR: file:"+intfile+" does not exist"
            log.error(msg)
            raise IOError(msg)
        file_stat = os.stat(intfile)
        if (file_stat.st_size < 1000):
            msg = "*** FATAL ERROR: file "+intfile+"does not seem to contain enough data"
            log.error(msg)
            raise IOError(msg)
        # reads the AFM file
        log.info("reading file: "+intfile)
        self.data=QTable.read(intfile,format='ascii.commented_header',
                            names=('Id','time','mass','radius','velocity',
                            'altitude','zenith_angle','distance','Xm','Ym','T',
                            'dm/dt','dr/dt','dv/dt','dh/dt','dz/dt',
                            'dl/dt','dXm/dt','dYm/dt','dT/dt'))
        # putting the unit to get quantity objects
        self.data['time'].unit='s'
        self.data['mass'].unit='kg'
        self.data['radius'].unit='m'
        self.data['velocity'].unit='km/s'
        self.data['altitude'].unit='km'
        self.data['zenith_angle'].unit='deg'
        self.data['distance'].unit='km'
        self.data['Xm'].unit='m'
        self.data['Ym'].unit='m'
        self.data['T'].unit='K'
        self.data['dm/dt'].unit='kg/s'
        self.data['dr/dt'].unit='m/s'
        self.data['dv/dt'].unit='km/s2'
        self.data['dh/dt'].unit='km/s'
        self.data['dz/dt'].unit='deg/s'
        self.data['dl/dt'].unit='km/s'
        self.data['dXm/dt'].unit='m/s'
        self.data['dYm/dt'].unit='m/s'
        self.data['dT/dt'].unit='K/s'
        return
    
    