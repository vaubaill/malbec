"""Shower simulation.

Simulates a meteor shower.

Authors:
Jeremie Vaubaillon (IMCCE, France)
The MALBEC project, 2020

"""


import os
import spiceypy as sp
#import powerlaw
import astropy.units as u
from astropy.time import Time
from astropy.modeling.functional_models import Moffat1D # ,Exponential1D

# import MALBEC utilities
from missionplanner.utils import log
from showersim import shower_utils,shower_radiant

# set home directory
home = os.getenv('HOME') + '/'


class MeteorShower (object):
    """Meteor Shower Object.
    
    Parameters
    ----------
    Name : string
        Name of the shower
    Ventry : astropy.units.Quantoty object.
        Entry velocity of meteors.
    population_index : float
        Population index.
    mass_index : float
        Mass index.
    cmass_index : float
        Cumulative Mass index.
    N : float.
        Number of random points to generate. Default is 1000.
    time_max : astropy.time.Time object.
        Time of shower maximum.
    et_max : float.
        Time of shower maximum in SPICE format (sec after J2000).
    ZHR : astropy.units.Quantity Object, optional
        Shower ZHR at time of maximum. Default is 10/hr.
    fwhm : astropy.units.Quantity Object, optional
        Shower full width at half maximum. Default is 2 hours.
    profile : array of astropy.units.Quantoty object.
        Profile of the shower as a function of time.
    ra : astropy.coordinate.Angle object, optional.
        Right ascension of the radiant. Default is 0*u.deg.
    dec : astropy.coordinate.Angle object, optional.
        Declination of the radiant. Default is 0*u.deg.
    dra : astropy.coordinate.Angle object, optional.
        Right ascension drift. Default is 0*u.deg/u.d.
    ddec : astropy.coordinate.Angle object, optional.
        Declination drift. Default is 0*u.deg/u.d.
    radiant : showersim.shower_radiant.Shower_Radiant object.
        Shower Radiant.
    
    """
    # version of the program
    __version__='1.0'
    
    #Camera private attributes
    @property
    def Name(self):
        return self.__Name
    @Name.setter
    def Name(self,Name):
        self.__Name = Name
    
    @property
    def Ventry(self):
        return self.__Ventry
    @Ventry.setter
    def Ventry(self,Ventry):
        self.__Ventry = Ventry
    
    @property
    def population_index(self):
        return self.__population_index
    @population_index.setter
    def population_index(self,population_index):
        self.__population_index = population_index
    
    @property
    def mass_index(self):
        return self.__mass_index
    @mass_index.setter
    def mass_index(self,mass_index):
        self.__mass_index = mass_index
    
    @property
    def cmass_index(self):
        return self.__cmass_index
    @cmass_index.setter
    def cmass_index(self,cmass_index):
        self.__cmass_index = cmass_index
    
    @property
    def N(self):
        return self.__N
    @N.setter
    def N(self,N):
        self.__N = N
    
    @property
    def time_max(self):
        return self.__time_max
    @time_max.setter
    def time_max(self,time_max):
        self.__time_max = time_max
        self.et_max = sp.str2et(self.__time_max.isot)
    
    @property
    def et_max(self):
        return self.__et_max
    @et_max.setter
    def et_max(self,et_max):
        self.__et_max = et_max
    
    @property
    def ZHR(self):
        return self.__ZHR
    @ZHR.setter
    def ZHR(self,ZHR):
        self.__ZHR = ZHR
    
    @property
    def fwhm(self):
        return self.__fwhm
    @fwhm.setter
    def fwhm(self,fwhm):
        self.__fwhm = fwhm
    
    @property
    def profile(self):
        return self.__profile
    @profile.setter
    def profile(self,profile):
        self.__profile = profile
    
    @property
    def ra(self):
        return self.__ra
    @ra.setter
    def ra(self,ra):
        self.__ra = ra
    
    @property
    def dec(self):
        return self.__dec
    @dec.setter
    def dec(self,dec):
        self.__dec = dec
    
    @property
    def dra(self):
        return self.__dra
    @dra.setter
    def dra(self,dra):
        self.__dra = dra
    
    @property
    def ddec(self):
        return self.__ddec
    @ddec.setter
    def ddec(self,ddec):
        self.__ddec = ddec
    
    @property
    def radiant(self):
        return self.__radiant
    @radiant.setter
    def radiant(self,radiant):
        self.__radiant = radiant
    
    
    # logger set is False until set
    log_set = False
    
    def __init__(self,Name,Ventry,population_index,ra=0.0*u.deg,dec=0.0*u.deg,dra=0.0*u.deg/u.d,ddec=0.0*u.deg/u.d,min_mag=-4.0,N=1000,radiant=None,ZHR=10.0/u.hr,fwhm=2.0*u.hr,time_max=Time('2000-01-01T00:00:00.000',scale='utc'),leap_sec_ker='/astrodata/kernels/naif0012.tls'):
        """Initialize ShowerSimulation Object.
        
        Parameters
        ----------
        Name : string
            Name of shower to simulate.
        Ventry : astropy.units.Quantity Object
            Atmosphere entry velocity.
        population_index : float
            magnitude population index, also called r in meteor science.
        min_mag : float or astropy.units.Quantity object, optional.
            Minimum Magnitude of the generated population. Default is -4.0
        N : float, optional.
            Number of random points to generate. Default is 1000.
        radiant : shower_radiant.Shower_Radiant Object, optional
            Shower radiant. Default is None.
        ra : astropy.coordinate.Angle object, optional.
            Right ascension of the radiant. Default is 0*u.deg.
        dec : astropy.coordinate.Angle object, optional.
            Declination of the radiant. Default is 0*u.deg.
        dra : astropy.coordinate.Angle object, optional.
            Right ascension drift. Default is 0*u.deg/u.d.
        ddec : astropy.coordinate.Angle object, optional.
            Declination drift. Default is 0*u.deg/u.d.
        ZHR : astropy.units.Quantity Object, optional
            Shower ZHR at time of maximum. Default is 10/hr.
        fwhm : astropy.units.Quantity Object, optional
            Shower full width at half maximum. Default is 2 hours.
        time : astropy.time.Time Object, optional
            Time of maximum of the shower. Default is J2000.
        leap_sec_ker : string, optional
            SPICE leap second kernel, used to convert UTC to/from SPICE time.
            Default is '/astrodata/kernels/naif0012.tls'.
        
        et_max : float
            Time of maximum in SPICE format (sec since J2000).
        
        Returns
        -------
        None.
        
        """
        # load SPICE leap second kernel
        sp.furnsh(leap_sec_ker)
        # set parameters
        self.Name = Name
        self.Ventry = Ventry
        self.population_index = population_index
        self.min_mag = min_mag
        self.N = N
        self.ZHR = ZHR
        self.fwhm = fwhm
        self.time_max = time_max
        # create shower radiant object
        if radiant:
            self.radiant = radiant
        else:
            self.set_radiant(ra,dec,dra,ddec,time_max)
        
        # creates shower profile object
        self.profile = Moffat1D(amplitude=self.__ZHR.to('/hr').value,gamma=self.__fwhm.to('hr').value/2)
        #self.profile = Exponential1D(amplitude=self.__ZHR.to('/hr').value,tau=self.__fwhm.to('hr').value/2)
        
        # compute all index
        self.mass_index = shower_utils.mag_idx2mass_idx(self.population_index)
        self.cmass_index = shower_utils.mass_idx2cmass_idx(self.mass_index)
        self.sz_idex = shower_utils.mass_idx2sz_idx(self.mass_index)
        
        return
    
    def set_radiant(self,ra,dec,dra,ddec,time_max):
        """Set shower radiant Object and parameters.
        
        Parameters
        ----------
        
        """
        self.radiant = shower_radiant.Shower_Radiant(ra=ra.to('deg'),dec=dec.to('deg'),dra=dra.to('deg/d'),ddec=ddec.to('deg/d'),time_max=self.__time_max)
        self.ra = self.__radiant.ra
        self.dec = self.__radiant.dec
        self.dra = self.__radiant.dra
        self.ddec = self.__radiant.ddec
        self.time_max = self.__radiant.time_max
        return
    
    def get_ZHR(self,time):
        """Get the ZHR for a given time.
        
        Parameters
        ----------
        time : astropy.time.Time Object, optional
            Time for which ZHR is to be retrieved.
        
        Returns
        -------
        ZHR_t : astropy.units.Quantity Object
            Shower ZHR at time time.
        
        """
        # compute time difference with time of maximum, in [hr]
        dt = (time - self.time_max)
        
        # compute ZHR at time time
        zhr = self.__profile(dt.to('hr').value) / u.hr
        log.debug('ZHR: '+str(zhr))
        return zhr
    
    def get_drifted_radiant(self,time):
        """Get radiant position at a given time, by applying the radiant drift.
        
        Parameters
        ----------
        time : astropy.time.Time Object.
            Time for which radiant coordinates are to be computed.
        
        Returns
        -------
        drifted_radiant : astropy.coordinates.SkyCoord object.
            Coordinates of the radiant after applying radiant drift.
        
        """
        drifted_radiant = self.__radiant.drift(time)
        return drifted_radiant
    
    def get_entry_angle(self,eloc,time):
        """Computes Earth Atmosphere entry angle for a given date and location.
        
        Parameters
        ----------
        eloc : astropy.coordinates.EarthLocation Object.
            Location on Earth where to compute azimuth-elevation.
        time : astropy.time.Time Object.
            Time for which radiant coordinates are to be computed.
        
        Returns
        -------
        entry_angle : astropy.coordinates.Angle object.
            Entry angle.
        
        """
        # drift radiant and compute radiant alt,az
        self.__radiant.to_altaz(time,eloc)
        """
        # drifted radiant as SkyCoord object
        self.__radiant.drift(time)
        # get drifted radiant azimuth-elevation
        altaz = drifted_radiant.transform_to(AltAz(obstime=time,location=eloc))
        """
        entry_angle = 90.0*u.deg - self.__radiant.altaz.alt.to('deg')
        log.debug('entry_angle at '+time.isot+' from '+str(eloc.to_geodetic())+' = '+str(entry_angle))
        return entry_angle
    
    def get_eyeHR(self,eloc,time,LM):
        """Get naked eye Hourly Rate given observation location, time and limiting magnitude.
        
        Parameters
        ----------
        eloc : astropy.coordinates.EarthLocation Object.
            Location on Earth where the observation takes place.
        time : astropy.time.Time Object.
            Time for which naked eye HR is to be computed.
        
        Returns
        -------
        eyeHR : astropy.units.Quantity object.
            Hurly rate, for a naked eye FOV.
        
        """
        # get ZHR at time t
        ZHR = self.get_ZHR(time)
        # drift radiant and compute azimuth/elevation
        self.__radiant.to_altaz(time,eloc)
        # compute HR for nake eye
        (eyeHR,mag_gain,f_radel,f_LM,LM_eff) = shower_utils.ZHR2HR(ZHR,
                                            self.__population_index,
                                            self.__radiant.altaz.alt.to('deg'),
                                            LM,LM_naked_eye=6.0*u.mag,
                                            h_meteor=100*u.km,
                                            cam_dist=(100.0*u.km-eloc.height.to('km')))
        log.debug('time='+time.isot)
        log.debug('ZHR='+str(ZHR))
        log.debug('eyeHR='+str(eyeHR))
        log.debug('mag_gain='+str(mag_gain))
        log.debug('f_radel='+str(f_radel))
        log.debug('f_LM='+str(f_LM))
        log.debug('LM_eff='+str(LM_eff))
        return eyeHR
    
    def info(self):
        """Prints the Meteor_Shower information.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        log.info('Shower name: '+self.__Name)
        self.__radiant.info()
        log.info('Ventry: '+str(self.__Ventry))
        log.info('time@max: '+self.__time_max.isot)
        log.info('zhr@max: '+str(self.__ZHR))
        log.info('r: '+str(self.__population_index))
        density_3D = shower_utils.zhr2spaceD(self.__ZHR,self.__population_index,self.__Ventry)
        log.info('meteoroid 3D density: '+str(density_3D))
        return
    
    def create_pop(self,N=1000,min_mag=-4.0*u.mag):
        """Create an artificial population of meteors.
        
        Parameters
        ----------
        N : float, optional.
            Number of random points to generate. Default is 1000.
        min_mag : float or astropy.units.Quantity object, optional.
            Minimum Magnitude of the generated population. Default is -4.0
        
        Returns
        -------
        None.
        
        """
        # set variables
        self.min_mag = min_mag
        self.N = N
        # creates a population of particles
        self.pop_mag = shower_utils.make_mag_pop(self.__population_index,self.min_mag,self.__N)
        return
    
    def create_masspop(self,N=1000,min_mass=1.0E-06*u.kg,max_mass=1.0E00*u.kg):
        """Create an artificial population of meteor masses.
        
        Parameters
        ----------
        N : float, optional.
            Number of random points to generate. Default is 1000.
        min_mass : float or astropy.units.Quantity object, optional.
            Minimum mass of the generated population. Default is 1.0E-06*u.kg.
        max_mass : float or astropy.units.Quantity object, optional.
            Maximum mass of the generated population. Default is 1.0E+00*u.kg.
        
        Returns
        -------
        None.
        
        """
        # set variables
        self.N = N
        # creates a population of particles
        self.pop_mass = shower_utils.make_mass_pop(self.__population_index,min_mass,max_mass,self.__N)
        return
