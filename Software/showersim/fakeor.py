"""Definition of a Fakeor object.

Author: J. Vaubaillon
IMCCE, 2020

"""
import os
import logging
import shutil
import numpy as np
import simplekml
import spiceypy as sp
from scipy import interpolate
import astropy.units as u
from astropy.time import Time
from astropy.table import QTable
from astropy.coordinates import EarthLocation

from showersim import afm, phot
from missionplanner.utils import log

afmo=afm.AFM()


def Sphe2Rec(r,theta,phi):
    """Convert spherical coordinates into rectangular.
    
    Parameters
    ----------
    r : float
        Distance of a point from the origin.
    theta : float
        Angle of the point from the Z-axis in radians.
    phi : float
        Angle of the point from the XZ plane in radians.
    
    Returns
    -------
    x,y,z : 3-float
        cartesian coordinates.
    
    """
    x = r*np.cos(phi)*np.sin(theta)
    y = r*np.sin(phi)*np.sin(theta)
    z = r*np.cos(theta)
    return (x,y,z)


def rotZvec(vec,angle):
    """Rotate a vector about Z axis by angle.
    
    Parameters
    ----------
    vec : 3-float vector
        Vector to rotate.
    angle : astropy.coordinates.Angle object
        Angle of rotation, counterclockwise.
    
    
    Returns
    -------
    rotvec3-float vector
        rotated Vector.
    """
    rotvec_x =  vec[0] * np.cos(angle.to('rad').value) + vec[1] * np.sin(angle.to('rad').value)
    rotvec_y = -vec[0] * np.sin(angle.to('rad').value) + vec[1] * np.cos(angle.to('rad').value)
    rotvec_z =  vec[2]
    return np.array([rotvec_x,rotvec_y,rotvec_z])

class Fakeor(object):
    """Fake meteor object.
    
    A Fakeor is a fake meteor (term invented by from G. Barentsen 2009).
    
    """
    def __init__(self,name='',load=False,std_kernel='/astrodata/kernels/standard.ker',logger=False):
        """Initializes a Fakeor object.
        
        Parameters
        ----------
        name : str, optional
            name of the Fakeor objects. Default is ''.
        load : boolean, optional
            If True, standard SPICE kernel is loaded. Default is False.
        std_kernel : str, optional
            name of the SPICE standard kernel to load.
            Default is '/astrodata/kernels/standard.ker'.
        logSH : boolean, optional
            If True, a logging.StreamHandler is created. Default is False.
        
        Returns
        -------
        None.
        
        """
        self.name=name
        if load:
            sp.furnsh(std_kernel)
        return
    
    def set_log_level(self,level=logging.DEBUG):
        """Set Logger object.
        
        Parameters
        ----------
        level : logging level object, optional.
            logging level. Default is logging.DEBUG.
        
        Returns
        -------
        None.
        
        """
        # set log level
        log.setLevel(level)
        return
    
    
    
    
    def from_afm_anchor(self,afmobj,anchobj,r=0.0,outdir='./'):
        """creates a Fakeor from an AFM and an Anchor objects.
        
        Parameters
        ----------
        afmobj : showersim.afm.AFM object
            AFM object containing the results of the simulation
            of the desintegration of a meteoroid in a planetary atmosphere.
        anchobj : showersim.anchor.Anchor object
            Anchor where fakeor starts
        outdir : string, optional
            Output directory where Ahcnor object frame file will be created if needed.
            Default is './'.
        
        
        Returns
        -------
        None.
        
        """
        # store data
        self.anchor = anchobj
        self.afm = afmobj
        # load Anchor SPICE kernels
        self.load_anchor_kernels()
        # make time
        self.time = self.anchor.time + self.afm.data['time']
        self.rel_time = self.afm.data['time']
        # create afm vector in Anchor topo-like frame
        afm_vec_x = self.afm.data['Xm'].to('m')
        afm_vec_y = np.repeat(0.0,len(afm_vec_x))*u.m
        afm_vec_z = self.afm.data['altitude'].to('m') - self.anchor.eloc.height.to('m')
        afm_vec_vx = self.afm.data['dXm/dt'].to('m/s')
        afm_vec_vy = np.repeat(0.0,len(afm_vec_x))*u.m/u.s
        afm_vec_vz = self.afm.data['dh/dt'].to('m/s')
        
        # create afm_vec_x interpolation function
        afm_x_interp_f = interpolate.interp1d(afm_vec_z.to('m').value,afm_vec_x.to('m').value,fill_value="extrapolate")
        # Value of x at anchor height: remember that afm_vec_z=0 at anchor height
        afm_x_at_anchor_height = afm_x_interp_f(0.0) * u.m
        """
        log.debug('Anchor height: '+str(self.anchor.eloc.height.to('m')))
        log.debug('afm_vec_z = '+str(afm_vec_z))
        log.debug('afm_vec_x = '+str(afm_vec_x))
        log.debug('afm_x_at_anchor_height=0 : '+str(afm_x_at_anchor_height))
        """
        # shift X so that X=0 at anchor altitude
        afm_vec_x = afm_vec_x - afm_x_at_anchor_height
        
        # make afm_vec vector before anchor azimuth rotation
        afm_vec_pos = [afm_vec_x,afm_vec_y,afm_vec_z]
        afm_vec_vel = [afm_vec_vx,afm_vec_vy,afm_vec_vz]
        
        # rotate afm_vec and afm_vec_v about Z axis of the azimuth in topo-like frame
        # Note that Anchor topo=like frame is: X=North, Y=West, Z=zenith
        self.pos_topo = rotZvec(afm_vec_pos,self.anchor.azimuth) *u.m
        self.vel_topo = rotZvec(afm_vec_vel,self.anchor.azimuth) *u.m/u.s
        
        # compute I and H from dm/dt
        self.tau = phot.compute_tau(self.afm.data['velocity'].to('km/s').value,self.afm.data['mass'].to('kg').value)
        log.debug('tau = '+str(self.tau))
        
        self.I = phot.compute_I(self.tau,
                                self.afm.data['velocity'].to('km/s').value,
                                self.afm.data['dm/dt'].to('kg/s').value) * u.W
        log.debug('I = '+str(self.I))
        
        self.H = phot.ItoH(self.I.to('W').value) * u.mag
        log.info("fakeor created. You might want to save it as spk file.")
        return
    
    def load_anchor_kernels(self):
        """Load Anchor SPICE kernels
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # load anchor TOPO-like frame
        if not self.anchor.spice_topoframe_file:
            msg = '*** FATAL ERROR: Anchor object MUST have a spice_topoframe_file for Fakeor to be created.'
            log.error(msg)
            raise ValueError(msg)
        if not os.path.exists(self.anchor.spice_topoframe_file):
            msg = '*** FATAL ERROR: Anchor topoframe_file does not exist: '+self.anchor.spice_topoframe_file
            log.error(msg)
            raise ValueError(msg)
        # load Anchor TOPO-like frame
        log.debug('Loading Anchor topo-like from from: '+self.anchor.spice_topoframe_file)
        sp.furnsh(self.anchor.spice_topoframe_file)
        log.debug('Loading Anchor bsp kernel from: '+self.anchor.bsp_file)
        sp.furnsh(self.anchor.bsp_file)
        # retrieve Anchor spice id and name
        self.anchor.spice_id = sp.spkobj(self.anchor.bsp_file)[0]
        self.anchor.spice_name = sp.bodc2n(self.anchor.spice_id)
        return
    
    def make_et(self):
        """Make SPICE et time.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        
        """
        self.et = []
        for t in self.time:
            self.et.append(sp.str2et(t.isot))
        self.et = np.array(self.et)        
        return
    
    def to_spk(self,spice_name,spice_id,spk_file,kernel_path='/astrodata/kernels/',spice_exe_path='/astrodata/exe/',tmpdir='/tmp/'):
        """Convert trajectory into SPICE spk file.
        
        Parameters
        ----------
        spice_id : integer
            SPICE id number of the object. See SPICE documentation for more info.
        spk_file : string
            SPICE kernel spk file full name.
        kernel_path : string, optional
            SPICE kernel files directory full name.
            Default is '/astrodata/kernels/'
        spice_exe_path : string, optional
            SPICE kernel executables directory full name.
            Default is '/astrodata/exe/'
        tmpdir : string, optional
            Temporary directory where files are created. This is useful if the path to the files
            are very long, since it causes the mkspk utility program to crash.
            Temporary files (e.g. trajectory in ascii format) andoutput files are first
            created in tmpdir directory, and then the output is saved in spk_file.
            Default is '/tmp/'.
        
        Returns
        -------
        None.
        
        """
        # set SPICE id, body-name
        self.spice_id = spice_id
        self.spice_name = spice_name
        self.spk_file = spk_file
        sp.boddef(self.spice_name,self.spice_id)
        # load Anchor SPICE data
        self.load_anchor_kernels()
        # creates the ephemeris data file
        names=['EPOCH','X','Y','Z','VX','VY','VZ']
        # get intermediate spk_dta file name
        data_file = tmpdir+os.path.basename(spk_file.replace('.spk','_spk.dat'))
        # intermediate setup file name
        setup_file = tmpdir+os.path.basename(spk_file.replace('.spk','.setup'))
        # intermediate output file
        tmpout_file = tmpdir+os.path.basename(spk_file)
        # double check that we are not about the erase an existing file
        for f2chk in [data_file,setup_file]:
            if f2chk==spk_file:
                msg = 'FATAL ERROR: '+spk_file+' is about to be erased: double check file name and make sure it contains spk'
                log.error(msg)
                raise ValueError(msg)
        log.debug('spk_file: '+spk_file)
        log.debug('data_file: '+data_file)
        log.debug('setup_file: '+setup_file)
        log.debug('tmpout_file: '+tmpout_file)
        
        # write intermediate raw spk data
        with open(data_file,'w') as out:
            out.write("# "+' '.join(names)+" \n")
            for (t,x,y,z,vx,vy,vz) in zip(self.time,self.pos_topo[0],self.pos_topo[1],self.pos_topo[2],
                                                    self.vel_topo[0],self.vel_topo[1],self.vel_topo[2]):
                # write time
                out.write(t.iso+"\n")
                # format data
                xstr = "{:.9f}".format(x.to('km').value)
                ystr = "{:.9f}".format(y.to('km').value)
                zstr = "{:.9f}".format(z.to('km').value)
                vxstr = "{:.9f}".format(vx.to('km/s').value)
                vystr = "{:.9f}".format(vy.to('km/s').value)
                vzstr = "{:.9f}".format(vz.to('km/s').value)
                # write data
                out.write(' '.join([xstr,ystr,zstr,vxstr,vystr,vzstr])+" \n")
        log.info("raw spk data saved in: "+data_file)
        # make a numpy array of self.et
        self.make_et()
        
        # copy Anchor frame kernel into temporary directory
        src = self.anchor.spice_topoframe_file
        dst = tmpdir + os.path.basename(src)
        try:
            shutil.copyfile(src, dst)
        except:
            msg = '*** FATAL ERROR: impossible to copy '+src+' into '+dst
            log.error(msg)
            raise IOError(msg)
        tmp_spice_topoframe_file = dst
        
        # make the intermediate setup file
        with open(setup_file,'w') as out:
            out.write("\\begindata \n")
            out.write("   INPUT_DATA_TYPE   = "+repr('STATES')+" \n")
            out.write("   OUTPUT_SPK_TYPE   = 13\n")
            out.write("   OBJECT_ID         = "+str(self.spice_id)+"\n")
            out.write("   OBJECT_NAME       = "+repr(self.spice_name)+"\n")
            out.write("   CENTER_ID         = "+str(self.anchor.spice_id)+"\n")
            out.write("   REF_FRAME_NAME    = "+repr(self.anchor.spice_topoframe_name)+"\n")
            out.write("   FRAME_DEF_FILE    = "+repr(tmp_spice_topoframe_file)+"\n")
            out.write("   PRODUCER_ID       = "+repr('J. Vaubaillon - IMCCE')+"\n")
            out.write("   DATA_ORDER        = "+repr(' '.join(names))+"\n")
            out.write("   DATA_DELIMITER    = "+repr(' ')+"\n")
            out.write("   LEAPSECONDS_FILE  = "+repr(kernel_path+'naif0012.tls')+"\n")
            out.write("   INPUT_DATA_FILE   = "+repr(data_file)+"\n")
            out.write("   OUTPUT_SPK_FILE   = "+repr(tmpout_file)+" \n")
            out.write("   PCK_FILE          = "+repr(kernel_path+'pck00010.tpc')+"\n")
            out.write("   INPUT_DATA_UNITS  = ("+repr('ANGLES=DEGREES')+' '+repr('DISTANCES=km')+") \n")
            out.write("   TIME_WRAPPER      = "+repr('# UTC')+"\n")
            out.write("   IGNORE_FIRST_LINE = 1 \n")
            out.write("   LINES_PER_RECORD  = 2 \n")
            out.write("   POLYNOM_DEGREE    = 3 \n")
            out.write("   SEGMENT_ID        = "+repr(str(spice_id))+"\n")
            out.write(" \n")
        log.info("setup saved in: "+setup_file)
        
        # remove spk file if it already exists
        if os.path.exists(tmpout_file):
            os.remove(tmpout_file)
        # build the command
        cmd = spice_exe_path+"mkspk -setup "+setup_file
        #log.info("cmd="+cmd)
        # launches the cmd
        try:
            os.system(cmd)
        except Exception:
            msg='*** FATAL ERROR: Impossible to submitt the cmd: '+cmd
            log.error(msg)
            raise IOError(msg)
        # double check that the SPK file was successfully created
        if not os.path.exists(tmpout_file):
            msg='*** FATAL ERROR: temporary SPK file: '+spk_file+' was not created.'
            log.error(msg)
            raise IOError(msg)
        # now copy temporary output file into spk_file
        outdir = os.path.dirname(spk_file)+'/'
        for file2move in [setup_file,data_file,tmpout_file]:
            dst = outdir+os.path.basename(file2move)
            if os.path.exists(dst):
                os.remove(dst)
            #log.debug('Moving file '+file2move+' into '+dst)
            try:
                # remark: os.remove does not always work when trying to move file from one FS to another...
                shutil.copy(file2move,dst)
            except:
                msg = '*** FATAL ERROR: impossible to move '+file2move+' into '+dst
                log.error(msg)
                raise IOError(msg)
        log.info("SPICE SPK kernel saved in: "+spk_file)
        
        # load spk file
        sp.furnsh(spk_file)
        return
    
    
    def save(self,fileout): 
        """Save all data into file.
        
        Parameters
        ----------
        fileout : string
            Full name output file name.
        
        Returns
        -------
        None.
        
        """
        outable=QTable([self.time,self.et,self.rel_time,self.tau,self.I,self.H],
                        names=['Time','et','rel_time','tau','I','H'],
                        dtype=['object','float','float','float','float','float'])
        outable['et'].info.format = '13.3f'
        outable['tau'].info.format = '6.3f'
        outable['I'].info.format = '8.2f'
        outable['H'].info.format = '8.2f'
        outable.write(fileout,format='ascii.fixed_width_two_line',delimiter=' ',
                    formats={'Time':'%23s'},overwrite=True)
        #log.info(str(len(self.H))+" data saved in "+fileout)
        return
    
    def read_int_fakeor(self,intfile):
        """Read Fakeor data.
        
        Parameters
        ----------
        intfile : string
            Fakeor output file.
        
        Returns
        -------
        None.
        
        """
        # checks that the Fakeor file does exist and has enough data
        if not os.path.isfile(intfile):
            msg = "*** FATAL ERROR: file:"+intfile+" does not exist"
            log.error(msg)
            raise IOError(msg)
        file_stat = os.stat(intfile)
        if (file_stat.st_size < 1000):
            msg = "*** FATAL ERROR: file "+intfile+"does not seem to contain enough data"
            log.error(msg)
            raise IOError(msg)
        # reads the Fakeor file
        log.info("reading file: "+intfile)
        self.data=QTable.read(intfile,format='ascii',
                            names=('Time','et','rel_time','tau','I','H'))
        # putting the unit to get quantity objects
        self.data['Time'].unit='s'
        self.data['et'].unit='s'
        self.data['rel_time'].unit='s'
        self.data['I'].unit='W'
        self.data['H'].unit='mag'

        return
    
    # loads all data from file
    def load(self,spkfile,framefile,datfile):
        """Load Fakeor
        
        Parameters
        ----------
        spkfile : string
            SPICE spk kernel file providing the trajectory.
        datfile : string
            Data file providing the physical data.
        
        Returns
        -------
        None.
        
        """
        # check if files exist
        for checkf in [spkfile,framefile,datfile]:
            if not os.path.exists(checkf):
                msg = '*** FATAL ERROR: file '+checkf+' does not exist.'
                log.error(msg)
                raise IOError(msg)
        # load frame file
        sp.furnsh(framefile)
        # load trajectory file
        sp.furnsh(spkfile)
        # set spice_id
        self.spice_id = sp.spkobj(spkfile)[0]
        self.spice_name = sp.bodc2n(self.spice_id)
        # load photometry file
        intable = QTable(names=['Time','rel_time','tau','I','H'],
                         dtype=['object','float','float','float','float']).read(datfile,
                         format='ascii.fixed_width_two_line')
        # store data into objects
        self.time = Time(intable['Time'],scale='utc')
        self.rel_time = intable['rel_time']
        self.tau = intable['tau']
        self.I = intable['I'] * u.W
        self.H = intable['H'] * u.mag
        log.info(str(len(self.H))+" data loaded from "+datfile)
        self.make_et()
        return
    
    def to_kml(self,kmlfile,et_start=None,et_stop=None): 
        """Save all data into file.
        
        Parameters
        ----------
        kmlfile : string
            Full name output KML file name.
        et_start : float, optional
            SPICE Time after which the Fakeor data are to be saved.
            Default is None, meaning that all Fakeor times will be saved.
        et_stop : float, optional
            Time before which the Fakeor data are to be saved.
            Default is None, meaning that all Fakeor times will be saved.
        
        Returns
        -------
        None.
        
        """
        # convert into kml data and save into output file
        kml = simplekml.Kml()
        coords = []
        # set et_start and et_stop
        if not et_start:
            et_start = np.min(self.et)
        if not et_stop:
            et_stop = np.max(self.et)
        #log.debug('self.et: '+str(self.et))
        
        # loop over time
        for (t,et) in zip(self.time,self.et):
            if ((et>=et_start) and (et<et_stop)):
                # retrieve fakeor location in  ITRF93
                #log.debug('self.spice_name: '+self.spice_name)
                (eph,lt) = sp.spkezr(self.spice_name,et,'ITRF93','None','EARTH')
                #log.debug('self.spice_name: '+self.spice_name)
                eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
                coord = (eloc.lon.to('deg').value,eloc.lat.to('deg').value,eloc.height.to('m').value)
                coords.append(coord)
                pnt = kml.newpoint(name="",coords=[coord]) # ,extrude=1
                pnt.altitudemode = simplekml.AltitudeMode.absolute
                pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/paddle/ylw-blank-lv.png'
                pnt.timestamp.when = t # .isot
        #lin = kml.newlinestring(name="Path", description="Path",coords=coords)
        #lin.altitudemode = simplekml.AltitudeMode.absolute
        kml.save(kmlfile)
        log.info('KML file saved in '+kmlfile)
        return
