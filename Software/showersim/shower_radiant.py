"""Definition of a meteor shower radiant Object.

Authors:
Jeremie Vaubaillon (IMCCE, France)
The MALBEC project, 2020

"""
import numpy as np
import astropy.units as u
from astropy.time import Time as Time
from astropy.coordinates import SkyCoord, AltAz, get_constellation, cartesian_to_spherical
import spiceypy as sp
from missionplanner.utils import log

class Shower_Radiant (object):
    """Shower Radiant object.
    
    Parameters
    ----------
    coo : astropy.coordinates.SkyCoord object
        sky coordinates (ra,dec)
    ra : astropy.coordinates.Angle object
        right ascencion
    dec : astropy.coordinates.Angle object
        declination
    constellation : string
        Constellation where the radiant is located
    uvec : numpy array
        3D unit vector representing (ra,dec)
    
    """
    @property
    def ra(self):
        return self.__ra
    @ra.setter
    def ra(self,ra):
        self.__ra = ra
    
    @property
    def dec(self):
        return self.__dec
    @dec.setter
    def dec(self,dec):
        self.__dec = dec
    
    @property
    def dra(self):
        return self.__dra
    @dra.setter
    def dra(self,dra):
        self.__dra = dra
    
    @property
    def ddec(self):
        return self.__ddec
    @ddec.setter
    def ddec(self,ddec):
        self.__ddec = ddec
    
    @property
    def time_max(self):
        return self.__time_max
    @time_max.setter
    def time_max(self,time_max):
        self.__time_max = time_max
    
    @property
    def coo(self):
        return self.__coo
    @coo.setter
    def coo(self,coo):
        self.__coo = coo
    
    @property
    def altaz(self):
        return self.__altaz
    @altaz.setter
    def altaz(self,altaz):
        self.__altaz = altaz
    
    @property
    def drifted(self):
        return self.__drifted
    @drifted.setter
    def drifted(self,drifted):
        self.__drifted = drifted
    
    @property
    def constellation(self):
        return self.__constellation
    @constellation.setter
    def constellation(self,constellation):
        self.__constellation = constellation
    
    def __init__(self,ra=0.0*u.deg,dec=0.0*u.deg,time_max=Time('2000-01-01T00:00:00.000',scale='utc'),dra=0.0*u.deg/u.d,ddec=0.0*u.deg/u.d):
        """Initialize Shower_Radiant Object.
        
        Parameters
        ----------
        ra : astropy.coordinate.Angle object, optional.
            Right ascension of the radiant. Default is 0*u.deg.
        dec : astropy.coordinate.Angle object, optional.
            Declination of the radiant. Default is 0*u.deg.
        dra : astropy.coordinate.Angle object, optional.
            Right ascension drift. Default is 0*u.deg/u.d.
        ddec : astropy.coordinate.Angle object, optional.
            Declination drift. Default is 0*u.deg/u.d.
        time_max : astropy.time.Time Object, optional
            Time of shower maximum: this is useful to compute a drifted radiant. Default is J2000.
        
        """
        self.from_radec(ra,dec)
        self.dra = dra
        self.ddec = ddec
        if time_max:
            self.time_max = time_max
        # set drifted to original values
        self.drifted = SkyCoord(ra,dec,unit='deg')
        return
    
    def from_radec(self,ra,dec,unit='deg'):
        """Initialize radiant object from right ascencion and declination.
        
        Parameters
        ----------
        ra: float or array of float
            right ascension of the radiant.
        dec:float or array of float
            declination of the radiant.
        unit: str, optional
            unit of the provided ra and dec. Default is 'deg'.
        
        Returns
        -------
        None.
        
        """
        self.coo = SkyCoord(ra,dec,unit=unit)
        self.ra = self.__coo.ra
        self.dec = self.__coo.dec
        self.drifted = SkyCoord(self.ra.to('deg'),self.dec.to('deg'),unit='deg')
        self.constellation = get_constellation(self.__coo).encode('utf-8')
        self.uvec = sp.latrec(1.0,self.__ra.to('rad').value,
                                  self.__dec.to('rad').value)
        return
    
    def from_unit_vector(self,Xi,Eta,Zeta):
        """Define the radiant object from a unit vector.
        
        The radiant is defined from eqn. 29 of Ceplecha 1987
        
        Parameters
        ----------
        Xi: float or array of floats
            first coordinate of the 3D unit vector
        Eta: float or array of floats
            second coordinate of the 3D unit vector
        Zeta: float or array of floats
            third coordinate of the 3D unit vector
        
        Returns
        -------
        None.
        
        """
        # normalized vector
        self.uvec = np.array([Xi,Eta,Zeta]) / sp.vnorm([Xi,Eta,Zeta])
        # transform to ra,dec
        (d,self.dec,self.ra) = cartesian_to_spherical(Xi,Eta,Zeta)
        # convert to astropy SkyCoord
        self.coo = SkyCoord(self.ra,self.dec)
        # set drifted coordinate to initial coordinates
        self.drifted = SkyCoord(self.ra.to('deg'),self.dec.to('deg'),unit='deg')
        # get constellation
        self.constellation = get_constellation(self.__coo).encode('utf-8')
        return
    
    def drift(self,time):
        """Apply radiant drift.
        
        Parameters
        ----------
        time : astropy.time.Time Object.
            Time for which radiant coordinates are to be computed.
        
        Returns
        -------
        drifted_radiant : astropy.coordinates.SkyCoord object.
            Coordinates of the radiant after applying radiant drift.
        
        """
        # compute time difference, in days
        dt = (self.time_max - time).to('d')
        # make SkyCoord Object
        drifted_ra = (self.__coo.ra.to('deg')+self.__dra.to('deg/d')*dt)%(360.0*u.deg)
        drifted_dec = self.__coo.dec.to('deg')+self.__ddec.to('deg/d')*dt
        test = False
        try:
            if(np.abs(drifted_dec.to('deg').value) > 90.0):
                test = True
        except:
            if (any(np.abs(drifted_dec.to('deg').value) > 90.0)):
                test = True
        if test:
            msg = '*** FATAL ERROR: drifted radiant dec > 90 deg for dt='+str(dt.to('d'))
            log.error(msg)
            raise ValueError(msg)
        self.drifted = SkyCoord(ra=self.__coo.ra.to('deg')+self.__dra.to('deg/d')*dt,
                               dec=self.__coo.dec.to('deg')+self.__ddec.to('deg/d')*dt)
        return self.drifted
    
    def get_invert(self):
        """Transform radiant into anti-radiant.
        
        This is useful when the computed radiant is found to lie under
        the horizon.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        anti_radiant : astropy.coordinates.SkyCoord object.
            Anti-radiant.
        
        """
        anti_radiant = SkyCoord(ra=((self.drifted.ra.to('deg').value+180.0)%360.0)*u.deg,
                                dec=-self.drifted.dec.to('deg'), frame='icrs')
        return anti_radiant
    
    def info(self):
        """Prints the radiant information.
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        None
        
        """
        log.info("Radiant = "+self.__coo.to_string('hmsdms'))
        log.info("ra = "+str(self.__coo.ra.to('deg')))
        log.info("dec = "+str(self.__coo.dec.to('deg')))
        log.info("constellation= "+str(self.__constellation))
        try:
            log.info("time = "+str(self.__time_max))
            log.info("az = "+str(self.__altaz.az.to('deg')))
            log.info("el = "+str(self.__altaz.alt.to('deg')))
        except AttributeError:
            msg = '*** WARNING: no AltAz defined yet'
            log.warning(msg)
            #raise NameError(msg)
        return
    
    def to_altaz(self,time,eloc):
        """Transform sky coordinates into azimuth-elevation.
        
        The self.altaz variable is set.
        
        Parameters
        ----------
        time : astropy.time.Time object.
            Time for which azimuth-elevation is required.
        eloc : astropy.coordinates.EarthLocation Object.
            Location on Earth where to compute azimuth-elevation.
        
        Returns
        -------
        None.
        
        """
        self.time = time
        # drift radiant at time time
        self.drift(time)
        # compute azimuth, elevation at time time and location eloc.
        self.altaz = self.drifted.transform_to(AltAz(obstime=time,location=eloc))
        return 
    
    
    def to_nparray_deg(self):
        """Convert to numpy array in [deg].
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        rad_as_nparray_deg : numpy 2-array.
            RA and DEC in [deg].
        
        """
        rad_as_nparray_deg = np.empty([2],dtype=np.float64)
        rad_as_nparray_deg = [self.coo.ra.to('deg').value,
                              self.coo.dec.to('deg').value]
        return rad_as_nparray_deg
    
    def to_nparray_rad(self):
        """Convert to numpy array in [rad].
        
        Parameters
        ----------
        None.
        
        Returns
        -------
        rad_as_nparray_rad : numpy 2-array.
            RA and DEC in [rad].
        
        """
        rad_as_nparray_rad = np.empty([2],dtype=np.float64)
        rad_as_nparray_rad = [self.coo.ra.to('rad').value,
                              self.coo.dec.to('rad').value]
        return rad_as_nparray_rad


