"""Instrument Object.

Author: J. Vaubaillon, IMCCE, 2020
The MALBEC project.


"""


import spiceypy as sp
import astropy.units as u
from astropy.coordinates import EarthLocation, AltAz, SkyCoord

from missionplanner import utils
from obsplanner.make_id import make_clock_id,make_topoframe_name

from missionplanner.utils import log


class Spacecraft(object):
    """Spacecraft object.
    
    Parameters
    ----------
    name : string
        Spacecraft SPICE name.
    spice_id : int
        SPICE id.
    frame_name : string
        Spacecraft SPICE frame name.
    spk_kernel : string, optional
        SPICE kernel file.
    cov : SPICE window.
        SPICE time coverage window.
    start_et : float
        SPICE coverage start time.
    stop_et : float
        SPICE coveraege stop time.
    start_time : astropy.time.Time Object.
        Coverage start time.
    stop_time : astropy.time.Time Object.
        Coveraege stop time.
    ck_kernel
        Spacecraft orientation kernel file.
    orientation_frame_name : string
        Orientation frame name.
    
    
    """
        
    def __init__(self,name,spice_id=None):
        """Initializes a Spacecraft object.
        
        Parameters
        ----------
        name : str, optional
            name of the Fakeor objects. Default is 'FakeObs'.
        spice_id : int, optional
            SPICE id.
        
        Returns
        -------
        None.
        
        """
        # set name
        self.name = name
        # set SPICE id if needed
        self.spice_id = spice_id
        #set SPICE clock id
        self.clock_id = make_clock_id(self.spice_id,from_instr=True)
        #s set topo-like frame name
        self.topoframe_name = make_topoframe_name(self.name)
        return
    
    def add_instrument(self,instrument):
        """Add an Instrument Object..
        
        Parameters
        ----------
        instrument : showersim.instrument Object
            Instrument.
        
        Returns
        ------
        None.
        """
        self.instrument = instrument
        return    
    
    def load(self,kernel,frame_name,frame_kernel=None,camera=None):
        """Initializes a FakeObs object.
        
        Parameters
        ----------
        name : str, optional
            name of the Fakeor objects. Default is 'FakeObs'.
        
        Returns
        -------
        None.
        
        """
        self.kernel = kernel
        utils.check_file(self.kernel, log)
        log.info('Loading Instrument kernel: '+self.kernel)
        sp.furnsh(self.kernel)
        self.id = sp.spkobj(self.kernel)[0]
        self.cov = sp.spkcov(self.kernel,self.id)
        self.start,self.stop = sp.wnfetd(self.cov,0)
        log.info('id'+str(self.id))
        log.info('coverage: start: '+sp.et2utc(self.start,'ISOC',2,50))
        log.info('coverage: stop: '+sp.et2utc(self.stop,'ISOC',2,50))
        self.frame_name = frame_name
        # set and load frame kernel, if needed
        if frame_kernel:
            self.frame_kernel = frame_kernel
            log.info('Loading Instrument frame kernel: '+self.frame_kernel)
            utils.check_file(self.frame_kernel, log)
            sp.furnsh(self.frame_kernel)
        # set Camera object, if needed
        if camera:
            self.camera = camera
        return
        
    def get_position(self,time,frame='ITRF93',silent=False):
        """Get spacecraft position at time t.
        
        Parameters
        ----------
        time : astropy.time.Time object
            Time for which the ephemeris is needed.
        silent : boolean, optional
            If True, no log is performed. Default is False
        
        Returns
        -------
        eloc : astropy.coordinates.EarthLocation
            Location of the spacecraft at time t.
        
        """
        et = sp.utc2et(time.isot)
        if not silent:
            log.debug('Retrieving '+self.name+' position for time: '+time.isot+' et: '+str(et))
        # check that the spacecraft is known to SPICE
        try:
            sp.bodn2c(self.name)
        except:
            msg = '*** FATAL ERROR: SPICE does not seem to know spacecraft: '+self.name
            log.error(msg)
            raise ValueError(msg)
        # get positons of spacecraft with SPICE tools
        (eph, lt) = sp.spkezr(self.name,et,frame,'NONE','EARTH')
        # convert x,y,z into an astropy.coordinates.EarthLocation (global) object:
        eloc = EarthLocation.from_geocentric(eph[0],eph[1],eph[2],unit=u.km)
        if not silent:
            log.debug('Position of '+self.name+' at '+time.isot+' = '+str(eloc.to_geodetic()))
        return eloc
    
    def get_orientation(self,time,append_posradec=False):
        """Get orientation of instrument in spacecraft topo-like frame.
        
        Parameters
        ---------
        time : astropy.time.Time Object
            Time at which the orientation is needed.
        append_posradec : boolean
            If True, returns also the RA,DEC sky coordinates position
            of the nacelle pointing direction.
            Default is False.
        
        Returns
        -----
        eloc : astropy.coordinates.EarthLocation Object, optional.
            Location of the nacelle at time time.
            Is not returned if append_posradec=False.
        radec : astropy.coordinates.SkyCoord object, optional.
            Pointing direction of the nacelle in ICRS at J2000.
            Is not returned if append_posradec=False.
        altaz : astropy.coordinates.AltAz object
            Local orientation of the nacelle, in nacelle frame.
        
        """
        # get SPICE time
        et = sp.str2et(time.isot)
        # get transformation between instrument frame and topo-like frame
        pointing_mtx = sp.pxform(self.instrument.frame_name,self.topoframe_name,et)
        # get instrument boresight vector
        (fov_shape,fov_frame,brst_vec,n_crn,crn_vec) = sp.getfov(self.instrument.spice_id,4)
        # get instrument orientation vector
        pointing_vec = sp.mxv(pointing_mtx,brst_vec)
        # translates orientation vector into angle
        rlonlat = sp.reclat(pointing_vec)
        # compute geographic azimuth/elevation
        az = (-rlonlat[1]*sp.dpr() %360) * u.deg
        el =   rlonlat[2]*sp.dpr() * u.deg
        # make astropy.coordinates.AltAz object
        altaz = AltAz(az=az,alt=el,obstime=time)
        if append_posradec:
            # retrieve spacecraft position
            eloc = self.get_position(self.name,time)
            # create AltAz frame
            altaz_frame = AltAz(obstime=time,location=eloc)
            # compute RA,DEC
            radec = SkyCoord(altaz.az,altaz.alt,frame=altaz_frame).transform_to('icrs')
            # FOR TEST PURPOSE: REMOVE
            pointing_mtx = sp.pxform(self.instrument.frame_name,'J2000',et)
            # compute orientation of boresight vecor
            pointing_vec = sp.mxv(pointing_mtx,brst_vec)
            rradec = sp.recrad(pointing_vec)
            radec_spice = SkyCoord(rradec[1]*sp.dpr(),rradec[2]*sp.dpr(),unit=u.deg,frame='icrs')
            log.debug('radec_spice : '+radec_spice.to_string('hmsdms'))
            log.debug('sep radec radec_spice: '+str(radec.separation(radec_spice)))
            output_data = (eloc,radec_spice,altaz)
        else:
            output_data = (altaz)
        return output_data
        
    
    def get_fov_center_maxrad(self,et,verbose=False):
        """Compute center of FOV and FOV max physical distance of all corners.
        
        Parameters
        ----------
        et : float
            Time in SPICE format [sec after J2000].
        verbose : boolean
            If True, log is performed. Default is False.
        
        Returns
        -------
        fov_center_eloc : astropy.coordinates.EarthLocation Object.
            Location of the middle point of center of the FOV of both cameras.
        max_phy_dist : astropy.unitsQuantity object
            Maximum physical distance between center of FOV and all FOV corners.
        
        """
        # get FOV data at time et
        fov_data = self.get_fov_data(et)
        # get middle point of fov in atmosphere (NOT boresight: at low elevation angle boresight is not at center of area)
        crn11_eloc = EarthLocation.from_geodetic(lon=fov_data[0]['crn1_lon'],lat=fov_data[0]['crn1_lat'],height=fov_data[0]['crn1_alt'])
        crn13_eloc = EarthLocation.from_geodetic(lon=fov_data[0]['crn1_lon'],lat=fov_data[0]['crn1_lat'],height=fov_data[0]['crn1_alt'])
        crn21_eloc = EarthLocation.from_geodetic(lon=fov_data[1]['crn1_lon'],lat=fov_data[1]['crn1_lat'],height=fov_data[1]['crn1_alt'])
        crn23_eloc = EarthLocation.from_geodetic(lon=fov_data[1]['crn1_lon'],lat=fov_data[1]['crn1_lat'],height=fov_data[1]['crn1_alt'])
        mid1_eloc = utils.elocs2mid(crn11_eloc,crn13_eloc)
        mid2_eloc = utils.elocs2mid(crn21_eloc,crn23_eloc)
        fov_center_eloc = utils.elocs2mid(mid1_eloc,mid2_eloc)
        
        # compute distance between center of fov and each of the 4 corners of the 2 camera FOV
        max_phyD = -1.0*u.m
        for drow in fov_data:
            for cid in range(4):
                eloc = EarthLocation.from_geodetic(drow['crn'+str(cid+1)+'_lon'],
                                                   drow['crn'+str(cid+1)+'_lat'],
                                            height=drow['crn'+str(cid+1)+'_alt'])
                phyD,az = utils.distaz(fov_center_eloc,eloc)
                if phyD > max_phyD:
                    max_phyD = phyD
        
        if verbose:
            log.debug('phyD: '+str(phyD.to('km')))
            log.debug('az: '+str(az))
            log.debug('fov_center_eloc: '+str(fov_center_eloc.to_geodetic()))
            log.debug('max_phyD: '+str(max_phyD.to('km')))
        
        return (fov_center_eloc,max_phyD)
