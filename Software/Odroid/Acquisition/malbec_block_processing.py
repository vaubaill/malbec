# -*- coding: utf8 -*-
################################################################################
# Malbec Block Processing Utility
#-------------------------------------------------------------------------------
# This program is intended to be used sperately from the Malbec core software.
# It processes the image blocks obtained during a flight.
# Before launching the block processing program, make sure to address the
# following points:
# - set the root path (-root_dir option)
# - set the sub-folder paths (block_folder with -block_dir, image_folder with -image_dir and log_folder with -log_dir options)
# - Set the output format (-output_format option)
# - Define if you want to use GPS and BME data (-no_gps and -no_bme options)
# - Define the size of the sensor (-img_h and -img_w options)
# - Define verbosity (-v option)
#-------------------------------------------------------------------------------
# Version: 3g
# Last modification: 2020-02-28
################################################################################
"""
Copyright (c) ScotopicLabs / IMCCE 2019
This file is part of the Malbec project.
"""
import os
import sys
import argparse
from datetime import datetime, date, time, timedelta
import numpy as np
import pandas as pd
from astropy.io import fits
from PIL import Image

#-------------------------------------------------------------------------------
# Argument parsing
parser = argparse.ArgumentParser(description='Convert blocks into fits images.')
parser.add_argument('-root_dir',default='/mnt/data/',help='root directory where blocks, log and fits sub-directories are located. Default is: /mnt/data/')
parser.add_argument('-block_dir',default='BLOCKS/',help='sub-directory name where blocks are located. Default is: BLOCKS/')
parser.add_argument('-log_dir',default='LOG/',help='sub-directory name where logs are located. Default is: LOG/')
parser.add_argument('-image_dir',default='IMAGES/',help='sub-directory name where images will be located. Default is: FITS/')
parser.add_argument('-output_format',default='fits',help='Out images Format. Default is: fits.', choices=['fits','png','pgm'])
parser.add_argument('-no_gps',action="store_true",help='if set, GPS data are ignored. For test purpose only.')
parser.add_argument('-no_bme',action="store_true",help='if set, BME data are ignored. For test purpose only.')
parser.add_argument('-camera',default='Unknown',help='Camera name. Default is: Unknown')
parser.add_argument('-img_h',type=int,default=1088,help='Images height in pixels. Default is: 1088 (Basler acA2000-165umNIR: 1088, Basler acA1920-155um: 1200, DMK 33UX174: 1200, DMK 33UX273: 1080)')
parser.add_argument('-img_w',type=int,default=2048,help='Images width in pixels.  Default is: 2048 (Basler acA2000-165umNIR: 2048, Basler acA1920-155um: 1920, DMK 33UX174: 1920, DMK 33UX273: 1440)')
parser.add_argument('-mkdir_imgdir', help="Make output image folder if needed. Default is False.",action="store_true")
parser.add_argument('-blkpd',default=None, help="number of blocks per created subdirectory. Default is None.")
parser.add_argument('-continuous', help="Continuous numbering of files. Default is False.",action="store_true")
parser.add_argument('-v','--verbose', help="Increase output verbosity",action="store_true")

# get args
args = parser.parse_args()

# store args in local variables
root_dir = args.root_dir
block_dir = args.block_dir
log_dir = args.log_dir
image_dir = args.image_dir
output_format = args.output_format
use_gps = False if args.no_gps else True
use_bme = False if args.no_bme else True
h = args.img_h
w = args.img_w
blkpd = args.blkpd
cont = args.continuous

# Set variables if camera parameter is set
if not args.camera=='Unknown':
    try:
        from reduction.camera import Camera
        [brand,name] = args.camera.split('_')
        cam = Camera(brand=brand,name=name)
        if cam.known:
            h = cam.sensor_pixnum[1]
            w = cam.sensor_pixnum[0]
    except:
        msg = 'Camera is unknown: default parameters set'
        print(msg)
        #log.warning('Camera is unknown: default parameters set')

# solve the folder ending slash syndrome
if not root_dir[-1]=='/': root_dir+='/'
if not block_dir[-1]=='/': block_dir+='/'
if not log_dir[-1]=='/': log_dir+='/'
if not image_dir[-1]=='/': image_dir+='/'

# Parameters to set before execution
# Folder where blocks can be found
block_folder = root_dir + block_dir

# Folder where output images must be put
image_folder = root_dir + image_dir

# Folder where logs can be found
log_folder = root_dir + log_dir

# print options if verbose
if args.verbose:
    print('==============')
    print(__file__)
    print('root_dir: '+root_dir)
    print('block_dir: '+block_dir)
    print('log_dir: '+log_dir)
    print('image_dir: '+image_dir)
    print('block_folder: '+block_folder)
    print('image_folder: '+image_folder)
    print('log_folder: '+log_folder)
    print('output_format: '+output_format)
    print('no_gps: '+str(args.no_gps))
    print('use_gps: '+str(use_gps))
    print('no_bme: '+str(args.no_bme))
    print('use_bme: '+str(use_bme))
    print('h: '+str(h))
    print('w: '+str(w))
    print('blkpd: '+str(blkpd))
    print('cont: '+str(cont))
    print('==============')

if args.mkdir_imgdir and (not os.path.isdir(image_folder)):
    try:
        os.mkdir(image_folder)
    except:
        raise IOError('*** Impossible to make image folder '+image_folder)

# test if data folders exist
for subdir in [root_dir,block_folder,log_folder,image_folder]:
    if not os.path.isdir(subdir):
        msg = '*** FATAL ERROR: folder '+subdir+' does not exist'
        raise IOError(msg)


#-------------------------------------------------------------------------------
# Useful global variables

# Dictionary to store exposure times
dic_exposures  = {}

# List of GPS date recording
list_gps_dates = []

# List of GPS location recording
list_gps_locs = []

# List of BME date recording
list_bme_dates = []

# List of GPS location recording
list_bme_tpus = []

# List of block index to process ([] means all blocks)
list_block_index = []

# Header of the stamp files
stamp_columns = ['block_num', 'num', 'camera_t', 'clock_t']

# Image counter
img_count = 0

# Millisecond clock for the first image of all blocks
clk_start = -1

#-------------------------------------------------------------------------------
def process_block(block_file, num_block,out_dir='IMAGE/',cont=False,reset=False):
    """Extract all image files from an image block,
    
    Parameters
    ----------
    block_file : string
        Block file name.
    num_block : int
        Block number.
    out_dir : string, optional
        Image output directory.
        Default is 'IMAGE'
    cont : boolean, optional
        If True continuous numbering of file is performed. Default is False.
    reset : boolean, optional
        If True numbering is reset to zero. To be used with the cont option.
        Default is False.
    
    """
    
    
    """Extract all FITS files from an image block"""
    global img_count
    global clk_start
    
    # reset img_count
    if reset:
        img_count = 0
    raw_path = block_folder + '/' + block_file
    stamp_file = os.path.splitext(os.path.basename(block_file))[0] + '.stamps'
    stamp_path = block_folder + '/' + stamp_file
    
    # Read the full block in memory
    block_img = np.fromfile(raw_path, dtype=np.uint8)
    
    # Get the number of images in the block
    nb_images_block = int(block_img.shape[0] / payload)
    
    # Reshape the block taking into account width and height
    try:
        block_img = np.reshape(block_img, (nb_images_block, h, w))
    except:
        print('Error: block %s does not have the right shape'%block_file)
        return
    
    # Read the timestamp file
    block_stamps = pd.read_csv(stamp_path, sep='\t', header=None, names=stamp_columns)
    
    # Allocate the FITS HDU object
    if output_format == 'fits':
        hdu = fits.PrimaryHDU()
    
    # Iterate through all the images in the block
    print('Processing image block %s (%d images)'%(block_file, nb_images_block))
    for i in range(nb_images_block):
        # Increment image counter
        img_count += 1
        # Get the image raw data
        data = block_img[i]
        # Get the time in ms from the first image
        if clk_start < 0:
            clk_start = block_stamps['clock_t'].iloc[i]
            clk_current = 0
        else:
            clk_current = block_stamps['clock_t'].iloc[i] - clk_start
        # Build image name
        if cont:
            image_root = 'img_%06d'%img_count
        else:
            # Build image root name under the form img_<nnnnnn>_<ms> where
            # <nnnnnn> is a sequential number and <ms> is the time in ms since the
            # first image on 8 digits
            image_root = 'img_%06d_%08d'%(img_count, clk_current)
        # Get the date of the image
        dt_current = dt_start + timedelta(milliseconds=np.float64(clk_current))
        # Produce the output file
        if output_format == 'png':
            img = Image.frombytes('L', (w, h), data)
            img.save(out_dir + '/' + image_root + '.png')
        elif output_format == 'pgm':
            img = Image.frombytes('L', (w, h), data)
            print('save image in: '+out_dir + '/' + image_root + '.pgm')
            img.save(out_dir + '/' + image_root + '.pgm')
        elif output_format == 'fits':
            hdu.data = data
            hdu.header['DATE-OBS'] = dt_current.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            hdu.header['EXPTIME'] = dic_exposures[num_block]
            hdu.header['GAIN'] = gain
            if use_gps:
                # Find the GPS location to be used
                date_in_list = min(list_gps_dates, key=lambda d: abs(d - dt_current))
                location = list_gps_locs[list_gps_dates.index(date_in_list)]
                hdu.header['GEOLAT'] = location[0]
                hdu.header['GEOLON'] = location[1]
                hdu.header['GEOALT'] = location[2]
            if use_bme:
                # Find the BME data to be used
                date_in_list = min(list_bme_dates, key=lambda d: abs(d - dt_current))
                tpu = list_bme_tpus[list_bme_dates.index(date_in_list)]
                hdu.header['AMBTEMP'] = tpu[0]
                hdu.header['ATMPRESS'] = tpu[1]
                hdu.header['RELHUM'] = tpu[2]
            hdu.writeto(image_folder + '/' + image_root + '.fits', overwrite=True)

# Get the datetime object of the starting image as well as the gain
#-------------------------------------------------------------------------------
if not os.path.exists(log_folder + '/malbec.log'):
    print('Error: log file malbec.log cannot be found')
    sys.exit(1)
f_log = open(log_folder + '/malbec.log', 'r')
lines = f_log.readlines()
f_log.close()
num_line = 0
stamp_start = ''
gain = -1
for line in lines:
    num_line += 1
    line = line.strip()
    if 'Gain' in line:
        gain = int(line.split('=', 1)[1].strip().split(' ', 1)[0])
    if '******** Block start: #0000 at ' in line:
        stamp_start = line.split('[', 1)[1].split(']', 1)[0]
        if gain != -1:
            break
if stamp_start == '':
    print('Error: start stamp not found in malbec.log')
    sys.exit(1)
if gain == -1:
    print('Error: gain not found in malbec.log')
    sys.exit(1)
print('Start stamp: %s'%stamp_start)
dt_start = datetime.strptime(stamp_start, '%Y-%m-%d %H:%M:%S.%f')

# Image payload
payload = w * h

# We read the log file to memorize the exposure times for all the blocks
#-------------------------------------------------------------------------------
if not os.path.exists(log_folder + '/malbec.log'):
    print('Error: log file malbec.log cannot be found')
    sys.exit(1)
f_log = open(log_folder + '/malbec.log', 'r')
lines = f_log.readlines()
f_log.close()
num_line = 0
cur_block = 0
for line in lines:
    num_line += 1
    line = line.strip()
    if 'Using starting exposure: ' in line:
        cur_exp = float(line.split('Using starting exposure: ', 1)[1].split(' s', 1)[0])
    elif '******** Block start: #' in line:
        cur_block = int(line.split('******** Block start: #', 1)[1].split(' at ')[0])
        dic_exposures[cur_block] = cur_exp
    elif 'Exposure time to be used: ' in line:
        cur_exp = float(line.split('Exposure time to be used: ', 1)[1].split('s', 1)[0])

# We read the GPS log file to memorize the location and elevation at all times
#-------------------------------------------------------------------------------
if use_gps:
    if not os.path.exists(log_folder + '/gps_acq.log'):
        print('Error: log file gps_acq.log cannot be found')
        sys.exit(1)
    f_log = open(log_folder + '/gps_acq.log', 'r')
    lines = f_log.readlines()
    f_log.close()
    num_line = 0
    for line in lines:
        num_line += 1
        line = line.strip()
        if 'GPS location: ' in line:
            data = line.split(' ')
            str_date = data[0] + ' ' + data[1]
            list_gps_dates.append(datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S'))
            str_latitude = data[4]
            str_longitude = data[5]
            str_altitude = data[6]
            list_gps_locs.append([float(str_latitude), float(str_longitude), int(str_altitude)])

# We read the BME log file to memorize the location and elevation at all times
#-------------------------------------------------------------------------------
if use_bme:
    if not os.path.exists(log_folder + '/bme_acq.log'):
        print('Error: log file bme_acq.log cannot be found')
        sys.exit(1)
    f_log = open(log_folder + '/bme_acq.log', 'r')
    lines = f_log.readlines()
    f_log.close()
    num_line = 0
    for line in lines:
        num_line += 1
        line = line.strip()
        if 'BME 280 reading' in line:
            data = line.split(' ')
            str_date = data[0] + ' ' + data[1]
            list_bme_dates.append(datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S'))
            str_temperature = data[5]
            str_pressure = data[6]
            str_humidity = data[7]
            list_bme_tpus.append([float(str_temperature), float(str_pressure), float(str_humidity)])

# We iterate through all the blocks. We do it sequentially to guarantee the
# correct order
#-------------------------------------------------------------------------------
icnt=0
idir=0
reset=False
for i_file in range(500):
    block_file = block_folder + '/block%04d.raw'%i_file
    icnt = icnt+1
    if (blkpd):
        if icnt==int(blkpd):
            icnt=0
            idir = idir+1
            reset=True
    out_dir = image_folder[:-1] + '%04d'%idir
    if os.path.exists(block_file):
        if list_block_index == [] or i_file in list_block_index:
            print('Process '+'block%04d.raw'%i_file,i_file,' blkpd: ',blkpd,  ' icnt: ',icnt,' outdir=',out_dir)
            if not os.path.isdir(out_dir):
                os.mkdir(out_dir)
            process_block('block%04d.raw'%i_file, i_file,out_dir=out_dir,cont=cont,reset=reset)
            reset=False
print('Conversion process done')
