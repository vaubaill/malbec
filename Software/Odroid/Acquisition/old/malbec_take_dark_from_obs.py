import time
import os
import datetime
import numpy as np
from scipy import ndimage, misc
#from gi.repository import Aravis
from astropy.io import fits
import warnings
from astropy.stats import sigma_clipped_stats
import malbec_tools

# files settings
dark_dir		= '/home/pi/images/DARK/'	# directory where data are saved
master_dark_file	= 'master_dark'
total_nb_frame		= 10				# total number of frames to make master dark
gain			= 800				# camera gain


def acquisition_dark(nb_exposures=3,dark_dir='dark/',dark_file_root='dark_',gain=gain):
    
    # set exposure time to minimum (given by camera manufacturer)
    exposure   = 0.000004			# [s] 
    
    # Camera connect
    try:
        camera = Aravis.Camera.new()
    except:
        print("Error: camera was not found")
        return

    # get sensor size
    [sensor_width, sensor_height] = camera.get_sensor_size()

    # Set ROI in pixels
    camera.set_region(0, 0, sensor_width, sensor_height)

    # Set Frame Rate and pixel format
    camera.set_frame_rate(frame_rate)
    camera.set_pixel_format (Aravis.PIXEL_FORMAT_MONO_8)
    camera.set_gain(gain)
    # Time exposure in micro-seconds
    camera.set_exposure_time(exposure * 1000000.0)

    # buffer Allocate
    payload = camera.get_payload()

    # initialize nb of exposure failures
    nb_failures = 0

    for i in range(nb_exposures):
        # Construction du nom de fichier
        if not os.path.isdir(image_dir + '/'):
                os.mkdir(image_dir + '/')
        file_name_raw = image_dir + '/' + dark_file_root + str(i) +'.raw'
        file_name_fit = image_dir + '/' + dark_file_root + str(i) +'.fit'
        # Start acquisition
        try:
            stream = camera.create_stream(None, None)
            stream.push_buffer (Aravis.Buffer.new_allocate(payload))
            camera.start_acquisition()
        except:
            print("Error during buffer allocation")
            return        
        # Wait during exposure
        time.sleep(exposure)
        # get image
        print('Acqusition of %s'%file_name_raw)
        buffer = stream.pop_buffer()
        if buffer:
          frame = buffer.get_data()
          stream.push_buffer(buffer)
          # save raw image
          with open(file_name_raw, 'wb') as f:
                f.write(frame)
                f.close()

          # Convert image into several formats
          #--------------------------------------
          raw_image_data		= np.fromfile(file_name_raw, np.uint8)
          raw_image_data.shape		= (sensor_height, sensor_width)
            
          # Convert into FITS format
          #---------------------------------------
          hdu = fits.PrimaryHDU(raw_image_data)
          # Header Modification
          hdu.header['DATE-OBS']	= (cur_utc.strftime("%Y-%m-%d") + 'T' + cur_utc.strftime("%H:%M:%S"),'YYY-MM-DDThh:mm:ss observation start, UT')
          hdu.header['TELESCOP']	= ('MALBEC', 'Instrument')
          hdu.header['CAMERA']		= ('Basler ac640-100gm','Camera')
          hdu.header['XPIXELSZ']	= (5.6,'Pixel size in X dimension in um')
          hdu.header['YPIXELSZ']	= (5.6,'Pixel size in X dimension in um')
          hdu.header['EXPTIME']		= (exposure,'Exposure time in s')
          hdu.header['OBSERVER']	= ('Vaubaillon, Caillou, CNES Balloon STC','Name of observers')
          hdu.header['IMAGETYP']	= ('Dark Frame','Type of image']
	  hdu.header['COMMENT']		= ('MALBEC dark image')
          # Write FITS file
          hdu.writeto(file_name_fit, output_verify='ignore', overwrite=True)
          print('image saved in %s'%file_name_fit)
	    
        else:		# end of if buffer
            nb_failures += 1
	# stop acquisition
        camera.stop_acquisition()
    print('Acquisition done')
    print('%d images requested'%nb_exposures)
    print('%d acquisitions not realized'%nb_failures)



# camera settings
frame_rate	= 25.0		# default frame rate per second

# take dark frames
#acquisition_dark(total_nb_frame,dark_file_root=dark_file_root,gain=gain)

# now make master dark 
#malbec_tools.malbec_make_master_dark(dark_dir='/home/pi/images/dark/',dark_file_root=dark_file_root,master_file_name=master_dark_file)
malbec_tools.malbec_make_master_dark(dark_dir='/Volumes/BackUpMac/MALBEC/Basler/DARK/',master_file_name=master_dark_file)

