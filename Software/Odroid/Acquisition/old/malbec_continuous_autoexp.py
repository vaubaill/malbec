# -*- coding: utf8 -*-
#################################################################################
#                 Programme de test de la caméra Malbec
#--------------------------------------------------------------------------------
# Version 0.0.1
# Dernière modification: 2017-08-10
# Auteurs : Philippe Deverchere, Jérémie Vaubaillon
#################################################################################
import time
import os
import datetime
import numpy as np
from scipy import ndimage, misc
from gi.repository import Aravis
from astropy.io import fits
import warnings
from astropy.stats import sigma_clipped_stats


# warnings.simplefilter('ignore', Warning)

frame_rate = 3.75		# frame rate per second
gain       = 200		# camera gain
exposure   = 0.04		# starting exposure time in seconds: 1/25=video rate
image_dir  = '/home/pi/images/'	# directory where data are saved

# To change in future: there must be a way to get those info automatically thanks to Aravis
nbbit		= 8.0		# number of bits of camera dynamic range
expo_upp	= 0.1		# exposure time upper limit, in seconds, given flight conditions
expo_low	= 0.000004	# exposure time upper limit, in seconds, given flight conditions

# estimate the total number of frames to record based on fpm and duration of flight
total_duration	= 3.0					# total duration of the flight [hours]
total_nb_frame	= frame_rate * 3600.0 * total_duration	# total number of frame to acquire

def acquisition_loop(nb_exposures=1):
    # Ouverture de la connexion vers la caméra
    try:
        camera = Aravis.Camera.new()
    except:
        print("Error: camera was not found")
        return

    # Acquisition de la taille du capteur
    [sensor_width, sensor_height] = camera.get_sensor_size()

    # Définition de la région à utiliser dans l'acquisition
    camera.set_region(0, 0, sensor_width, sensor_height)

    # Définition du Frame Rate et du format des pixels
    camera.set_frame_rate(frame_rate)
    camera.set_pixel_format (Aravis.PIXEL_FORMAT_MONO_8)
    camera.set_gain(gain)
    
    # Définition de la durée d'exposition en micro-secondes
    exposure   = 0.0000040		# starting exposure time in seconds: 1/25=video rate
    camera.set_exposure_time(exposure * 1000000.0)

    # Allocation du buffer pour recevoir l'image
    payload = camera.get_payload()

    # Boucle sur les poses à réaliser
    nb_failures = 0
    image_subdir = ''
    Lkeepgoing=True

    #for i in range(nb_exposures):
    while(Lkeepgoing):
        # Définition de la durée d'exposition en micro-secondes
        camera.set_exposure_time(exposure * 1000000.0)
        # Construction du nom de fichier
        cur_utc = datetime.datetime.utcnow()
        cur_subdir = cur_utc.strftime("%Y%m%dT%H0000_UT") # subdir of type: YYYYMMDDTHH0000_UT. ex: '20170808T090000_UT'
        if cur_subdir != image_subdir:
            image_subdir = cur_subdir
            if not os.path.isdir(image_dir + '/' + image_subdir):
                os.mkdir(image_dir + '/' + image_subdir)
        file_name_raw = image_dir + image_subdir + '/' + cur_utc.strftime("%Y%m%dT%H%M%S_%f")[:-3]+'_UT.raw'
        file_name_png = image_dir + image_subdir + '/' + cur_utc.strftime("%Y%m%dT%H%M%S_%f")[:-3]+'_UT.png'
        file_name_fit = image_dir + image_subdir + '/' + cur_utc.strftime("%Y%m%dT%H%M%S_%f")[:-3]+'_UT.fit'
        # Démarrage de l'acquisition
        try:
            stream = camera.create_stream(None, None)
            stream.push_buffer (Aravis.Buffer.new_allocate(payload))
            camera.start_acquisition()
        except:
            print("Error during buffer allocation")
            return        
        # On attend pour la duree de la pose
        time.sleep(exposure)
        # Obtention de l'image
        print('Acqusition de %s'%file_name_raw)
        buffer = stream.pop_buffer()
        if buffer:
          frame = buffer.get_data()
          stream.push_buffer(buffer)
          # save the raw image
          with open(file_name_raw, 'wb') as f:
                f.write(frame)
                f.close()

          # Production d'un fichier au format PNG
          #--------------------------------------
          raw_image_data = np.fromfile(file_name_raw, np.uint8)
          raw_image_data.shape = (sensor_height, sensor_width)
          misc.imsave(file_name_png, raw_image_data)
          print('image saved in %s'%file_name_png)
            
          # Production d'un fichier au format FITS
          #---------------------------------------
          hdu = fits.PrimaryHDU(raw_image_data)
          #hdu = fits.PrimaryHDU(frame)
          # Header Modification
          hdu.header['DATE-OBS']	= (cur_utc.strftime("%Y-%m-%d") + 'T' + cur_utc.strftime("%H:%M:%S"),'YYY-MM-DDThh:mm:ss observation start, UT')
          hdu.header['TELESCOP']	= ('MALBEC', 'Instrument')
          hdu.header['CAMERA']		= ('Basler ac640-100gm','Camera')
          hdu.header['FOCAL']		= (12.0,'Focal length in mm')
          hdu.header['DIAMETER']	= (27.0,'Lens diameter in mm')
          hdu.header['XPIXELSZ']	= (5.6,'Pixel size in X dimension in um')
          hdu.header['YPIXELSZ']	= (5.6,'Pixel size in X dimension in um')
          hdu.header['EXPTIME']		= (exposure,'Exposure time in s')
          hdu.header['Filter']		= ('700-1200','Filter bandpass in nm')
          hdu.header['OBSERVER']	= ('Vaubaillon, Caillou, CNES Balloon STC','Name of observers')
          hdu.header['COMMENT']		= ('MALBEC Observation Campaign')
          # Ecriture de l'image FITS
          hdu.writeto(file_name_fit, output_verify='ignore', overwrite=True)
          print('image saved in %s'%file_name_fit)
	    
	    
          # computing stats in order to automatically change the exposure time
          Lchange	= False			# change exposure time
          cam_min_adu	= 0.0			# minumum possible ADU level in image
          cam_max_adu	= int(2.0**nbbit)-1	# maximum possible ADU level in image
          medmargin	= 0.3			# median margin: 
	    					        # if median > cam_max_adu*(1-medmargin) => too bright
					            	# if median < cam_max_adu*   medmargin  => too dim
          # bin the image in order to fasten the stat calculation
          (xsz,ysz)	= np.shape(raw_image_data)
          binfct	= 4
          #subdata	= misc.imresize(raw_image_data,(xsz/binfct,ysz/binfct), interp='bilinear')
          subdata	= raw_image_data[::binfct,::binfct]
          # computes basic stats
          max_adu	= np.max(subdata)
          min_adu	= np.min(subdata)
          mean, median, std = sigma_clipped_stats(subdata)
          print(' min    =  %f '%min_adu)
          print(' max    =  %f'%max_adu)
          print(' median = %f'%median)
          print(' std    =  %f'%std)
          # now performs test to determine if change is needed and by how much
          if (median > cam_max_adu*(1.0-medmargin)): 		# if image is too bright...
               Lchange	= True
               if (std <1.2): 					# and close to saturation...
                change_fct	= 0.2				# then divide exposure time
                print('*** WARNING: SATURATION!!! ***')		# for log purpose
               else:
                change_fct	= 0.4				# else divide exposure time
          if (median < cam_max_adu*medmargin):		# if image is too dim...
               Lchange	= True
               if (std <1.2):					# and almost all black...
                change_fct	= 4.0				# then multiply exposure time
                print('*** WARNING: TOO DIM!!! ***')		# for log purpose
               else:
                change_fct	= 2.0				# else multiply exposure time
          # now changes exposure time if needed
          if (Lchange):
                print('*** Chainging exposure time by factor %f'%change_fct)
                exposure	= exposure * change_fct

          print(' exposure = %f'%exposure)
	    
          # checking boundary values of exposure time
          if (exposure > expo_upp):
               exposure	= expo_upp
               print('*** Warning: maximum exposure time reached: %f'%exposure)
          if (exposure < expo_low):
              exposure	= expo_low
              print('*** Warning: minumum exposure time reached: %f'%exposure)
	    	    
          
        else:		# end of if buffer
            nb_failures += 1
        camera.stop_acquisition()
    print('Acquisition terminée')
    print('%d images demandées'%nb_exposures)
    print('%d acquisitions non réalisées'%nb_failures)


acquisition_loop(total_nb_frame)
