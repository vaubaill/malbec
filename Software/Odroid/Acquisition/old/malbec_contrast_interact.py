# -*- coding: utf8 -*-
#################################################################################
#                 Programme de test de la caméra Malbec
#--------------------------------------------------------------------------------
# Version 0.0.1
# Dernière modification: 2017-07-18
# Auteurs : Philippe Deverchere, Jérémie Vaubaillon
#################################################################################
import time
import os
import datetime
import numpy as np
from scipy import ndimage, misc
from gi.repository import Aravis
from astropy.io import fits
import warnings
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
#import matplotlib.pyplot as plt # Note: not 100% certain it is insmkdir tall on current version of the RaspBerryPI: to double check!!!



# warnings.simplefilter('ignore', Warning)

frame_rate = 3.75		# frame rate per second
gain       = 200		# caera gain
exposure   = 0.00002		# exposure time in seconds
image_dir  = '/home/pi/images/contrast'	# directory where data are saved
wait_time  = 10.0		# waiting time between 2 exposures to perform the contrast [s]

def acquisition_loop(nb_exposures=1):
    # Ouverture de la connexion vers la caméra
    try:
        camera = Aravis.Camera.new()
    except:
        print("Error: camera was not found")
        return

    # Acquisition de la taille du capteur
    [sensor_width, sensor_height] = camera.get_sensor_size()

    # Définition de la région à utiliser dans l'acquisition
    camera.set_region(0, 0, sensor_width, sensor_height)

    # Définition du Frame Rate et du format des pixels
    camera.set_frame_rate(frame_rate)
    camera.set_pixel_format (Aravis.PIXEL_FORMAT_MONO_8)
    camera.set_gain(gain)

    # Définition de la durée d'exposition en micro-secondes
    camera.set_exposure_time(exposure * 1000000.0)

    # Allocation du buffer pour recevoir l'image
    payload = camera.get_payload()

    # create image directory if it does not exist already
    if not os.path.isdir(image_dir):	# create directory if it does not exist
           os.mkdir(image_dir)

    # Boucle sur les poses à réaliser
    nb_failures = 0
    image_subdir = ''
    for i in range(nb_exposures):
        # Construction du nom de fichier
        cur_utc = datetime.datetime.utcnow()
        cur_subdir = cur_utc.strftime("%Y%m%dT%H0000_UT") # subdir of type: YYYYMMDDTHH0000_UT. ex: '20170808T090000_UT'
        if cur_subdir != image_subdir:
            image_subdir = cur_subdir
            if not os.path.isdir(image_dir + '/' + image_subdir):	# create subdirectory if it does not exist
                os.mkdir(image_dir + '/' + image_subdir)
        file_name_raw = image_dir + '/' + image_subdir + '/' + cur_utc.strftime("%Y%m%dT%H%M%S_%f")[:-3]+'_UT.raw'
        file_name_png = image_dir + '/' + image_subdir + '/' + cur_utc.strftime("%Y%m%dT%H%M%S_%f")[:-3]+'_UT.png'
        file_name_fit = image_dir + '/' + image_subdir + '/' + cur_utc.strftime("%Y%m%dT%H%M%S_%f")[:-3]+'_UT.fit'
        # Démarrage de l'acquisition
        try:
            stream = camera.create_stream(None, None)
            stream.push_buffer (Aravis.Buffer.new_allocate(payload))
            camera.start_acquisition()
        except:
            print("Error during buffer allocation")
            return        
        # On attend pour la durée de la pose
        time.sleep(exposure)
        # Obtention de l'image
        print('Acquisition de %s'%file_name_raw)
        buffer = stream.pop_buffer()
        if buffer:
            frame = buffer.get_data()
            stream.push_buffer(buffer)
            with open(file_name_raw, 'wb') as f:
                f.write(frame)
                f.close()

            # Production d'un fichier au format PNG
            #--------------------------------------
            raw_image_data = np.fromfile(file_name_raw, np.uint8)
            raw_image_data.shape = (sensor_height, sensor_width)
            misc.imsave(file_name_png, raw_image_data)
            
            # Production d'un fichier au format FITS
            #---------------------------------------
            hdu = fits.PrimaryHDU(raw_image_data)
            # Ajout de l'heure UTC
            hdu.header['DATE-OBS']	= (cur_utc.strftime("%Y-%m-%d") + 'T' + cur_utc.strftime("%H:%M:%S"),'YYY-MM-DDThh:mm:ss observation start, UT')
            hdu.header['TELESCOP']	= ('MALBEC', 'Instrument')
            hdu.header['CAMERA']		= ('Basler ac640-100gm','Camera')
            hdu.header['FOCAL']		= (12.0,'Focal length in mm')
            hdu.header['DIAMETER']	= (27.0,'Lens diameter in mm')
            hdu.header['XPIXELSZ']	= (5.6,'Pixel size in X dimension in mm')
            hdu.header['YPIXELSZ']	= (5.6,'Pixel size in X dimension in mm')
            hdu.header['EXPTIME']	= (exposure,'Exposure time in s')
            hdu.header['Filter']	= ('700-1200','Filter bandpass in nm')
            hdu.header['OBSERVER']	= ('Vaubaillon, Caillou, CNES Balloon STC','Name of observers')
            hdu.header['COMMENT']	= ('MALBEC Observation Campaign')
            # Ecriture de l'image FITS
            hdu.writeto(file_name_fit, output_verify='ignore', overwrite=True)
            print('image saved in %s'%file_name_fit)

            # visualisation avec astropy
            #norm = ImageNormalize(stretch=SqrtStretch())
            #plt.imshow(raw_image_data, cmap='Greys', origin='lower', norm=norm)
            #plt.show(block=False)

            time.sleep(wait_time)


        else:
            nb_failures += 1
        camera.stop_acquisition()
    print('Acquisition terminée')
    print('%d images demandées'%nb_exposures)
    print('%d acquisitions non réalisées'%nb_failures)

acquisition_loop(10000)
