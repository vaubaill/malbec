import time
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
from photutils import datasets, DAOStarFinder, CircularAperture
from PIL import Image,ImageStat


# user part
binfct	= 3		# binning factor to compute the image stats and fasten the run


print 'loading image...'
#hdu = datasets.load_star_image()
#hdu.writeto('sample.fits', output_verify='ignore', overwrite=True)
#
#
# Solution with an image taken by the Basler camera
hdulist	= fits.open('2017-07-19T16-38-35-5460.fit') # ou sample.fits
hdu	= hdulist[0]
nbbitimg	= float(hdu.header['BITPIX'])		# number of bits of the original image
nbbit		= 8.0		# number of bits of camera dynamic range
cam_min_adu	= 0.0		    # minumum possible ADU level in image
cam_max_adu	= int(2.0**nbbit)-1     # maximum possible ADU level in image
medmargin	= 0.2		    # median margin: 
print 'low/upp threshold=',medmargin*cam_max_adu,(1.0-medmargin)*cam_max_adu

# resample image so that its size is similar to what the MALBEC Basler camera is providing
#xo	= 350	# pour sample.fits
#yo	= 400   # pour sample.fits
xo	= 0     # pour image taken with Basler camera
yo	= 0     # pour image taken with Basler camera
(xsz,ysz)	= (494,659)    # size of Basler camera image
print 'binned image size=',xsz/binfct,ysz/binfct
data_ori	= hdu.data[xo:xo+xsz, yo:yo+ysz]		# original data
data		= (data_ori*2.0 / 2.0**nbbitimg) * 2.0**nbbit	# to make it camera-similar
max_adu		= np.max(data)
min_adu		= np.min(data)
mean, median, std = sigma_clipped_stats(data, sigma=3.0)
print 'Original bit-reduced image: min=',min_adu,' max=',max_adu,' mean=',mean,' median=',median,' std=',std




#nexp=10
exp_fct		= 1.0
change_fct	= 1.0	# initialization
#bck_arr		= [0.0,100.0,110.0,90.0,200.0,210.0,190.0,240.0,245.0,235.0]
print '=== Starting looping over images'
#for i in xrange(nexp):
Lkeepgoing	= True
Lchange		= False		     # change exposure time
while(Lkeepgoing):
 # add background to data
 bck_fct	= 25500.0 / 2.0**nbbit  * 2.0**nbbitimg    # CHANGE THIS AT WILL
 bck		= np.ones((xsz,ysz)) * bck_fct				# float in range [0;2.0**nbbit]
 data		= ((data_ori*2.0 + bck ) * exp_fct / 2.0**nbbitimg) * 2.0**nbbit	# to make it camera-similar
 data[np.where(data > cam_max_adu )]	= cam_max_adu	# normalizing to max possible value
 print 'bck_fct=',bck_fct,' min/max(newdata)=',np.min(data),np.max(data)
 
 
  
 t0 = time.time()
 # rebin data
 #subdata	= misc.imresize(newdata,(xsz/binfct,ysz/binfct), interp='bilinear')
 subdata	= data[::binfct,::binfct]
 max_adu	= np.max(subdata)
 min_adu	= np.min(subdata)
 mean, median, std = sigma_clipped_stats(subdata, sigma=3.0)
 print 'subdata min=',min_adu,' max=',max_adu,' mean=',mean,' median=',median,' std=',std
 t1 = time.time()
 print 'time=',t1-t0
 
 Lchange     = False		     # change exposure time
 # now performs test to determine if change is needed and by how much
 if (median > cam_max_adu*(1.0-medmargin)):	     # if image is too bright...
  if (std <1.2):				     # and close to saturation...
   change_fct	     = 0.2			     # then divide exposure time by 10
   print '*** WARNING: SATURATION!!! ***'
  else:
   change_fct	     = 0.4			     # else divide exposure time by 2
   Lchange    = True
 if (median < cam_max_adu*medmargin):		     # if image is too dim...
  if (std <1.2):				     # and almost all black...
   change_fct	     = 4.0			     # then multiply exposure time by 10
   print '*** WARNING: TOO DIM!!! ***'
  else:
   change_fct	     = 2.0			     # else multiply exposure time by 2
  Lchange    = True	 
 # now changes exposure time if needed
 if Lchange:
  exp_fct	= exp_fct * change_fct
  print '*** Changing change_fct ***',change_fct, ' exp_fct=',exp_fct
 else:
  change_fct	= 1.0
  Lkeepgoing	= False
 
 #norm = ImageNormalize(stretch=SqrtStretch())
 #plt.imshow(subdata, cmap='Greys', origin='lower', norm=norm, aspect='auto')
 #plt.tight_layout()
 #plt.show(block=False)
