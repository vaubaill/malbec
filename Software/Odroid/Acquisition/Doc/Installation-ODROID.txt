                                  M A L B E C

Notes techniques
Dernire modification: 2019-03-21

Installation sur Odroid XU4
===========================
Utiliser Ubuntu 16.04

# **************************************** Installation initiale
Se connecter sous root (mot de passe odroid)
Changer le mot de passe root: malbec9++

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install vim

Changer le clavier depuis la console avec la commande:
dpkg-reconfigure keyboard-configuration

Ouvrir un ssh, et crer le nouvel utilisateur malbec avec les commandes suivantes:
sudo adduser malbec [entrer ensuite un mot de passe: malbec9]
sudo usermod -aG sudo malbec
su malbec

Tester sudo avec:
sudo ls -la /root

Ajouter les deux lignes suivantes  ~/.bashrc
vim ~/.bashrc
VISUAL="vim" ; export VISUAL
EDITOR="$VISUAL" ; export EDITOR

Taper ensuite les 2 commandes suivantes:
export VISUAL="vim"
export EDITOR="$VISUAL"

Lancer visudo:
sudo -E visudo
et ajouter la ligne suivante pour permettre de faire des sudo sans password:
malbec ALL=(ALL) NOPASSWD: ALL

Login automatique : 
sudo vi /usr/share/lightdm/lightdm.conf.d/60-lightdm-gtk-greeter.conf
modifier selon : 
[SeatDefaults]
greeter-session=lightdm-gtk-greeter
autologin-user=malbec

Reboot

# install git
sudo apt-get install git
# clone malbec git repository
cd
git clone https://gitlab.com/vaubaill/malbec.git


# **************************************** Environnement Python
#!/bin/bash
sudo apt-get -y install python3-numpy
sudo apt-get -y install python3-scipy
sudo apt-get -y install python3-astropy
sudo apt-get -y install python3-pil
sudo apt-get -y install python3-pip
pip3 install setuptools
sudo apt-get -y install git
sudo pip3 install pyserial
sudo pip3 install pynmea
sudo pip3 install spiceypy
sudo pip3 install pandas
sudo pip3 install smbus2
# for tha MaBallTrap project:
sudo pip3 install spiceypy


# *************************************** smbus
sudo apt-get install i2c-tools python-smbus
sudo -H pip3 install smbus2

# **************************************** Serveur FTP
#!/bin/bash
# Installation de vsftpd
#-----------------------
sudo apt-get -y install vsftpd
sudo sed -i -e 's/listen=NO/listen=YES/' /etc/vsftpd.conf
sudo sed -i -e 's/listen_ipv6=YES/listen_ipv6=NO/' /etc/vsftpd.conf
sudo sed -i -e 's/#write_enable=YES/write_enable=YES/' /etc/vsftpd.conf
sudo sed -i -e 's/#local_umask=022/local_umask=022/' /etc/vsftpd.conf
echo "# Change the default port to 26002" | sudo tee --append /etc/vsftpd.conf
echo "listen_port=26002" | sudo tee --append /etc/vsftpd.conf
echo "pasv_enable=YES" | sudo tee --append /etc/vsftpd.conf
echo "pasv_addr_resolve=YES" | sudo tee --append /etc/vsftpd.conf
echo "pasv_min_port=26050" | sudo tee --append /etc/vsftpd.conf
echo "pasv_max_port=26099" | sudo tee --append /etc/vsftpd.conf
sudo service vsftpd restart

# **************************************** Tiscamera
# Build dependencies
sudo apt-get -y install git g++ cmake pkg-config libudev-dev libudev1 libtinyxml-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libglib2.0-dev libgirepository1.0-dev libusb-1.0-0-dev libzip-dev uvcdynctrl python-setuptools libxml2-dev libpcap-dev libaudit-dev libnotify-dev autoconf intltool gtk-doc-tools

# Runtime dependencies
sudo apt-get -y install gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly libxml2 libpcap0.8 libaudit1 libnotify4

git clone --recursive https://github.com/TheImagingSource/tiscamera.git
cd tiscamera
mkdir build
cd build

# With ARAVIS:
cmake -DBUILD_ARAVIS=ON -DBUILD_GST_1_0=ON -DBUILD_TOOLS=ON -DBUILD_V4L2=ON -DCMAKE_INSTALL_PREFIX=/usr ..
# Without ARAVIS
# cmake -DBUILD_ARAVIS=OFF -DBUILD_GST_1_0=ON -DBUILD_TOOLS=ON -DBUILD_V4L2=ON -DCMAKE_INSTALL_PREFIX=/usr ..

make
sudo make install

# **************************************** Aravis
# GET THE COPY IN GITLAB REPOSITORY and coy it your home:
cp /home/malbec/malbec/Software/Odroid/Acquisition/aravis-0.6.0-malbec.tar.xz ~/
cd
tar xvf aravis-0.6.0-malbec.tar.xz
cd aravis-0.6.0
./configure --enable-gst-plugin --enable-introspection=yes --enable-usb
make
sudo make install
#sudo ln -s /usr/local/lib/python3.5/dist-packages/Aravis-0.6.typelib /usr/lib/girepository-1.0
sudo ln -s /usr/local/lib/girepository-1.0/Aravis-0.6.typelib  /usr/local/lib/python3.5/dist-packages/
sudo ln -s /usr/local/lib/girepository-1.0/Aravis-0.6.typelib  /usr/lib/girepository-1.0
sudo ldconfig

Pour les camras USB, il faut ajouter les permissions de la manire suivante.
1) Editer le fichier /etc/udev/rules.d/aravis.rules
sudo vim /etc/udev/rules.d/aravis.rules
2) Ajouter les lignes suivantes:
# Basler
SUBSYSTEM=="usb", ATTRS{idVendor}=="2676", MODE:="0666", TAG+="uaccess", TAG+="udev-acl"
# The Imaging Source
SUBSYSTEM=="usb", ATTRS{idVendor}=="199e", MODE:="0666", TAG+="uaccess", TAG+="udev-acl"
# Point Grey Research
SUBSYSTEM=="usb", ATTRS{idVendor}=="1e10", MODE:="0666", TAG+="uaccess", TAG+="udev-acl"
3) Eventuellement ajouter les codes camras obtenus par lsusb
4) Rebooter

# **************************************** Test

Pour voir une camra:
camera-ip-conf -l
Tester tiscamera
Tester acquisition Aravis:
./aravis-0.6.0/tests/arv-camera-test

Pour voir toutes les camras:
sudo arv-tool-0.6 -d all:
Le sudo est ncessaire pour voir les camras USB3

# **************************************** Montage de la carte SD pour la migration des blocs
La commande
sudo fdisk -l
permet de voir l'ensemble des devices. On trouve par exemple la carte SD:
Disk /dev/mmcblk1: 14.9 GiB, 15931539456 bytes, 31116288 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes

La commande:
sudo blkid
liste aussi les devices connects.

Pour crer une partition ext4 sur une nouvelle carte:
lsblk				Pour trouver le nom du device
sudo dd if=/dev/zero of=/dev/mmcblk1 bs=1M count=100
sudo sync
sudo mkfs.ext4 -L "data" /dev/mmcblk1

Pour monter manuellement le device:
sudo mkdir /mnt/data
sudo mount /dev/mmcblk1 /mnt/data

Changer les droits:
sudo chmod 777 /mnt/data

Pour un montage permanent:
sudo vim /etc/fstab

et ajouter la ligne:
/dev/mmcblk1 /mnt/data ext4 defaults 0 0

# **************************************** RTC
Une fois la batterie du RTC branche, il suffit de faire:
sudo hwclock -w
pour mettre  jour la RTC.
sudo hwclock -r
permet de lire l'heure du RTC

# **************************************** Serveur VNC
On utilise TightVNC:

sudo apt-get update
sudo apt install xfce4 xfce4-goodies tightvncserver
vncserver

A ce stade il faut entrer un mot de passe. On continue en configfurant le serveur VNC:
vncserver -kill :1
cp ~/.vnc/xstartup ~/.vnc/xstartup.bak
vim ~/.vnc/xstartup

et remplacer le contenu par:

#!/bin/sh
unset DBUS_SESSION_BUS_ADDRESS
xrdb $HOME/.Xresources
xsetroot -solid grey
# Fix to make GNOME work
export XKL_XMODMAP_DISABLE=1
exec /usr/bin/mate-session &

Tester ensuite avec:
vncserver :1 -geometry 1920x1080 -depth 24
ou
vncserver :1 -geometry 1366x768 -depth 24
pour une moindre rsolution.

Pour lancer le serveur VNC au dmarrage:

# Mise  jour de rc.local
echo "# Start the VNC server" | sudo tee --append /etc/rc.local
sudo sed -i -e 's/exit 0//' /etc/rc.local
echo "su -c \"vncserver :1 -geometry 1920x1080 -depth 24 \" malbec" | sudo tee --append /etc/rc.local
echo "exit 0" | sudo tee --append /etc/rc.local

Se connecter avec un client VNC sur <adresse IP>:5901


# **************************************** DHCP
-s'assurer que la carte réseau est en DHCP, adresse dynamique
-si besoin, pour renouveler l'adresse, faire :
ifconfig /release
ifconfig /renew




# **************************************** Pylon Viewer

#wget https://www.baslerweb.com/fp-1535524592/media/downloads/software/pylon_software/pylon-5.1.0.12682-armhf.tar.gz
cp malbec/Software/Odroid/Acquisition/pylon-5.1.0.12682-armhf.tar.gz ./
tar xvf pylon-5.1.0.12682-armhf.tar.gz

Les dtails de l'installation sont dans le fichier INSTALL:

cd pylon-5.1.0.12682-armhf
sudo tar -C /opt -xzf pylonSDK-5.1.0.12682-armhf.tar.gz
./setup-usb.sh

Unplug and replug all USB cameras to get the udev rules applied.

Pour tester la camra:
/opt/pylon5/bin/PylonViewerApp

# symlink PylonViewer to Desktop:
ln -s /opt/pylon5/bin/PylonViewerApp ~/Desktop/




# **************************************** Lancement de Malbec

# copy malbec program source in HOME:
cp ~/malbec/Software/Odroid/Acquisition/Source_Malbec_V3f.zip ~/
cd
unzip Source_Malbec_V3d.zip
# if you are asked to overwrite some already existing file say yes to all [A]ll
# change authorizations of ./comp.sh:
chmod u+x ./comp.sh
# compile malbec acquisition code:
./comp.sh malbec3

##### MALBEC Acquisition program launch description
Arguments (<n> represents an integer and <f> a floating-point number):
-e <f> Starting exposure time in seconds
-g <n> Gain
-f <f> Target frame rate
-n <n> Number of image blocks to be acquired
-l <n> Duration of an image block in seconds
-i <n> Throughput to migrate blocks to the SD card in MB/s
-b <n> USB throughput to be used in bits/s (e.g. 70000000)
--wait <f> Wait time before starting acquisitions in minutes
--addup Add blocks to existing ones if they exist
--start Start acquisitions after a specific time given in file malbec.start
--gps-elevation <n> Elevation in meters above which acquisitions start

Examples:
Command used for the flight on 2019-02-19
./malbec3 -e 0.02 -g 10 -f 20 -n 360 -l 15 -b 70000000 -i 20 --wait 20 --gps-elevation 10000 &
Test command
./malbec3 -e 0.02 -g 10 -f 20 -n 10 -l 15 -b 70000000 -i 20 --wait 0 --gps-elevation 10000 &


# **************************************** Effacement des acquisitions prcdentes
Pour effacer toutes les images et les logs, le script 'clean' suivant est utilis:

#!/bin/bash
rm block*.raw
rm block*.stamps
rm malbec.log
rm image_analysis.log
rm block_migration.log
rm /mnt/data/block*.raw
rm /mnt/data/block*.stamps



# **************************************** Script de compilation de Malbec

#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Syntax: comp_arc <C file without extension>"
    exit
fi
gcc -DHAVE_CONFIG_H -I. -I/home/malbec/aravis-0.6.0/src  -g -I/home/malbec/aravis-0.6.0/src/ -pthread -I/usr/include/libxml2 -I/usr/include/glib-2.0 -I/usr/lib/arm-linux-gnueabihf/glib-2.0/include -I/usr/include/libusb-1.0  -Wall -g -O2 -MT $1.o -MD -MP -MF ./$1.Tpo -c -o $1.o $1.c
gcc -c log.c
rm -f ./$1.Tpo
rm -f $1
/bin/bash /home/malbec/aravis-0.6.0/libtool --silent --tag=CC --mode=link gcc -Wall -g -O2 -o $1 $1.o log.o /home/malbec/aravis-0.6.0/src/libaravis-0.6.la -lm -lgio-2.0 -lgobject-2.0 -lxml2 -lgthread-2.0 -pthread -lglib-2.0 -lz -lusb-1.0 -laudit
