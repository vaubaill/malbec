"""Compute fall point given a weather file and an initial point.

"""

import os
from astropy.coordinates import EarthLocation, cartesian_to_spherical, Longitude, Latitude
from astropy.table import QTable
import astropy.units as u
from scipy import interpolate
from scipy.integrate import odeint
import simplekml
import numpy as np

def get_wind_from_interpolator(windF,winD,loc):
    """get wind vector in 3D
    
    Parameters
    ----------
    windF : interpolate.interp1d Object
        interpolator for the wind force
    windD : interpolate.interp1d Object
        interpolator for the wind direction
    loc : astropy.coordinates.EarthLocation Object
        location where to get the weather data
    
    Returns
    -------
    wind_force : float
        wind force in m/s
    wind_direction : float
        wind direction in deg, origin at North.
    
    """
    Altitude = loc.height.to('m').value
    wind_force = windF(Altitude)
    wind_direction = windD(Altitude)
    return (wind_force*u.m/u.s,wind_direction*u.deg)
    

def get_wind(weather_data,loc):
    """get wind vector in 3D
    
    Parameters
    ----------
    weather_data : astropy.table.QTable Object
        weather data from a cor file
    loc : astropy.coordinates.EarthLocation Object
        location where to get the weather data
    
    """
    windF = interpolate.interp1d(weather_data['WindF'],weather_data['Altitude'])    # wind velocity [m/s]
    windD = interpolate.interp1d(weather_data['WindD'],weather_data['Altitude'])    # wind direction [m/s]
    Altitude = loc.height.to('m').value
    wind_force = windF(Altitude)
    wind_direction = windD(Altitude)
    return

def compute_rhoatmo(P,T,method='SI'):
    """Compute air density in kg/m-3
    
    Parameters
    ----------
    P : float
        Pressure [Pa]
    T :  float
        Temperature [K]
    model : str, optional
        model to compute the density: choice between:
        'SI' => rhoatmo= P / R.T with R=287.05 [u.J/(u.K*u.kg)] for dry air.
        'Ce' => from Ceplecha 1987 [1]_:
        rhoatmo=(34.83676*(u.K/u.Pa)*self.datameteo[0].to('Pa')/self.datameteo[1].to('K'))
        # rho=3.483676*P/T x1.0E-04 g/cm^3 with P in [hPa]
        => rho=3.483676*P/T x10 kg/m^3 with P in [Pa]
    
    Return
    ------
    rhoatmo : float
        air density [kg/m-3]
    
    Notes
    -----
    Details of the algorythm can be found in [1]_.
    
 .. [1] Ceplecha, 1987, Geometric, dynamic, orbital and photometric data on meteoroids
    from photographic fireball networks. Astronomical Institutes of Czechoslovakia,
    Bulletin (ISSN 0004-6248), vol. 38, July 1987, p. 222-234.
    http://cdsads.u-strasbg.fr/abs/1987BAICz..38..222C
    
    See Also
    --------
    https://wahiduddin.net/calc/density_altitude.htm
    
    """
    # note: specific gas constant for dry air=287.05 J/(kg.K)
    R_air = 287.05 *u.J/(u.K*u.kg)
    # D=P/RT
    rhoatmo_SI = (P.to('Pa')/(R_air.decompose() * T.to('K'))).to('kg/m3')
    #    rho=3.483676*P/T x1.0E-04 g/cm^3 with P in [hPa]
    # => rho=3.483676*P/T x10 kg/m^3 with P in [Pa]
    rhoatmo_Ce = (34.83676*(u.K/u.Pa)*P.to('Pa')/T.to('K'))
    if (method=='Ce'):
        rhoatmo_out = rhoatmo_Ce
    else:
        rhoatmo_out = rhoatmo_SI
    return rhoatmo_out

def compute_Mach(V,P,rho):
    """Compute Mach number
    
    Parameters
    ----------
    V : astropy.coordinates.Quantity Object
        velocity norm, in [m/s]
    P : astropy.units.Quantity 
        Pressure, in [Pa]
    rho : astropy.punits.Quantity Object
        atmospheric density
        
    Returns
    -------
    Mach : Mach number - no dimension
    
    """
    c_sound = np.sqrt(1.4 * P.decompose() / rho).to('m/s')
    Mach = V.to('m/s') / c_sound.to('m/s')
    return Mach

def compute_new_position(loc,V,dt):
    """Compute new position given velocity
    
    Parameters
    ----------
    loc : astropy.coordinates.EarthLocation Object
        location of the falling object to integrate.
    V : astropy.coordinates.Quantity Object
        3D-velocity vector in m/s
    dt : astropy.time.DeltaTime Object
        tie step
    
    Returns
    -------
    newloc :astropy.coordinates.EarthLocation Object
        new location of the falling object to integrate.
    
    """
    R_pla = (1.0*u.R_earth).to('m')
    vel   = [V[0].to('m/s').value,V[1].to('m/s').value,V[2].to('m/s').value]
    Vrlatlon = cartesian_to_spherical(vel[0],vel[1],vel[2])
    nvel  = Vrlatlon[0]*u.m/u.s # [m/s]
    Vxy = nvel * np.cos(Vrlatlon[1]) # velocity in North-East plan [m/s]
    Dxy = Vxy * dt.to('s')  # Displacement in North-East plan [m]
    dN = Dxy * np.sin(Vrlatlon[1]) # displacement towards the North [m]
    dE = Dxy * np.cos(Vrlatlon[1]) # displacement towards the East [m]
    azdz = [0*u.m/u.s,0.0*u.rad,0.0*u.rad]  # [velocity [m/s], zenith distance [rad], astronomical azimuth [rad]]
    azdz[0] = Vrlatlon[0]*u.m/u.s           # modulus of velocity vector [m/s]
    azdz[2] = (90*u.rad-Vrlatlon[2])        # astronomical azimuth of velocity vector [rad]
    azdz[1] = Vrlatlon[1]                   # zenithal angle of velocity vector [rad]
    # note: ax [deg]: Eastward wind, according to Ceplecha's convention
    ax = ((azdz[2].to('deg').value+90.0)%360.0)*u.deg
    dlat = u.rad*((np.cos(azdz[2].to('rad').value)*dN / (R_pla+loc.height)                                 )+(np.cos(ax.to('rad').value)*dE/( R_pla+loc.height)                                 ) ) # [rad]
    dlon = u.rad*((np.sin(azdz[2].to('rad').value)*dN /((R_pla+loc.height)*np.cos(loc.lat.to('rad').value)))+(np.sin(ax.to('rad').value)*dE/((R_pla+loc.height)*np.cos(loc.lat.to('rad').value))) ) # [rad]
    dh = V[2].to('m/s') * dt.to('s')
    """
    print 'V=',V
    print vel
    print dh
    print Vxy,Dxy,dN,dE,dlon,dlat
    """
    newloc = EarthLocation.from_geodetic(loc.lon+dlon,
                                         loc.lat+dlat,
                                         loc.height+dh)
    #print 'newloc=',newloc.lon,newloc.lat,newloc.height,vel, dN, dE,azdz[0],azdz[1].to('deg').value,azdz[2].to('deg').value
    return newloc


def velocity_derivative(v,t,windV,rhoatmo,mass,Diam):
    """computes the derivative of the velocity for the calculation of Fall.
    
    Parameters
    ----------
    v : numpy array [3]
        relative velocity vector in m/s
    t : float
        time in s
    
    Returns
    -------
    dv : numpy array [3]
        dv/dt: derivative of velocity wrt height [m/s]
    
    """
    
    vel = v *u.m/u.s
    # compute relativ wind velocity vector
    relwind = windV - vel
    # compute relative wind velocity norm in [m/s]
    relwindnorm = np.linalg.norm(np.array([relwind[0].to('m/s').value,
                                           relwind[1].to('m/s').value,
                                           relwind[2].to('m/s').value]))
    # relative wind velocity unit vector
    relwindunit = relwind / relwindnorm
    # compute cx of a sphere
    mu = 1.8E-05*u.kg/u.m/u.s # air dynamic viscosity
    Re = relwindnorm * Diam * rhoatmo / mu  # Reynold'snumber
    # https://fr.wikipedia.org/wiki/Fichier:Drag-fr.svg
    cx = 0.47*u.m/u.s# 24.0 / Re # consider laminar case.
    #cx = 100.0*u.m/u.s# 24.0 / Re # consider laminar case.
    # compute equivalent surface
    S = 4.0 * np.pi * (Diam/2)**2.0
    # compute drag force
    Fdrag_mod = 0.5 * rhoatmo * S * cx * relwindnorm**2.0
    Fdrag = Fdrag_mod * relwindunit
    # compute drag acceleration
    Adrag = Fdrag / mass
    # compute gravity acceleration
    AG = [0.0,0.0,-9.81]*u.m/u.s/u.s
    # compute total acceleration
    dv= Adrag + AG
    # arrange output vector
    dvout = np.asarray([dv[0].to('m/s2').value,dv[1].to('m/s2').value,dv[2].to('m/s2').value])
    
    """
    print 'vel=',vel
    print 'windV=',windV
    print 'relwind=',relwind
    print 'relwindnorm=',relwindnorm
    print 'relwindunit=',relwindunit
    print 'rhoatmo=',rhoatmo
    print 'mu=',mu
    print 'Re=',Re
    print 'cx=',cx
    print 'Fdrag_mod=',Fdrag_mod
    print 'Adrag=',Adrag
    print 'AG=',AG
    print 'dv=',dv
    """
    
    return dvout


########### MAIN ###########3

# Object features
Diam=0.1*u.m
mass=0.150*u.kg

# break point
break_point = EarthLocation.from_geodetic(Longitude('00:28:08.40',unit='deg'),
                            Latitude('43:37:07.74',unit='deg'),
                            32.0*u.km)
# weather file
cor_file = '/home/vaubaill/PROJECTS/PODET/PODET-MET/MALBEC/DATA/201806/GPS/AS2018062612_1.dat'
weather_data = QTable().read(cor_file,format='ascii.fixed_width',delimiter=' ')
# construct the interpolator object for the wind
windF = interpolate.interp1d(weather_data['Altitude'],weather_data['WindF'],fill_value=np.array(weather_data['WindF'][0],weather_data['WindF'][-1]))    # wind velocity [m/s]
windD = interpolate.interp1d(weather_data['Altitude'],weather_data['WindD'],fill_value=np.array(weather_data['WindD'][0],weather_data['WindD'][-1]))    # wind direction [m/s]
Press = interpolate.interp1d(weather_data['Altitude'],weather_data['Press'],fill_value=np.array(weather_data['Press'][0],weather_data['Press'][-1]))    # pressure [m/s]
Temp  = interpolate.interp1d(weather_data['Altitude'],weather_data['T']    ,fill_value=np.array(weather_data['T'][0],weather_data['T'][-1]))    # pressure [m/s]


# integrator setting
R_earth = (1.0*u.R_earth).to('m')
dh = -10.0*u.m # in meters
t = 0.0*u.s
dt = 1.0 *u.s # dt in [s]
vel_ms = [0.1,0.1,0.0]
vel = vel_ms*u.m/u.s # initial velocity in m/s
loc = break_point

keep_going=True
while(keep_going):
    try: 
        (windF,windD) = get_wind_from_interpolator(windF,windD,loc)
    except:
        windF = weather_data['WindF'][-1]
        windV = weather_data['WindD'][-1]
    windV = [windF*np.sin(windV),windF*np.cos(windV),0.0]*u.m/u.s
    try: 
        (P,T) = (Press(loc.height.to('m')),Temp(loc.height.to('m')))
    except:
        P = weather_data['Press'][-1]
        T = weather_data['T'][-1]
    rhoatmo = compute_rhoatmo(P*u.Pa,(T+273.15)*u.K)
    delta_t = [t.to('s').value,(t+dt).to('s').value]
    delta_h = [loc.height.to('m').value, (loc.height+dh).to('m').value]
    #print 'vel_ms=',vel_ms
    ODE_output = odeint(velocity_derivative,vel_ms,delta_t,args=(windV,rhoatmo,mass,Diam),full_output=True) #,rtol=1.0E-12,atol=1.0E-12) # ,full_output=True)
    #print 'ODE_output=',ODE_output[0][1]
    vel_ms = [ODE_output[0][1][0],ODE_output[0][1][1],ODE_output[0][1][2]]
    vel_norm = np.linalg.norm(vel_ms)*u.m/u.s
    vel = vel_ms*u.m/u.s
    #print 'vel_ms=',vel_ms
    newloc = compute_new_position(loc,vel_ms*u.m/u.s,dt)
    loc    = EarthLocation.from_geodetic(newloc.lon,newloc.lat,newloc.height)
    t = t + dt
    # copmute Mach number
    Mach = compute_Mach(vel_norm,P*u.Pa,rhoatmo)
    print t,vel_norm,Mach,loc.height,loc.lon,loc.lat
    if (loc.height<0.0):
        keep_going=False

