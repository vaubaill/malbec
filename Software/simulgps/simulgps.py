"""Simulates a GPS measurement during a MALBEC flight.

this is useful for the MissionPlanner software tests.

Author
------
Jeremie Vaubaillon, IMCCE, 2019


Syntax
------
python3 simulgps.py real_file simu_file

Parameters
----------
real_file : string
    Name of the real GPS file from which data are taken.
simu_file : string
    Name of simulated GPS file where data are output.

Returns
-------
None.
    Data from gps_real are copied into gps_simu at a rate of one data per second,
    in order to simulate real GPS data that are being writte during a flight.

Example
-------
Launch without time constrain:
python3 simulgps.py ~/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CNES/AS2019052306_1.dat ~/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/Weather/real_time_test.dat

Laucnh with time constrain:
python3 simulgps.py ~/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/CNES/AS2019052306_1.dat ~/PROJECTS/PODET/PODET-MET/MALBEC/malbec/DATA/20190523/Weather/real_time_test.dat 2019-05-23T10:23:41.000

"""
import sys
import os
import time
from astropy.table import QTable,Column,Row
from astropy.time import Time
import astropy.units as u

# read arguments
real_file=sys.argv[1]
simu_file=sys.argv[2]
try:
    time_start_str=sys.argv[3]
    accountime=True
except:
    time_start_str=None
    accountime=False

if time_start_str:
    try:
        time_start = Time(time_start_str,format='isot')
    except:
        msg = '*** Time is not in ISOT format'
        log.error(msg)
        raise ValueError(msg)

with open (real_file, "r") as real:
    listOfLines = real.readlines()

ignorefirstline=True
for line in listOfLines:
    with open(simu_file, mode='a') as simu:
        simu.seek(0,os.SEEK_END)
        simu.write(line)
        # now determine if it is worth waiting or not
        if ignorefirstline:
            ignorefirstline=False
            continue
        # set wait to True by default
        wait = True
        # check if time is to be taken into account
        if accountime:
            # compute time of current line
            time_cur = Time(line.split()[0],format='isot')
            if time_cur<time_start:
                wait = False
        if wait:
            time.sleep(1.0)
