"""Simulates a GPS measurement from SODAQ card during a MALBEC flight.

this is useful for the MissionPlanner software tests.

Author
------
Jeremie Vaubaillon, IMCCE, 2019


Syntax
------
python3 simulgps.py real_file simu_sodaq_file

Parameters
----------
real_file : string
    Name of the real GPS file from which data are taken.
simu_file : string
    Name of simulated GPS file where data are output.
time_start : string, optional
    Time after which data are saved at a given frequency.
    Before this time (is real_file) data are just dumped into simu_file.
    Then the dumping happens at frequancy freq.
time_real : int
    1/Frequency of data in real_file.
    Default is 1 sec, meaning that real data were written once per second.
time_simu : int
    1/Frequency of data dumping into simu_file.
    Default is 1 sec, meaning that data are written once every second.
    A value of 5 would mean that data are dumped every 5 second.

Returns
-------
None.
    Data from gps_real are copied into gps_simu at a rate of one data per second,
    in order to simulate real GPS data that are being writte during a flight.

Example
-------
Launch without time constrain:
python3 simulgps_sodaq.py ~/malbec/DATA/20190523/CNES/AS2019052306_1.dat ~/malbec/DATA/20190523/Trajectory-testLoRa/trajectory_rlt_sodaq.dat

Laucnh with time constrain:
only descent (starting at top: ~29000 m
python3 simulgps_sodaq.py ~/malbec/DATA/20190523/CNES/AS2019052306_1.dat ~/malbec/DATA/20190523/Trajectory-testLoRa/trajectory_rlt_sodaq.dat 2019-05-23T09:59:02.000
only descent below 8000 m
python3 simulgps_sodaq.py ~/malbec/DATA/20190523/CNES/AS2019052306_1.dat ~/malbec/DATA/20190523/Trajectory-testLoRa/trajectory_rlt_sodaq.dat 2019-05-23T10:23:41.000

"""
import sys
import os
import time
from astropy.table import QTable,Column,Row
from astropy.time import Time
import astropy.units as u

# read arguments
try:
    real_file=sys.argv[1]
    simu_file=sys.argv[2]
except:
    msg = '*** FATAL ERROR: syntax is: python3 simulgps_sodaq.py real_file simu_file [time_start] [freq]'
    raise ValueError(msg)

try:
    time_start_str=sys.argv[3]
    accountime=True
except:
    time_start_str=None
    accountime=False

# set time of start
if time_start_str:
    try:
        time_start = Time(time_start_str,format='isot')
    except:
        msg = '*** Time is not in ISOT format'
        log.error(msg)
        raise ValueError(msg)

# real original GPS measurements data file
with open (real_file, "r") as real:
    listOfLines = real.readlines()

# set ignorefirstline
ignorefirstline=True
# loop over initial GPS measurements file.
for line in listOfLines:
    with open(simu_file, mode='a') as simu:
        simu.seek(0,os.SEEK_END)
        # now determine if it is worth waiting or not
        if ignorefirstline:
            ignorefirstline=False
            continue
        # get SODAQ info
        params = line.split()
        t = Time(params[0],format='isot')
        lon = float(params[3]) *u.deg
        lat = float(params[2]) *u.deg
        alt = float(params[1]) *u.m
        # write into SODAQ-type file
        simu.write(t.isot+' '+str(lon.to('deg').value)+' '+str(lat.to('deg').value)+' '+str(alt.to('m').value)+'\n')
        
        # set wait to True by default
        wait = True
        # check if time is to be taken into account
        if accountime:
            # compute time of current line
            time_cur = Time(line.split()[0],format='isot')
            if time_cur<time_start:
                wait = False
        if wait:
            time.sleep(1.0)
