# MALBEC

Meteor Automated Light Balloon Experimental Camera

## Where to Start?

Read the Software description.
Read the HOWTO section.

# Software : 
    
    missionplanner : computes the trajectory given winds and initial conditions, for a single balloon.
        Simulates the ascension and descent of a stratospheric balloon given input characteristics.
        Creates SPICE kernel of trajectory.
    
    obsplanner : doublbe-station observation optimization.
        Input from missionplanner are mandatory (trajectory as SPICE kernels) for each station.
        Computation of azimuth/elevation of Sun, Moon, radiant.
        Considers the two cameras are pointing towards each other, and add tilt angles in 2D. Computes optimal poitning directions given these constraints.
        Computation of observed area for each station and for both stations.
        Concludes regarding the observation feasibility.
        If feasibility is ok, creates fakeors and simulates their observation from each camera.
    campaignplanner : launches missionplanner and obsplanner automatically.
        Allows one to explore all different settings and geometry at once.
        Might take a long time to run.
    conf : includes all necessary configuration files, common to all packages.
        sites.in: possible lauch / landing sites
        fixed_stations.in: possible fixed stations
        spice_kernels: the basics ones.
    showersim : scripts necesssary to characterize and simulate a meteor shower
        Definition of the following (python) objects: MeteorShower, Fakeor, FakeObs, Anchor, Radiant, AFM (launches the Fortran simulation of a physical meteor ; needs extra package).
    reduction : tools to reduce the data.
        Definition of the following Objects: Camera, Lens, Filter.
        Conversion of file types (csv, dat, kml)
        Block processing (from Basler acquisition)
        Compute expected sky brightness thanks to ONERA codes.
        Compute twilight times as a function of altitude and location.
    stationmaker : utility to quickly create a fixed station.
        Useful for ground-based observations.
    simulgps : tools to simulates a communication with LoRa card.
        Useful for test purpose.
    skybackground : compute theoretical sky background
        Under development. See the reduction package.
    airplanemaker : case of a single airplane.
        Makes the trajectory SPICE kernels from either predictions of in-flight data.
        Computes the monitored area given camera features.
        Eventually, *must* be included in missionplanner.
    Odroid : anything that goes to the Odroid
        Acquisition : acquisition softwares
        Trajectory : trajectory prediction softwares: replaced by missionplanner.
    LoRa : LoRa communication.
        Allows communications between nacelle and ground.
        Under development.



## CheckLists : all possible checklists

    pre-departure checklist : so you won't forget anything you would need before
        the flight at home..

    pre-flight checklists : so the flight goes well.

    post-flight checklist : so the processing goes well and data are saved.

    




## Typical git commands:
    it is all descried here:
    https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
    but here are the most useful ones:

    # clone current MALBEC git project to your PC
    git clone https://gitlab.com/vaubaill/malbec.git

    # get current data (from master branch ; so far there is no other branch)
    git checkout master

    # in case, Download the latest changes
    git pull origin

    # check status
    git status

    # view differences
    git diff

    # add toto file (or folder)
    git add toto

    # commit the change
    git commit -m "JV adds toto file"

    # send changes to master
    git push origin master

    # delete all local changes
    git checkout .

    # Delete all untracked changes in local repository
    git clean -f

    #Undo most recent commit
    git reset HEAD~1



## Dependencies:

    astropy: www.astropy.org

    numpy: https://numpy.org/ 

    scipy: https://www.scipy.org/

    simplekml: https://simplekml.readthedocs.io/en/latest/

    matplotlib: https://matplotlib.org/

    pygc: https://pypi.org/project/pygc/

    pygrib: https://jswhit.github.io/pygrib/docs/

# HOWTO

In order to run a complete simulation, the different steps are the following:
## missionplanner
This package simulates the trajectory of a single stratospheric balloon. The output is a SPICE trajectory kernel file (spk).
In order to optimise a double-station observation, this package needs to be run twice for 2 different initial conditions (location especially).
Note that the concept can be extended to ANY type of so called spacecraft (in SPICE sense). In particular, airplane can be considered (thanks to the airplaneplanner package).
What matters most is that the output is a SPICE kernel (spk).

## obsplanner
This package computes the ideal observation angles to maximize the atmosphere area monitored by the 2 cameras onboard the 2 spacecraft (balloon, airplane, satellite or even fixed stations). For this, 2 SPICE spk kernels created by missionplanner (or any other tool) are needed.
The user can set the camera and lens type. By default, the orientation angle is computed so that the 2 spacecraft camera point towards eachother. However, a tilt angle might be applied, and the optimum angle is computed considering such angle.
Constraints if the pointing direction of the cameras w.r.t the targeted meteor shower radiant, the Moon or even the Sun can be defined. It helps decide if a given observation is feasible given the constraints. If no constraint is desired, the user might simply set such angular constraints to negative values.

The program also computes the area (in km^2) monitored by each camera and by both cameras.
If the [AFM Frotran package](https://gitlab.com/vaubaill/AFM) is installed, the program can even simulate the observation of meteors by simulating the entry of meteoroids given the conditions of observation and the targeted meteor shower.
Fake observations are cerated and can be injected into a meteor reduction software to asses the expected number of detected double-station meteors, as well as the trajectory and orbit accuracy.

