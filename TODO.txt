MALBEC TODO LIST

CheckList :
-make all check lists better before next flight (all)

Nacelle : 
-increase inertia for better stability (AC)

Environnement Electromagnétique : 
-caractériser le rayonnement électromagnétique de la nacelle, au sol

Odroid : 
X-JV : URGENT : Update acquisition program version 3f on ALL Odroids + install wiringPi + smbus2 python library -> done sur l'eMMC "valide"
X-PD : electro-mag compatibility between camera and GPS to solve (PD)
X-PD : Faire des tests avec le GPS et la caméra en fonction (celle donnée par Jérémie) pour confirmer les interférences qui empêchent le fix GPS. Essayer d'identifier une solution, quitte à déporter une antenne.
X-PD : Dans l'utilitaire d'extraction des images, ajouter la gain et le temps d'exposition dans les images FITS . Jérémie me fournit d'autres informations éventuelles à ajouter. => si on peut mettre la localisation c'est parfait stp :-)
X-JV : faire un test de démarrage d'acquisition pour une date / altitude donnée
-PD : Résoudre le problème avec la petite carte BME280 qui avait empêché son utilisation sur le vol technique du 19 février. Tester le BME280 dans l'environnement Malbec.
X-PD : déveloper un moyen de reprendre les acquisitions après remplissage eMMC + transfer blocks vers SD
-JV : faire des acquisitions avec l'ancienne caméra pour soustraire la contribution de la hauteur du Soleil et estimer le gain de magnitude avec l'altitude uniquement
-JV : faire un test d'acquisition pour mesure S/N=f(fond de ciel) : e.g. avec / sans Lune
-JV : faire un test de reprise d'acquisition après perte d'alimentation
-JV+PD : faire un programme de mise au point de l'objectif de la caméra : prise d'image, mesure psf, affichage psf, détection de mieux/pire
-JV : reformat le 2ème eMMC + reinstall all lib and soft
-JV : CHECKER et CHANGER les FILTRES !!!



Arduino : 
-mesurer la consommation de l'arduino + catpeur de pression + servo

Data reduction Software : 
-dev a way to measure sky background (JV)

Trajectory Prediction Software : 
-JV : finish the MaBallTraP soft
LC : rajoute Vvert
RM : propage liveobject.py sous gitlab
JV : change liveobject.py pour créer l'objet MissionPlanner au moment
